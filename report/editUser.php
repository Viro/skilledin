<?php
session_start();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

error_reporting(E_ALL);
require '../../vendor/autoload.php';

if (!isset($_SESSION['user']) || ($_SESSION['user']['idRuolo'] != 0 && $_SESSION['user']['idRuolo'] != 1 )) {
    header("location:index.php");
}
require_once($_SERVER['DOCUMENT_ROOT'] . "/skilledin/report/obj/Skilledin.php");

if(isset($_POST['action'])) {
    if($_POST['action'] == 'editUser') {
        //Errore utente da modificare mancante
        if(!isset($_POST['update_user_id'])) header("location:index.php");

        $skilledin                    = new Skilledin();
        $platforms                    = $skilledin->getPlatforms();
        $active_user                  = $skilledin->getUser($_SESSION['user']['id']);
        $update_user                  = $skilledin->getUser($_POST['update_user_id']);
        $update_user['idPiattaforma'] = explode(",", $update_user['idPiattaforma']);
    }
    if($_POST['action'] == 'updateUser'){
        if(!isset($_POST['update_user_id'])) header("location:index.php");
        $skilledin                    = new Skilledin();
        $data = array();
        if(isset($_POST['email'])    && strlen(trim($_POST['email']))) $data['email'] = $_POST['email'];
        if(isset($_POST['password']) && strlen(trim($_POST['password']))){
            $data['password'] = $_POST['password'];
        }
        if(isset($_POST['role'])     && $_POST['role'] >= 0){
            $data['idRuolo'] = $_POST['role'];
            $data['ruolo']   = '';
            if($_POST['role'] == 0) $data['ruolo'] = 'admin';
            if($_POST['role'] == 1) $data['ruolo'] = 'manager';
            if($_POST['role'] == 2) $data['ruolo'] = 'utente/personale interno';
            if($_POST['role'] == 3) $data['ruolo'] = 'azienda/ente';
            if($_POST['role'] == 4) $data['ruolo'] = 'supervisor';
            if($_POST['role'] == 5) $data['ruolo'] = 'Coordinatore piani';
        }
        if(isset($_POST['platform'])) $data['idPiattaforma'] = implode(",", $_POST['platform']);
        $result = $skilledin->editUser((int) $_POST['update_user_id'], $data);
        $active_user                  = $skilledin->getUser($_SESSION['user']['id']);
        $update_user                  = $skilledin->getUser($_POST['update_user_id']);
        $update_user['idPiattaforma'] = explode(",", $update_user['idPiattaforma']);
        $platforms                    = $skilledin->getPlatforms();
        if($result->getMatchedCount() == 1 && $result->getModifiedCount() == 1) $update_message =  'Modifica eseguita correttamenete';
    }
    if($_POST['action'] == 'deleteUser'){
        $skilledin                    = new Skilledin();
        $update_user                  = $skilledin->getUser($_POST['update_user_id']);
        if($update_user){
            $skilledin->deleteUser((int) $_POST['update_user_id']);
            $delete_message = "Eliminazione eseguita correttamente";
        }else{
            $delete_message = "Nessun utente da eliminare trovato";
        }
    }
}
?>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css">

    <title>Skilledin - Modifica utente</title>

    <style>
        .field-icon {
            float: right;
            margin-left: -35px;
            margin-top: -35px;
            position: relative;
            z-index: 2;
        }
        .btn-create {
            font-size: 0.9rem;
            letter-spacing: 0.05rem;
            padding: 0.75rem 1rem;
        }
    </style>
</head>

<body>
<div class="container">
    <div class="row">
        <a href="confirmPage.php?admin" class="previous" style="text-decoration: none;margin-top: 10px;">‹ Torna indietro</a>
        <?php if(isset($delete_message)){  echo $delete_message ?>
                    </div>
                </div>
            </body>
        <?php exit; ?>
        <?php } ?>
        <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">

            <div class="card border-0 shadow rounded-3 my-5">
                <div class="card-body p-4 p-sm-5">
                    <h5 class="card-title text-center mb-5 fw-light fs-5">Modifica utenza</h5>
                    <?php if(isset($update_message)) echo '<p class="text-success"><strong>' . $update_message . '</strong></p>'; ?>
                    <form action="" method="POST">
                        <input type="hidden" name="update_user_id" value="<?php echo $update_user['id'] ?>" />
                        <p id="error" style="display:none;color: red;" >Indirizzo email già esistente!</p>
                        <div class="form-floating mb-2">
                            <input type="email" class="form-control" id="floatingInput" name="email" placeholder="name@example.com" onkeyup="verifyEmail(this.value)" value="<?php echo $update_user['email'] ?>">
                            <label for="floatingInput">Indirizzo email</label>
                        </div>
                        <div class="form-floating mb-2">
                            <input type="password" class="form-control" id="floatingPassword" name="password" placeholder="******">
                            <label for="floatingPassword">Nuova password</label>
                            <span id="toggle" toggle="#floatingPassword" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                        </div>
                        <div >
                            <select class="form-select" name="role" id="role"
                                    aria-label="Floating label select example">
                                <option value="-1" selected="" >Seleziona ruolo...</option>
                                <?php $selected = (isset($update_user['idRuolo']) && $update_user['idRuolo'] == 0) ? 'selected="true"' : '' ?>
                                <?php if(isset($active_user['id']) && $active_user['id'] == 3){ ?>
                                    <option value="0" <?php echo $selected; ?>>Admin</option>
                                <?php } ?>
                                <?php $selected = (isset($update_user['idRuolo']) && $update_user['idRuolo'] == 1) ? 'selected="true"' : '' ?>
                                <option value="1" <?php echo $selected; ?>>Manager</option>
                                <?php $selected = (isset($update_user['idRuolo']) && $update_user['idRuolo'] == 2) ? 'selected="true"' : '' ?>
                                <option value="2" <?php echo $selected; ?>>Utente/Personale interno</option>
                                <?php $selected = (isset($update_user['idRuolo']) && $update_user['idRuolo'] == 3) ? 'selected="true"' : '' ?>
                                <option value="3" <?php echo $selected; ?>>Azienda/Ente</option>
                                <?php $selected = (isset($update_user['idRuolo']) && $update_user['idRuolo'] == 4) ? 'selected="true"' : '' ?>
                                <option value="4" <?php echo $selected; ?>>Supervisor</option>
                            </select>
                        </div>
                        <div style="margin-top:10%;margin-bottom: 10%;display: flex;justify-content: center;">
                            <div>
                                <div>
                                    <p>Scegli piattaforma</p>
                                </div>
                                <?php foreach ($platforms as $platform_id => $value) { ?>
                                    <div>
                                        <?php
                                            $checkbox_attributes   = array();
                                            $checkbox_attributes[] = 'type="checkbox"';
                                            $checkbox_attributes[] = 'name="platform[]"';
                                            $checkbox_attributes[] = 'value="' . $platform_id . '"';
                                            $checkbox_attributes[] = (in_array($platform_id, $update_user['idPiattaforma'])) ? 'checked="true"' : '';
                                            if ($platform_id == 4) $checkbox_attributes[] = 'disabled="true"';
                                        ?>
                                        <input <?php echo implode(" ", $checkbox_attributes); ?>>
                                        <label for="platform"><?php echo $value['nome']; ?></label>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="d-grid">
                                <button class="btn btn-primary text-uppercase fw-bold" name="action" value="updateUser" id="updateButton" type="submit" onclick="return checkCampi()">Modifica</button>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="d-grid">
                                <button class="btn btn-danger text-uppercase fw-bold" name="action" value="deleteUser" id="deleteButton" type="submit" onclick="return confirm('Eliminare l''utente?')">Elimina</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
<script>

    $("#toggle").click(function () {
        $(this).toggleClass("fa-eye fa-eye-slash");
        var x = document.getElementById("floatingPassword");
        if (x.type === "password") {
            x.type = "text";
        } else {
            x.type = "password";
        }
    });


    function checkCampi() {
        var emText = document.getElementById("floatingInput").value;
        var pwd = document.getElementById("floatingPassword").value;
        var e = document.getElementById("role");
        var role = e.options[e.selectedIndex].value;
        if (emText.length == 0) {
            alert("Inserisci email!");
            return false;
        }
        if (pwd.length > 0 && pwd.length < 6) {
            alert("Inserisci una password con almeno 6 caratteri!");
            return false;
        }
        if (role == -1) {
            alert("Seleziona ruolo!");
            return false;
        }
        var textinputs = document.querySelectorAll('input[type=checkbox]');
        var empty = [].filter.call(textinputs, function (el) {
            return !el.checked
        });

        if (textinputs.length == empty.length) {
            alert("Seleziona almeno una piattaforma");
            return false;
        }

        return true;
    }


    function verifyEmail(email) {
        if (email.length > 4) {
            $.ajax({
                method: 'GET',
                url: "verifyEmail.php",
                data: {
                    email: email
                }
            })
                .done(function (resultParsed) {

                    if (resultParsed !== "no") {
                        document.getElementById("error").style.display = "block";
                        document.getElementById('creaButton').disabled = true;
                    } else {
                        document.getElementById("error").style.display = "none";
                        document.getElementById('creaButton').disabled = false;
                    }
                })
                .fail(function (jqXHR, exception) {
                    var msg = '';
                    if (jqXHR.status === 0) {
                        msg = 'Not connect.\n Verify Network.';
                    } else if (jqXHR.status == 404) {
                        msg = 'Requested page not found. [404]';
                    } else if (jqXHR.status == 500) {
                        msg = 'Internal Server Error [500].';
                    } else if (exception === 'parsererror') {
                        msg = 'Requested JSON parse failed.';
                    } else if (exception === 'timeout') {
                        msg = 'Time out error.';
                    } else if (exception === 'abort') {
                        msg = 'Ajax request aborted.';
                    } else {
                        msg = 'Uncaught Error.\n' + jqXHR.responseText;
                    }
                    console.log(msg);
                });
        }
    }

</script>
</html>
