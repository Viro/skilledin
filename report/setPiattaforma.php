<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

session_start();

$selected=$_POST['platform'];

switch($selected){
    case "Fondo Nuove Competenze":
        $selected="fnc";
        break;
    case "Formazione 4.0":
        $selected="f40";
        break;
    case "New":
        $selected="new";
        break;
    case "Live":
        $selected="live";
        break;
    case "Formazione":
        $selected="formaz";
        break;
}

$_SESSION['user']['piattaforma'] = $selected;
echo "Setting piattaforma completato: ".$selected;
exit();


