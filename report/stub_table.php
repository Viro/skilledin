<?php

session_start();

ini_set('display_errors', 0);
ini_set('display_startup_errors', 1);
ini_set('memory_limit', '-1');
ini_set('max_execution_time', -1);
set_time_limit(0);

error_reporting(E_ALL);
require '../../vendor/autoload.php';

include("obj/Report.php");

//Aggiungo le classi di piattaforma
require_once("obj/SkilledinReport.php");
require_once("obj/Formazione.php");
require_once("obj/Formazione40.php");
require_once("obj/FondoNuoveCompetenze.php");
require_once("obj/Live.php");
require_once("obj/PlatformNew.php");
require_once("obj/Utility.php");

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//var_dump(dirname(__FILE__).'/public/'.slugify("nome file prova".' '.date('Y-m-d H:i:s', strtotime('now'))).'.xls'); exit;
if (isset($_GET['uid'])) {
    $ok = 1;
} else if (!isset($_SESSION['user'])) {
    http_response_code(403);
    exit("Non autorizzato");
}
if (isset($_GET['report'])) {
    $client['Report'] = new Report();

    if (isset($_GET['skip'])){
        $skip = $_GET['skip'];
    }else{
        $skip = 0;
    }

    $corso = $_GET['course'] ?? null;
    $data_inizio = $_GET['start'] ?? '01-04-2021';
    $data_fine = $_GET['end'] ?? '31-04-2021';
    $gruppo = $_GET['enterprise'] ?? 'Tutti';
    $gruppo = urldecode($gruppo);

    $utente = $_GET['utente'] ?? 0;

    $inizio = ($data_inizio == '01-01-1970') ? 0 : strtotime($data_inizio);
    $fine = ($data_fine == '01-01-2999') ? strtotime('now') + 86400 : strtotime($data_fine) + 86400;

    /*
        $collection = new MongoDB\Client(
        "mongodb+srv://jobtek:TimeVision2021%40%21%3F@timevision.t9ed5.mongodb.net/TimeVision?retryWrites=true&w=majority");
    */

    //INIZIO TASK RE1-T3: Ottimizzazione
    require_once('config/connection/MongoDB/skilledin.php');
    $connection_string  = "mongodb+srv://";
    $connection_string .= conn_username . ":" . urlencode(conn_password) . "@" . conn_server;
    $connection_string .= "?retryWrites=true&w=majority";
    $collection = new MongoDB\Client($connection_string);
    //FINE TASK

    if (!isset($_GET['platf'])){
        $db = $_SESSION['user']['piattaforma'];
    }else{
        $db = $_GET['platf'];
    }
    if (!isset($_GET['uid'])){
        $userid = $_SESSION['user']['id'];
    } else{
        $userid = $_GET['uid'];
    }
    $array_id = $client['Report']->getIdUtentiFromAzienda($db, $gruppo);

    //Nuova gestione classi
    $object = false;
    if($db == 'formaz') $object = new Formazione($db);
    if($db == 'f40')    $object = new Formazione40($db);
    if($db == 'fnc')    $object = new FondoNuoveCompetenze($db);
    if($db == 'live')   $object = new Live($db);
    if($db == 'new')    $object = new PlatformNew($db);

    if ($_GET['report'] == "zoom") {
        $finale = array();
        $corso = $_GET['course'];
        $key = 'zoom_' . $gruppo . '_' . $corso . "_" . $inizio . '_' . $fine;
        if (!isset($_SESSION[$key]) || $_SESSION[$key]['time'] <= strtotime("-20 min")) {
            $res = $client['Report']->getAggregateZoom($db, $corso, $inizio, $fine, $gruppo);
            $new = $client['Report']->constructZoomReport($res);
            $_SESSION[$key] = $new;
            $_SESSION[$key]['time'] = time();
        } else {
            $new = $_SESSION[$key];
            unset($new['time']);
        }
        $tot = count($new);
        if(!isset($_GET['excel'])){
            die(json_encode($new) . "*?" . $tot);
        }else{
            if ($tot != 0) {
                $nome = "report_zoom_$gruppo" . "_" . $data_inizio . "_" . $data_fine . ".xlsx";
                $now = date("D, d M Y H:i:s");
                header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
                header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
                header("Last-Modified: {$now} GMT");
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header("Content-Disposition: attachment;filename=$nome");
                header("Content-Transfer-Encoding: binary");

                $spreadsheet = new Spreadsheet();

                $filtri = $_GET['filtri'];
                $letter = 'A';
                $j = 0;

                ///*vedo impostazioni copertina*///

                if (!isset($_GET['uid'])){
                    $userid = $_SESSION['user']['id'];
                }else{
                    $userid = 0;
                }

                $client['Report']->createCopertina($userid, $spreadsheet, $j);

                /*                 * ************************************************************ */

                /**
                 * Vedo impostazioni sheet
                 * */
                $j++;
                $spreadsheet->setActiveSheetIndex($j);
                $index = $client['Report']->createSheet($userid, $spreadsheet, $j);
                $i = $client['Report']->setZoomSheet($index, $spreadsheet, $letter, $filtri, $j, $new);
                $client['Report']->addSettingsExcel($userid, $spreadsheet, $i);

                $writer = new Xlsx($spreadsheet);
                ob_start();
                $writer->save("php://output");
                $xlsData = ob_get_contents();
                ob_end_clean();

                $response = array(
                    'op' => 'ok',
                    'file' => "data:application/vnd.ms-excel;base64," . base64_encode($xlsData)
                );

                die(json_encode($response));
            } else {
                $message = "Nessun log";
                die(json_encode($message));
            }
        }
        exit();
    }
    else if ($_GET['report'] == "s_zoom") {
        $key = 's_zoom_' . $gruppo . '_' . "_" . $inizio . '_' . $fine;
//                                       if (!isset($_SESSION[$key]) || $_SESSION[$key]['time'] <= strtotime("-20 min")) {
        if( 1 == 1 ) {
            $result = $client['Report']->getAggregateZoomSintesi($db, $inizio, $fine, $gruppo);
            $temp = $client['Report']->constructZoomReport($result);
            $totResult = count($temp);
            $res = $client['Report']->constructMinCumulatiReport($temp, $totResult);
            $res = explode("*", $res);
            $new = json_decode($res[0], true);
            $array_mesi = json_decode($res[1], true);
            $_SESSION[$key] = $new;
            $_SESSION[$key]['mesi'] = $array_mesi;
            $_SESSION[$key]['time'] = time();
        } else {
            $new = $_SESSION[$key];
            $array_mesi = $_SESSION[$key]['mesi'];
            unset($new['mesi']);
            unset($new['time']);
        }
        $total = count($new);
        if (!isset($_GET['excel'])) {
            die(json_encode($new) . "*?$total" . "*?" . json_encode($array_mesi));
        } else {
            if ($total != 0) {
                $nome = "report_s_zoom_$gruppo" . "_" . $data_inizio . "_" . $data_fine . ".xlsx";
                $now = date("D, d M Y H:i:s");
                header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
                header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
                header("Last-Modified: {$now} GMT");
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header("Content-Disposition: attachment;filename=$nome");
                header("Content-Transfer-Encoding: binary");
                $spreadsheet = new Spreadsheet();
                $filtri = $_GET['filtri'];
                $letter = "A";
                $j = 0;

                ///*vedo impostazioni copertina*///
                if (!isset($_GET['uid'])){
                    $userid = $_SESSION['user']['id'];
                }else{
                    $userid = 0;
                }
                $client['Report']->createCopertina($userid, $spreadsheet, $j);
                /*                 * ************************************************************ */
                /**
                * vedo impostazioni sheet
                * */
                $j++;
                $spreadsheet->setActiveSheetIndex($j);
                $index = $client['Report']->createSheet($userid, $spreadsheet, $j);
                $i = $client['Report']->setZoomSintesiSheet($index, $spreadsheet, $letter, $filtri, $j, $new, $array_mesi);
                $client['Report']->addSettingsExcel($userid, $spreadsheet, $i);

                $writer = new Xlsx($spreadsheet);
                ob_start();
                $writer->save("php://output");
                $xlsData = ob_get_contents();
                ob_end_clean();

                $response = array(
                    'op' => 'ok',
                    'file' => "data:application/vnd.ms-excel;base64," . base64_encode($xlsData)
                );

                die(json_encode($response));
            } else {
                $message = "Nessun log";
                die(json_encode($message));
            }
        }
    }
    else if ($_GET['report'] == "s_zoomName") {
        $nomeCorso = $_GET['course'];

        $key = 's_zoom_' . $gruppo . '_' . $nomeCorso . "_" . $inizio . '_' . $fine;
//                                        if (!isset($_SESSION[$key]) || $_SESSION[$key]['time'] <= strtotime("-20 min")) {

        if( 1 == 1 ) {
            $result = $client['Report']->getAggregateZoom($db, $nomeCorso, $inizio, $fine, $gruppo);
            $temp = $client['Report']->constructZoomReport($result);
            $totResult = count($temp);
            $res = $client['Report']->constructMinCumulatiReport($temp, $totResult);
            $res = explode("*", $res);
            $new = json_decode($res[0], true);
            $array_mesi = json_decode($res[1], true);
            $_SESSION[$key] = $new;
            $_SESSION[$key]['mesi'] = $array_mesi;
            $_SESSION[$key]['time'] = time();
        } else {
            $new = $_SESSION[$key];
            $array_mesi = $_SESSION[$key]['mesi'];
            unset($new['mesi']);
            unset($new['time']);
        }

        $total = count($new);

        if (!isset($_GET['excel'])) {
            die(json_encode($new) . "*?$total" . "*?" . json_encode($array_mesi));
        } else {

            if ($total != 0) {
                $nome = "report_s_zoom_$gruppo" . "_" . $data_inizio . "_" . $data_fine . ".xlsx";
                $now = date("D, d M Y H:i:s");
                header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
                header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
                header("Last-Modified: {$now} GMT");
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header("Content-Disposition: attachment;filename=$nome");
                header("Content-Transfer-Encoding: binary");
                $spreadsheet = new Spreadsheet();
                $filtri = $_GET['filtri'];
                $letter = "A";
                $j = 0;


                ///*vedo impostazioni copertina*///

                if (!isset($_GET['uid'])){
                    $userid = $_SESSION['user']['id'];
                }else{
                    $userid = 0;
                }

                $client['Report']->createCopertina($userid, $spreadsheet, $j);


                /*                 * ************************************************************ */

                /**
                 * vedo impostazioni sheet
                 * */
                $j++;
                $spreadsheet->setActiveSheetIndex($j);
                $index = $client['Report']->createSheet($userid, $spreadsheet, $j);
                $i = $client['Report']->setZoomSintesiSheet($index, $spreadsheet, $letter, $filtri, $j, $new, $array_mesi);
                $client['Report']->addSettingsExcel($userid, $spreadsheet, $i);

                $writer = new Xlsx($spreadsheet);
                ob_start();
                $writer->save("php://output");
                $xlsData = ob_get_contents();
                ob_end_clean();

                $response = array(
                    'op' => 'ok',
                    'file' => "data:application/vnd.ms-excel;base64," . base64_encode($xlsData)
                );

                die(json_encode($response));
            } else {
                $message = "Nessun log";
                die(json_encode($message));
            }
        }
    }
    else if ($_GET['report'] == "zoomName") {

        $corso = $_GET['course'];
        $key = 'zoomName_' . $gruppo . '_' . $corso . "_" . $inizio . '_' . $fine;
        if (!isset($_SESSION[$key]) || $_SESSION[$key]['time'] <= strtotime("-20 min")) {
            $result = $client['Report']->getAggregateZoom($db, $corso, $inizio, $fine, $gruppo);
            $new = $client['Report']->constructZoomReport($result);
            $_SESSION[$key] = $new;
            $_SESSION[$key]['time'] = time();
        } else {
            $new = $_SESSION[$key];
            unset($new['time']);
        }

        $tot = count($new);

        if (!isset($_GET['excel']))
            die(json_encode($new) . "*?" . $tot);
        else {
            if ($tot != 0) {
                $nome = "report_zoomName_$gruppo" . "_" . $data_inizio . "_" . $data_fine . ".xlsx";
                $now = date("D, d M Y H:i:s");
                header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
                header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
                header("Last-Modified: {$now} GMT");
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header("Content-Disposition: attachment;filename=$nome");
                header("Content-Transfer-Encoding: binary");

                $spreadsheet = new Spreadsheet();

                $filtri = $_GET['filtri'];
                $letter = 'A';
                $j = 0;


                ///*vedo impostazioni copertina*///
                if (!isset($_GET['uid'])){
                    $userid = $_SESSION['user']['id'];
                }else{
                    $userid = 0;
                }
                $client['Report']->createCopertina($userid, $spreadsheet, $j);
                /*                 * ************************************************************ */

                /**
                 * vedo impostazioni sheet
                 * */
                $j++;
                $spreadsheet->setActiveSheetIndex($j);
                $index = $client['Report']->createSheet($userid, $spreadsheet, $j);
                $i = $client['Report']->setZoomSheet($index, $spreadsheet, $letter, $filtri, $j, $new);
                $client['Report']->addSettingsExcel($userid, $spreadsheet, $i);
                $writer = new Xlsx($spreadsheet);
                ob_start();
                $writer->save("php://output");
                $xlsData = ob_get_contents();
                ob_end_clean();

                $response = array(
                    'op' => 'ok',
                    'file' => "data:application/vnd.ms-excel;base64," . base64_encode($xlsData)
                );
                die(json_encode($response));
            } else {
                $message = "Nessun log";
                die(json_encode($message));
            }
        }
        exit();
    }
    else if ($_GET['report'] == "sintesiName") {
        $idUtente = $_GET['enterprise'];
        $key = 'sintesi_' . $idUtente . '_' . $inizio . '_' . $fine;
        if (!isset($_SESSION[$key]) || $_SESSION[$key]['time'] <= strtotime("-20 min")) {

            $temporaneo = $client['Report']->getLogstore($db, $inizio, $fine, $idUtente);
            $temp = $client['Report']->constructLogArray($temporaneo);
            $result = $client['Report']->constructSintesiReport($temp);

            $_SESSION[$key] = $result;
            $_SESSION[$key]['time'] = time();
        } else {
            $result = $_SESSION[$key];
            unset($result['time']);
        }
        $total = count($result);
        if (!isset($_GET['excel'])) {
            die(json_encode($result) . "*?$total");
        } else {
            if ($total != 0) {
                $nome = "report_sintesiLogin_$gruppo" . "_" . $data_inizio . "_" . $data_fine . ".xlsx";
                $now = date("D, d M Y H:i:s");
                header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
                header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
                header("Last-Modified: {$now} GMT");
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header("Content-Disposition: attachment;filename=$nome");
                header("Content-Transfer-Encoding: binary");

                $spreadsheet = new Spreadsheet();
                $filtri = $_GET['filtri'];
                $j = 0;
                $letter = "A";

                ///*vedo impostazioni copertina*///

                if (!isset($_GET['uid'])){
                    $userid = $_SESSION['user']['id'];
                }else{
                    $userid = 0;
                }
                $client['Report']->createCopertina($userid, $spreadsheet, $j);
                /*                 * ************************************************************ */

                /**
                 * vedo impostazioni sheet
                 * */
                $j++;
                $spreadsheet->setActiveSheetIndex($j);
                $index = $client['Report']->createSheet($userid, $spreadsheet, $j);
                $i = $client['Report']->setSintesiSheet($index, $spreadsheet, $letter, $filtri, $j, $result);
                $client['Report']->addSettingsExcel($userid, $spreadsheet, $i);

                $writer = new Xlsx($spreadsheet);
                ob_start();
                $writer->save("php://output");
                $xlsData = ob_get_contents();
                ob_end_clean();

                $response = array(
                    'op' => 'ok',
                    'file' => "data:application/vnd.ms-excel;base64," . base64_encode($xlsData)
                );
                die(json_encode($response));
            } else {
                $message = "Nessun log";
                die(json_encode($message));
            }
        }
    }
    else if ($_GET['report'] == "sintesi") {
        if (isset($_GET['iscritto'])) {
            if (is_array($_GET['iscritto'])) {
                $iscritti = implode(",", $_GET['iscritto']);
            } else {
                $iscritti = (int) $_GET['iscritto'];
            }
            $key = 'sintesi_' . $gruppo . "_" . $iscritti . "_" . $inizio . "_" . $fine;
        } else{
            $key = 'sintesi_' . $gruppo . "_" . $inizio . "_" . $fine;
        }
//                                            if (!isset($_SESSION[$key]) || $_SESSION[$key]['time'] <= strtotime("-20 min")) {
        if (1 == 1) {
            $array_id = array_values($array_id);
//                                                var_dump($array_id); exit;
            if (isset($_GET['iscritto']) && $_GET['iscritto'] != 0 && isset($iscritti) && !is_string($iscritti)) {
                $temporaneo = $client['Report']->getLogstore($db, $inizio, $fine, $_GET['iscritto'], null);
            } else {
                if (isset($iscritti) && is_array($_GET['iscritto'])) {
                    $array_iscritti = array_map('intval', explode(',', $iscritti));
                    $array_id = array_values($array_iscritti);
                }
                $temporaneo = $client['Report']->getLogstore($db, $inizio, $fine, null, $array_id);
            }
//                                                if($_SERVER['REMOTE_ADDR'] == '87.26.93.180'){
//                                                    var_dump($temporaneo); exit;
//                                                }

            $temp = $client['Report']->constructLogArray($temporaneo);
            $result = $client['Report']->constructSintesiReport($temp);

            $_SESSION[$key] = $result;
            $_SESSION[$key]['time'] = time();
        } else {
            $result = $_SESSION[$key];
            unset($result['time']);
        }

        $total = count($result);
        if (!isset($_GET['excel'])) {
            die(json_encode($result) . "*?$total");
        } else {
            if ($total != 0) {
                $nome = "report_sintesiLogin_$gruppo" . "_" . $data_inizio . "_" . $data_fine . ".xlsx";
                $now = date("D, d M Y H:i:s");
                header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
                header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
                header("Last-Modified: {$now} GMT");
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header("Content-Disposition: attachment;filename=$nome");
                header("Content-Transfer-Encoding: binary");

                $spreadsheet = new Spreadsheet();
                $filtri = $_GET['filtri'];
                $j = 0;
                $letter = "A";

                ///*vedo impostazioni copertina*///

                if (!isset($_GET['uid'])){
                    $userid = $_SESSION['user']['id'];
                }else{
                    $userid = 0;
                }
                $client['Report']->createCopertina($userid, $spreadsheet, $j);
                /*                 * ************************************************************ */

                /**
                 * vedo impostazioni sheet
                 * */
                $j++;
                $spreadsheet->setActiveSheetIndex($j);
                $index = $client['Report']->createSheet($userid, $spreadsheet, $j);
                $i = $client['Report']->setSintesiSheet($index, $spreadsheet, $letter, $filtri, $j, $result);
                $client['Report']->addSettingsExcel($userid, $spreadsheet, $i);

                $writer = new Xlsx($spreadsheet);
                ob_start();
                $writer->save("php://output");
                $xlsData = ob_get_contents();
                ob_end_clean();

                $response = array(
                    'op' => 'ok',
                    'file' => "data:application/vnd.ms-excel;base64," . base64_encode($xlsData)
                );

                die(json_encode($response));
            } else {
                $message = "Nessun log";
                die(json_encode($message));
            }
        }
    }
    else if ($_GET['report'] == "timein**") {
        if (isset($_GET['iscritto'])) {
            if (is_array($_GET['iscritto'])) {
                $iscritti = implode(",", $_GET['iscritto']);
            } else {
                $iscritti = (int) $_GET['iscritto'];
            }
            $key = 'timein_' . $gruppo . "_" . $iscritti . "_" . $inizio . "_" . $fine;
        } else{
            $key = 'timein_' . $gruppo . "_" . $inizio . "_" . $fine;
        }
        if (!isset($_SESSION[$key]) || $_SESSION[$key]['time'] <= strtotime("-20 min")) {
            $array_id = array_values($array_id);
            if (isset($_GET['iscritto']) && $_GET['iscritto'] != 0 && isset($iscritti) && !is_string($iscritti)) {
                $temporaneo = $client['Report']->getLogstore($db, $inizio, $fine, $_GET['iscritto'], null);
            } else {
                if (isset($iscritti) && is_array($_GET['iscritto'])) {
                    $array_iscritti = array_map('intval', explode(',', $iscritti));
                    $array_id = array_values($array_iscritti);
                }
                $temporaneo = $client['Report']->getLogstore($db, $inizio, $fine, null, $array_id);
            }
            $temp = $client['Report']->constructLogArray($temporaneo);
            $result = $client['Report']->constructSintesiReport($temp);
            $totResult = count($result);
            $res = $client['Report']->constructMinCumulatiReport($result, $totResult);
            $res = explode("*", $res);
            $finale = json_decode($res[0], true);
            $array_mesi = json_decode($res[1], true);

            $_SESSION[$key] = $finale;
            $_SESSION[$key]['mesi'] = $array_mesi;
            $_SESSION[$key]['time'] = time();
        } else {
            $finale = $_SESSION[$key];
            $array_mesi = $_SESSION[$key]['mesi'];
            unset($finale['mesi']);
            unset($finale['time']);
        }

        $total = count($finale);
        if (!isset($_GET['excel'])) {
            die(json_encode($finale) . "*?$total" . "*?" . json_encode($array_mesi));
        } else {
            if ($total != 0) {
                $nome = "report_tip_$gruppo" . "_" . $data_inizio . "_" . $data_fine . ".xlsx";
                $now = date("D, d M Y H:i:s");
                header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
                header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
                header("Last-Modified: {$now} GMT");
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header("Content-Disposition: attachment;filename=$nome");
                header("Content-Transfer-Encoding: binary");

                $spreadsheet = new Spreadsheet();
                $filtri = $_GET['filtri'];
                $letter = "A";
                $j = 0;

                ///*vedo impostazioni copertina*///
                if (!isset($_GET['uid'])){
                    $userid = $_SESSION['user']['id'];
                }else{
                    $userid = 0;
                }
                $client['Report']->createCopertina($userid, $spreadsheet, $j);
                /*                 * ************************************************************ */

                /**
                 * vedo impostazioni sheet
                 * */
                $j++;
                $spreadsheet->setActiveSheetIndex($j);
                $index = $client['Report']->createSheet($userid, $spreadsheet, $j);
                $i = $client['Report']->setTimeinSheet($index, $spreadsheet, $letter, $filtri, $j, $finale, $array_mesi);
                $client['Report']->addSettingsExcel($userid, $spreadsheet, $i);

                $writer = new Xlsx($spreadsheet);
                ob_start();
                $writer->save("php://output");
                $xlsData = ob_get_contents();
                ob_end_clean();

                $response = array(
                    'op' => 'ok',
                    'file' => "data:application/vnd.ms-excel;base64," . base64_encode($xlsData)
                );

                die(json_encode($response));
            } else {
                $message = "Nessun log";
                die(json_encode($message));
            }
        }
    }
    else if ($_GET['report'] == "timeinAldo") {
        if (isset($_GET['iscritto'])) {
            if (is_array($_GET['iscritto'])) {
                $iscritti = implode(",", $_GET['iscritto']);
            } else {
                $iscritti = (int) $_GET['iscritto'];
            }
            $key = 'timein_' . $gruppo . "_" . $iscritti . "_" . $inizio . "_" . $fine;
        } else{
            $key = 'timein_' . $gruppo . "_" . $inizio . "_" . $fine;
        }
        if (true || !isset($_SESSION[$key]) || $_SESSION[$key]['time'] <= strtotime("-20 min")) {
            $array_id = array_values($array_id);
            if (isset($_GET['iscritto']) && $_GET['iscritto'] != 0 && isset($iscritti) && !is_string($iscritti)) {
                $temporaneo = $client['Report']->getLogstore($db, $inizio, $fine, $_GET['iscritto'], null);
            } else {
                if (isset($iscritti) && is_array($_GET['iscritto'])) {
                    $array_iscritti = array_map('intval', explode(',', $iscritti));
                    $array_id = array_values($array_iscritti);
                }
                //$temporaneo = $client['Report']->getLogstore($db, $inizio, $fine, null, $array_id);

            }
            /* Modifiche Aldo */
            $temporaneo = $client['Report']->getLoginDate($db, $inizio, $fine, null, $array_id);
            $arrDateLogin = array();
            $arrUtenti = array();
            foreach ($temporaneo as $key => $arrCursore) {
                $data = date("d-m-Y", $arrCursore['timecreated']);
                $userId = $arrCursore['userid'];
                $arrDateLogin[$userId][$data]['timecreatedLogin'][] = $arrCursore['timecreated'];
                /* Array che mi serve per creare l'array finale da passare alla tabella */
                $arrUtenti[$userId]['id'] = $arrCursore['id'];
                $arrUtenti[$userId]['userid'] = $arrCursore['userid'];
                $arrUtenti[$userId]['name'] = $arrCursore['nome'];
                $arrUtenti[$userId]['username'] = $arrCursore['username'];
                $arrUtenti[$userId]['email'] = $arrCursore['email'];
                $arrUtenti[$userId]['cf'] = 'n/d';
                $arrUtenti[$userId]['data_nascita'] = date("d-m-Y", $arrCursore['data']);
                $arrUtenti[$userId]['citta'] = $arrCursore['citta'];
                $arrUtenti[$userId]['timecreated'] = $data;
                $arrUtenti[$userId]['timestamp'] = $arrCursore['timecreated'];
                $arrUtenti[$userId]['action'] = $arrCursore['action'];
                $arrUtenti[$userId]['azienda'] = $arrCursore['azienda'];


            }
            foreach ($arrDateLogin as $keyUserid => $dataLogin) {
                foreach ($dataLogin as $keyDataGiorno => $arrTimecreatedLogin) {
                    $arrGiorno = $client['Report']->getTimestampGiorno($keyDataGiorno);
                    $inizioGiorno = $arrGiorno['inizio'];
                    $fineGiorno = $arrGiorno['fine'];
                    $differenza = 0;

                    foreach ($arrTimecreatedLogin['timecreatedLogin'] as $keyLogout => $timestampLogin) {
                        $timestampLogout = $client['Report']->getLogoutDate($db, $timestampLogin, $fineGiorno, $keyUserid);

                        $arrDateLogin[$keyUserid][$keyDataGiorno]['timecreatedLogout'][$keyLogout] = $timestampLogout;
                        $differenza += ($timestampLogout - $timestampLogin);
                        $arrDateLogin[$keyUserid][$keyDataGiorno]['tempo'] = $client['Report']->convertSeconds($differenza);
                        $arrDateLogin[$keyUserid][$keyDataGiorno]['tempoSecondi'] = $differenza;
                    }
                }
            }
            //var_dump($arrDateLogin);exit();
            $arr1 = array();
            $array_mesi = array();
            foreach ($arrUtenti as $keyUtenteId => $arrUtente) {//var_dump($arrDateLogin[$keyUtenteId]);exit();
                $arrMesi = array();
                $tempoTotale = 0;
                foreach ($arrDateLogin[$keyUtenteId] as $keyDataGiorno => $arrTimecreatedLogin) {
                    $arrMesi[$keyDataGiorno] = $arrDateLogin[$keyUtenteId][$keyDataGiorno]['tempo'];
                    $tempoTotale += $arrDateLogin[$keyUtenteId][$keyDataGiorno]['tempoSecondi'];
                    $array_mesi[$keyDataGiorno] = $keyDataGiorno;
                }
                $arrUtenti[$keyUtenteId]['mesi'] = $arrMesi;
                $arrUtenti[$keyUtenteId]['tempo_tot'] = $client['Report']->convertSeconds($tempoTotale);
                $arr1[] = $arrUtenti[$keyUtenteId];
            }
            $totResult = count($arr1);
            $finale = $arr1;
            var_dump($arr1);exit();
            /* /Modifiche Aldo */
            /* CODICE SKILLEDIN
                                                $temp = $client['Report']->constructLogArray($temporaneo);
                                                $result = $client['Report']->constructSintesiReport($temp);

                                                 $totResult = count($result);

                                                 $res = $client['Report']->constructMinCumulatiReport($result, $totResult);
                                                 $res = explode("*", $res);
                                                 $finale = json_decode($res[0], true);
                                                 $array_mesi = json_decode($res[1], true);
                                                 /* CODICE SKILLEDIN */

                                                /* lOG STANDARD */
            $bbbName = $client['Report']->getNameAttivitaBBB($db);
            $hvpName = $client['Report']->getNameAttivitaHVP($db);
            $nameCorsi = $client['Report']->getAllNameCourse($db);
            $finaleLogStandard = $client['Report']->constructTotalLogArray($temporaneo, $bbbName, $hvpName, $nameCorsi);

            /* /lOG STANDARD */

            $_SESSION[$key] = $finale;
            $_SESSION[$key]['mesi'] = $array_mesi;
            $_SESSION[$key]['time'] = time();
        } else {
            $finale = $_SESSION[$key];
            $array_mesi = $_SESSION[$key]['mesi'];
            unset($finale['mesi']);
            unset($finale['time']);
        }
        $total = count($finale);
        if (!isset($_GET['excel'])) {
            die(json_encode($finale) . "*?$total" . "*?" . json_encode($array_mesi));
        } else {
            if ($total != 0) {
                $nome = "report_tip_$gruppo" . "_" . $data_inizio . "_" . $data_fine . ".xlsx";
                $now = date("D, d M Y H:i:s");
                header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
                header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
                header("Last-Modified: {$now} GMT");
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header("Content-Disposition: attachment;filename=$nome");
                header("Content-Transfer-Encoding: binary");

                $spreadsheet = new Spreadsheet();
                $filtri = $_GET['filtri'];
                $letter = "A";
                $j = 0;
                ///*vedo impostazioni copertina*///

                if (!isset($_GET['uid'])){
                    $userid = $_SESSION['user']['id'];
                }else{
                    $userid = 0;
                }
                $client['Report']->createCopertina($userid, $spreadsheet, $j);
                /*                 * ************************************************************ */
                /**
                 * vedo impostazioni sheet
                 * */
                $j++;
                $spreadsheet->setActiveSheetIndex($j);
                $index = $client['Report']->createSheet($userid, $spreadsheet, $j);
                $i = $client['Report']->setTimeinSheet($index, $spreadsheet, $letter, $filtri, $j, $finale, $array_mesi);
                $spreadsheet->createSheet();
                $SheetCount = $spreadsheet->getSheetCount();
                $j = ($SheetCount - 1);
                //var_dump($SheetCount);exit();
                $spreadsheet->setActiveSheetIndex($j);
                //$index = $client['Report']->createSheet($userid, $spreadsheet, $j);
                // $index++;
                $i = $client['Report']->setSintesiSheet($index, $spreadsheet, $letter, $filtri, $j, $result);

                /*Log Standard */
                $spreadsheet->createSheet();
                $SheetCount = $spreadsheet->getSheetCount();
                $j = ($SheetCount - 1);
                //var_dump($SheetCount);exit();
                $spreadsheet->setActiveSheetIndex($j);
                //$index = $client['Report']->createSheet($userid, $spreadsheet, $j);
                // $index++;
                $i = $client['Report']->setTotalLogSheet($index, $spreadsheet, $letter, $filtri, $j, $finaleLogStandard);
                /* /Log Standard */
                $client['Report']->addSettingsExcel($userid, $spreadsheet, $i);
                $writer = new Xlsx($spreadsheet);
                ob_start();
                $writer->save("php://output");
                $xlsData = ob_get_contents();
                ob_end_clean();

                $response = array(
                    'op' => 'ok',
                    'file' => "data:application/vnd.ms-excel;base64," . base64_encode($xlsData)
                );

                die(json_encode($response));
            } else {
                $message = "Nessun log";
                die(json_encode($message));
            }
        }
                                            //modifiche aldo da controllare
//                                    } else if ($_GET['report'] == "timein") {
//
//                                        if (isset($_GET['iscritto'])) {
//                                            if (is_array($_GET['iscritto'])) {
//                                                $iscritti = implode(",", $_GET['iscritto']);
//                                            } else {
//                                                $iscritti = (int) $_GET['iscritto'];
//                                            }
//
//                                            $key = 'timein_' . $gruppo . "_" . $iscritti . "_" . $inizio . "_" . $fine;
//                                        } else
//                                            $key = 'timein_' . $gruppo . "_" . $inizio . "_" . $fine;
//
////                                            if (true || !isset($_SESSION[$key]) || $_SESSION[$key]['time'] <= strtotime("-20 min")) {
//                                            if (1 == 1) {
//                                                $array_id = array_values($array_id);
//                                                if (isset($_GET['iscritto']) && $_GET['iscritto'] != 0 && isset($iscritti) && !is_string($iscritti)) {
//                                                    $temporaneo = $client['Report']->getLogstore($db, $inizio, $fine, $_GET['iscritto'], null);
//                                                } else {
//                                                    if (isset($iscritti) && is_array($_GET['iscritto'])) {
//                                                        $array_iscritti = array_map('intval', explode(',', $iscritti));
//                                                        $array_id = array_values($array_iscritti);
//                                                    }
//
//                                                    $temporaneo = $client['Report']->getLogstore($db, $inizio, $fine, null, $array_id);
//                                                }
//
//
//
//                                                /* Modifiche Aldo */
//                                                $temporaneo2 = $client['Report']->getLoginDate($db, $inizio, $fine, null, $array_id);
//
//                                                $arrDateLogin = array();
//                                                $arrUtenti = array();
//                                                foreach ($temporaneo2 as $keyC => $arrCursore) {
//                                                    $data = date("d-m-Y", $arrCursore['timecreated']);
//                                                    $userId = $arrCursore['userid'];
//                                                    $arrDateLogin[$userId][$data]['timecreatedLogin'][] = $arrCursore['timecreated'];
//                                                    /* Array che mi serve per creare l'array finale da passare alla tabella */
//                                                    $arrUtenti[$userId]['id'] = $arrCursore['id'];
//                                                    $arrUtenti[$userId]['userid'] = $arrCursore['userid'];
//                                                    $arrUtenti[$userId]['name'] = $arrCursore['nome'];
//                                                    $arrUtenti[$userId]['username'] = $arrCursore['username'];
//                                                    $arrUtenti[$userId]['email'] = $arrCursore['email'];
//                                                    $arrUtenti[$userId]['cf'] = 'n/d';
//                                                    $arrUtenti[$userId]['data_nascita'] = date("d/m/Y", $arrCursore['data']);
//                                                    $arrUtenti[$userId]['citta'] = $arrCursore['citta'];
//                                                    $arrUtenti[$userId]['timecreated'] = $data;
//                                                    $arrUtenti[$userId]['timestamp'] = $arrCursore['timecreated'];
//                                                    $arrUtenti[$userId]['action'] = $arrCursore['action'];
//                                                    $arrUtenti[$userId]['azienda'] = $arrCursore['azienda'];
//
//
//                                                }
//
//
//                                                foreach ($arrDateLogin as $keyUserid => $dataLogin) {
//                                                    foreach ($dataLogin as $keyDataGiorno => $arrTimecreatedLogin) {
//
//                                                        $arrGiorno = $client['Report']->getTimestampGiorno($keyDataGiorno);
//                                                        $inizioGiorno = $arrGiorno['inizio'];
//                                                        $fineGiorno = $arrGiorno['fine'];
//
//                                                        $differenza = 0;
//
//                                                        foreach ($arrTimecreatedLogin['timecreatedLogin'] as $keyLogout => $timestampLogin) {
//
//                                                            $timestampLogout = $client['Report']->getLogoutDate($db, $timestampLogin, $fineGiorno, $keyUserid);
//
//                                                            $arrDateLogin[$keyUserid][$keyDataGiorno]['timecreatedLogout'][$keyLogout] = $timestampLogout;
//                                                            $differenza += ($timestampLogout - $timestampLogin);
//                                                            $arrDateLogin[$keyUserid][$keyDataGiorno]['tempo'] = $client['Report']->convertSeconds($differenza);
//                                                            $arrDateLogin[$keyUserid][$keyDataGiorno]['tempoSecondi'] = $differenza;
//
//                                                        }
//
//
//
//                                                    }
//                                                }
//                                                //var_dump($arrDateLogin);exit();
//                                                $arr1 = array();
//                                                $array_mesi = array();
//                                                foreach ($arrUtenti as $keyUtenteId => $arrUtente) {//var_dump($arrDateLogin[$keyUtenteId]);exit();
//                                                    $arrMesi = array();
//                                                    $tempoTotale = 0;
//                                                    foreach ($arrDateLogin[$keyUtenteId] as $keyDataGiorno => $arrTimecreatedLogin) {
//
//                                                        $newkeyDataGiorno = str_replace('-', '/', $keyDataGiorno);
//                                                        $arrMesi[$newkeyDataGiorno] = $arrDateLogin[$keyUtenteId][$keyDataGiorno]['tempo'];
//                                                        $tempoTotale += $arrDateLogin[$keyUtenteId][$keyDataGiorno]['tempoSecondi'];
//                                                        $array_mesi[$keyDataGiorno] = $keyDataGiorno;
//                                                    }
//
//                                                    $arrUtenti[$keyUtenteId]['mesi'] = $arrMesi;
//                                                    $arrUtenti[$keyUtenteId]['tempo_tot'] = $client['Report']->convertSeconds($tempoTotale);
//                                                    $arr1[] = $arrUtenti[$keyUtenteId];
//
//                                                }
//                                                //var_dump($arr1);exit('adada');
//
//                                                /* Se c'� un giorno in array_mesi che non � presente nell'array mesi di arrUtenti ( poich� in quel giorno non ha fatto login ) in posto il tempo a 00:00:00
//                                                 * e formatto le date con lo shash */
//                                                foreach ($array_mesi as $valgiorno) {
//
//                                                    $valgiorno = str_replace('-', '/', $valgiorno);
//
//                                                    foreach ($arr1 as $keyArr2 => $arr) {
//                                                        if (!array_key_exists($valgiorno, $arr['mesi'])) {
//                                                            $arr['mesi'][$valgiorno] = '00:00:00';
//                                                            //echo $valgiorno.' -> '.var_dump($arr['mesi']);exit();
//                                                        }
//                                                        ksort($arr['mesi']);
//                                                        $arr1[$keyArr2]['mesi'] = $arr['mesi'];
//                                                    }
//                                                    $newArrayMesi[] = $valgiorno;
//                                                }
//
//                                                $array_mesi = $newArrayMesi;
////                                                sort($array_mesi);
//                                                usort($array_mesi, "compareByTimeStamp");
//
//                                                $totResult = count($arr1);
//                                                $finale = $arr1;
//                                                //var_dump($finale);exit();
//                                                /* /Modifiche Aldo */
//
//
//                                                /* CODICE SKILLEDIN
//                                                 $temp = $client['Report']->constructLogArray($temporaneo);
//                                                 $result = $client['Report']->constructSintesiReport($temp);
//
//
//                                                 $totResult = count($result);
//
//                                                 $res = $client['Report']->constructMinCumulatiReport($result, $totResult);
//                                                 $res = explode("*", $res);
//                                                 $finale = json_decode($res[0], true);
//
//
//                                                 $array_mesi = json_decode($res[1], true);
//
//                                                 /* CODICE SKILLEDIN */
//                                                //var_dump($array_mesi);exit();
//
//                                                /* lOG STANDARD */
//                                                $bbbName = $client['Report']->getNameAttivitaBBB($db);
//                                                $hvpName = $client['Report']->getNameAttivitaHVP($db);
//                                                $nameCorsi = $client['Report']->getAllNameCourse($db);
//                                                $finaleLogStandard = $client['Report']->constructTotalLogArray($temporaneo, $bbbName, $hvpName, $nameCorsi);
//
//                                                /* /lOG STANDARD */
//
//                                                $_SESSION[$key] = $finale;
//                                                $_SESSION[$key]['mesi'] = $array_mesi;
//                                                $_SESSION[$key]['time'] = time();
//                                            } else {
//                                                $finale = $_SESSION[$key];
//                                                $array_mesi = $_SESSION[$key]['mesi'];
//                                                unset($finale['mesi']);
//                                                unset($finale['time']);
//                                            }
//
//                                            $total = count($finale);
//                                            if (!isset($_GET['excel'])) {
//                                                die(json_encode($finale) . "*?$total" . "*?" . json_encode($array_mesi));
//                                            } else {
//                                                if ($total != 0) {
//                                                    $nome = "report_tip_$gruppo" . "_" . $data_inizio . "_" . $data_fine . ".xlsx";
//                                                    $now = date("D, d M Y H:i:s");
//                                                    header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
//                                                    header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
//                                                    header("Last-Modified: {$now} GMT");
//                                                    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
//                                                    header("Content-Disposition: attachment;filename=$nome");
//                                                    header("Content-Transfer-Encoding: binary");
//
//                                                    $spreadsheet = new Spreadsheet();
//                                                    $filtri = $_GET['filtri'];
//                                                    $letter = "A";
//                                                    $j = 0;
//
//
//                                                    ///*vedo impostazioni copertina*///
//
//                                                    if (!isset($_GET['uid']))
//                                                        $userid = $_SESSION['user']['id'];
//                                                        else
//                                                            $userid = 0;
//                                                            $client['Report']->createCopertina($userid, $spreadsheet, $j);
//
//
//                                                            /*                 * ************************************************************ */
//
//                                                            /**
//                                                             * vedo impostazioni sheet
//                                                             * */
//                                                            $j++;
//                                                            $spreadsheet->setActiveSheetIndex($j);
//                                                            $index = $client['Report']->createSheet($userid, $spreadsheet, $j);
//                                                            $i = $client['Report']->setTimeinSheet($index, $spreadsheet, $letter, $filtri, $j, $finale, $array_mesi);
//
//
//
//
//                                                            $spreadsheet->createSheet();
//                                                            $SheetCount = $spreadsheet->getSheetCount();
//                                                            $j = ($SheetCount - 1);
//                                                            //var_dump($SheetCount);exit();
//                                                            $spreadsheet->setActiveSheetIndex($j);
//                                                            //$index = $client['Report']->createSheet($userid, $spreadsheet, $j);
//                                                            // $index++;
//                                                            $i = $client['Report']->setSintesiSheet($index, $spreadsheet, $letter, $filtri, $j, $result);
//
//
//
//
//                                                            /*Log Standard */
//                                                            $spreadsheet->createSheet();
//                                                            $SheetCount = $spreadsheet->getSheetCount();
//                                                            $j = ($SheetCount - 1);
//                                                            //var_dump($SheetCount);exit();
//                                                            $spreadsheet->setActiveSheetIndex($j);
//                                                            //$index = $client['Report']->createSheet($userid, $spreadsheet, $j);
//                                                            // $index++;
//                                                            $i = $client['Report']->setTotalLogSheet($index, $spreadsheet, $letter, $filtri, $j, $finaleLogStandard);
//                                                            /* /Log Standard */
//
//
//
//
//
//                                                            $client['Report']->addSettingsExcel($userid, $spreadsheet, $i);
//
//
//                                                            $writer = new Xlsx($spreadsheet);
//                                                            ob_start();
//                                                            $writer->save("php://output");
//                                                            $xlsData = ob_get_contents();
//                                                            ob_end_clean();
//
//                                                            $response = array(
//                                                                'op' => 'ok',
//                                                                'file' => "data:application/vnd.ms-excel;base64," . base64_encode($xlsData)
//                                                            );
//
//                                                            die(json_encode($response));
//                                                } else {
//                                                    $message = "Nessun log";
//                                                    die(json_encode($message));
//                                                }
//                                            }
//                                    }
    }
    else if ($_GET['report'] == "timein") {
        if (isset($_GET['iscritto'])) {
            if (is_array($_GET['iscritto'])) {
                $iscritti = implode(",", $_GET['iscritto']);
            } else {
                $iscritti = (int) $_GET['iscritto'];
            }
            $key = 'timein_' . $gruppo . "_" . $iscritti . "_" . $inizio . "_" . $fine;
        } else {
            $key = 'timein_' . $gruppo . "_" . $inizio . "_" . $fine;
        }
//                                        if (true || !isset($_SESSION[$key]) || $_SESSION[$key]['time'] <= strtotime("-20 min")) {
        if(1 == 1) {
            $array_id = array_values($array_id);
            if (isset($_GET['iscritto']) && $_GET['iscritto'] != 0 && isset($iscritti) && !is_string($iscritti)) {
//                                               $temporaneo = $client['Report']->getLogstore($db, $inizio, $fine, $_GET['iscritto'], null);
                $temporaneo = $client['Report']->getLogstoreSintesi($db, $inizio, $fine, $_GET['iscritto'], null);
            } else {
                if (isset($iscritti) && is_array($_GET['iscritto'])) {
                    $array_iscritti = array_map('intval', explode(',', $iscritti));
                    $array_id = array_values($array_iscritti);
                }
//                                               $temporaneo = $client['Report']->getLogstore($db, $inizio, $fine, null, $array_id);
                $temporaneo = $client['Report']->getLogstoreSintesi($db, $inizio, $fine, null, $array_id);
            }
//                                            var_dump($temporaneo); exit;
            $temp = $client['Report']->constructLogArray($temporaneo);
//                                            $result = $client['Report']->constructSintesiReport($temp); //qui
                                            //$result = $client['Report']->constructSintesiReportEnzo($temp, $db); //nuovo
            $result = $client['Report']->constructSintesiReportPietro($temp, $db); //nuovo
//                                            var_dump($result); exit;
            $totResult = count($result);
            $res = $client['Report']->constructMinCumulatiReport($result, $totResult);
            $res = explode("*", $res);
            $finale = json_decode($res[0], true);
            $array_mesi = json_decode($res[1], true);

//                                            /* lOG STANDARD */
//                                            $bbbName = $client['Report']->getNameAttivitaBBB($db);
//                                            $hvpName = $client['Report']->getNameAttivitaHVP($db);
//                                            $nameCorsi = $client['Report']->getAllNameCourse($db);
//                                            $finaleLogStandard = $client['Report']->constructTotalLogArray($temporaneo, $bbbName, $hvpName, $nameCorsi);
//
//                                            /* /lOG STANDARD */

            $_SESSION[$key] = $finale;
            $_SESSION[$key]['mesi'] = $array_mesi;
            $_SESSION[$key]['time'] = time();
        } else {
            $finale = $_SESSION[$key];
            $array_mesi = $_SESSION[$key]['mesi'];
            unset($finale['mesi']);
            unset($finale['time']);
        }
        $total = count($finale);
        if (!isset($_GET['excel'])) {
            die(json_encode($finale) . "*?$total" . "*?" . json_encode($array_mesi));
        } else {
            if ($total != 0) {
                $nome = "report_tip_$gruppo" . "_" . $data_inizio . "_" . $data_fine . ".xlsx";
                $now = date("D, d M Y H:i:s");
                header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
                header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
                header("Last-Modified: {$now} GMT");
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header("Content-Disposition: attachment;filename=$nome");
                header("Content-Transfer-Encoding: binary");

                $spreadsheet = new Spreadsheet();
                $filtri = $_GET['filtri'];
                $letter = "A";
                $j = 0;
                                                ///*vedo impostazioni copertina*///

                if (!isset($_GET['uid'])){
                    $userid = $_SESSION['user']['id'];
                }else{
                    $userid = 0;
                }
                $client['Report']->createCopertina($userid, $spreadsheet, $j);
                /*                 * ************************************************************ */

                /**
                 * vedo impostazioni sheet
                 * */
                $j++;
                $spreadsheet->setActiveSheetIndex($j);
                $index = $client['Report']->createSheet($userid, $spreadsheet, $j);
                $i = $client['Report']->setTimeinSheet($index, $spreadsheet, $letter, $filtri, $j, $finale, $array_mesi);

                $spreadsheet->createSheet();
                $SheetCount = $spreadsheet->getSheetCount();
                $j = ($SheetCount - 1);
                //var_dump($SheetCount);exit();
                $spreadsheet->setActiveSheetIndex($j);
                //$index = $client['Report']->createSheet($userid, $spreadsheet, $j);
                // $index++;
                $i = $client['Report']->setSintesiSheet($index, $spreadsheet, $letter, $filtri, $j, $result);

                /*Log Standard */
                $spreadsheet->createSheet();
                $SheetCount = $spreadsheet->getSheetCount();
                $j = ($SheetCount - 1);
                //var_dump($SheetCount);exit();
                $spreadsheet->setActiveSheetIndex($j);
                //$index = $client['Report']->createSheet($userid, $spreadsheet, $j);
                // $index++;
                //$i = $client['Report']->setTotalLogSheet($index, $spreadsheet, $letter, $filtri, $j, $finaleLogStandard);
                $i = $client['Report']->setTotalLogSheet($index, $spreadsheet, $letter, $filtri, $j, $finale);
                /* /Log Standard */

                $client['Report']->addSettingsExcel($userid, $spreadsheet, $i);
                $writer = new Xlsx($spreadsheet);
                ob_start();
                $writer->save("php://output");
                $xlsData = ob_get_contents();
                ob_end_clean();

                $response = array(
                    'op' => 'ok',
                    'file' => "data:application/vnd.ms-excel;base64," . base64_encode($xlsData)
                );

                die(json_encode($response));
            } else {
                $message = "Nessun log";
                die(json_encode($message));
            }
        }
    }
    else if ($_GET['report'] == "timeinName") {
        $idCourse = $_GET['course'];
        $key = 'timein_' . $idCourse . '_' . $gruppo . "_" . $inizio . '_' . $fine;
        if(1 == 1){
//                                       if (!isset($_SESSION[$key]) || $_SESSION[$key]['time'] <= strtotime("-20 min")) {
//                                            $array_id = $client['Report']->getIdUtentiFromAzienda($db, $gruppo); //enzo
//                                            $array_id = array_values($array_id);
//                                            var_dump($array_id); exit;
            $temporaneo = $client['Report']->getLogstoreNew($db, $inizio, $fine, null, $array_id,$idCourse);
//                                           $temporaneo = $client['Report']->getLogstore($db, $inizio, $fine, null, $array_id);
            $temp = $client['Report']->constructLogArray($temporaneo);
//                                            foreach($temp as $t){
//                                                if($t['userid'] == 2165){
//                                                    var_dump($t);
//                                                }
//                                            }
            $result = $client['Report']->constructSintesiReport($temp);
            if(isset($_GET['debug'])){
                print_r($result);
                exit();
            }
            $totResult = count($result);
            $res = $client['Report']->constructMinCumulatiReport($result, $totResult);
            $res = explode("*", $res);
            $finale = json_decode($res[0], true);
            $array_mesi = json_decode($res[1], true);

            $_SESSION[$key] = $finale;
            $_SESSION[$key]['mesi'] = $array_mesi;
            $_SESSION[$key]['time'] = time();
        }else {
            $finale = $_SESSION[$key];
            $array_mesi = $_SESSION[$key]['mesi'];
            unset($finale['mesi']);
            unset($finale['time']);
        }
        $total = count($finale);
        if (!isset($_GET['excel'])) {
            die(json_encode($finale) . "*?$total" . "*?" . json_encode($array_mesi));
        } else {
            if ($total != 0) {
                $nome = "report_tip_$gruppo" . "_" . $data_inizio . "_" . $data_fine . ".xlsx";
                $now = date("D, d M Y H:i:s");
                header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
                header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
                header("Last-Modified: {$now} GMT");
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header("Content-Disposition: attachment;filename=$nome");
                header("Content-Transfer-Encoding: binary");

                $spreadsheet = new Spreadsheet();
                $filtri = $_GET['filtri'];
                $letter = "A";
                $j = 0;
                ///*vedo impostazioni copertina*///

                if (!isset($_GET['uid'])){
                    $userid = $_SESSION['user']['id'];
                }else{
                    $userid = 0;
                }
                $client['Report']->createCopertina($userid, $spreadsheet, $j);
                /*                 * ************************************************************ */

                /**
                 * vedo impostazioni sheet
                 * */
                $j++;
                $spreadsheet->setActiveSheetIndex($j);
                $index = $client['Report']->createSheet($userid, $spreadsheet, $j);
                $i = $client['Report']->setTimeinSheet($index, $spreadsheet, $letter, $filtri, $j, $finale, $array_mesi);
                $client['Report']->addSettingsExcel($userid, $spreadsheet, $i);
                $writer = new Xlsx($spreadsheet);
                ob_start();
                $writer->save("php://output");
                $xlsData = ob_get_contents();
                ob_end_clean();

                $response = array(
                    'op' => 'ok',
                    'file' => "data:application/vnd.ms-excel;base64," . base64_encode($xlsData)
                );

                die(json_encode($response));
            } else {
                $message = "Nessun log";
                die(json_encode($message));
            }
        }
    }
    else if ($_GET['report'] == "totalName") {
        $userid = $_GET['enterprise'];

        $key = 'total_' . $userid . '_' . $inizio . '_' . $fine;
        if (!isset($_SESSION[$key]) || $_SESSION[$key]['time'] <= strtotime("-20 min")) {
            $bbbName = $client['Report']->getNameAttivitaBBB($db);
            $hvpName = $client['Report']->getNameAttivitaHVP($db);
            $nameCorsi = $client['Report']->getAllNameCourse($db);

            $result = $client['Report']->getLogstore($db, $inizio, $fine, $userid);

            $finale = $client['Report']->constructTotalLogArray($result, $bbbName, $hvpName, $nameCorsi);


            $_SESSION[$key] = $finale;
            $_SESSION[$key]['time'] = time();
        } else {
            $finale = $_SESSION[$key];
            unset($finale['time']);
        }
        $cont = count($finale);
        if (!isset($_GET['excel'])) {
            die(json_encode($finale) . "*?$cont");
        } else {
            if ($cont != 0) {
                $nome = "report_logstore_$userid" . "_" . $data_inizio . "_" . $data_fine . ".xlsx";
                $now = date("D, d M Y H:i:s");
                header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
                header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
                header("Last-Modified: {$now} GMT");
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header("Content-Disposition: attachment;filename=$nome");
                header("Content-Transfer-Encoding: binary");

                $spreadsheet = new Spreadsheet();
                $filtri = $_GET['filtri'];
                $letter = "A";
                $j = 0;

                ///*vedo impostazioni copertina*///
                if (!isset($_GET['uid'])){
                    $userid = $_SESSION['user']['id'];
                }else{
                    $userid = 0;
                }
                $client['Report']->createCopertina($userid, $spreadsheet, $j);
                /*                 * ************************************************************ */

                /**
                 * vedo impostazioni sheet
                 * */
                $j++;
                $spreadsheet->setActiveSheetIndex($j);
                $index = $client['Report']->createSheet($userid, $spreadsheet, $j);
                $i = $client['Report']->setTotalLogSheet($index, $spreadsheet, $letter, $filtri, $j, $finale);
                $client['Report']->addSettingsExcel($userid, $spreadsheet, $i);

                $writer = new Xlsx($spreadsheet);
                ob_start();
                $writer->save("php://output");
                $xlsData = ob_get_contents();
                ob_end_clean();

                $response = array(
                    'op' => 'ok',
                    'file' => "data:application/vnd.ms-excel;base64," . base64_encode($xlsData)
                );

                die(json_encode($response));
            } else {
                $message = "Nessun log";
                die(json_encode($message));
            }
        }
    }
    else if ($_GET['report'] == "total") {
        $key = 'total_' . $gruppo . '_' . $inizio . '_' . $fine;
        if (!isset($_SESSION[$key]) || $_SESSION[$key]['time'] <= strtotime("-20 min")) {
            $bbbName = $client['Report']->getNameAttivitaBBB($db);
            $hvpName = $client['Report']->getNameAttivitaHVP($db);
            $nameCorsi = $client['Report']->getAllNameCourse($db);
            $array_id = array_diff($array_id, [2]);
            $array_id = array_values($array_id);

            $result = $client['Report']->getLogstore($db, $inizio, $fine, null, $array_id);
            $finale = $client['Report']->constructTotalLogArray($result, $bbbName, $hvpName, $nameCorsi);

            $_SESSION[$key] = $finale;
            $_SESSION[$key]['time'] = time();
        } else {
            $finale = $_SESSION[$key];
            unset($finale['time']);
        }

        $cont = count($finale);
        if (!isset($_GET['excel'])) {
            echo json_encode($finale) . "*?$cont";
            exit();
        } else {
            if ($cont != 0) {
                $nome = "report_logstore_$gruppo" . "_" . $data_inizio . "_" . $data_fine . ".xlsx";
                $now = date("D, d M Y H:i:s");
                header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
                header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
                header("Last-Modified: {$now} GMT");
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header("Content-Disposition: attachment;filename=$nome");
                header("Content-Transfer-Encoding: binary");

                $spreadsheet = new Spreadsheet();
                $filtri = $_GET['filtri'];
                $letter = "A";
                $j = 0;
                                                ///*vedo impostazioni copertina*///
                if (!isset($_GET['uid'])){
                    $userid = $_SESSION['user']['id'];
                }else{
                    $userid = 0;
                }

                $client['Report']->createCopertina($userid, $spreadsheet, $j);
                /*                 * ************************************************************ */
                /**
                 * vedo impostazioni sheet
                 * */
                $j++;
                $spreadsheet->setActiveSheetIndex($j);
                $index = $client['Report']->createSheet($userid, $spreadsheet, $j);
                $i = $client['Report']->setTotalLogSheet($index, $spreadsheet, $letter, $filtri, $j, $finale);
                $client['Report']->addSettingsExcel($userid, $spreadsheet, $i);

                $writer = new Xlsx($spreadsheet);
                ob_start();
                $writer->save("php://output");
                $xlsData = ob_get_contents();
                ob_end_clean();

                $response = array(
                    'op' => 'ok',
                    'file' => "data:application/vnd.ms-excel;base64," . base64_encode($xlsData)
                );

                die(json_encode($response));
            } else {
                $message = "Nessun log";
                die(json_encode($message));
            }
        }
    }
    else if ($_GET['report'] == "hvp") {
        $valoreCorso = $_GET['course'];
        $key = 'hvp' . $gruppo . '_' . $valoreCorso . "_" . $inizio . '_' . $fine;
//                                        if (!isset($_SESSION[$key]) || $_SESSION[$key]['time'] <= strtotime("-20 min")) {
        if(1 == 1) {
            $hvpName   = $client['Report']->getNameAttivitaHVP($db);
            $nameCorsi = $client['Report']->getAllNameCourse($db);
            $array_id  = array_values($array_id);
            $res       = $client['Report']->getAggregateHvp($db, $inizio, $fine, $valoreCorso, $array_id);
            $result    = $client['Report']->constructHvpArray($res, $hvpName, $nameCorsi);
            if (count($result) == 0) {
                echo json_encode($result) . "*?" . count($result);
                exit();
            }
            //$new = $client['Report']->constructHvpReport($result, $db);
            $new                    = $client['Report']->constructHvpReportPietro($result, $db);
            $_SESSION[$key]         = $new;
            $_SESSION[$key]['time'] = time();
        } else {
            $new = $_SESSION[$key];
            unset($new['time']);
        }

        $cont = count($new);
        if (!isset($_GET['excel'])) {
            echo json_encode($new) . "*?$cont";
            exit();
        } else {
            if ($cont != 0) {
                $nome = "report_hvplog_$gruppo" . "_" . $data_inizio . "_" . $data_fine . ".xlsx";
                $now = date("D, d M Y H:i:s");
                header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
                header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
                header("Last-Modified: {$now} GMT");
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header("Content-Disposition: attachment;filename=$nome");
                header("Content-Transfer-Encoding: binary");

                $spreadsheet = new Spreadsheet();
                $filtri = $_GET['filtri'];
                $letter = 'A';
                $j = 0;

                ///*vedo impostazioni copertina*///
                if (!isset($_GET['uid'])){
                    $userid = $_SESSION['user']['id'];
                }else{
                    $userid = 0;
                }

                $client['Report']->createCopertina($userid, $spreadsheet, $j);

                /*                 * ************************************************************ */

                /**
                 * vedo impostazioni sheet
                 * */
                $j++;
                $spreadsheet->setActiveSheetIndex($j);
                $index = $client['Report']->createSheet($userid, $spreadsheet, $j);
                $i = $client['Report']->setHvpSheet($index, $spreadsheet, $letter, $filtri, $j, $new);
                $client['Report']->addSettingsExcel($userid, $spreadsheet, $i);

                $writer = new Xlsx($spreadsheet);
                ob_start();
                $writer->save("php://output");
                $xlsData = ob_get_contents();
                ob_end_clean();

                $response = array(
                    'op' => 'ok',
                    'file' => "data:application/vnd.ms-excel;base64," . base64_encode($xlsData)
                );

                die(json_encode($response));
            } else {
                $message = "Nessun log";
                die(json_encode($message));
            }
        }
    }
    else if ($_GET['report'] == "summaryhvp") {
        $valoreCorso = $_GET['course'];
        $key         = 'summaryhvp' . $gruppo . '_' . $valoreCorso . "_" . $inizio . '_' . $fine;
        $hvpName     = $client['Report']->getNameAttivitaHVP($db);
        $nameCorsi   = $client['Report']->getAllNameCourse($db);
        $array_id    = array_values($array_id);
        $res         = $client['Report']->getAggregateHvp($db, $inizio, $fine, $valoreCorso, $array_id);
        $result      = $client['Report']->constructHvpArray($res, $hvpName, $nameCorsi);
        if (count($result) == 0) {
            echo json_encode($result) . "*?" . count($result);
            exit();
        }
        $new = $client['Report']->constructSummaryHvpReportPietro($result, $db);
        $_SESSION[$key] = $new;
        $_SESSION[$key]['time'] = time();
        $cont = count($new);
        if (!isset($_GET['excel'])) {
            //echo json_encode($result);
            echo json_encode($new) . "*?$cont";
            exit();
        } else {
            if ($cont != 0) {
                $nome = "report_hvplog_$gruppo" . "_" . $data_inizio . "_" . $data_fine . ".xlsx";
                $now = date("D, d M Y H:i:s");
                header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
                header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
                header("Last-Modified: {$now} GMT");
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header("Content-Disposition: attachment;filename=$nome");
                header("Content-Transfer-Encoding: binary");

                $spreadsheet = new Spreadsheet();
                $filtri = $_GET['filtri'];
                $letter = 'A';
                $j = 0;

                ///*vedo impostazioni copertina*///
                if (!isset($_GET['uid'])){
                    $userid = $_SESSION['user']['id'];
                } else{
                    $userid = 0;
                }

                $client['Report']->createCopertina($userid, $spreadsheet, $j);
                /*                 * ************************************************************ */
                /**
                 * vedo impostazioni sheet
                 * */
                $j++;
                $spreadsheet->setActiveSheetIndex($j);
                $index = $client['Report']->createSheet($userid, $spreadsheet, $j);
                $report_data = array();
                $report_data['date_from'] = $data_inizio;
                $report_data['date_to']   = $data_fine;
                $report_data['data']      = $new;

                $i = $client['Report']->setSummaryHvpSheet($index, $spreadsheet, $letter, $filtri, $j, $report_data);
                $client['Report']->addSettingsExcel($userid, $spreadsheet, $i);

                $writer = new Xlsx($spreadsheet);
                ob_start();
                $writer->save("php://output");
                $xlsData = ob_get_contents();
                ob_end_clean();

                $response = array(
                    'op' => 'ok',
                    'file' => "data:application/vnd.ms-excel;base64," . base64_encode($xlsData)
                );

                die(json_encode($response));
            } else {
                $message = "Nessun log";
                die(json_encode($message));
            }
        }
    }
    else if ($_GET['report'] == "rhvp") {
        $key = 'rhvp_' . $gruppo . "_" . $inizio . "_" . $fine;
//                                        if (!isset($_SESSION[$key]) || $_SESSION[$key]['time'] <= strtotime("-20 min")) {
        if (1 == 1){
            $result = array();
            /*
                                           $collection = new MongoDB\Client(
                                               'mongodb+srv://jobtek:TimeVision2021%40%21%3F@timevision.t9ed5.mongodb.net/TimeVision?retryWrites=true&w=majority');
                                            */
            require_once('config/connection/MongoDB/skilledin.php');
            $connection_string  = "mongodb+srv://";
            $connection_string .= conn_username . ":" . urlencode(conn_password) . "@" . conn_server;
            $connection_string .= "?retryWrites=true&w=majority";
            $collection = new MongoDB\Client($connection_string);

            $array_secondi = getTotalCorso($collection);
            $utenti = $collection->$db->mdl_utenti_complete;
            $cursor = $utenti->find(
                ['azienda' => $gruppo],
                ['projection' => ['userid' => 1, 'username' => 1, 'idnumber' => 1, 'email' => 1
                    , 'nome' => 1, 'azienda' => 1, 'citta' => 1, 'data' => 1]]
            );
            foreach ($cursor as $singolo) {
                if (in_array($singolo['userid'], array_column($result, "userid"))) {
                    continue;
                }
                if (!isset($array_secondi[$singolo['userid']])) continue;
                $tmp = array();
                $tmp['userid'] = $singolo['userid'];
                $name = $singolo['nome'];
                $tmp['name'] = $name;
                $tmp['username'] = $singolo['username'];
                $tmp['email'] = $singolo['email'];

                $tmp['phone'] = "n/d";
                if (isset($singolo['phone1']) && $singolo['phone1'] != "") $tmp['phone'] = $singolo['phone1'];
                if (isset($singolo['phone1']) && $singolo['phone2'] != "") $tmp['phone'] = $singolo['phone2'];
                if (isset($singolo['idnumber'])){
                    $tmp['cf'] = $singolo['idnumber'];
                } else{
                    $tmp['cf'] = "n/d";
                }
                if (!isset($singolo['data'])){
                    $tmp['data_nascita'] = "n/d";
                }else{
                    $tmp['data_nascita'] = date("d/m/Y", $singolo['data']);
                }
                if (!isset($singolo['citta'])){
                    $tmp['citta'] = "n/d";
                }else{
                    $tmp['citta'] = $singolo['citta'];
                }

                $tmp['azienda'] = $singolo['azienda'];
                $secondsViewed = getTotalSecondsViewed($singolo['userid'], $inizio, $fine, $collection);
                $secondsCorso = $array_secondi[$singolo['userid']];
                $differenza = $secondsCorso - $secondsViewed;
                $tmp['totalVisto'] = convertSeconds($secondsViewed);
                $tmp['totalCorso'] = convertSeconds($secondsCorso);
                $tmp['diff'] = convertSeconds($differenza);
                array_push($result, $tmp);
            }

            usort($result, function ($first, $second) {
                return $first['userid'] >= $second['userid'];
            });

            $_SESSION[$key] = $result;
            $_SESSION[$key]['time'] = time();
        } else {
            $result = $_SESSION[$key];
            unset($result['time']);
        }
        $cont = count($result);
        if (!isset($_GET['excel'])) {
            echo json_encode($result) . "*?$cont";
            exit();
        } else {
            if ($cont != 0) {
                $nome = "report_rhvp_$gruppo" . "_" . $data_inizio . "_" . $data_fine . ".xlsx";
                $now = date("D, d M Y H:i:s");
                header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
                header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
                header("Last-Modified: {$now} GMT");
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header("Content-Disposition: attachment;filename=$nome");
                header("Content-Transfer-Encoding: binary");

                $spreadsheet = new Spreadsheet();
                $filtri = $_GET['filtri'];
                $j = 0;
                $letter = "A";
/*
                                                $collection = new MongoDB\Client(
                                                    'mongodb+srv://jobtek:TimeVision2021%40%21%3F@timevision.t9ed5.mongodb.net/TimeVision?retryWrites=true&w=majority');
*/
                //TODO modificare stringa di connessione

                require_once('config/connection/MongoDB/skilledin.php');
                $connection_string  = "mongodb+srv://";
                $connection_string .= conn_username . ":" . urlencode(conn_password) . "@" . conn_server;
                $connection_string .= "?retryWrites=true&w=majority";
                $collection = new MongoDB\Client($connection_string);

                ///*vedo impostazioni copertina*///
                $copertina = $collection->admin->settings_excel_copertina;
                if (!isset($_GET['uid'])){
                    $userid = $_SESSION['user']['id'];
                }else{
                    $userid = 0;
                }

                $cursor = $copertina->find(
                    ['userid' => (int) $userid],
                    ['projection' => ['txt1' => 1, 'txt2' => 1, 'img' => 1],
                        'limit' => 1,
                        'sort' => ['_id' => -1],
                    ]
                );
                $txt1 = "";
                $txt2 = "";
                $src = "";
                foreach ($cursor as $singolo) {
                    $txt1 = $singolo['txt1'];
                    $txt2 = $singolo['txt2'];
                    $src = $singolo['img'];
                }
                if ($src != "") {
                    $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
                    $drawing->setName('S1');
                    $drawing->setDescription('Intestazione');
                    $drawing->setPath('img/' . $src);
                    $drawing->setCoordinates('A1');
                    $drawing->setHeight(100);
                    $drawing->setWidth(800);
                    $drawing->setWorksheet($spreadsheet->getActiveSheet());
                }
                if ($txt1 != "") {
                    $spreadsheet->setActiveSheetIndex($j)->setCellValue("A13", $txt1);
                }
                if ($txt2 != "") {
                    $spreadsheet->setActiveSheetIndex($j)->setCellValue("A19", $txt2);
                }
                $spreadsheet->setActiveSheetIndex($j)->setCellValue("H17", "REGISTRO ATTIVITA' FORMATIVE");
                $spreadsheet->getActiveSheet()->getStyle("H17")->getFont()->setBold(true);
                $spreadsheet->getActiveSheet()->getStyle("H17")->getFont()->setSize(18);
                $spreadsheet->getActiveSheet()->setTitle("Copertina");
                $spreadsheet->createSheet();
                /*                 * ************************************************************ */

                /**
                 * vedo impostazioni sheet
                 * */
                $j++;
                $spreadsheet->setActiveSheetIndex($j);
                $sheet = $collection->admin->settings_excel_sheet;
                $s1 = "";
                $s2 = "";
                $s3 = "";
                $t1 = "";
                $cursor2 = $sheet->find(
                    ['userid' => (int) $userid],
                    ['projection' => ['s1' => 1, 's2' => 1, 'timbro' => 1, 't1' => 1],
                        'limit' => 1,
                        'sort' => ['_id' => -1],
                    ]
                );
                foreach ($cursor2 as $singolo) {
                    $s1 = $singolo['s1'];
                    $s2 = $singolo['s2'];
                    $s3 = $singolo['timbro'];
                    $t1 = $singolo['t1'];
                }

                $index = 1;
                if ($s1 != "") {
                    $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
                    $drawing->setName('S1');
                    $drawing->setDescription('Intestazione Sheet');
                    $drawing->setPath('img/' . $s1);
                    $drawing->setCoordinates('A1');
                    $drawing->setHeight(100);
                    $drawing->setWidth(800);
                    $drawing->setWorksheet($spreadsheet->getActiveSheet());
                    $index = 16;
                }
                if ($t1 != "") {
                    $spreadsheet->setActiveSheetIndex($j)->setCellValue("A13", $t1);
                    $index = 16;
                }
                if (!in_array("userid", $filtri)) {
                    $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Userid");
                    $letter++;
                }
                if (!in_array("username", $filtri)) {
                    $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Username");
                    $letter++;
                }
                if (!in_array("name", $filtri)) {
                    $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Nome Cognome");
                    $letter++;
                }
                if (!in_array("cf", $filtri)) {
                    $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Codice Fiscale");
                    $letter++;
                }
                if (!in_array("email", $filtri)) {
                    $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Email");
                    $letter++;
                }
                if (!in_array("dn", $filtri)) {
                    $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Data di nascita");
                    $letter++;
                }
                if (!in_array("ln", $filtri)) {
                    $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Luogo di nascita");
                    $letter++;
                }
                if (!in_array("azienda", $filtri)) {
                    $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Azienda");
                    $letter++;
                }
                if (!in_array("phone", $filtri)) {
                    $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "N.di telefono");
                    $letter++;
                }
                if (!in_array("orarioA", $filtri)) {
                    $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Tempo effettivo di fruizione");
                    $letter++;
                }
                if (!in_array("orarioL", $filtri)) {
                    $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Durata complessiva");
                    $letter++;
                }
                if (!in_array("ts", $filtri)) {
                    $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Differenza");
                }
                $spreadsheet->getActiveSheet()->setTitle("Report");
                $from = "A1";
                $to = $letter . $index;
                $spreadsheet->getActiveSheet()->getStyle("$from:$to")->getFont()->setBold(true);
                $i = $index + 1;
                foreach ($result as $singolo) {
                    $letter = "A";
                    if (!in_array("userid", $filtri)) {
                        $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['userid']);
                        $letter++;
                    }
                    if (!in_array("username", $filtri)) {
                        $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['username']);
                        $letter++;
                    }
                    if (!in_array("name", $filtri)) {
                        $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['name']);
                        $letter++;
                    }
                    if (!in_array("cf", $filtri)) {
                        $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, @$singolo['cf']);
                        $letter++;
                    }
                    if (!in_array("email", $filtri)) {
                        $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['email']);
                        $letter++;
                    }
                    if (!in_array("dn", $filtri)) {
                        $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['data_nascita']);
                        $letter++;
                    }
                    if (!in_array("ln", $filtri)) {
                        $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['citta']);
                        $letter++;
                    }
                    if (!in_array("azienda", $filtri)) {
                        $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['azienda']);
                        $letter++;
                    }
                    if (!in_array("phone", $filtri)) {
                        $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['phone']);
                        $letter++;
                    }
                    if (!in_array("orarioA", $filtri)) {
                        $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["totalVisto"]);
                        $letter++;
                    }
                    if (!in_array("orarioL", $filtri)) {
                        $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["totalCorso"]);
                        $letter++;
                    }
                    if (!in_array("ts", $filtri)) {
                        $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["diff"]);
                    }
                    $i++;
                }
                if ($s2 != "") {
                    $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
                    $drawing->setName('S2');
                    $drawing->setDescription('S2 bordo sinistro');
                    $drawing->setPath('img/' . $s2);
                    $coordinate = $i + 3;
                    $drawing->setCoordinates('A' . $coordinate);
                    $drawing->setHeight(100);
                    $drawing->setWidth(100);
                    $drawing->setWorksheet($spreadsheet->getActiveSheet());
                }
                if ($s3 != "") {
                    $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
                    $drawing->setName('S3');
                    $drawing->setDescription('S3 timbro');
                    $drawing->setPath('img/' . $s3);
                    $coordinate = $i + 3;
                    $drawing->setCoordinates('L' . $coordinate);
                    $drawing->setHeight(100);
                    $drawing->setWidth(100);
                    $drawing->setWorksheet($spreadsheet->getActiveSheet());
                }
                foreach (range('A', $letter) as $columnID) {
                    $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                        ->setAutoSize(true);
                }
                $spreadsheet->getActiveSheet()->getStyle("A" . $index . ":" . $letter . $index)
                    ->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('00FF7F');
                $writer = new Xlsx($spreadsheet);
                ob_start();
                $writer->save("php://output");
                $xlsData = ob_get_contents();
                ob_end_clean();

                $response = array(
                    'op' => 'ok',
                    'file' => "data:application/vnd.ms-excel;base64," . base64_encode($xlsData)
                );

                die(json_encode($response));
            } else {
                $message = "Nessun log";
                die(json_encode($message));
            }
        }
    }
    else if ($_GET['report'] == "shvpAldo") {echo $client['Report']->convertSeconds(7193.002666);exit();
        $key = 'shvp_' . $gruppo . "_" . $inizio . "_" . $fine;
        if (true || !isset($_SESSION[$key]) || $_SESSION[$key]['time'] <= strtotime("-20 min")) {
            $hvpName = $client['Report']->getNameAttivitaHVP($db);
            $nameCorsi = $client['Report']->getAllNameCourse($db);
            $array_id = array_values($array_id);
            $res = $client['Report']->getAggregateHvp($db, $inizio, $fine, null, $array_id);

            /* Aldo */
            $client['Report']->setArray($res);
            /* / Aldo */
            //var_dump($res);exit();
            $result = $client['Report']->constructHvpArray($res, $hvpName, $nameCorsi);
            //var_dump($result);exit();

            if (count($result) == 0) {
                echo json_encode($result) . "*?" . count($result);
                exit();
            }
            $tmp = $client['Report']->constructHvpReport($result, $db);
            $res = $client['Report']->constructSHvpReport($tmp);

            $res = explode("*", $res);

            $new = json_decode($res[0], true);
            var_dump($new);exit();
            $array_mesi = json_decode($res[1], true);



            $_SESSION[$key] = $new;
            $_SESSION[$key]['mesi'] = $array_mesi;
            $_SESSION[$key]['time'] = time();
        } else {
            $new = $_SESSION[$key];
            $array_mesi = $_SESSION[$key]['mesi'];
            unset($new['mesi']);
            unset($new['time']);
        }

        $total = count($new);
        if (!isset($_GET['excel'])) {
            die(json_encode($new) . "*?$total" . "*?" . json_encode($array_mesi));
        } else {
            if ($total != 0) {
                $nome = "report_hvplog_$gruppo" . "_" . $data_inizio . "_" . $data_fine . ".xlsx";
                $now = date("D, d M Y H:i:s");
                header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
                header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
                header("Last-Modified: {$now} GMT");
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header("Content-Disposition: attachment;filename=$nome");
                header("Content-Transfer-Encoding: binary");


                $spreadsheet = new Spreadsheet();
                $filtri = $_GET['filtri'];
                $letter = 'A';
                $j = 0;


                ///*vedo impostazioni copertina*///
                if (!isset($_GET['uid'])){
                    $userid = $_SESSION['user']['id'];
                }else{
                    $userid = 0;
                }

                $client['Report']->createCopertina($userid, $spreadsheet, $j);


                /*                 * ************************************************************ */

                /**
                 * vedo impostazioni sheet
                 * */
                $j++;
                $spreadsheet->setActiveSheetIndex($j);
                $index = $client['Report']->createSheet($userid, $spreadsheet, $j);
                $i = $client['Report']->setTimeinSheet($index, $spreadsheet, $letter, $filtri, $j, $new, $array_mesi);
                $client['Report']->addSettingsExcel($userid, $spreadsheet, $i);

                $writer = new Xlsx($spreadsheet);
                ob_start();
                $writer->save("php://output");
                $xlsData = ob_get_contents();
                ob_end_clean();

                $response = array(
                    'op' => 'ok',
                    'file' => "data:application/vnd.ms-excel;base64," . base64_encode($xlsData)
                );

                die(json_encode($response));
            } else {
                $message = "Nessun log";
                die(json_encode($message));
            }
        }
    }
    else if ($_GET['report'] == "shvp") {
        $key = 'shvp_' . $gruppo . "_" . $inizio . "_" . $fine;
        if (1 == 1) {
            // if (true || !isset($_SESSION[$key]) || $_SESSION[$key]['time'] <= strtotime("-20 min")) {
            $hvpName   = $client['Report']->getNameAttivitaHVP($db);
            $nameCorsi = $client['Report']->getAllNameCourse($db);
            $array_id  = array_values($array_id);
            $res       = $client['Report']->getAggregateHvp($db, $inizio, $fine, null, $array_id);
            $result    = $client['Report']->constructHvpArray($res, $hvpName, $nameCorsi);
            if (count($result) == 0) {
                echo json_encode($result) . "*?" . count($result);
                exit();
            }

            // $last_progres_enrico = $this->getLastProgressEnrico($singolo['userid'], $singolo['timestamp'], $singolo['objectid'], $db);
            // $first_viewed_enrico = $this->getFirstViewedEnrico($singolo['userid'], $singolo['timestamp'], $singolo['objectid'], $db);
            // $enzo = $client['Report']->reportMongo($db, $inizio, $fine);
            // var_dump(count($enzo));
            // exit;
            //formazione originale
            // $tmp = $client['Report']->constructHvpReportOriginal($result, $db);
            // $res = $client['Report']->constructSHvpReportOriginal($tmp);
            // $pp = $client['Report']->getObjectId(1181, $db);
            // var_dump($pp);
            // exit;

            $tmp = $client['Report']->constructHvpFinal($result, $db);
            $res = $client['Report']->constructSHvpReportFinal($tmp, $db);

            $res = explode("*", $res);
            $new = json_decode($res[0], true);

            $array_mesi = json_decode($res[1], true);
            //var_dump($array_mesi);exit();


            $_SESSION[$key] = $new;
            $_SESSION[$key]['mesi'] = $array_mesi;
            $_SESSION[$key]['time'] = time();
        } else {
            $new = $_SESSION[$key];
            $array_mesi = $_SESSION[$key]['mesi'];
            unset($new['mesi']);
            unset($new['time']);
        }
        $total = count($new);
        if (!isset($_GET['excel'])) {
            die(json_encode($new) . "*?$total" . "*?" . json_encode($array_mesi));
        } else {
            if ($total != 0) {
                $nome = "report_hvplog_$gruppo" . "_" . $data_inizio . "_" . $data_fine . ".xlsx";
                $now = date("D, d M Y H:i:s");
                if(isset($_GET['save']) && $_GET['save'] == true) {
                    mail("flauto.vincenzo84@gmail.com","Inizio generazione file Excel","Link al file: "."https://update.corsinrete.com/skilledin/report/public/".slugify($gruppo.' '.date('Y-m-d H:i:s', strtotime('now'))).".xlsx");
                }else{
                    header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
                    header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
                    header("Last-Modified: {$now} GMT");
                    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                    header("Content-Disposition: attachment;filename=$nome");
                    header("Content-Transfer-Encoding: binary");
                }

                $spreadsheet = new Spreadsheet();
                $filtri = $_GET['filtri'];
                $letter = 'A';
                $j = 0;
                 ///*vedo impostazioni copertina*///
                if (!isset($_GET['uid'])){
                    $userid = $_SESSION['user']['id'];
                }else{
                    $userid = 0;
                }

                $client['Report']->createCopertina($userid, $spreadsheet, $j);


                /*                 * ************************************************************ */

                /**
                 * vedo impostazioni sheet
                 * */
                $j++;
                $spreadsheet->setActiveSheetIndex($j);
                $index = $client['Report']->createSheet($userid, $spreadsheet, $j);
                $i = $client['Report']->setTimeinSheet($index, $spreadsheet, $letter, $filtri, $j, $new, $array_mesi);
                $client['Report']->addSettingsExcel($userid, $spreadsheet, $i);

                if(isset($_GET['save']) && $_GET['save'] == true) {
                    $writer = new Xlsx($spreadsheet);
                    ob_start();
                    $writer->save(dirname(__FILE__).'/public/'.slugify($gruppo.' '.date('Y-m-d H:i:s', strtotime('now'))).'.xlsx'); //qui
                    ob_end_clean();

                    $response = array(
                        'op' => 'ok',
                        'file' => "https://update.corsinrete.com/skilledin/report/public/".slugify($gruppo.' '.date('Y-m-d H:i:s', strtotime('now'))).".xlsx"
                    );

                    mail("flauto.vincenzo84@gmail.com","Generazione file Excel","Link al file: "."https://update.corsinrete.com/skilledin/report/public/".slugify($gruppo.' '.date('Y-m-d H:i:s', strtotime('now'))).".xlsx");


                    die(json_encode($response));
                }else{
                    $writer = new Xlsx($spreadsheet);
                    ob_start();
                    $writer->save("php://output");
                    $xlsData = ob_get_contents();
                    ob_end_clean();

                    $response = array(
                        'op' => 'ok',
                        'file' => "data:application/vnd.ms-excel;base64," . base64_encode($xlsData)
                    );
                    die(json_encode($response));
                }
            } else {
                $message = "Nessun log";
                die(json_encode($message));
            }
        }
    }
    else if ($_GET['report'] == "shvpName") {
        //enzo vista
        $idCourse = $_GET['course'];
        $key = 'shvpName_' . $idCourse . "_" . $gruppo . "_" . $inizio . '_' . $fine;

        //enzo
//                                        if (!isset($_SESSION[$key]) || $_SESSION[$key]['time'] <= strtotime("-20 min")) {
        if (1 == 1) {
            $hvpName = $client['Report']->getNameAttivitaHVP($db);
            $nameCorsi = $client['Report']->getAllNameCourse($db);
            $array_id = array_values($array_id);
            $res = $client['Report']->getAggregateHvp($db, $inizio, $fine, $idCourse, $array_id);
            $result = $client['Report']->constructHvpArray($res, $hvpName, $nameCorsi);
            if (count($result) == 0) {
                echo json_encode($result) . "*?" . count($result) . "*?" . '[]';
                exit();
            }

//                                            $tmp = $client['Report']->constructHvpReportOriginal($result, $db);
////                                            $res = $client['Report']->constructSHvpReportNewEnzo($tmp);
//                                            $res = $client['Report']->constructSHvpReportOriginal($tmp);

            $tmp = $client['Report']->constructHvpFinal($result, $db);
            $res = $client['Report']->constructSHvpReportFinal($tmp, $db);
            $res = explode("*", $res);
            $new = json_decode($res[0], true);
            $array_mesi = json_decode($res[1], true);

            $_SESSION[$key] = $new;
            $_SESSION[$key]['mesi'] = $array_mesi;
            $_SESSION[$key]['time'] = time();
        } else {
            $new = $_SESSION[$key];
            $array_mesi = $_SESSION[$key]['mesi'];
            unset($new['mesi']);
            unset($new['time']);
        }
        $total = count($new);
        if (!isset($_GET['excel'])) {
            die(json_encode($new) . "*?$total" . "*?" . json_encode($array_mesi));
        } else {
            if ($total != 0) {
                $nome = "report_hvplog_$gruppo" . "_" . $data_inizio . "_" . $data_fine . ".xlsx";
                $now = date("D, d M Y H:i:s");
                header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
                header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
                header("Last-Modified: {$now} GMT");
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header("Content-Disposition: attachment;filename=$nome");
                header("Content-Transfer-Encoding: binary");

                $spreadsheet = new Spreadsheet();
                $filtri = $_GET['filtri'];
                $letter = 'A';
                $j = 0;


                ///*vedo impostazioni copertina*///
                if (!isset($_GET['uid'])){
                    $userid = $_SESSION['user']['id'];
                }else{
                    $userid = 0;
                }

                $client['Report']->createCopertina($userid, $spreadsheet, $j);


                /*                 * ************************************************************ */

                /**
                 * vedo impostazioni sheet
                 * */
                $j++;
                $spreadsheet->setActiveSheetIndex($j);
                $index = $client['Report']->createSheet($userid, $spreadsheet, $j);
                $i = $client['Report']->setTimeinSheet($index, $spreadsheet, $letter, $filtri, $j, $new, $array_mesi, "shvpName");
                $client['Report']->addSettingsExcel($userid, $spreadsheet, $i);


                $writer = new Xlsx($spreadsheet);
                ob_start();
                $writer->save("php://output");
                $xlsData = ob_get_contents();
                ob_end_clean();

                $response = array(
                    'op' => 'ok',
                    'file' => "data:application/vnd.ms-excel;base64," . base64_encode($xlsData)
                );
                die(json_encode($response));
            } else {
                $message = "Nessun log";
                die(json_encode($message));
            }
        }
    }
    else if ($_GET['report'] == "rhvpName") {
        //enzo vista
        $idCourse = $_GET['course'];
        $key = 'rhvpName_' . $idCourse . "_" . $gruppo . "_" . $inizio . '_' . $fine;


        //enzo
        //                                        if (!isset($_SESSION[$key]) || $_SESSION[$key]['time'] <= strtotime("-20 min")) {
        if (1 == 1) {
/*
                                            $collection = new MongoDB\Client(
                                                'mongodb+srv://jobtek:TimeVision2021%40%21%3F@timevision.t9ed5.mongodb.net/TimeVision?retryWrites=true&w=majority');
*/
            require_once('config/connection/MongoDB/skilledin.php');
            $connection_string  = "mongodb+srv://";
            $connection_string .= conn_username . ":" . urlencode(conn_password) . "@" . conn_server;
            $connection_string .= "?retryWrites=true&w=majority";
            $collection = new MongoDB\Client($connection_string);


            $hvpName = $client['Report']->getNameAttivitaHVP($db);
            $nameCorsi = $client['Report']->getAllNameCourse($db);
            $array_id = array_values($array_id);
            $res = $client['Report']->getAggregateHvp($db, $inizio, $fine, $idCourse, $array_id);
            $data = $client['Report']->constructHvpArray($res, $hvpName, $nameCorsi);

            if (count($data) == 0) {
                echo json_encode($data) . "*?" . count($data);
                exit();
            }

            $corso_parsato = array();
            $result = array();

            foreach ($data as $singolo) {
//                                                var_dump($singolo);

                $tempo_cumulato = 0;

                $tmp = array();
                $tmp['userid'] = $singolo['userid'];

                $tmp['name'] = $singolo['name'];
                $tmp['username'] = $singolo['username'];
                $tmp['email'] = $singolo['email'];

                $tmp['phone'] = "n/d";
                if (isset($singolo['phone1']) && $singolo['phone1'] != "")
                    $tmp['phone'] = $singolo['phone1'];
                if (isset($singolo['phone1']) && $singolo['phone2'] != "")
                    $tmp['phone'] = $singolo['phone2'];
                $tmp['cf'] = $singolo['cf'];

                $tmp['data_nascita'] = $singolo['data_nascita'];


                if (!isset($singolo['citta'])){
                    $tmp['citta'] = "n/d";
                }else{
                    $tmp['citta'] = $singolo['citta'];
                }

                $tmp['azienda'] = $singolo['azienda'];
                $tmp['corso'] = $singolo['corso'];
                $tmp['lezione'] = $singolo['lezione'];

                $tmpid = $singolo['userid'].$singolo['courseid'].$singolo['lezione_id'];

//                                                $corso_parsato[$tmpid] = $singolo['objectid'];

                if (!array_key_exists($singolo['tmpid'], $corso_parsato)) {
                    if($singolo['action'] == 'progress'){
                        $progress = $singolo['other'];
                        if(@unserialize($progress) === false){
                            $progress = json_decode($progress);
                        }else{
                            $progress = json_decode(unserialize($progress));
                        }

                        if(!is_null($progress)){
                            @$tempo_cumulato += (int)$progress->progress;
                        }else{
                            $tempo_cumulato += 0;
                        }

                        $tmp['totalVisto'] = $tempo_cumulato;

//                                                        var_dump(intval($singolo['courseid'])); exit;

                        $mongo_second_corse_total = 0;
                        $mongo_second_corse_total = $client['Report']->getTimeLesson($singolo['objectid'], $db);

                        $tmp['totalCorso'] = $mongo_second_corse_total;
                        $tmp['diff'] = ($mongo_second_corse_total - $tempo_cumulato);

                        $corso_parsato[$tmpid] = $tmp;
                    }
                }else{
                    if($singolo['action'] == 'progress'){
                        $progress = $singolo['other'];
                        if(@unserialize($progress) === false){
                            $progress = json_decode($progress);
                        }else{
                            $progress = json_decode(unserialize($progress));
                        }

                        if(!is_null($progress)){
                            @$tempo_cumulato += (int)$progress->progress;
                        }else{
                            $tempo_cumulato += 0;
                        }

                        $corso_parsato[$tmpid]['totalVisto'] += $tempo_cumulato;
                        $corso_parsato[$tmpid]['diff'] = $corso_parsato[$tmpid]['totalCorso'] - $corso_parsato[$tmpid]['totalVisto'];
                    }
                }
            }
            foreach($corso_parsato as $dati){
                $percentage = (($dati['totalVisto'] / $dati['totalCorso']) * 100);
                $dati['totalVisto'] = convertSeconds($dati['totalVisto']);
                $dati['totalCorso'] = convertSeconds($dati['totalCorso']);
                $dati['diff'] = convertSeconds($dati['diff']);
                $dati['percentage'] = number_format(round($percentage, 2), 2) . '%';

                array_push($result, $dati);
            }

            $_SESSION[$key] = $result;
            $_SESSION[$key]['time'] = time();

        } else {
            $result = $_SESSION[$key];
            unset($result['time']);
        }

        $cont = count($result);

        if (!isset($_GET['excel'])) {
            $returnData = json_encode($result) . "*?$cont";
            echo $returnData;
            exit();
        } else {
//                                           var_dump($cont); exit;
            if ($cont != 0) {
                $nome = "report_rhvp_$gruppo" . "_" . $data_inizio . "_" . $data_fine . ".xlsx";
                $now = date("D, d M Y H:i:s");
                header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
                header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
                header("Last-Modified: {$now} GMT");
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header("Content-Disposition: attachment;filename=$nome");
                header("Content-Transfer-Encoding: binary");

                $spreadsheet = new Spreadsheet();
                $filtri = $_GET['filtri'];
                $j = 0;


                $letter = "A";
/*
                                                $collection = new MongoDB\Client(
                                                    'mongodb+srv://jobtek:TimeVision2021%40%21%3F@timevision.t9ed5.mongodb.net/TimeVision?retryWrites=true&w=majority');
*/
                require_once('config/connection/MongoDB/skilledin.php');
                $connection_string  = "mongodb+srv://";
                $connection_string .= conn_username . ":" . urlencode(conn_password) . "@" . conn_server;
                $connection_string .= "?retryWrites=true&w=majority";
                $collection = new MongoDB\Client($connection_string);



                ///*vedo impostazioni copertina*///
                $copertina = $collection->admin->settings_excel_copertina;
                if (!isset($_GET['uid'])){
                    $userid = $_SESSION['user']['id'];
                }else{
                    $userid = 0;
                }

                $cursor = $copertina->find(
                    ['userid' => (int) $userid],
                    ['projection' => ['txt1' => 1, 'txt2' => 1, 'img' => 1],
                        'limit' => 1,
                        'sort' => ['_id' => -1],
                    ]
                );
                $txt1 = "";
                $txt2 = "";
                $src = "";
                foreach ($cursor as $singolo) {
                    $txt1 = $singolo['txt1'];
                    $txt2 = $singolo['txt2'];
                    $src = $singolo['img'];
                }
                if ($src != "") {
                    $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
                    $drawing->setName('S1');
                    $drawing->setDescription('Intestazione');
                    $drawing->setPath('img/' . $src);
                    $drawing->setCoordinates('A1');
                    $drawing->setHeight(100);
                    $drawing->setWidth(800);
                    $drawing->setWorksheet($spreadsheet->getActiveSheet());
                }
                if ($txt1 != "") {
                    $spreadsheet->setActiveSheetIndex($j)->setCellValue("A13", $txt1);
                }
                if ($txt2 != "") {
                    $spreadsheet->setActiveSheetIndex($j)->setCellValue("A19", $txt2);
                }
                $spreadsheet->setActiveSheetIndex($j)->setCellValue("H17", "REGISTRO ATTIVITA' FORMATIVE");
                $spreadsheet->getActiveSheet()->getStyle("H17")->getFont()->setBold(true);
                $spreadsheet->getActiveSheet()->getStyle("H17")->getFont()->setSize(18);

                $spreadsheet->getActiveSheet()->setTitle("Copertina");
                $spreadsheet->createSheet();
                /*                 * ************************************************************ */
                /**
                 * vedo impostazioni sheet
                 * */
                $j++;
                $spreadsheet->setActiveSheetIndex($j);
                $sheet = $collection->admin->settings_excel_sheet;
                $s1 = "";
                $s2 = "";
                $s3 = "";
                $t1 = "";
                $cursor2 = $sheet->find(
                    ['userid' => (int) $userid],
                    ['projection' => ['s1' => 1, 's2' => 1, 'timbro' => 1, 't1' => 1],
                        'limit' => 1,
                        'sort' => ['_id' => -1],
                    ]
                );
                foreach ($cursor2 as $singolo) {
                    $s1 = $singolo['s1'];
                    $s2 = $singolo['s2'];
                    $s3 = $singolo['timbro'];
                    $t1 = $singolo['t1'];
                }

                $index = 1;
                if ($s1 != "") {
                    $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
                    $drawing->setName('S1');
                    $drawing->setDescription('Intestazione Sheet');
                    $drawing->setPath('img/' . $s1);
                    $drawing->setCoordinates('A1');
                    $drawing->setHeight(100);
                    $drawing->setWidth(800);
                    $drawing->setWorksheet($spreadsheet->getActiveSheet());
                    $index = 16;
                }
                if ($t1 != "") {
                    $spreadsheet->setActiveSheetIndex($j)->setCellValue("A13", $t1);
                    $index = 16;
                }

                if (!in_array("userid", $filtri)) {
                    $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Userid");
                    $letter++;
                }
                if (!in_array("username", $filtri)) {
                    $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Username");
                    $letter++;
                }
                if (!in_array("name", $filtri)) {
                    $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Nome Cognome");
                    $letter++;
                }
                if (!in_array("cf", $filtri)) {
                    $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Codice Fiscale");
                    $letter++;
                }
                if (!in_array("email", $filtri)) {
                    $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Email");
                    $letter++;
                }
                if (!in_array("dn", $filtri)) {
                    $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Data di nascita");
                    $letter++;
                }
                if (!in_array("ln", $filtri)) {
                    $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Luogo di nascita");
                    $letter++;
                }
                if (!in_array("phone", $filtri)) {
                    $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "N.di telefono");
                    $letter++;
                }
                if (!in_array("azienda", $filtri)) {
                    $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Azienda");
                    $letter++;
                }
                if (!in_array("corso", $filtri)) {
                    $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Corso");
                    $letter++;
                }
                if (!in_array("lezione", $filtri)) {
                    $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Lezione");
                    $letter++;
                }
                if (!in_array("orarioA", $filtri)) {
                    $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Tempo effettivo di fruizione");
                    $letter++;
                }
                if (!in_array("orarioL", $filtri)) {
                    $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Durata complessiva");
                    $letter++;
                }
                if (!in_array("ts", $filtri)) {
                    $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Differenza");
                    $letter++;
                }
                if (!in_array("percentage", $filtri)) {
                    $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Percentuale completamento");
                }
                $spreadsheet->getActiveSheet()->setTitle("Report");

                $from = "A1";
                $to = $letter . $index;
                $spreadsheet->getActiveSheet()->getStyle("$from:$to")->getFont()->setBold(true);

                $i = $index + 1;
                foreach ($result as $singolo) {
                    $letter = "A";
                    if (!in_array("userid", $filtri)) {
                        $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['userid']);
                        $letter++;
                    }
                    if (!in_array("username", $filtri)) {
                        $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['username']);
                        $letter++;
                    }
                    if (!in_array("name", $filtri)) {
                        $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['name']);
                        $letter++;
                    }
                    if (!in_array("cf", $filtri)) {
                        $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, @$singolo['cf']);
                        $letter++;
                    }
                    if (!in_array("email", $filtri)) {
                        $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['email']);
                        $letter++;
                    }
                    if (!in_array("dn", $filtri)) {
                        $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['data_nascita']);
                        $letter++;
                    }
                    if (!in_array("ln", $filtri)) {
                        $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['citta']);
                        $letter++;
                    }
                    if (!in_array("phone", $filtri)) {
                        $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['phone']);
                        $letter++;
                    }
                    if (!in_array("azienda", $filtri)) {
                        $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['azienda']);
                        $letter++;
                    }
                    if (!in_array("corso", $filtri)) {
                        $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['corso']);
                        $letter++;
                    }
                    if (!in_array("lezione", $filtri)) {
                        $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['lezione']);
                        $letter++;
                    }
                    if (!in_array("orarioA", $filtri)) {
                        $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["totalVisto"]);
                        $letter++;
                    }
                    if (!in_array("orarioL", $filtri)) {
                        $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["totalCorso"]);
                        $letter++;
                    }
                    if (!in_array("ts", $filtri)) {
                        $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["diff"]);
                        $letter++;
                    }
                    if (!in_array("percentage", $filtri)) {
                        $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["percentage"]);
                    }
                    $i++;
                }


                if ($s2 != "") {
                    $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
                    $drawing->setName('S2');
                    $drawing->setDescription('S2 bordo sinistro');
                    $drawing->setPath('img/' . $s2);
                    $coordinate = $i + 3;
                    $drawing->setCoordinates('A' . $coordinate);
                    $drawing->setHeight(100);
                    $drawing->setWidth(100);
                    $drawing->setWorksheet($spreadsheet->getActiveSheet());
                }
                if ($s3 != "") {
                    $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
                    $drawing->setName('S3');
                    $drawing->setDescription('S3 timbro');
                    $drawing->setPath('img/' . $s3);
                    $coordinate = $i + 3;
                    $drawing->setCoordinates('L' . $coordinate);
                    $drawing->setHeight(100);
                    $drawing->setWidth(100);
                    $drawing->setWorksheet($spreadsheet->getActiveSheet());
                }
                foreach (range('A', $letter) as $columnID) {
                    $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                        ->setAutoSize(true);
                }
                $spreadsheet->getActiveSheet()->getStyle("A" . $index . ":" . $letter . $index)
                    ->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('00FF7F');
                $writer = new Xlsx($spreadsheet);
                ob_start();
                $writer->save("php://output");
                $xlsData = ob_get_contents();
                ob_end_clean();

                $response = array(
                    'op' => 'ok',
                    'file' => "data:application/vnd.ms-excel;base64," . base64_encode($xlsData)
                );

                die(json_encode($response));
            } else {
                $message = "Nessun log";
                die(json_encode($message));
            }
        }
    }
    else if ($_GET['report'] == "hvpName") {
        $idCourse = $_GET['course'];
        $key = 'hvp_' . $idCourse . '_' . $gruppo . "_" . $inizio . '_' . $fine;
        if (!isset($_SESSION[$key]) || $_SESSION[$key]['time'] <= strtotime("-20 min")) {
            $hvpName = $client['Report']->getNameAttivitaHVP($db);
            $nameCorsi = $client['Report']->getAllNameCourse($db);
            $array_id = array_values($array_id);
            $res = $client['Report']->getAggregateHvp($db, $inizio, $fine, $idCourse, $array_id);
            $result = $client['Report']->constructHvpArray($res, $hvpName, $nameCorsi);


            if (count($result) == 0) {
                echo json_encode($result) . "*?" . count($result);
                exit();
            }
            $new = $client['Report']->constructHvpReport($result, $db);
            $_SESSION[$key] = $new;
            $_SESSION[$key]['time'] = time();
        } else {
            $new = $_SESSION[$key];
            unset($new['time']);
        }

        $cont = count($new);

        if (!isset($_GET['excel'])) {
            echo json_encode($new) . "*?$cont";
            exit();
        } else {
            if ($cont != 0) {
                $nome = "report_hvplog_$gruppo" . "_" . $data_inizio . "_" . $data_fine . ".xlsx";
                $now = date("D, d M Y H:i:s");
                header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
                header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
                header("Last-Modified: {$now} GMT");
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header("Content-Disposition: attachment;filename=$nome");
                header("Content-Transfer-Encoding: binary");

                $spreadsheet = new Spreadsheet();

                $filtri = $_GET['filtri'];
                $letter = 'A';
                $j = 0;
                                                ///*vedo impostazioni copertina*///
                if (!isset($_GET['uid'])){
                    $userid = $_SESSION['user']['id'];
                }else{
                    $userid = 0;
                }

                $client['Report']->createCopertina($userid, $spreadsheet, $j);


                /*                 * ************************************************************ */
                /**
                 * vedo impostazioni sheet
                 * */
                $j++;
                $spreadsheet->setActiveSheetIndex($j);
                $index = $client['Report']->createSheet($userid, $spreadsheet, $j);
                $i = $client['Report']->setHvpSheet($index, $spreadsheet, $letter, $filtri, $j, $new);
                $client['Report']->addSettingsExcel($userid, $spreadsheet, $i);

                $writer = new Xlsx($spreadsheet);
                ob_start();
                $writer->save("php://output");
                $xlsData = ob_get_contents();
                ob_end_clean();

                $response = array(
                    'op' => 'ok',
                    'file' => "data:application/vnd.ms-excel;base64," . base64_encode($xlsData)
                );

                die(json_encode($response));
            } else {
                $message = "Nessun log";
                die(json_encode($message));
            }
        }
    }
    else if ($_GET['report'] == "bbbName") {
        $idCourse = $_GET['course'];

        $key = 'bbb_' . $idCourse . '_' . $gruppo . '_' . $inizio . '_' . $fine;
        if (!isset($_SESSION[$key]) || $_SESSION[$key]['time'] <= strtotime("-20 min")) {
            $bbbName = $client['Report']->getNameAttivitaBBB($db);
            $nameCorsi = $client['Report']->getAllNameCourse($db);
            $array_id = array_values($array_id);

            $result = $client['Report']->getAggregateBBB($db, $inizio, $fine, $idCourse, $array_id);
            $res = $client['Report']->constructBbbArray($result, $bbbName, $nameCorsi);

            if (count($res) == 0) {
                echo json_encode($res) . "*?" . count($res);
                exit();
            }

            $finale = $client['Report']->constructSintesiReport($res);

            $_SESSION[$key] = $finale;
            $_SESSION[$key]['time'] = time();
        } else {
            $finale = $_SESSION[$key];
            unset($finale['time']);
        }

        $cont = count($finale);

        if (!isset($_GET['excel'])) {
            echo json_encode($finale) . "*?$cont";
            exit();
        } else {
            if ($cont != 0) {
                $nome = "report_bbb_$gruppo" . "_" . $data_inizio . "_" . $data_fine . ".xlsx";
                $now = date("D, d M Y H:i:s");
                header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
                header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
                header("Last-Modified: {$now} GMT");
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header("Content-Disposition: attachment;filename=$nome");
                header("Content-Transfer-Encoding: binary");

                $spreadsheet = new Spreadsheet();

                $filtri = $_GET['filtri'];
                $letter = 'A';
                $j = 0;


                ///*vedo impostazioni copertina*///
                if (!isset($_GET['uid'])){
                    $userid = $_SESSION['user']['id'];
                }else{
                    $userid = 0;
                }

                $client['Report']->createCopertina($userid, $spreadsheet, $j);


                /*                 * ************************************************************ */

                /**
                 * vedo impostazioni sheet
                 * */
                $j++;
                $spreadsheet->setActiveSheetIndex($j);
                $index = $client['Report']->createSheet($userid, $spreadsheet, $j);
                $i = $client['Report']->setBbbSheet($index, $spreadsheet, $letter, $filtri, $j, $finale);
                $client['Report']->addSettingsExcel($userid, $spreadsheet, $i);


                $writer = new Xlsx($spreadsheet);
                ob_start();
                $writer->save("php://output");
                $xlsData = ob_get_contents();
                ob_end_clean();

                $response = array(
                    'op' => 'ok',
                    'file' => "data:application/vnd.ms-excel;base64," . base64_encode($xlsData)
                );

                die(json_encode($response));
            } else {
                $message = "Nessun log";
                die(json_encode($message));
            }
        }
    }
    else if ($_GET['report'] == "bbb") {
        $valoreCorso = $_GET['course'];
        $key = 'bbbR_' . $gruppo . "_" . $valoreCorso . '_' . $inizio . '_' . $fine;
        if (!isset($_SESSION[$key]) || $_SESSION[$key]['time'] <= strtotime("-20 min")) {


            $bbbName = $client['Report']->getNameAttivitaBBB($db);
            $nameCorsi = $client['Report']->getAllNameCourse($db);

            $array_id = array_values($array_id);

            $res = $client['Report']->getAggregateBBB($db, $inizio, $fine, $valoreCorso, $array_id);
            $result = $client['Report']->constructBbbArray($res, $bbbName, $nameCorsi);

            if (count($result) == 0) {
                echo json_encode($result) . "*?" . count($result);
                exit();
            }

            $finale = $client['Report']->constructSintesiReport($result);


            $_SESSION[$key] = $finale;
            $_SESSION[$key]['time'] = time();
        } else {
            $finale = $_SESSION[$key];
            unset($finale['time']);
        }

        $cont = count($finale);
        if (!isset($_GET['excel'])) {
            die(json_encode($finale) . "*?$cont");
        } else {
            if ($cont != 0) {
                $nome = "report_bbb_$gruppo" . "_" . $data_inizio . "_" . $data_fine . ".xlsx";
                $now = date("D, d M Y H:i:s");
                header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
                header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
                header("Last-Modified: {$now} GMT");
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header("Content-Disposition: attachment;filename=$nome");
                header("Content-Transfer-Encoding: binary");

                $spreadsheet = new Spreadsheet();

                $filtri = $_GET['filtri'];
                $letter = 'A';
                $j = 0;


                ///*vedo impostazioni copertina*///
                if (!isset($_GET['uid'])){
                    $userid = $_SESSION['user']['id'];
                }else{
                    $userid = 0;
                }

                $client['Report']->createCopertina($userid, $spreadsheet, $j);


                /*                 * ************************************************************ */
                /**
                 * vedo impostazioni sheet
                 * */
                $j++;
                $spreadsheet->setActiveSheetIndex($j);
                $index = $client['Report']->createSheet($userid, $spreadsheet, $j);
                $i = $client['Report']->setBbbSheet($index, $spreadsheet, $letter, $filtri, $j, $finale);
                $client['Report']->addSettingsExcel($userid, $spreadsheet, $i);


                $writer = new Xlsx($spreadsheet);
                ob_start();
                $writer->save("php://output");
                $xlsData = ob_get_contents();
                ob_end_clean();

                $response = array(
                    'op' => 'ok',
                    'file' => "data:application/vnd.ms-excel;base64," . base64_encode($xlsData)
                );

                die(json_encode($response));
            } else {
                $message = "Nessun log";
                die(json_encode($message));
            }
        }
    }
    else if ($_GET['report'] == "sbbb") {
        $key = 'sBbb_' . $gruppo . "_" . $inizio . '_' . $fine;
        if (!isset($_SESSION[$key]) || $_SESSION[$key]['time'] <= strtotime("-20 min")) {

            $bbbName = $client['Report']->getNameAttivitaBBB($db);
            $nameCorsi = $client['Report']->getAllNameCourse($db);
            $array_id = array_values($array_id);
            $res    = $client['Report']->getAggregateBBB($db, $inizio, $fine, null, $array_id);
            $result = $client['Report']->constructBbbArray($res, $bbbName, $nameCorsi);

            if (count($result) == 0) {
                echo json_encode($res);
                //echo json_encode($result) . "*?" . count($result);
                exit();
            }

            $finale = $client['Report']->constructSintesiReport($result);
            $totResult = count($finale);
            $tmp = $client['Report']->constructMinCumulatiReport($finale, $totResult);
            $tmp = explode("*", $tmp);
            $new = json_decode($tmp[0], true);
            $array_mesi = json_decode($tmp[1], true);


            $_SESSION[$key] = $new;
            $_SESSION[$key]['mesi'] = $array_mesi;
            $_SESSION[$key]['time'] = time();
        } else {
            $new = $_SESSION[$key];
            $array_mesi = $_SESSION[$key]['mesi'];
            unset($new['mesi']);
            unset($new['time']);
        }

        $total = count($new);
        if (!isset($_GET['excel'])) {
            die(json_encode($new) . "*?$total" . "*?" . json_encode($array_mesi));
        } else {
            if ($total != 0) {
                $nome = "report_sbbb_$gruppo" . "_" . $data_inizio . "_" . $data_fine . ".xlsx";
                $now = date("D, d M Y H:i:s");
                header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
                header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
                header("Last-Modified: {$now} GMT");
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header("Content-Disposition: attachment;filename=$nome");
                header("Content-Transfer-Encoding: binary");

                $spreadsheet = new Spreadsheet();
                $filtri = $_GET['filtri'];
                $letter = "A";
                $j = 0;

                ///*vedo impostazioni copertina*///
                if (!isset($_GET['uid'])){
                    $userid = $_SESSION['user']['id'];
                }else{
                    $userid = 0;
                }

                $client['Report']->createCopertina($userid, $spreadsheet, $j);


                /*                 * ************************************************************ */
                /**
                 * vedo impostazioni sheet
                 * */
                $j++;
                $spreadsheet->setActiveSheetIndex($j);
                $index = $client['Report']->createSheet($userid, $spreadsheet, $j);
                $i = $client['Report']->setSintesiBbbSheet($index, $spreadsheet, $letter, $filtri, $j, $new, $array_mesi);
                $client['Report']->addSettingsExcel($userid, $spreadsheet, $i);

                $writer = new Xlsx($spreadsheet);
                ob_start();
                $writer->save("php://output");
                $xlsData = ob_get_contents();
                ob_end_clean();

                $response = array(
                    'op' => 'ok',
                    'file' => "data:application/vnd.ms-excel;base64," . base64_encode($xlsData)
                );

                die(json_encode($response));
            } else {
                $message = "Nessun log";
                die(json_encode($message));
            }
        }
    }
    else if ($_GET['report'] == "sbbbName") {
        $idCourse = $_GET['course'];
        $key = 'sBbbName_' . $idCourse . "_" . $gruppo . "_" . $inizio . '_' . $fine;
        if (!isset($_SESSION[$key]) || $_SESSION[$key]['time'] <= strtotime("-20 min")) {
            $bbbName = $client['Report']->getNameAttivitaBBB($db);
            $nameCorsi = $client['Report']->getAllNameCourse($db);
            $array_id = array_values($array_id);

            $res = $client['Report']->getAggregateBBB($db, $inizio, $fine, $idCourse, $array_id);
            $result = $client['Report']->constructBbbArray($res, $bbbName, $nameCorsi);

            if (count($result) == 0) {
                echo json_encode($result) . "*?" . count($result);
                exit();
            }

            $finale = $client['Report']->constructSintesiReport($result);
            $totResult = count($finale);
            $tmp = $client['Report']->constructMinCumulatiReport($finale, $totResult);
            $tmp = explode("*", $tmp);
            $new = json_decode($tmp[0], true);
            $array_mesi = json_decode($tmp[1], true);

            $_SESSION[$key] = $new;
            $_SESSION[$key]['mesi'] = $array_mesi;
            $_SESSION[$key]['time'] = time();
        } else {
            $new = $_SESSION[$key];
            $array_mesi = $_SESSION[$key]['mesi'];
            unset($new['mesi']);
            unset($new['time']);
        }

        $total = count($new);
        if (!isset($_GET['excel'])) {
            die(json_encode($new) . "*?$total" . "*?" . json_encode($array_mesi));
        } else {
            if ($total != 0) {
                $nome = "report_sbbb_$gruppo" . "_" . $data_inizio . "_" . $data_fine . ".xlsx";
                $now = date("D, d M Y H:i:s");
                header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
                header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
                header("Last-Modified: {$now} GMT");
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header("Content-Disposition: attachment;filename=$nome");
                header("Content-Transfer-Encoding: binary");

                $spreadsheet = new Spreadsheet();
                $filtri = $_GET['filtri'];
                $letter = "A";
                $j = 0;


                ///*vedo impostazioni copertina*///
                if (!isset($_GET['uid'])){
                    $userid = $_SESSION['user']['id'];
                }else{
                    $userid = 0;
                }

                $client['Report']->createCopertina($userid, $spreadsheet, $j);


                /*                 * ************************************************************ */

                /**
                 * vedo impostazioni sheet
                 * */
                $j++;
                $spreadsheet->setActiveSheetIndex($j);
                $spreadsheet->setActiveSheetIndex($j);
                $index = $client['Report']->createSheet($userid, $spreadsheet, $j);
                $i = $client['Report']->setSintesiBbbSheet($index, $spreadsheet, $letter, $filtri, $j, $new, $array_mesi, "sbbbName");
                $client['Report']->addSettingsExcel($userid, $spreadsheet, $i);


                $writer = new Xlsx($spreadsheet);
                ob_start();
                $writer->save("php://output");
                $xlsData = ob_get_contents();
                ob_end_clean();

                $response = array(
                    'op' => 'ok',
                    'file' => "data:application/vnd.ms-excel;base64," . base64_encode($xlsData)
                );
                die(json_encode($response));
            } else {
                $message = "Nessun log";
                die(json_encode($message));
            }
        }
    }
    else if ($_GET['report'] == "videotimepro"){
        if(!$object){
            echo json_encode(array("error_message" => "Piattaforma non gestita"));
            exit;
        }
        $company_users = $client['Report']->getIdUtentiFromAzienda($db, $gruppo);


        $valoreCorso = $_GET['course'];
        $key = 'videotimepro' . $gruppo . '_' . $valoreCorso . "_" . $inizio . '_' . $fine;

        //TODO Normalizzare la tabella per includere il campo "company_id" o creare tabella aggregata
        $filter                  = array();
        //$filter['company_id']    = 0;
        $filter['company_users'] = $company_users;
        $filter['start_from']    = $inizio;
        $filter['start_to']      = $fine;
        $data                    = $object->getVideoTimeSessions($filter, array('sort' => 'timestarted'));
        if(!$data){
            echo json_encode(array("error_message" => "Nessun risultato trovato" , "filter" => $filter));
            exit();
        }

        $data['company_name'] = $gruppo;

        $aggregate              = $object->getVideoTimeReportData($data);
        $_SESSION[$key]         = $aggregate['array_session_values'];
        $_SESSION[$key]['mesi'] = $aggregate['array_session_dates'];
        $_SESSION[$key]['time'] = time();

        $cont = count($aggregate['array_session_values']);
        if (!isset($_GET['excel'])) {
            echo json_encode($aggregate['array_session_values']) . "*?" . $cont . "*?" . json_encode($aggregate['array_session_dates']);
            //echo json_encode($aggregate['report_data']);
            exit();
        } else {
            if ($cont != 0) {
                $nome = "report_videotimepro_$gruppo" . "_" . $data_inizio . "_" . $data_fine . ".xlsx";
                $now = date("D, d M Y H:i:s");
                header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
                header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
                header("Last-Modified: {$now} GMT");
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header("Content-Disposition: attachment;filename=$nome");
                header("Content-Transfer-Encoding: binary");

                $spreadsheet = new Spreadsheet();
                $filtri = $_GET['filtri'];
                $letter = 'A';
                $j = 0;

                ///*vedo impostazioni copertina*///
                if (!isset($_GET['uid'])){
                    $userid = $_SESSION['user']['id'];
                } else{
                    $userid = 0;
                }

                $client['Report']->createCopertina($userid, $spreadsheet, $j);
                /*                 * ************************************************************ */
                /**
                 * vedo impostazioni sheet
                 * */
                $j++;
                $spreadsheet->setActiveSheetIndex($j);
                $index = $client['Report']->createSheet($userid, $spreadsheet, $j);
                $i     = $client['Report']->setVideoTimeProSheet($index, $spreadsheet, $letter, $filtri, $j, $aggregate);
                $client['Report']->addSettingsExcel($userid, $spreadsheet, $i);

                $writer = new Xlsx($spreadsheet);
                ob_start();
                $writer->save("php://output");
                $xlsData = ob_get_contents();
                ob_end_clean();

                $response = array(
                    'op' => 'ok',
                    'file' => "data:application/vnd.ms-excel;base64," . base64_encode($xlsData)
                );

                die(json_encode($response));
            } else {
                $message = "Nessun log";
                die(json_encode($message));
            }
        }
    }
    else if ($_GET['report'] == "hvp_new"){
        if(!$object){
            echo json_encode(array("error_message" => "Piattaforma non gestita"));
            exit;
        }
        $company_users = $client['Report']->getIdUtentiFromAzienda($db, $gruppo);
        $course_list   = $client['Report']->getAllNameCourse($db);

        $course_id = 0;
        if(isset($_GET['course'])){
            foreach($course_list as $key => $name){
                if($_GET['course'] == $name){
                    $course_id = $key;
                }
            }
        }


        $valoreCorso = $_GET['course'];
        $key = 'hvp_new_' . $gruppo . '_' . $valoreCorso . "_" . $inizio . '_' . $fine;

        //TODO Normalizzare la tabella per includere il campo "company_id" o creare tabella aggregata
        $filter                  = array();
        //$filter['company_id']    = 0;
        $filter['company_users'] = $company_users;
        $filter['start_from']    = $inizio;
        $filter['start_to']      = $fine;
        if($course_id > 0)  $filter['course_id'] = (int) $course_id;
        $data                    = $object->getHVPSessions($filter, array('sort' => 'timecreated'));
        //$data                    = $object->getHVPStoredSessions($filter, array('sort' => 'timecreated'));
        if(!$data){
            echo json_encode(array("error_message" => "Nessun risultato trovato" , "filter" => $filter));
            exit();
        }

        $aggregate              = $object->getHVPReportData($data);
        $_SESSION[$key]         = $aggregate;
        $_SESSION[$key]['time'] = time();
        $cont = 0;
        $cont = count($aggregate);
        if (!isset($_GET['excel'])) {
            echo json_encode($aggregate) . "*?" . $cont;
            exit();
        } else {
            if ($cont != 0) {
                $nome = "report_hvp_new_$gruppo" . "_" . $data_inizio . "_" . $data_fine . ".xlsx";
                $now = date("D, d M Y H:i:s");
                header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
                header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
                header("Last-Modified: {$now} GMT");
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header("Content-Disposition: attachment;filename=$nome");
                header("Content-Transfer-Encoding: binary");

                $spreadsheet = new Spreadsheet();
                $filtri = $_GET['filtri'];
                $letter = 'A';
                $j = 0;

                ///*vedo impostazioni copertina*///
                if (!isset($_GET['uid'])){
                    $userid = $_SESSION['user']['id'];
                } else{
                    $userid = 0;
                }

                $client['Report']->createCopertina($userid, $spreadsheet, $j);
                /*                 * ************************************************************ */
                /**
                 * vedo impostazioni sheet
                 * */
                $j++;
                $spreadsheet->setActiveSheetIndex($j);
                $index = $client['Report']->createSheet($userid, $spreadsheet, $j);
                $i     = $client['Report']->setHvpNewSheet($index, $spreadsheet, $letter, $filtri, $j, $aggregate);
                $client['Report']->addSettingsExcel($userid, $spreadsheet, $i);

                $writer = new Xlsx($spreadsheet);
                ob_start();
                $writer->save("php://output");
                $xlsData = ob_get_contents();
                ob_end_clean();

                $response = array(
                    'op' => 'ok',
                    'file' => "data:application/vnd.ms-excel;base64," . base64_encode($xlsData)
                );

                die(json_encode($response));
            } else {
                $message = "Nessun log";
                die(json_encode($message));
            }
        }
    }
    else if ($_GET['report'] == "shvp_new"){
        if(!$object){
            echo json_encode(array("error_message" => "Piattaforma non gestita"));
            exit;
        }
        $company_users = $client['Report']->getIdUtentiFromAzienda($db, $gruppo);


        $valoreCorso = $_GET['course'];
        $key = 'shvp_new_' . $gruppo . '_' . $valoreCorso . "_" . $inizio . '_' . $fine;

        //TODO Normalizzare la tabella per includere il campo "company_id" o creare tabella aggregata
        $filter                  = array();
        //$filter['company_id']    = 0;
        $filter['company_users'] = $company_users;
        $filter['start_from']    = $inizio;
        $filter['start_to']      = $fine;
        if($object->database == 'fnc'){
            $data                    = $object->getHVPStoredSessions($filter, array('sort' => 'timecreated'));
        }else{
            $data                    = $object->getHVPSessions($filter, array('sort' => 'timecreated'));
        }
        if(!$data){
            echo json_encode(array("error_message" => "Nessun risultato trovato" , "filter" => $filter));
            exit;
        }


        $aggregate              = $object->getSHVPReportData($data);
        $_SESSION[$key]         = $aggregate['array_session_values'];
        $_SESSION[$key]['mesi'] = $aggregate['array_session_dates'];
        $_SESSION[$key]['time'] = time();

        $cont = count($aggregate['array_session_values']);
        if (!isset($_GET['excel'])){
            echo json_encode($aggregate['array_session_values']) . "*?" . $cont . "*?" . json_encode($aggregate['array_session_dates']);
            //echo json_encode($aggregate['session_record']);
            //echo json_encode(array_shift($aggregate['array_session_values']));
            //echo "*?" . json_encode($aggregate['array_session_dates']);
            exit();
        } else {
            if ($cont != 0) {
                $nome = "report_shvp_course_new_$gruppo" . "_" . $data_inizio . "_" . $data_fine . ".xlsx";
                $now = date("D, d M Y H:i:s");
                header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
                header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
                header("Last-Modified: {$now} GMT");
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header("Content-Disposition: attachment;filename=$nome");
                header("Content-Transfer-Encoding: binary");

                $spreadsheet = new Spreadsheet();
                $filtri = $_GET['filtri'];
                $letter = 'A';
                $j = 0;

                ///*vedo impostazioni copertina*///
                if (!isset($_GET['uid'])){
                    $userid = $_SESSION['user']['id'];
                } else{
                    $userid = 0;
                }

                $client['Report']->createCopertina($userid, $spreadsheet, $j);
                /*                 * ************************************************************ */
                /**
                 * vedo impostazioni sheet
                 * */
                $j++;
                $spreadsheet->setActiveSheetIndex($j);
                $index = $client['Report']->createSheet($userid, $spreadsheet, $j);
                $i     = $client['Report']->setTimeinSheetNew($index, $spreadsheet, $letter, $filtri, $j, $aggregate['array_session_values'], $aggregate['array_session_dates'], "shvpName");
                $client['Report']->addSettingsExcel($userid, $spreadsheet, $i);

                $writer = new Xlsx($spreadsheet);
                ob_start();
                $writer->save("php://output");
                $xlsData = ob_get_contents();
                ob_end_clean();

                $response = array(
                    'op' => 'ok',
                    'file' => "data:application/vnd.ms-excel;base64," . base64_encode($xlsData)
                );

                die(json_encode($response));
            } else {
                $message = "Nessun log";
                die(json_encode($message));
            }
        }
    }
    else if ($_GET['report'] == "shvp_course_new"){
        if(!$object){
            echo json_encode(array("error_message" => "Piattaforma non gestita"));
            exit;
        }
        $company_users = $client['Report']->getIdUtentiFromAzienda($db, $gruppo);


        $valoreCorso = $_GET['course'];
        $key = 'shvp_new_' . $gruppo . '_' . $valoreCorso . "_" . $inizio . '_' . $fine;

        //TODO Normalizzare la tabella per includere il campo "company_id" o creare tabella aggregata
        $filter                  = array();
        //$filter['company_id']    = 0;
        $filter['company_users'] = $company_users;
        $filter['course_id']     = (int) $_GET['course'];
        $filter['start_from']    = $inizio;
        $filter['start_to']      = $fine;
        //$data                    = $object->getHVPSessions($filter, array('sort' => 'timecreated'));
        $data                    = $object->getHVPStoredSessions($filter, array('sort' => 'timecreated'));
        if(!$data){
            echo json_encode(array("error_message" => "Nessun risultato trovato" , "filter" => $filter));
            exit();
        }

        $aggregate              = $object->getSHVPReportData($data);
        $_SESSION[$key]         = $aggregate['array_session_values'];
        $_SESSION[$key]['mesi'] = $aggregate['array_session_dates'];
        $_SESSION[$key]['time'] = time();
        $cont = count($aggregate['array_session_values']);
        if (!isset($_GET['excel'])){
            echo json_encode($aggregate['array_session_values']) . "*?" . $cont . "*?" . json_encode($aggregate['array_session_dates']);
            exit();
        } else {
            if ($cont != 0) {
                $nome = "report_shvp_course_new_$gruppo" . "_" . $data_inizio . "_" . $data_fine . ".xlsx";
                $now = date("D, d M Y H:i:s");
                header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
                header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
                header("Last-Modified: {$now} GMT");
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header("Content-Disposition: attachment;filename=$nome");
                header("Content-Transfer-Encoding: binary");

                $spreadsheet = new Spreadsheet();
                $filtri = $_GET['filtri'];
                $letter = 'A';
                $j = 0;

                ///*vedo impostazioni copertina*///
                if (!isset($_GET['uid'])){
                    $userid = $_SESSION['user']['id'];
                } else{
                    $userid = 0;
                }

                $client['Report']->createCopertina($userid, $spreadsheet, $j);
                /*                 * ************************************************************ */
                /**
                 * vedo impostazioni sheet
                 * */
                $j++;
                $spreadsheet->setActiveSheetIndex($j);
                $index = $client['Report']->createSheet($userid, $spreadsheet, $j);
                $i     = $client['Report']->setTimeinSheetNew($index, $spreadsheet, $letter, $filtri, $j, $aggregate['array_session_values'], $aggregate['array_session_dates'], "shvpName");
                $client['Report']->addSettingsExcel($userid, $spreadsheet, $i);

                $writer = new Xlsx($spreadsheet);
                ob_start();
                $writer->save("php://output");
                $xlsData = ob_get_contents();
                ob_end_clean();

                $response = array(
                    'op' => 'ok',
                    'file' => "data:application/vnd.ms-excel;base64," . base64_encode($xlsData)
                );

                die(json_encode($response));
            } else {
                $message = "Nessun log";
                die(json_encode($message));
            }
        }
    }
    else if ($_GET['report'] == "summaryhvp_new"){
        if(!$object){
            echo json_encode(array("error_message" => "Piattaforma non gestita"));
            exit;
        }
        $company_users = $client['Report']->getIdUtentiFromAzienda($db, $gruppo);
        $course_list   = $client['Report']->getAllNameCourse($db);

        $course_id = 0;
        if(isset($_GET['course'])){
            foreach($course_list as $key => $name){
                if($_GET['course'] == $name){
                    $course_id = $key;
                }
            }
        }


        $valoreCorso = $_GET['course'];
        $key = 'hvp_new_' . $gruppo . '_' . $valoreCorso . "_" . $inizio . '_' . $fine;

        //TODO Normalizzare la tabella per includere il campo "company_id" o creare tabella aggregata
        $filter                  = array();
        //$filter['company_id']    = 0;
        $filter['company_users'] = $company_users;
        $filter['start_from']    = $inizio;
        $filter['start_to']      = $fine;
        if($course_id > 0)  $filter['course_id'] = (int) $course_id;
        //$data                    = $object->getHVPSessions($filter, array('sort' => 'timecreated'));
        $data                    = $object->getHVPStoredSessions($filter, array('sort' => 'timecreated'));
        if(!$data){
            echo json_encode(array("error_message" => "Nessun risultato trovato" , "filter" => $filter));
            exit();
        }
        $aggregate              = $object->getSummaryHVPReportData($data);
        $_SESSION[$key]         = $aggregate;
        $_SESSION[$key]['time'] = time();
        $cont = 0;
        $cont = count($aggregate);
        if (!isset($_GET['excel'])) {
            echo json_encode($aggregate) . "*?" . $cont;
            exit();
        } else {
            if ($cont != 0) {
                $nome = "report_hvp_new_$gruppo" . "_" . $data_inizio . "_" . $data_fine . ".xlsx";
                $now = date("D, d M Y H:i:s");
                header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
                header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
                header("Last-Modified: {$now} GMT");
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header("Content-Disposition: attachment;filename=$nome");
                header("Content-Transfer-Encoding: binary");

                $spreadsheet = new Spreadsheet();
                $filtri = $_GET['filtri'];
                $letter = 'A';
                $j = 0;

                ///*vedo impostazioni copertina*///
                if (!isset($_GET['uid'])){
                    $userid = $_SESSION['user']['id'];
                } else{
                    $userid = 0;
                }

                $client['Report']->createCopertina($userid, $spreadsheet, $j);
                /*                 * ************************************************************ */
                /**
                 * vedo impostazioni sheet
                 * */
                $j++;
                $spreadsheet->setActiveSheetIndex($j);
                $index = $client['Report']->createSheet($userid, $spreadsheet, $j);
                $report_data = array();
                $report_data['date_from'] = $data_inizio;
                $report_data['date_to']   = $data_fine;
                $report_data['data']      = $aggregate;

                $i = $client['Report']->setSummaryHvpSheet($index, $spreadsheet, $letter, $filtri, $j, $report_data);
                $client['Report']->addSettingsExcel($userid, $spreadsheet, $i);

                $writer = new Xlsx($spreadsheet);
                ob_start();
                $writer->save("php://output");
                $xlsData = ob_get_contents();
                ob_end_clean();

                $response = array(
                    'op' => 'ok',
                    'file' => "data:application/vnd.ms-excel;base64," . base64_encode($xlsData)
                );

                die(json_encode($response));
            } else {
                $message = "Nessun log";
                die(json_encode($message));
            }
        }
    }


}

function getConfigLog($id, $db = '') {
    $value = "";
    $collection = new MongoDB\Client(
        'mongodb+srv://MoodleUserTest:be9r4hthJtVOG4RW@clustermoodle.oezle.mongodb.net/$db?retryWrites=true&w=majority');
    $collection = $collection->$db->mdl_config_log;
    $cursore = $collection->find(
        ['id' => (int) $id],
        ['projection' => ['name' => 1, 'value' => 1]]
        );
    foreach ($cursore as $singolo) {
        $value = $singolo['name'] . ": " . $singolo['value'];
        return $value;
    }
    return $value;
}

function getNameCourse($id, $collection) {
    $name = "";
    global $db;
    $collection = $collection->$db->mdl_course;
    $cursore = $collection->find(
        ['id' => (int) $id],
        ['projection' => ['fullname' => 1]]
        );
    foreach ($cursore as $singolo) {
        $name = $singolo['fullname'];
        return $name;
    }
    return $name;
}

function getAllNameCourse($collection) {
    $name = array();
    global $db;
    $collection = $collection->$db->mdl_course;
    $cursore = $collection->find(
        ['id' => ['$ne' => 0]],
        ['projection' => ['fullname' => 1, 'id' => 1]]
        );
    foreach ($cursore as $singolo) {
        $name[$singolo['id']] = $singolo['fullname'];
    }
    return $name;
}

function getRole($id, $db = '') {
    $name = "";
    $collection = new MongoDB\Client(
        'mongodb+srv://MoodleUserTest:be9r4hthJtVOG4RW@clustermoodle.oezle.mongodb.net/$db?retryWrites=true&w=majority');
    $collection = $collection->$db->mdl_role_capabilities;
    $cursore = $collection->find(
        ['id' => (int) $id],
        ['projection' => ['capability' => 1]]
        );
    foreach ($cursore as $singolo) {
        $name = $singolo['capability'];
        return $name;
    }
    return $name;
}

/* function getNameAttivitaBBB($id,$collection) {
 $name = "";

 $collection = $collection->$db->mdl_bigbluebuttonbn;
 $cursore = $collection->find(
 ['id' => (int) $id],
 ['projection' => ['name' => 1]]
 );
 foreach ($cursore as $singolo) {

 $name = $singolo['name'];
 return $name;
 }
 return $name;
 } */

function getAdded($idUser, $idCohort) {
    $redis = new Redis();
    $redis->connect('localhost', 6379);
    $a = $redis->get("usersCohort");
    $array = json_decode($a, true);
    foreach ($array as $singolo) {
        if (isset($singolo[$idUser][$idCohort])) {
            return $singolo[$idUser][$idCohort];
        }
    }
}

function getRemoved($idUser, $idGroup) {
    $redis = new Redis();
    $redis->connect('localhost', 6379);
    $a = $redis->get("usersGroup");
    $array = json_decode($a, true);
    foreach ($array as $singolo) {
        if (isset($singolo[$idUser][$idGroup])) {
            return $singolo[$idUser][$idGroup];
        }
    }
}

function compareByTimeStamp($time1, $time2) {
    $time1 = str_replace("/", "-", $time1);
    $time2 = str_replace("/", "-", $time2);

    return strtotime($time1) - strtotime($time2);
}

function getRuoliCorsi($collection) {
    global $db;
    $collection = $collection->$db->corsi_ruoli;
    $cursore = $collection->find(
        [],
        ['projection' => ['userid' => 1, 'ruolo' => 1]]
        );
    $ruoli = array();

    foreach ($cursore as $singolo) {
        $ruolo = "";
        switch ($singolo['ruolo']) {
            case 'student':
                $ruolo = "studente";
                break;
            case 'manager':
                $ruolo = "manager";
                break;
            case 'editingteacher':
                $ruolo = "docente";
                break;
            case 'teacher':
                $ruolo = "controller";
                break;
            default:break;
        }
        $ruoli[$singolo['userid']] = $ruolo;
    }



    return $ruoli;
}

function getTotalSecondsViewed($userid, $inizio, $fine, $collection) {
    global $db;
    $collection = $collection->$db->mdl_logstore_standard_log;
    $pipeline = array(
        array(
            '$match' => array(
                'userid' => $userid,
                'component' => 'mod_hvp',
                'action' => 'progress',
                'timecreated' => ['$gte' => $inizio, '$lte' => $fine]
            )
        ),
        array(
            '$project' => array(
                'objectid' => 1,
                'courseid' => 1,
                'userid' => 1,
                'other' => array(
                    '$split' => array(
                        '$other',
                        '{"progress":'
                    )
                )
            )
        ),
        array(
            '$project' => array(
                'objectid' => 1,
                'courseid' => 1,
                'userid' => 1,
                'other' => array(
                    '$split' => array(
                        array(
                            '$arrayElemAt' => array(
                                '$other',
                                1
                            )
                        ),
                        ','
                    )
                )
            )
        ),
        array(
            '$project' => array(
                'objectid' => 1,
                'courseid' => 1,
                'userid' => 1,
                'other' => array(
                    '$split' => array(
                        array(
                            '$arrayElemAt' => array(
                                '$other',
                                0
                            )
                        ),
                        '.'
                    )
                )
            )
        ),
        array(
            '$project' => array(
                'objectid' => 1,
                'courseid' => 1,
                'userid' => 1,
                'other' => array(
                    '$arrayElemAt' => array(
                        '$other',
                        0
                    )
                )
            )
        ),
        [
            '$project' => [
                'objectid' => 1,
                'courseid' => 1,
                'userid' => 1,
                'other' => [
                    '$trim' => [
                        'input' => '$other'
                    ]
                ]
            ]
        ],
        [
            '$project' => [
                'objectid' => 1,
                'courseid' => 1,
                'userid' => 1,
                'other' => [
                    '$convert' => [
                        'input' => '$other',
                        'to' => 'int',
                        'onError' => 0
                    ]
                ]
            ]
        ],
        [
            '$group' => [
                '_id' => [
                    'objectid' => '$objectid',
                    'userid' => '$userid'
                ],
                'max' => [
                    '$max' => '$other'
                ]
            ]
        ],
        array(
            '$group' => array(
                '_id' => null,
                'totalSeconds' => array(
                    '$sum' => '$max'
                )
            )
        )
    );
    $out = $collection->aggregate($pipeline);
    foreach ($out as $singolo) {
        return $singolo['totalSeconds'];
    }
}

function getTotalCorso($collection) {
    global $db;
    $collection = $collection->$db->mdl_logstore_standard_log;
    $pipeline2 = [
        ['$match' => [
            'component' => 'mod_hvp'
        ]
        ], [
            '$group' => [
                '_id' => [
                    'courseid' => '$courseid',
                    'userid' => '$userid'
                ]
            ]
        ],
        ['$lookup' => [
            'from' => 'secondsCourse',
            'localField' => '_id.courseid',
            'foreignField' => '_id',
            'as' => 'corsi'
        ]
        ], ['$project' => [
            '_id' => 0,
            'userid' => '$_id.userid',
            'courseid' => '$_id.courseid',
            'total' => '$corsi.totalSeconds'
        ]
        ], ['$unwind' => [
            'path' => '$total'
        ]
        ], ['$group' => [
            '_id' => '$userid',
            'somma' => [
                '$sum' => '$total'
            ]
        ]
        ]
    ];

    $out2 = $collection->aggregate($pipeline2);

//    var_dump($out2); exit;

    $tmp = array();
    foreach ($out2 as $singolo) {

        $tmp[$singolo['_id']] = $singolo['somma'];
    }

    return $tmp;
}

function convertSeconds($seconds) {
    $hours = floor($seconds / 3600);
    $mins = floor($seconds / 60 % 60);
    $secs = floor($seconds % 60);
    $timeFormat = sprintf('%02d:%02d:%02d', $hours, $mins, $secs);
    return $timeFormat;
}

function slugify($text)
{
    // replace non letter or digits by -
    $text = preg_replace('~[^\pL\d]+~u', '-', $text);
    // transliterate
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
    // remove unwanted characters
    $text = preg_replace('~[^-\w]+~', '', $text);
    // trim
    $text = trim($text, '-');
    // remove duplicate -
    $text = preg_replace('~-+~', '-', $text);
    // lowercase
    $text = strtolower($text);
    if (empty($text)) {
        return 'n-a';
    }
    return $text;
}

exit();
