<?php  // Moodle configuration file

unset($CFG);
global $CFG;
$CFG = new stdClass();

$CFG->dbtype    = 'mysqli';
$CFG->dblibrary = 'native';
$CFG->dbhost    = 'localhost';
// $CFG->dbname    = 'timevision_moodle';
// $CFG->dbname    = 'live_moodle';
// $CFG->dbname    = 'formazione40_moodle';
$CFG->dbname    = 'fnc_moodle';
$CFG->dbuser    = 'root';
$CFG->dbpass    = '';
$CFG->prefix    = 'mdl_';

// $CFG->dbtype    = 'mysqli';
// $CFG->dblibrary = 'native';
// $CFG->dbhost    = '95.141.47.197';
// $CFG->dbname    = 'moodle_formazione40';
// $CFG->dbuser    = 'forma40';
// $CFG->dbpass    = 'rD&6jq97';
// $CFG->prefix    = 'mdl_';

$CFG->dboptions = array (
  'dbpersist' => 0,
  'dbport' => '',
  'dbsocket' => '',
  'dbcollation' => 'utf8_general_ci',
);

$CFG->wwwroot   = 'https://update.corsinrete.com';
$CFG->dataroot  = '/var/www/vhosts/update.corsinrete.com/moodledata';
$CFG->admin     = 'admin';


$CFG->directorypermissions = 0777;
//$CFG->sessiontimeout = 28800; //TOLTO IL 29/12/2020 // Aggiunta manualmente il 07/04/2020 per aumento timeout di sessione a 8 ore (28800 sec)
// require_once(__DIR__ . '/lib/setup.php');

// There is no php closing tag in this file,
// it is intentional because it prevents trailing whitespace problems!
