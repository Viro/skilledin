<?php
//Controllo sessione
session_start();
if (!isset($_SESSION['user']) || ($_SESSION['user']['idRuolo'] != 0 && $_SESSION['user']['idRuolo'] != 1 && $_SESSION['user']['idRuolo'] != 4)) {
    exit;
}

//Classe Skilledin
$abs_path = '/home/admin/public_html';
if(isset($_SERVER['DOCUMENT_ROOT']) && strlen(trim($_SERVER['DOCUMENT_ROOT']))) $abs_path = $_SERVER['DOCUMENT_ROOT'];

require_once($abs_path . "/skilledin/report/obj/Skilledin.php");
require_once($abs_path . "/skilledin/report/obj/SkilledinWeb.php");
require_once($abs_path . "/skilledin/report/obj/Report.php");
require_once($abs_path . "/skilledin/report/obj/Admin.php");
require_once($abs_path . "/skilledin/report/obj/SkilledinReport.php");
require_once($abs_path . "/skilledin/report/obj/Formazione.php");
require_once($abs_path . "/skilledin/report/obj/Formazione40.php");
require_once($abs_path . "/skilledin/report/obj/FondoNuoveCompetenze.php");
require_once($abs_path . "/skilledin/report/obj/Live.php");
require_once($abs_path . "/skilledin/report/obj/PlatformNew.php");
require_once($abs_path . "/skilledin/report/obj/Utility.php");

//Response
if(isset($_POST['action'])){
    $object    = new Admin('admin');

    if($_POST['action'] == 'getHtmlSelectPlatforms') {
        //init
        $response = array();
        $response[] = array('id' => '', 'text' => 'Seleziona piattaforma');

        //func
        $platforms = $object->getPlatforms();
        if ($platforms) {
            foreach ($platforms as $platform) {
                $response[] = array('id' => $platform['id'], 'text' => $platform['nome']);
            }
        }

        //return
        echo json_encode($response);
    }
}
?>