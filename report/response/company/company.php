<?php
//Controllo sessione
session_start();
if (!isset($_SESSION['user']) || ($_SESSION['user']['idRuolo'] != 0 && $_SESSION['user']['idRuolo'] != 1 && $_SESSION['user']['idRuolo'] != 4)) {
    exit;
}

//Classe Skilledin
$abs_path = '/home/admin/public_html';
if(isset($_SERVER['DOCUMENT_ROOT']) && strlen(trim($_SERVER['DOCUMENT_ROOT']))) $abs_path = $_SERVER['DOCUMENT_ROOT'];

require_once($abs_path . "/skilledin/report/obj/Skilledin.php");
require_once($abs_path . "/skilledin/report/obj/SkilledinWeb.php");
require_once($abs_path . "/skilledin/report/obj/Report.php");
require_once($abs_path . "/skilledin/report/obj/Admin.php");
require_once($abs_path . "/skilledin/report/obj/SkilledinReport.php");
require_once($abs_path . "/skilledin/report/obj/Formazione.php");
require_once($abs_path . "/skilledin/report/obj/Formazione40.php");
require_once($abs_path . "/skilledin/report/obj/FondoNuoveCompetenze.php");
require_once($abs_path . "/skilledin/report/obj/Live.php");
require_once($abs_path . "/skilledin/report/obj/PlatformNew.php");
require_once($abs_path . "/skilledin/report/obj/Utility.php");

//Response
if(isset($_POST['action'])){
    $skilledin = new Skilledin();
    if($_POST['action'] == 'getHtmlSelectCompanies') {
        $response = array(array('id' => '', 'text' => 'Tutte le aziende'));
        $companies = $skilledin->getCompaniesOLD(array('platform_id' => $_POST['platform_id']));
        if ($companies) {
            foreach ($companies as $company) {
                $response[] = array('id' => $company, 'text' => $company);
            }
            echo json_encode($response);
        }
    }
    if($_POST['action'] == 'getHtmlSelectCompaniesAggregated') {
        $object    = new Admin('admin');
        $companies = $object->getCompaniesFromAggregation(array(), array('sort' => 'company_name'));
        if ($companies) {
            $response  = array(array('id' => '', 'text' => 'Seleziona azienda'));
            foreach ($companies as $company) {
                $response[] = array('id' => $company, 'text' => $company);
            }
            echo json_encode($response);
        }else{
            echo json_encode(array(array('id' => '', 'text' => 'Nessuna azienda trovata')));
        }
    }
    if($_POST['action'] == 'getHtmlSelectUserCompany'){
        $response = array(array('id' => 0, 'text' => 'Tutti gli utenti'));
        $users = $skilledin->getCompanyUsersOLD(array('platform_id' => $_POST['platform_id'], 'company_name' => $_POST['company_name']));
        if ($users) {
            foreach ($users as $user) {
                $response[] = array('id' => $user['id'], 'text' => $user['id'] . ' - ' . $user['fullname']);
            }
            echo json_encode($response);
        }
    }
    if($_POST['action'] == 'getHtmlSelectPlanCompany'){
        $platform_id = $_POST['platform_id'];
        //Estrazione dei dati
        $object = new Admin('admin');
        $response    = array();
        $response[0] = array('id' => '0', 'text' => 'Tutti i piani');
        $company_plans = $object->getAziendePianiFormativi(array('nome_azienda' => $_POST['company_name']));
        if ($company_plans) {
            foreach ($company_plans as $company_plan) {
                $response[$company_plan['skilledin_plan_id']] = array('id'   => $company_plan['skilledin_plan_id'],
                                                                      'text' => $company_plan['nome_piano']        );
            }
        }
        $output = array();
        foreach ($response as $item){
            $output[] = $item;
        }
        echo json_encode($output);
    }

    if($_POST['action'] == 'addCourseToPianoAzienda'){
        var_dump($_POST);

        $object    = new Admin('admin');

        //Definizione di object
        $subobject = false;
        if($_POST['platform_id'] == 5) $subobject = new Formazione('formaz');
        if($_POST['platform_id'] == 1) $subobject = new Formazione40('f40');
        if($_POST['platform_id'] == 0) $subobject = new FondoNuoveCompetenze('fnc');
        if($_POST['platform_id'] == 2) $subobject = new Live('live');
        if($_POST['platform_id'] == 3) $subobject = new PlatformNew('new');

        //Definizione di web object
        if($_POST['platform_id'] == 0) $platform_name = 'fnc';
        if($_POST['platform_id'] == 1) $platform_name = 'f40';
        if($_POST['platform_id'] == 2) $platform_name = 'live';
        if($_POST['platform_id'] == 3) $platform_name = 'new';
        if($_POST['platform_id'] == 5) $platform_name = 'formaz';
        $skilledin_web = new SkilledinWeb($platform_name);
        $company_plan_data = $object->getAziendaPianoFormativo($_POST['piano_azienda_id']);
        $course_data       = $subobject->getCourse($_POST['course_id']);
        $durata_corso = 0;
        $filter    = array();
        $filter['course_id']   = $_POST['course_id'];
        $course_modules = $subobject->getHVPCoursesModules($filter, array('sort' => 'id'));
        $total_time = 0;
        if($course_modules){
            foreach($course_modules as $course_module){
                $course_module['object_time'] = '00:00:00';
                $split = isset($course_module['json_content']) ? explode('"time":', $course_module['json_content']) : array();
                if (isset($split[1])) {
                    $split = explode(",", $split[1]);
                    $split = explode(".", $split[0]);
                    $course_module['object_time'] = $split[0];
                }
                $durata_corso+= $course_module['object_time'];
            }
        }else{
            echo 'non è un corso HVP';
        }
        $durata_corso = $object->tool->convertSecondsToTime($durata_corso);

        //Verifica esistenza nel piano aziendale mysql relativo al corso inserito in riferimento alla piattaforma derivante
        $piano_azienda = $skilledin_web->getAziendePianiCorsiBy('skilledin_plan_id', $_POST['piano_azienda_id']);
        if(!$piano_azienda){
            echo 'Devo creare il piano azienda';
            //creazione del piano aziendale nel db di piattaforma
            $company_plan_data['id'] = $skilledin_web->addAziendePianiFormativi($company_plan_data);
            if($company_plan_data['id'] >0) $subobject->addAziendePianiFormativi($company_plan_data);
            $piano_azienda = $skilledin_web->getAziendePianiCorsiBy('skilledin_plan_id', $_POST['piano_azienda_id']);
        }
        if($piano_azienda){
            //Creazione del piano aziendale corso nel mysql relativo al corso e al mysql generico
            $data = array();
            $data['id_aziende_piani'] = (int) $piano_azienda['id'];
            $data['azienda']          = $company_plan_data['nome_azienda'];
            $data['id_corso']         = (int) $_POST['course_id'];
            $data['nome_corso']       = $course_data['fullname'];
            $data['durata_corso']     = $durata_corso;

            var_dump($data);

            $data['id'] = $skilledin_web->addAziendePianiCorsi($data);
            if($data['id'] >0){
                $object->addAziendaPianoFormativoCorso($data);
                $subobject->addAziendaPianoFormativoCorso($data);
            }
        }
    }
    if($_POST['action'] == 'deleteCourseToPianoAzienda'){
        $object    = new Admin('admin');

        //Definizione di subobject
        $subobject = false;
        if($_POST['platform_id'] == 5) $subobject = new Formazione('formaz');
        if($_POST['platform_id'] == 1) $subobject = new Formazione40('f40');
        if($_POST['platform_id'] == 0) $subobject = new FondoNuoveCompetenze('fnc');
        if($_POST['platform_id'] == 2) $subobject = new Live('live');
        if($_POST['platform_id'] == 3) $subobject = new PlatformNew('new');

        //Definizione di web object
        if($_POST['platform_id'] == 0) $platform_name = 'fnc';
        if($_POST['platform_id'] == 1) $platform_name = 'f40';
        if($_POST['platform_id'] == 2) $platform_name = 'live';
        if($_POST['platform_id'] == 3) $platform_name = 'new';
        if($_POST['platform_id'] == 5) $platform_name = 'formaz';
        $skilledin_web = new SkilledinWeb($platform_name);

        $company_plan_data = $subobject->getAziendaPianoFormativo($_POST['skilledin_plan_id']);

        if($company_plan_data){
            echo 'ho trovato il piano';
            $filter                     = array();
            $filter['piano_azienda_id'] = $company_plan_data['id'];
            if(isset($_POST['course_id'])){
                $filter['id_corso'] = $_POST['course_id'];
//                var_dump($filter);
                $piano_corso = $skilledin_web->getAziendePianoFormativoCorsiByPlanCourse($filter['piano_azienda_id'], $filter['id_corso']);
                if($piano_corso){
                    echo 'ho trovato il corso per il piano';
                    if($skilledin_web->deleteAziendePianiCorsi($piano_corso['id']) > 0){
                        $object->deleteAziendaPianoFormativoCorso($filter);
                        $subobject->deleteAziendaPianoFormativoCorso($filter);
                        echo 'ho eliminato il piano';
                    }
                }
            }
        }
    }
    if($_POST['action'] == 'reloadPianoAziendaCorsiAssociati') {
        $object                = new Admin('admin');
        $platforms             = $object->getPlatforms();
        $total_course_duration = 0;
        foreach ($platforms as $platform) {
            //Definizione di object
            $subobject = false;
            if($platform['id'] == 5) $subobject = new Formazione('formaz');
            if($platform['id'] == 1) $subobject = new Formazione40('f40');
            if($platform['id'] == 0) $subobject = new FondoNuoveCompetenze('fnc');
            if($platform['id'] == 2) $subobject = new Live('live');
            if($platform['id'] == 3) $subobject = new PlatformNew('new');
            if(!$subobject) continue;

            $company_plan_data = $subobject->getAziendaPianoFormativo($_POST['skilledin_plan_id']);
            if($company_plan_data){
                $company_plan_courses = $subobject->getAziendePianiFormativiCorsi(array('id_aziende_piani' => $company_plan_data['id']));
                if($company_plan_courses){
                    foreach ($company_plan_courses as $company_plan_course){
                        if(!isset($company_plan_course['id'])) continue;
                        echo '<tr>';
                        echo '<td>' . $platform['nome']                    . '</td>';
                        echo '<td>' . $company_plan_course['id_corso']     . '</td>';
                        echo '<td>' . $company_plan_course['nome_corso']   . '</td>';
                        echo '<td>' . $company_plan_course['durata_corso'] . '</td>';
                        echo '<td>';
                        echo '<button type="submit" class="btn btn-danger" title="Elimina corso" onclick="javascript:deleteCourseToPianoAzienda(' . $platform['id'] .', \'' . $_POST['skilledin_plan_id'] . '\', ' . $company_plan_course['id_corso'] .')">';
                        echo '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
                                                          <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6Z"/>
                                                            <path d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1ZM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118ZM2.5 3h11V2h-11v1Z"/>
                                                        </svg>';
                        echo '</button>';
                        echo '</td>';
                        echo '</tr>';
                        $total_course_duration += $object->tool->convertTimeToSeconds($company_plan_course['durata_corso']);
                    }
                }
            }
        }
        if($total_course_duration > 0){
            echo '<tr>';
            echo '<td colspan="3" class="text-right"><strong>Durata totale</strong></td>';
            echo '<td>' . $object->tool->convertSecondsToTime($total_course_duration) . '</td>';
            echo '<td></td>';
            echo '</tr>';
        }else{
            echo '<tr>';
            echo '<td colspan="5" class="text-center"><strong>Nessun corso associato</strong></td>';
            echo '</tr>';
        }
    }
}
?>
