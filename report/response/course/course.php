<?php
//Controllo sessione
session_start();
if (!isset($_SESSION['user']) || ($_SESSION['user']['idRuolo'] != 0 && $_SESSION['user']['idRuolo'] != 1 && $_SESSION['user']['idRuolo'] != 4)) {
    exit;
}

//Classe Skilledin
$abs_path = '/home/admin/public_html';
if(isset($_SERVER['DOCUMENT_ROOT']) && strlen(trim($_SERVER['DOCUMENT_ROOT']))) $abs_path = $_SERVER['DOCUMENT_ROOT'];

require_once($abs_path . "/skilledin/report/obj/Skilledin.php");
require_once($abs_path . "/skilledin/report/obj/SkilledinWeb.php");
require_once($abs_path . "/skilledin/report/obj/Report.php");
require_once($abs_path . "/skilledin/report/obj/Admin.php");
require_once($abs_path . "/skilledin/report/obj/SkilledinReport.php");
require_once($abs_path . "/skilledin/report/obj/Formazione.php");
require_once($abs_path . "/skilledin/report/obj/Formazione40.php");
require_once($abs_path . "/skilledin/report/obj/FondoNuoveCompetenze.php");
require_once($abs_path . "/skilledin/report/obj/Live.php");
require_once($abs_path . "/skilledin/report/obj/PlatformNew.php");
require_once($abs_path . "/skilledin/report/obj/Utility.php");

//Response
if(isset($_POST['action'])){
    $response = array();
    if($_POST['action'] == 'getHtmlSelectCourses'){
        $excluded_course = array();
        if(isset($_POST['piano_formativo_id'])){
            //Definizione di object
            $object = false;
            if($_POST['platform_id'] == 5) $object = new Formazione('formaz');
            if($_POST['platform_id'] == 1) $object = new Formazione40('f40');
            if($_POST['platform_id'] == 0) $object = new FondoNuoveCompetenze('fnc');
            if($_POST['platform_id'] == 2) $object = new Live('live');
            if($_POST['platform_id'] == 3) $object = new PlatformNew('new');
            $company_plan_courses = $object->getAziendePianiFormativiCorsi(array('id_aziende_piani' => $_POST['piano_formativo_id']));
            if($company_plan_courses){
                foreach($company_plan_courses as $company_plan_course){
                    $excluded_course[] = $company_plan_course['id_corso'];
                }
            }
        }
        $response = array(array('id' => 0, 'text' => 'Tutti i corsi'));

        $skilledin = new Skilledin();
        $filter = array();
        $filter['platform_id'] = $_POST['platform_id'];
        $courses = $skilledin->getCoursesOLD($filter);
        if($courses){
            foreach($courses as $course){
                if(in_array($course['id'], $excluded_course)) continue;
                $response[] = array('id' => $course['id'], 'text' => $course['id'] . ' - ' . $course['fullname']);
            }
        }
        echo json_encode($response);
    }
    if($_POST['action'] == 'getHtmlSelectCourseModules'){
        $response = array(array('id' => 0, 'text' => 'Tutte le lezioni'));

        $skilledin = new Skilledin();
        $filter = array();
        $filter['platform_id'] = $_POST['platform_id'];
        $filter['course_id']   = $_POST['course_id'];
        $course_modules = $skilledin->getCoursesModulesHVPOLD($filter, array('sort' => 'id'));
        if($course_modules){
            foreach($course_modules as $course_module){
                $response[] = array('id' => $course_module['id'], 'text' => $course_module['id'] . ' - ' . $course_module['name']);
            }
        }
        echo json_encode($response);
    }

    if($_POST['action'] == 'showCourseInfo'){
        $skilledin = new Skilledin();
        $tool      = new Utility();

        $filter    = array();
        $filter['platform_id'] = $_POST['platform_id'];
        $filter['course_id']   = $_POST['course_id'];
        $course_modules = $skilledin->getCoursesModulesHVPOLD($filter, array('sort' => 'id'));

        $total_time  = 0;
        $total_count = 0;

        echo '<tr>';
        echo '<th>Lezione</th>';
        echo '<th>Durata</th>';
        echo '</tr>';
        if($course_modules){
            foreach($course_modules as $course_module){
                $course_module['object_time'] = '00:00:00';
                $split = isset($course_module['json_content']) ? explode('"time":', $course_module['json_content']) : array();
                if (isset($split[1])) {
                    $split = explode(",", $split[1]);
                    $split = explode(".", $split[0]);
                    $course_module['object_time'] = $split[0];
                }
                echo '<tr>';
                echo '<td>' . $course_module['name']                                     . '</td>';
                echo '<td>' . $tool->convertSecondsToTime($course_module['object_time']) . '</td>';
                echo '</tr>';

                $total_count++;
                $total_time+= $course_module['object_time'];
            }
        }
        echo '<tr>';
        echo '<td>TOTALE (' . $total_count . ' lezioni)</td>';
        echo '<td>' . $tool->convertSecondsToTime($total_time) . '</td>';
        echo '</tr>';
        echo '<tr>';
        echo '<td class="text-center">';
        echo '<form action="https://update.corsinrete.com/skilledin/report/custom/script/course_module_check.php" method="POST" target="_blank">';
        echo '<input type="hidden" name="platform_id" value="' . $_POST['platform_id'] . '" />';
        echo '<input type="hidden" name="course_id"   value="' . $_POST['course_id']   . '" />';
        echo '<button class="btn btn-primary" type="sumbit" name="action" value="check_course">Verifica corso</button>';
        echo '</form>';
        echo '</td>';

        echo '</tr>';
    }

    if($_POST['action'] == 'syncCourseInfo'){
        $skilledin = new Skilledin();
        $tool      = new Utility();

        if($_POST['platform_id'] == 0) $platform_name = 'fnc';
        if($_POST['platform_id'] == 1) $platform_name = 'f40';
        if($_POST['platform_id'] == 2) $platform_name = 'live';
        if($_POST['platform_id'] == 3) $platform_name = 'new';
        if($_POST['platform_id'] == 5) $platform_name = 'formaz';

        $skilledin_web = new SkilledinWeb($platform_name);
//        var_dump($platform_name); exit;
        $course = $skilledin_web->getCourse($_POST['course_id']);

        $filter    = array();
        $filter['platform_id'] = $_POST['platform_id'];
        $filter['course_id']   = $_POST['course_id'];
        $course_modules = $skilledin->syncCourse($filter, $course);

        echo $course_modules;


    }



}
?>
