<?php
//Controllo sessione
session_start();
if (!isset($_SESSION['user']) || ($_SESSION['user']['idRuolo'] != 0 && $_SESSION['user']['idRuolo'] != 1)) {
    exit;
}

//Classe Skilledin
require_once($_SERVER['DOCUMENT_ROOT'] . "/skilledin/report/obj/Skilledin.php");

//Response
if(isset($_POST['action'])){
    $response = array();
    if($_POST['action'] == 'getUsers'){
        $skilledin = new Skilledin();
        $users    = $skilledin->getUsers();
        if($users){
            foreach ($users as $user){
                if(is_null($user['email']))  $user['email']  = '';
                if(is_null($user['ruolo']))  $user['ruolo']  = '';
                if(is_null($user['data']))   $user['data']   = '';
                if(is_null($user['orario'])) $user['orario'] = '';

                if($user['id'] == 0) continue;
                $response[] = array('id'    => $user['id']    ,
                                    'email' => $user['email'] ,
                                    'role'  => $user['ruolo'] ,
                                    'date'  => $user['data']  ,
                                    'time'  => $user['orario'] );
           }
       }
       echo json_encode($response);
    }
}
?>