<?php
session_start();
if (!isset($_SESSION['user']) || ($_SESSION['user']['idRuolo'] != 0 && $_SESSION['user']['idRuolo'] != 1 && $_SESSION['user']['idRuolo'] != 4)) {
    header("location:index.php");
}
require_once($_SERVER['DOCUMENT_ROOT'] . "/skilledin/report/obj/Skilledin.php");
$skilledin = new Skilledin();
$platforms = $skilledin->getPlatforms();
?>
<!DOCTYPE html>
<html>
<head>
    <title>Skilledin - RESET HVP</title>
    <meta charset="utf-8">

    <!-- Bootstrap CSS -->
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->

    <!-- Bootstrap 5 JS-->
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js"
            integrity="sha384-SR1sx49pcuLnqZUnnPwx6FCym0wLsk5JZuNx2bPPENzswTNFaQU1RDvt3wT4gWFG"
            crossorigin="anonymous"></script>
    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4"
            crossorigin="anonymous"></script>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap4.min.css">
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap4.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/2.1.0/css/buttons.dataTables.min.css">
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/2.1.0/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/2.1.0/js/buttons.colVis.min.js"></script>


    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/js-cookie@3.0.1/dist/js.cookie.min.js"></script>
    <script src="../js/colors.js"></script>
    <script src="../js/skilledin.js"></script>
    <style>
        body {
            margin-top: 40px;
            font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
        }
        #calendar {
            max-width: 800px;
            margin: 0 auto;
        }
        .select2-container {
            width: 100% !important;
            padding: 0;
        }
        ul.no-bullets {
            list-style-type: none; /* Remove bullets */
            padding: 0; /* Remove padding */
            margin: 0; /* Remove margins */
        }
        .dt-button-collection button.buttons-columnVisibility:before,
        .dt-button-collection button.buttons-columnVisibility.active span:before {
            display:block;
            position:absolute;
            top:1.2em;
            left:0;
            width:12px;
            height:12px;
            box-sizing:border-box;
        }

        .dt-button-collection button.buttons-columnVisibility:before {
            content:' ';
            margin-top:-6px;
            margin-left:10px;
            border:1px solid black;
            border-radius:3px;
        }
        .dt-button-collection button.buttons-columnVisibility.active span:before {
            content:'\2714';
            margin-top:-11px;
            margin-left:12px;
            text-align:center;
            text-shadow:1px 1px #DDD, -1px -1px #DDD, 1px -1px #DDD, -1px 1px #DDD;
        }
        .dt-button-collection button.buttons-columnVisibility span {
            margin-left:20px;
        }
    </style>

</head>

<body>
<div class="container">
    <?php if ($_SESSION['user']['idRuolo'] == 0 || $_SESSION['user']['idRuolo'] == 1 || $_SESSION['user']['idRuolo'] == 4) { ?>
        <a href="confirmPage.php?admin" class="previous" style="text-decoration: none;">‹ Torna indietro</a>
    <?php } ?>
    <?php if(isset($_POST['action'])){ ?>
        <?php if($_POST['action'] == 'reset_hvp'){ ?>
            <div class="row my-2" >
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">Esito reset</div>
                        <div class="card-body">
                            <?php
                                $selected_platform = false;
                                foreach($platforms as $platform){
                                    if($platform['id'] == $_POST['platform_id']){
                                        $selected_platform = $platform;
                                    }
                                }


                                $filter = array();
                                if(isset($_POST['platform_id'])  && (int) $_POST['platform_id']          >= 0) $filter['platform_id']  = (int) $_POST['platform_id'];
                                if(isset($_POST['company_name']) && strlen(trim($_POST['company_name']))  > 0) $filter['company_name'] = $_POST['company_name'];
                                if(isset($_POST['user_id'])      && (int) $_POST['user_id']               > 0) $filter['user_id']      = (int) $_POST['user_id'];
                                if(isset($_POST['course_id'])    && (int) $_POST['course_id']             > 0) $filter['course_id']    = (int) $_POST['course_id'];
                                if(isset($_POST['module_id'])    && (int) $_POST['module_id']             > 0) $filter['module_id']    = (int) $_POST['module_id'];
                                if(isset($filter['module_id'])){
                                    $contextinstanceid = $skilledin->getContextIstanceIDFromObjectId($filter['module_id'], $filter['platform_id']);
                                    if($contextinstanceid) $filter['contextinstanceid'] = $contextinstanceid;
                                }
                                $filter['reset']     = ((int) $_POST['reset_value'] == 1) ? false : true;
                                $filter['component'] = 'mod_hvp';

                                $search_filter = $filter;
                                if(isset($search_filter['contextinstanceid'])) unset($search_filter['module_id']);
                                $hvp_data = $skilledin->getDataForHVPReset($search_filter);
                                if($hvp_data){
                                    $data = array();
                                    $data['key']               = 'job_' . date('dmYhisu');
                                    $data['platform_id']       = $selected_platform['id'];
                                    $data['platform_name']     = $selected_platform['nome'];
                                    $data['login_user_id']     = $_SESSION['user']['id'];
                                    $data['company_name']      = (isset($filter['company_name']))      ? $filter['company_name']      : '';
                                    $data['user_id']           = (isset($filter['user_id']))           ? $filter['user_id']           : 0;
                                    $data['course_id']         = (isset($filter['course_id']))         ? $filter['course_id']         : 0;
                                    $data['module_id']         = (isset($filter['module_id']))         ? $filter['module_id']         : 0;
                                    $data['contextinstanceid'] = (isset($filter['contextinstanceid'])) ? $filter['contextinstanceid'] : 0;
                                    $data['reset_value']       = ((int) $_POST['reset_value'] == 1) ? true : false;
                                    $data['reset_to']          = time();
                                    $data['date_scheduled']    = date('Y-m-d H:i:s');
                                    $data['expected_record']   = count($hvp_data);
                                    $data['date_completed']    = 0;
                                    $data['affected_record']   = 0;
                                    $data['status']            = 0;
                                    //Nome corso
                                    if(isset($filter['course_id'])){
                                        $filter2 = array();
                                        $filter2['platform_id'] = $filter['platform_id'];
                                        $filter2['id']          = $filter['course_id'];
                                        $courses = $skilledin->getCoursesOLD($filter2);
                                        if($courses){
                                            $course = array_shift($courses);
                                            $data['course_name'] = $course['fullname'];
                                        }
                                    }else{
                                        $data['course_name'] = 'Tutti i corsi';
                                    }
                                    if(isset($filter['module_id'])){
                                        $filter2 = array();
                                        $filter2['platform_id'] = $filter['platform_id'];
                                        $filter2['course_id']   = $filter['course_id'];
                                        $filter2['module_id']   = $filter['module_id'];
                                        $course_modules = $skilledin->getCoursesModulesHVPOLD($filter2);
                                        if($course_modules){
                                            $course_module = array_shift($course_modules);
                                            $data['module_name'] = $course_module['name'];
                                        }
                                    }else{
                                        $data['module_name'] = 'Tutti i moduli';
                                    }

                                    if(isset($filter['user_id'])) {
                                        $filter2 = array();
                                        $filter2['platform_id'] = $filter['platform_id'];
                                        $filter2['user_id']     = $filter['user_id'];
                                        $company_users = $skilledin->getCompanyUserOLD($filter2);
                                        if($company_users){
                                            $company_user = array_shift($company_users);
                                            $data['user_name'] = $company_user['nome'];
                                        }
                                    }else{
                                        $data['user_name'] = 'Tutti i corsisti';
                                    }

                                    $insert = $skilledin->scheduleResetLogHVP($data, $filter['platform_id']);
                                    echo date('d/m/Y H:i:s') . ' - Schedulati  correttamente ' . count($hvp_data) . ' record per la sincronizzazione';
                                    echo '<script type="text/javascript">window.location = window.location.href;</script>';
                                }else{
                                    echo date('d/m/Y H:i:s') . ' - Non ci sono record schedulabili<br>';
                                    echo 'Filtro usato: <br>';
                                    var_dump($search_filter);
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php }
        elseif($_POST['action'] == 'execute_job'){ ?>
            <script type="text/javascript">window.location = window.location.href;</script>
        <?php
        }
        elseif($_POST['action'] == 'delete_job'){ ?>
            <?php $skilledin->unsetScheduledResetLogHPV($_POST['job_key'], 0, $_POST['platform_id']); ?>
            <script type="text/javascript">window.location = window.location.href;</script>
        <?php
        }
        else{ ?>
            Azione non prevista
            <?php var_dump($_POST); ?>
        <?php } ?>
        <!-- Reload senza POST-->

    <?php }else{ ?>
        <div class="row my-2" >
            <div class="col-12">
               <div class="card">
                    <div class="card-header">Lezioni da resettare</div>
                    <div class="card-body">
                        <form action="" method="POST">
                            <table class="table" id="table_user_list">
                                <tbody>
                                <tr>
                                    <td>Piattaforma</td>
                                    <td>
                                        <select class="form-select" name="platform_id" id="select_platform_id" onchange="javascript:updateCompaniesFromPlatform(this.value); javascript:updateCoursesFromPlatform(this.value)">
                                            <option value="-1" disabled selected>Seleziona piattaforma...</option>
                                            <?php foreach ($platforms as $platform) {
                                                if ($platform['id'] == 4) { ?>
                                                    <option value="<?php echo  $platform['id'] ?>" disabled><?php echo $platform['nome'] ?></option>
                                                <?php } else { ?>
                                                    <option value="<?php echo  $platform['id'] ?>" ><?php echo $platform['nome'] ?></option>
                                                <?php } ?>
                                            <?php } ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Azienda</td>
                                    <td><select class="form-select" name="company_name" id="select_company_name"  onchange="javascript:updateUserFromCompany(this.value, document.getElementById('select_platform_id').value);"></select></td>
                                </tr>
                                <tr>
                                    <td>Utente</td>
                                    <td><select class="form-select" name="user_id" id="select_user_id"></select></td>
                                </tr>
                                <tr>
                                    <td>Corso</td>
                                    <td><select class="form-select" name="course_id" id="select_course_id"   onchange="javascript:updateCoursesMoudleFromCourse(this.value, document.getElementById('select_platform_id').value);"></select></td>
                                </tr>
                                <tr>
                                    <td>Lezione</td>
                                    <td><select class="form-select" name="module_id" id="select_course_module_id"></select></td>
                                </tr>
                                <tr>
                                    <td>Stato lezione</td>
                                    <td>
                                        <select class="form-select" name="reset_value" >
                                            <option value="1">Reset</option>
                                            <!-- <option value="0">Valida (Annulla reset)</option> -->
                                        </select>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <button type="submit" class="btn btn-primary" name="action" value="reset_hvp">Reset</button>
                        </form>
                    </div>
               </div>
            </div>
        </div>
        <div class="row my-2" >
            <div class="col-12">
                <div class="card">
                    <div class="card-header">Elenco dei job schedulati</div>
                    <div class="card-body">
                        <?php
                        $scheduled_jobs = array();
                        foreach ($platforms as $platform) {
                            if($platform['id'] == 4) continue;
                            $scheduled = $skilledin->getScheduledResetLogHPV(array('platform_id'    => $platform['id'],
                                                                                   'date_completed' =>  0             ,
                                                                                   'status'         =>  0              ));

                            if($scheduled && !empty($scheduled)) $scheduled_jobs = array_merge($scheduled_jobs, $scheduled);
                        }
                        ?>


                        <table class="table" id="table_scheduled_job">
                            <thead>
                            <tr>
                                <th>Piattaforma</th>
                                <th>Utente</th>
                                <th>Corso</th>
                                <th>Lezione</th>
                                <th>Azienda</th>
                                <th>Corsista</th>
                                <th>Data</th>
                                <th>Record</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if(!empty($scheduled_jobs)){ ?>
                                <?php foreach ($scheduled_jobs as $scheduled_job) { ?>
                                    <tr>
                                        <td><?php echo $scheduled_job['platform_name'] ?></td>
                                        <td><?php echo $scheduled_job['login_user_id'] ?></td>
                                        <td><?php echo $scheduled_job['course_name'] ?></td>
                                        <td><?php echo $scheduled_job['module_name'] ?></td>
                                        <td><?php echo $scheduled_job['company_name'] ?></td>
                                        <td><?php echo $scheduled_job['user_name'] ?></td>
                                        <td><?php echo date('d/m/Y H:i', strtotime($scheduled_job['date_scheduled'])) ?></td>
                                        <td><?php echo $scheduled_job['expected_record'] ?></td>
                                        <td>
                                            <form action="" method="POST" onsubmit="confirm("Confermare?")" >
                                                <input type="hidden" name="job_key" value="<?php echo $scheduled_job['key'] ?>">
                                                <button type="submit" class="btn btn-primary" title="Esegui Job" name="action" value="execute_job" onclick="window.open('https://update.corsinrete.com/skilledin/report/custom/script/process_reset_hvp.php?platform_id=<?php echo $scheduled_job['platform_id'] ?>&key=<?php echo $scheduled_job['key'] ?>')"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-play-circle" viewBox="0 0 16 16">
                                                        <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                                                        <path d="M6.271 5.055a.5.5 0 0 1 .52.038l3.5 2.5a.5.5 0 0 1 0 .814l-3.5 2.5A.5.5 0 0 1 6 10.5v-5a.5.5 0 0 1 .271-.445z"/>
                                                    </svg></button><br>&nbsp;
                                                <button type="submit" class="btn btn-danger" title="Elimina Job" name="action" value="delete_job"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
                                                        <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6Z"/>
                                                        <path d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1ZM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118ZM2.5 3h11V2h-11v1Z"/>
                                                    </svg></button>
                                            </form>
                                        </td>
                                        <td>
                                            <form action="" method="POST" onsubmit="confirm("Confermare?")" >
                                                <input type="hidden" name="job_key" value="<?php echo $scheduled_job['key'] ?>">
                                                <button type="submit" class="btn btn-primary" title="Esegui Job" name="action" value="execute_job"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-play-circle" viewBox="0 0 16 16">
                                                        <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                                                        <path d="M6.271 5.055a.5.5 0 0 1 .52.038l3.5 2.5a.5.5 0 0 1 0 .814l-3.5 2.5A.5.5 0 0 1 6 10.5v-5a.5.5 0 0 1 .271-.445z"/>
                                                    </svg></button><br>&nbsp;
                                                <button type="submit" class="btn btn-danger" title="Elimina Job" name="action" value="delete_job"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
                                                        <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6Z"/>
                                                        <path d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1ZM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118ZM2.5 3h11V2h-11v1Z"/>
                                                    </svg></button>
                                            </form>
                                        </td>
                                    </tr>
                                <?php } ?>
                            <?php }else{ ?>
                                <tr>
                                    <td colspan="8" class="text-center"><p class="font-weight-bold font-italic">- Nessun job schedulato -</p></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>

</div>
</body>
<script type="text/javascript" charset="utf8">
    $("#select_company_name").select2({placeholder: "Seleziona azienda..."});
    $("#select_user_id").select2({placeholder: "Seleziona utente..."});
    $("#select_course_id").select2({placeholder: "Seleziona corso..."});
    $("#select_course_module_id").select2({placeholder: "Seleziona lezione..."});
</script>


</html>
