#!/usr/bin/php
<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

error_reporting(E_ALL);
require '/home/admin/public_html/vendor/autoload.php';

include("obj/Report.php");

$report = new Report();

$in_corso = $_POST['status'];

$key = 'get-corsi-' . slugify($_POST['company']);

$collection = new MongoDB\Client(
    "mongodb+srv://jobtek:TimeVision2021%40%21%3F@timevision.t9ed5.mongodb.net/TimeVision?retryWrites=true&w=majority");

$collectionCorsiCron = $collection->admin->cron_corsi;

$cursorCron = $collectionCorsiCron->find(['redis_key' => $key]);

foreach ($cursorCron as $cron) {
    $mongo_redis_key_id = $cron['redis_key'];
}

if($mongo_redis_key_id){
    $collectionCorsiCron->updateOne(
        ['redis_key' => $mongo_redis_key_id],
        [
            '$set' => [
                'in_corso' => ($in_corso == 1) ? true : false
            ]
        ],
        ['upsert' => true]
    );
}

function slugify($string){
    return strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $string), '-'));
}

echo "Aggiornato";
