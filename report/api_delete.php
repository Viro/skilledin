#!/usr/bin/php
<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

error_reporting(E_ALL);
require '/home/admin/public_html/vendor/autoload.php';

$key = 'get-corsi-' . slugify($_POST['company']);

$collection = new MongoDB\Client(
    "mongodb+srv://jobtek:TimeVision2021%40%21%3F@timevision.t9ed5.mongodb.net/TimeVision?retryWrites=true&w=majority");

$collectionCorsiCron = $collection->admin->cron_corsi;

$cursorCron = $collectionCorsiCron->find(['redis_key' => $key]);

foreach ($cursorCron as $cron) {
    $mongo_redis_key_id = $cron['redis_key'];
}

if($mongo_redis_key_id){
    $collectionCorsiCron->deleteOne(
        ['redis_key' => $mongo_redis_key_id],
    );
}

function slugify($string){
    return strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $string), '-'));
}

echo "Eliminato";
