<?php
session_start();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

error_reporting(E_ALL);
require '../../vendor/autoload.php';

if (!isset($_SESSION['user']) || ($_SESSION['user']['idRuolo'] != 0 && $_SESSION['user']['idRuolo'] != 1)) {

    header("location:index.php");
}

$collection = new MongoDB\Client(
        "mongodb+srv://jobtek:TimeVision2021%40%21%3F@timevision.t9ed5.mongodb.net/TimeVision?retryWrites=true&w=majority");
$piattaforme = $collection->admin->piattaforme;

$cursor = $piattaforme->find(
        [],
        ['projection' => ['id' => 1, 'nome' => 1]]
);
foreach ($cursor as $singolo) {
    $array_piattaforma[$singolo['id']] = $singolo['nome'];
}

if (isset($_POST['crea'])) {

    $collection = new MongoDB\Client(
            "mongodb+srv://jobtek:TimeVision2021%40%21%3F@timevision.t9ed5.mongodb.net/TimeVision?retryWrites=true&w=majority");
    $collection = $collection->admin->utenti;

    $email = strtolower(trim($_POST['email']));
    $password = trim($_POST['password']);
    $idRuolo = $_POST['role'];

    switch ($idRuolo) {
        case 1:
            $nomeRuolo = "manager";
            break;
        case 2:
            $nomeRuolo = "utente/personale interno";
            break;
        case 3:
            $nomeRuolo = "azienda/ente";
            break;
        default : break;
    }
    $password = md5($password);
    $piattaforma = join(",", $_POST['platform']);

    $cursor = $collection->find(
            [],
            ['projection' => ['id' => 1],
                'limit' => 1,
                'sort' => ['id' => -1],
            ]
    );
    $lastId = 0;
    foreach ($cursor as $singolo) {
        $lastId = $singolo['id'] + 1;
    }

    $data = date("Y-m-d");
    $orario = date("H:i:s");

    $collection->insertOne([
        'id' => (int) $lastId,
        'email' => $email,
        'password' => $password,
        'ruolo' => $nomeRuolo,
        'idRuolo' => (int) $idRuolo,
        'idPiattaforma' => $piattaforma,
        'data' => $data,
        'orario' => $orario
    ]);


    header("location: confirmPage.php");
}
?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css">

        <title>Crea utenza</title>

        <style>
            .field-icon {
                float: right;
                margin-left: -35px;
                margin-top: -35px;
                position: relative;
                z-index: 2;
            }
            .btn-create {
                font-size: 0.9rem;
                letter-spacing: 0.05rem;
                padding: 0.75rem 1rem;
            }
        </style>
    </head>

    <body>
        <div class="container">
            <div class="row">
                <a href="confirmPage.php?admin" class="previous" style="text-decoration: none;margin-top: 10px;">‹ Torna indietro</a>
                <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">

                    <div class="card border-0 shadow rounded-3 my-5">
                        <div class="card-body p-4 p-sm-5">
                            <h5 class="card-title text-center mb-5 fw-light fs-5">Crea utenza</h5>
                            <form action="createUser.php" method="POST">
                                <p id="error" style="display:none;color: red;" >Indirizzo email già esistente!</p>
                                <div class="form-floating mb-2">
                                    <input type="email" class="form-control" id="floatingInput" name="email" placeholder="name@example.com" onkeyup="verifyEmail(this.value)">
                                    <label for="floatingInput">Indirizzo email</label>
                                </div>
                                <div class="form-floating mb-2">
                                    <input type="password" class="form-control" id="floatingPassword" name="password" placeholder="Password">
                                    <label for="floatingPassword">Password</label>
                                    <span id="toggle" toggle="#floatingPassword" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                </div>
                                <div >
                                    <select class="form-select" name="role" id="role"
                                            aria-label="Floating label select example">
                                        <option value="-1" selected="" >Seleziona ruolo...</option>
                                        <option value="0" >Admin</option>
                                        <option value="1" >Manager</option>
                                        <option value="2" >Utente/Personale interno</option>
                                        <option value="3" >Azienda/Ente</option>
                                        <option value="4" >Supervisor</option>
                                        <option value="5" >Coordinatore piani</option>
                                    </select>
                                </div>
                                <div style="margin-top:10%;margin-bottom: 10%;display: flex;justify-content: center;">
                                    <div>
                                        <div>
                                            <p>Scegli piattaforma</p>
                                        </div>
                                        <?php
                                        foreach ($array_piattaforma as $key => $value) {
                                            ?>
                                            <div>
                                                <?php
                                                if ($key == 4) {
                                                    ?>
                                                    <input type="checkbox"  name="platform[]" value="<?php echo $key; ?>" disabled="true">
                                                    <label for="platform"><?php echo $value; ?></label>
                                                <?php } else { ?>
                                                    <input type="checkbox"  name="platform[]" value="<?php echo $key; ?>">
                                                    <label for="platform"><?php echo $value; ?></label>
                                                    <?php }
                                                ?>

                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>



                                <div class="d-grid">
                                    <button class="btn btn-primary btn-create text-uppercase fw-bold" name="crea" id="creaButton" type="submit" onclick="return checkCampi()">Crea</button>
                                </div>


                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>

                                        $("#toggle").click(function () {
                                            $(this).toggleClass("fa-eye fa-eye-slash");
                                            var x = document.getElementById("floatingPassword");
                                            if (x.type === "password") {
                                                x.type = "text";
                                            } else {
                                                x.type = "password";
                                            }
                                        });


                                        function checkCampi() {
                                            var emText = document.getElementById("floatingInput").value;
                                            var pwd = document.getElementById("floatingPassword").value;
                                            var e = document.getElementById("role");
                                            var role = e.options[e.selectedIndex].value;
                                            if (emText.length == 0) {
                                                alert("Inserisci email!");
                                                return false;
                                            }
                                            if (pwd.length < 6) {
                                                alert("Inserisci una password con almeno 6 caratteri!");
                                                return false;
                                            }
                                            if (role == -1) {
                                                alert("Seleziona ruolo!");
                                                return false;
                                            }
                                            var textinputs = document.querySelectorAll('input[type=checkbox]');
                                            var empty = [].filter.call(textinputs, function (el) {
                                                return !el.checked
                                            });

                                            if (textinputs.length == empty.length) {
                                                alert("Seleziona almeno una piattaforma");
                                                return false;
                                            }

                                            return true;

                                        }


                                        function verifyEmail(email) {
                                            if (email.length > 4) {
                                                $.ajax({
                                                    method: 'GET',
                                                    url: "verifyEmail.php",
                                                    data: {
                                                        email: email
                                                    }
                                                })
                                                        .done(function (resultParsed) {

                                                            if (resultParsed !== "no") {
                                                                document.getElementById("error").style.display = "block";
                                                                document.getElementById('creaButton').disabled = true;
                                                            } else {
                                                                document.getElementById("error").style.display = "none";
                                                                document.getElementById('creaButton').disabled = false;
                                                            }
                                                        })
                                                        .fail(function (jqXHR, exception) {
                                                            var msg = '';
                                                            if (jqXHR.status === 0) {
                                                                msg = 'Not connect.\n Verify Network.';
                                                            } else if (jqXHR.status == 404) {
                                                                msg = 'Requested page not found. [404]';
                                                            } else if (jqXHR.status == 500) {
                                                                msg = 'Internal Server Error [500].';
                                                            } else if (exception === 'parsererror') {
                                                                msg = 'Requested JSON parse failed.';
                                                            } else if (exception === 'timeout') {
                                                                msg = 'Time out error.';
                                                            } else if (exception === 'abort') {
                                                                msg = 'Ajax request aborted.';
                                                            } else {
                                                                msg = 'Uncaught Error.\n' + jqXHR.responseText;
                                                            }
                                                            console.log(msg);
                                                        });
                                            }
                                        }

    </script>
</html>
