<?php

ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);

error_reporting(E_ALL);
require '../../vendor/autoload.php';

include("obj/Report.php");

$report = new Report();

$redis = new Redis();
$redis->connect('127.0.0.1', 6379);

if (isset($_POST['email']) && isset($_POST['password']) && isset($_POST['enterprise']) && isset($_POST['action'])) {

    $inizio = strtotime('now -24 months');
    $fine = strtotime('now');

    $email = strtolower(trim($_POST['email']));
    $password = trim($_POST['password']);
    $password = md5($password);

    $action = trim($_POST['action']);

    $time = '00:00:00';

    $gruppo = $_POST['enterprise'];
    $gruppo = urldecode($gruppo);

    $collection = new MongoDB\Client(
        "mongodb+srv://jobtek:TimeVision2021%40%21%3F@timevision.t9ed5.mongodb.net/TimeVision?retryWrites=true&w=majority");
    $collectionUsers = $collection->admin->utenti;
    $collectionPiattafrome = $collection->admin->piattaforme;

    $cursor = $collectionUsers->find(
        ['email' => $email, 'password' => $password],
        ['projection' => ['id' => 1, 'idRuolo' => 1, 'idPiattaforma' => 1]]
    );

    foreach ($cursor as $value) {
        if (!$value->id) {
            http_response_code(403);
            echo "Non autorizzato!";
        } else {
            $user_id = $value->id;

            //TODO OLD
//            if ($action == 'getCorsi') {
//
//                //checkRedis
//                $key = 'get-corsi-' . slugify($gruppo);
//
//                $collectionCorsiCron = $collection->admin->cron_corsi;
////                $cursorCron = $collectionCorsiCron->find(['redis_key' => $key], ['sort' => ['company' => 1]]);
//                $cursorCron = $collectionCorsiCron->find(['redis_key' => $key]);
//
//                foreach ($cursorCron as $cron) {
//                    $mongo_redis_key_id = $cron['redis_key'];
//                }
//
////                $ttl = $redis->ttl($key);
////                var_dump($ttl); exit;
////                $redis->del($key);
//                if($mongo_redis_key_id && $redis->exists($key)){
//
//                    $response = $redis->get($key);
//                    header('Content-Type: application/json; charset=utf-8');
//                    http_response_code(201);
//                    echo $response;
//
//                } elseif ($mongo_redis_key_id && !$redis->exists($key)) {
//
//                    $response = [
//                        'corsi_totali' => 'In aggiornamento',
//                        'corsi' => []
//                    ];
//
//                    header('Content-Type: application/json; charset=utf-8');
//                    http_response_code(201);
//                    echo json_encode($response);
//                }else{
//                    $collectionCorsiCron->insertOne([
//                        'redis_key' => $key,
//                        'company' => $gruppo,
//                        'action' => $action,
//                        'data' => '',
//                        'in_corso' => true,
//                        'flag' => 0,
//                        'created_at' => strtotime("now"),
//                        'last_update' => strtotime("now")
//                    ]);
//
//                    $response = [
//                        'corsi_totali' => 'In aggiornamento',
//                        'corsi' => []
//                    ];
//
//                    header('Content-Type: application/json; charset=utf-8');
//                    http_response_code(201);
//                    echo json_encode($response);
//                }
//            }

            if ($action == 'getCorsi') {

                //checkRedis
                $key = 'get-corsi-' . slugify($gruppo);

//                echo $key;

                $collectionCorsiCron = $collection->admin->cron_corsi;
//                $cursorCron = $collectionCorsiCron->find(['redis_key' => $key], ['sort' => ['company' => 1]]);
                $cursorCron = $collectionCorsiCron->find(['redis_key' => $key]);

                foreach ($cursorCron as $cron) {
                    $mongo_redis_key_id = $cron['redis_key'];
                    $mongo_data = $cron['data'];
                }

//                $ttl = $redis->ttl($key);
//                var_dump($ttl); exit;
//                $redis->del($key);
                if($mongo_redis_key_id){

                    $response = $mongo_data;
                    header('Content-Type: application/json; charset=utf-8');
                    http_response_code(201);
                    echo $response;

                } elseif ($mongo_redis_key_id && $mongo_data == '') {

                    $response = [
                        'corsi_totali' => 'In aggiornamento',
                        'corsi' => []
                    ];

                    header('Content-Type: application/json; charset=utf-8');
                    http_response_code(201);
                    echo json_encode($response);
                }else{
                    $collectionCorsiCron->insertOne([
                        'redis_key' => $key,
                        'company' => $gruppo,
                        'action' => $action,
                        'data' => '',
                        'in_corso' => true,
                        'flag' => 0,
                        'created_at' => strtotime("now"),
                        'last_update' => strtotime("now")
                    ]);

                    $response = [
                        'corsi_totali' => 'In aggiornamento',
                        'corsi' => []
                    ];

                    header('Content-Type: application/json; charset=utf-8');
                    http_response_code(201);
                    echo json_encode($response);
                }
            }

            //dettaglio corsi
            if ($action == 'detailCorsi') {

                $mongo_redis_key_id = false;
                $collectionCorsiDetailCron = $collection->admin->cron_corsi_detail;

                $courseid = trim($_POST['courseid']);
                $selected = trim($_POST['platform']);

                $key = 'detail-corso-' . $courseid . '-' . slugify($gruppo);
                $key_time = 'detail-corso-' . $courseid . '-' . slugify($gruppo) . '__time';

                //tempo salvataggio
                $now = strtotime('now');

                //decommentare per test
//                $redis->del($key);
//                $redis->del($key_time);

//                var_dump($key);
//                var_dump($redis->get($key_time)); exit;

                if ($redis->get($key_time)) {

                    $response = $redis->get($key);

                    if($response) {
                        header('Content-Type: application/json; charset=utf-8');
                        http_response_code(201);
                        echo $response;
                    }else{
                        $response = 'In aggiornamento';

                        header('Content-Type: application/json; charset=utf-8');
                        http_response_code(201);
                        echo $response;
                    }

                }else{

                    $collectionCorsiDetailCron->insertOne([
                        'redis_key' => $key,
                        'key_time' => $key_time,
                        'database' => $selected,
                        'courseid' => $courseid,
                        'inizio' => $inizio,
                        'fine' => $fine,
                        'gruppo' => $gruppo,
                        'created_at' => strtotime("now"),
                        'last_update' => strtotime("now")
                    ]);

                    $response = 'In aggiornamento';

                    $redis->set($key_time, strtotime('now'));

                    header('Content-Type: application/json; charset=utf-8');
                    http_response_code(201);
                    echo $response;
                }

//                $redis->del($key);
//                if ($redis->exists($key)) {
//
//                    $response = $redis->get($key);
//                    header('Content-Type: application/json; charset=utf-8');
//                    http_response_code(201);
//                    echo $response;
//
//                } else {
//
////                    echo "qui"; exit;
//
//                    $array_cf = array();
//
//                    $tempo_totale_all_secondi_utenti = 0;
//                    $time_not_formatted_sum = 0;
//
//                    $collectionCorsiAssoc = $collection->$selected->corsiAssociati;
//                    $collectionHvp = $collection->$selected->mdl_hvp;
//                    $collectionUserInfoData = $collection->$selected->mdl_user_info_data;
//                    $collectionLog = $collection->$selected->mdl_logstore_standard_log;
//                    $collectionZoom = $collection->$selected->mdl_zoom_meeting_participants_aggregate;
//
//                    $cursoreHvp = $collectionHvp->find(
//                        ['course' => (int)$courseid],
//                        ['projection' => ['json_content' => 1]]
//                    );
//
//                    $dataHvp = iterator_to_array($cursoreHvp);
//
////                    var_dump($dataHvp); exit;
//
//                    $time_not_formatted_sum = 0;
//
//                    if ($dataHvp) {
//                        foreach ($dataHvp as $hvp) {
//                            $hvpContent = json_decode($hvp['json_content']);
////                            var_dump($hvpContent); exit;
//                            if(array_key_exists(0, $hvpContent->interactiveVideo->assets->endscreens)) {
//                                $time_not_formatted = $hvpContent->interactiveVideo->assets->endscreens[0]->time;
//                                $time_not_formatted_sum += $time_not_formatted;
//                            }
//                        }
//                    }
//
//                    $time = $report->convertSeconds($time_not_formatted_sum);
//
//                    $array_users_azienda = $report->getIdUtentiFromAzienda($selected, $gruppo);
//                    $cursoreLog = $collectionLog->distinct("userid", ['userid' => array('$in' => $array_users_azienda), 'courseid' => (int)$courseid]);
////
////                    if ($cursoreLog) {
////                        $total_iscritti = count($cursoreLog);
////                    } else {
////                        $total_iscritti = 0;
////                    }
//
//                    foreach ($cursoreLog as $user_id) {
//                        $cf = $report->getCfUtenti($selected, $user_id);
//
//                        if($cf == ''){
//                            $cf = 'N.D.';
//                        }
//
//                        array_push($array_cf, $cf);
//                    }
//
////                    var_dump($array_cf); exit;
//
//
//                    //totale dati corsi di tutti gli utenti
//    //                $res = $report->getAggregateHvpAll($selected, (int) $courseid, $array_users_azienda);
//                    $hvpName = $report->getNameAttivitaHVP($selected);
//                    $nameCorsi = $report->getAllNameCourse($selected);
//                    //vedere qui
//
//                    //TODO Vecchio metodo
////                    $res = $report->getAggregateHvpGlobal($selected, $inizio, $fine, (int)$courseid, $array_users_azienda);
////                    $result = $report->constructHvpArray($res, $hvpName, $nameCorsi, $selected);
////                    $tmp = $report->constructHvpReportTest3($result, $selected);
////                    $res_data = $report->constructSHvpReportNewEnzo2($tmp, $selected);
////                    $res = explode("*", $res_data);
//
//                    //TODO Nuovo metodo
//                    $res = $report->getAggregateHvpGlobal($selected, $inizio, $fine, (int)$courseid, $array_users_azienda);
//                    $result = $report->constructHvpArray($res, $hvpName, $nameCorsi);
//                    $tmp = $report->constructHvpFinal($result, $selected);
//                    $res_data = $report->constructSHvpReportFinal($tmp, $selected);
//                    $res = explode("*", $res_data);
//
////                    var_dump($res); exit;
//
//                    $decode_res = json_decode($res[0], true);
//
////                    var_dump($decode_res); exit;
//
//                    $parse_mese = array();
//                    $duration_zoom = 0;
//                    foreach ($decode_res as $dec) {
//
////                        var_dump($dec); exit;
//
//                        $array_mesi = json_decode(json_encode($dec->mesi), true);
//
////                        var_dump($array_mesi); exit;
//
//    //                    $cf = $report->getCfUtenti($selected, $dec->userid);
//                        $dec->cf = $report->getCfUtenti($selected, $dec->userid);
//
//                        //zoom
//                        $pipeline_zoom = [
//                            [
//                                '$match'=> [
//                                    'userid' => $dec->userid,
//                                    'idCorso' => intval($dec->courseid),
//                                    'join_time' => [
//                                        '$gte' => $inizio,
//                                        '$lte' => $fine
//                                    ],
//                                ]
//                            ], [
//                                '$project' => [
//                                    'userid' => 1,
//                                    'duration' => 1,
//                                    'date' => [
//                                        '$toDate' => [
//                                            '$multiply' => [
//                                                '$join_time', 1000
//                                            ]
//                                        ]
//                                    ]
//                                ]
//                            ], [
//                                '$project' => [
//                                    'userid' => 1,
//                                    'duration' => 1,
//                                    'date' => [
//                                        '$dateToString' =>  [
//                                            'format' => "%d/%m/%Y", 'date' => '$date'
//                                        ]
//                                    ]
//                                ]
//                            ], [
//                                '$group' => [
//                                    '_id'=> [ 'dateToString' => [ 'format' => "%d/%m/%Y", 'date' => '$date'] ],
//                                    'join_time' => [
//                                        '$addToSet' => '$join_time'
//                                    ],
//                                    'durata'=> [
//                                        '$sum' => '$duration'
//                                    ]
//                                ]
//                            ]
//                        ];
//
//                        $zoom = $collectionZoom->aggregate($pipeline_zoom);
//                        $zoom_out = $zoom->toArray();
//
////                        var_dump($zoom_out);
//
//                        $tempo_totale_secondi_utenti = 0;
//
//                        //esperimento
//                        $array_esp = array();
//                        $array_mesi = array();
//                        $zoom_durata_totale = 0;
//                        if(count($zoom_out) > 0) {
//
//                            foreach ($zoom_out as $key => $zoom) {
////                                var_dump($zoom['_id']['dateToString']['date']); exit;
//                                $accesso_zoom = $zoom['_id']['dateToString']['date'];
//                                $duration_zoom = $zoom['durata'];
////                                var_dump($duration_zoom); exit;
//                                $array_esp[$accesso_zoom] = $duration_zoom;
//                                $zoom_durata_totale += $duration_zoom;
//                            }
//                        }
//
////                        var_dump($array_esp); exit;
//
////                        $array_mesi = json_decode(json_encode($dec->mesi), true);
////
////                        if (!array_key_exists($accesso_zoom, $array_mesi)){
////                            echo $accesso_zoom . "\n";
////                        }
//
//                        $dec_tempo_tot = 0;
//                        if(count($array_esp) > 0){
//
//                            foreach($array_esp as $key => $zoom){
////                                $accesso_zoom = date("d/m/Y", $zoom['join_time'][0]);
//                                $accesso_zoom = $key;
//                                $duration_zoom = $zoom;
////                                echo $duration_zoom."\n";
////                                exit;
//
//                                $tempo_mese_all_secondi_utenti = 0;
//
//                                foreach($dec->mesi as $key => $mesi){
//
//                                    $orario = $dec->mesi->$key;
//
////                                    var_dump($array_mesi); exit;
//
//                                    if (array_key_exists($accesso_zoom, $array_mesi)){
//
////                                        echo $dec->mesi->$key . "\n";
//
//
//
////                                        var_dump(in_array($dec->mesi->$key, $parse_mese)); exit;
//
////                                        if(!in_array($dec->mesi->$key, $parse_mese)) {
//
//
//                                            $tempo_mese_all = explode(":", $orario);
//                                            $tempo_mese_all_secondi_utenti += $tempo_mese_all[0] * 3600 + $tempo_mese_all[1] * 60 + $tempo_mese_all[2];
//
//                                            $new_tempo = $tempo_mese_all_secondi_utenti + $duration_zoom;
////                                        var_dump($dec->mesi->$key);
////                                        var_dump($tempo_mese_all_secondi_utenti);
////                                        var_dump($duration_zoom);
////                                        var_dump($new_tempo);
////                                        var_dump($key);
////                                        var_dump($dec->mesi->$key);
//                                            $dec->mesi->$key = $report->convertSeconds($new_tempo);
//
////                                        if($dec->userid == 604){
////                                            echo "si\n";
////                                            var_dump($accesso_zoom);
////                                            var_dump($duration_zoom);
////                                            echo $dec->mesi->$key . "\n";
////                                            echo "--------\n";
////                                        }
//
////                                        var_dump($key);
////                                        var_dump($dec->mesi->$key); exit;
//
////                                            $dec_tempo_tot += $new_tempo;
//
////                                            $parse_mese[] = $key;
//
////                                            var_dump($parse_mese); exit;
////                                        }
//
//                                    }else{
////                                        if(!in_array($dec->mesi->$key, $parse_mese)) {
//
////                                            if($accesso_zoom == '29/03/2021' && $dec->userid == 604){
////                                                echo "secondo";
////                                                var_dump($duration_zoom);
////                                            }
//
//                                            $dec->mesi->$accesso_zoom = $report->convertSeconds($duration_zoom);
//
////                                            if($accesso_zoom == '29/03/2021' && $dec->userid == 604){
////                                                echo "qui";
////                                                var_dump($dec->mesi->$accesso_zoom);
////                                            }
//
////                                            if($dec->userid == 604){
////                                                echo "no\n";
////                                                var_dump($accesso_zoom);
////                                                var_dump($duration_zoom);
////                                                echo $dec->mesi->$key . "\n";
////                                                echo "--------\n";
////                                            }
//
////                                            $dec_tempo_tot += $duration_zoom;
////                                            $parse_mese[] = $key;
//
////                                            if($accesso_zoom == '29/03/2021' && $dec->userid == 604){
////                                                var_dump($dec->mesi);
////                                                exit;
////                                            }
////                                        }
//                                    }
//                                }
//                            }
//                        }
//
//                        $tempo_totale_dec = explode(":", $dec->tempo_tot);
////                        if($dec->userid == 604){
////                            var_dump($dec->tempo_tot);
////                        }
//                        $tempo_totale_secondi_utenti += $tempo_totale_dec[0] * 3600 + $tempo_totale_dec[1] * 60 + $tempo_totale_dec[2];
//                        $dec->tempo_tot = $report->convertSeconds($zoom_durata_totale + $tempo_totale_secondi_utenti);
//
//    //                    var_dump($cf);
////                        var_dump($dec);
//
////                        if($dec->userid == 9495){
////                            var_dump($dec->tempo_tot);
////                            var_dump($dec->tempo_tot);
////                            exit;
////                        }
//
//                        array_push($newArray, $dec);
//                    }
//
////                    var_dump($newArray); exit;
//
//                    $arrM = array();
//                    $arrMesiFinale = array();
//                    foreach($newArray as $arr){
////                        foreach($arr->mesi as $key => $mesi){
////                            $arrM = json_decode(json_encode($arr->mesi), true);
////                            usort($arrM, "compareByTimeStampNew");
////                        }
//
////                        var_dump($arr->mesi);
//
//                        $arrM = json_decode(json_encode($arr->mesi), true);
//
//                        uksort($arrM, "compareByTimeStampNew");
//
////                        $ao = new ArrayObject($arr->mesi);
////                        $ao->uasort('compareByTimeStampNew');
////
//////                        $arr->mesi->uasort('compareByTimeStampNew');
////
//////                        usort($arr->mesi, "compareByTimeStampNew");
////                        var_dump($ao); exit;
//
//                        $arr->mesi = (object)$arrM;
//
//                        foreach($arr->mesi as $key => $mesi){
////                            $arrMesiFinale = array($key);
//                            if(!in_array($key, $arrMesiFinale)){
//                                array_push($arrMesiFinale, $key);
//                            }
//                        }
//
////                        var_dump($arr); exit;
//                    }
//
////                    array_unique($arrMesiFinale);
//
////                    var_dump($arrMesiFinale); exit;
//
////                    var_dump(json_encode($arrMesiFinale));
////                    var_dump($res[1]); exit;
//
////                    var_dump($arrMesiFinale); exit;
////                    var_dump($newArray);
////
////                    exit;
//
//                    $dec_result = json_encode($newArray);
//
//    //                var_dump($dec_result); exit;
//
//    //                $array_mesi = json_decode($res[1], true);
//    //
//    //                $res_finale = die(json_encode($res) . "*" . json_encode($array_mesi));
//
//                    $allData = json_decode($dec_result, true);
//
//                    $new = array();
//                    foreach ($allData as &$singolo) {
//                        foreach ($arrMesiFinale as $mese) {
//                            if (!in_array($mese, array_keys($singolo['mesi']))) {
//                                $singolo['mesi'][$mese] = "00:00:00";
//                            }
//                        }
//                        uksort($singolo['mesi'], "compareByTimeStampNew");
//
//
//
////                        if(!in_array($key, $arrMesiFinale)){
////                            array_push($arrMesiFinale, $key);
////                        }
//
//                        array_push($new, $singolo);
//                    };
//
////                    var_dump($new); exit;
//
//    //                            var_dump($corsi->_id);
//    //                            var_dump($allData); exit;
//
//                    $arrMesiFinale = array();
//                    foreach ($new as $data) {
//
////                        if($data['userid'] == 604){
////
////                        }
//
//                        foreach($data['mesi'] as $mese => $ora){
//                            if(!in_array($mese, $arrMesiFinale)){
//                                array_push($arrMesiFinale, $mese);
//                            }
//                        }
//
//                        $tempo_totale_all = explode(":", $data['tempo_tot']);
//                        $tempo_totale_all_secondi_utenti += $tempo_totale_all[0] * 3600 + $tempo_totale_all[1] * 60 + $tempo_totale_all[2];
//                    }
//
//                    $total_iscritti = count($new);
//
////                    var_dump($arrMesiFinale); exit;
//
//                    //calcolo percentuali
//                    $totali_tempo_corso_da_fare = ($total_iscritti * $time_not_formatted_sum);
////                    var_dump($tempo_totale_all_secondi_utenti);
////                    var_dump($totali_tempo_corso_da_fare);
////                    exit;
//                    if ($tempo_totale_all_secondi_utenti > 0 && $totali_tempo_corso_da_fare > 0) {
//                        $totale_seguito_utenti = $report->convertSeconds($tempo_totale_all_secondi_utenti);
//                        $percent = (($tempo_totale_all_secondi_utenti / $totali_tempo_corso_da_fare) * 100);
//                    } else {
//                        $totale_seguito_utenti = '00:00:00';
//                        $percent = 0;
//                    }
//
//                    $percentage = $percent;
//
////                    if ($percent > 0) {
////                        $percentage = 100 - $percent;
////                    } else {
////                        $percentage = 0;
////                    }
//
//                    //pezza
//                    if ($tempo_totale_all_secondi_utenti > $totali_tempo_corso_da_fare) {
//                        $percentage = 100;
//                    }
//
//                    $arrayCorsi[] = array(
//                        'id' => (int)$courseid,
//    //                    'name' => $name, //manca
//                        'selected' => $selected,
//                        'total_iscritti' => $total_iscritti,
//                        'time' => $time,
//                        'total_following' => $totale_seguito_utenti,
//                        'percentage' => $percentage,
//                        'users' => $array_cf,
//                        'user_data' => json_decode(json_encode($new), true, JSON_UNESCAPED_SLASHES)
//                    );
//
//                    $response = [
//                        'data' => $arrayCorsi,
//                        'month_data' => json_decode(json_encode($arrMesiFinale), true, JSON_UNESCAPED_SLASHES)
//                    ];
//
//                    $redis->set($key, json_encode($response), 14400);
//
//                    header('Content-Type: application/json; charset=utf-8');
//                    http_response_code(201);
//                    echo json_encode($response);
//
//
//                }
            }

        }
    }
}

function slugify($string){
    return strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $string), '-'));
}
