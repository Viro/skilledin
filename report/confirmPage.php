<?php
session_start();


if (!isset($_SESSION['user']) || ($_SESSION['user']['idRuolo'] != 0 && $_SESSION['user']['idRuolo'] != 1 && $_SESSION['user']['idRuolo'] != 4)) {
    header("location:index.php");
}


if (isset($_POST['goTo'])) {
    header('location:index.php');
} else if (isset($_POST['crea'])) {
    header('location:createUser.php');
} else if (isset($_POST['manage'])) {
    header('location:manageUser.php');
}else if (isset($_POST['resetH5P'])) {
    header('location:resetH5P.php');
}else if (isset($_POST['upload_data'])) {
    header('location:upload_data.php');
}else if (isset($_POST['course_module_info'])) {
    header('location:course_module_info.php');
}else if (isset($_POST['course_module_sync'])) {
    header('location:course_module_sync.php');
}else if (isset($_POST['cron_history'])) {
    header('location:pages/cron_history.php');
}else if (isset($_POST['aziende_piani'])) {
    header('location:pages/aziende_piani.php');
}else if (isset($_POST['report_piani'])) {
    header('location:pages/report_piani.php');
}else if(isset($_POST['settings'])){
    header('location:settings.php?from=confirm');
}
?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css">

        <style>

            .btn-create {
                font-size: 0.9rem;
                letter-spacing: 0.05rem;
                padding: 0.75rem 1rem;
            }
        </style>
    </head>

    <body>
        <div class="container">
            <div class="row">
                <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
                    <div class="card border-0 shadow rounded-3 my-5">
                        <div class="card-body p-4 p-sm-5">
                            <?php if (isset($_GET['admin'])) { ?>
                                <h5 class="card-title text-center mb-5 ">Cosa vuoi fare?</h5>
                            <?php } else { ?>
                                <h5 class="card-title text-center mb-5 ">Utente creato correttamente!</h5>
                            <?php } ?>

                            <form action="confirmPage.php" method="POST">
                                <div class="d-grid mb-2">
                                    <button class="btn btn-primary btn-create text-uppercase fw-bold" name="goTo"  type="submit" ">Vai ai report</button>
                                </div>
                                <div class="d-grid mb-2">
                                    <button class="btn btn-primary btn-create text-uppercase fw-bold" name="resetH5P"  type="submit" ">Reset H5P</button>
                                </div>
                                <?php if($_SESSION['user']['idRuolo'] == 0 || $_SESSION['user']['idRuolo'] == 1){ ?>
                                    <div class="d-grid mb-2">
                                        <button class="btn btn-primary btn-create text-uppercase fw-bold" name="settings"  type="submit" ">Impostazioni Excel</button>
                                    </div>
                                    <div class="d-grid mb-2">
                                        <button class="btn btn-primary btn-create text-uppercase fw-bold" name="upload_data"  type="submit" ">Upload dati consolidati</button>
                                    </div>
                                    <div class="d-grid mb-2">
                                        <button class="btn btn-primary btn-create text-uppercase fw-bold" name="crea"  type="submit" ">Crea utente</button>
                                    </div>
                                    <div class="d-grid mb-2">
                                        <button class="btn btn-primary btn-create text-uppercase fw-bold" name="manage"  type="submit" ">Gestisci utenti</button>
                                    </div>
                                    <div class="d-grid mb-2">
                                        <button class="btn btn-primary btn-create text-uppercase fw-bold" name="course_module_info"  type="submit" ">Elenco lezioni corsi</button>
                                    </div>
                                    <div class="d-grid mb-2">
                                        <button class="btn btn-primary btn-create text-uppercase fw-bold" name="course_module_sync"  type="submit" ">Sincronizza corsi</button>
                                    </div>
                                    <div class="d-grid mb-2">
                                        <button class="btn btn-primary btn-create text-uppercase fw-bold" name="cron_history"  type="submit" ">Riepilogo esecuzione cron</button>
                                    </div>
                                <?php } ?>

                                <?php if($_SESSION['user']['idRuolo'] == 0 || $_SESSION['user']['idRuolo'] == 5){ ?>
                                <div class="d-grid mb-2">
                                    <button class="btn btn-primary btn-create text-uppercase fw-bold" name="aziende_piani"  type="submit" ">Gestione piani formativi</button>
                                </div>
                                <div class="d-grid mb-2">
                                    <button class="btn btn-primary btn-create text-uppercase fw-bold" name="report_piani"  type="submit" ">Report piani formativi</button>
                                </div>
                                <?php } ?>


                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
