#!/usr/bin/php
<?php

ini_set('memory_limit', '2048M');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
ini_set('max_execution_time', 0);
error_reporting(E_ALL);

if (isset($_POST['email']) && isset($_POST['password'])) {

    $file_name = fopen("/home/admin/public_html/skilledin/report/log_update_manual_log.txt", "a") or die("Unable to open file!");

    require '/home/admin/public_html/vendor/autoload.php';

    $email = strtolower(trim($_POST['email']));
    $password = trim($_POST['password']);
    $passwordMongo = md5($password);

    $platform = $_POST['platform'];

    switch ($platform) {
        case 'live':
            $servername = "77.39.212.59:3306";
            $username = "livecors_moodle";
            $password = "bn_moodle_01!";
            $dbname = "livecors_moodle";
            $db = "live";
            break;
        case 'f40':
            $servername = "77.39.212.60:3306";
            $username = "formazio_forma40";
            $password = "uh$^^?YFM2l6";
            $dbname = "formazio_formazione40";
            $db = "f40";
            break;
        case 'fnc':
            $servername = "77.39.212.58:3306";
            $username = "fnccorsi_fncmoodle";
            $password = "CWo{}=+CWqU&";
            $dbname = "fnccorsi_fnc";
            $db = "fnc";
            break;
        case 'formaz':
            $servername = "77.39.209.12:3306";
            $username = "formazionecorsin_corsi";
            $password = "W~Q_NPp20X&y";
            $dbname = "formazionecorsin_corsi";
            $db = "formaz";
            break;
        case 'new':
            $servername = "185.81.6.69:3306";
            $username = "newcorsi_moodle";
            $password = "9f.LOcY9ArUt";
            $dbname = "newcorsi_moodle";
            $db = "new";
            break;
    }

    //Connessione a mongodb
    $connection = new MongoDB\Client(
        "mongodb+srv://jobtek:TimeVision2021%40%21%3F@timevision.t9ed5.mongodb.net/TimeVision?retryWrites=true&w=majority");

    $collectionUsers = $connection->admin->utenti;
    $collectionPiattafrome = $connection->admin->piattaforme;
    $collection = $connection->$db->mdl_logstore_standard_log;

    $cursor = $collectionUsers->find(
        ['email' => $email, 'password' => $passwordMongo],
        ['projection' => ['id' => 1, 'idRuolo' => 1, 'idPiattaforma' => 1]]
    );

    $dataUser = $cursor->toArray();

    if(count($dataUser) == 0){
        http_response_code(403);
        fwrite($myfile, 'Accesso non autorizzato '.$email);
        echo "Non autorizzato!";
        exit;
    }

    fwrite($file_name, date('d-m-Y H:i:s', strtotime('now')) . ': Connesso '.$email . "\n");

    $myfile = fopen("mdl_logstore_standard_log.txt", "w") or die("Unable to open file!");


    $conn = new mysqli($servername, $username, $password, $dbname);


    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $pivot = $connection->$db->pivot;

    $data = date("Y-m-d");
    $orario = date("H:i:s");


    $cursor = $pivot->find(
        ['collection' => "mdl_logstore"],
        ['projection' => ['id' => 1, 'data' => 1, 'orario' => 1],
            'sort' => ['id' => -1], 'limit' => 1]
    );
    $id = 0;

    $idsString = $_POST['ids'];

    $ids = explode(',', $idsString);

    $count_array = count($ids);

    if ($count_array == 1) {
        $sql = "SELECT * FROM mdl_logstore_standard_log WHERE id = " . $ids[0];
    }

    if ($count_array == 2) {
        $sql = "SELECT * FROM mdl_logstore_standard_log WHERE id >= " . $ids[0] . " and id <= " . $ids[0];
    }

    if ($count_array > 2) {
        $idsTxt = implode(',', $ids);
        $sql = "SELECT * FROM mdl_logstore_standard_log WHERE id IN (" . $idsTxt . ")";
    }

    fwrite($file_name, date('d-m-Y H:i:s', strtotime('now')) . ': Query lanciata: '.$sql . "\n");

//    echo $sql;
//    exit;

    $result = $conn->query($sql);
//echo $result->num_rows;exit();
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {

            $id = $row["id"];

//        var_dump($id); exit;

            echo $id . "\n";

            fwrite($myfile, "Id aggiornati:\n ".$id . "\n");


//        if($id == 6992589){
//            print_r($row);
//            exit;
//        }

            $logstore_standard_logId = $row["id"];

            $curId = $collection->find(['id' => intval($logstore_standard_logId)]);

            $mongo_mdl_logstore_standard_log_id = 0;

            foreach ($curId as $row2) {
                $mongo_mdl_logstore_standard_log_id = $row2['id'];
            }

//            var_dump($mongo_mdl_logstore_standard_log_id); exit;

            if ($mongo_mdl_logstore_standard_log_id) {

                $updateResult = $collection->updateOne(
                    ['id' => intval($mongo_mdl_logstore_standard_log_id)],
                    [
                        '$set' => [
                            'eventname' => utf8_encode($row["eventname"]),
                            'component' => utf8_encode($row["component"]),
                            'action' => utf8_encode($row["action"]),
                            'target' => utf8_encode($row["target"]),
                            'objecttable' => utf8_encode($row["objecttable"]),
                            'objectid' => (int)$row["objectid"],
                            'crud' => utf8_encode($row["crud"]),
                            'edulevel' => (int)$row["edulevel"],
                            'contextid' => (int)$row["contextid"],
                            'contextlevel' => utf8_encode($row["contextlevel"]),
                            'contextinstanceid' => (int)$row["contextinstanceid"],
                            'userid' => (int)$row["userid"],
                            'courseid' => (int)$row["courseid"],
                            'relateduserid' => (int)$row["relateduserid"],
                            'anonymous' => (int)$row["anonymous"],
                            'other' => utf8_encode($row["other"]),
                            'timecreated' => (int)$row["timecreated"],
                            'origin' => utf8_encode($row["origin"]),
                            'ip' => utf8_encode($row["ip"]),
                            'realuserid' => (int)$row["realuserid"]
                        ]
                    ]
                );

            } else {
                $collection->insertOne([
                    'id' => (int)$row["id"],
                    'eventname' => utf8_encode($row["eventname"]),
                    'component' => utf8_encode($row["component"]),
                    'action' => utf8_encode($row["action"]),
                    'target' => utf8_encode($row["target"]),
                    'objecttable' => utf8_encode($row["objecttable"]),
                    'objectid' => (int)$row["objectid"],
                    'crud' => utf8_encode($row["crud"]),
                    'edulevel' => (int)$row["edulevel"],
                    'contextid' => (int)$row["contextid"],
                    'contextlevel' => utf8_encode($row["contextlevel"]),
                    'contextinstanceid' => (int)$row["contextinstanceid"],
                    'userid' => (int)$row["userid"],
                    'courseid' => (int)$row["courseid"],
                    'relateduserid' => (int)$row["relateduserid"],
                    'anonymous' => (int)$row["anonymous"],
                    'other' => utf8_encode($row["other"]),
                    'timecreated' => (int)$row["timecreated"],
                    'origin' => utf8_encode($row["origin"]),
                    'ip' => utf8_encode($row["ip"]),
                    'realuserid' => (int)$row["realuserid"]
                ]);
            }


        }

        mail("enrico.timevision@gmail.com ", "Piattaforma ".$platform." - Fine importazione manuale", "Piattaforma ".$platform." - Importazione terminata con successo. Verifica !!!");

    }

    fclose($myfile);
    fclose($file_name);


    $conn->close();

}
