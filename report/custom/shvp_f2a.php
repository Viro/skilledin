#!/usr/bin/php
<?php

//example
//sp-php report_manual.php "tv3vEaH0wFu!OTV" f40 shvp "FLORA NAPOLI SRL" "2022-12-30" "2022-12-31"

ini_set('memory_limit', '2048M');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
ini_set('max_execution_time', 0);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT);

require_once($_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/skilledin/report/obj/Report.php');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$token       = $_GET['token'];
$platform    = 'fnc';
$report_type = 'shvp';
$gruppo      = 'F2A';
$inizio      = strtotime('2023-01-16 00:00:00');
$fine        = strtotime(date('Y-m-d 23:59:59'));


if ($token == 'tv3vEaH0wFu!OTV') {
    $file_name = fopen("/home/admin/public_html/skilledin/report/log_report_manual_log.txt", "a") or die("Unable to open file!");
    require '/home/admin/public_html/vendor/autoload.php';

    $report = new Report();

    switch ($platform) {
        case 'live':
            $servername = "77.39.212.59:3306";
            $username = "livecors_moodle";
            $password = "bn_moodle_01!";
            $dbname = "livecors_moodle";
            $db = "live";
            break;
        case 'f40':
            $servername = "77.39.212.60:3306";
            $username = "formazio_forma40";
            $password = "uh$^^?YFM2l6";
            $dbname = "formazio_formazione40";
            $db = "f40";
            break;
        case 'fnc':
            $servername = "77.39.212.58:3306";
            $username = "fnccorsi_fncmoodle";
            $password = "CWo{}=+CWqU&";
            $dbname = "fnccorsi_fnc";
            $db = "fnc";
            break;
        case 'formaz':
            $servername = "77.39.209.12:3306";
            $username = "formazionecorsin_corsi";
            $password = "W~Q_NPp20X&y";
            $dbname = "formazionecorsin_corsi";
            $db = "formaz";
            break;
        case 'new':
            $servername = "185.81.6.69:3306";
            $username = "newcorsi_moodle";
            $password = "9f.LOcY9ArUt";
            $dbname = "newcorsi_moodle";
            $db = "new";
            break;
    }

    //Connessione a mongodb
    $connection = new MongoDB\Client(
        "mongodb+srv://jobtek:TimeVision2021%40%21%3F@timevision.t9ed5.mongodb.net/TimeVision?retryWrites=true&w=majority");
    $collection = $connection->$db->mdl_logstore_standard_log;
    $conn = new mysqli($servername, $username, $password, $dbname);
    if ($conn->connect_error) {
        fwrite($file_name, date("d-m-Y H:i:s", strtotime('now')) . ": Errore connessione al database: " . $conn->connect_error . "\n");
        die("Connection failed: " . $conn->connect_error);
    }

    fwrite($file_name, date("d-m-Y H:i:s", strtotime('now')) . ": Sono connesso al database: " . $db . "\n");


    $array_id  = $report->getIdUtentiFromAzienda($db, $gruppo);
    $hvpName   = $report->getNameAttivitaHVP($db);
    $nameCorsi = $report->getAllNameCourse($db);
    $array_id  = array_values($array_id);

    //recupero dati
    $res       = $report->getAggregateHvp($db, $inizio, $fine, null, $array_id);
    $result    = $report->constructHvpArray($res, $hvpName, $nameCorsi);

    if (count($result) == 0) {
        echo json_encode($result) . "*?" . count($result);
        fwrite($file_name, date("d-m-Y H:i:s", strtotime('now')) . ": Non sono presenti dati disponibili\n");
        exit();
    }

    $tmp        = $report->constructHvpFinal($result, $db);
    $res        = $report->constructSHvpReportFinal($tmp, $db);
    $res        = explode("*", $res);
    $new        = json_decode($res[0], true);
    $array_mesi = json_decode($res[1], true);

    $total = count($new);

    if ($total != 0) {

        //EXCEL FILE
        /*
        $userid = 0;
        $nome   = "report_hvplog_$gruppo" . "_" . $inizio . "_" . $fine . ".xlsx";
        $now    = date("D, d M Y H:i:s");
        $spreadsheet = new Spreadsheet();
        $filtri      = array();
        $letter      = 'A';
        $j           = 0;
        $report->createCopertina($userid, $spreadsheet, $j);
        $j++;
        $spreadsheet->setActiveSheetIndex($j);
        $index  = $report->createSheet($userid, $spreadsheet, $j);
        $i      = $report->setTimeinSheet($index, $spreadsheet, $letter, $filtri, $j, $new, $array_mesi);
        $report->addSettingsExcel($userid, $spreadsheet, $i);
        $writer = new Xlsx($spreadsheet);
        ob_start();
        $writer->save('/home/admin/public_html/skilledin/report/public/f2a_report.xlsx');
        ob_end_clean();
        */
        //CSV FILE
        $filename = '/home/admin/public_html/skilledin/report/public/f2a_report.csv';

        //INIT
        file_put_contents($filename, '');

        //MAKE CSV
        $index   = 0;
        $letter  = 'A';
        $filtri  = array();
        $j           = 0;
        $report->setTimeinCSV($index, $filename, $letter, $filtri, $j, $new, $array_mesi);

        $response = array(
            'op'   => 'ok',
            'file' => "https://update.corsinrete.com/skilledin/report/public/f2a_report.csv",
            'completed' => date('d/m/Y H:i')
        );
        die(json_encode($response));
    }
    fclose($file_name);
    $conn->close();

}

function slugify($text)
{
    // replace non letter or digits by -
    $text = preg_replace('~[^\pL\d]+~u', '-', $text);
    // transliterate
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
    // remove unwanted characters
    $text = preg_replace('~[^-\w]+~', '', $text);
    // trim
    $text = trim($text, '-');
    // remove duplicate -
    $text = preg_replace('~-+~', '-', $text);
    // lowercase
    $text = strtolower($text);
    if (empty($text)) {
        return 'n-a';
    }
    return $text;
}
