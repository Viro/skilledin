<?php
session_start();

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once($_SERVER['DOCUMENT_ROOT'] . "/skilledin/report/obj/Skilledin.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/skilledin/report/obj/SkilledinWeb.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/skilledin/report/obj/Report.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/skilledin/report/obj/SkilledinReport.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/skilledin/report/obj/Formazione.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/skilledin/report/obj/Formazione40.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/skilledin/report/obj/FondoNuoveCompetenze.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/skilledin/report/obj/Live.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/skilledin/report/obj/PlatformNew.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/skilledin/report/obj/Utility.php");


echo date('d/m/Y H:i:s') . 'Inizio estrazione dati';

$db = 'fnc';
//Nuova gestione classi
$object = false;
if($db == 'formaz') $object = new Formazione($db);
if($db == 'f40')    $object = new Formazione40($db);
if($db == 'fnc')    $object = new FondoNuoveCompetenze($db);
if($db == 'live')   $object = new Live($db);
if($db == 'new')    $object = new PlatformNew($db);


$company_plans = $object->getAziendePianiFormativi(array('company_name' => 'F2A'));
var_dump($company_plans);

?>

