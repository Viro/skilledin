<?php
session_start();

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once($_SERVER['DOCUMENT_ROOT'] . "/skilledin/report/obj/Skilledin.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/skilledin/report/obj/SkilledinWeb.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/skilledin/report/obj/Report.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/skilledin/report/obj/SkilledinReport.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/skilledin/report/obj/Formazione.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/skilledin/report/obj/Formazione40.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/skilledin/report/obj/FondoNuoveCompetenze.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/skilledin/report/obj/Live.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/skilledin/report/obj/PlatformNew.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/skilledin/report/obj/Utility.php");


echo date('d/m/Y H:i:s') . 'Inizio estrazione dati';

$db = 'fnc';
//Nuova gestione classi
$object = false;
if($db == 'formaz') $object = new Formazione($db);
if($db == 'f40')    $object = new Formazione40($db);
if($db == 'fnc')    $object = new FondoNuoveCompetenze($db);
if($db == 'live')   $object = new Live($db);
if($db == 'new')    $object = new PlatformNew($db);

$client['Report'] = new Report();

$company_users = $client['Report']->getIdUtentiFromAzienda('fnc', 'F2A');
$data_inizio   = '13-02-2023';
$data_fine     = '19-02-2023';
$inizio        = strtotime($data_inizio);
$fine          = strtotime($data_fine) + 86400;

$company_users = array(6972);

$filter                  = array();
//$filter['company_id']    = 0;
$filter['company_users'] = $company_users;
$filter['start_from']    = $inizio;
$filter['start_to']      = $fine;
$data                    = $object->getHVPStoredSessions($filter, array('sort' => 'timecreated'));
if($data){
    echo 'Trovati ' . count($data) . ' record<br>';
    echo '<br>';
    echo '<br>';
    $aggregate              = $object->getSHVPReportData($data);
    var_dump($aggregate);
    exit;
}else{
    echo 'Nessun record trovato';
    var_dump($filter);
}




echo '<br>';
echo '<br>';

echo date('d/m/Y H:i:s') . 'Fine estrazione dati';

?>

