<?php
session_start();

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$abs_path = '/home/admin/public_html';
if(isset($_SERVER['DOCUMENT_ROOT']) && strlen(trim($_SERVER['DOCUMENT_ROOT']))) $abs_path = $_SERVER['DOCUMENT_ROOT'];

require_once($abs_path . "/skilledin/report/obj/Skilledin.php");
require_once($abs_path . "/skilledin/report/obj/SkilledinWeb.php");
require_once($abs_path . "/skilledin/report/obj/Report.php");
require_once($abs_path . "/skilledin/report/obj/SkilledinReport.php");
require_once($abs_path . "/skilledin/report/obj/Formazione.php");
require_once($abs_path . "/skilledin/report/obj/Formazione40.php");
require_once($abs_path . "/skilledin/report/obj/FondoNuoveCompetenze.php");
require_once($abs_path . "/skilledin/report/obj/Live.php");
require_once($abs_path . "/skilledin/report/obj/PlatformNew.php");
require_once($abs_path . "/skilledin/report/obj/Utility.php");

//Vecchia gestione delle classi
$skilledin        = new Skilledin();
$client['Report'] = new Report();

//Estrazione dei dati
$object = false;
if($_POST['platform_id'] == 5) $object = new Formazione('formaz');
if($_POST['platform_id'] == 1) $object = new Formazione40('f40');
if($_POST['platform_id'] == 0) $object = new FondoNuoveCompetenze('fnc');
if($_POST['platform_id'] == 2) $object = new Live('live');
if($_POST['platform_id'] == 3) $object = new PlatformNew('new');

//Definizione di web object
if($_POST['platform_id'] == 0) $platform_name = 'fnc';
if($_POST['platform_id'] == 1) $platform_name = 'f40';
if($_POST['platform_id'] == 2) $platform_name = 'live';
if($_POST['platform_id'] == 3) $platform_name = 'new';
if($_POST['platform_id'] == 5) $platform_name = 'formaz';
$skilledin_web = new SkilledinWeb($platform_name);

$filter    = array();
$filter['platform_id'] = $_POST['platform_id'];
$filter['course_id']   = $_POST['course_id'];
$course_modules = $skilledin->getCoursesModulesHVPOLD($filter, array('sort' => 'id'));
if($course_modules){
    foreach ($course_modules as $course_module){
        echo 'Modulo HVP : ' . $course_module['id'] . '<br>';
        $course_module_web = $skilledin_web->getCourseModule($course_module['id']);
        if(!$course_module_web){
            echo 'Modulo non trovato in MYSQL. Procedo con l\'eliminazione<br>';
            $object->deleteHVPCourseModule($course_module['id']);
        }
    }
}


?>

