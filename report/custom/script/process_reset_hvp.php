<?php
session_start();
$abs_path = '/home/admin/public_html';
if(isset($_SERVER['DOCUMENT_ROOT']) && strlen(trim($_SERVER['DOCUMENT_ROOT']))) $abs_path = $_SERVER['DOCUMENT_ROOT'];

require_once($abs_path . "/skilledin/report/obj/Skilledin.php");
require_once($abs_path . "/skilledin/report/obj/SkilledinWeb.php");
require_once($abs_path . "/skilledin/report/obj/Report.php");
require_once($abs_path . "/skilledin/report/obj/SkilledinReport.php");
require_once($abs_path . "/skilledin/report/obj/Formazione.php");
require_once($abs_path . "/skilledin/report/obj/Formazione40.php");
require_once($abs_path . "/skilledin/report/obj/FondoNuoveCompetenze.php");
require_once($abs_path . "/skilledin/report/obj/Live.php");
require_once($abs_path . "/skilledin/report/obj/PlatformNew.php");
require_once($abs_path . "/skilledin/report/obj/Utility.php");

$response = array();
$platform_id   = -1;
$platform_name = '';

if(isset($_GET['platform_id'])) $platform_id = $_GET['platform_id'];

if($platform_id == -1) $platform_name = 'database_test';
if($platform_id == 0)  $platform_name = 'fnc';
if($platform_id == 1)  $platform_name = 'f40';
if($platform_id == 2)  $platform_name = 'live';
if($platform_id == 3)  $platform_name = 'new';
if($platform_id == 5)  $platform_name = 'formaz';

$object = false;
if($platform_id == 5) $object = new Formazione('formaz');
if($platform_id == 1) $object = new Formazione40('f40');
if($platform_id == 0) $object = new FondoNuoveCompetenze('fnc');
if($platform_id == 2) $object = new Live('live');
if($platform_id == 3) $object = new PlatformNew('new');

$skilledin     = new Skilledin();
$skilledin_web = new SkilledinWeb($platform_name);

$filter = array();
$filter['platform_id']    = $platform_id;
$filter['date_completed'] = 0;
$filter['status']         = 0;
if(isset($_GET['key'])) $filter['key'] = $_GET['key'];
$scheduled = $skilledin->getScheduledResetLogHPV($filter);
if($scheduled){
    $reset_count = 0;
    foreach ($scheduled as $job){
        if(isset($job['contextinstanceid']) && $job['contextinstanceid'] > 0 && isset($job['module_id'])) unset($job['module_id']);

        $filter = array();
                                               $filter['platform_id']       = $platform_id;
        if(strlen(trim($job['company_name']))) $filter['company_name']      = $job['company_name'];
        if($job['user_id'] > 0)                $filter['user_id']           = $job['user_id'];
        if($job['course_id'] > 0)              $filter['course_id']         = $job['course_id'];
        if($job['module_id'] > 0)              $filter['module_id']         = $job['module_id'];
        if($job['contextinstanceid'] > 0)      $filter['contextinstanceid'] = $job['contextinstanceid'];
                                               $filter['component']         = 'mod_hvp';
        $logs = $skilledin->getDataForHVPReset($filter);
        if(count($logs)){
            $resetted_logs = array();
            foreach($logs as $log){
                $reset_response = $skilledin->resetLogHVP($log['id'], array('reset' => $job['reset_value']), $platform_id);
                $reset_count++;
                if($job['reset_value']){
                    if(!isset($resetted_logs[$log['userid']]))                                              $resetted_logs[$log['userid']]                                              = array();
                    if(!isset($resetted_logs[$log['userid']][$log['courseid']]))                            $resetted_logs[$log['userid']][$log['courseid']]                            = array();
                    if(!isset($resetted_logs[$log['userid']][$log['courseid']][$log['contextinstanceid']])){
                        $resetted_logs[$log['userid']][$log['courseid']][$log['contextinstanceid']] = array('objectid'    => $log['objectid'],
                                                                                                            'contextlevel' => $log['contextlevel'],
                                                                                                            'contextid'    => $log['contextid'],
                                                                                                            'time'         => time());
                    }else{
                        if((int) $log['objectid'] > 0) $resetted_logs[$log['userid']][$log['courseid']][$log['contextinstanceid']]['objectid'] = $log['objectid'];
                    }
                }
            }

            //Acquisisco log consolidati per il reset
            $filter = array();
            $filter['user_id']   = $job['user_id'];
            $filter['course_id'] = $job['course_id'];
            $filter['module_id'] = $job['module_id'];
            $stored_data = $object->getHVPStoredSessions($filter);
            if($stored_data){
                foreach ($stored_data as $item){
                    $item['reset'] = true;
                    $object->insertReportLog($item);
                }
            }

            //Eseguo reset su MySQL
            if($job['reset_value']){
                foreach($resetted_logs as $user_id => $courses){
                    foreach($courses as $course_id => $instances){
                        foreach($instances as $contextinstanceid => $item){
                            $data = array();
                            $data['eventname']         = '\\mod_hvp\\event\\course_module_progress';
                            $data['component']         = 'mod_hvp';
                            $data['action']            = 'progress';
                            $data['target']            = 'course_module';
                            $data['objecttable']       = 'hvp';
                            $data['objectid']          = $item['objectid'];
                            $data['crud']              = 'r';
                            $data['edulevel']          = 2;
                            $data['contextid']         = $item['contextid'];
                            $data['contextlevel']      = $item['contextlevel'];
                            $data['contextinstanceid'] = $contextinstanceid;
                            $data['userid']            = $user_id;
                            $data['courseid']          = $course_id;
                            $data['other']             = 'RESET';
                            $data['timecreated']       = $item['time'];
                            $data['origin']            = 'web';
                            $data['ip']                = '79.9.102.200';

                            //Inserisco il record in MySQL
                            $data['id']    = $skilledin_web->insertManualLog($data);
                            $data['reset'] = (bool) $job['reset_value'];

                            //Inserisco il record in MongoDB
                            $skilledin->insertManualLog($data);

                            $data = array();
                            $data['eventname']         = '\\mod_hvp\\event\\course_module_viewed';
                            $data['component']         = 'mod_hvp';
                            $data['action']            = 'viewed';
                            $data['target']            = 'course_module';
                            $data['objecttable']       = 'hvp';
                            $data['objectid']          = $item['objectid'];
                            $data['crud']              = 'r';
                            $data['edulevel']          = 2;
                            $data['contextid']         = $item['contextid'];
                            $data['contextlevel']      = $item['contextlevel'];
                            $data['contextinstanceid'] = $contextinstanceid;
                            $data['userid']            = $user_id;
                            $data['courseid']          = $course_id;
                            $data['other']             = serialize("RESET");
                            $data['timecreated']       = $item['time'];
                            $data['origin']            = 'web';
                            $data['ip']                = '79.9.102.200';

                            //Inserisco il record in MySQL
                            $data['id']    = $skilledin_web->insertManualLog($data);
                            $data['reset'] = (bool) $job['reset_value'];


                            echo 'Resetto il content user data in MySQL<br>';
                            //Resetto il content user data in MySQL
                            $hvp_content = $skilledin_web->getHVPContentUserDataByObject($data['userid'], $data['objectid']);
                            if($hvp_content){
                                $hvp_content = array_shift($hvp_content);
                                $skilledin_web->editHVPContentUserData($hvp_content['id'], array('data' => 'RESET'));
                            }else{
                                echo 'Non ho trovato il content HVP';
                                var_dump($data['userid']);
                                var_dump($data['objectid']);
                            }

                            echo 'Resetto il course module completion<br>';
                            //Resetto il course module completion
                            $course_module = $skilledin_web->getCourseModuleByCourseIstance($data['courseid'], $data['objectid']);
                            if($course_module){
                                $course_module = array_shift($course_module);
                                $course_module_completion = $skilledin_web->getCourseModuleCompletionByUserCourseModule($data['userid'], $course_module['id']);
                                if($course_module_completion){
                                    $course_module_completion = array_shift($course_module_completion);
                                    $skilledin_web->editCourseModuleCompletion($course_module_completion['id'], array('completionstate' => 0, 'viewed' => 0));
                                }else{
                                    echo 'Non ho trovato il modulo completato';
                                    var_dump($data['courseid']);
                                    var_dump($data['objectid']);
                                }
                            }else{
                                echo 'Non ho trovato il modulo';
                                var_dump($data['courseid']);
                                var_dump($data['objectid']);
                            }

                            //Inserisco il record in MongoDB
                            $skilledin->insertManualLog($data);
                        }
                    }
                }
            }
            $return = $skilledin->completeScheduledResetLogHPV($job['key'], $reset_count, $platform_id);
            $response['log_message'] = 'Job eseguito correttamente';
            echo 'Eseguito job ' . $job['key'] . '<br>';
        }else{
            $response['log_message'] = 'Non sono stati trovati altri log da resettare per il job schedulato';
        }
    }
}else{
    $response['log_message'] = 'Non sono stati trovati log da resettare schedulati';
}

echo json_encode($response);
if(isset($_GET['key'])) echo "<script>window.close();</script>";

 ?>

