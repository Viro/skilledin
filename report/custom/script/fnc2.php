<?php

ini_set('memory_limit', '-1');
ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);
ini_set('max_execution_time', 0);
error_reporting(E_ALL);

require '/home/admin/public_html/vendor/autoload.php';
include("/home/admin/public_html/skilledin/report/obj/Report.php");
//require '../../../../../vendor/autoload.php';


$servername = "77.39.212.58:3306";
$username = "fnccorsi_fncmoodle";
$password = "CWo{}=+CWqU&";
$dbname = "fnccorsi_fnc";
$db ="fnc";

$abs_path = '/home/admin/public_html';
if(isset($_SERVER['DOCUMENT_ROOT']) && strlen(trim($_SERVER['DOCUMENT_ROOT']))) $abs_path = $_SERVER['DOCUMENT_ROOT'];

require_once($abs_path . "/skilledin/report/obj/Skilledin.php");
require_once($abs_path . "/skilledin/report/obj/Utility.php");

$categories_id = "'98','99','118','121','122','123','124','125','128','129','130','133','134','135','136','137','138','139','140','141','142','143','144','145','147','148','149','150','151','152','153','154','155','157','158','159','160','161','162','163','167','168','169','170','171','174','175','176','177','181','184','185','186','188','190','192','193','194','195','196','197','198','201','202','204','206','208','211','214','215','217','218','221','222','224','232','233','234','238','239','244','247','248','257','264','266','271','275','278','282','283','284','287','298','303','305','306','101','205','207','209','210','216','226','227','228','229','230','240','253','258','260','268','116','117','156','166','173','236','243','255','274','131','132','146','164','165','178','179','180','242','246','270','272','273','279','288','291','182','183','187','189','191','199','200','203','212','219','220','223','225','235','241','245','256','265','267','249','250','251','252','254','259','261','262','263','269','276','277','280','281','285','286','289','290','292','293','294','295','296','297','299','300','301','302','304','307','308','309','310','311','312','313','314','315','316','317','318','319','320'";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

//Connessione a mongodb
$connection = new MongoDB\Client(
    "mongodb+srv://jobtek:TimeVision2021%40%21%3F@timevision.t9ed5.mongodb.net/TimeVision?retryWrites=true&w=majority");

$collection = $connection->$db->mdl_logstore_standard_log;

$client['Report'] = new Report();
$skilledin = new Skilledin();
$tool      = new Utility();

$sql = "SELECT id, fullname FROM `mdl_course` WHERE category IN ($categories_id) ORDER BY id ASC";
//echo $sql;
//exit;

$resultCourse = $conn->query($sql);

//echo $result->num_rows;exit();
if ($resultCourse->num_rows > 0) {

    $courses = [];

    while ($row = $resultCourse->fetch_assoc()) {

        echo $row['id'] . "\n";
        echo $row['name'] . "\n";
        echo "-------------------------\n\n";

        $filter    = array();
        $filter['platform_id'] = 0; //fnc
        $filter['course_id']   = $row['id'];
        $course_modules = $skilledin->getCoursesModulesHVPOLD($filter, array('sort' => 'id'));

        $total_time  = 0;
        $total_count = 0;

        if($course_modules){
            foreach($course_modules as $course_module){
                $course_module['object_time'] = '00:00:00';
                $split = isset($course_module['json_content']) ? explode('"time":', $course_module['json_content']) : array();

                if (isset($split[1])) {
                    $split = explode(",", $split[1]);
                    $split = explode(".", $split[0]);
                    $course_module['object_time'] = $split[0];
                }

                $total_time+= $course_module['object_time'];
            }
        }

        array_push($courses, array('id' => $row['id'], 'name' => $row['fullname'], 'time' => $tool->convertSecondsToTime($total_time) ) );
    }
}

if(count($courses) > 0){

    $fh = fopen($abs_path . "/skilledin/report/csv/fnc2.csv", 'w');

    fputcsv($fh, array('Nome Azienda', 'Nome', 'Codice Fiscale', 'Corso', 'Tempo totale del corso', 'Totale cumulato su corso') );

    $hvpName = $client['Report']->getNameAttivitaHVP($db);

    foreach($courses as $course){

        $users_id = [];

        $match = ['$match' => [
            'courseid' => (int) $course['id'],
            'component' => "mod_hvp",
            'timecreated' => [
                '$gte' => strtotime('2023-05-15'),
                '$lte' => strtotime('now')
            ]
        ]];

        $pipeline = [
            $match,
            ['$group' => ['_id' => '$userid']]
            ];

        $users = $collection->aggregate($pipeline)->toArray();

        if(count($users) > 0){
            foreach($users as $user){
                $users_id[] = $user['_id'];
            }

            if(count($users_id) > 0){
                $res = $client['Report']->getAggregateHvp($db, strtotime('2023-05-15'), strtotime('now'), $course['id'], $users_id);
                $result = $client['Report']->constructHvpArray($res, $hvpName, $course['name']);

                if (count($result) > 0) {
                    $tmp = $client['Report']->constructHvpFinal($result, $db);
                    $res = $client['Report']->constructSHvpReportFinal($tmp, $db);
                    $res = explode("*", $res);

                    if(count($res) > 0){
                        $data = json_decode($res[0], true);
                        foreach($data as $d){
                            fputcsv($fh, array($d['azienda'], $d['name'], $d['cf'], $course['name'], $course['time'], $d['tempo_tot'] ));

                        }
                    }
                }
            }
        }

    }

    fclose($fh);
}

$conn->close();
