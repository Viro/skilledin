<?php
session_start();

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$abs_path = '/home/admin/public_html';
if(isset($_SERVER['DOCUMENT_ROOT']) && strlen(trim($_SERVER['DOCUMENT_ROOT']))) $abs_path = $_SERVER['DOCUMENT_ROOT'];

//MySQL
require_once($abs_path . "/skilledin/report/obj/SkilledinWeb.php");

//MongoDB
require_once($abs_path . "/skilledin/report/obj/SkilledinReport.php");
require_once($abs_path . "/skilledin/report/obj/Admin.php");
require_once($abs_path . "/skilledin/report/obj/Formazione.php");
require_once($abs_path . "/skilledin/report/obj/Formazione40.php");
require_once($abs_path . "/skilledin/report/obj/FondoNuoveCompetenze.php");
require_once($abs_path . "/skilledin/report/obj/Live.php");
require_once($abs_path . "/skilledin/report/obj/PlatformNew.php");
require_once($abs_path . "/skilledin/report/obj/Utility.php");

$object = new Admin('admin');

//Estrazione delle piattaforme
$platforms = $object->getPlatforms();
if($platforms){
    foreach ($platforms as $platform){
        $subobject = false;
        if($platform['id'] == 5) $subobject = new Formazione('formaz');
        if($platform['id'] == 1) $subobject = new Formazione40('f40');
        if($platform['id'] == 0) $subobject = new FondoNuoveCompetenze('fnc');
        if($platform['id'] == 2) $subobject = new Live('live');
        if($platform['id'] == 3) $subobject = new PlatformNew('new');
        if(!$subobject) continue;

        $companies = $subobject->getCompaniesFromUsers();
        if($companies){
            foreach($companies as $company_name){
                $data = array();
                $data['platform_id']   = $platform['id'];
                $data['platform_name'] = $platform['nome'];
                $data['company_name']  = $company_name;
                $insert = $object->addAziendaPiattaforma($data);
                $object->LogPrint("Inserita azienda");
            }
        }

    }
    $object->LogPrint("Operazione completata");
}



?>

