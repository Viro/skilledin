#!/usr/bin/php
<?php
session_start();

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$abs_path = '/home/admin/public_html';
if(isset($_SERVER['DOCUMENT_ROOT']) && strlen(trim($_SERVER['DOCUMENT_ROOT']))) $abs_path = $_SERVER['DOCUMENT_ROOT'];

require_once($abs_path . "/skilledin/report/obj/Skilledin.php");
require_once($abs_path . "/skilledin/report/obj/SkilledinWeb.php");
require_once($abs_path . "/skilledin/report/obj/Report.php");
require_once($abs_path . "/skilledin/report/obj/SkilledinReport.php");
require_once($abs_path . "/skilledin/report/obj/Formazione.php");
require_once($abs_path . "/skilledin/report/obj/Formazione40.php");
require_once($abs_path . "/skilledin/report/obj/FondoNuoveCompetenze.php");
require_once($abs_path . "/skilledin/report/obj/Live.php");
require_once($abs_path . "/skilledin/report/obj/PlatformNew.php");
require_once($abs_path . "/skilledin/report/obj/Utility.php");

//Vecchia gestione delle classi
$skilledin        = new Skilledin();
$client['Report'] = new Report();


//var_dump($argv[1]); exit;

//Verifico selezione della piattaforma
if(!isset($_GET['platform_id']) && !isset($argv[1])){
    echo 'Manca piattaforma';
    var_dump($_GET);
    var_dump($argv);
    exit;
}

//Definizione della piattaforma da consolidare
$platform_id = $_GET['platform_id'] ?? $argv[1];
$platform    = $skilledin->getPlatform($platform_id);

$cron = array();
$cron['platform_id']   = $platform['id'];
$cron['platform_name'] = $platform['nome'];
$cron['job_name']      = 'process_report_data';
$cron['job_started']   = date('Y-m-d H:i:s');
if(isset($_GET)) $cron['other'] = json_encode($_GET);
if(isset($argv)) $cron['other'] = json_encode($argv);
$skilledin->addCronHistory($cron);

//Estrazione dei dati
$object = false;
if($platform['id'] == 5) $object = new Formazione('formaz');
if($platform['id'] == 1) $object = new Formazione40('f40');
if($platform['id'] == 0) $object = new FondoNuoveCompetenze('fnc');
if($platform['id'] == 2) $object = new Live('live');
if($platform['id'] == 3) $object = new PlatformNew('new');

//Calcolo della data ultima di aggiornamento della piattaforma
$data_start_job = $object->getLastDateLog(array('platform' => $object->database));
$data_start_job = ($data_start_job) ? date('d-m-Y', strtotime(str_replace("/", "-", $data_start_job))) : '02-01-2022';
$data_end_job   = $data_start_job;
if(isset($_GET['data_job']) || isset($argv[2])){
    $user_data_job = $_GET['data_job'] ?? $argv[2];
    //Se l'utente vuole eseguire un aggiornamento a partire da una data specifica, questa viene confrontata con la data di ultimo aggiornamento
    //La data d'inizio aggiornamento non può essere successiva al giorno di ultima sincronizzazione
    if($user_data_job == 'current_date') $user_data_job = date('d-m-Y');
    $data_end_job = $user_data_job;

    $origin = new DateTimeImmutable(date('Y-m-d', strtotime($data_end_job)));
    $target = new DateTimeImmutable(date('Y-m-d', strtotime($user_data_job)));
    $interval = $origin->diff($target);

    //Se la data richiesta è precedente alla data di ultimo job, allora ricostruisci dalla data indicata
    if($interval->invert) $data_start_job = $user_data_job;
}

$object->LogPrint('Data job di riferimento : ' . $data_start_job);

$data_inizio      = date('d-m-Y 00:00:00', strtotime('-1 day', strtotime($data_start_job)));
$data_fine        = date('d-m-Y 23:59:59', strtotime('-1 day', strtotime($data_end_job)));

$object->LogPrint('Inizio log : ' . $data_inizio);
$object->LogPrint('Fine log   : ' . $data_fine);
//exit;


$object->LogPrint('Inizio piattaforma : ' . $object->database . '(ID : ' . $platform['id'] . ')');
$companies = $skilledin->getCompaniesOLD(array('platform_id' => $platform['id']));
if ($companies) {
    $object->LogPrint('Trovate ' . count($companies) . ' aziende da elaborare');
    //foreach ($companies as $company) {
       // $company_users = $client['Report']->getIdUtentiFromAzienda($object->database, $company);
        $filter                  = array();
        //$filter['company_id']  = 0;
        //$filter['company_users'] = $company_users;
        $filter['start_from']    = strtotime($data_inizio);
        $filter['start_to']      = strtotime($data_fine);
        $data                    = $object->getHVPSessions($filter, array('sort' => 'timecreated'));
        if($data){
            $total_record = count($data);
            //$object->LogPrint('Azienda : ' . $company);
            $object->LogPrint('Trovati record da elaborare: ' . $total_record);
            $count = 0;
            $progression = array();
            foreach($data as $item){
                //Calcolo i dati
                set_time_limit(0);
                $item['work_time']   = (float) $item['work_time'];
                $item['object_time'] = (float) $item['object_time'];
                $item['platform']    = $object->database;
                $key = $item['user_id'] . '_' . $item['course_id'] . '_' . $item['object_id'];
                if(!isset($progression[$key]) || $item['reset']) $progression[$key] = 0;

                if($item['work_time'] >  $progression[$key])  $progression[$key] = $item['work_time'];
                $item['progress_time'] = (float) $progression[$key];

                //Compara con il log immediatamente precedente
                if(!$item['reset']){
                    $filter['log_id']    = $item['log_id'];
                    $filter['user_id']   = $item['user_id'];
                    $filter['course_id'] = $item['course_id'];
                    $filter['object_id'] = $item['object_id'];
                    if( $platform['id'] == 0) $object->LogPrint('Inzio ricerca ultimo progresso per utente ' . $item['user_id'] . ', Corso ' . $item['course_id'] . ' > ' . $item['object_id']);
                    $last_progress        = $object->getLastReportLog($filter);
                    if( $platform['id'] == 0) $object->LogPrint('Fine ricerca ultimo progresso per utente ' . $item['user_id'] . ', Corso ' . $item['course_id'] . ' > ' . $item['object_id']);


                        if($last_progress && $last_progress['progress_time'] > $item['progress_time']){
                        //$object->LogPrint('Il progress time dell\'ultima sessione super il progress attuale');
                        //$object->LogPrint('Progress time (item) : ' . $item['progress_time']);
                        //$object->LogPrint('Progress time (last) : ' . $last_progress['progress_time']);

                        $item['progress_time'] = $last_progress['progress_time'];

                    }
                }
                $object->LogPrint("Upsert log " . $item['log_id'] . " data: " . $item['date']);
                $insert = $object->insertReportLog($item);
                $count++;
                if (PHP_SAPI === 'cli'){
                    $object->LogPrint("Elaborati $count record di $total_record");
                }else{
                    if($count%100==0) $object->LogPrint("Elaborati $count record");
                }
            }
            $object->LogPrint("Elaborati $count record");
            //if( $platform['id'] == 0) exit;
        }
   // }
}
$object->LogPrint('Fine piattaforma : ' . $object->database);
$skilledin->setCompletdCronHistory($cron);


$origin = new DateTimeImmutable(date('Y-m-d', strtotime($data_end_job)));
$target = new DateTimeImmutable(date('Y-m-d'));
$interval = $origin->diff($target);
if($interval->invert >= 0){
    $next_day     = date('d-m-Y', strtotime('+1 day', strtotime($data_end_job)));
    $new_location = "https://update.corsinrete.com/skilledin/report/custom/script/process_report_data.php?platform_id=" . $platform['id'] . "&data_job=" . $next_day;
    if (PHP_SAPI === 'cli'){
        $object->LogPrint("Operazione completata.");
    }else{
        echo 'Continuo con job : <a href="' . $new_location . '">' . $next_day . '</a>';
        ?>
        <script type="text/javascript">
            window.location = '<?php echo $new_location; ?>';
        </script>
        <?php
    }
}
?>

