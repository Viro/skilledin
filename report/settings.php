<?php
//exit("WORK IN PROGRESS");
session_start();

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require '../../vendor/autoload.php';

if (!isset($_SESSION['user']) || ($_SESSION['user']['idRuolo'] != 0 && $_SESSION['user']['idRuolo'] != 1)) {
    header("location:index.php");
}

$from="index.php";
if (isset($_GET['from'])) {
    if ($_GET['from'] == "confirm")
        $from = "confirmPage.php";
    
}

$collection = new MongoDB\Client(
            "mongodb+srv://jobtek:TimeVision2021%40%21%3F@timevision.t9ed5.mongodb.net/TimeVision?retryWrites=true&w=majority");
$copertina = $collection->admin->settings_excel_copertina;
$sheet = $collection->admin->settings_excel_sheet;

$userid = $_SESSION['user']['id'];

$cursor = $copertina->find(
        ['userid' => (int) $userid],
        ['projection' => ['txt1' => 1, 'txt2' => 1, 'img' => 1],
            'limit' => 1,
            'sort' => ['_id' => -1],
        ]
);
$txt1 = "";
$txt2 = "";
$src = "";
$s1 = "";
$s2 = "";
$s3 = "";
$t1 = "";
foreach ($cursor as $singolo) {
    $txt1 = $singolo['txt1'];
    $txt2 = $singolo['txt2'];
    if ($singolo['img'] != "")
        $src = $singolo['img'];
}

$cursor2 = $sheet->find(
        ['userid' => (int) $userid],
        ['projection' => ['s1' => 1, 's2' => 1, 'timbro' => 1, 't1' => 1],
            'limit' => 1,
            'sort' => ['_id' => -1],
        ]
);


foreach ($cursor2 as $singolo) {
    if ($singolo['s1'] != "")
        $s1 = $singolo['s1'];
    if ($singolo['s2'] != "")
        $s2 = $singolo['s2'];
    if ($singolo['timbro'] != "")
        $s3 = $singolo['timbro'];
    $t1 = $singolo['t1'];
}


if (isset($_POST['salva'])) {
    $txt1 = trim($_POST['txt1']);
    $txt2 = trim($_POST['txt2']);
    $t1 = trim($_POST['t1']);
    $userid = $_SESSION['user']['id'];


    if (isset($_FILES)) {

        if ($_FILES['copertina']['error'] == 0)
            $src = getFileName($_FILES['copertina']);
        if ($_FILES['s1']['error'] == 0)
            $s1 = getFileName($_FILES['s1']);
        if ($_FILES['s2']['error'] == 0)
            $s2 = getFileName($_FILES['s2']);
        if ($_FILES['s3']['error'] == 0)
            $s3 = getFileName($_FILES['s3']);
    }
    insertSettingsCopertina($userid, $txt1, $txt2, $src, $copertina);
    insertSettingsSheet($userid, $s1, $s2, $s3, $t1, $sheet);
    header("location:settings.php");
}

/*
 * Funzioni  
 */

function getFileName($file) {
    $path = pathinfo($file["name"]);
    $fileName = "img" . time() . rand(11, 99) . "_" . "tvexc" . "." . $path['extension'];
    move_uploaded_file($file['tmp_name'], "img/" . $fileName);
    return $fileName;
}

function insertSettingsCopertina($userid, $txt1, $txt2, $filename, $collection) {

    $orario = date("H:i:s");
    $data = date('Ymd');

    $cursor = $collection->find(
            [],
            ['projection' => ['id' => 1],
                'limit' => 1,
                'sort' => ['id' => -1],
            ]
    );
    $lastId = 0;
    foreach ($cursor as $singolo) {
        $lastId = $singolo['id'] + 1;
    }
    $collection->insertOne([
        'id' => (int) $lastId,
        'userid' => (int) $userid,
        'target' => "copertina",
        'txt1' => $txt1,
        'txt2' => $txt2,
        'img' => $filename,
        'data' => (int) $data,
        'orario' => $orario
    ]);
}

function insertSettingsSheet($userid, $s1, $s2, $s3, $t1, $collection) {

    $orario = date("H:i:s");
    $data = date('Ymd');

    $cursor = $collection->find(
            [],
            ['projection' => ['id' => 1],
                'limit' => 1,
                'sort' => ['id' => -1],
            ]
    );
    $lastId = 0;
    foreach ($cursor as $singolo) {
        $lastId = $singolo['id'] + 1;
    }
    $collection->insertOne([
        'id' => (int) $lastId,
        'userid' => (int) $userid,
        'target' => "sheet",
        's1' => $s1,
        's2' => $s2,
        'timbro' => $s3,
        't1' => $t1,
        'data' => (int) $data,
        'orario' => $orario
    ]);
}
?>

<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->


        <link href='..lib/main.css' rel='stylesheet' />
        <link href='../css/bootstrap_fix.css' rel='stylesheet' />

        <!-- Bootstrap 5 JS-->
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js"
                integrity="sha384-SR1sx49pcuLnqZUnnPwx6FCym0wLsk5JZuNx2bPPENzswTNFaQU1RDvt3wT4gWFG"
        crossorigin="anonymous"></script>
        <!-- JavaScript Bundle with Popper -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
                integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4"
        crossorigin="anonymous"></script>

        <title>Settings</title>


        <style>

            body{
                background: #f5f5f5;
                margin-top:20px;
            }



            .btn-default {
                border-color: rgba(24,28,33,0.1);
                background: rgba(0,0,0,0);
                color: #4E5155;
            }

            label.btn {
                margin-bottom: 0;
            }

            .btn-outline-primary {
                border-color: #26B4FF;
                background: transparent;
                color: #26B4FF;
            }

            .btn {
                cursor: pointer;
            }

            .text-light {
                color: #babbbc !important;
            }


            .card {
                background-clip: padding-box;
                box-shadow: 0 1px 4px rgba(24,28,33,0.012);
            }

            .row-bordered {
                overflow: hidden;
            }

            .account-settings-fileinput {
                position: absolute;
                visibility: hidden;
                width: 1px;
                height: 1px;
                opacity: 0;
            }
            .account-settings-links .list-group-item.active {
                font-weight: bold !important;
            }
            html:not(.dark-style) .account-settings-links .list-group-item.active {
                background: transparent !important;
            }
            .account-settings-multiselect ~ .select2-container {
                width: 100% !important;
            }
            .light-style .account-settings-links .list-group-item {
                padding: 0.85rem 1.5rem;
                border-color: rgba(24, 28, 33, 0.03) !important;
            }
            .light-style .account-settings-links .list-group-item.active {
                color: #4e5155 !important;
            }
            .material-style .account-settings-links .list-group-item {
                padding: 0.85rem 1.5rem;
                border-color: rgba(24, 28, 33, 0.03) !important;
            }
            .material-style .account-settings-links .list-group-item.active {
                color: #4e5155 !important;
            }
            .dark-style .account-settings-links .list-group-item {
                padding: 0.85rem 1.5rem;
                border-color: rgba(255, 255, 255, 0.03) !important;
            }
            .dark-style .account-settings-links .list-group-item.active {
                color: #fff !important;
            }
            .light-style .account-settings-links .list-group-item.active {
                color: #4E5155 !important;
            }
            .light-style .account-settings-links .list-group-item {
                padding: 0.85rem 1.5rem;
                border-color: rgba(24,28,33,0.03) !important;
            }
            * {
                box-sizing: border-box;
            }

            .column {
                float: left;
                width: 50%;
                padding: 5px;

            }

            .row::after {
                content: "";
                clear: both;
                display: table;
            }

            img[src="img/"] {
                display: none;
            }
        </style>
    </head>

    <body>
        <div class="container light-style flex-grow-1 container-p-y">
            <a href="<?php echo $from; ?>" class="previous" style="text-decoration: none;">‹ Torna indietro</a>
            <h4 class="font-weight-bold py-3 mb-4">
                Impostazioni Excel 
            </h4>

            <div class="card overflow-hidden">
                <form method="POST" action="settings.php" enctype="multipart/form-data">
                    <div class="row no-gutters row-bordered row-border-light" style="height:500px;overflow: auto;">
                        <div class="col-md-3 pt-0">
                            <div class="list-group list-group-flush account-settings-links">
                                <a class="list-group-item list-group-item-action active"  data-bs-toggle="pill"
                                   data-bs-target="#setting-copertina">Copertina</a>

                                <a class="list-group-item list-group-item-action"  data-bs-toggle="pill"
                                   data-bs-target="#setting-sheet">Sheet</a>

                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="tab-content">
                                <div class="tab-pane fade active show" id="setting-copertina">

                                    <div class="card-body media align-items-center">
                                        <img src="<?php echo "img/" . $src; ?>" id="img_copertina" alt="" style="width: 100% !important;height: 250px;">
                                        <div class="media-body ml-4">
                                            <label class="btn btn-outline-primary" style="display: grid;">
                                                Upload
                                                <input type="file" name="copertina" id="upload_image"class="account-settings-fileinput" onchange="imagePreview(this.id)" accept="image/gif,image/jpeg,image/jpg,image/png,image/bmp">
                                            </label>


                                            <div class="text-light small mt-1">Carica immagine per intestazione (S1)</div>
                                        </div>
                                    </div>
                                    <hr class="border-light m-0">

                                    <div class="card-body">
                                        <div class="form-group">
                                            <label class="form-label">Inserisci testo per sezione TXT1</label>
                                            <textarea class="form-control" name="txt1" id="txt1" rows="5"><?php echo $txt1; ?></textarea>
                                        </div>

                                        <div class="form-group">
                                            <label class="form-label">Inserisci testo per sezione TXT2</label>
                                            <textarea class="form-control" name="txt2" id="txt2" rows="5"><?php echo $txt2; ?></textarea>
                                        </div>
                                    </div>
                                    <hr class="border-light m-0">
                                    <div class="card-body">
                                        <img src="img/format_copertina_report.png" alt="" style="width: 70%;">
                                        <div class="text-light small mt-1">Esempio formato copertina</div>
                                    </div>

                                </div>
                                <div class="tab-pane fade" id="setting-sheet">


                                    <div class="card-body media align-items-center">
                                        <img src="<?php echo "img/" . $s1; ?>" id="img_sheet" alt="S1" style="width: 100% !important;height: 250px;">
                                        <div class="media-body ml-4">
                                            <label class="btn btn-outline-primary" style="display: grid;">
                                                Upload
                                                <input type="file" name="s1" id="upload_image_s1"class="account-settings-fileinput" onchange="imagePreview(this.id)" accept="image/gif,image/jpeg,image/jpg,image/png,image/bmp">
                                            </label>


                                            <div class="text-light small mt-1">Carica immagine per intestazione (S1)</div>
                                        </div>
                                    </div>
                                    <hr class="border-light m-0">


                                    <div class="card-body">
                                        <div class="form-group">
                                            <label class="form-label">Inserisci testo per sezione T1</label>
                                            <textarea class="form-control" name="t1" id="t1" rows="5"><?php echo $t1; ?></textarea>
                                        </div>
                                    </div>
                                    <hr class="border-light m-0">
                                    <div class="row" style="margin-top: 15px">
                                        <div class="column">
                                            <img src="<?php echo "img/" . $s2; ?>" id="img_s2" alt="S2" style="width:100%;height: 50%;">
                                            <div class="media-body ml-4">
                                                <label class="btn btn-outline-primary" style="width: 100%;">
                                                    Upload
                                                    <input type="file" name="s2" id="upload_image_s2"class="account-settings-fileinput" onchange="imagePreview(this.id)" accept="image/gif,image/jpeg,image/jpg,image/png,image/bmp">
                                                </label>
                                                <div class="text-light small mt-1">Carica immagine per S2</div>
                                            </div>
                                        </div>
                                        <div class="column">
                                            <img src="<?php echo "img/" . $s3; ?>" id="img_s3" alt="S3" style="width:100%;height: 50%;">
                                            <div class="media-body ml-4">
                                                <label class="btn btn-outline-primary" style="width: 100%;">
                                                    Upload
                                                    <input type="file" name="s3" id="upload_image_s3"class="account-settings-fileinput" onchange="imagePreview(this.id)" accept="image/gif,image/jpeg,image/jpg,image/png,image/bmp">
                                                </label>
                                                <div class="text-light small mt-1">Carica immagine per timbro(S3)</div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr class="border-light m-0">
                                    <div class="card-body">
                                        <img src="img/format_sheet_report.png" alt="" style="width: 70%;">
                                        <div class="text-light small mt-1">Esempio formato sheet</div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="text-right mt-3" style="display: flex;justify-content:end;margin-bottom: 10px;">
                        <button type="submit" class="btn btn-primary" id="salva" name="salva">Salva</button>&nbsp;
                        <button type="submit" class="btn btn-default" id="annulla" name="annulla">Annulla</button>
                    </div>
                </form>


            </div>
            <script>
                function imagePreview(id) {
                    var idPrw = "";
                    switch (id) {
                        case "upload_image":
                            idPrw = "img_copertina";
                            break;
                        case "upload_image_s1":
                            idPrw = "img_sheet";
                            break;
                        case "upload_image_s2":
                            idPrw = "img_s2";
                            break;
                        case "upload_image_s3":
                            idPrw = "img_s3";
                            break;
                        default:
                            break;

                    }
                    var PreviewIMG = document.getElementById(idPrw);
                    var UploadFile = document.getElementById(id).files[0];
                    PreviewIMG.src = URL.createObjectURL(UploadFile);
                }

                function check() {

                    var txt1 = document.getElementById('txt1').value;
                    var txt2 = document.getElementById('txt2').value;
                    var t1 = document.getElementById('t1').value;
                    if (txt1.length == 0 || txt2.length == 0 || t1.length == 0) {
                        alert("Uno o piu' campi sono vuoti");
                        return false;
                    }

                    return true;
                }

            </script>
    </body>
</html>
