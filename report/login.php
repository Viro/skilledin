<?php
session_start();

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

error_reporting(E_ALL);
require '../../vendor/autoload.php';

if (isset($_SESSION['user'])) {
    if ($_SESSION['user']['idRuolo'] != 0)
        header("location:index.php");
    else
        header("location:confirmPage.php?admin");
}


if (isset($_POST['email']) && isset($_POST['password'])) {

    $email = strtolower(trim($_POST['email']));
    $password = trim($_POST['password']);
    $password = md5($password);

    $collection = new MongoDB\Client(
            "mongodb+srv://jobtek:TimeVision2021%40%21%3F@timevision.t9ed5.mongodb.net/TimeVision?retryWrites=true&w=majority");
    $collection = $collection->admin->utenti;

    $cursor = $collection->find(
            ['email' => $email, 'password' => $password],
            ['projection' => ['id' => 1, 'idRuolo' => 1, 'idPiattaforma' => 1]]
    );

    foreach ($cursor as $value) {
        $_SESSION['user']['id'] = $value->id;
        $_SESSION['user']['idRuolo'] = $value->idRuolo;
        $_SESSION['user']['idPiattaforma'] = $value->idPiattaforma;
        if ($value->idRuolo == 0)
            header("location:confirmPage.php?admin");
        else
            header("location:index.php");
        exit();
    }

    header("location:login.php?err");
}
?>

<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css">

        <title>Accedi</title>


        <style>

            .field-icon {
                float: right;
                margin-left: -35px;
                margin-top: -35px;
                position: relative;
                z-index: 2;
            }

            .btn-login {
                font-size: 0.9rem;
                letter-spacing: 0.05rem;
                padding: 0.75rem 1rem;
            }
        </style>
    </head>

    <body>
        <div class="container">
            <div class="row">
                <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
                    <div class="card border-0 shadow rounded-3 my-5">
                        <div class="card-body p-4 p-sm-5">
                            <?php if (isset($_GET['err'])) { ?>
                                <h5 class="card-title text-center mb-5 fw-bold fs-5" style="color: red">Credenziali errate!</h5>
                            <?php } else { ?>
                                <h5 class="card-title text-center mb-5 fw-light fs-5">Accedi per visualizzare i report</h5>
                            <?php } ?>
                            <form action="login.php" method="POST">
                                <div class="form-floating mb-3">
                                    <input type="email" class="form-control" id="floatingInput" name="email" placeholder="name@example.com">
                                    <label for="floatingInput">Indirizzo email</label>
                                </div>
                                <div class="form-floating mb-3">
                                    <input type="password" class="form-control" id="floatingPassword" name="password" placeholder="Password">
                                    <label for="floatingPassword">Password</label>
                                    <span id="toggle" toggle="#floatingPassword" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                </div>


                                <div class="d-grid">
                                    <button class="btn btn-primary btn-login text-uppercase fw-bold" type="submit">Login</button>
                                </div>


                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>

        localStorage.clear();

        $("#toggle").click(function () {
            $(this).toggleClass("fa-eye fa-eye-slash");
            var x = document.getElementById("floatingPassword");
            if (x.type === "password") {
                x.type = "text";
            } else {
                x.type = "password";
            }
        });


    </script>
</html>


