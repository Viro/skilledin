<?php
session_start();




if (isset($_POST['idC']) && isset($_POST['platf']) && isset($_POST['type']) && isset($_POST['uid'])) {

    include("obj/Report.php");
    $client['Report'] = new Report();
    $idCorso = $_POST['idC'];
    $piattaforma = $_POST['platf'];
    $report = $_POST['type'];
    $idUtente = $_POST['uid'];
    $nomeCorso = $client['Report']->getNomeCorso($idCorso, $piattaforma);
    if ($nomeCorso == "Corso non trovato") {
        header('HTTP/1.1 404 Not Found');
        exit();
    }
} else {
    if (isset($_GET['debug'])) {
        $_POST['idC'] = 85;
        $_POST['platf'] = "live";
        $_POST['type'] = "shvpName";
        $_POST['uid'] = 13;
        include("obj/Report.php");
        $client['Report'] = new Report();
        $idCorso = $_POST['idC'];
        $piattaforma = $_POST['platf'];
        $report = $_POST['type'];
        $idUtente = $_POST['uid'];
        $nomeCorso = $client['Report']->getNomeCorso($idCorso, $piattaforma);
        if ($nomeCorso == "Corso non trovato") {
            header('HTTP/1.1 404 Not Found');
            exit();
        }
    } else {
        header('HTTP/1.1 403 Forbidden');
        exit();
    }
}
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">

        <!-- Bootstrap CSS -->
        <!-- CSS only -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->




        <!-- Bootstrap 5 JS-->
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js"
                integrity="sha384-SR1sx49pcuLnqZUnnPwx6FCym0wLsk5JZuNx2bPPENzswTNFaQU1RDvt3wT4gWFG"
        crossorigin="anonymous"></script>
        <!-- JavaScript Bundle with Popper -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
                integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4"
        crossorigin="anonymous"></script>



        <title>Report corsi</title>






        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap4.min.css">
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap4.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/2.1.0/css/buttons.dataTables.min.css">
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/2.1.0/js/dataTables.buttons.min.js"></script>
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/2.1.0/js/buttons.colVis.min.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>



        <script src="../js/colors.js"></script>
        <script src="../js/courseReport.js"></script>



        <style>
            body {
                margin-top: 40px;
                font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
            }

            #calendar {
                max-width: 800px;
                margin: 0 auto;
            }

            .select2-container {
                width: 100% !important;
                padding: 0;
            }

            ul.no-bullets {
                list-style-type: none; /* Remove bullets */
                padding: 0; /* Remove padding */
                margin: 0; /* Remove margins */
            }

            .dt-button-collection button.buttons-columnVisibility:before,
            .dt-button-collection button.buttons-columnVisibility.active span:before {
                display:block;
                position:absolute;
                top:1.2em;
                left:0;
                width:12px;
                height:12px;
                box-sizing:border-box;
            }

            .dt-button-collection button.buttons-columnVisibility:before {
                content:' ';
                margin-top:-6px;
                margin-left:10px;
                border:1px solid black;
                border-radius:3px;
            }

            .dt-button-collection button.buttons-columnVisibility.active span:before {
                content:'\2714';
                margin-top:-11px;
                margin-left:12px;
                text-align:center;
                text-shadow:1px 1px #DDD, -1px -1px #DDD, 1px -1px #DDD, -1px 1px #DDD;
            }

            .dt-button-collection button.buttons-columnVisibility span {
                margin-left:20px;    
            }




        </style>
    </head>

    <body>
        <div class="container" >
            <div class="row my-2" >
                <div class="col-12">
                    <div class="card">
                        <div class="card-header"  id="card-header">
                        </div>
                        <div class="card-body" id="card-body"  >
                            <div class="d-flex align-items-start">
                                <div class="nav d-flex flex-row nav-pills me-3" id="v-pills-tab" role="tablist" aria-orientation="vertical" style="
                                     overflow: auto;
                                     width: 15%;
                                     height: 210px;
                                     justify-content: center;
                                     ">
                                    <button class="nav-link active " id="v-pills-report-tab" data-bs-toggle="pill"
                                            data-bs-target="#v-pills-report" type="button" role="tab"
                                            aria-controls="v-pills-report" aria-selected="false">Report corso
                                    </button>
                                    <button class="nav-link " id="v-pills-sintesi-tab" data-bs-toggle="pill"
                                            data-bs-target="#v-pills-sintesi" type="button" role="tab"
                                            aria-controls="v-pills-sintesi" aria-selected="false">Sintesi Login-Logout
                                    </button>
                                    <button class="nav-link" id="v-pills-timein-tab" data-bs-toggle="pill"
                                            data-bs-target="#v-pills-timein" type="button" role="tab"
                                            aria-controls="v-pills-timein" aria-selected="false">Tempo in piattaforma
                                    </button>
                                </div>

                                <div class="tab-content w-100" id="v-pills-tabContent">
                                    <div class="tab-pane fade show active" id="v-pills-report" role="tabpanel"
                                         aria-labelledby="v-pills-report-tab">
                                        <div class="row row-cols-2 g-2 g-lg-3">

                                            <div class="col-6">
                                                <label for="id_enterprise">Azienda</label>
                                                <select class="js-example-basic-single js-states form-control" id="id_enterprise">
                                                </select>
                                            </div>
                                            <div class="col-6">
                                                <label for="id_corso">Corso</label>
                                                <p class="form-control" id="nome_corso" style="word-break: break-all;" title="<?php echo ucfirst($nomeCorso); ?>">
                                                    <?php
                                                    $nomeCorsoCut = $nomeCorso;
                                                    if (strlen($nomeCorso) > 58) {
                                                        $nomeCorsoCut = substr($nomeCorso, 0, 58) . "...";
                                                    }
                                                    echo ucfirst($nomeCorsoCut);
                                                    ?>
                                                </p>
                                            </div>


                                            <div class="col-6">
                                                <div class="form-floating">
                                                    <input type="date" class="form-control" id="id_start"
                                                           min="2021-01-01" value='<?php echo date("Y-m-d", strtotime("-1 month")); ?>'>
                                                    <label for="id_start">Inizio:</label>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-floating">
                                                    <input type="date" class="form-control" id="id_end"
                                                           min="2021-01-01" value='<?php echo date('Y-m-d'); ?>'>
                                                    <label for="id_end">Fine:</label>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-floating">
                                                    <button id="id_submitbutton" type="button"
                                                            class="btn btn-primary btn-sm">Genera</button>
                                                    <button id="id_excel" type="button"
                                                            class="btn btn-secondary btn-sm" disabled="true">Excel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="v-pills-sintesi" role="tabpanel"
                                         aria-labelledby="v-pills-sintesi-tab">
                                        <div class="tab-content" id="myTabContent-sintesi">
                                            <div class="tab-pane fade show active" id="home-sintesi" role="tabpanel"
                                                 aria-labelledby="home-tab-sintesi">
                                                <div class="row row-cols-2 g-2 g-lg-3">
                                                    <div class="col-6">
                                                        <label for="id_enterprise_sintesi">Azienda</label>
                                                        <select class="js-example-basic-single js-states form-control" id="id_enterprise_sintesi"></select>
                                                    </div>

                                                    <div class="col-6">
                                                        <label for="id_enterprise_sintesi">Utente</label>
                                                        <select class="js-example-basic-single js-states form-control" id="id_iscritto_sintesi"></select>
                                                    </div>

                                                    <div class="col-6">
                                                        <div class="form-floating">
                                                            <input type="date" class="form-control" id="id_start_sintesi"
                                                                   min="2021-01-01" value='<?php echo date("Y-m-d", strtotime("-1 month")); ?>'>
                                                            <label for="id_start_sintesi">Inizio:</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="form-floating">
                                                            <input type="date" class="form-control" id="id_end_sintesi"
                                                                   min="2021-01-01" value='<?php echo date('Y-m-d'); ?>'>
                                                            <label for="id_end_sintesi">Fine:</label>
                                                        </div>
                                                    </div>

                                                    <div class="col-12">
                                                        <div class="form-floating">
                                                            <button id="id_submitbutton_sintesi" type="button"
                                                                    class="btn btn-primary btn-sm">Genera</button>
                                                            <button id="id_excel_sintesi" type="button"
                                                                    class="btn btn-secondary btn-sm" disabled="true">Excel</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="v-pills-timein" role="tabpanel"
                                         aria-labelledby="v-pills-timein-tab">
                                        <div class="tab-content" id="myTabContent-timein">
                                            <div class="tab-pane fade show active" id="home-timein" role="tabpanel"
                                                 aria-labelledby="home-tab-timein">
                                                <div class="row row-cols-2 g-2 g-lg-3">
                                                    <div class="col-6">
                                                        <label for="id_enterprise_timein">Azienda</label>
                                                        <select class="js-example-basic-single js-states form-control" id="id_enterprise_timein"></select>
                                                    </div>

                                                    <div class="col-6">
                                                        <label for="id_enterprise_timein">Utente</label>
                                                        <select class="js-example-basic-single js-states form-control" id="id_iscritto_timein"></select>
                                                    </div>

                                                    <div class="col-6">
                                                        <div class="form-floating">
                                                            <input type="date" class="form-control" id="id_start_timein"
                                                                   min="2021-01-01" value='<?php echo date("Y-m-d", strtotime("-1 month")); ?>'>
                                                            <label for="id_start_timein">Inizio:</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="form-floating">
                                                            <input type="date" class="form-control" id="id_end_timein"
                                                                   min="2021-01-01" value='<?php echo date('Y-m-d'); ?>'>
                                                            <label for="id_end_timein">Fine:</label>
                                                        </div>
                                                    </div>

                                                    <div class="col-12">
                                                        <div class="form-floating">
                                                            <button id="id_submitbutton_timein" type="button"
                                                                    class="btn btn-primary btn-sm">Genera</button>
                                                            <button id="id_excel_timein" type="button"
                                                                    class="btn btn-secondary btn-sm" disabled="true">Excel</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <a id="nav"  style="display: flex;justify-content:center;"></a>
        </div>

        <div style="margin-right: 7.5%;margin-left: 7.5%;margin-bottom: 7.5%;margin-top: 3%">

            <table id="report" class="table table-striped table-bordered" cellspacing="0" style="width:100%;">

                <thead id="report_head">
                    <tr>
                        <th>Report</th>

                    </tr>
                </thead>

                <tbody id="report_body">

                </tbody>

            </table>
        </div>

    </body>
    <script>
        let report = '<?php echo $report; ?>';
        let idCorso =<?php echo $idCorso; ?>;

        var piattaforma = '<?php echo $piattaforma; ?>';
        $(document).ready(function () {
            getAziendeAssociate(idCorso, piattaforma, "");
            if (report === "zoomName" || report == "s_zoomName") {
                idCorso = '<?php echo $nomeCorso; ?>';
            }
            $('#report').DataTable(
                    {
                        info: false,
                        searching: false,
                        bLengthChange: false,
                        bPaginate: false,
                        "language": {
                            "emptyTable": "Tabella vuota"
                        }

                    });
        });
        $('#report').on('column-visibility.dt', function (e, settings, column, state) {

            if ($('#report').DataTable().columns(column).nodes()[0][0]) {
                let className = $('#report').DataTable().columns(column).nodes()[0][0].className;
                className = className.trim();
                className = className.split(" ");
                if (state) {
                    checked = checked.filter(e => e !== className[0]);
                } else {
                    checked.push(className[0]);
                }

            }
        });



        $("#id_submitbutton").click(function () {
            getData("", report);
        });
        $("#id_submitbutton_sintesi").click(function () {
            getData("_sintesi", "sintesi");
        });
        $("#id_submitbutton_timein").click(function () {
            getData("_timein", "timein");
        });
        $("#id_excel").click(function () {
            getExcel("", report);
        });
        $("#id_excel_sintesi").click(function () {
            getExcel("_sintesi", "sintesi");
        });
        $("#id_excel_timein").click(function () {
            getExcel("_timein", "timein");
        });

        $("#id_enterprise").change(function () {
            $("#id_excel").prop("disabled", true);
        });

        $("#v-pills-sintesi-tab").click(function () {
            let idCorso =<?php echo $idCorso; ?>;
            getAziendeAssociate(idCorso, piattaforma, "_sintesi");
        });
        $("#v-pills-timein-tab").click(function () {
            let idCorso =<?php echo $idCorso; ?>;
            getAziendeAssociate(idCorso, piattaforma, "_timein");
        });
        $("#id_enterprise_sintesi").change(function () {
            let idCorso =<?php echo $idCorso; ?>;
            $("#id_excel_sintesi").prop("disabled", true);
            let azienda = $("#id_enterprise_sintesi option:selected").text();
            getUtentiAssociati(azienda, piattaforma, "_sintesi", idCorso);
        });
        $("#id_enterprise_timein").change(function () {
            let idCorso =<?php echo $idCorso; ?>;
            $("#id_excel_timein").prop("disabled", true);
            let azienda = $("#id_enterprise_timein option:selected").text();
            getUtentiAssociati(azienda, piattaforma, "_timein", idCorso);
        });
        $("#id_start").change(function () {
            $("#id_excel").prop("disabled", true);
        });
        $("#id_end").change(function () {
            $("#id_excel").prop("disabled", true);
        });
        $("#id_start_sintesi").change(function () {
            $("#id_excel_sintesi").prop("disabled", true);
        });
        $("#id_end_sintesi").change(function () {
            $("#id_excel_sintesi").prop("disabled", true);
        });
        $("#id_start_timein").change(function () {
            $("#id_excel_timein").prop("disabled", true);
        });
        $("#id_end_timein").change(function () {
            $("#id_excel_timein").prop("disabled", true);
        });


    </script>
</html>

