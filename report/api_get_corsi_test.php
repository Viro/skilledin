#!/usr/bin/php
<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

error_reporting(E_ALL);
require '/home/admin/public_html/vendor/autoload.php';

include("obj/Report.php");

$report = new Report();

$redis = new Redis();
$redis->connect('127.0.0.1', 6379);

$inizio = strtotime('now -12 months');
$fine = strtotime('now');

$time = '00:00:00';

$collection = new MongoDB\Client(
    "mongodb+srv://jobtek:TimeVision2021%40%21%3F@timevision.t9ed5.mongodb.net/TimeVision?retryWrites=true&w=majority");

$collectionCorsiCron = $collection->admin->cron_corsi;
$collectionPiattafrome = $collection->admin->piattaforme;

$cursorCron = $collectionCorsiCron->find(['redis_key' => 'get-corsi-global-naval-srl'], ['sort' => ['company' => 1]]);
//$cursorCron = $collectionCorsiCron->find(['in_corso' => true, 'flag' => 0], ['sort' => ['last_update' => 1]]);

foreach ($cursorCron as $cron) {

//    var_dump($cron['company']); exit;

    $array = array();
    $arrayCorsi = array();
    $response = array();

    $key = $cron['redis_key'];
    $last_update = $cron['last_update'];
    $action = $cron['action'];
    $gruppo = $cron['company'];
    $inCorso = $cron['in_corso'];

    $mins = (strtotime('now') - $last_update) / 60;

//    var_dump($mins);

//    if ($mins > 240 || $cron['data'] == ''){

//        $updateResult = $collectionCorsiCron->updateOne(
//            ['redis_key' => $key],
//            [
//                '$set' => [
//                    'flag' => 1
//                ]
//            ]
//        );

        $cursor = $collectionPiattafrome->find(
            ['id' => ['$ne' => 4]],
            ['projection' => ['id' => 1, 'nome' => 1]]);

    foreach ($cursor as $piattaforma) {
        $array[] = $piattaforma['nome'];
    }

//    var_dump($array); exit;

    foreach ($array as $platform) {
        switch ($platform) {
            case "Fondo Nuove Competenze":
                $selected = "fnc";
                break;
            case "Formazione 4.0":
                $selected = "f40";
                break;
            case "New":
                $selected = "new";
                break;
            case "Live":
                $selected = "live";
                break;
            case "Formazione":
                $selected = "formaz";
                break;
            default:
                $selected = 'null';
                break;
        }

//        var_dump($selected); exit;

        $collectionCorsiAssoc = $collection->$selected->corsiAssociati;
        $collectionCourse = $collection->$selected->mdl_course;
        $collectionHvp = $collection->$selected->mdl_hvp;
        $collectionUserInfoData = $collection->$selected->mdl_user_info_data;
        $collectionLog = $collection->$selected->mdl_logstore_standard_log;
        $collectionZoom = $collection->$selected->mdl_zoom_meeting_participants_aggregate;
        $cursoreAziende = $collectionCorsiAssoc->find(
            ['aziende' => array('$in' => array($gruppo))],
            ['projection' => ['fullname' => 1, '_id' => 1]]
        );

        $array_users_azienda = $report->getIdUtentiFromAzienda($selected, $gruppo);

        var_dump($array_users_azienda); exit;

//        echo $selected . "\n";


        foreach ($cursoreAziende as $corsi) {

//            var_dump($selected); exit;

            $array_cf = array();

            echo $selected . "\n";
            echo $corsi->_id . "\n";
            echo "-----------------\n\n";

            if ($corsi->_id > 1 && checkActiveCourse($corsi->_id)) {

//                echo $selected . "\n";
//                echo $corsi->_id . "\n";
//                echo "-----------------\n\n";


                $tempo_totale_all_secondi_utenti = 0;
                $time_not_formatted_sum = 0;
                $tempo_totale = 0;

                $cursoreHvp = $collectionHvp->find(
                    ['course' => $corsi->_id],
                    ['projection' => ['json_content' => 1]]
                );

                $dataHvp = iterator_to_array($cursoreHvp);

//                var_dump($dataHvp);
//                exit;

                if ($dataHvp) {
                    foreach ($dataHvp as $hvp) {
                        $hvpContent = json_decode($hvp['json_content']);
                        if(array_key_exists(0, $hvpContent->interactiveVideo->assets->endscreens)) {
                            $time_not_formatted = $hvpContent->interactiveVideo->assets->endscreens[0]->time;
//                                    var_dump($time_not_formatted);
                            $time_not_formatted_sum += $time_not_formatted;
                        }
                    }
                }

                $time = $report->convertSeconds($time_not_formatted_sum);
//                                $cursoreLog = $collectionLog->distinct("userid", ['userid' => array('$in' => $array_users_azienda), 'courseid' => $corsi->_id]);

//                $cursoreLog = $collectionLog->distinct("userid", ['userid' => array('$in' => $array_users_azienda), 'courseid' => $corsi->_id, 'action' => 'progress', 'timecreated' => ['$gte' => $inizio, '$lte' => $fine]]);

//
//                            if ($cursoreLog) {
//                                $total_iscritti = count($cursoreLog);
//                            } else {
//                                $total_iscritti = 0;
//                            }



//                $cursoreLogObjectId = $collectionLog->distinct("objectid", ['userid' => array('$in' => $array_users_azienda), 'courseid' => $corsi->_id, 'action' => 'progress']);


//                $nameCorsi = $report->getAllNameCourse($selected);
//                $countIscrittiArray = $report->getAggregateHvp($selected, $inizio, $fine, intval($corsi->_id), $array_users_azienda);
//                $result = $report->constructHvpArray($countIscrittiArray, false, $nameCorsi, $selected);
////                $total_iscritti = count($countIscrittiArray);
//                var_dump(count($countIscrittiArray)); exit;

                $pipelineUser = [
                    [
                        '$match' => [
                            'userid' => [
                                '$in' => $array_users_azienda
                            ],
//                            'component' => 'mod_hvp',
                            'courseid' => intval($corsi->_id),
                            'action' => [
                                    '$in' => ['progress', 'clicked', 'joined', 'submitted']
                            ],
                            'timecreated' => [
                                '$gte' => $inizio,
                                '$lte' => $fine
                            ],
                        ]
                    ], [
                        '$group' => [
                            '_id'=> '$userid',
//                            'userid'=> [
//                                '$addToSet' => '$userid'
//                            ],
                            'contextinstanceid'=> [
                                '$addToSet' => '$contextinstanceid'
                            ],
                            'objectid'=> [
                                '$addToSet' => '$objectid'
                            ]
                        ]
                    ]
                ];

//                echo json_encode($pipelineUser); exit;

                $dataUser = $collectionLog->aggregate($pipelineUser);
                $cursoreLog = $dataUser->toArray();

//                $countUs = 0;
//                foreach ($cursoreLog as $d) {
////                    $uss = $d['_id'];
//                    var_dump($d);
//                }
//
//                exit;
//
//                var_dump($countUs);
//                exit;

//                $tempo_totale = 0;
                foreach ($cursoreLog as $d) {
                    $user_id = $d['_id'];
//                    var_dump($user_id);
                    $cf = $report->getCfUtentiMdlUser($selected, $user_id);

                    $tempo_cumulato_singolo = 0;

//                    var_dump($d['contextinstanceid']); exit;

                    foreach ($d['contextinstanceid'] as $objectid) {

//                        var_dump($user_id);
//                        var_dump($objectid);
//                        echo "-----------------\n";

                        $times = $report->getLastProgressGlobalNewApi($user_id, $inizio, $objectid, $selected);
//                        var_dump($user_id);
//                        var_dump($times);
//                        echo "-----------------\n";

                        //zoom
                        $duration_zoom = 0;

//                        $pipeline_zoom = [
//                            [
//                                '$match'=> [
//                                    'userid' => $user_id,
//                                    'idCorso' => intval($corsi->_id),
//                                    'join_time' => [
//                                        '$gte' => $inizio,
//                                        '$lte' => $fine
//                                    ],
//                                ]
//                            ], [
//                                '$project' => [
//                                    'userid' => 1,
//                                    'duration' => 1,
//                                    'date' => [
//                                        '$toDate' => [
//                                            '$multiply' => [
//                                                '$join_time', 1000
//                                            ]
//                                        ]
//                                    ]
//                                ]
//                            ], [
//                                '$project' => [
//                                    'userid' => 1,
//                                    'duration' => 1,
//                                    'date' => [
//                                        '$dateToString' =>  [
//                                            'format' => "%d/%m/%Y", 'date' => '$date'
//                                        ]
//                                    ]
//                                ]
//                            ], [
//                                '$group' => [
//                                    '_id'=> [ 'dateToString' => [ 'format' => "%d/%m/%Y", 'date' => '$date'] ],
//                                    'join_time' => [
//                                        '$addToSet' => '$join_time'
//                                    ],
//                                    'durata'=> [
//                                        '$sum' => '$duration'
//                                    ]
//                                ]
//                            ]
//                        ];

                        $pipeline_zoom = [
                            [
                                '$match'=> [
                                    'userid' => $user_id,
                                    'idCorso' => intval($corsi->_id),
                                    'join_time' => [
                                        '$gte' => $inizio,
                                        '$lte' => $fine
                                    ],
                                ]
                            ], [
                                '$group'=> [
                                    '_id'=> 'null',
                                    'durata'=> [
                                        '$sum' => '$duration'
                                    ]
                                ]
                            ]
                        ];

                        $zoom = $collectionZoom->aggregate($pipeline_zoom);
                        $zoom_out = $zoom->toArray();

                        if(count($zoom_out) > 0){
                            $duration_zoom = $zoom_out[0]->durata;
                        }


//                        $zoom = $collectionZoom->find( ['userid' => $user_id, 'idCorso' => intval($objectid), 'join_time' => ['$gte' => $inizio], 'leave_time' => ['$lte' => $fine] ] );
//                        foreach ($zoom as $z) {
//                            $duration_zoom += $z['duration'];
//
//                            echo $user_id . "\n";
//                            echo intval($objectid) . "\n";
//                            echo $duration_zoom . "\n";
//                            echo "-----------\n\n";
//                        }

//                                        if($times > 0) {
                        $tempo_cumulato_singolo += $times;

                        if($duration_zoom > 0){
                            $tempo_cumulato_singolo += $duration_zoom;
                        }

//                                        }

//                                        $times = $collectionLog->find(
//                                            ['userid' => $user_id, 'courseid' => $corsi->_id, 'action' => 'progress'],
//                                            ['projection' => ['other' => 1],
//                                                'sort' => ['_id' => -1],
//                                                'limit' => 1]
//                                        );

                    }

//                    var_dump($tempo_cumulato_singolo);

                    $tempo_totale += $tempo_cumulato_singolo;

//                    var_dump($tempo_totale);

                    array_push($array_cf, ['id' => $user_id, 'cf' => $cf, 'time' => $report->convertSeconds($tempo_cumulato_singolo)]);

//                    if($corsi->_id == 97){
//                        var_dump($d['users']);
//                        exit;
//                    }

//                    $countIscritti = count($d['users']);
//                    var_dump($array_cf); exit;
                }

//                exit;

//                                var_dump($array_cf); exit;


                //totale dati corsi di tutti gli utenti
//                $res = $report->getAggregateHvp($selected, $inizio, $fine, $corsi->_id, $array_users_azienda);
//                $result = $report->constructHvpArray($res, false, false, $selected);
//
//                $tmp = $report->constructHvpReport($result, $selected);
//                $res = $report->constructSHvpReportNew($tmp);
//
//                $res = explode("*", $res);
//
//                $allData = json_decode($res[0], true);
//
//                $total_iscritti = count($allData);


                $total_iscritti = count($array_cf);

//                var_dump($tempo_totale);

//                var_dump($total_iscritti); exit;
                $tempo_totale_all_secondi_utenti = $tempo_totale;


//                foreach ($allData as $data) {
//                    $tempo_totale_all = explode(":", $data['tempo_tot']);
//                    $tempo_totale_all_secondi_utenti += $tempo_totale_all[0] * 3600 + $tempo_totale_all[1] * 60 + $tempo_totale_all[2];
//                }

                //calcolo percentuali
//                var_dump($total_iscritti);
//                var_dump($time_not_formatted_sum);
                $totali_tempo_corso_da_fare = ($total_iscritti * $time_not_formatted_sum);
//                var_dump($tempo_totale_all_secondi_utenti);
//                var_dump($totali_tempo_corso_da_fare);
                if ($tempo_totale_all_secondi_utenti > 0 && $totali_tempo_corso_da_fare > 0) {
                    $totale_seguito_utenti = $report->convertSeconds($tempo_totale_all_secondi_utenti);

                    $percent = ($tempo_totale_all_secondi_utenti / $totali_tempo_corso_da_fare) * 100;
                } else {
                    $totale_seguito_utenti = '00:00:00';
                    $percent = 0;
                }

                $percentage = $percent;

//                if ($percent > 0) {
//                    $percentage = 100 - $percent;
//                } else {
//                    $percentage = 0;
//                }
//
//                if($percentage < 0){
//                    $percentage = 100;
//                }

//                var_dump($array_cf);

                $arrayCorsi[] = array(
                    'id' => $corsi->_id,
                    'name' => $corsi->fullname,
                    'total_iscritti' => $total_iscritti,
                    'time' => $time,
                    'total_following' => $totale_seguito_utenti,
                    'percentage' => $percentage,
                    'selected' => $selected,
                    'users' => $array_cf
                );

//                var_dump($arrayCorsi); exit;
            }
        }

        //                $countCorsi[] = count($cursoreAziende->toArray());
    }

//    echo "qui"; exit;

//    exit;

//    var_dump($arrayCorsi);

        $corsiTotali = count($arrayCorsi);

//    var_dump($corsiTotali); exit;

        $response = [
            'corsi_totali' => $corsiTotali,
            'corsi' => $arrayCorsi
        ];

//        var_dump($response); exit;

        $response_json = json_encode($response);

    /**
     * decommentare di seguito dopo test
     */
        $collectionCorsiCron->updateOne(
            ['redis_key' => $key],
            [
                '$set' => [
                    'company' => $gruppo,
                    'action' => $action,
                    'data' => $response_json,
                    'last_update' => strtotime("now")
                ]
            ]);

        //TODO se abilitato redis
//        $redis->del($key);
//        $redis->set($key, $response_json);

        $updateResult = $collectionCorsiCron->updateOne(
            ['redis_key' => $key],
            [
                '$set' => [
                    'flag' => 0
                ]
            ]
        );
    //fine commento

        $corsiTotali = 0;

//        mail("flauto.vincenzo84@gmail.com","Report Nuovi - Fine importazione " . $gruppo,"Importazione terminata con successo. Verifica !!!");
//    }


}

function checkActiveCourse($id_corso){

    global $collectionCourse;

    $enddate = false;

    $cursoreCorsi = $collectionCourse->aggregate([['$match' => ['id' => $id_corso]], ['$limit' => 1]]);

    foreach ($cursoreCorsi as $corso) {
        $enddate = $corso['enddate'];
    }

//    echo $enddate . "\n";

    if(($enddate == 0) || ( ($enddate !== false && $enddate != 0) && (strtotime('now') <= $enddate))){
        return true;
    }else{
        return false;
    }
}



//header('Content-Type: application/json; charset=utf-8');
//http_response_code(201);
//echo json_encode($response);
