<?php
session_start();
if (!isset($_SESSION['user']) || ($_SESSION['user']['idRuolo'] != 0 && $_SESSION['user']['idRuolo'] != 1 && $_SESSION['user']['idRuolo'] != 4)) {
    header("location:index.php");
}
$abs_path = '/home/admin/public_html';
if(isset($_SERVER['DOCUMENT_ROOT']) && strlen(trim($_SERVER['DOCUMENT_ROOT']))) $abs_path = $_SERVER['DOCUMENT_ROOT'];

require_once($abs_path . "/skilledin/report/obj/Skilledin.php");
require_once($abs_path . "/skilledin/report/obj/SkilledinWeb.php");
require_once($abs_path . "/skilledin/report/obj/Report.php");
require_once($abs_path . "/skilledin/report/obj/Admin.php");
require_once($abs_path . "/skilledin/report/obj/SkilledinReport.php");
require_once($abs_path . "/skilledin/report/obj/Formazione.php");
require_once($abs_path . "/skilledin/report/obj/Formazione40.php");
require_once($abs_path . "/skilledin/report/obj/FondoNuoveCompetenze.php");
require_once($abs_path . "/skilledin/report/obj/Live.php");
require_once($abs_path . "/skilledin/report/obj/PlatformNew.php");
require_once($abs_path . "/skilledin/report/obj/Utility.php");

//Vecchia gestione delle classi
$skilledin        = new Skilledin();
$client['Report'] = new Report();

$platforms = $skilledin->getPlatforms();

?>
<!DOCTYPE html>
<html>
<head>
    <title>Skilledin - Report Piani formativi</title>
    <meta charset="utf-8">

    <!-- Bootstrap CSS -->
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->

    <!-- Bootstrap 5 JS-->
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js"
            integrity="sha384-SR1sx49pcuLnqZUnnPwx6FCym0wLsk5JZuNx2bPPENzswTNFaQU1RDvt3wT4gWFG"
            crossorigin="anonymous"></script>
    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4"
            crossorigin="anonymous"></script>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap4.min.css">
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap4.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/2.1.0/css/buttons.dataTables.min.css">
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/2.1.0/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/2.1.0/js/buttons.colVis.min.js"></script>


    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/js-cookie@3.0.1/dist/js.cookie.min.js"></script>
    <script src="../../js/colors.js"></script>
    <script src="../../js/skilledin.js"></script>
    <script src="../../js/aziende_piani.js"></script>
    <style>
        body {
            margin-top: 40px;
            font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
        }
        #calendar {
            max-width: 800px;
            margin: 0 auto;
        }
        .select2-container {
            width: 100% !important;
            padding: 0;
        }
        ul.no-bullets {
            list-style-type: none; /* Remove bullets */
            padding: 0; /* Remove padding */
            margin: 0; /* Remove margins */
        }
        .dt-button-collection button.buttons-columnVisibility:before,
        .dt-button-collection button.buttons-columnVisibility.active span:before {
            display:block;
            position:absolute;
            top:1.2em;
            left:0;
            width:12px;
            height:12px;
            box-sizing:border-box;
        }

        .dt-button-collection button.buttons-columnVisibility:before {
            content:' ';
            margin-top:-6px;
            margin-left:10px;
            border:1px solid black;
            border-radius:3px;
        }
        .dt-button-collection button.buttons-columnVisibility.active span:before {
            content:'\2714';
            margin-top:-11px;
            margin-left:12px;
            text-align:center;
            text-shadow:1px 1px #DDD, -1px -1px #DDD, 1px -1px #DDD, -1px 1px #DDD;
        }
        .dt-button-collection button.buttons-columnVisibility span {
            margin-left:20px;
        }
    </style>

</head>

<body>
<div class="container">
    <?php if ($_SESSION['user']['idRuolo'] == 0 || $_SESSION['user']['idRuolo'] == 1 || $_SESSION['user']['idRuolo'] == 4) { ?>
        <a href="../confirmPage.php?admin" class="previous" style="text-decoration: none;">‹ Torna indietro</a>
    <?php } ?>
    <?php if(isset($_POST['action'])){ ?>
        <?php
            if($_POST['action'] == 'generate_report'){
                $object      = new Admin('admin');
                $report_data = $object->calculateReportAziendePianiFormativi($_POST['skilledin_plan_id']);
                ?>
                <div class="row my-2" >
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">Piano visualizzato</div>
                            <div class="card-body">
                                <table class="table" id="table_company_plan_search">
                                    <thead>
                                        <tr>
                                            <th>Azienda</th>
                                            <th colspan="2">Piano</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><?php echo $report_data['nome_azienda'] ?></td>
                                            <td colspan="2"><?php echo $report_data['nome_piano'] ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row my-2" >
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">Dati economici piano</div>
                            <div class="card-body">
                                <table class="table" id="table_company_plan_search">
                                    <thead>
                                    <tr>
                                        <th>Importo massimo da CMO</th>
                                        <th>Importo massimo finanziato da ANPAL</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td><?php  echo number_format($report_data['importo_massimo'], 2, ',', '.') ?> &euro;</td>
                                        <td><?php  echo number_format($report_data['importo_finanziato'], 2, ',', '.') ?> &euro;</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row my-2" >
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">Timing piano</div>
                            <div class="card-body">
                                <table class="table" id="table_company_plan_search">
                                    <thead>
                                    <tr>
                                        <th>Tot durata piano in ore</th>
                                        <th>Tot durata piano in giorni lavorativi</th>
                                        <th>Tot durata piano in settimane</th>
                                        <th>Giorni utili alla conclusione del piano</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td><?php echo $report_data['durata_piano_in_ore'] ?></td>
                                        <td><?php echo $report_data['durata_piano_in_giorni'] ?></td>
                                        <td><?php echo $report_data['durata_piano_in_settimane'] ?></td>
                                        <td><?php echo $report_data['days_to_go'] . ' (' . date('d/m/Y', $report_data['stamp_fine_piano']) . ')' ?></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row my-2" >
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">Report sintetico</div>
                            <div class="card-body">
                                <table class="table" id="table_company_plan_search">
                                    <thead>
                                    <tr>
                                        <th colspan="2">Dati effettivo consuntivo</th>
                                        <th colspan="2">Dati da pianificazione</th>
                                        <th colspan="2">Scostamento tra pianificato ed effettivo</th>
                                    </tr>
                                    <tr>
                                        <th>Totale ore fruite ad oggi</th>
                                        <th>Valore maturato in &euro;</th>
                                        <th>Totale ore pianificate (da asincrona)</th>
                                        <th>Valore pianificato in &euro;</th>
                                        <th>Scostamento in ore (da asincrona)</th>
                                        <th>Scostamento in &euro;</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>
                                            <p><?php echo $report_data['total_course_time'] ?></>
                                            <p style="font-size:11px; padding:0px; margin: 0px; color:red; font-weight: bold;">Di cui live: <?php echo $report_data['live_course_time'] ?></p>
                                            <p style="font-size:11px; padding:0px; margin: 0px; color:red; font-weight: bold;">Di cui asincrona: <?php echo $report_data['partial_course_time'] ?></p>
                                        </td>
                                        <td>0</td>
                                        <td><?php echo $report_data['total_course_duration'] ?></td>
                                        <td>0</td>
                                        <td><?php echo $report_data['scotamento_time'] ?></td>
                                        <td>0</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            }
        ?>
    <?php }else{ ?>
        <div class="row my-2" >
            <div class="col-12">
                <div class="card">
                    <div class="card-header">Genera report per piani formativi</div>
                    <div class="card-body">
                        <form action="" method="POST">
                            <table class="table" id="table_company_plan_search">
                                <tbody>
                                <tr>
                                    <td>Azienda</td>
                                    <td><select class="form-select" name="company_name" id="select_company_name_search" onchange="javascript:updatePlansFromCompany(this.value)"></select></td>
                                </tr>
                                <tr>
                                    <td>Piano</td>
                                    <td><select class="form-select" name="skilledin_plan_id" id="select_plan_id"></select></td>
                                </tr>
                                <tr>
                                    <td colspan="2"><button type="submit" class="btn btn-primary" name="action" value="generate_report">Genera</button></td>
                                </tr>
                                </tbody>
                            </table>
                            <script type="text/javascript">realoadCompaniesFromAggregation()</script>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
</div>
</body>
</html>
