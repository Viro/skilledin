<?php
session_start();
if (!isset($_SESSION['user']) || ($_SESSION['user']['idRuolo'] != 0 && $_SESSION['user']['idRuolo'] != 1 && $_SESSION['user']['idRuolo'] != 4)) {
    header("location:index.php");
}
$abs_path = '/home/admin/public_html';
if(isset($_SERVER['DOCUMENT_ROOT']) && strlen(trim($_SERVER['DOCUMENT_ROOT']))) $abs_path = $_SERVER['DOCUMENT_ROOT'];

require_once($abs_path . "/skilledin/report/obj/Skilledin.php");
require_once($abs_path . "/skilledin/report/obj/SkilledinWeb.php");
require_once($abs_path . "/skilledin/report/obj/Report.php");
require_once($abs_path . "/skilledin/report/obj/Admin.php");
require_once($abs_path . "/skilledin/report/obj/SkilledinReport.php");
require_once($abs_path . "/skilledin/report/obj/Formazione.php");
require_once($abs_path . "/skilledin/report/obj/Formazione40.php");
require_once($abs_path . "/skilledin/report/obj/FondoNuoveCompetenze.php");
require_once($abs_path . "/skilledin/report/obj/Live.php");
require_once($abs_path . "/skilledin/report/obj/PlatformNew.php");
require_once($abs_path . "/skilledin/report/obj/Utility.php");

$object    = new Admin('admin');
$platforms = $object->getPlatforms();
$companies = $object->getCompaniesFromAggregation();
?>
<!DOCTYPE html>
<html>
<head>
    <title>Skilledin - Piani formativi</title>
    <meta charset="utf-8">

    <!-- Bootstrap CSS -->
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->

    <!-- Bootstrap 5 JS-->
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js"
            integrity="sha384-SR1sx49pcuLnqZUnnPwx6FCym0wLsk5JZuNx2bPPENzswTNFaQU1RDvt3wT4gWFG"
            crossorigin="anonymous"></script>
    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4"
            crossorigin="anonymous"></script>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap4.min.css">
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap4.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/2.1.0/css/buttons.dataTables.min.css">
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/2.1.0/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/2.1.0/js/buttons.colVis.min.js"></script>


    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/js-cookie@3.0.1/dist/js.cookie.min.js"></script>
    <script src="../../js/colors.js"></script>
    <script src="../../js/skilledin.js"></script>
    <script src="../../js/aziende_piani.js"></script>
    <style>
        body {
            margin-top: 40px;
            font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
        }
        #calendar {
            max-width: 800px;
            margin: 0 auto;
        }
        .select2-container {
            width: 100% !important;
            padding: 0;
        }
        ul.no-bullets {
            list-style-type: none; /* Remove bullets */
            padding: 0; /* Remove padding */
            margin: 0; /* Remove margins */
        }
        .dt-button-collection button.buttons-columnVisibility:before,
        .dt-button-collection button.buttons-columnVisibility.active span:before {
            display:block;
            position:absolute;
            top:1.2em;
            left:0;
            width:12px;
            height:12px;
            box-sizing:border-box;
        }

        .dt-button-collection button.buttons-columnVisibility:before {
            content:' ';
            margin-top:-6px;
            margin-left:10px;
            border:1px solid black;
            border-radius:3px;
        }
        .dt-button-collection button.buttons-columnVisibility.active span:before {
            content:'\2714';
            margin-top:-11px;
            margin-left:12px;
            text-align:center;
            text-shadow:1px 1px #DDD, -1px -1px #DDD, 1px -1px #DDD, -1px 1px #DDD;
        }
        .dt-button-collection button.buttons-columnVisibility span {
            margin-left:20px;
        }
    </style>

</head>

<body>
<div class="container">
    <?php if ($_SESSION['user']['idRuolo'] == 0 || $_SESSION['user']['idRuolo'] == 1 || $_SESSION['user']['idRuolo'] == 4) { ?>
        <a href="../confirmPage.php?admin" class="previous" style="text-decoration: none;">‹ Torna indietro</a>
    <?php } ?>
    <?php if(isset($_POST['action']) && $_POST['action'] != 'edit'){ ?>
        <?php
            if($_POST['action'] == 'insert'){
                $data = array();
                $data['skilledin_plan_id']         = uniqid("skpl_");
                $data['nome_azienda']              = $_POST['nome_azienda'];
                $data['nome_piano']                = $_POST['nome_piano'];
                $data['importo_massimo']           = (float) $_POST['importo_massimo'];
                $data['importo_finanziato']        = (float) $_POST['importo_finanziato'];
                $data['durata_piano_in_ore']       = (int) $_POST['durata_piano_in_ore'];
                $data['durata_piano_in_giorni']    = (int) $_POST['durata_piano_in_giorni'];
                $data['durata_piano_in_settimane'] = (int) $_POST['durata_piano_in_settimane'];
                $data['data_inizio_piano']         = $_POST['data_inizio'];
                $data['stamp_inizio_piano']        = strtotime($_POST['data_inizio']);
                $data['data_fine_piano']           = $_POST['data_fine'];
                $data['stamp_fine_piano']          = strtotime($_POST['data_fine']);
                $object->addAziendePianiFormativi($data);
                echo date('d/m/Y H:i:s') . ' - Piano inserito correttamente. Pronto per la sincronizzazione';
                echo '<script type="text/javascript">window.location = window.location.href;</script>';
            }
            if($_POST['action'] == 'update'){
                $data = array();
                if(isset($_POST['nome_piano']))                $data['nome_piano']                = $_POST['nome_piano'];
                if(isset($_POST['importo_massimo']))           $data['importo_massimo']           = $_POST['importo_massimo'];
                if(isset($_POST['importo_finanziato']))        $data['importo_finanziato']        = $_POST['importo_finanziato'];
                if(isset($_POST['durata_piano_in_ore']))       $data['durata_piano_in_ore']       = $_POST['durata_piano_in_ore'];
                if(isset($_POST['durata_piano_in_giorni']))    $data['durata_piano_in_giorni']    = $_POST['durata_piano_in_giorni'];
                if(isset($_POST['durata_piano_in_settimane'])) $data['durata_piano_in_settimane'] = $_POST['durata_piano_in_settimane'];
                if(isset($_POST['data_inizio']))               $data['data_inizio_piano']         = $_POST['data_inizio'];
                if(isset($_POST['data_inizio']))               $data['stamp_inizio_piano']        = strtotime($_POST['data_inizio']);
                if(isset($_POST['data_fine']))                 $data['data_fine_piano']           = $_POST['data_fine'];
                if(isset($_POST['data_fine']))                 $data['stamp_fine_piano']          = strtotime($_POST['data_fine']);
                if(isset($_POST['skilledin_plan_id'])){
                    $data['skilledin_plan_id'] = $_POST['skilledin_plan_id'];
                    $object->addAziendePianiFormativi($data);
                    foreach($platforms as $platform){
                        if($platform['id'] == 0) $platform_name = 'fnc';
                        if($platform['id'] == 1) $platform_name = 'f40';
                        if($platform['id'] == 2) $platform_name = 'live';
                        if($platform['id'] == 3) $platform_name = 'new';
                        if($platform['id'] == 5) $platform_name = 'formaz';
                        $skilledin_web[$platform_name] = new SkilledinWeb($platform_name);
                        if($skilledin_web[$platform_name]){
                            $piano_azienda = $skilledin_web[$platform_name]->getAziendePianiCorsiBy('skilledin_plan_id', $_POST['skilledin_plan_id']);
                            if($piano_azienda) $skilledin_web[$platform_name]->editAziendePianiFormativi($piano_azienda['id'], $data);
                        }
                    }
                    echo date('d/m/Y H:i:s') . ' - Piano aggiornato correttamente. Pronto per la sincronizzazione';
                    echo '<script type="text/javascript">window.location = window.location.href;</script>';
                }else{
                    echo 'Errore nell\'aggiornamento del piano. Mancano parametri!';
                }

            }
            if($_POST['action'] == 'search'){
                $company_plan_data = $object->getAziendaPianoFormativo($_POST['skilledin_plan_id']);
                ?>
                <div class="row my-2" >
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">Informazioni piano</div>
                            <div class="card-body">
                                <table class="table">
                                    <tbody>
                                    <tr>
                                        <th>Azienda</th>
                                        <td><?php  echo $company_plan_data['nome_azienda'] ?></td>
                                        <th>Piano</th>
                                        <td colspan="3"><?php  echo $company_plan_data['nome_piano'] ?></td>
                                    </tr>
                                    <tr>
                                        <th>Durata in ore</th>
                                        <td><?php  echo $company_plan_data['durata_piano_in_ore'] ?></td>
                                        <th>Durata in giorni</th>
                                        <td><?php  echo $company_plan_data['durata_piano_in_giorni'] ?></td>
                                        <th>Durata in settimane</th>
                                        <td><?php  echo $company_plan_data['durata_piano_in_settimane'] ?></td>
                                    </tr>
                                    <tr>
                                        <th>Importo massimo</th>
                                        <td><?php  echo number_format($company_plan_data['importo_massimo'], 2, ',', '.') ?> &euro;</td>
                                        <th>Importo finanziato</th>
                                        <td colspan="3"><?php  echo number_format($company_plan_data['importo_finanziato'], 2, ',', '.') ?> &euro;</td>
                                    </tr>
                                    <tr>
                                        <th>Inizio corso</th>
                                        <td><?php  echo date('d/m/Y', $company_plan_data['stamp_inizio_piano']) ?></td>
                                        <th>Fine corso</th>
                                        <td colspan="3"><?php  echo date('d/m/Y', $company_plan_data['stamp_fine_piano']) ?></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row my-2" >
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">Elenco corsi associati</div>
                            <div class="card-body">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Piattaforma</th>
                                        <th>ID Corso</th>
                                        <th>Nome corso</th>
                                        <th>Durata</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                    </thead>
                                    <tbody id="company_plan_course_list">
                                    </tbody>
                                </table>
                                <script type="text/javascript" lang="javascript">reloadPianoAziendaCorsiAssociati('<?php echo $company_plan_data['skilledin_plan_id']; ?>')</script>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row my-2" >
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">Aggiungi corso</div>
                            <div class="card-body">
                                <table class="table table-hover table-fixed" id="table_user_list">
                                    <tbody>
                                    <tr>
                                        <th class="w-45">Piattaforma</th>
                                        <th class="w-45">Corso</th>
                                        <th></th>
                                    </tr>

                                    <tr>
                                        <td class="w-45"><select class="form-select  w-100" name="course_id" id="select_platform_id" onchange="javascript:updateCoursesFromPlatform(this.value)"></select></td>
                                        <td class="w-45"><select class="form-select" name="course_id" id="select_course_id"></select></td>
                                        <td class="w-10">
                                            <button type="button" class="btn btn-primary" title="Aggiungi corso" onclick="javascript:addCourseToPianoAzienda('<?php echo $_POST['skilledin_plan_id']; ?>', document.getElementById('select_platform_id').value, document.getElementById('select_course_id').value)" id="button_add_azienda_piano_corso" >
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus-circle-fill" viewBox="0 0 16 16">
                                                    <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8.5 4.5a.5.5 0 0 0-1 0v3h-3a.5.5 0 0 0 0 1h3v3a.5.5 0 0 0 1 0v-3h3a.5.5 0 0 0 0-1h-3v-3z"/>
                                                </svg>
                                            </button>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <script type="text/javascript" lang="javascript">reloadPlatforms()</script>
                <?php

            }
        ?>
    <?php }else{ ?>
        <?php
            $company_plan_data = array('skilledin_plan_id'         => '',
                                       'nome_piano'                => '',
                                       'importo_massimo'           => 0.00,
                                       'importo_finanziato'        => 0.00,
                                       'durata_piano_in_ore'       => '',
                                       'durata_piano_in_giorni'    => '',
                                       'durata_piano_in_settimane' => '',
                                       'data_inizio_piano'         => date('Y-01-01'),
                                       'data_fine_piano'           => date('Y-12-31'));
            if(isset($_POST['action']) && $_POST['action'] == 'edit') {
               $company_plan_data = $object->getAziendaPianoFormativo($_POST['skilledin_plan_id']);
            }
        ?>

        <div class="row my-2" >
            <div class="col-12">
                <div class="card">
                    <div class="card-header">Inserimento corsi per piani formativi aziende</div>
                    <div class="card-body">
                        <form action="" method="POST">
                            <input class="form-control" type="hidden" name="skilledin_plan_id" value="<?php echo $company_plan_data['skilledin_plan_id'] ?>" />
                            <table class="table" id="table_company_plan_add">
                                <tbody>
                                <tr>
                                    <td>Azienda</td>
                                    <? if(isset($_POST['action']) && $_POST['action'] == 'edit') { ?>
                                        <td><?php echo $company_plan_data['nome_azienda'] ?></td>
                                    <? } else { ?>
                                        <td><select class="form-select" name="nome_azienda" id="select_company_name_create"></select></td>
                                    <? } ?>
                                </tr>
                                <tr>
                                    <td>Piano</td>
                                    <td><input class="form-control" type="text" name="nome_piano" value="<?php echo $company_plan_data['nome_piano'] ?>" /></td>
                                </tr>
                                <tr>
                                    <td>Importo massimo</td>
                                    <td><input class="form-control" type="number" step="0.01" name="importo_massimo" value="<?php echo $company_plan_data['importo_massimo'] ?>" /></td>
                                </tr>
                                <tr>
                                    <td>Importo finanziato</td>
                                    <td><input class="form-control" type="number" step="0.01" name="importo_finanziato" value="<?php echo $company_plan_data['importo_finanziato'] ?>" /></td>
                                </tr>
                                <tr>
                                    <td>Durata in ore</td>
                                    <td><input class="form-control" type="number" step="1" name="durata_piano_in_ore" value="<?php echo $company_plan_data['durata_piano_in_ore'] ?>" /></td>
                                </tr>
                                <tr>
                                    <td>Durata in giorni</td>
                                    <td><input class="form-control" type="number" step="1" name="durata_piano_in_giorni" value="<?php echo $company_plan_data['durata_piano_in_giorni'] ?>" /></td>
                                </tr>
                                <tr>
                                    <td>Durata in settimane</td>
                                    <td><input class="form-control" type="number" step="1" name="durata_piano_in_settimane" value="<?php echo $company_plan_data['durata_piano_in_settimane'] ?>" /></td>
                                </tr>
                                <tr>
                                    <td>Data inizio</td>
                                    <td><input class="form-control" type="date" name="data_inizio" value="<?php echo $company_plan_data['data_inizio_piano'] ?>" /></td>
                                </tr>
                                <tr>
                                    <td>Data fine</td>
                                    <td><input class="form-control" type="date" name="data_fine" value="<?php echo $company_plan_data['data_fine_piano'] ?>" /></td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <? if(isset($_POST['action']) && $_POST['action'] == 'edit') { ?>
                                            <button type="submit" class="btn btn-primary" name="action" value="update">Aggiorna</button>
                                        <? } else { ?>
                                            <button type="submit" class="btn btn-primary" name="action" value="insert">Inserisci</button>
                                        <? } ?>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row my-2" >
            <div class="col-12">
                <div class="card">
                    <div class="card-header">Cerca corsi per piani formativi aziende</div>
                    <div class="card-body">
                        <form action="" method="POST">
                            <table class="table" id="table_company_plan_search">
                                <tbody>
                                <tr>
                                    <td>Azienda</td>
                                    <td><select class="form-select" name="company_name" id="select_company_name_search" onchange="javascript:updatePlansFromCompany(this.value)"></select></td>
                                </tr>
                                <tr>
                                    <td>Piano</td>
                                    <td><select class="form-select" name="skilledin_plan_id" id="select_plan_id"></select></td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <button type="submit" class="btn btn-primary" name="action" value="search">Visualizza</button>
                                        <button type="submit" class="btn btn-primary" name="action" value="edit">Modifica</button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">realoadCompaniesFromAggregation()</script>
    <?php } ?>
</div>
</body>
</html>
