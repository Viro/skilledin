<?php
session_start();
if (!isset($_SESSION['user']) || ($_SESSION['user']['idRuolo'] != 0 && $_SESSION['user']['idRuolo'] != 1 && $_SESSION['user']['idRuolo'] != 4)) {
    header("location:index.php");
}
require_once($_SERVER['DOCUMENT_ROOT'] . "/skilledin/report/obj/Skilledin.php");
$skilledin = new Skilledin();
$platforms = $skilledin->getPlatforms();
?>
<!DOCTYPE html>
<html>
<head>
    <title>Skilledin - CRON</title>
    <meta charset="utf-8">

    <!-- Bootstrap CSS -->
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->

    <!-- Bootstrap 5 JS-->
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js"
            integrity="sha384-SR1sx49pcuLnqZUnnPwx6FCym0wLsk5JZuNx2bPPENzswTNFaQU1RDvt3wT4gWFG"
            crossorigin="anonymous"></script>
    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4"
            crossorigin="anonymous"></script>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap4.min.css">
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap4.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/2.1.0/css/buttons.dataTables.min.css">
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/2.1.0/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/2.1.0/js/buttons.colVis.min.js"></script>


    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/js-cookie@3.0.1/dist/js.cookie.min.js"></script>
    <script src="../js/colors.js"></script>
    <script src="../js/skilledin.js"></script>
    <style>
        body {
            margin-top: 40px;
            font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
        }
        #calendar {
            max-width: 800px;
            margin: 0 auto;
        }
        .select2-container {
            width: 100% !important;
            padding: 0;
        }
        ul.no-bullets {
            list-style-type: none; /* Remove bullets */
            padding: 0; /* Remove padding */
            margin: 0; /* Remove margins */
        }
        .dt-button-collection button.buttons-columnVisibility:before,
        .dt-button-collection button.buttons-columnVisibility.active span:before {
            display:block;
            position:absolute;
            top:1.2em;
            left:0;
            width:12px;
            height:12px;
            box-sizing:border-box;
        }

        .dt-button-collection button.buttons-columnVisibility:before {
            content:' ';
            margin-top:-6px;
            margin-left:10px;
            border:1px solid black;
            border-radius:3px;
        }
        .dt-button-collection button.buttons-columnVisibility.active span:before {
            content:'\2714';
            margin-top:-11px;
            margin-left:12px;
            text-align:center;
            text-shadow:1px 1px #DDD, -1px -1px #DDD, 1px -1px #DDD, -1px 1px #DDD;
        }
        .dt-button-collection button.buttons-columnVisibility span {
            margin-left:20px;
        }
    </style>

</head>

<body>
<div class="container">
    <?php if ($_SESSION['user']['idRuolo'] == 0 || $_SESSION['user']['idRuolo'] == 1 || $_SESSION['user']['idRuolo'] == 4) { ?>
        <a href="../confirmPage.php?admin" class="previous" style="text-decoration: none;">‹ Torna indietro</a>
    <?php } ?>
    <div class="row my-2" >
            <div class="col-12">
                <div class="card">
                    <div class="card-header">Elenco dei cron storicizzati</div>
                    <div class="card-body">
                        <?php
                        $scheduled_crons = $skilledin->getCronHistories();
                        ?>


                        <table class="table" id="table_scheduled_job">
                            <thead>
                            <tr>
                                <th>Piattaforma</th>
                                <th>Job</th>
                                <th>Data</th>
                                <th>Inzio</th>
                                <th>Completamento</th>
                                <th>Tempo</th>
                                <th>Stato</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if(!empty($scheduled_crons)){ ?>
                                <?php foreach ($scheduled_crons as $scheduled_cron) { ?>
                                        <?php $scheduled_cron['job_completed'] = ($scheduled_cron['status'] == 2) ? date('d/m/Y  H:i:s', strtotime($scheduled_cron['job_completed'])) : '-' ?>
                                    <tr>
                                        <td><?php echo $scheduled_cron['platform_name'] ?></td>
                                        <td><?php echo $scheduled_cron['job_name'] ?></td>
                                        <td><?php echo date('d/m/Y', strtotime($scheduled_cron['job_date'])) ?></td>
                                        <td><?php echo date('d/m/Y H:i:s', strtotime($scheduled_cron['job_started'])) ?></td>
                                        <td><?php echo $scheduled_cron['job_completed'] ?></td>
                                        <td><?php echo $scheduled_cron['job_time'] ?></td>
                                        <td><?php echo $scheduled_cron['status_text'] ?></td>
                                    </tr>
                                <?php } ?>
                            <?php }else{ ?>
                                <tr>
                                    <td colspan="8" class="text-center"><p class="font-weight-bold font-italic">- Nessun job schedulato -</p></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
</div>
</body>
</html>
