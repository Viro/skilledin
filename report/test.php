<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

error_reporting(E_ALL);

require '../../vendor/autoload.php';

include("obj/Report.php");
$report = new Report();

$collection = new MongoDB\Client(
    "mongodb+srv://jobtek:TimeVision2021%40%21%3F@timevision.t9ed5.mongodb.net/TimeVision?retryWrites=true&w=majority");

/**
 * Zoom
 */
//$collectionZoom = $collection->live->mdl_zoom_meeting_participants_aggregate;
//
//$pipeline = [
//    [
//        '$match'=> [
//            'userid' => 99,
//            'idCorso' => 27,
//            'join_time' => [
//                '$gte' => 1618495247
//            ],
//            'leave_time' => [
//                '$lte' => 1663599831
//            ]
//        ]
//    ], [
//        '$group'=> [
//            '_id'=> 'null',
//            'durata'=> [
//                '$sum' => '$duration'
//            ]
//        ]
//    ]
//];
//
//$data = $collectionZoom->aggregate($pipeline);
//$res = $data->toArray();
//
//if(count($res) > 0){
//    var_dump($res[0]->durata);
//}
//exit;

//$gruppo = 'F2A';
//$array_users_azienda = $report->getIdUtentiFromAzienda('live', $gruppo);
//
//$collectionLog = $collection->live->mdl_logstore_standard_log;

$pipeline = [
    [
        '$project' => [
            'id' => 1,
            'username' => 1,
            'idnumber' => 1,
            'nome' => [
                '$concat' => [
                    '$firstname', ' ', '$lastname'
                ]
            ],
            'email' => 1,
            'phone1' => 1,
            'phone2' => 1
        ]
    ], [
        '$lookup' => [
            'from' => 'mdl_user_info_data',
            'localField' => 'id',
            'foreignField' => 'userid',
            'as' => 'info'
        ]
    ], [
        '$unwind' => [
            'path' => '$info',
            'preserveNullAndEmptyArrays' => False
        ]
    ], [
        '$match' => [
            '$or' => [
                [
                    'info.fieldid' => [
                        '$eq' => 1
                    ]
                ], [
                    'info.fieldid' => [
                        '$eq' => 4
                    ]
                ], [
                    'info.fieldid' => [
                        '$eq' => 5
                    ]
                ]
            ]
        ]
    ], [
        '$sort' => [
            'info.fieldid' => 1
        ]
    ], [
        '$group' => [
            '_id' => [
                'userid' => '$id',
                'username' => '$username',
                'email' => '$email',
                'nome' => '$nome',
                'phone1' => '$phone1',
                'phone2' => '$phone2',
                'idnumber' => '$idnumber'
            ],
            'dati' => [
                '$push' => '$info.data'
            ]
        ]
    ], [
        '$set' => [
            'azienda' => [
                '$arrayElemAt' => [
                    '$dati', 0
                ]
            ],
            'citta' => [
                '$arrayElemAt' => [
                    '$dati', 1
                ]
            ],
            'data' => [
                '$arrayElemAt' => [
                    '$dati', 2
                ]
            ]
        ]
    ], [
        '$project' => [
            'dati' => 0
        ]
    ], [
        '$project' => [
            'userid' => '$_id.userid',
            'username' => '$_id.username',
            'idnumber' => '$_id.idnumber',
            'email' => '$_id.email',
            'nome' => '$_id.nome',
            'phone1' => '$_id.phone1',
            'phone2' => '$_id.phone2',
            'citta' => 1,
            'data' => 1,
            'azienda' => [
                '$trim' => [
                    'input' => '$azienda'
                ]
            ],
            '_id' => 0
        ]
    ], [
        '$match' => [
            'azienda' => ['$ne' => null, '$ne' => "",'$ne' => "0"]
        ]
    ], [
        '$lookup' => [
            'from' => 'corsi_ruoli',
            'localField' => 'userid',
            'foreignField' => 'userid',
            'as' => 'ruoli'
        ]
    ], [
        '$unwind' => [
            'path' => '$ruoli'
        ]
    ], [
        '$set' => [
            'ruolo' => '$ruoli.ruolo'
        ]
    ], [
        '$project' => [
            'ruoli' => 0
        ]
    ], [
        '$out' => 'mdl_utenti_complete'
    ]
];

echo json_encode($pipeline);
