<?php

require '../../vendor/autoload.php';

class Report {

    private $conn = null;

    public function __construct() {
        $this->conn = new MongoDB\Client(
                "mongodb+srv://jobtek:TimeVision2021%40%21%3F@timevision.t9ed5.mongodb.net/TimeVision?retryWrites=true&w=majority");
    }

    public function getNomeCorso($id, $db) {
        $error = "Corso non trovato";
        $collection = $this->conn->$db->mdl_course;
        $cursor = $collection->find(
                ['id' => (int) $id],
                ['projection' => ['fullname' => 1]]
        );
        foreach ($cursor as $singolo) {
            return $singolo->fullname;
        }
        return $error;
    }

    public function getIdUtentiFromAzienda($db, $gruppo) {
        $collection = $this->conn->$db->mdl_user_info_data;
        $gruppo = trim($gruppo);

        $cursor = $collection->find(
                ['data' => $gruppo],
                ['projection' => ['userid' => 1]]
        );
        $tmp = array();
        $array_id = array();
        foreach ($cursor as $singolo) {
            $tmp = $singolo['userid'];
            array_push($array_id, $tmp);
        }

        return $array_id;
    }

    public function getAggregateZoom($db, $corso, $inizio, $fine, $gruppo) {
        $collection = $this->conn->$db->mdl_zoom_meeting_participants_aggregate;
        if ($corso == "Tutti")
            $match = ['$match' => [
                    'join_time' => [
                        '$gte' => $inizio,
                        '$lte' => $fine
                    ],
            ]];
        else
            $match = ['$match' => [
                    'join_time' => [
                        '$gte' => $inizio,
                        '$lte' => $fine
                    ],
                    'nomeCorso' => $corso
            ]];

        if ($gruppo != "Tutti") {
            $match2 = ['$match' => [
                    'azienda' => $gruppo
            ]];
        } else {

            $match2 = ['$match' => [
                    'azienda' => [
                        '$ne' => $gruppo
                    ]
            ]];
        }

        $pipeline = [$match,
            ['$lookup' => [
                    'from' => 'mdl_utenti_complete',
                    'localField' => 'userid',
                    'foreignField' => 'userid',
                    'as' => 'info'
                ]], ['$set' => [
                    'nome' => [
                        '$arrayElemAt' => [
                            '$info.nome', 0
                        ]
                    ],
                    'username' => [
                        '$arrayElemAt' => [
                            '$info.username',
                            0
                        ]
                    ],
                    'cf' => [
                        '$arrayElemAt' => [
                            '$info.idnumber',
                            0
                        ]
                    ],
                    'email' => [
                        '$arrayElemAt' => [
                            '$info.email',
                            0
                        ]
                    ],
                    'azienda' => [
                        '$arrayElemAt' => [
                            '$info.azienda',
                            0
                        ]
                    ],
                    'ruolo' => [
                        '$arrayElemAt' => [
                            '$info.ruolo',
                            0
                        ]
                    ],
                    'citta' => [
                        '$arrayElemAt' => [
                            '$info.citta',
                            0
                        ]
                    ],
                    'data' => [
                        '$arrayElemAt' => [
                            '$info.data',
                            0
                        ]
                    ],
                    'phone2' => [
                        '$arrayElemAt' => [
                            '$info.phone2',
                            0
                        ]
                    ],
                    'phone1' => [
                        '$arrayElemAt' => [
                            '$info.phone1',
                            0
                        ]
                    ],
                ]], ['$project' => [
                    'info' => 0,
                    'uuid' => 0,
                    'zoomuserid' => 0,
                    'user_email' => 0
                ]], $match2
        ];
        $out = $collection->aggregate($pipeline);

        $res = $out->toArray();
        return $res;
    }

    public function constructZoomReport($res) {
        $finale = array();

        foreach ($res as $singolo) {

            if (!isset($singolo['azienda']))
                continue;
            $ruolo = "";
            if (isset($singolo['ruolo'])) {
                switch ($singolo['ruolo']) {
                    case 'student':
                        $ruolo = "studente";
                        break;
                    case 'manager':
                        $ruolo = "manager";
                        break;
                    case 'editingteacher':
                        $ruolo = "docente";
                        break;
                    case 'teacher':
                        $ruolo = "controller";
                        break;
                    default:break;
                }
            }
            $singolo['ruolo'] = $ruolo;
            $singolo['timecreated'] = date("d-m-Y", $singolo['join_time']);
            $singolo['timestamp'] = $singolo['join_time'];
            $singolo['orario'] = date("H:i:s", $singolo['join_time']);
            $singolo['orario_logout'] = date("H:i:s", $singolo['leave_time']);
            $singolo['tempo_cumulato'] = $this->convertSeconds($singolo['duration']);
            if (!isset($singolo['citta']))
                $singolo['citta'] = "n/d";
            if (!isset($singolo['cf']))
                $singolo['cf'] = "n/d";
            if (!isset($singolo['data']))
                $singolo['data_nascita'] = "n/d";
            else
                $singolo['data_nascita'] = date("d/m/Y", $singolo['data']);
            $singolo['phone'] = "n/d";
            if (isset($singolo['phone1']) && $singolo['phone1'] != "")
                $singolo['phone'] = $singolo['phone1'];
            if (isset($singolo['phone1']) && $singolo['phone2'] != "")
                $singolo['phone'] = $singolo['phone2'];

            array_push($finale, $singolo);
        }

        $new = array();
        foreach ($finale as $singolo) {
            $singolo = json_decode(json_encode($singolo, true), true);
            if (isset($singolo))
                array_push($new, $singolo);
        }

        return $new;
    }

    public function createCopertina($userid, $spreadsheet, $j) {
        $txt1 = "";
        $txt2 = "";
        $src = "";
        $collection = $this->conn->admin->settings_excel_copertina;
        $cursor = $collection->find(
                ['userid' => (int) $userid],
                ['projection' => ['txt1' => 1, 'txt2' => 1, 'img' => 1],
                    'limit' => 1,
                    'sort' => ['_id' => -1],
                ]
        );

        foreach ($cursor as $singolo) {
            $txt1 = $singolo['txt1'];
            $txt2 = $singolo['txt2'];
            $src = $singolo['img'];
        }
        if ($src != "") {
            $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing->setName('S1');
            $drawing->setDescription('Intestazione');
            $drawing->setPath('img/' . $src);
            $drawing->setCoordinates('A1');
            $drawing->setHeight(100);
            $drawing->setWidth(800);
            $drawing->setWorksheet($spreadsheet->getActiveSheet());
        }
        if ($txt1 != "") {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue("A13", $txt1);
        }
        if ($txt2 != "") {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue("A19", $txt2);
        }
        $spreadsheet->setActiveSheetIndex($j)->setCellValue("H17", "REGISTRO ATTIVITA' FORMATIVE");
        $spreadsheet->getActiveSheet()->getStyle("H17")->getFont()->setBold(true);
        $spreadsheet->getActiveSheet()->getStyle("H17")->getFont()->setSize(18);

        $spreadsheet->getActiveSheet()->setTitle("Copertina");
        $spreadsheet->createSheet();
    }

    public function createSheet($userid, $spreadsheet, $j) {
        $s1 = "";
        $t1 = "";
        $collection = $this->conn->admin->settings_excel_sheet;
        $cursor = $collection->find(
                ['userid' => (int) $userid],
                ['projection' => ['s1' => 1, 't1' => 1],
                    'limit' => 1,
                    'sort' => ['_id' => -1],
                ]
        );
        foreach ($cursor as $singolo) {
            $s1 = $singolo['s1'];
            $t1 = $singolo['t1'];
        }
        $index = 1;
        if ($s1 != "") {
            $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing->setName('S1');
            $drawing->setDescription('Intestazione Sheet');
            $drawing->setPath('img/' . $s1);
            $drawing->setCoordinates('A1');
            $drawing->setHeight(100);
            $drawing->setWidth(800);
            $drawing->setWorksheet($spreadsheet->getActiveSheet());
            $index = 16;
        }
        if ($t1 != "") {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue("A13", $t1);
            $index = 16;
        }
        return $index;
    }

    public function setZoomSheet($index, $spreadsheet, $letter, $filtri, $j, $new) {
        if (!in_array("data", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Data");
            $letter++;
        }
        if (!in_array("userid", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Userid");
            $letter++;
        }

        if (!in_array("username", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Username");
            $letter++;
        }

        if (!in_array("name", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Nome Cognome");
            $letter++;
        }

        if (!in_array("cf", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Codice Fiscale");
            $letter++;
        }

        if (!in_array("email", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Email");
            $letter++;
        }

        if (!in_array("dn", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Data di nascita");
            $letter++;
        }

        if (!in_array("ln", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Luogo di nascita");
            $letter++;
        }

        if (!in_array("azienda", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Azienda");
            $letter++;
        }
        if (!in_array("phone", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "N.di telefono");
            $letter++;
        }
        if (!in_array("ruolo", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Ruolo");
            $letter++;
        }
        if (!in_array("corso", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Corso");
            $letter++;
        }
        if (!in_array("aula", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Nome aula");
            $letter++;
        }
        if (!in_array("orarioA", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Ora ingresso");
            $letter++;
        }
        if (!in_array("orarioL", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Ora uscita");
            $letter++;
        }
        if (!in_array("tc", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Tempo cumulato");
        }

        $spreadsheet->getActiveSheet()->setTitle("Report");


        $from = "A1";
        $to = $letter . $index;
        $spreadsheet->getActiveSheet()->getStyle("$from:$to")->getFont()->setBold(true);

        $i = $index + 1;

        foreach ($new as $singolo) {
            $letter = 'A';
            if (!in_array("data", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['timecreated']);
                $letter++;
            }
            if (!in_array("userid", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['userid']);
                $letter++;
            }

            if (!in_array("username", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['username']);
                $letter++;
            }

            if (!in_array("name", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['nome']);
                $letter++;
            }

            if (!in_array("cf", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, @$singolo['cf']);
                $letter++;
            }

            if (!in_array("email", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["email"]);
                $letter++;
            }

            if (!in_array("dn", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, @$singolo["data_nascita"]);
                $letter++;
            }

            if (!in_array("ln", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, @$singolo["citta"]);
                $letter++;
            }

            if (!in_array("azienda", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, @$singolo["azienda"]);
                $letter++;
            }

            if (!in_array("phone", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['phone']);
                $letter++;
            }

            if (!in_array("ruolo", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["ruolo"]);
                $letter++;
            }
            if (!in_array("corso", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["nomeCorso"]);
                $letter++;
            }
            if (!in_array("aula", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["name"]);
                $letter++;
            }
            if (!in_array("orarioA", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["orario"]);
                $letter++;
            }
            if (!in_array("orarioL", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["orario_logout"]);
                $letter++;
            }
            if (!in_array("tc", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["tempo_cumulato"]);
            }
            $i++;
        }
        foreach (range('A', $letter) as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
        }

        $spreadsheet->getActiveSheet()->getStyle("A" . $index . ":" . $letter . $index)
                ->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('00FF7F');

        return $i;
    }

    public function addSettingsExcel($userid, $spreadsheet, $i) {
        $s2 = "";
        $s3 = "";
        $collection = $this->conn->admin->settings_excel_sheet;
        $cursor = $collection->find(
                ['userid' => (int) $userid],
                ['projection' => ['s2' => 1, 'timbro' => 1],
                    'limit' => 1,
                    'sort' => ['_id' => -1],
                ]
        );
        foreach ($cursor as $singolo) {
            $s2 = $singolo['s2'];
            $s3 = $singolo['timbro'];
        }
        if ($s2 != "") {
            $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing->setName('S2');
            $drawing->setDescription('S2 bordo sinistro');
            $drawing->setPath('img/' . $s2);
            $coordinate = $i + 3;
            $drawing->setCoordinates('A' . $coordinate);
            $drawing->setHeight(100);
            $drawing->setWidth(100);
            $drawing->setWorksheet($spreadsheet->getActiveSheet());
        }
        if ($s3 != "") {
            $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing->setName('S3');
            $drawing->setDescription('S3 timbro');
            $drawing->setPath('img/' . $s3);
            $coordinate = $i + 3;
            $drawing->setCoordinates('L' . $coordinate);
            $drawing->setHeight(100);
            $drawing->setWidth(100);
            $drawing->setWorksheet($spreadsheet->getActiveSheet());
        }
    }

    public function getAggregateZoomSintesi($db, $inizio, $fine, $gruppo) {
        $collection = $this->conn->$db->mdl_zoom_meeting_participants_aggregate;
        $match = ['$match' => [
                'join_time' => [
                    '$gte' => $inizio,
                    '$lte' => $fine
                ],
        ]];
        $pipeline = [$match,
            ['$lookup' => [
                    'from' => 'mdl_utenti_complete',
                    'localField' => 'userid',
                    'foreignField' => 'userid',
                    'as' => 'info'
                ]], ['$set' => [
                    'nome' => [
                        '$arrayElemAt' => [
                            '$info.nome', 0
                        ]
                    ],
                    'username' => [
                        '$arrayElemAt' => [
                            '$info.username',
                            0
                        ]
                    ],
                    'cf' => [
                        '$arrayElemAt' => [
                            '$info.idnumber',
                            0
                        ]
                    ],
                    'email' => [
                        '$arrayElemAt' => [
                            '$info.email',
                            0
                        ]
                    ],
                    'azienda' => [
                        '$arrayElemAt' => [
                            '$info.azienda',
                            0
                        ]
                    ],
                    'ruolo' => [
                        '$arrayElemAt' => [
                            '$info.ruolo',
                            0
                        ]
                    ],
                    'phone2' => [
                        '$arrayElemAt' => [
                            '$info.phone2',
                            0
                        ]
                    ],
                    'phone1' => [
                        '$arrayElemAt' => [
                            '$info.phone1',
                            0
                        ]
                    ],
                    'citta' => [
                        '$arrayElemAt' => [
                            '$info.citta',
                            0
                        ]
                    ],
                    'data' => [
                        '$arrayElemAt' => [
                            '$info.data',
                            0
                        ]
                    ]
                ]], ['$project' => [
                    'info' => 0,
                    'uuid' => 0,
                    'zoomuserid' => 0,
                    'user_email' => 0
                ]], ['$match' => [
                    'azienda' => $gruppo
                ]]
        ];
        $out = $collection->aggregate($pipeline);
        $result = $out->toArray();
        return $result;
    }

    public function constructMinCumulatiReport($array, $totResult) {
        $array = $this->sortArray($array);
        $prov = null;
        $mesi = array();
        $new = array();
        $array_mesi = array();
        $cumulatoTot = 0;
        $tempo_tot = 0;
        $cont = 0;



        foreach ($array as $singolo) {

            if ($prov == null && $totResult > 1) {
                $prov = $singolo;
                $cont++;
                continue;
            }

            if ($totResult == 1) {
                $data_singolo = str_replace("/", "", $singolo['timecreated']);
                $timesingolo_split = explode(":", $singolo['tempo_cumulato']);
                $seconds_singolo = $timesingolo_split[0] * 3600 + $timesingolo_split[1] * 60 + $timesingolo_split[2];
                $mesi[$singolo['timecreated']] = $singolo['tempo_cumulato'];
                if (!in_array($singolo['timecreated'], $array_mesi))
                    array_push($array_mesi, $singolo['timecreated']);
                $tempo = explode(":", $singolo['tempo_cumulato']);
                $secondi = $tempo[0] * 3600 + $tempo[1] * 60 + $tempo[2];
                $tempo_tot = $secondi;
                $singolo['mesi'] = $mesi;
                $singolo['tempo_tot'] = $this->convertSeconds($tempo_tot);
                array_push($new, $singolo);
                break;
            }

            $cont++;
            $data_prov = str_replace("/", "", $prov['timecreated']);
            $data_singolo = str_replace("/", "", $singolo['timecreated']);
            $timeprov_split = explode(":", $prov['tempo_cumulato']);
            $timesingolo_split = explode(":", $singolo['tempo_cumulato']);
            $seconds_prov = $timeprov_split[0] * 3600 + $timeprov_split[1] * 60 + $timeprov_split[2];
            $seconds_singolo = $timesingolo_split[0] * 3600 + $timesingolo_split[1] * 60 + $timesingolo_split[2];
            if ($prov['userid'] == $singolo['userid']) {
                if ($data_prov == $data_singolo) {

                    $cumulatoTot = $seconds_prov + $seconds_singolo;
                    if (!in_array($prov['timecreated'], $array_mesi))
                        array_push($array_mesi, $prov['timecreated']);

                    $mesi[$prov['timecreated']] = $this->convertSeconds($cumulatoTot);

                    $prov = $singolo;
                    $prov['mesi'] = $mesi;
                    $prov['tempo_cumulato'] = $this->convertSeconds($cumulatoTot);
                } else {

                    $mesi[$prov['timecreated']] = $prov['tempo_cumulato'];
                    $mesi[$singolo['timecreated']] = $singolo['tempo_cumulato'];
                    if (!in_array($prov['timecreated'], $array_mesi))
                        array_push($array_mesi, $prov['timecreated']);
                    if (!in_array($singolo['timecreated'], $array_mesi))
                        array_push($array_mesi, $singolo['timecreated']);

                    $prov = $singolo;
                    $prov['mesi'] = $mesi;
                }
            } else {
                if (isset($prov['mesi'])) {
                    foreach ($prov['mesi'] as $tempo) {
                        $tempo = explode(":", $tempo);
                        $secondi = $tempo[0] * 3600 + $tempo[1] * 60 + $tempo[2];
                        $tempo_tot += $secondi;
                    }
                } else {
                    $mesi[$prov['timecreated']] = $prov['tempo_cumulato'];
                    $prov['mesi'] = $mesi;
                    if (!in_array($prov['timecreated'], $array_mesi))
                        array_push($array_mesi, $prov['timecreated']);
                    $tempo = explode(":", $prov['tempo_cumulato']);
                    $secondi = $tempo[0] * 3600 + $tempo[1] * 60 + $tempo[2];
                    $tempo_tot = $secondi;
                }

                $prov['tempo_tot'] = $this->convertSeconds($tempo_tot);
                array_push($new, $prov);
                $tempo_tot = 0;
                $prov = $singolo;
                $mesi = null;
            }

            if ($cont == $totResult) {
                if (isset($prov['mesi'])) {
                    foreach ($prov['mesi'] as $tempo) {
                        $tempo = explode(":", $tempo);
                        $secondi = $tempo[0] * 3600 + $tempo[1] * 60 + $tempo[2];
                        $tempo_tot += $secondi;
                    }
                } else {
                    $mesi[$prov['timecreated']] = $prov['tempo_cumulato'];
                    $prov['mesi'] = $mesi;
                    if (!in_array($prov['timecreated'], $array_mesi))
                        array_push($array_mesi, $prov['timecreated']);
                    $tempo = explode(":", $prov['tempo_cumulato']);
                    $secondi = $tempo[0] * 3600 + $tempo[1] * 60 + $tempo[2];
                    $tempo_tot = $secondi;
                }
                $prov['tempo_tot'] = $this->convertSeconds($tempo_tot);
                array_push($new, $prov);
            }
        }
        usort($array_mesi, "compareByTimeStamp");

        foreach ($new as &$singolo) {
            foreach ($array_mesi as $mese) {
                if (!in_array($mese, array_keys($singolo['mesi']))) {
                    $singolo['mesi'][$mese] = "00:00:00";
                }
            }
            uksort($singolo['mesi'], "compareByTimeStamp");
        }
        $return = json_encode($new) . "*" . json_encode($array_mesi);

        return $return;
    }

    public function setZoomSintesiSheet($index, $spreadsheet, $letter, $filtri, $j, $new, $array_mesi) {
        if (!in_array("userid", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Userid");
            $letter++;
        }
        if (!in_array("username", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Username");
            $letter++;
        }

        if (!in_array("name", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Nome Cognome");
            $letter++;
        }
        if (!in_array("cf", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Codice Fiscale");
            $letter++;
        }
        if (!in_array("email", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Email");
            $letter++;
        }
        if (!in_array("dn", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Data di Nascita");
            $letter++;
        }
        if (!in_array("ln", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Luogo di Nascita");
            $letter++;
        }
        if (!in_array("azienda", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Azienda");
            $letter++;
        }
        if (!in_array("phone", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "N.di telefono");
            $letter++;
        }
        if (!in_array("ruolo", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Ruolo");
            $letter++;
        }
        if (!in_array("corso", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Corso");
            $letter++;
        }
        if (!in_array("idCorso", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Id Corso");
            $letter++;
        }
        if (!in_array("tc", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue("$letter" . $index, "Tempo cumulato");
            $letter++;
        }

        $column = $letter;
        $giro = 0;

        foreach ($array_mesi as $mese) {
            if (!in_array("data_" . $giro, $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($column . $index, $mese);
                $column++;
            }
            $giro++;
        }


        $spreadsheet->getActiveSheet()->setTitle("Report");


        $from = "A1";
        $to = "$column" . $index;
        $spreadsheet->getActiveSheet()->getStyle("$from:$to")->getFont()->setBold(true);

        $i = $index + 1;
        foreach ($new as $singolo) {
            $letter = 'A';

            if (!in_array("userid", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['userid']);
                $letter++;
            }
            if (!in_array("username", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['username']);
                $letter++;
            }
            if (!in_array("name", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['nome']);
                $letter++;
            }
            if (!in_array("cf", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['cf']);
                $letter++;
            }
            if (!in_array("email", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['email']);
                $letter++;
            }

            if (!in_array("dn", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['data_nascita']);
                $letter++;
            }

            if (!in_array("ln", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['citta']);
                $letter++;
            }

            if (!in_array("azienda", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['azienda']);
                $letter++;
            }
            if (!in_array("phone", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['phone']);
                $letter++;
            }
            if (!in_array("ruolo", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['ruolo']);
                $letter++;
            }
            if (!in_array("corso", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['nomeCorso']);
                $letter++;
            }
            if (!in_array("idCorso", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['idCorso']);
                $letter++;
            }
            if (!in_array("tc", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue("$letter$i", $singolo["tempo_tot"]);
                $letter++;
            }


            $column = $letter;
            $giro = 0;
            foreach ($singolo['mesi'] as $value) {
                if (!in_array("data_" . $giro, $filtri)) {
                    $spreadsheet->setActiveSheetIndex($j)->setCellValue("$column$i", $value);
                    $column++;
                }
                $giro++;
            }


            $i++;
        }

        foreach (range('A', "$column") as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
        }

        $spreadsheet->getActiveSheet()->getStyle("A" . $index . ":" . $column . $index)
                ->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('00FF7F');

        return $i;
    }

    public function compareByTimeStamp($time1, $time2) {
        $time1 = str_replace("/", "-", $time1);
        $time2 = str_replace("/", "-", $time2);

        return strtotime($time1) - strtotime($time2);
    }

    public function getAggregateZoomSintesiName($db, $nomeCorso, $inizio, $fine) {
        $collection = $this->conn->$db->mdl_zoom_meeting_participants_aggregate;
        $match = ['$match' => [
                'join_time' => [
                    '$gte' => $inizio,
                    '$lte' => $fine
                ],
                'nomeCorso' => $nomeCorso
        ]];
        $pipeline = [$match,
            ['$lookup' => [
                    'from' => 'mdl_utenti_complete',
                    'localField' => 'userid',
                    'foreignField' => 'userid',
                    'as' => 'info'
                ]], ['$set' => [
                    'nome' => [
                        '$arrayElemAt' => [
                            '$info.nome', 0
                        ]
                    ],
                    'username' => [
                        '$arrayElemAt' => [
                            '$info.username',
                            0
                        ]
                    ],
                    'cf' => [
                        '$arrayElemAt' => [
                            '$info.idnumber',
                            0
                        ]
                    ],
                    'email' => [
                        '$arrayElemAt' => [
                            '$info.email',
                            0
                        ]
                    ],
                    'azienda' => [
                        '$arrayElemAt' => [
                            '$info.azienda',
                            0
                        ]
                    ],
                    'ruolo' => [
                        '$arrayElemAt' => [
                            '$info.ruolo',
                            0
                        ]
                    ],
                    'phone2' => [
                        '$arrayElemAt' => [
                            '$info.phone2',
                            0
                        ]
                    ],
                    'phone1' => [
                        '$arrayElemAt' => [
                            '$info.phone1',
                            0
                        ]
                    ],
                    'citta' => [
                        '$arrayElemAt' => [
                            '$info.citta',
                            0
                        ]
                    ],
                    'data' => [
                        '$arrayElemAt' => [
                            '$info.data',
                            0
                        ]
                    ]
                ]], ['$project' => [
                    'info' => 0,
                    'uuid' => 0,
                    'zoomuserid' => 0,
                    'user_email' => 0
                ]]
        ];
        $out = $collection->aggregate($pipeline);
        $result = $out->toArray();
        return $result;
    }

    public function getLogstore($db, $inizio, $fine, $idUtente = null, $array_id = null, $idCorso = null) {
        $collection = $this->conn->$db->mdl_logstore_standard_log;

        if (!isset($idUtente)) {
            if (isset($idCorso) && isset($array_id)) {
                if ($idCorso == 0) {
                    $match = ['$match' => [
                            'userid' => [
                                '$in' => $array_id
                            ],
                            'timecreated' => [
                                '$gte' => $inizio,
                                '$lte' => $fine
                            ]
                    ]];
                } else if (count($array_id) == 0) {
                    $match = ['$match' => [
                            'courseid' => (int) $idCorso,
                            'timecreated' => [
                                '$gte' => $inizio,
                                '$lte' => $fine
                            ]
                    ]];
                } else {
                    $match = ['$match' => [
                            'userid' => [
                                '$in' => $array_id
                            ],
                            'courseid' => (int) $idCorso,
                            'timecreated' => [
                                '$gte' => $inizio,
                                '$lte' => $fine
                            ]
                    ]];
                }
            } else if (!isset($idCorso)) {
                $match = ['$match' => [
                        'userid' => [
                            '$in' => $array_id
                        ],
                        'timecreated' => [
                            '$gte' => $inizio,
                            '$lte' => $fine
                        ]
                ]];
            }
        } else {
            $match = ['$match' => [
                    'userid' => (int) $idUtente,
                    'timecreated' => [
                        '$gte' => $inizio,
                        '$lte' => $fine
                    ]
            ]];
        }
        $pipeline = [$match,
            ['$lookup' => [
                    'from' => 'mdl_utenti_complete',
                    'localField' => 'userid',
                    'foreignField' => 'userid',
                    'as' => 'info'
                ]], ['$set' => [
                    'username' => [
                        '$arrayElemAt' => [
                            '$info.username',
                            0
                        ]
                    ],
                    'email' => [
                        '$arrayElemAt' => [
                            '$info.email',
                            0
                        ]
                    ],
                    'cf' => [
                        '$arrayElemAt' => [
                            '$info.idnumber',
                            0
                        ]
                    ],
                    'nome' => [
                        '$arrayElemAt' => [
                            '$info.nome',
                            0
                        ]
                    ],
                    'citta' => [
                        '$arrayElemAt' => [
                            '$info.citta',
                            0
                        ]
                    ],
                    'data' => [
                        '$arrayElemAt' => [
                            '$info.data',
                            0
                        ]
                    ],
                    'azienda' => [
                        '$arrayElemAt' => [
                            '$info.azienda',
                            0
                        ]
                    ]
                    ,
                    'phone2' => [
                        '$arrayElemAt' => [
                            '$info.phone2',
                            0
                        ]
                    ],
                    'phone1' => [
                        '$arrayElemAt' => [
                            '$info.phone1',
                            0
                        ]
                    ],
                ]], ['$project' => [
                    'info' => 0
        ]]];
        $out = $collection->aggregate($pipeline);
        return $out->toArray();
    }

    public function constructLogArray($array) {
        $res = array();
        foreach ($array as $singolo) {
            if ($singolo['action'] == "failed")
                continue;
            $tmp = array();

            $tmp['id'] = $singolo['id'];
            $tmp['userid'] = $singolo['userid'];
            if (!isset($singolo['nome']))
                continue;
            $name = $singolo['nome'];
            $tmp['name'] = $name;
            $tmp['username'] = $singolo['username'];
            $tmp['email'] = $singolo['email'];
            if (isset($singolo['cf']))
                $tmp['cf'] = $singolo['cf'];
            else
                $tmp['cf'] = "n/d";

            if (!isset($singolo['data']))
                $tmp['data_nascita'] = "n/d";
            else
                $tmp['data_nascita'] = date("d/m/Y", $singolo['data']);
            if (!isset($singolo['citta']))
                $tmp['citta'] = "n/d";
            else
                $tmp['citta'] = $singolo['citta'];
            $data = date("d/m/Y", $singolo['timecreated']);
            $tmp['timecreated'] = $data;
            $orario = date("H:i:s", $singolo['timecreated']);
            $tmp['timestamp'] = $singolo['timecreated'];
            $tmp['orario'] = $orario;
            $tmp['action'] = $singolo['action'];
            $tmp['azienda'] = $singolo['azienda'];
            $tmp['phone'] = "n/d";
            if (isset($singolo['phone1']) && $singolo['phone1'] != "")
                $tmp['phone'] = $singolo['phone1'];
            if (isset($singolo['phone1']) && $singolo['phone2'] != "")
                $tmp['phone'] = $singolo['phone2'];
            array_push($res, $tmp);
        }
        return $res;
    }

    public function constructSintesiReport($array) {
        $array = $this->sortArray($array);

        if (isset($_GET['debug'])) {
            print_r($array);
            exit();
        }
        $result = array();
        $prov = null;
        $primo = array();
        $prec = array(); //per risolvere viewed dashboard
        $contatore = 1;

        foreach ($array as $singolo) {

            if ($prov == null) {
                $prov = $singolo;
                $primo = $prov;
                $prec = $prov;
                $contatore++;
                continue;
            }
            $contatore++;
            $data_prov = str_replace("/", "", $prov['timecreated']);
            $data_singolo = str_replace("/", "", $singolo['timecreated']);
            if ($prov['userid'] == $singolo['userid']) {
                if ($data_prov == $data_singolo) {
                    if ($singolo['action'] == 'loggedin' || $singolo['action'] == 'joined') {

                        if ($singolo['action'] == 'joined' && $prov['action'] == 'joined') {

                            $differenza = $singolo['timestamp'] - $prov['timestamp'];
                            if ($differenza < 3600) {
                                if ($prec['action'] == 'left') {
                                    $primo['orario_logout'] = $prec['orario'];
                                    $primo['id_logout'] = $prec['id'];
                                    $tempo_cumulato = $prec['timestamp'] - $primo['timestamp'];
                                    $tempo_cumulato = $this->convertSeconds($tempo_cumulato);
                                    $primo['tempo_cumulato'] = $tempo_cumulato;
                                    array_push($result, $primo);
                                    $primo = $singolo;
                                    $prov = $singolo;
                                    $prec = $singolo;
                                }
                                continue;
                            }
                        }

                        if ($singolo['action'] == "joined") {
                            if ($prec['action'] == "left") {
                                $primo['orario_logout'] = $prec['orario'];
                                $primo['id_logout'] = $prec['id'];
                                $tempo_cumulato = $prec['timestamp'] - $primo['timestamp'];
                                $tempo_cumulato = $this->convertSeconds($tempo_cumulato);
                                $primo['tempo_cumulato'] = $tempo_cumulato;
                                array_push($result, $primo);
                                $primo = $singolo;
                                $prov = $singolo;
                                $prec = $singolo;
                                continue;
                            }
                        }
                        if ($singolo['timestamp'] == $prov['timestamp']) {
                            if ($prec == $prov) {
                                $primo = $singolo;
                                $prov = $singolo;
                                $prec = $prov;
                                continue;
                            } else {
                                $primo['orario_logout'] = $prec['orario'];
                                $primo['id_logout'] = $prec['id'];
                                $tempo_cumulato = $prec['timestamp'] - $primo['timestamp'];
                                $tempo_cumulato = $this->convertSeconds($tempo_cumulato);
                                $primo['tempo_cumulato'] = $tempo_cumulato;
                                array_push($result, $primo);
                            }
                        } else {
                            if ($primo == $prov)
                                continue;
                            $primo['orario_logout'] = $prov['orario'];
                            $primo['id_logout'] = $prov['id'];
                            $tempo_cumulato = $prov['timestamp'] - $primo['timestamp'];
                            $tempo_cumulato = $this->convertSeconds($tempo_cumulato);
                            $primo['tempo_cumulato'] = $tempo_cumulato;
                            array_push($result, $primo);
                        }
                        $prov = $singolo;
                        $primo = $singolo;
                        $prec = $prov;
                    } else if ($singolo['action'] == 'loggedout' || $singolo['action'] == 'left') {

                        if ($singolo['action'] == "left") {
                            $prec = $singolo;
                            $prov = $singolo;
                        } else {
                            $primo['orario_logout'] = $singolo['orario'];
                            $primo['id_logout'] = $singolo['id'];
                            $tempo_cumulato = $singolo['timestamp'] - $primo['timestamp'];
                            $tempo_cumulato = $this->convertSeconds($tempo_cumulato);
                            $primo['tempo_cumulato'] = $tempo_cumulato;
                            array_push($result, $primo);
                            $prov = null;
                            continue;
                        }
                    } else if ($singolo['action'] != 'loggedout' || $singolo['action'] != 'left') {
                        $prec = $prov;
                        $prov = $singolo;
                    }
                } else {

                    $midnight = $this->getMidnight($singolo['timecreated']);
                    $fourHours = $midnight + 60 * 60 * 6;

                    if ($singolo['timestamp'] >= $midnight && $singolo['timestamp'] <= $fourHours && $singolo['action'] != 'loggedin') {
                        $primo['orario_logout'] = "23:59:59";
                        $primo['id_logout'] = $primo['id'];

                        $beforeMidnight = $this->getTime($primo['timecreated'], true);
                        $tempo_cumulato = $beforeMidnight - $primo['timestamp'];
                        $tempo_cumulato = $this->convertSeconds($tempo_cumulato);
                        $primo['tempo_cumulato'] = $tempo_cumulato;
                        array_push($result, $primo);

                        $primo = $singolo;
                        $primo['orario'] = "00:00:00";
                        $afterMidnight = $this->getTime($singolo['timecreated'], null, true);
                        $primo['timestamp'] = $afterMidnight;
                        $prec = $prov;
                        $prov = $singolo;
                    } else {
                        if ($prec['action'] == "left") {
                            $primo['orario_logout'] = $prec['orario'];
                            $primo['id_logout'] = $prec['id'];
                            $tempo_cumulato = $prec['timestamp'] - $primo['timestamp'];
                            $tempo_cumulato = $this->convertSeconds($tempo_cumulato);
                            $primo['tempo_cumulato'] = $tempo_cumulato;
                            array_push($result, $primo);
                        } else {
                            $primo['orario_logout'] = $prov['orario'];
                            $primo['id_logout'] = $prov['id'];
                            $tempo_cumulato = $prov['timestamp'] - $primo['timestamp'];
                            $tempo_cumulato = $this->convertSeconds($tempo_cumulato);
                            $primo['tempo_cumulato'] = $tempo_cumulato;
                            array_push($result, $primo);
                        }
                        $prov = $singolo;
                        $primo = $singolo;
                        $prec = $prov;
                    }
                }

                if ($contatore == count($array)) {
                    if ($prec['action'] == "left") {
                        $primo['orario_logout'] = $prec['orario'];
                        $primo['id_logout'] = $prec['id'];
                        $tempo_cumulato = $prec['timestamp'] - $primo['timestamp'];
                        $tempo_cumulato = $this->convertSeconds($tempo_cumulato);
                        $primo['tempo_cumulato'] = $tempo_cumulato;
                        array_push($result, $primo);
                    } else {
                        $primo['orario_logout'] = $prov['orario'];
                        $primo['id_logout'] = $prov['id'];
                        $tempo_cumulato = $prov['timestamp'] - $primo['timestamp'];
                        $tempo_cumulato = $this->convertSeconds($tempo_cumulato);
                        $primo['tempo_cumulato'] = $tempo_cumulato;
                        array_push($result, $primo);
                    }
                }
            } else {
                if ($prec['action'] == "left") {
                    $primo['orario_logout'] = $prec['orario'];
                    $primo['id_logout'] = $prec['id'];
                    $tempo_cumulato = $prec['timestamp'] - $primo['timestamp'];
                    $tempo_cumulato = $this->convertSeconds($tempo_cumulato);
                    $primo['tempo_cumulato'] = $tempo_cumulato;
                    array_push($result, $primo);
                } else {
                    $primo['orario_logout'] = $prov['orario'];
                    $primo['id_logout'] = $prov['id'];
                    $tempo_cumulato = $prov['timestamp'] - $primo['timestamp'];
                    $tempo_cumulato = $this->convertSeconds($tempo_cumulato);
                    $primo['tempo_cumulato'] = $tempo_cumulato;
                    array_push($result, $primo);
                }
                $prov = $singolo;
                $primo = $singolo;
                $prec = $prov;
            }
        }
        return $result;
    }

    public function sortArray($array) {
        uasort($array, function ($first, $second) {
            if ($first['userid'] == $second['userid']) {
                return $first['timestamp'] - $second['timestamp'];
            } else {
                return $first['userid'] <= $second['userid'];
            }
        });
        return $array;
    }

    public function setSintesiSheet($index, $spreadsheet, $letter, $filtri, $j, $result) {
        if (!in_array("data", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Data/Ora");
            $letter++;
        }
        if (!in_array("userid", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Userid");
            $letter++;
        }
        if (!in_array("username", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Username");
            $letter++;
        }
        if (!in_array("name", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Nome Cognome");
            $letter++;
        }
        if (!in_array("cf", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Codice Fiscale");
            $letter++;
        }
        if (!in_array("email", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Email");
            $letter++;
        }
        if (!in_array("dn", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Data di nascita");
            $letter++;
        }
        if (!in_array("ln", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Luogo di nascita");
            $letter++;
        }
        if (!in_array("azienda", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Azienda");
            $letter++;
        }

        if (!in_array("phone", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "N.di telefono");
            $letter++;
        }
        if (!in_array("orarioA", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Ora del login");
            $letter++;
        }
        if (!in_array("orarioL", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Ora del logout");
            $letter++;
        }
        if (!in_array("tc", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Tempo cumulato");
        }



        $spreadsheet->getActiveSheet()->setTitle("Report");


        $from = "A1";
        $to = $letter . $index;
        $spreadsheet->getActiveSheet()->getStyle("$from:$to")->getFont()->setBold(true);


        $i = $index + 1;
        foreach ($result as $singolo) {
            $letter = "A";
            if (!in_array("data", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['timecreated']);
                $letter++;
            }
            if (!in_array("userid", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['userid']);
                $letter++;
            }
            if (!in_array("username", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['username']);
                $letter++;
            }

            if (!in_array("name", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['name']);
                $letter++;
            }
            if (!in_array("cf", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, @$singolo['cf']);
                $letter++;
            }
            if (!in_array("email", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['email']);
                $letter++;
            }
            if (!in_array("dn", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['data_nascita']);
                $letter++;
            }
            if (!in_array("ln", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['citta']);
                $letter++;
            }
            if (!in_array("azienda", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['azienda']);
                $letter++;
            }
            if (!in_array("phone", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['phone']);
                $letter++;
            }
            if (!in_array("orarioA", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["orario"]);
                $letter++;
            }
            if (!in_array("orarioL", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["orario_logout"]);
                $letter++;
            }
            if (!in_array("tc", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["tempo_cumulato"]);
            }

            $i++;
        }
        foreach (range('A', $letter) as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
        }
        $spreadsheet->getActiveSheet()->getStyle("A" . $index . ":" . $letter . $index)
                ->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('00FF7F');

        return $i;
    }

    public function setTimeinSheet($index, $spreadsheet, $letter, $filtri, $j, $finale, $array_mesi, $fromReport = null) {

        if (!in_array("userid", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Userid");
            $letter++;
        }
        if (!in_array("username", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Username");
            $letter++;
        }

        if (!in_array("name", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Nome Cognome");
            $letter++;
        }
        if (!in_array("cf", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Codice Fiscale");
            $letter++;
        }
        if (!in_array("email", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Email");
            $letter++;
        }
        if (!in_array("dn", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Data di Nascita");
            $letter++;
        }
        if (!in_array("ln", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Luogo di Nascita");
            $letter++;
        }
        if (!in_array("azienda", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Azienda");
            $letter++;
        }
        if (!in_array("phone", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "N.di telefono");
            $letter++;
        }
        if (isset($fromReport) && !in_array("corso", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Corso");
            $letter++;
        }
        if (isset($fromReport) && !in_array("courseid", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Id Corso");
            $letter++;
        }
        if (!in_array("tc", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue("$letter" . $index, "Tempo cumulato");
            $letter++;
        }


        $column = $letter;
        $giro = 0;

        foreach ($array_mesi as $mese) {
            if (!in_array("data_" . $giro, $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($column . $index, $mese);
                $column++;
            }
            $giro++;
        }


        $spreadsheet->getActiveSheet()->setTitle("Report");


        $from = "A1";
        $to = "$column" . $index;
        $spreadsheet->getActiveSheet()->getStyle("$from:$to")->getFont()->setBold(true);

        $i = $index + 1;

        foreach ($finale as $singolo) {
            $letter = 'A';

            if (!in_array("userid", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['userid']);
                $letter++;
            }
            if (!in_array("username", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['username']);
                $letter++;
            }
            if (!in_array("name", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['name']);
                $letter++;
            }
            if (!in_array("cf", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['cf']);
                $letter++;
            }
            if (!in_array("email", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['email']);
                $letter++;
            }

            if (!in_array("dn", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, @$singolo['data_nascita']);
                $letter++;
            }

            if (!in_array("ln", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, @$singolo['citta']);
                $letter++;
            }

            if (!in_array("azienda", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['azienda']);
                $letter++;
            }
            if (!in_array("phone", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['phone']);
                $letter++;
            }

            if (isset($fromReport) && !in_array("corso", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['corso']);
                $letter++;
            }
            if (isset($fromReport) && !in_array("courseid", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['courseid']);
                $letter++;
            }
            if (!in_array("tc", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue("$letter$i", $singolo["tempo_tot"]);
                $letter++;
            }

            $column = $letter;
            $giro = 0;
            foreach ($singolo['mesi'] as $value) {
                if (!in_array("data_" . $giro, $filtri)) {
                    $spreadsheet->setActiveSheetIndex($j)->setCellValue("$column$i", $value);
                    $column++;
                }
                $giro++;
            }


            $i++;
        }
        foreach (range('A', "$column") as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
        }
        $spreadsheet->getActiveSheet()->getStyle("A" . $index . ":" . $column . $index)
                ->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('00FF7F');

        return $i;
    }

    public function getNameAttivitaBBB($db) {
        $name = array();
        $collection = $this->conn->$db->mdl_bigbluebuttonbn;
        $cursore = $collection->find(
                ['id' => ['$ne' => 0]],
                ['projection' => ['name' => 1, 'id' => 1]]
        );
        foreach ($cursore as $singolo) {
            $name[$singolo['id']] = $singolo['name'];
        }
        return $name;
    }

    public function getNameAttivitaHVP($db) {
        $name = array();
        $collection = $this->conn->$db->mdl_hvp;
        $cursore = $collection->find(
                [],
                ['projection' => ['name' => 1, 'id' => 1]]
        );
        foreach ($cursore as $singolo) {
            if (isset($singolo['name'])) {
                $name[$singolo['id']] = $singolo['name'];
            }
        }
        return $name;
    }

    public function getAllNameCourse($db) {
        $name = array();
        $collection = $this->conn->$db->mdl_course;
        $cursore = $collection->find(
                ['id' => ['$ne' => 0]],
                ['projection' => ['fullname' => 1, 'id' => 1]]
        );
        foreach ($cursore as $singolo) {
            $name[$singolo['id']] = $singolo['fullname'];
        }
        return $name;
    }

    public function constructTotalLogArray($array, $bbbName, $hvpName, $nameCorsi) {
        $finale = array();
        foreach ($array as $singolo) {

            $tmp = array();
            $tmp['id'] = $singolo['id'];
            $tmp['userid'] = $singolo['userid'];
            if (!isset($singolo['nome']))
                continue;
            $name = $singolo['nome'];
            $tmp['name'] = $name;
            $tmp['username'] = $singolo['username'];
            $tmp['email'] = $singolo['email'];
            if (isset($singolo['cf']))
                $tmp['cf'] = $singolo['cf'];
            else
                $tmp['cf'] = "n/d";

            if (!isset($singolo['data']))
                $tmp['data_nascita'] = "n/d";
            else
                $tmp['data_nascita'] = date("d/m/Y", $singolo['data']);


            if (!isset($singolo['citta']))
                $tmp['citta'] = "n/d";
            else
                $tmp['citta'] = $singolo['citta'];
            $time = date("d/m/Y H:i:s", $singolo['timecreated']);
            $tmp['timecreated'] = $time;
            $tmp['ip'] = $singolo['ip'];
            $tmp['action'] = $singolo['action'];
            $tmp['attivita'] = "";
            $tmp['phone'] = "n/d";
            if (isset($singolo['phone1']) && $singolo['phone1'] != "")
                $tmp['phone'] = $singolo['phone1'];
            if (isset($singolo['phone1']) && $singolo['phone2'] != "")
                $tmp['phone'] = $singolo['phone2'];
            if (($singolo['action'] == 'viewed' && $singolo['target'] != "course") && $singolo['objecttable'] == null || $singolo['target'] == 'user' || $singolo['target'] == 'user_login') {
                $toReplace = ['{', '}', ';', '"'];
                $singolo['other'] = str_replace($toReplace, " ", $singolo['other']);

                $tmp['attivita'] = $singolo['other'];
            }
            if ($singolo['component'] == "mod_hvp") {
                if (isset($hvpName[$singolo['objectid']]))
                    $tmp['attivita'] = $hvpName[$singolo['objectid']];
            } else if ($singolo['component'] == "mod_bigbluebuttonbn") {
                if (isset($bbbName[$singolo['objectid']]))
                    $tmp['attivita'] = $bbbName[$singolo['objectid']];
            }
            if ($singolo['target'] == "course") {
                if (isset($nameCorsi[$singolo['courseid']]))
                    $tmp['attivita'] = $nameCorsi[$singolo['courseid']];
            }
            if ($tmp['attivita'] == "null" || $tmp['attivita'] == null || $tmp['attivita'] == "")
                continue;

            array_push($finale, $tmp);
        }

        return $finale;
    }

    public function setTotalLogSheet($index, $spreadsheet, $letter, $filtri, $j, $finale) {

        if (!in_array("data", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Data/Ora");
            $letter++;
        }
        if (!in_array("userid", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Userid");
            $letter++;
        }
        if (!in_array("username", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Username");
            $letter++;
        }
        if (!in_array("name", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Nome Cognome");
            $letter++;
        }

        if (!in_array("cf", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Codice Fiscale");
            $letter++;
        }

        if (!in_array("email", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Email");
            $letter++;
        }

        if (!in_array("dn", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Data di nascita");
            $letter++;
        }

        if (!in_array("ln", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Luogo di nascita");
            $letter++;
        }
        if (!in_array("phone", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "N.di telefono");
            $letter++;
        }
        if (!in_array("attivita", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Attività tracciata");
            $letter++;
        }
        if (!in_array("azione", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Azione");
            $letter++;
        }

        if (!in_array("ip", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Ip");
        }

        $spreadsheet->getActiveSheet()->setTitle("Report");


        $from = "A13";
        $to = $letter . $index;
        $spreadsheet->getActiveSheet()->getStyle("$from:$to")->getFont()->setBold(true);

        $i = $index + 1;
        foreach ($finale as $singolo) {
            $letter = 'A';

            if (!in_array("data", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['timecreated']);
                $letter++;
            }

            if (!in_array("userid", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['userid']);
                $letter++;
            }
            if (!in_array("username", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['username']);
                $letter++;
            }
            if (!in_array("name", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['name']);
                $letter++;
            }

            if (!in_array("cf", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['cf']);
                $letter++;
            }

            if (!in_array("email", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['email']);
                $letter++;
            }

            if (!in_array("dn", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['data_nascita']);
                $letter++;
            }

            if (!in_array("ln", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['citta']);
                $letter++;
            }
            if (!in_array("phone", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['phone']);
                $letter++;
            }

            if (!in_array("attivita", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, @$singolo["attivita"]);
                $letter++;
            }
            if (!in_array("azione", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["action"]);
                $letter++;
            }
            if (!in_array("ip", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["ip"]);
            }
            $i++;
        }
        foreach (range('A', $letter) as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
        }
        $spreadsheet->getActiveSheet()->getStyle("A" . $index . ":" . $letter . $index)
                ->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('00FF7F');

        return $i;
    }

    public function getAggregateHvp($db, $inizio, $fine, $valoreCorso = null, $array_id = null) {
        $collection = $this->conn->$db->mdl_logstore_standard_log;

        if (isset($valoreCorso) && isset($array_id)) {
            if ($valoreCorso == 0) {
                $match = ['$match' => [
                        'userid' => [
                            '$in' => $array_id
                        ],
                        'component' => "mod_hvp",
                        'timecreated' => [
                            '$gte' => $inizio,
                            '$lte' => $fine
                        ]
                ]];
            } else if (count($array_id) == 0) {
                $match = ['$match' => [
                        'courseid' => (int) $valoreCorso,
                        'component' => "mod_hvp",
                        'timecreated' => [
                            '$gte' => $inizio,
                            '$lte' => $fine
                        ]
                ]];
            } else {
                $match = ['$match' => [
                        'userid' => [
                            '$in' => $array_id
                        ],
                        'courseid' => (int) $valoreCorso,
                        'component' => "mod_hvp",
                        'timecreated' => [
                            '$gte' => $inizio,
                            '$lte' => $fine
                        ]
                ]];
            }
        } else if (isset($array_id) && !isset($valoreCorso)) {
            $match = ['$match' => [
                    'userid' => [
                        '$in' => $array_id
                    ],
                    'component' => "mod_hvp",
                    'timecreated' => [
                        '$gte' => $inizio,
                        '$lte' => $fine
                    ]
            ]];
        } else if (isset($valoreCorso) && !isset($array_id)) {
            $match = ['$match' => [
                    'courseid' => (int) $valoreCorso,
                    'component' => "mod_hvp",
                    'timecreated' => [
                        '$gte' => $inizio,
                        '$lte' => $fine
                    ]
            ]];
        }

        $pipeline = [
            $match
            , ['$lookup' => [
                    'from' => 'mdl_utenti_complete',
                    'localField' => 'userid',
                    'foreignField' => 'userid',
                    'as' => 'info'
                ]], ['$set' => [
                    'username' => [
                        '$arrayElemAt' => [
                            '$info.username',
                            0
                        ]
                    ],
                    'email' => [
                        '$arrayElemAt' => [
                            '$info.email',
                            0
                        ]
                    ],
                    'cf' => [
                        '$arrayElemAt' => [
                            '$info.idnumber',
                            0
                        ]
                    ],
                    'nome' => [
                        '$arrayElemAt' => [
                            '$info.nome',
                            0
                        ]
                    ],
                    'citta' => [
                        '$arrayElemAt' => [
                            '$info.citta',
                            0
                        ]
                    ],
                    'data' => [
                        '$arrayElemAt' => [
                            '$info.data',
                            0
                        ]
                    ],
                    'azienda' => [
                        '$arrayElemAt' => [
                            '$info.azienda',
                            0
                        ]
                    ]
                    ,
                    'phone2' => [
                        '$arrayElemAt' => [
                            '$info.phone2',
                            0
                        ]
                    ],
                    'phone1' => [
                        '$arrayElemAt' => [
                            '$info.phone1',
                            0
                        ]
                    ],
                ]], ['$project' => [
                    'info' => 0
        ]]];

        $out = $collection->aggregate($pipeline);

        return $out->toArray();
    }

    public function constructHvpArray($array, $hvpName, $nameCorsi) {
        $result = array();

        foreach ($array as $singolo) {
            if (!isset($singolo['nome']))
                continue;
            if (!isset($singolo['azienda']))
                continue;
            $tmp = array();
            $tmp['id'] = $singolo['id'];
            $tmp['userid'] = $singolo['userid'];
            $name = $singolo['nome'];
            $tmp['name'] = $name;
            $tmp['username'] = $singolo['username'];
            $tmp['email'] = $singolo['email'];
            if (isset($singolo['cf']))
                $tmp['cf'] = $singolo['cf'];
            else
                $tmp['cf'] = "n/d";

            if (!isset($singolo['data']))
                $tmp['data_nascita'] = "n/d";
            else
                $tmp['data_nascita'] = date("d/m/Y", (int) $singolo['data']);

            if (!isset($singolo['citta']))
                $tmp['citta'] = "n/d";
            else
                $tmp['citta'] = $singolo['citta'];
            $tmp['phone'] = "n/d";
            if (isset($singolo['phone1']) && $singolo['phone1'] != "")
                $tmp['phone'] = $singolo['phone1'];
            if (isset($singolo['phone1']) && $singolo['phone2'] != "")
                $tmp['phone'] = $singolo['phone2'];
            $time = date("d/m/Y", $singolo['timecreated']);
            $tmp['timecreated'] = $time;
            $orario = date("H:i:s", $singolo['timecreated']);
            $tmp['timestamp'] = $singolo['timecreated'];
            $tmp['orario'] = $orario;
            $tmp['ip'] = $singolo['ip'];
            $tmp['action'] = $singolo['action'];
            $tmp['objectid'] = $singolo['objectid'];
            $tmp['azienda'] = $singolo['azienda'];
            $tmp['other'] = $singolo['other'];
            if (isset($hvpName[$singolo['objectid']]))
                $tmp['lezione'] = $hvpName[$singolo['objectid']];
            else
                $tmp['lezione'] = "";
            if (isset($nameCorsi[$singolo['courseid']]))
                $tmp['corso'] = $nameCorsi[$singolo['courseid']];
            else
                $tmp['corso'] = "";
            $tmp['courseid'] = $singolo['courseid'];


            array_push($result, $tmp);
        }
        return $result;
    }

    public function constructHvpReport($array, $db) {
        $array = $this->sortArray($array);

        //var_dump($array);exit();

        if (isset($_GET['debug'])) {
            print_r($array);
            exit();
        }
        $finale = array();
        $prov = null;
        $prec = array();
        $tmp = array();
        $viewed = 0;
        $lezioni_terminate = array();
        $entrato = false;
        $last= end($array);

        foreach ($array as $singolo) {

            if (strpos($singolo['other'], "RESET") !== FALSE) {
               //continue;
                foreach ($finale as $key => $elem) {
                    if ($elem['userid'] == $singolo['userid'] && $elem['objectid'] == $singolo['objectid']) {

                        unset($finale[$key]);
                        unset($lezioni_terminate[$singolo['lezione']]);
                        $entrato = true;
                    }
                }
                if ($entrato == false)
                    continue;
            }

            if ($prov == null) {
                if (isset($prec['userid']) && $singolo['userid'] != $prec['userid'])
                    $lezioni_terminate = array();
                $prov = $singolo;
                $prec = $prov;
                continue;
            }

            if ($prov['userid'] == $singolo['userid'] && $prov['timecreated'] == $singolo['timecreated']) {//var_dump($prov);var_dump($singolo);exit('aaaa');
                /*if($singolo['userid']==2122 && $prov['objectid']==969){
                        print_r($singolo);
                        print_r($prov);
                        print_r($lezioni_terminate);
                        exit();
                    }*/
                if ($prov['action'] == 'viewed' && $singolo['action'] == 'progress') {
                    if (count($array) == 2 || $singolo==$last) {
                        $prov['orario_logout'] = $singolo['orario'];
                        $prov['id_logout'] = $singolo['id'];
                        $progress = $singolo['other'];
                        $toReplace = ['{', '}', '"'];
                        $progress = str_replace($toReplace, "", $progress);

                        if ($progress != "") {
                            $progress = explode(".", $progress);
                            $progress = explode(":", $progress[0]);
                            @$tempo_cumulato = (int) @$progress[1];
                        } else
                            $tempo_cumulato = 0;
                        $tempo_cumulato = $this->convertSeconds($tempo_cumulato);

                        if($prov['userid']==2221) {
                            //echo $prov['userid'].' Data:'.$prov['timecreated'].' Tempo cumulato:'.$tempo_cumulato.' other:'.$prov['other'].' objectid:'.$prov['objectid'].' Action: '.$prov['action']."<br>";
                        }

                        $prov['tempo_cumulato'] = $tempo_cumulato;
                        array_push($finale, $prov);
                        break;
                    }
                    $prec = $singolo;
                    $viewed = 1;
                } else if ($singolo['action'] == "submitted") {

                    /* MODIFICA ALDO
                    $tempo_cumulatoSumittedTotale = $this->getLastProgress($prov['userid'], $prov['timecreated'], $prov['objectid'], $db);

                    */
                    $tempo_cumulato = $this->getTimeLesson($prov['objectid'], $db);

                    /* MODIFICA ALDO $tempo_cumulato = $this->convertSeconds(($tempo_cumulato - $tempo_cumulatoSumittedTotale));*/

                    $tempo_cumulato = $this->convertSeconds(($tempo_cumulato));

                    $prov['orario_logout'] = $singolo['orario'];
                    $prov['id_logout'] = $singolo['id'];
                    $prov['tempo_cumulato'] = $tempo_cumulato;

//                    if($prov['userid']==2221) {
//                        //echo $prov['userid'].' Data:'.$prov['timecreated'].' Tempo cumulato:'.$tempo_cumulato.' other:'.$prov['other'].' objectid:'.$prov['objectid'].' Action: '.$prov['action']."<br>";
//                    }

                    array_push($finale, $prov);
                    $lezioni_terminate[$prov['lezione']] = $prov['objectid'];
                    $prov = null;
                    $viewed = 1;
                } else if ($singolo['action'] == "viewed" && $prov['action'] != "progress") {
                    if ($prec['action'] == "progress") {
                        $prov['orario_logout'] = $prec['orario'];
                        $prov['id_logout'] = $prec['id'];
                        $progress = $prec['other'];
                        $toReplace = ['{', '}', '"'];
                        $progress = str_replace($toReplace, "", $progress);
                        if ($progress != "") {
                            $progress = explode(".", $progress);
                            $progress = explode(":", $progress[0]);
                            @$tempo_cumulato = (int) @$progress[1];
                        } else
                            $tempo_cumulato = 0;
                        $tempo_cumulato = $this->convertSeconds($tempo_cumulato);
                        $prov['tempo_cumulato'] = $tempo_cumulato;

                        if($prov['userid']==2221) {
                            //echo $prov['userid'].' Data:'.$prov['timecreated'].' Tempo cumulato:'.$tempo_cumulato.' other:'.$prov['other'].' objectid:'.$prov['objectid'].' Action: '.$prov['action']."<br>";
                        }


                        if (!array_key_exists($prov['lezione'], $lezioni_terminate))
                            array_push($finale, $prov);
                        $viewed = 1;
                    } else {
                        if (array_key_exists($singolo['lezione'], $lezioni_terminate)) {
                            continue;
                        }
                    }
                    $prov = $singolo;
                    $prec = $prov;
                } else if ($prov['action'] == "progress") {
                    $prov = $singolo;
                    $prec = $prov;
                }
            } else {

                if ($prec['action'] == "progress") {
                    $prov['orario_logout'] = $prec['orario'];
                    $prov['id_logout'] = $prec['id'];
                    $progress = $prec['other'];
                    $toReplace = ['{', '}', '"'];
                    $progress = str_replace($toReplace, "", $progress);
                    if ($progress != "") {
                        $progress = explode(".", $progress);
                        $progress = explode(":", $progress[0]);
                        @$tempo_cumulato = (int) @$progress[1];
                    } else
                        $tempo_cumulato = 0;
                    $tempo_cumulato = $this->convertSeconds($tempo_cumulato);
                    $prov['tempo_cumulato'] = $tempo_cumulato;

                    if($prov['userid']==2221) {
                        //echo $prov['userid'].' Data:'.$prov['timecreated'].' Tempo cumulato:'.$tempo_cumulato.' other:'.$prov['other'].' objectid:'.$prov['objectid'].' Action: '.$prov['action']."<br>";
                    }

                    if (!array_key_exists($prov['lezione'], $lezioni_terminate))
                        array_push($finale, $prov);
                }
                $viewed = 1;
                $lezioni_terminate = array();
                $prov = $singolo;
                $prec = $prov;
            }
        }
        //var_dump($finale);exit();
        /*if ( $singolo['userid'] == 2122 ) {
            var_
        }*/
//exit();

        $prov = null;
        $new = array();
        $temp = array();
        foreach ($finale as $singolo) {
            if ($prov == null) {
                $time_sing = explode(":", $singolo['tempo_cumulato']);
                $seconds_sing = $time_sing[0] * 3600 + $time_sing[1] * 60 + $time_sing[2];
                $temp[$singolo['lezione']] = $seconds_sing;
                $lastProgress = $this->getLastProgress($singolo['userid'], $singolo['timestamp'], $singolo['objectid'], $db);
                $tc = $seconds_sing - (int) $lastProgress;
                $singolo['diff'] = $this->convertSeconds($tc);
                array_push($new, $singolo);
                $prov = $singolo;
                continue;
            }
            $time_prov = explode(":", $prov['tempo_cumulato']);
            $time_sing = explode(":", $singolo['tempo_cumulato']);
            $seconds_prov = $time_prov[0] * 3600 + $time_prov[1] * 60 + $time_prov[2];
            $seconds_sing = $time_sing[0] * 3600 + $time_sing[1] * 60 + $time_sing[2];

            if ($prov['userid'] == $singolo['userid']) {
                if (array_key_exists($singolo['lezione'], $temp)) {
                    if ($seconds_sing >= $temp[$singolo['lezione']])
                        $diff = $seconds_sing - $temp[$singolo['lezione']];
                    else
                        $diff = 0;
                } else {
                    if (strpos($singolo['other'], "RESET") === FALSE)
                        $lastProgress = $this->getLastProgress($singolo['userid'], $singolo['timestamp'], $singolo['objectid'], $db);
                    else
                        $lastProgress = 0;
                    $diff = $seconds_sing - $lastProgress;
                }
                $temp[$singolo['lezione']] = $seconds_sing;

                $singolo['diff'] = $this->convertSeconds($diff);
//                if ($singolo['userid'] == 6527 && $singolo['objectid'] == 57) {
//                    echo $lastProgress . "<br>";
//                    print_r($singolo);
//                    exit();
//                }
                array_push($new, $singolo);
            } else {
                $temp = array();
                $lastProgress = $this->getLastProgress($singolo['userid'], $singolo['timestamp'], $singolo['objectid'], $db);
                $tc = $seconds_sing - (int) $lastProgress;
                $singolo['diff'] = $this->convertSeconds($tc);
                array_push($new, $singolo);
                $prov = $singolo;
            }
        }



        $new2 = array();
        foreach ($new as $singolo) {
            $singolo = json_decode(json_encode($singolo, true), true);
            if (isset($singolo))
                array_push($new2, $singolo);
        }



        return $new2;
    }

    public function setHvpSheet($index, $spreadsheet, $letter, $filtri, $j, $new) {
        if (!in_array("data", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Data");
            $letter++;
        }
        if (!in_array("userid", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Userid");
            $letter++;
        }

        if (!in_array("username", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Username");
            $letter++;
        }
        if (!in_array("name", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Nome Cognome");
            $letter++;
        }

        if (!in_array("cf", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Codice Fiscale");
            $letter++;
        }

        if (!in_array("email", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Email");
            $letter++;
        }

        if (!in_array("dn", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Data di nascita");
            $letter++;
        }

        if (!in_array("ln", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Luogo di nascita");
            $letter++;
        }

        if (!in_array("azienda", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Azienda");
            $letter++;
        }
        if (!in_array("phone", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "N.di telefono");
            $letter++;
        }
        if (!in_array("corso", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Corso");
            $letter++;
        }
        if (!in_array("lezione", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Lezione");
            $letter++;
        }
        if (!in_array("orarioA", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Ora accesso al video");
            $letter++;
        }
        if (!in_array("orarioL", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Ora uscita dal video");
            $letter++;
        }
        if (!in_array("tc", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Tempo cumulato");
            $letter++;
        }
        if (!in_array("ts", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Tempo sessione video");
        }

        $spreadsheet->getActiveSheet()->setTitle("Report");


        $from = "A1";
        $to = $letter . $index;
        $spreadsheet->getActiveSheet()->getStyle("$from:$to")->getFont()->setBold(true);

        $i = $index + 1;
        foreach ($new as $singolo) {
            $letter = 'A';
            if (!in_array("data", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['timecreated']);
                $letter++;
            }
            if (!in_array("userid", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['userid']);
                $letter++;
            }
            if (!in_array("username", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['username']);
                $letter++;
            }
            if (!in_array("name", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['name']);
                $letter++;
            }

            if (!in_array("cf", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, @$singolo['cf']);
                $letter++;
            }

            if (!in_array("email", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["email"]);
                $letter++;
            }

            if (!in_array("dn", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["data_nascita"]);
                $letter++;
            }

            if (!in_array("ln", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["citta"]);
                $letter++;
            }

            if (!in_array("azienda", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["azienda"]);
                $letter++;
            }
            if (!in_array("phone", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["phone"]);
                $letter++;
            }
            if (!in_array("corso", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["corso"]);
                $letter++;
            }
            if (!in_array("lezione", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["lezione"]);
                $letter++;
            }
            if (!in_array("orarioA", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["orario"]);
                $letter++;
            }
            if (!in_array("orarioL", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["orario_logout"]);
                $letter++;
            }
            if (!in_array("tc", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["tempo_cumulato"]);
                $letter++;
            }
            if (!in_array("ts", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["diff"]);
            }
            $i++;
        }

        foreach (range('A', $letter) as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
        }

        $spreadsheet->getActiveSheet()->getStyle("A" . $index . ":" . $letter . $index)
                ->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('00FF7F');

        return $i;
    }

    public function constructSHvpReport($array) {

        $prov = null;
        $mesi = array();
        $new = array();
        $cumulatoTot = 0;
        $tempo_tot = 0;
        $cont = 0;
        $totResult = count($array);
        $array_mesi = array();
        $temp = array();

        foreach ($array as $singolo) {

            if ($totResult == 1) {
                $data_singolo = str_replace("/", "", $singolo['timecreated']);
                $timesingolo_split = explode(":", $singolo['diff']);
                $seconds_singolo = $timesingolo_split[0] * 3600 + $timesingolo_split[1] * 60 + $timesingolo_split[2];
                $mesi[$singolo['timecreated']] = $singolo['tempo_cumulato'];
                array_push($array_mesi, $singolo['timecreated']);
                $tempo = explode(":", $singolo['tempo_cumulato']);
                $secondi = $tempo[0] * 3600 + $tempo[1] * 60 + $tempo[2];
                $tempo_tot = $secondi;
                $singolo['mesi'] = $mesi;
                $singolo['tempo_tot'] = $this->convertSeconds($tempo_tot);
                array_push($new, $singolo);

                break;
            }

            if ($prov == null) {
                $prov = $singolo;
                $cont++;
                continue;
            }
            $cont++;
            $data_prov = str_replace("/", "", $prov['timecreated']);
            $data_singolo = str_replace("/", "", $singolo['timecreated']);
            $timeprov_split = explode(":", $prov['diff']);
            $timesingolo_split = explode(":", $singolo['diff']);
            $seconds_prov = $timeprov_split[0] * 3600 + $timeprov_split[1] * 60 + $timeprov_split[2];
            $seconds_singolo = $timesingolo_split[0] * 3600 + $timesingolo_split[1] * 60 + $timesingolo_split[2];

            if ($prov['userid'] == $singolo['userid']) {


                if ($data_prov == $data_singolo) {
                    if ($cumulatoTot == 0) {
                        $cumulatoTot += $seconds_prov + $seconds_singolo;
                    } else {
                        $cumulatoTot += $seconds_singolo;
                    }
                    if (!in_array($prov['timecreated'], $array_mesi))
                        array_push($array_mesi, $prov['timecreated']);

                    $mesi[$prov['timecreated']] = $this->convertSeconds($cumulatoTot);

                    $prov = $singolo;
                    $prov['mesi'] = $mesi;
                    $prov['tempo_cumulato'] = $this->convertSeconds($cumulatoTot);
                } else {
                    if ($cumulatoTot == 0) {
                        $mesi[$prov['timecreated']] = $prov['diff'];
                    } else {
                        $mesi[$prov['timecreated']] = $prov['tempo_cumulato'];
                    }
                    $mesi[$singolo['timecreated']] = $singolo['tempo_cumulato'];
                    if (!in_array($prov['timecreated'], $array_mesi))
                        array_push($array_mesi, $prov['timecreated']);
                    if (!in_array($singolo['timecreated'], $array_mesi))
                        array_push($array_mesi, $singolo['timecreated']);

                    $prov = $singolo;
                    $prov['mesi'] = $mesi;
                    $cumulatoTot = 0;
                }
            } else {
                if (isset($prov['mesi'])) {
                    foreach ($prov['mesi'] as $tempo) {
                        $tempo = explode(":", $tempo);
                        $secondi = $tempo[0] * 3600 + $tempo[1] * 60 + $tempo[2];
                        $tempo_tot += $secondi;
                    }
                } else {
                    $mesi[$prov['timecreated']] = $prov['tempo_cumulato'];
                    $prov['mesi'] = $mesi;
                    if (!in_array($prov['timecreated'], $array_mesi))
                        array_push($array_mesi, $prov['timecreated']);
                    $tempo = explode(":", $prov['tempo_cumulato']);
                    $secondi = $tempo[0] * 3600 + $tempo[1] * 60 + $tempo[2];
                    $tempo_tot = $secondi;
                }

                $prov['tempo_tot'] = $this->convertSeconds($tempo_tot);

                array_push($new, $prov);
                $tempo_tot = 0;
                $cumulatoTot = 0;
                $prov = $singolo;
                $mesi = null;
            }
            if ($cont == $totResult) {
                if (isset($prov['mesi'])) {
                    foreach ($prov['mesi'] as $tempo) {
                        $tempo = explode(":", $tempo);
                        $secondi = $tempo[0] * 3600 + $tempo[1] * 60 + $tempo[2];
                        $tempo_tot += $secondi;
                    }
                } else {
                    $mesi[$prov['timecreated']] = $prov['tempo_cumulato'];
                    $prov['mesi'] = $mesi;
                    if (!in_array($prov['timecreated'], $array_mesi))
                        array_push($array_mesi, $prov['timecreated']);
                    $tempo = explode(":", $prov['tempo_cumulato']);
                    $secondi = $tempo[0] * 3600 + $tempo[1] * 60 + $tempo[2];
                    $tempo_tot = $secondi;
                }
                $prov['tempo_tot'] = $this->convertSeconds($tempo_tot);
                array_push($new, $prov);
            }
        }


        usort($array_mesi, "compareByTimeStamp");


        foreach ($new as &$singolo) {
            foreach ($array_mesi as $mese) {
                if (!in_array($mese, array_keys($singolo['mesi']))) {
                    $singolo['mesi'][$mese] = "00:00:00";
                }
            }
            uksort($singolo['mesi'], "compareByTimeStamp");
        }
        $return = json_encode($new) . "*" . json_encode($array_mesi);
        return $return;
    }

    public function convertSeconds($seconds) {
        $hours = floor($seconds / 3600);
        $mins = floor($seconds / 60 % 60);
        $secs = floor($seconds % 60);
        $timeFormat = sprintf('%02d:%02d:%02d', $hours, $mins, $secs);
        return $timeFormat;
    }

    public function getRuoliCorsi($db) {
        $collection = $this->conn->$db->corsi_ruoli;
        $cursore = $collection->find(
                [],
                ['projection' => ['userid' => 1, 'ruolo' => 1]]
        );
        $ruoli = array();

        foreach ($cursore as $singolo) {
            $ruolo = "";
            switch ($singolo['ruolo']) {
                case 'student':
                    $ruolo = "studente";
                    break;
                case 'manager':
                    $ruolo = "manager";
                    break;
                case 'editingteacher':
                    $ruolo = "docente";
                    break;
                case 'teacher':
                    $ruolo = "controller";
                    break;
                default:break;
            }
            $ruoli[$singolo['userid']] = $ruolo;
        }

        return $ruoli;
    }

    public function getAggregateBBB($db, $inizio, $fine, $idCorso = null, $array_id = null) {
        $collection = $this->conn->$db->mdl_logstore_standard_log;

        if (isset($idCorso) && isset($array_id)) {
            if ($idCorso == 0) {
                $match = ['$match' => [
                        'userid' => [
                            '$in' => $array_id
                        ],
                        'component' => "mod_bigbluebuttonbn",
                        'timecreated' => [
                            '$gte' => $inizio,
                            '$lte' => $fine
                        ],
                        'action' => [
                            '$ne' => 'viewed'
                        ]
                ]];
            } else if (count($array_id) == 0) {
                $match = ['$match' => [
                        'courseid' => (int) $idCorso,
                        'component' => "mod_bigbluebuttonbn",
                        'timecreated' => [
                            '$gte' => $inizio,
                            '$lte' => $fine
                        ],
                        'action' => [
                            '$ne' => 'viewed'
                        ]
                ]];
            } else {
                $match = ['$match' => [
                        'userid' => [
                            '$in' => $array_id
                        ],
                        'courseid' => (int) $idCorso,
                        'component' => "mod_bigbluebuttonbn",
                        'timecreated' => [
                            '$gte' => $inizio,
                            '$lte' => $fine
                        ],
                        'action' => [
                            '$ne' => 'viewed'
                        ]
                ]];
            }
        } else if (isset($idCorso) && !isset($array_id)) {
            $match = ['$match' => [
                    'courseid' => (int) $idCorso,
                    'component' => "mod_bigbluebuttonbn",
                    'timecreated' => [
                        '$gte' => $inizio,
                        '$lte' => $fine
                    ],
                    'action' => [
                        '$ne' => 'viewed'
                    ]
            ]];
        } else if (!isset($idCorso) && isset($array_id)) {
            $match = ['$match' => [
                    'userid' => [
                        '$in' => $array_id
                    ],
                    'component' => "mod_bigbluebuttonbn",
                    'timecreated' => [
                        '$gte' => $inizio,
                        '$lte' => $fine
                    ],
                    'action' => [
                        '$ne' => 'viewed'
                    ]
            ]];
        }
        $pipeline = [$match,
            ['$lookup' => [
                    'from' => 'mdl_utenti_complete',
                    'localField' => 'userid',
                    'foreignField' => 'userid',
                    'as' => 'info'
                ]], ['$set' => [
                    'username' => [
                        '$arrayElemAt' => [
                            '$info.username',
                            0
                        ]
                    ],
                    'email' => [
                        '$arrayElemAt' => [
                            '$info.email',
                            0
                        ]
                    ],
                    'cf' => [
                        '$arrayElemAt' => [
                            '$info.idnumber',
                            0
                        ]
                    ],
                    'nome' => [
                        '$arrayElemAt' => [
                            '$info.nome',
                            0
                        ]
                    ],
                    'citta' => [
                        '$arrayElemAt' => [
                            '$info.citta',
                            0
                        ]
                    ],
                    'data' => [
                        '$arrayElemAt' => [
                            '$info.data',
                            0
                        ]
                    ],
                    'azienda' => [
                        '$arrayElemAt' => [
                            '$info.azienda',
                            0
                        ]
                    ],
                    'ruolo' => [
                        '$arrayElemAt' => [
                            '$info.ruolo',
                            0
                        ]
                    ],
                    'phone2' => [
                        '$arrayElemAt' => [
                            '$info.phone2',
                            0
                        ]
                    ],
                    'phone1' => [
                        '$arrayElemAt' => [
                            '$info.phone1',
                            0
                        ]
                    ],
                ]], ['$project' => [
                    'info' => 0
        ]]];

        $out = $collection->aggregate($pipeline);
        $res = $out->toArray();


        return $res;
    }

    public function constructBbbArray($array, $bbbName, $nameCorsi) {
        $result = array();

        foreach ($array as $singolo) {
            $tmp = array();
            if (!isset($singolo['nome']))
                continue;
            if (isset($bbbName[$singolo['objectid']]))
                $tmp['aula'] = $bbbName[$singolo['objectid']];
            else
                continue;

            if (isset($nameCorsi[$singolo['courseid']]))
                $tmp['corso'] = $nameCorsi[$singolo['courseid']];
            else
                continue;

            $tmp['id'] = $singolo['id'];
            $tmp['userid'] = $singolo['userid'];

            $name = $singolo['nome'];
            $tmp['name'] = $name;
            $tmp['username'] = $singolo['username'];
            $tmp['email'] = $singolo['email'];

            $tmp['phone'] = "n/d";
            if (isset($singolo['phone1']) && $singolo['phone1'] != "")
                $tmp['phone'] = $singolo['phone1'];
            if (isset($singolo['phone1']) && $singolo['phone2'] != "")
                $tmp['phone'] = $singolo['phone2'];
            if (isset($singolo['cf']))
                $tmp['cf'] = $singolo['cf'];
            else
                $tmp['cf'] = "n/d";

            if (!isset($singolo['data']))
                $tmp['data_nascita'] = "n/d";
            else
                $tmp['data_nascita'] = date("d/m/Y", $singolo['data']);
            if (!isset($singolo['citta']))
                $tmp['citta'] = "n/d";
            else
                $tmp['citta'] = $singolo['citta'];
            $time = date("d/m/Y", $singolo['timecreated']);
            $tmp['timecreated'] = $time;
            $orario = date("H:i:s", $singolo['timecreated']);
            $tmp['timestamp'] = $singolo['timecreated'];
            $tmp['orario'] = $orario;
            $tmp['ip'] = $singolo['ip'];
            $tmp['action'] = $singolo['action'];
            $tmp['objectid'] = $singolo['objectid'];
            $tmp['courseid'] = $singolo['courseid'];
            if (isset($singolo['azienda']))
                $tmp['azienda'] = $singolo['azienda'];
            else
                $tmp['azienda'] = "n/d";
            $ruolo = "n/d";
            switch ($singolo['ruolo']) {
                case 'student':
                    $ruolo = "studente";
                    break;
                case 'manager':
                    $ruolo = "manager";
                    break;
                case 'editingteacher':
                    $ruolo = "docente";
                    break;
                case 'teacher':
                    $ruolo = "controller";
                    break;
                default:break;
            }
            $tmp['ruolo'] = $ruolo;
            array_push($result, $tmp);
        }

        return $result;
    }

    public function getEnrole($userid, $array) {

        if (isset($array[$userid])) {
            return $array[$userid];
        }
    }

    public function setBbbSheet($index, $spreadsheet, $letter, $filtri, $j, $finale) {
        if (!in_array("data", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Data");
            $letter++;
        }
        if (!in_array("userid", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Userid");
            $letter++;
        }

        if (!in_array("username", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Username");
            $letter++;
        }
        if (!in_array("name", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Nome Cognome");
            $letter++;
        }

        if (!in_array("cf", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Codice Fiscale");
            $letter++;
        }

        if (!in_array("email", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Email");
            $letter++;
        }

        if (!in_array("dn", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Data di nascita");
            $letter++;
        }

        if (!in_array("ln", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Luogo di nascita");
            $letter++;
        }

        if (!in_array("azienda", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Azienda");
            $letter++;
        }
        if (!in_array("phone", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "N.di telefono");
            $letter++;
        }

        if (!in_array("ruolo", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Ruolo");
            $letter++;
        }
        if (!in_array("corso", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Corso");
            $letter++;
        }
        if (!in_array("aula", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Nome Aula");
            $letter++;
        }
        if (!in_array("orarioA", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Ora ingresso");
            $letter++;
        }
        if (!in_array("orarioL", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Ora uscita");
            $letter++;
        }
        if (!in_array("tc", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Tempo cumulato");
        }




        $spreadsheet->getActiveSheet()->setTitle("Report");


        $from = "A1";
        $to = $letter . $index;
        $spreadsheet->getActiveSheet()->getStyle("$from:$to")->getFont()->setBold(true);

        $i = $index + 1;
        foreach ($finale as $singolo) {
            $letter = 'A';
            if (!in_array("data", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['timecreated']);
                $letter++;
            }
            if (!in_array("userid", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['userid']);
                $letter++;
            }
            if (!in_array("username", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['username']);
                $letter++;
            }
            if (!in_array("name", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['name']);
                $letter++;
            }

            if (!in_array("cf", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, @$singolo['cf']);
                $letter++;
            }

            if (!in_array("email", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["email"]);
                $letter++;
            }

            if (!in_array("dn", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, @$singolo["data_nascita"]);
                $letter++;
            }

            if (!in_array("ln", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, @$singolo["citta"]);
                $letter++;
            }

            if (!in_array("azienda", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, @$singolo["azienda"]);
                $letter++;
            }
            if (!in_array("phone", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, @$singolo["phone"]);
                $letter++;
            }

            if (!in_array("ruolo", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["ruolo"]);
                $letter++;
            }
            if (!in_array("corso", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["corso"]);
                $letter++;
            }
            if (!in_array("aula", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["aula"]);
                $letter++;
            }
            if (!in_array("orarioA", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["orario"]);
                $letter++;
            }
            if (!in_array("orarioL", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["orario_logout"]);
                $letter++;
            }
            if (!in_array("tc", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["tempo_cumulato"]);
            }


            $i++;
        }
        foreach (range('A', $letter) as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
        }

        $spreadsheet->getActiveSheet()->getStyle("A" . $index . ":" . $letter . $index)
                ->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('00FF7F');

        return $i;
    }

    public function setSintesiBbbSheet($index, $spreadsheet, $letter, $filtri, $j, $new, $array_mesi, $fromReport = null) {

        if (!in_array("userid", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Userid");
            $letter++;
        }
        if (!in_array("username", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Username");
            $letter++;
        }

        if (!in_array("name", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Nome Cognome");
            $letter++;
        }
        if (!in_array("cf", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Codice Fiscale");
            $letter++;
        }
        if (!in_array("email", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Email");
            $letter++;
        }
        if (!in_array("dn", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Data di Nascita");
            $letter++;
        }
        if (!in_array("ln", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Luogo di Nascita");
            $letter++;
        }
        if (!in_array("azienda", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Azienda");
            $letter++;
        }
        if (!in_array("phone", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "N.di telefono");
            $letter++;
        }
        if (!in_array("ruolo", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Ruolo");
            $letter++;
        }
        if (isset($fromReport) && !in_array("corso", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Corso");
            $letter++;
        }
        if (isset($fromReport) && !in_array("courseid", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Id Corso");
            $letter++;
        }
        if (!in_array("tc", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue("$letter" . $index, "Tempo cumulato");
            $letter++;
        }

        $column = $letter;
        $giro = 0;
        foreach ($array_mesi as $mese) {
            if (!in_array("data_" . $giro, $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($column . $index, $mese);
                $column++;
            }
            $giro++;
        }


        $spreadsheet->getActiveSheet()->setTitle("Report");


        $from = "A1";
        $to = "$column" . $index;
        $spreadsheet->getActiveSheet()->getStyle("$from:$to")->getFont()->setBold(true);

        $i = $index + 1;
        foreach ($new as $singolo) {
            $letter = 'A';

            if (!in_array("userid", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['userid']);
                $letter++;
            }
            if (!in_array("username", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['username']);
                $letter++;
            }
            if (!in_array("name", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['name']);
                $letter++;
            }
            if (!in_array("cf", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['cf']);
                $letter++;
            }
            if (!in_array("email", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['email']);
                $letter++;
            }

            if (!in_array("dn", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, @$singolo['data_nascita']);
                $letter++;
            }

            if (!in_array("ln", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, @$singolo['citta']);
                $letter++;
            }

            if (!in_array("azienda", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['azienda']);
                $letter++;
            }
            if (!in_array("phone", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['phone']);
                $letter++;
            }
            if (!in_array("ruolo", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['ruolo']);
                $letter++;
            }
            if (isset($fromReport) && !in_array("corso", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['corso']);
                $letter++;
            }
            if (isset($fromReport) && !in_array("courseid", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['courseid']);
                $letter++;
            }
            if (!in_array("tc", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue("$letter$i", $singolo["tempo_tot"]);
                $letter++;
            }


            $column = $letter;
            $giro = 0;
            foreach ($singolo['mesi'] as $value) {
                if (!in_array("data_" . $giro, $filtri)) {
                    $spreadsheet->setActiveSheetIndex($j)->setCellValue("$column$i", $value);
                    $column++;
                }
                $giro++;
            }


            $i++;
        }
        foreach (range('A', "$column") as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
        }
        $spreadsheet->getActiveSheet()->getStyle("A" . $index . ":" . $column . $index)
                ->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('00FF7F');

        return $i;
    }

    public function getMidnight($data) {
        $date = date_create_from_format("d/m/Y", $data);
        $date = date_format($date, 'Y-m-d');
        $midnight = strtotime($date);
        return $midnight;
    }

    public function getTime($data, $beforeMidnight = null, $afterMidnight = null) {
        $date = date_create_from_format("d/m/Y", $data);
        $date = date_format($date, 'Y-m-d');
        if (isset($beforeMidnight)) {
            $timestamp = strtotime($date . " 23:59:59");
        } else {
            $timestamp = strtotime($date . " 00:00:00");
        }
        return $timestamp;
    }

    public function getTimeLesson($objectid, $db) {
        $tempoLezione = 0;
        $collection = $this->conn->$db->mdl_hvp;
        $cursor = $collection->find(
                ['id' => (int) $objectid],
                ['projection' => ['json_content' => 1]]
        );
        foreach ($cursor as $singolo) {
            $split = explode('"time":', $singolo['json_content']);
            if (isset($split[1])) {
                $split = explode(",", $split[1]);
                $split = explode(".", $split[0]);
                $tempoLezione = (int) $split[0];
            }
        }
        return $tempoLezione;
    }

    public function getLastProgress($userid, $timecreated, $objectid, $db) {
        $collection = $this->conn->$db->mdl_logstore_standard_log;
        $cursor = $collection->find(
                ['objectid' => (int) $objectid, 'userid' => (int) $userid, 'component' => "mod_hvp",
                    "action" => "progress", "timecreated" => ['$lt' => (int) $timecreated]],
                ['projection' => ['other' => 1],
                    'sort' => ['_id' => -1],
                    'limit' => 1]
        );
        $tempo_cumulato = 0;
        foreach ($cursor as $singolo) {
            $progress = $singolo['other'];
            $toReplace = ['{', '}', '"'];
            $progress = str_replace($toReplace, "", $progress);
            if ($progress != "") {
                $progress = explode(".", $progress);
                $progress = explode(":", $progress[0]);
                @$tempo_cumulato = (int) @$progress[1];
            } else
                $tempo_cumulato = 0;
        }

        return $tempo_cumulato;
    }

}
