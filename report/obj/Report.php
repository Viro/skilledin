<?php

$abs_path = '/home/admin/public_html';
if(isset($_SERVER['DOCUMENT_ROOT']) && strlen(trim($_SERVER['DOCUMENT_ROOT']))) $abs_path = $_SERVER['DOCUMENT_ROOT'];

if(is_file($abs_path . '/vendor/autoload.php')){
    require($abs_path . '/vendor/autoload.php');
}else{
    if(is_file('../../vendor/autoload.php')) require '../../vendor/autoload.php';
}
//function compareByTimeStamp($time1, $time2) {
//    $time1 = str_replace("/", "-", $time1);
//    $time2 = str_replace("/", "-", $time2);
//
//    return strtotime($time1) - strtotime($time2);
//}

class Report {

    private $conn = null;

    public function __construct() {
        $this->conn = new MongoDB\Client(
            "mongodb+srv://jobtek:TimeVision2021%40%21%3F@timevision.t9ed5.mongodb.net/TimeVision?retryWrites=true&w=majority");
    }

    public function getNomeCorso($id, $db) {
        $error = "Corso non trovato";
        $collection = $this->conn->$db->mdl_course;
        $cursor = $collection->find(
            ['id' => (int) $id],
            ['projection' => ['fullname' => 1]]
        );
        foreach ($cursor as $singolo) {
            return $singolo->fullname;
        }
        return $error;
    }

    public function getCfUtenti($db, $user_id) {
        $cf = '';
        $collection = $this->conn->$db->mdl_user_info_data;
        $user_id = trim($user_id);

        $cursor = $collection->find(
            ['userid' => (int) $user_id, 'fieldid' => 17],
            ['projection' =>
                [
                    'data' => 1
                ]
            ]
        );

        foreach ($cursor as $singolo) {
            $cf = $singolo['data'];
        }

        return $cf;
    }

    public function getCfUtentiMdlUser($db, $user_id) {
        $cf = '';
        $collection = $this->conn->$db->mdl_user;
        $user_id = trim($user_id);

        $cursor = $collection->find(
            ['id' => (int) $user_id,],
            ['projection' =>
                [
                    'idnumber' => 1
                ]
            ]
        );

        foreach ($cursor as $singolo) {
            $cf = $singolo['idnumber'];
        }

        return $cf;
    }

    public function getIdUtentiFromAzienda($db, $gruppo) {
        $collection = $this->conn->$db->mdl_user_info_data;
        $gruppo = trim($gruppo);

        $cursor = $collection->find(
            ['data' => $gruppo],
            ['projection' =>
                [
                    'userid' => 1
                ]
            ]
        );

//        var_dump($gruppo); exit;

        $tmp = array();
        $array_id = array();
        foreach ($cursor as $singolo) {
            $tmp = $singolo['userid'];
            array_push($array_id, $tmp);
        }

        return $array_id;
    }

    public function getAggregateZoom($db, $corso, $inizio, $fine, $gruppo) {
        $collection = $this->conn->$db->mdl_zoom_meeting_participants_aggregate_new;
        if ($corso == "Tutti")
            $match = ['$match' => [
                'join_time' => [
                    '$gte' => $inizio,
                    '$lte' => $fine
                ],
            ]];
        else
            $match = ['$match' => [
                'join_time' => [
                    '$gte' => $inizio,
                    '$lte' => $fine
                ],
                'nomeCorso' => $corso
            ]];

        if ($gruppo != "Tutti") {
            $match2 = ['$match' => [
                'azienda' => $gruppo
            ]];
        } else {

            $match2 = ['$match' => [
                'azienda' => [
                    '$ne' => $gruppo
                ]
            ]];
        }

        $addFields = ['$addFields' => [
            'unique_value' => [
                '$concat' => [ ['$toString' => '$userid'], '-', ['$toString' => '$join_time'], '-', ['$toString' => '$leave_time'] ]
            ]
        ]
        ];

        $pipeline = [$match, $addFields,
            ['$lookup' => [
                'from' => 'mdl_utenti_complete',
                'localField' => 'userid',
                'foreignField' => 'userid',
                'as' => 'info'
            ]], ['$set' => [
                'nome' => [
                    '$arrayElemAt' => [
                        '$info.nome', 0
                    ]
                ],
                'username' => [
                    '$arrayElemAt' => [
                        '$info.username',
                        0
                    ]
                ],
                'cf' => [
                    '$arrayElemAt' => [
                        '$info.idnumber',
                        0
                    ]
                ],
                'email' => [
                    '$arrayElemAt' => [
                        '$info.email',
                        0
                    ]
                ],
                'azienda' => [
                    '$arrayElemAt' => [
                        '$info.azienda',
                        0
                    ]
                ],
                'ruolo' => [
                    '$arrayElemAt' => [
                        '$info.ruolo',
                        0
                    ]
                ],
                'citta' => [
                    '$arrayElemAt' => [
                        '$info.citta',
                        0
                    ]
                ],
                'data' => [
                    '$arrayElemAt' => [
                        '$info.data',
                        0
                    ]
                ],
                'phone2' => [
                    '$arrayElemAt' => [
                        '$info.phone2',
                        0
                    ]
                ],
                'phone1' => [
                    '$arrayElemAt' => [
                        '$info.phone1',
                        0
                    ]
                ],
            ]], ['$project' => [
                'info' => 0,
                'uuid' => 0,
                'zoomuserid' => 0,
                'user_email' => 0
            ]], $match2
        ];

//        var_dump($pipeline); exit;

//        echo json_encode($pipeline); exit;

        $out = $collection->aggregate($pipeline);

        $res = $out->toArray();
        return $res;
    }

    public function constructZoomReport($res) {
        $finale = array();

        $arrControl = array();
        foreach ($res as $singolo) {

            if (!array_key_exists($singolo['unique_value'], $finale)) {
                if (!isset($singolo['azienda']))
                    continue;
                $ruolo = "";
                if (isset($singolo['ruolo'])) {
                    switch ($singolo['ruolo']) {
                        case 'student':
                            $ruolo = "studente";
                            break;
                        case 'manager':
                            $ruolo = "manager";
                            break;
                        case 'editingteacher':
                            $ruolo = "docente";
                            break;
                        case 'teacher':
                            $ruolo = "controller";
                            break;
                        default:
                            break;
                    }
                }
                $singolo['ruolo'] = $ruolo;
                $singolo['timecreated'] = date("d-m-Y", $singolo['join_time']);
                $singolo['timestamp'] = $singolo['join_time'];
                $singolo['orario'] = date("H:i:s", $singolo['join_time']);
                $singolo['orario_logout'] = date("H:i:s", $singolo['leave_time']);
                $singolo['tempo_cumulato'] = $this->convertSeconds($singolo['duration']);
                if (!isset($singolo['citta']))
                    $singolo['citta'] = "n/d";
                if (!isset($singolo['cf']))
                    $singolo['cf'] = "n/d";
                if (!isset($singolo['data']))
                    $singolo['data_nascita'] = "n/d";
                else
                    $singolo['data_nascita'] = date("d/m/Y", $singolo['data']);
                $singolo['phone'] = "n/d";
                if (isset($singolo['phone1']) && $singolo['phone1'] != "")
                    $singolo['phone'] = $singolo['phone1'];
                if (isset($singolo['phone1']) && $singolo['phone2'] != "")
                    $singolo['phone'] = $singolo['phone2'];


                $arrControl[$singolo['unique_value']] = $singolo;
//                array_push($arrControl[$singolo['unique_value']], $singolo);
            }
        }

        foreach($arrControl as $control){
            array_push($finale, $control);
        }

        $new = array();
        foreach ($finale as $singolo) {
            $singolo = json_decode(json_encode($singolo, true), true);
            if (isset($singolo))
                array_push($new, $singolo);
        }

//        foreach($new as $n){
//            if($n['userid'] == 44){
//                var_dump($n);
//            }
//        }
//
//        exit();

//        var_dump($new); exit;


//        var_dump($singolo); exit;
//
//        $new2 = array();
//
//        $dataUserValue = array_column($new, 'userid');
//        $dataUserJoin = array_column($new, 'join_time');
//        $dataUserLeave = array_column($new, 'leave_time');
//        $dataUserIdCorso = array_column($new, 'idCorso');
//
////        var_dump($dataUserValue);
////        var_dump(array_count_values('43', $dataUserValue)); exit;
//
//        $counts = array_count_values(
//            array_column($new, 'userid')
//        );
//
//        $final = array_filter($counts, function($a) {
//            return $a >= 2;
//        });
//
//        var_dump($final); exit;
//
//        foreach($new as $key => $n){
//
//            var_dump(count(in_array($n['userid'], $dataUserValue)));
//            exit;
//
//
//            if (count(in_array($n['userid'], $dataUserValue)) > 1 && count(in_array($n['join_time'], $dataUserJoin)) > 1 && count(in_array($n['leave_time'], $dataUserLeave)) > 1 && count(in_array($n['idCorso'], $dataUserIdCorso)) > 1) {
////                array_push($new2, $n);
//                unset($new[$key]);
//            }
//        }
//
//        var_dump($new);
//
//        exit;

        return $new;
    }

    public function createCopertina($userid, $spreadsheet, $j) {
        $txt1 = "";
        $txt2 = "";
        $src = "";
        $collection = $this->conn->admin->settings_excel_copertina;
        $cursor = $collection->find(
            ['userid' => (int) $userid],
            ['projection' => ['txt1' => 1, 'txt2' => 1, 'img' => 1],
                'limit' => 1,
                'sort' => ['_id' => -1],
            ]
        );

        foreach ($cursor as $singolo) {
            $txt1 = $singolo['txt1'];
            $txt2 = $singolo['txt2'];
            $src = $singolo['img'];
        }
        if ($src != "") {
            $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing->setName('S1');
            $drawing->setDescription('Intestazione');
            $drawing->setPath('img/' . $src);
            $drawing->setCoordinates('A1');
            $drawing->setHeight(100);
            $drawing->setWidth(800);
            $drawing->setWorksheet($spreadsheet->getActiveSheet());
        }
        if ($txt1 != "") {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue("A13", $txt1);
        }
        if ($txt2 != "") {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue("A19", $txt2);
        }
        $spreadsheet->setActiveSheetIndex($j)->setCellValue("H17", "REGISTRO ATTIVITA' FORMATIVE");
        $spreadsheet->getActiveSheet()->getStyle("H17")->getFont()->setBold(true);
        $spreadsheet->getActiveSheet()->getStyle("H17")->getFont()->setSize(18);

        $spreadsheet->getActiveSheet()->setTitle("Copertina");
        $spreadsheet->createSheet();
    }

    public function createSheet($userid, $spreadsheet, $j) {
        $s1 = "";
        $t1 = "";
        $collection = $this->conn->admin->settings_excel_sheet;
        $cursor = $collection->find(
            ['userid' => (int) $userid],
            ['projection' => ['s1' => 1, 't1' => 1],
                'limit' => 1,
                'sort' => ['_id' => -1],
            ]
        );
        foreach ($cursor as $singolo) {
            $s1 = $singolo['s1'];
            $t1 = $singolo['t1'];
        }
        $index = 1;
        if ($s1 != "") {
            $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing->setName('S1');
            $drawing->setDescription('Intestazione Sheet');
            $drawing->setPath('img/' . $s1);
            $drawing->setCoordinates('A1');
            $drawing->setHeight(100);
            $drawing->setWidth(800);
            $drawing->setWorksheet($spreadsheet->getActiveSheet());
            $index = 16;
        }
        if ($t1 != "") {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue("A13", $t1);
            $index = 16;
        }
        return $index;
    }

    public function setZoomSheet($index, $spreadsheet, $letter, $filtri, $j, $new) {
        //INIZIO RE1-T2 Inserimento titolo del corso in intestazione (setZoomSheet)
        $course_filtered = false;
        if(isset($new[0]['corso']) && $course_filtered){
            //Imposto il titolo
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Corso seguito : " . $new[0]['corso']);

            //Unisco le celle
            $all_letters = range('A', 'Z');
            $merge_to_index = 16+$letter-count($filtri);
            if($merge_to_index >= count($all_letters)) $merge_to_index = count($all_letters)-1;
            $sheet = $spreadsheet->getActiveSheet();
            $sheet->MergeCells($letter . $index . ':' . $all_letters[$merge_to_index] . $index);
            $index += 2;
        }
        //FINE RE1-T2

        if (!in_array("data", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Data");
            $letter++;
        }
        if (!in_array("userid", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Userid");
            $letter++;
        }
        if (!in_array("username", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Username");
            $letter++;
        }
        if (!in_array("name", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Nome Cognome");
            $letter++;
        }
        if (!in_array("cf", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Codice Fiscale");
            $letter++;
        }
        if (!in_array("email", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Email");
            $letter++;
        }
        if (!in_array("dn", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Data di nascita");
            $letter++;
        }
        if (!in_array("ln", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Luogo di nascita");
            $letter++;
        }
        if (!in_array("azienda", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Azienda");
            $letter++;
        }
        if (!in_array("phone", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "N.di telefono");
            $letter++;
        }
        if (!in_array("ruolo", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Ruolo");
            $letter++;
        }
        if (!in_array("corso", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Corso");
            $letter++;
        }
        if (!in_array("aula", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Nome aula");
            $letter++;
        }
        if (!in_array("orarioA", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Ora ingresso");
            $letter++;
        }
        if (!in_array("orarioL", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Ora uscita");
            $letter++;
        }
        if (!in_array("tc", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Tempo cumulato");
        }

        $spreadsheet->getActiveSheet()->setTitle("Report");


        $from = "A1";
        $to = $letter . $index;
        $spreadsheet->getActiveSheet()->getStyle("$from:$to")->getFont()->setBold(true);

        $i = $index + 1;

        foreach ($new as $singolo) {
            $letter = 'A';
            if (!in_array("data", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['timecreated']);
                $letter++;
            }
            if (!in_array("userid", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['userid']);
                $letter++;
            }

            if (!in_array("username", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['username']);
                $letter++;
            }

            if (!in_array("name", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['nome']);
                $letter++;
            }

            if (!in_array("cf", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, @$singolo['cf']);
                $letter++;
            }

            if (!in_array("email", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["email"]);
                $letter++;
            }

            if (!in_array("dn", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, @$singolo["data_nascita"]);
                $letter++;
            }

            if (!in_array("ln", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, @$singolo["citta"]);
                $letter++;
            }

            if (!in_array("azienda", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, @$singolo["azienda"]);
                $letter++;
            }

            if (!in_array("phone", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['phone']);
                $letter++;
            }

            if (!in_array("ruolo", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["ruolo"]);
                $letter++;
            }
            if (!in_array("corso", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["nomeCorso"]);
                $letter++;
            }
            if (!in_array("aula", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["name"]);
                $letter++;
            }
            if (!in_array("orarioA", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["orario"]);
                $letter++;
            }
            if (!in_array("orarioL", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["orario_logout"]);
                $letter++;
            }
            if (!in_array("tc", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["tempo_cumulato"]);
            }
            $i++;
        }
        foreach (range('A', $letter) as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        $spreadsheet->getActiveSheet()->getStyle("A" . $index . ":" . $letter . $index)
            ->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('00FF7F');

        return $i;
    }

    public function addSettingsExcel($userid, $spreadsheet, $i) {
        $s2 = "";
        $s3 = "";
        $collection = $this->conn->admin->settings_excel_sheet;
        $cursor = $collection->find(
            ['userid' => (int) $userid],
            ['projection' => ['s2' => 1, 'timbro' => 1],
                'limit' => 1,
                'sort' => ['_id' => -1],
            ]
        );
        foreach ($cursor as $singolo) {
            $s2 = $singolo['s2'];
            $s3 = $singolo['timbro'];
        }
        if ($s2 != "") {
            $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing->setName('S2');
            $drawing->setDescription('S2 bordo sinistro');
            $drawing->setPath('img/' . $s2);
            $coordinate = $i + 3;
            $drawing->setCoordinates('A' . $coordinate);
            $drawing->setHeight(100);
            $drawing->setWidth(100);
            $drawing->setWorksheet($spreadsheet->getActiveSheet());
        }
        if ($s3 != "") {
            $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing->setName('S3');
            $drawing->setDescription('S3 timbro');
            $drawing->setPath('img/' . $s3);
            $coordinate = $i + 3;
            $drawing->setCoordinates('L' . $coordinate);
            $drawing->setHeight(100);
            $drawing->setWidth(100);
            $drawing->setWorksheet($spreadsheet->getActiveSheet());
        }
    }

    public function getAggregateZoomSintesi($db, $inizio, $fine, $gruppo) {

//        var_dump($inizio);
//        var_dump($fine);
//        exit;

        $collection = $this->conn->$db->mdl_zoom_meeting_participants_aggregate_new;
        $match = ['$match' => [
            'join_time' => [
                '$gte' => $inizio,
                '$lte' => $fine
            ]
        ]];
        $addFields = ['$addFields' => [
            'unique_value' => [
                '$concat' => [ ['$toString' => '$userid'], '-', ['$toString' => '$join_time'], '-', ['$toString' => '$leave_time'] ]
            ]
        ]
        ];
        $sort = ['$sort' => [
            'join_time' => 1
        ]];
        $pipeline = [$match, $sort, $addFields,
            ['$lookup' => [
                'from' => 'mdl_utenti_complete',
                'localField' => 'userid',
                'foreignField' => 'userid',
                'as' => 'info'
            ]], ['$set' => [
                'nome' => [
                    '$arrayElemAt' => [
                        '$info.nome', 0
                    ]
                ],
                'username' => [
                    '$arrayElemAt' => [
                        '$info.username',
                        0
                    ]
                ],
                'cf' => [
                    '$arrayElemAt' => [
                        '$info.idnumber',
                        0
                    ]
                ],
                'email' => [
                    '$arrayElemAt' => [
                        '$info.email',
                        0
                    ]
                ],
                'azienda' => [
                    '$arrayElemAt' => [
                        '$info.azienda',
                        0
                    ]
                ],
                'ruolo' => [
                    '$arrayElemAt' => [
                        '$info.ruolo',
                        0
                    ]
                ],
                'phone2' => [
                    '$arrayElemAt' => [
                        '$info.phone2',
                        0
                    ]
                ],
                'phone1' => [
                    '$arrayElemAt' => [
                        '$info.phone1',
                        0
                    ]
                ],
                'citta' => [
                    '$arrayElemAt' => [
                        '$info.citta',
                        0
                    ]
                ],
                'data' => [
                    '$arrayElemAt' => [
                        '$info.data',
                        0
                    ]
                ]
            ]], ['$project' => [
                'info' => 0,
                'uuid' => 0,
                'zoomuserid' => 0,
                'user_email' => 0
            ]], ['$match' => [
                'azienda' => $gruppo
            ]]
        ];

//        var_dump(json_encode($pipeline)); exit;

        $out = $collection->aggregate($pipeline);
        $result = $out->toArray();
//        var_dump($result); exit;
        return $result;
    }

    public function constructMinCumulatiReport($array, $totResult) {
        $array = $this->sortArray($array);
        $prov = null;
        $mesi = array();
        $new = array();
        $array_mesi = array();
        $cumulatoTot = 0;
        $tempo_tot = 0;
        $cont = 0;



        foreach ($array as $singolo) {

            if ($prov == null && $totResult > 1) {
                $prov = $singolo;
                $cont++;
                continue;
            }

            if ($totResult == 1) {
                $data_singolo = str_replace("/", "", $singolo['timecreated']);
                $timesingolo_split = explode(":", $singolo['tempo_cumulato']);
                $seconds_singolo = $timesingolo_split[0] * 3600 + $timesingolo_split[1] * 60 + $timesingolo_split[2];
                $mesi[$singolo['timecreated']] = $singolo['tempo_cumulato'];
                if (!in_array($singolo['timecreated'], $array_mesi))
                    array_push($array_mesi, $singolo['timecreated']);
                $tempo = explode(":", $singolo['tempo_cumulato']);
                $secondi = $tempo[0] * 3600 + $tempo[1] * 60 + $tempo[2];
                $tempo_tot = $secondi;
                $singolo['mesi'] = $mesi;
                $singolo['tempo_tot'] = $this->convertSeconds($tempo_tot);
                array_push($new, $singolo);
                break;
            }

            $cont++;
            $data_prov = str_replace("/", "", $prov['timecreated']);
            $data_singolo = str_replace("/", "", $singolo['timecreated']);
            $timeprov_split = explode(":", $prov['tempo_cumulato']);
            $timesingolo_split = explode(":", $singolo['tempo_cumulato']);
            $seconds_prov = $timeprov_split[0] * 3600 + $timeprov_split[1] * 60 + $timeprov_split[2];
            $seconds_singolo = $timesingolo_split[0] * 3600 + $timesingolo_split[1] * 60 + $timesingolo_split[2];
            if ($prov['userid'] == $singolo['userid']) {
                if ($data_prov == $data_singolo) {

                    $cumulatoTot = $seconds_prov + $seconds_singolo;
                    if (!in_array($prov['timecreated'], $array_mesi))
                        array_push($array_mesi, $prov['timecreated']);

                    $mesi[$prov['timecreated']] = $this->convertSeconds($cumulatoTot);

                    $prov = $singolo;
                    $prov['mesi'] = $mesi;
                    $prov['tempo_cumulato'] = $this->convertSeconds($cumulatoTot);
                } else {

                    $mesi[$prov['timecreated']] = $prov['tempo_cumulato'];
                    $mesi[$singolo['timecreated']] = $singolo['tempo_cumulato'];
                    if (!in_array($prov['timecreated'], $array_mesi))
                        array_push($array_mesi, $prov['timecreated']);
                    if (!in_array($singolo['timecreated'], $array_mesi))
                        array_push($array_mesi, $singolo['timecreated']);

                    $prov = $singolo;
                    $prov['mesi'] = $mesi;
                }
            } else {
                if (isset($prov['mesi'])) {
                    foreach ($prov['mesi'] as $tempo) {
                        $tempo = explode(":", $tempo);
                        $secondi = $tempo[0] * 3600 + $tempo[1] * 60 + $tempo[2];
                        $tempo_tot += $secondi;
                    }
                } else {
                    $mesi[$prov['timecreated']] = $prov['tempo_cumulato'];
                    $prov['mesi'] = $mesi;
                    if (!in_array($prov['timecreated'], $array_mesi))
                        array_push($array_mesi, $prov['timecreated']);
                    $tempo = explode(":", $prov['tempo_cumulato']);
                    $secondi = $tempo[0] * 3600 + $tempo[1] * 60 + $tempo[2];
                    $tempo_tot = $secondi;
                }

                $prov['tempo_tot'] = $this->convertSeconds($tempo_tot);
                array_push($new, $prov);
                $tempo_tot = 0;
                $prov = $singolo;
                $mesi = null;
            }

            if ($cont == $totResult) {
                if (isset($prov['mesi'])) {
                    foreach ($prov['mesi'] as $tempo) {
                        $tempo = explode(":", $tempo);
                        $secondi = $tempo[0] * 3600 + $tempo[1] * 60 + $tempo[2];
                        $tempo_tot += $secondi;
                    }
                } else {
                    $mesi[$prov['timecreated']] = $prov['tempo_cumulato'];
                    $prov['mesi'] = $mesi;
                    if (!in_array($prov['timecreated'], $array_mesi))
                        array_push($array_mesi, $prov['timecreated']);
                    $tempo = explode(":", $prov['tempo_cumulato']);
                    $secondi = $tempo[0] * 3600 + $tempo[1] * 60 + $tempo[2];
                    $tempo_tot = $secondi;
                }
                $prov['tempo_tot'] = $this->convertSeconds($tempo_tot);
                array_push($new, $prov);
            }
        }
        usort($array_mesi, "compareByTimeStamp");

        foreach ($new as &$singolo) {
            foreach ($array_mesi as $mese) {
                if (!in_array($mese, array_keys($singolo['mesi']))) {
                    $singolo['mesi'][$mese] = "00:00:00";
                }
            }
            uksort($singolo['mesi'], "compareByTimeStamp");
        }
        $return = json_encode($new) . "*" . json_encode($array_mesi);

        return $return;
    }

    public function setZoomSintesiSheet($index, $spreadsheet, $letter, $filtri, $j, $new, $array_mesi) {
        //INIZIO RE1-T2 Inserimento titolo del corso in intestazione (setZoomSintesiSheet)
        $course_filtered = false;
        if(isset($new[0]['corso']) && $course_filtered){
            //Imposto il titolo
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Corso seguito : " . $new[0]['corso']);

            //Unisco le celle
            $all_letters = range('A', 'Z');
            $merge_to_index = 13+$letter+count($array_mesi)-count($filtri);
            if($merge_to_index >= count($all_letters)) $merge_to_index = count($all_letters)-1;
            $sheet = $spreadsheet->getActiveSheet();
            $sheet->MergeCells($letter . $index . ':' . $all_letters[$merge_to_index] . $index);
            $index += 2;
        }
        //FINE RE1-T2

        if (!in_array("userid", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Userid");
            $letter++;
        }
        if (!in_array("username", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Username");
            $letter++;
        }
        if (!in_array("name", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Nome Cognome");
            $letter++;
        }
        if (!in_array("cf", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Codice Fiscale");
            $letter++;
        }
        if (!in_array("email", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Email");
            $letter++;
        }
        if (!in_array("dn", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Data di Nascita");
            $letter++;
        }
        if (!in_array("ln", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Luogo di Nascita");
            $letter++;
        }
        if (!in_array("azienda", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Azienda");
            $letter++;
        }
        if (!in_array("phone", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "N.di telefono");
            $letter++;
        }
        if (!in_array("ruolo", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Ruolo");
            $letter++;
        }
        if (!in_array("corso", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Corso");
            $letter++;
        }
        if (!in_array("idCorso", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Id Corso");
            $letter++;
        }
        if (!in_array("tc", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue("$letter" . $index, "Tempo cumulato");
            $letter++;
        }

        $column = $letter;
        $giro = 0;

        foreach ($array_mesi as $mese) {
            if (!in_array("data_" . $giro, $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($column . $index, $mese);
                $column++;
            }
            $giro++;
        }


        $spreadsheet->getActiveSheet()->setTitle("Report");


        $from = "A1";
        $to = "$column" . $index;
        $spreadsheet->getActiveSheet()->getStyle("$from:$to")->getFont()->setBold(true);

        $i = $index + 1;
        foreach ($new as $singolo) {
            $letter = 'A';

            if (!in_array("userid", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['userid']);
                $letter++;
            }
            if (!in_array("username", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['username']);
                $letter++;
            }
            if (!in_array("name", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['nome']);
                $letter++;
            }
            if (!in_array("cf", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['cf']);
                $letter++;
            }
            if (!in_array("email", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['email']);
                $letter++;
            }

            if (!in_array("dn", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['data_nascita']);
                $letter++;
            }

            if (!in_array("ln", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['citta']);
                $letter++;
            }

            if (!in_array("azienda", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['azienda']);
                $letter++;
            }
            if (!in_array("phone", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['phone']);
                $letter++;
            }
            if (!in_array("ruolo", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['ruolo']);
                $letter++;
            }
            if (!in_array("corso", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['nomeCorso']);
                $letter++;
            }
            if (!in_array("idCorso", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['idCorso']);
                $letter++;
            }
            if (!in_array("tc", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue("$letter$i", $singolo["tempo_tot"]);
                $letter++;
            }


            $column = $letter;
            $giro = 0;
            foreach ($singolo['mesi'] as $value) {
                if (!in_array("data_" . $giro, $filtri)) {
                    $spreadsheet->setActiveSheetIndex($j)->setCellValue("$column$i", $value);
                    $column++;
                }
                $giro++;
            }


            $i++;
        }

        foreach (range('A', "$column") as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        $spreadsheet->getActiveSheet()->getStyle("A" . $index . ":" . $column . $index)
            ->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('00FF7F');

        return $i;
    }

    public function compareByTimeStamp($time1, $time2) {
        $time1 = str_replace("/", "-", $time1);
        $time2 = str_replace("/", "-", $time2);

        return strtotime($time1) - strtotime($time2);
    }

    public function getAggregateZoomSintesiName($db, $nomeCorso, $inizio, $fine) {
        $collection = $this->conn->$db->mdl_zoom_meeting_participants_aggregate;
        $match = ['$match' => [
            'join_time' => [
                '$gte' => $inizio,
                '$lte' => $fine
            ],
            'nomeCorso' => $nomeCorso
        ]];
        $pipeline = [$match,
            ['$lookup' => [
                'from' => 'mdl_utenti_complete',
                'localField' => 'userid',
                'foreignField' => 'userid',
                'as' => 'info'
            ]], ['$set' => [
                'nome' => [
                    '$arrayElemAt' => [
                        '$info.nome', 0
                    ]
                ],
                'username' => [
                    '$arrayElemAt' => [
                        '$info.username',
                        0
                    ]
                ],
                'cf' => [
                    '$arrayElemAt' => [
                        '$info.idnumber',
                        0
                    ]
                ],
                'email' => [
                    '$arrayElemAt' => [
                        '$info.email',
                        0
                    ]
                ],
                'azienda' => [
                    '$arrayElemAt' => [
                        '$info.azienda',
                        0
                    ]
                ],
                'ruolo' => [
                    '$arrayElemAt' => [
                        '$info.ruolo',
                        0
                    ]
                ],
                'phone2' => [
                    '$arrayElemAt' => [
                        '$info.phone2',
                        0
                    ]
                ],
                'phone1' => [
                    '$arrayElemAt' => [
                        '$info.phone1',
                        0
                    ]
                ],
                'citta' => [
                    '$arrayElemAt' => [
                        '$info.citta',
                        0
                    ]
                ],
                'data' => [
                    '$arrayElemAt' => [
                        '$info.data',
                        0
                    ]
                ]
            ]], ['$project' => [
                'info' => 0,
                'uuid' => 0,
                'zoomuserid' => 0,
                'user_email' => 0
            ]]
        ];
        $out = $collection->aggregate($pipeline);
        $result = $out->toArray();
//        var_dump($result); exit;
        return $result;
    }

    public function getLogstoreSintesi($db, $inizio, $fine, $idUtente = null, $array_id = null, $idCorso = null) {
        $collection = $this->conn->$db->mdl_logstore_standard_log;

        if (!isset($idUtente)) {
            if (isset($idCorso) && isset($array_id)) {
                if ($idCorso == 0) {
                    $match = ['$match' => [
                        'userid' => [
                            '$in' => $array_id
                        ],
                        'action' => [
                            '$in' => ['loggedin', 'loggedout']
                        ],
                        'timecreated' => [
                            '$gte' => $inizio,
                            '$lte' => $fine
                        ]
                    ]];
                } else if (count($array_id) == 0) {
                    $match = ['$match' => [
                        'courseid' => (int) $idCorso,
                        'action' => [
                            '$in' => ['loggedin', 'loggedout']
                        ],
                        'timecreated' => [
                            '$gte' => $inizio,
                            '$lte' => $fine
                        ]
                    ]];
                } else {
                    $match = ['$match' => [
                        'userid' => [
                            '$in' => $array_id
                        ],
                        'action' => [
                            '$in' => ['loggedin', 'loggedout']
                        ],
                        'courseid' => (int) $idCorso,
                        'timecreated' => [
                            '$gte' => $inizio,
                            '$lte' => $fine
                        ]
                    ]];
                }
            } else if (!isset($idCorso)) {
                $match = ['$match' => [
                    'userid' => [
                        '$in' => $array_id
                    ],
                    'action' => [
                        '$in' => ['loggedin', 'loggedout']
                    ],
                    'timecreated' => [
                        '$gte' => $inizio,
                        '$lte' => $fine
                    ]
                ]];
            }
        } else {
            $match = ['$match' => [
                'userid' => (int) $idUtente,
                'action' => [
                    '$in' => ['loggedin', 'loggedout']
                ],
                'timecreated' => [
                    '$gte' => $inizio,
                    '$lte' => $fine
                ]
            ]];
        }

//        var_dump($match);exit;
        $pipeline = [$match,
            ['$lookup' => [
                'from' => 'mdl_utenti_complete',
                'localField' => 'userid',
                'foreignField' => 'userid',
                'as' => 'info'
            ]], ['$set' => [
                'username' => [
                    '$arrayElemAt' => [
                        '$info.username',
                        0
                    ]
                ],
                'email' => [
                    '$arrayElemAt' => [
                        '$info.email',
                        0
                    ]
                ],
                'cf' => [
                    '$arrayElemAt' => [
                        '$info.idnumber',
                        0
                    ]
                ],
                'nome' => [
                    '$arrayElemAt' => [
                        '$info.nome',
                        0
                    ]
                ],
                'citta' => [
                    '$arrayElemAt' => [
                        '$info.citta',
                        0
                    ]
                ],
                'data' => [
                    '$arrayElemAt' => [
                        '$info.data',
                        0
                    ]
                ],
                'azienda' => [
                    '$arrayElemAt' => [
                        '$info.azienda',
                        0
                    ]
                ]
                ,
                'phone2' => [
                    '$arrayElemAt' => [
                        '$info.phone2',
                        0
                    ]
                ],
                'phone1' => [
                    '$arrayElemAt' => [
                        '$info.phone1',
                        0
                    ]
                ],
            ]], ['$project' => [
                'info' => 0
            ]],
            ['$sort' => [
                'userid' => 1,
                'timecreated' => 1
            ]]
        ];

//        ['projection' => ['other' => 1],
//            'sort' => ['_id' => -1],
//            'limit' => 1]

//        var_dump(json_encode($pipeline)) ;exit();
        $out = $collection->aggregate($pipeline);
//        var_dump($out->toArray()); exit;
        return $out->toArray();
    }

    public function getLogstore($db, $inizio, $fine, $idUtente = null, $array_id = null, $idCorso = null) {
        $collection = $this->conn->$db->mdl_logstore_standard_log;

        if (!isset($idUtente)) {
            if (isset($idCorso) && isset($array_id)) {
                if ($idCorso == 0) {
                    $match = ['$match' => [
                        'userid' => [
                            '$in' => $array_id
                        ],
                        'timecreated' => [
                            '$gte' => $inizio,
                            '$lte' => $fine
                        ]
                    ]];
                } else if (count($array_id) == 0) {
                    $match = ['$match' => [
                        'courseid' => (int) $idCorso,
                        'timecreated' => [
                            '$gte' => $inizio,
                            '$lte' => $fine
                        ]
                    ]];
                } else {
                    $match = ['$match' => [
                        'userid' => [
                            '$in' => $array_id
                        ],
                        'courseid' => (int) $idCorso,
                        'timecreated' => [
                            '$gte' => $inizio,
                            '$lte' => $fine
                        ]
                    ]];
                }
            } else if (!isset($idCorso)) {
                $match = ['$match' => [
                    'userid' => [
                        '$in' => $array_id
                    ],
                    'timecreated' => [
                        '$gte' => $inizio,
                        '$lte' => $fine
                    ]
                ]];
            }
        } else {
            $match = ['$match' => [
                'userid' => (int) $idUtente,
                'timecreated' => [
                    '$gte' => $inizio,
                    '$lte' => $fine
                ]
            ]];
        }

//        var_dump($match);exit;
        $pipeline = [$match,
            ['$lookup' => [
                'from' => 'mdl_utenti_complete',
                'localField' => 'userid',
                'foreignField' => 'userid',
                'as' => 'info'
            ]], ['$set' => [
                'username' => [
                    '$arrayElemAt' => [
                        '$info.username',
                        0
                    ]
                ],
                'email' => [
                    '$arrayElemAt' => [
                        '$info.email',
                        0
                    ]
                ],
                'cf' => [
                    '$arrayElemAt' => [
                        '$info.idnumber',
                        0
                    ]
                ],
                'nome' => [
                    '$arrayElemAt' => [
                        '$info.nome',
                        0
                    ]
                ],
                'citta' => [
                    '$arrayElemAt' => [
                        '$info.citta',
                        0
                    ]
                ],
                'data' => [
                    '$arrayElemAt' => [
                        '$info.data',
                        0
                    ]
                ],
                'azienda' => [
                    '$arrayElemAt' => [
                        '$info.azienda',
                        0
                    ]
                ]
                ,
                'phone2' => [
                    '$arrayElemAt' => [
                        '$info.phone2',
                        0
                    ]
                ],
                'phone1' => [
                    '$arrayElemAt' => [
                        '$info.phone1',
                        0
                    ]
                ],
            ]], ['$project' => [
                'info' => 0
            ]]];
        //var_dump($pipeline) ;exit();
        $out = $collection->aggregate($pipeline);
//        var_dump($out->toArray()); exit;
        return $out->toArray();
    }

    public function getLogstoreNew($db, $inizio, $fine, $idUtente = null, $array_id = null, $idCorso = null) {
        $collection = $this->conn->$db->mdl_logstore_standard_log;

        $cursoreLog= $collection->distinct("userid", ['courseid' => (int) $idCorso]);

        foreach ($cursoreLog as $user_id) {
            $array_id[] = $user_id;
        }

        $match = ['$match' => [
            'userid' => [
                '$in' => $array_id
            ],
            'timecreated' => [
                '$gte' => $inizio,
                '$lte' => $fine
            ]
        ]];

        $pipeline = [$match,
            ['$lookup' => [
                'from' => 'mdl_utenti_complete',
                'localField' => 'userid',
                'foreignField' => 'userid',
                'as' => 'info'
            ]], ['$set' => [
                'username' => [
                    '$arrayElemAt' => [
                        '$info.username',
                        0
                    ]
                ],
                'email' => [
                    '$arrayElemAt' => [
                        '$info.email',
                        0
                    ]
                ],
                'cf' => [
                    '$arrayElemAt' => [
                        '$info.idnumber',
                        0
                    ]
                ],
                'nome' => [
                    '$arrayElemAt' => [
                        '$info.nome',
                        0
                    ]
                ],
                'citta' => [
                    '$arrayElemAt' => [
                        '$info.citta',
                        0
                    ]
                ],
                'data' => [
                    '$arrayElemAt' => [
                        '$info.data',
                        0
                    ]
                ],
                'azienda' => [
                    '$arrayElemAt' => [
                        '$info.azienda',
                        0
                    ]
                ]
                ,
                'phone2' => [
                    '$arrayElemAt' => [
                        '$info.phone2',
                        0
                    ]
                ],
                'phone1' => [
                    '$arrayElemAt' => [
                        '$info.phone1',
                        0
                    ]
                ],
            ]], ['$project' => [
                'info' => 0
            ]]];
        //var_dump($pipeline) ;exit();
        $out = $collection->aggregate($pipeline);

        return $out->toArray();
    }

    public function getLogstoreAll($db, $idUtente = null, $array_id = null, $idCorso = null) {
        $collection = $this->conn->$db->mdl_logstore_standard_log;

        if (!isset($idUtente)) {
            if (isset($idCorso) && isset($array_id)) {
                if ($idCorso == 0) {
                    $match = ['$match' => [
                        'userid' => [
                            '$in' => $array_id
                        ]
                    ]];
                } else if (count($array_id) == 0) {
                    $match = ['$match' => [
                        'courseid' => (int) $idCorso
                    ]];
                } else {
                    $match = ['$match' => [
                        'userid' => [
                            '$in' => $array_id
                        ],
                        'courseid' => (int) $idCorso
                    ]];
                }
            } else if (!isset($idCorso)) {
                $match = ['$match' => [
                    'userid' => [
                        '$in' => $array_id
                    ]
                ]];
            }
        } else {
            $match = ['$match' => [
                'userid' => (int) $idUtente
            ]];
        }
//        $pipeline = [$match,
//            ['$lookup' => [
//                'from' => 'mdl_utenti_complete',
//                'localField' => 'userid',
//                'foreignField' => 'userid',
//                'as' => 'info'
//            ]], ['$set' => [
//                'username' => [
//                    '$arrayElemAt' => [
//                        '$info.username',
//                        0
//                    ]
//                ],
//                'email' => [
//                    '$arrayElemAt' => [
//                        '$info.email',
//                        0
//                    ]
//                ],
//                'cf' => [
//                    '$arrayElemAt' => [
//                        '$info.idnumber',
//                        0
//                    ]
//                ],
//                'nome' => [
//                    '$arrayElemAt' => [
//                        '$info.nome',
//                        0
//                    ]
//                ],
//                'citta' => [
//                    '$arrayElemAt' => [
//                        '$info.citta',
//                        0
//                    ]
//                ],
//                'data' => [
//                    '$arrayElemAt' => [
//                        '$info.data',
//                        0
//                    ]
//                ],
//                'azienda' => [
//                    '$arrayElemAt' => [
//                        '$info.azienda',
//                        0
//                    ]
//                ]
//                ,
//                'phone2' => [
//                    '$arrayElemAt' => [
//                        '$info.phone2',
//                        0
//                    ]
//                ],
//                'phone1' => [
//                    '$arrayElemAt' => [
//                        '$info.phone1',
//                        0
//                    ]
//                ],
//            ]], ['$project' => [
//                'info' => 0
//            ]]];
        //var_dump($pipeline) ;exit();
        $out = $collection->aggregate($match);
        return $out->toArray();
    }

    public function constructLogArray($array) {
        $res = array();
        foreach ($array as $singolo) {
            if ($singolo['action'] == "failed")
                continue;
            $tmp = array();

            $tmp['id'] = $singolo['id'];
            $tmp['userid'] = $singolo['userid'];
            if (!isset($singolo['nome']))
                continue;
            $name = $singolo['nome'];
            $tmp['name'] = $name;
            $tmp['username'] = $singolo['username'];
            $tmp['email'] = $singolo['email'];
            if (isset($singolo['cf']))
                $tmp['cf'] = $singolo['cf'];
            else
                $tmp['cf'] = "n/d";

            if (!isset($singolo['data']))
                $tmp['data_nascita'] = "n/d";
            else
                $tmp['data_nascita'] = date("d/m/Y", $singolo['data']);
            if (!isset($singolo['citta']))
                $tmp['citta'] = "n/d";
            else
                $tmp['citta'] = $singolo['citta'];
            $data = date("d/m/Y", $singolo['timecreated']);
            $tmp['timecreated'] = $data;
            $orario = date("H:i:s", $singolo['timecreated']);
            $tmp['timestamp'] = $singolo['timecreated'];
            $tmp['orario'] = $orario;
            $tmp['action'] = $singolo['action'];
            $tmp['azienda'] = $singolo['azienda'];
            $tmp['phone'] = "n/d";
            if (isset($singolo['phone1']) && $singolo['phone1'] != "")
                $tmp['phone'] = $singolo['phone1'];
            if (isset($singolo['phone1']) && $singolo['phone2'] != "")
                $tmp['phone'] = $singolo['phone2'];
            array_push($res, $tmp);
        }
        return $res;
    }

    public function constructSintesiReportTest($array) {
        $array = $this->sortArray($array);

        if (isset($_GET['debug'])) {
            print_r($array);
            exit();
        }
        $result = array();
        $prov = null;
        $primo = array();
        $prec = array(); //per risolvere vewed dashboard
        $contatore = 1;

        foreach ($array as $singolo) {

            if ($prov == null) {
                $prov = $singolo;
                $primo = $prov;
                $prec = $prov;
                $contatore++;
                continue;
            }
            $contatore++;
            $data_prov = str_replace("/", "", $prov['timecreated']);
            $data_singolo = str_replace("/", "", $singolo['timecreated']);
            if ($prov['userid'] == $singolo['userid']) {
                if ($data_prov == $data_singolo) {
                    if ($singolo['action'] == 'loggedin') {

                        if (false && $singolo['action'] == 'joined' && $prov['action'] == 'joined') {

                            $differenza = $singolo['timestamp'] - $prov['timestamp'];
                            if ($differenza < 3600) {
                                if ($prec['action'] == 'left') {
                                    $primo['orario_logout'] = $prec['orario'];
                                    $primo['id_logout'] = $prec['id'];
                                    $tempo_cumulato = $prec['timestamp'] - $primo['timestamp'];
                                    $tempo_cumulato = $this->convertSeconds($tempo_cumulato);
                                    $primo['tempo_cumulato'] = $tempo_cumulato;
                                    array_push($result, $primo);
                                    $primo = $singolo;
                                    $prov = $singolo;
                                    $prec = $singolo;
                                }
                                continue;
                            }
                        }

                        if (false && $singolo['action'] == "joined") {
                            if ($prec['action'] == "left") {
                                $primo['orario_logout'] = $prec['orario'];
                                $primo['id_logout'] = $prec['id'];
                                $tempo_cumulato = $prec['timestamp'] - $primo['timestamp'];
                                $tempo_cumulato = $this->convertSeconds($tempo_cumulato);
                                $primo['tempo_cumulato'] = $tempo_cumulato;
                                array_push($result, $primo);
                                $primo = $singolo;
                                $prov = $singolo;
                                $prec = $singolo;
                                continue;
                            }
                        }
                        if ($singolo['timestamp'] == $prov['timestamp']) {
                            if ($prec == $prov) {
                                $primo = $singolo;
                                $prov = $singolo;
                                $prec = $prov;
                                continue;
                            } else {
                                $primo['orario_logout'] = $prec['orario'];
                                $primo['id_logout'] = $prec['id'];
                                $tempo_cumulato = $prec['timestamp'] - $primo['timestamp'];
                                $tempo_cumulato = $this->convertSeconds($tempo_cumulato);
                                $primo['tempo_cumulato'] = $tempo_cumulato;
                                array_push($result, $primo);
                            }
                        } else {
                            if ($primo == $prov)
                                continue;
                            $primo['orario_logout'] = $prov['orario'];
                            $primo['id_logout'] = $prov['id'];
                            $tempo_cumulato = $prov['timestamp'] - $primo['timestamp'];
                            $tempo_cumulato = $this->convertSeconds($tempo_cumulato);
                            $primo['tempo_cumulato'] = $tempo_cumulato;
                            array_push($result, $primo);
                        }
                        $prov = $singolo;
                        $primo = $singolo;
                        $prec = $prov;
                    } else if ($singolo['action'] == 'loggedout') {

                        if (false && $singolo['action'] == "left") {
                            $prec = $singolo;
                            $prov = $singolo;
                        } else {
                            $primo['orario_logout'] = $singolo['orario'];
                            $primo['id_logout'] = $singolo['id'];
                            $tempo_cumulato = $singolo['timestamp'] - $primo['timestamp'];
                            $tempo_cumulato = $this->convertSeconds($tempo_cumulato);
                            $primo['tempo_cumulato'] = $tempo_cumulato;
                            array_push($result, $primo);
                            $prov = null;
                            continue;
                        }
                    } else if ($singolo['action'] != 'loggedout') {
                        $prec = $prov;
                        $prov = $singolo;
                    }
                } else {

                    $midnight = $this->getMidnight($singolo['timecreated']);
                    $fourHours = $midnight + 60 * 60 * 6;

                    if ($singolo['timestamp'] >= $midnight && $singolo['timestamp'] <= $fourHours && $singolo['action'] != 'loggedin') {
                        $primo['orario_logout'] = "23:59:59";
                        $primo['id_logout'] = $primo['id'];

                        $beforeMidnight = $this->getTime($primo['timecreated'], true);
                        $tempo_cumulato = $beforeMidnight - $primo['timestamp'];
                        $tempo_cumulato = $this->convertSeconds($tempo_cumulato);
                        $primo['tempo_cumulato'] = $tempo_cumulato;
                        array_push($result, $primo);

                        $primo = $singolo;
                        $primo['orario'] = "00:00:00";
                        $afterMidnight = $this->getTime($singolo['timecreated'], null, true);
                        $primo['timestamp'] = $afterMidnight;
                        $prec = $prov;
                        $prov = $singolo;
                    } else {
                        if (false  && $prec['action'] == "left") {
                            $primo['orario_logout'] = $prec['orario'];
                            $primo['id_logout'] = $prec['id'];
                            $tempo_cumulato = $prec['timestamp'] - $primo['timestamp'];
                            $tempo_cumulato = $this->convertSeconds($tempo_cumulato);
                            $primo['tempo_cumulato'] = $tempo_cumulato;
                            array_push($result, $primo);
                        } else {
                            $primo['orario_logout'] = $prov['orario'];
                            $primo['id_logout'] = $prov['id'];
                            $tempo_cumulato = $prov['timestamp'] - $primo['timestamp'];
                            $tempo_cumulato = $this->convertSeconds($tempo_cumulato);
                            $primo['tempo_cumulato'] = $tempo_cumulato;
                            array_push($result, $primo);
                        }
                        $prov = $singolo;
                        $primo = $singolo;
                        $prec = $prov;
                    }
                }

                if ($contatore == count($array)) {
                    if (false  && $prec['action'] == "left") {
                        $primo['orario_logout'] = $prec['orario'];
                        $primo['id_logout'] = $prec['id'];
                        $tempo_cumulato = $prec['timestamp'] - $primo['timestamp'];
                        $tempo_cumulato = $this->convertSeconds($tempo_cumulato);
                        $primo['tempo_cumulato'] = $tempo_cumulato;
                        array_push($result, $primo);
                    } else {
                        $primo['orario_logout'] = $prov['orario'];
                        $primo['id_logout'] = $prov['id'];
                        $tempo_cumulato = $prov['timestamp'] - $primo['timestamp'];
                        $tempo_cumulato = $this->convertSeconds($tempo_cumulato);
                        $primo['tempo_cumulato'] = $tempo_cumulato;
                        array_push($result, $primo);
                    }
                }
            } else {
                if (false  && $prec['action'] == "left") {
                    $primo['orario_logout'] = $prec['orario'];
                    $primo['id_logout'] = $prec['id'];
                    $tempo_cumulato = $prec['timestamp'] - $primo['timestamp'];
                    $tempo_cumulato = $this->convertSeconds($tempo_cumulato);
                    $primo['tempo_cumulato'] = $tempo_cumulato;
                    array_push($result, $primo);
                } else {
                    $primo['orario_logout'] = $prov['orario'];
                    $primo['id_logout'] = $prov['id'];
                    $tempo_cumulato = $prov['timestamp'] - $primo['timestamp'];
                    $tempo_cumulato = $this->convertSeconds($tempo_cumulato);
                    $primo['tempo_cumulato'] = $tempo_cumulato;
                    array_push($result, $primo);
                }
                $prov = $singolo;
                $primo = $singolo;
                $prec = $prov;
            }
        }
        return $result;
    }

    public function constructSintesiReportEnzo($array, $db){
        $finale = array();
        $diff_add = 0; //contatore tempo da aggiungere
        $check = array();
        $temp = array();

        foreach ($array as $key => $singolo) {

            //se un azione di login
            if($singolo['action'] == 'loggedin'){
                //controllo se dopo ce azione loggedout
                if($array[$key+1]['action'] == 'loggedout'){
                    $singolo['data_logout'] = date('d/m/Y', $array[$key+1]['timestamp']);
                    $singolo['orario_logout'] = $array[$key+1]['orario'];
                    $singolo['id_logout'] = $array[$key+1]['id'];

                    $diff = abs($array[$key+1]['timestamp'] - $singolo['timestamp']);

//                    //differenza pi� di un giorno imposto fino alla mezzanotte???
//                    if($diff >= 86400){
//                        $singolo['orario_logout'] = '23:59:59';
//                        $date_singolo = date('Y-m-d', $singolo['timestamp']);
//
//                        $diff = abs($singolo['timestamp'] - strtotime($date_singolo. ' 23:59:59'));
//                    }

                    $singolo['tempo_cumulato'] = $this->convertSeconds($diff);

                    array_push($finale, $singolo);
                }

//                if($singolo['id'] == 8519279){
//                    var_dump($diff_add);
//                    exit;
//                }

//                if($singolo['id'] == 7713264){
//                    var_dump($array[$key+1]['id']);
//                    var_dump($array[$key+1]['action']);
//                    var_dump($array[$key+1]);
//                }
//
//                if($singolo['id'] == 7702067){
//                    echo "tomorrow\n";
//                    var_dump($array[$key+1]['action']);
//                }

                //controllo se dopo ce altra azione azione loggedin
                if($array[$key+1]['action'] == 'loggedin'){

//                    $diff_add = 0;

                    $key_control = $singolo['userid'].date('Ymd', $array[$key+1]['timestamp']);

                    //se esiste altra azione di login prendo l'ultima azione generica prima del prossimo login
                    $last_action = $this->getLastActionAfterNextLogin($singolo['userid'], $array[$key+1]['id'], $db);

//                    if($singolo['id'] == 7713264){
//                        var_dump($last_action);
//                    }

                    //controllo se � il giorno successivo
                    $isTomorrow = date('Ymd', $singolo['timestamp']) == date('Ymd', $last_action[1] - 86400);

//                    if($singolo['id'] == 7713264){
//                        var_dump($isTomorrow);
//                    }
//
//                    if($singolo['id'] == 7561670){
//                        echo "tomorrow\n";
//                        var_dump($isTomorrow);
//                    }


                    if($isTomorrow){

                        $original_logout = strtotime(date('Y-m-d', $array[$key+1]['timestamp']) . ' ' . date("H:i:s", $last_action[1]));

                        $singolo['id_logout'] = '999999999';
                        $singolo['data_logout'] = date('d/m/Y', $last_action[1]);
                        $singolo['orario_logout'] = '23:59:59';

                        $today_midnyght = strtotime(date('Ymd', $singolo['timestamp']) . ' 23:59:59');

                        $diff = abs($singolo['timestamp'] - $today_midnyght);
                        $singolo['tempo_cumulato'] = $this->convertSeconds($diff);

                        //differenza di tempo da riportare al giorno successivo
//                        $diff_add = abs($original_logout - $today_midnyght);
                        $diff_add = abs($last_action[1] - $today_midnyght);

//                        if($singolo['id'] == 7702067){
//                            var_dump(date('Ymd', $singolo['timestamp'])); //20220810
//                            var_dump(date('Ymd', $last_action[1] - 86400)); //20220810
//                            var_dump($isTomorrow); //true
//                            var_dump($last_action); // 11/08/2022 01:23:30
//                            var_dump($singolo['timestamp']); // 10/08/2022 20:19:01
//                            var_dump($today_midnyght); // 10/08/2022 23:59:59
//                            var_dump($diff_add); // 91411 => 1523 min 31 sec
//                            exit;
//                        }

//                        if($singolo['id'] == 7702067){
//                            echo "diff_add\n";
//                            var_dump($diff_add);
//                        }


                        $check[$key_control] = $diff_add;

//                        if($singolo['id'] == 7545501){
//                            var_dump($original_logout);
//                            var_dump($today_midnyght);
//                            var_dump($diff_add);
//                            exit;
//                        }

//                        array_push($check, array($key_control => $diff_add));

//                        $check[$key_control] = $diff_add;
//                        array_push($result, $check);
//                        if (!array_key_exists($key_control, $check)) {
//                            array_push($result, $check);
//                        }

                    }else{
                        $singolo['id_logout'] = $last_action[0];
                        $singolo['data_logout'] = date("d/m/Y", $last_action[1]);
                        $singolo['orario_logout'] = date("H:i:s", $last_action[1]);

//                        if($singolo['id'] == 8291682){
//                            var_dump($singolo['data_logout']);
//                            var_dump($singolo['orario_logout']);
//                            var_dump($singolo['timestamp']);
//                            var_dump($last_action[1]);
//                        }

                        $diff = abs($singolo['timestamp'] - $last_action[1]);

                        //aggiungo del tempo se devo riportarlo dal giorno precedente
                        if (array_key_exists($singolo['userid'].date('Ymd', $array[$key+1]['timestamp']), $check)) {
                            $diff += $check[$singolo['userid'].date('Ymd', $array[$key+1]['timestamp'])];
                        }

//                        if($singolo['id'] == 8291682){
//                            var_dump($diff);
//                        }

                        if($diff >= 86400){
                            $last_action2 = $this->getLastActionToday($singolo['userid'], $singolo['timestamp'], $db);
                            $singolo['id_logout'] = $last_action[0];
                            $singolo['data_logout'] = date("d/m/Y", $last_action2[1]);
                            $singolo['orario_logout'] = date("H:i:s", $last_action2[1]);

                            $diff = abs($singolo['timestamp'] - $last_action2[1]);
                        }

//                        if($singolo['id'] == 8291682){
//                            var_dump($singolo['data_logout']);
//                            var_dump($singolo['orario_logout']);
//                            var_dump($singolo['timestamp']);
//                            var_dump($last_action2);
//                            var_dump($diff);
//                        }

//                        if($singolo['id'] == 7713264){ //brandu
//                            var_dump($diff); // 1084 => 18 min 4 sec
//                            var_dump($last_action); // 30/08/2022 17:02:27
//                            var_dump($check[$singolo['userid'].date('Ymd', $singolo['timestamp'])]); // 30/08/2022 17:02:27
//                            exit;
//                        }

//                        if($singolo['id'] == 7561642){
//                            var_dump($singolo['orario_logout']);
//                            var_dump($diff);
//                            var_dump($check[$singolo['userid'].date('Ymd', $singolo['timestamp'])]);
//                            exit;
//                        }

//                        if($singolo['id'] == 7561642){
//                            var_dump($singolo['id']);
//                            var_dump($singolo['userid'].date('Ymd', $singolo['timestamp']));
//                            var_dump($check);
////                            exit;
//                        }

//                        if($singolo['id'] == 7713264){
//                            var_dump($check);
//                            var_dump($singolo['userid'].date('Ymd', $array[$key+1]['timestamp']));
//                            var_dump($check[$singolo['userid'].date('Ymd', $array[$key+1]['timestamp'])]);
//                        }



//                        var_dump($check['1147120221008']);

//                        if($check[$key_control]){
//                            var_dump($check[$key_control]);
//                            if($key_control == 1147120221008){
//                                echo "qio";
//                            }
//                            $diff += $check[$key_control];
//                        }

                        $singolo['tempo_cumulato'] = $this->convertSeconds($diff);

//                        if($singolo['id'] == 8291682){
//                            var_dump($singolo['tempo_cumulato']);
//                        }

//                        if($singolo['id'] == 7713264){
//                            var_dump($diff);
//                            var_dump($singolo['tempo_cumulato']);
//                            exit;
//                        }

                        //resetto tempo da aggiungere
//                        $diff_add = 0;

                    }

//                    $diff_add = 0;
                    array_push($finale, $singolo);
                    $check[$singolo['userid'].date('Ymd', $array[$key+1]['timestamp'])] = 0;
                }
            }

            $isTomorrow = false;
//            $diff_add = 0;

        }

        return $finale;
    }

    public function constructSintesiReportPietro($array, $db){
        $finale = array();
        $diff_add = 0; //contatore tempo da aggiungere
        $check = array();
        $temp = array();

        foreach ($array as $key => $singolo) {
            //se un azione di login
            if($singolo['action'] == 'loggedin'){
                //controllo se dopo ce azione loggedout
                if($array[$key+1]['action'] == 'loggedout'){
                    $singolo['data_logout'] = date('d/m/Y', $array[$key+1]['timestamp']);
                    $singolo['orario_logout'] = $array[$key+1]['orario'];
                    $singolo['id_logout'] = $array[$key+1]['id'];

                    $diff = abs($array[$key+1]['timestamp'] - $singolo['timestamp']);
                    $singolo['tempo_cumulato'] = $this->convertSeconds($diff);
                    array_push($finale, $singolo);
                }
                //controllo se dopo ce altra azione azione loggedin
                if($array[$key+1]['action'] == 'loggedin'){
                    $key_control = $singolo['userid'].date('Ymd', $array[$key+1]['timestamp']);
                    $last_action = $this->getLastActionAfterNextLogin($singolo['userid'], $array[$key+1]['id'], $db);
                    //controllo se � il giorno successivo
                    $isTomorrow = date('Ymd', $singolo['timestamp']) == date('Ymd', $last_action[1] - 86400);
                    if($isTomorrow){
                        //GIORNO PRECEDENTE
                        $original_logout = strtotime(date('Y-m-d', $array[$key+1]['timestamp']) . ' ' . date("H:i:s", $last_action[1]));
                        $singolo['id_logout'] = '999999999';
                        $singolo['data_logout'] = date('d/m/Y', $last_action[1]);
                        $singolo['orario_logout'] = '23:59:59';
                        $today_midnyght = strtotime(date('Ymd', $singolo['timestamp']) . ' 23:59:59');
                        $diff = abs($singolo['timestamp'] - $today_midnyght);
                        $singolo['tempo_cumulato'] = $this->convertSeconds($diff);
                        array_push($finale, $singolo);

                        //GIORNO SUCCESSIVO
                        $singolo['id_logout']     = $last_action[0];
                        $singolo['timecreated']   = date("d/m/Y", $last_action[1]);
                        $singolo['orario']        = '00:00:00';
                        $singolo['data_logout']   = date("d/m/Y", $last_action[1]);
                        $singolo['orario_logout'] = date("H:i:s", $last_action[1]);
                        $diff = $this->convertTimeToSeconds($singolo['orario_logout']) - $this->convertTimeToSeconds($singolo['orario']);
                        $singolo['tempo_cumulato'] = $this->convertSeconds($diff);
                        array_push($finale, $singolo);

                        //differenza di tempo da riportare al giorno successivo
                        //$diff_add = abs($last_action[1] - $today_midnyght);
                        //$check[$key_control] = $diff_add;
                    }else{
                        $singolo['id_logout'] = $last_action[0];
                        $singolo['data_logout'] = date("d/m/Y", $last_action[1]);
                        $singolo['orario_logout'] = date("H:i:s", $last_action[1]);
                        $diff = abs($singolo['timestamp'] - $last_action[1]);
                        //aggiungo del tempo se devo riportarlo dal giorno precedente
                        if (array_key_exists($singolo['userid'].date('Ymd', $array[$key+1]['timestamp']), $check)) {
                            $diff += $check[$singolo['userid'].date('Ymd', $array[$key+1]['timestamp'])];
                        }
                        if($diff >= 86400){
                            $last_action2 = $this->getLastActionToday($singolo['userid'], $singolo['timestamp'], $db);
                            $singolo['id_logout'] = $last_action[0];
                            $singolo['data_logout'] = date("d/m/Y", $last_action2[1]);
                            $singolo['orario_logout'] = date("H:i:s", $last_action2[1]);

                            $diff = abs($singolo['timestamp'] - $last_action2[1]);
                        }
                        $singolo['tempo_cumulato'] = $this->convertSeconds($diff);
                        array_push($finale, $singolo);
                    }

                    $check[$singolo['userid'].date('Ymd', $array[$key+1]['timestamp'])] = 0;
                }
            }
            $isTomorrow = false;
        }
        return $finale;
    }

    public function constructSintesiReport($array) {
        $array = $this->sortArray($array);

        if (isset($_GET['debug'])) {
            print_r($array);
            exit();
        }
        $result = array();
        $prov = null;
        $primo = array();
        $prec = array(); //per risolvere viewed dashboard
        $contatore = 0; //per risolvere duplicati

        foreach ($array as $singolo) {

            if ($prov == null) {
                $prov = $singolo;
                $primo = $prov;
                $prec = $prov;
                $contatore++;
                continue;
            }
            $contatore++;
            $data_prov = str_replace("/", "", $prov['timecreated']);
            $data_singolo = str_replace("/", "", $singolo['timecreated']);
            if ($prov['userid'] == $singolo['userid']) {

//                if(($singolo['userid'] == 2165 || $prov['userid'] == 2165) && ($data_singolo == '13072022' || $data_prov == '13072022')){
//                    var_dump($data_singolo);
//                    var_dump($data_prov);
//                    echo "--------------<br>";
//                }

//                if($data_singolo == '08092022'){
//                    echo "qui";
//                }

                if ($data_prov == $data_singolo) {
                    if ($singolo['action'] == 'loggedin' || $singolo['action'] == 'joined') {

                        if ($singolo['action'] == 'joined' && $prov['action'] == 'joined') {

                            $differenza = $singolo['timestamp'] - $prov['timestamp'];
                            if ($differenza < 3600) {
                                if ($prec['action'] == 'left') {
                                    $primo['orario_logout'] = $prec['orario'];
                                    $primo['id_logout'] = $prec['id'];



                                    $tempo_cumulato = $prec['timestamp'] - $primo['timestamp'];
                                    $tempo_cumulato = $this->convertSeconds($tempo_cumulato);
                                    $primo['tempo_cumulato'] = $tempo_cumulato;
                                    array_push($result, $primo);
                                    $primo = $singolo;
                                    $prov = $singolo;
                                    $prec = $singolo;
                                }
                                continue;
                            }
                        }

                        if ($singolo['userid'] == 5078) {
                            //var_dump($singolo);var_dump($prec);exit('ASDA');
                        }

                        if ($singolo['action'] == "joined") {
                            if ($prec['action'] == "left") {
                                $primo['orario_logout'] = $prec['orario'];
                                $primo['id_logout'] = $prec['id'];
                                $tempo_cumulato = $prec['timestamp'] - $primo['timestamp'];
                                $tempo_cumulato = $this->convertSeconds($tempo_cumulato);
                                $primo['tempo_cumulato'] = $tempo_cumulato;
                                array_push($result, $primo);
                                $primo = $singolo;
                                $prov = $singolo;
                                $prec = $singolo;
                                continue;
                            }
                        }

                        if ($singolo['timestamp'] == $prov['timestamp']) {
                            if ($prec == $prov) {
                                $primo = $singolo;
                                $prov = $singolo;
                                $prec = $prov;
                                continue;
                            } else {
                                $primo['orario_logout'] = $prec['orario'];
                                $primo['id_logout'] = $prec['id'];
                                $tempo_cumulato = $prec['timestamp'] - $primo['timestamp'];
                                $tempo_cumulato = $this->convertSeconds($tempo_cumulato);
                                $primo['tempo_cumulato'] = $tempo_cumulato;
                                array_push($result, $primo);
                            }
                        } else {
                            if ($primo == $prov)
                                continue;
                            $primo['orario_logout'] = $prov['orario'];
                            $primo['id_logout'] = $prov['id'];
                            $tempo_cumulato = $prov['timestamp'] - $primo['timestamp'];
                            $tempo_cumulato = $this->convertSeconds($tempo_cumulato);
                            $primo['tempo_cumulato'] = $tempo_cumulato;

//                            if (!array_key_exists($singolo['id'], $primo)) {
//                                array_push($result, $primo);
//                            }

                            array_push($result, $primo);
                        }


                        $prov = $singolo;
                        $primo = $singolo;
                        $prec = $prov;
                    } else if ($singolo['action'] == 'loggedout' || $singolo['action'] == 'left') {

                        if ($singolo['action'] == "left") {
                            $prec = $singolo;
                            $prov = $singolo;
                        } else {

                            //qui entra
                            $primo['orario_logout'] = $singolo['orario'];
                            $primo['id_logout'] = $singolo['id'];
                            $tempo_cumulato = $singolo['timestamp'] - $primo['timestamp'];
                            $tempo_cumulato = $this->convertSeconds($tempo_cumulato);
                            $primo['tempo_cumulato'] = $tempo_cumulato;
                            array_push($result, $primo);
                            $prov = null;
                            continue;
                        }
                    } else if ($singolo['action'] != 'loggedout' || $singolo['action'] != 'left') {
                        $prec = $prov;
                        $prov = $singolo;
                    }
                } else {

                    $midnight = $this->getMidnight($singolo['timecreated']);
                    $fourHours = $midnight + 60 * 60 * 6;

//                    if($singolo['userid'] == 11471 && $singolo['timecreated'] == '08/09/2022'){
//                        var_dump($fourHours);
//                        var_dump($midnight);
//                        var_dump($singolo['timestamp']);
//                        var_dump($singolo['action']);
//
//                        if ($singolo['timestamp'] >= $midnight && $singolo['timestamp'] <= $fourHours && $singolo['action'] != 'loggedin'){
//                            var_dump('enzo');
//                        }
//                        exit;
//                    }

//                    if($singolo['userid'] == 2165 && $prov['timestamp'] == '1657740177' && $primo['timestamp'] == '1657697123'){
////                        var_dump($midnight);
////                        var_dump($fourHours);
//                        var_dump($prov);
//                    }

                    if ($singolo['timestamp'] >= $midnight && $singolo['timestamp'] <= $fourHours && $singolo['action'] != 'loggedin') {
                        $primo['orario_logout'] = "23:59:59";
                        $primo['id_logout'] = $primo['id'];

                        $beforeMidnight = $this->getTime($primo['timecreated'], true);
                        $tempo_cumulato = $beforeMidnight - $primo['timestamp'];
                        $tempo_cumulato = $this->convertSeconds($tempo_cumulato);
                        $primo['tempo_cumulato'] = $tempo_cumulato;
                        array_push($result, $primo);

                        $primo = $singolo;
                        $primo['orario'] = "00:00:00";
                        $afterMidnight = $this->getTime($singolo['timecreated'], null, true);
                        $primo['timestamp'] = $afterMidnight;
                        $prec = $prov;
                        $prov = $singolo;
                    } else {
                        if ($prec['action'] == "left") {
                            $primo['orario_logout'] = $prec['orario'];
                            $primo['id_logout'] = $prec['id'];
                            $tempo_cumulato = $prec['timestamp'] - $primo['timestamp'];
                            $tempo_cumulato = $this->convertSeconds($tempo_cumulato);
                            $primo['tempo_cumulato'] = $tempo_cumulato;
                            array_push($result, $primo);
                        } else {
                            //entra qui
//                            if($prec['userid'] == 11471 && $prec['timecreated'] == '08/09/2022'){
//                                var_dump($prov['timestamp'] - $primo['timestamp']);
////                                var_dump($primo['timestamp']);
//                                exit;
//                            }

//                            if($singolo['userid'] == 11471 && $singolo['id'] == 7831869){
//                                var_dump($prov['orario']);
//                                var_dump($primo['orario']);
//                            }

                            $primo['orario_logout'] = $prov['orario'];
                            $primo['id_logout'] = $prov['id'];
                            $tempo_cumulato_oo = $prov['timestamp'] - $primo['timestamp'];
                            $tempo_cumulato = $this->convertSeconds($tempo_cumulato_oo);
                            $primo['tempo_cumulato'] = $tempo_cumulato;

                            //revisione enzo
//                            if($singolo['userid'] == 2165){
//                                var_dump($tempo_cumulato);
////                                if($tempo_cumulato == '11:57:34')
//                                if($prov['timestamp'] == '1657740177' && $primo['timestamp'] == '1657697123')
////                                    $tempo_cumulato2 = $prov['timestamp'] - $primo['timestamp'];
////                                var_dump($prov['timestamp']);
////                                var_dump($primo['timestamp']);
//                                    var_dump($tempo_cumulato_oo);
//                                echo "----------<br>";
//                            }
                            array_push($result, $primo);
                        }
                        $prov = $singolo;
                        $primo = $singolo;
                        $prec = $prov;
                    }
                }

//                var_dump($array); exit;

                if ($contatore == count($array)) {
                    if ($prec['action'] == "left") {
                        $primo['orario_logout'] = $prec['orario'];
                        $primo['id_logout'] = $prec['id'];
                        $tempo_cumulato = $prec['timestamp'] - $primo['timestamp'];
                        $tempo_cumulato = $this->convertSeconds($tempo_cumulato);
                        $primo['tempo_cumulato'] = $tempo_cumulato;
                        array_push($result, $primo);
                    } else {

                        //qui il problema duplicato
//                        if($singolo['userid'] == 5194){
//                            var_dump($contatore);
//                        }

                        $primo['orario_logout'] = $prov['orario'];
                        $primo['id_logout'] = $prov['id'];
                        $tempo_cumulato = $prov['timestamp'] - $primo['timestamp'];
                        $tempo_cumulato = $this->convertSeconds($tempo_cumulato);
                        $primo['tempo_cumulato'] = $tempo_cumulato;

//                        if($singolo['userid'] == 5194 && ($contatore == count($array))){
//                            var_dump($result);
//                            exit;
//                        }

//                        if (!array_key_exists($primo['id'], $result)) {
                        array_push($result, $primo);
//                        }
                    }
                }
            } else {
                if ($prec['action'] == "left") {
                    $primo['orario_logout'] = $prec['orario'];
                    $primo['id_logout'] = $prec['id'];
                    $tempo_cumulato = $prec['timestamp'] - $primo['timestamp'];
                    $tempo_cumulato = $this->convertSeconds($tempo_cumulato);
                    $primo['tempo_cumulato'] = $tempo_cumulato;
                    array_push($result, $primo);
                } else {
                    $primo['orario_logout'] = $prov['orario'];
                    $primo['id_logout'] = $prov['id'];
                    $tempo_cumulato = $prov['timestamp'] - $primo['timestamp'];
                    $tempo_cumulato = $this->convertSeconds($tempo_cumulato);
                    $primo['tempo_cumulato'] = $tempo_cumulato;
                    array_push($result, $primo);
                }
                $prov = $singolo;
                $primo = $singolo;
                $prec = $prov;
            }
        }

        return $result;
    }

    public function sortArray($array) {
        uasort($array, function ($first, $second) {
            if ($first['userid'] == $second['userid']) {
                return $first['timestamp'] - $second['timestamp'];
            } else {
                return $first['userid'] <= $second['userid'];
            }
        });
        return $array;
    }

    public function setSintesiSheet($index, $spreadsheet, $letter, $filtri, $j, $result) {
        if (!in_array("data", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Data/Ora");
            $letter++;
        }
        if (!in_array("userid", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Userid");
            $letter++;
        }
        if (!in_array("username", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Username");
            $letter++;
        }
        if (!in_array("name", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Nome Cognome");
            $letter++;
        }
        if (!in_array("cf", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Codice Fiscale");
            $letter++;
        }
        if (!in_array("email", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Email");
            $letter++;
        }
        if (!in_array("dn", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Data di nascita");
            $letter++;
        }
        if (!in_array("ln", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Luogo di nascita");
            $letter++;
        }
        if (!in_array("azienda", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Azienda");
            $letter++;
        }
        if (!in_array("phone", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "N.di telefono");
            $letter++;
        }
        if (!in_array("orarioA", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Ora del login");
            $letter++;
        }
        if (!in_array("orarioL", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Ora del logout");
            $letter++;
        }
        if (!in_array("tc", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Tempo cumulato");
        }



        $spreadsheet->getActiveSheet()->setTitle("Report");


        $from = "A1";
        $to = $letter . $index;
        $spreadsheet->getActiveSheet()->getStyle("$from:$to")->getFont()->setBold(true);


        $i = $index + 1;
        foreach ($result as $singolo) {
            $letter = "A";
            if (!in_array("data", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['timecreated']);
                $letter++;
            }
            if (!in_array("userid", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['userid']);
                $letter++;
            }
            if (!in_array("username", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['username']);
                $letter++;
            }

            if (!in_array("name", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['name']);
                $letter++;
            }
            if (!in_array("cf", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, @$singolo['cf']);
                $letter++;
            }
            if (!in_array("email", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['email']);
                $letter++;
            }
            if (!in_array("dn", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['data_nascita']);
                $letter++;
            }
            if (!in_array("ln", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['citta']);
                $letter++;
            }
            if (!in_array("azienda", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['azienda']);
                $letter++;
            }
            if (!in_array("phone", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['phone']);
                $letter++;
            }
            if (!in_array("orarioA", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["orario"]);
                $letter++;
            }
            if (!in_array("orarioL", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["orario_logout"]);
                $letter++;
            }
            if (!in_array("tc", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["tempo_cumulato"]);
            }

            $i++;
        }
        foreach (range('A', $letter) as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }
        $spreadsheet->getActiveSheet()->getStyle("A" . $index . ":" . $letter . $index)
            ->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('00FF7F');

        return $i;
    }

    public function setTimeinSheet($index, $spreadsheet, $letter, $filtri, $j, $finale, $array_mesi, $fromReport = null) {
        //INIZIO RE1-T2 Inserimento titolo del corso in intestazione (setTimeinSheet)
        $course_filtered = false;
        if(isset($fromReport)) $course_filtered = true;
        if(isset($finale[0]['corso']) && $course_filtered && $fromReport >= 0){
            //Imposto il titolo
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Corso seguito : " . $finale[0]['corso']);

            //Unisco le celle
            $all_letters = range('A', 'Z');

            $int_1 = 12;
            $int_2 = (int) $letter;
            $int_3 = is_array($array_mesi) ? count($array_mesi) : 0;
            $int_4 = is_array($filtri)     ? count($filtri)     : 0;

            $merge_to_index = $int_1+$int_2+$int_3-$int_4;
            if($merge_to_index >= count($all_letters)) $merge_to_index = count($all_letters)-1;
            $sheet = $spreadsheet->getActiveSheet();
            $sheet->MergeCells($letter . $index . ':' . $all_letters[$merge_to_index] . $index);
            $index += 2;
        }
        //FINE RE1-T2

        if (!in_array("userid", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Userid");
            $letter++;
        }
        if (!in_array("username", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Username");
            $letter++;
        }
        if (!in_array("name", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Nome Cognome");
            $letter++;
        }
        if (!in_array("cf", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Codice Fiscale");
            $letter++;
        }
        if (!in_array("email", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Email");
            $letter++;
        }
        if (!in_array("dn", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Data di Nascita");
            $letter++;
        }
        if (!in_array("ln", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Luogo di Nascita");
            $letter++;
        }
        if (!in_array("azienda", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Azienda");
            $letter++;
        }
        if (!in_array("phone", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "N.di telefono");
            $letter++;
        }
        if (isset($fromReport) && !in_array("corso", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Corso");
            $letter++;
        }
        if (isset($fromReport) && !in_array("courseid", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Id Corso");
            $letter++;
        }
        if (!in_array("tc", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue("$letter" . $index, "Tempo cumulato");
            $letter++;
        }


        $column = $letter;
        $giro = 0;

        foreach ($array_mesi as $mese) {
            if (!in_array("data_" . $giro, $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($column . $index, $mese);
                $column++;
            }
            $giro++;
        }


        $spreadsheet->getActiveSheet()->setTitle("Report");

        $from = "A1";
        $to = "$column" . $index;
        $spreadsheet->getActiveSheet()->getStyle("$from:$to")->getFont()->setBold(true);

        $i = $index + 1;

        //INIZIO RE1-T5
        $total_time   = 0;
        $total_letter = '';
        //FINE   RE1-T5
        foreach ($finale as $singolo) {
            $letter = 'A';

            if (!in_array("userid", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['userid']);
                $letter++;
            }
            if (!in_array("username", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['username']);
                $letter++;
            }
            if (!in_array("name", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['name']);
                $letter++;
            }
            if (!in_array("cf", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['cf']);
                $letter++;
            }
            if (!in_array("email", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['email']);
                $letter++;
            }
            if (!in_array("dn", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, @$singolo['data_nascita']);
                $letter++;
            }
            if (!in_array("ln", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, @$singolo['citta']);
                $letter++;
            }
            if (!in_array("azienda", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['azienda']);
                $letter++;
            }
            if (!in_array("phone", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['phone']);
                $letter++;
            }
            if (isset($fromReport) && !in_array("corso", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['corso']);
                $letter++;
            }
            if (isset($fromReport) && !in_array("courseid", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['courseid']);
                $letter++;
            }
            if (!in_array("tc", $filtri)) {
                //INIZIO RE1-T5
                $total_time   += $this->convertTimeToSeconds($singolo["tempo_tot"]);
                $total_letter  = $letter;
                //FINE   RE1-T5
                $spreadsheet->setActiveSheetIndex($j)->setCellValue("$letter$i", $singolo["tempo_tot"]);
                $letter++;
            }

            $column = $letter;
            /*
            $giro = 0;
            foreach ($singolo['mesi'] as $value) {
                if (!in_array("data_" . $giro, $filtri)) {
                    $spreadsheet->setActiveSheetIndex($j)->setCellValue("$column$i", $value);
                    $column++;
                }
                $giro++;
            }
            */

            $giro = 0;
            foreach ($array_mesi as $mese) {
                if (!in_array("data_" . $giro, $filtri)) {
                    if(isset($singolo['mesi'][$mese])){
                        $spreadsheet->setActiveSheetIndex($j)->setCellValue("$column$i", $singolo['mesi'][$mese]);
                    }else{
                        $spreadsheet->setActiveSheetIndex($j)->setCellValue("$column$i", '00:00:00');
                    }
                    $column++;
                }
                $giro++;
            }
            $i++;
        }

        //INIZIO RE1-T5
        if (!in_array("tc", $filtri)) {
            //Inserisco il titolo del totale
            $spreadsheet->setActiveSheetIndex($j)->setCellValue("A$i", "TOTALE ORE FORMAZIONE");

            //Unisco le celle
            $all_letters    = range('A', 'Z');
            if(!is_array($filtri)) $filtri = array();
            $l_num = (int) $letter;
            $merge_to_index = 9+$l_num-count($filtri);
            if($merge_to_index >= count($all_letters)) $merge_to_index = count($all_letters)-1;
            $sheet = $spreadsheet->getActiveSheet();
            $sheet->MergeCells('A' . $i . ':' . $all_letters[$merge_to_index] . $i);

            //Inserisco il valore del totale
            $spreadsheet->setActiveSheetIndex($j)->setCellValue("$total_letter$i", $this->convertSeconds($total_time));

            //Imposto la stilizzazione
            $spreadsheet->getActiveSheet()->getStyle("A$i:$total_letter$i")->getFont()->setBold(true);
            $spreadsheet->getActiveSheet()->getStyle("A$i:" . $all_letters[$merge_to_index] . "$i")->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $spreadsheet->getActiveSheet()->getStyle("A$i:$total_letter$i")->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('F2FF2C');

        }
        //FINE   RE1-T5


        foreach (range('A', "$column") as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }
        $spreadsheet->getActiveSheet()->getStyle("A" . $index . ":" . $column . $index)
            ->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('00FF7F');

        return $i;
    }

    public function setTimeinCSV($index, $filename, $letter, $filtri, $j, $finale, $array_mesi, $fromReport = null) {
        //INIZIO RE1-T2 Inserimento titolo del corso in intestazione (setTimeinSheet)
        $row = array();

        $course_filtered = false;
        if(isset($fromReport)) $course_filtered = true;
        if(isset($finale[0]['corso']) && $course_filtered){
            //Imposto il titolo
            $row[] = "Corso seguito : " . $finale[0]['corso'];
            $string = implode(";", $row);
            file_put_contents($filename, $string . "\n", FILE_APPEND);
            file_put_contents($filename, "\n", FILE_APPEND);
        }
        //FINE RE1-T2
        $row = array();
        if(!in_array("userid", $filtri))                          $row[] = "Userid";
        if(!in_array("username", $filtri))                        $row[] = "Username";
        if (!in_array("name", $filtri))                           $row[] = "Nome Cognome";
        if (!in_array("cf", $filtri))                             $row[] = "Codice Fiscale";
        if (!in_array("email", $filtri))                          $row[] = "Email";
        if (!in_array("dn", $filtri))                             $row[] = "Data di Nascita";
        if (!in_array("ln", $filtri))                             $row[] = "Luogo di Nascita";
        if (!in_array("azienda", $filtri))                        $row[] = "Azienda";
        if (!in_array("phone", $filtri))                          $row[] = "N.di telefono";
        if (isset($fromReport) && !in_array("corso", $filtri))    $row[] = "Corso";
        if (isset($fromReport) && !in_array("courseid", $filtri)) $row[] = "Id Corso";

        $column_count_total = count($row);
        if (!in_array("tc", $filtri))                             $row[] = "Tempo cumulato";

        $column = $letter;
        $giro = 0;

        foreach ($array_mesi as $mese) {
            $row[] = $mese;
        }
        $string = implode(";", $row);
        file_put_contents($filename, $string . "\n", FILE_APPEND);

        $i = $index + 1;

        //INIZIO RE1-T5
        $total_time   = 0;
        //FINE   RE1-T5
        foreach ($finale as $singolo) {
            $row = array();
            if (!in_array("userid", $filtri))   $row[] = $singolo['userid'];
            if (!in_array("username", $filtri)) $row[] = $singolo['username'];
            if (!in_array("name", $filtri))     $row[] = $singolo['name'];
            if (!in_array("cf", $filtri))       $row[] = $singolo['cf'];
            if (!in_array("email", $filtri))    $row[] = $singolo['email'];
            if (!in_array("dn", $filtri))       $row[] = @$singolo['data_nascita'];
            if (!in_array("ln", $filtri))       $row[] = @$singolo['citta'];
            if (!in_array("azienda", $filtri))  $row[] = $singolo['azienda'];
            if (!in_array("phone", $filtri))    $row[] = $singolo['phone'];
            if (isset($fromReport) && !in_array("corso", $filtri))    $row[] = $singolo['corso'];
            if (isset($fromReport) && !in_array("courseid", $filtri)) $row[] = $singolo['courseid'];
            if (!in_array("tc", $filtri)) {
                //INIZIO RE1-T5
                $total_time   += $this->convertTimeToSeconds($singolo["tempo_tot"]);
                //FINE   RE1-T5
                $row[] = $singolo["tempo_tot"];
                $letter++;
            }

            $column = $letter;
            $giro = 0;
            foreach ($singolo['mesi'] as $value) {
                $row[] = $value;
            }
            $string = implode(";", $row);
            file_put_contents($filename, $string . "\n", FILE_APPEND);
        }

        //INIZIO RE1-T5
        if (!in_array("tc", $filtri)) {
            $row = array();
            //Inserisco il titolo del totale
            for($i = 0; $i < ($column_count_total-1); $i++){
                $row[] = '';
            }
            $row[] = "TOTALE ORE FORMAZIONE";

            //Inserisco il valore del totale
            $row[] = $this->convertSeconds($total_time);
            $string = implode(";", $row);
            file_put_contents($filename, $string . "\n", FILE_APPEND);
        }
        //FINE   RE1-T5
    }

    public function setTimeinSheetNew($index, $spreadsheet, $letter, $filtri, $j, $finale, $array_mesi, $fromReport = null) {
        //INIZIO RE1-T2 Inserimento titolo del corso in intestazione (setTimeinSheet)
        $course_filtered = false;
        if(isset($fromReport)) $course_filtered = true;
        if(isset($finale[0]['corso']) && $course_filtered){
            //Imposto il titolo
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Corso seguito : " . $finale[0]['course_name']);

            //Unisco le celle
            $all_letters = range('A', 'Z');
            $merge_to_index = 12+$letter+count($array_mesi)-count($filtri);
            if($merge_to_index >= count($all_letters)) $merge_to_index = count($all_letters)-1;
            $sheet = $spreadsheet->getActiveSheet();
            $sheet->MergeCells($letter . $index . ':' . $all_letters[$merge_to_index] . $index);
            $index += 2;
        }
        //FINE RE1-T2
        if (!in_array("user_id", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "User ID");
            $letter++;
        }
        if (!in_array("username", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Username");
            $letter++;
        }
        if (!in_array("user_fullname", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Nome Cognome");
            $letter++;
        }
        if (!in_array("user_fiscalcode", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Codice Fiscale");
            $letter++;
        }
        if (!in_array("user_email", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Email");
            $letter++;
        }
        if (!in_array("user_date_of_birth", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Data di Nascita");
            $letter++;
        }
        if (!in_array("user_city_of_birth", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Luogo di Nascita");
            $letter++;
        }
        if (!in_array("user_company_name", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Azienda");
            $letter++;
        }
        if (!in_array("user_telephone", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "N.di telefono");
            $letter++;
        }
        if (isset($fromReport) && !in_array("course_name", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Corso");
            $letter++;
        }
        if (isset($fromReport) && !in_array("course_id", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Id Corso");
            $letter++;
        }
        if (!in_array("total_time", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue("$letter" . $index, "Tempo cumulato");
            $letter++;
        }

        $column = $letter;
        $giro = 0;

        foreach ($array_mesi as $mese) {
            if (!in_array("data_" . $giro, $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($column . $index, $mese);
                $column++;
            }
            $giro++;
        }
        $spreadsheet->getActiveSheet()->setTitle("Report");

        $from = "A1";
        $to = "$column" . $index;
        $spreadsheet->getActiveSheet()->getStyle("$from:$to")->getFont()->setBold(true);

        $i = $index + 1;

        //INIZIO RE1-T5
        $total_time   = 0;
        $total_letter = '';
        //FINE   RE1-T5
        foreach ($finale as $singolo) {
            $letter = 'A';
            if (!in_array("user_id", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['user_id']);
                $letter++;
            }
            if (!in_array("username", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['username']);
                $letter++;
            }
            if (!in_array("user_fullname", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['user_fullname']);
                $letter++;
            }
            if (!in_array("user_fiscalcode", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['user_fiscalcode']);
                $letter++;
            }
            if (!in_array("user_email", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['user_email']);
                $letter++;
            }
            if (!in_array("user_date_of_birth", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, @$singolo['user_date_of_birth']);
                $letter++;
            }
            if (!in_array("user_city_of_birth", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, @$singolo['user_city_of_birth']);
                $letter++;
            }
            if (!in_array("user_company_name", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['user_company_name']);
                $letter++;
            }
            if (!in_array("user_telephone", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['user_telephone']);
                $letter++;
            }
            if (isset($fromReport) && !in_array("course_name", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['course_name']);
                $letter++;
            }
            if (isset($fromReport) && !in_array("course_id", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['course_id']);
                $letter++;
            }
            if (!in_array("total_time", $filtri)) {
                //INIZIO RE1-T5
                $total_time   += $this->convertTimeToSeconds($singolo["total_time"]);
                $total_letter  = $letter;
                //FINE   RE1-T5
                $spreadsheet->setActiveSheetIndex($j)->setCellValue("$letter$i", $singolo["total_time"]);
                $letter++;
            }

            $column = $letter;
            $giro = 0;
            foreach ($array_mesi as $mese) {
                if (!in_array("data_" . $giro, $filtri)) {
                    if(!isset($singolo[$mese])) $singolo[$mese] = '00:00:00';
                    $spreadsheet->setActiveSheetIndex($j)->setCellValue("$column$i", $singolo[$mese]);
                    $column++;
                }
                $giro++;
            }
            $i++;
        }

        //INIZIO RE1-T5
        if (!in_array("total_time", $filtri)) {
            //Inserisco il titolo del totale
            $spreadsheet->setActiveSheetIndex($j)->setCellValue("A$i", "TOTALE ORE FORMAZIONE");

            //Unisco le celle
            $all_letters    = range('A', 'Z');
            if(!is_array($filtri)) $filtri = array();
            $l_num = (int) $letter;
            $merge_to_index = 9+$l_num-count($filtri);
            if($merge_to_index >= count($all_letters)) $merge_to_index = count($all_letters)-1;
            $sheet = $spreadsheet->getActiveSheet();
            $sheet->MergeCells('A' . $i . ':' . $all_letters[$merge_to_index] . $i);

            //Inserisco il valore del totale
            $spreadsheet->setActiveSheetIndex($j)->setCellValue("$total_letter$i", $this->convertSeconds($total_time));

            //Imposto la stilizzazione
            $spreadsheet->getActiveSheet()->getStyle("A$i:$total_letter$i")->getFont()->setBold(true);
            $spreadsheet->getActiveSheet()->getStyle("A$i:" . $all_letters[$merge_to_index] . "$i")->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $spreadsheet->getActiveSheet()->getStyle("A$i:$total_letter$i")->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('F2FF2C');

        }
        //FINE   RE1-T5


        foreach (range('A', "$column") as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }
        $spreadsheet->getActiveSheet()->getStyle("A" . $index . ":" . $column . $index)
            ->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('00FF7F');

        return $i;
    }


    public function getNameAttivitaBBB($db) {
        $name = array();
        $collection = $this->conn->$db->mdl_bigbluebuttonbn;
        $cursore = $collection->find(
            ['id' => ['$ne' => 0]],
            ['projection' => ['name' => 1, 'id' => 1]]
        );
        foreach ($cursore as $singolo) {
            $name[$singolo['id']] = $singolo['name'];
        }
        return $name;
    }

    public function getNameAttivitaHVP($db) {
        $name = array();
        $collection = $this->conn->$db->mdl_hvp;
        $cursore = $collection->find(
            [],
            ['projection' => ['name' => 1, 'id' => 1]]
        );
        foreach ($cursore as $singolo) {
            if (isset($singolo['name'])) {
                $name[$singolo['id']] = $singolo['name'];
            }
        }
        return $name;
    }

    public function getAllNameCourse($db) {
        $name = array();
        $collection = $this->conn->$db->mdl_course;
        $cursore = $collection->find(
            ['id' => ['$ne' => 0]],
            ['projection' => ['fullname' => 1, 'id' => 1]]
        );
        foreach ($cursore as $singolo) {
            $name[$singolo['id']] = $singolo['fullname'];
        }
        return $name;
    }

    public function constructTotalLogArray($array, $bbbName, $hvpName, $nameCorsi) {
        $finale = array();
        foreach ($array as $singolo) {

            $tmp = array();
            $tmp['id'] = $singolo['id'];
            $tmp['userid'] = $singolo['userid'];
            if (!isset($singolo['nome']))
                continue;
            $name = $singolo['nome'];
            $tmp['name'] = $name;
            $tmp['username'] = $singolo['username'];
            $tmp['email'] = $singolo['email'];
            if (isset($singolo['cf']))
                $tmp['cf'] = $singolo['cf'];
            else
                $tmp['cf'] = "n/d";

            if (!isset($singolo['data']))
                $tmp['data_nascita'] = "n/d";
            else
                $tmp['data_nascita'] = date("d/m/Y", $singolo['data']);


            if (!isset($singolo['citta']))
                $tmp['citta'] = "n/d";
            else
                $tmp['citta'] = $singolo['citta'];
            $time = date("d/m/Y H:i:s", $singolo['timecreated']);
            $tmp['timecreated'] = $time;
            $tmp['ip'] = $singolo['ip'];
            $tmp['action'] = $singolo['action'];
            $tmp['attivita'] = "";
            $tmp['phone'] = "n/d";
            if (isset($singolo['phone1']) && $singolo['phone1'] != "")
                $tmp['phone'] = $singolo['phone1'];
            if (isset($singolo['phone1']) && $singolo['phone2'] != "")
                $tmp['phone'] = $singolo['phone2'];
            if (($singolo['action'] == 'viewed' && $singolo['target'] != "course") && $singolo['objecttable'] == null || $singolo['target'] == 'user' || $singolo['target'] == 'user_login') {
                $toReplace = ['{', '}', ';', '"'];
                $singolo['other'] = str_replace($toReplace, " ", $singolo['other']);

                $tmp['attivita'] = $singolo['other'];
            }
            if ($singolo['component'] == "mod_hvp") {
                if (isset($hvpName[$singolo['objectid']]))
                    $tmp['attivita'] = $hvpName[$singolo['objectid']];
            } else if ($singolo['component'] == "mod_bigbluebuttonbn") {
                if (isset($bbbName[$singolo['objectid']]))
                    $tmp['attivita'] = $bbbName[$singolo['objectid']];
            }
            if ($singolo['target'] == "course") {
                if (isset($nameCorsi[$singolo['courseid']]))
                    $tmp['attivita'] = $nameCorsi[$singolo['courseid']];
            }
            if ($tmp['attivita'] == "null" || $tmp['attivita'] == null || $tmp['attivita'] == "")
                continue;

            array_push($finale, $tmp);
        }

        return $finale;
    }

    public function setTotalLogSheet($index, $spreadsheet, $letter, $filtri, $j, $finale) {

        if (!in_array("data", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Data/Ora");
            $letter++;
        }
        if (!in_array("userid", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Userid");
            $letter++;
        }
        if (!in_array("username", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Username");
            $letter++;
        }
        if (!in_array("name", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Nome Cognome");
            $letter++;
        }
        if (!in_array("cf", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Codice Fiscale");
            $letter++;
        }
        if (!in_array("email", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Email");
            $letter++;
        }
        if (!in_array("dn", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Data di nascita");
            $letter++;
        }
        if (!in_array("ln", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Luogo di nascita");
            $letter++;
        }
        if (!in_array("phone", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "N.di telefono");
            $letter++;
        }
        if (!in_array("attivita", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Attività tracciata");
            $letter++;
        }
        if (!in_array("azione", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Azione");
            $letter++;
        }
        if (!in_array("ip", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Ip");
        }

        $spreadsheet->getActiveSheet()->setTitle("Report");


        $from = "A13";
        $to = $letter . $index;
        $spreadsheet->getActiveSheet()->getStyle("$from:$to")->getFont()->setBold(true);

        $i = $index + 1;
        foreach ($finale as $singolo) {
            $letter = 'A';

            if (!in_array("data", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['timecreated']);
                $letter++;
            }

            if (!in_array("userid", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['userid']);
                $letter++;
            }
            if (!in_array("username", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['username']);
                $letter++;
            }
            if (!in_array("name", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['name']);
                $letter++;
            }

            if (!in_array("cf", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['cf']);
                $letter++;
            }

            if (!in_array("email", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['email']);
                $letter++;
            }

            if (!in_array("dn", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['data_nascita']);
                $letter++;
            }

            if (!in_array("ln", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['citta']);
                $letter++;
            }
            if (!in_array("phone", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['phone']);
                $letter++;
            }

            if (!in_array("attivita", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, @$singolo["attivita"]);
                $letter++;
            }
            if (!in_array("azione", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["action"]);
                $letter++;
            }
            if (!in_array("ip", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["ip"]);
            }
            $i++;
        }
        foreach (range('A', $letter) as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }
        $spreadsheet->getActiveSheet()->getStyle("A" . $index . ":" . $letter . $index)
            ->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('00FF7F');

        return $i;
    }

    public function getAggregateHvpNew($db, $inizio, $fine, $valoreCorso = null, $array_id = null) {
        $collection = $this->conn->$db->mdl_logstore_standard_log;

        if (isset($valoreCorso) && isset($array_id)) {
            if ($valoreCorso == 0) {
                $match = ['$match' => [
                    'userid' => [
                        '$in' => $array_id
                    ],
                    'component' => "mod_hvp",
                    'action' => [
                        '$in' => ['progress', 'submitted']
                    ],
                    'timecreated' => [
                        '$gte' => $inizio,
                        '$lte' => $fine
                    ]
                ]];
            } else if (count($array_id) == 0) {
                $match = ['$match' => [
                    'courseid' => (int) $valoreCorso,
                    'component' => "mod_hvp",
                    'action' => [
                        '$in' => ['progress', 'submitted']
                    ],
                    'timecreated' => [
                        '$gte' => $inizio,
                        '$lte' => $fine
                    ]
                ]];
            } else {
                $match = ['$match' => [
                    'userid' => [
                        '$in' => $array_id
                    ],
                    'courseid' => (int) $valoreCorso,
                    'component' => "mod_hvp",
                    'action' => [
                        '$in' => ['progress', 'submitted']
                    ],
                    'timecreated' => [
                        '$gte' => $inizio,
                        '$lte' => $fine
                    ]
                ]];
            }
        } else if (isset($array_id) && !isset($valoreCorso)) {
            $match = ['$match' => [
                'userid' => [
                    '$in' => $array_id
                ],
                'component' => "mod_hvp",
                'action' => [
                    '$in' => ['progress', 'submitted']
                ],
                'timecreated' => [
                    '$gte' => $inizio,
                    '$lte' => $fine
                ]
            ]];
        } else if (isset($valoreCorso) && !isset($array_id)) {
            $match = ['$match' => [
                'courseid' => (int) $valoreCorso,
                'component' => "mod_hvp",
                'action' => [
                    '$in' => ['progress', 'submitted']
                ],
                'timecreated' => [
                    '$gte' => $inizio,
                    '$lte' => $fine
                ]
            ]];
        }

        $lookup_objectid = array (
            '$lookup' =>
                array (
                    'from' => 'mdl_logstore_standard_log',
                    'let' =>
                        array (
                            'objectid' => '$objectid',
                            'contextinstanceid' => '$contextinstanceid',
                        ),
                    'pipeline' =>
                        array (
                            0 =>
                                array (
                                    '$match' =>
                                        array (
                                            '$and' =>
                                                array (
                                                    0 =>
                                                        array (
                                                            '$expr' =>
                                                                array (
                                                                    '$eq' =>
                                                                        array (
                                                                            0 => '$objectid',
                                                                            1 => 'submitted',
                                                                        ),
                                                                ),
                                                        ),
                                                    1 =>
                                                        array (
                                                            '$expr' =>
                                                                array (
                                                                    '$eq' =>
                                                                        array (
                                                                            0 => '$contextinstanceid',
                                                                            1 => '$$contextinstanceid',
                                                                        ),
                                                                ),
                                                        ),
                                                ),
                                        ),
                                ),
                            1 =>
                                array (
                                    '$sort' =>
                                        array (
                                            'timecreated' => -1,
                                        ),
                                ),
                            2 =>
                                array (
                                    '$limit' => 1,
                                ),
                            3 =>
                                array (
                                    '$project' =>
                                        array (
                                            '_id' => 0,
                                            'objectid' => 1,
                                        ),
                                ),
                        ),
                    'as' => 'object_calculated',
                ),
        );

        $lookup_last_progress = array (
            '$lookup' =>
                array (
                    'from' => 'mdl_logstore_standard_log',
                    'let' =>
                        array (
                            'contextinstanceid' => '$contextinstanceid',
                            'objectid' => '$object_calculated[0].objectid',
                            'userid' => '$userid',
                            'timecreated' => '$timecreated',
                        ),
                    'pipeline' =>
                        array (
                            0 =>
                                array (
                                    '$match' =>
                                        array (
                                            '$and' =>
                                                array (
                                                    0 =>
                                                        array (
                                                            '$expr' =>
                                                                array (
                                                                    '$eq' =>
                                                                        array (
                                                                            0 => '$userid',
                                                                            1 => '$$userid',
                                                                        ),
                                                                ),
                                                        ),
                                                    1 =>
                                                        array (
                                                            '$expr' =>
                                                                array (
                                                                    '$eq' =>
                                                                        array (
                                                                            0 => '$objectid',
                                                                            1 => '$$objectid',
                                                                        ),
                                                                ),
                                                        ),
                                                    2 =>
                                                        array (
                                                            '$expr' =>
                                                                array (
                                                                    '$eq' =>
                                                                        array (
                                                                            0 => '$component',
                                                                            1 => 'mod_hvp',
                                                                        ),
                                                                ),
                                                        ),
                                                    3 =>
                                                        array (
                                                            '$expr' =>
                                                                array (
                                                                    '$eq' =>
                                                                        array (
                                                                            0 => '$action',
                                                                            1 => 'progress',
                                                                        ),
                                                                ),
                                                        ),
                                                    4 =>
                                                        array (
                                                            '$expr' =>
                                                                array (
                                                                    '$lt' =>
                                                                        array (
                                                                            0 => '$timecreated',
                                                                            1 => '$$timecreated',
                                                                        ),
                                                                ),
                                                        ),
                                                ),
                                        ),
                                ),
                            1 =>
                                array (
                                    '$sort' =>
                                        array (
                                            'timecreated' => -1,
                                        ),
                                ),
                            2 =>
                                array (
                                    '$limit' => 1,
                                ),
                        ),
                    'as' => 'last_progress',
                ),
        );

        $lookup_last_viewed = array (
            '$lookup' =>
                array (
                    'from' => 'mdl_logstore_standard_log',
                    'let' =>
                        array (
                            'contextinstanceid' => '$contextinstanceid',
                            'objectid' => '$object_calculated[0].objectid',
                            'userid' => '$userid',
                            'timecreated' => '$timecreated',
                        ),
                    'pipeline' =>
                        array (
                            0 =>
                                array (
                                    '$match' =>
                                        array (
                                            '$and' =>
                                                array (
                                                    0 =>
                                                        array (
                                                            '$expr' =>
                                                                array (
                                                                    '$eq' =>
                                                                        array (
                                                                            0 => '$userid',
                                                                            1 => '$$userid',
                                                                        ),
                                                                ),
                                                        ),
                                                    1 =>
                                                        array (
                                                            '$expr' =>
                                                                array (
                                                                    '$eq' =>
                                                                        array (
                                                                            0 => '$objectid',
                                                                            1 => '$$objectid',
                                                                        ),
                                                                ),
                                                        ),
                                                    2 =>
                                                        array (
                                                            '$expr' =>
                                                                array (
                                                                    '$eq' =>
                                                                        array (
                                                                            0 => '$component',
                                                                            1 => 'mod_hvp',
                                                                        ),
                                                                ),
                                                        ),
                                                    3 =>
                                                        array (
                                                            '$expr' =>
                                                                array (
                                                                    '$eq' =>
                                                                        array (
                                                                            0 => '$action',
                                                                            1 => 'viewed',
                                                                        ),
                                                                ),
                                                        ),
                                                    4 =>
                                                        array (
                                                            '$expr' =>
                                                                array (
                                                                    '$lt' =>
                                                                        array (
                                                                            0 => '$timecreated',
                                                                            1 => '$$timecreated',
                                                                        ),
                                                                ),
                                                        ),
                                                ),
                                        ),
                                ),
                            1 =>
                                array (
                                    '$sort' =>
                                        array (
                                            'timecreated' => -1,
                                        ),
                                ),
                            2 =>
                                array (
                                    '$limit' => 1,
                                ),
                        ),
                    'as' => 'last_viewed',
                ),
        );

        $set_lasts = array (
            '$set' =>
                array (
                    'last_progress_id' =>
                        array (
                            '$arrayElemAt' =>
                                array (
                                    0 => '$last_progress.id',
                                    1 => 0,
                                ),
                        ),
                    'last_progress' =>
                        array (
                            '$arrayElemAt' =>
                                array (
                                    0 => '$last_progress.other',
                                    1 => 0,
                                ),
                        ),
                    'last_viewed_id' =>
                        array (
                            '$arrayElemAt' =>
                                array (
                                    0 => '$last_viewed.id',
                                    1 => 0,
                                ),
                        ),
                    'last_viewed' =>
                        array (
                            '$arrayElemAt' =>
                                array (
                                    0 => '$last_viewed.other',
                                    1 => 0,
                                ),
                        ),
                    'new_object_id' =>
                        array (
                            '$arrayElemAt' =>
                                array (
                                    0 => '$last_progress.objectid',
                                    1 => 0,
                                ),
                        ),
                ),
        );

        $pipeline = [
            $match, $lookup_last_progress, $lookup_last_viewed, $set_lasts
            , ['$lookup' => [
                'from' => 'mdl_utenti_complete',
                'localField' => 'userid',
                'foreignField' => 'userid',
                'as' => 'info'
            ]], ['$set' => [
                'username' => [
                    '$arrayElemAt' => [
                        '$info.username',
                        0
                    ]
                ],
                'email' => [
                    '$arrayElemAt' => [
                        '$info.email',
                        0
                    ]
                ],
                'cf' => [
                    '$arrayElemAt' => [
                        '$info.idnumber',
                        0
                    ]
                ],
                'nome' => [
                    '$arrayElemAt' => [
                        '$info.nome',
                        0
                    ]
                ],
                'citta' => [
                    '$arrayElemAt' => [
                        '$info.citta',
                        0
                    ]
                ],
                'data' => [
                    '$arrayElemAt' => [
                        '$info.data',
                        0
                    ]
                ],
                'azienda' => [
                    '$arrayElemAt' => [
                        '$info.azienda',
                        0
                    ]
                ]
                ,
                'phone2' => [
                    '$arrayElemAt' => [
                        '$info.phone2',
                        0
                    ]
                ],
                'phone1' => [
                    '$arrayElemAt' => [
                        '$info.phone1',
                        0
                    ]
                ],
            ]], ['$project' => [
                'info' => 0
            ]]];

        $out = $collection->aggregate($pipeline);

        return $out->toArray();
    }

    public function getAggregateHvp($db, $inizio, $fine, $valoreCorso = null, $array_id = null) {
        $collection = $this->conn->$db->mdl_logstore_standard_log;

        $sort = '';

        if(empty($array_id)){
            unset($array_id);
        }

        if (isset($valoreCorso) && isset($array_id)) {
            if ($valoreCorso == 0) {
                $match = ['$match' => [
                    'userid' => [
                        '$in' => $array_id
                    ],
                    'component' => "mod_hvp",
                    'timecreated' => [
                        '$gte' => $inizio,
                        '$lte' => $fine
                    ]
                ]];
            } else if (count($array_id) == 0) {
                $match = ['$match' => [
                    'courseid' => (int) $valoreCorso,
                    'component' => "mod_hvp",
                    'timecreated' => [
                        '$gte' => $inizio,
                        '$lte' => $fine
                    ]
                ]];
            } else {
                $match = ['$match' => [
                    'userid' => [
                        '$in' => $array_id
                    ],
                    'courseid' => (int) $valoreCorso,
                    'component' => "mod_hvp",
                    'timecreated' => [
                        '$gte' => $inizio,
                        '$lte' => $fine
                    ]
                ]];
            }
        } else if (isset($array_id) && !isset($valoreCorso)) {
            $sort['$sort'] = ['userid' => -1];
            $match = ['$match' => [
                'userid' => [
                    '$in' => $array_id
                ],
                'component' => "mod_hvp",
                'timecreated' => [
                    '$gte' => $inizio,
                    '$lte' => $fine
                ]
            ]];
        } else if (isset($valoreCorso) && !isset($array_id)) {
            $sort['$sort'] = ['userid' => -1];
//            $sort = [ '$sort' => [ 'userid' => -1 ]];
            $match = ['$match' => [
                'courseid' => (int) $valoreCorso,
                'component' => "mod_hvp",
                'timecreated' => [
                    '$gte' => $inizio,
                    '$lte' => $fine
                ]
            ]];
        }

        $pipeline = [
            $match,
            ['$lookup' => [
                'from' => 'mdl_utenti_complete',
                'localField' => 'userid',
                'foreignField' => 'userid',
                'as' => 'info'
            ]], ['$set' => [
                'username' => [
                    '$arrayElemAt' => [
                        '$info.username',
                        0
                    ]
                ],
                'email' => [
                    '$arrayElemAt' => [
                        '$info.email',
                        0
                    ]
                ],
                'cf' => [
                    '$arrayElemAt' => [
                        '$info.idnumber',
                        0
                    ]
                ],
                'nome' => [
                    '$arrayElemAt' => [
                        '$info.nome',
                        0
                    ]
                ],
                'citta' => [
                    '$arrayElemAt' => [
                        '$info.citta',
                        0
                    ]
                ],
                'data' => [
                    '$arrayElemAt' => [
                        '$info.data',
                        0
                    ]
                ],
                'azienda' => [
                    '$arrayElemAt' => [
                        '$info.azienda',
                        0
                    ]
                ]
                ,
                'phone2' => [
                    '$arrayElemAt' => [
                        '$info.phone2',
                        0
                    ]
                ],
                'phone1' => [
                    '$arrayElemAt' => [
                        '$info.phone1',
                        0
                    ]
                ],
            ]], ['$project' => [
                'info' => 0
            ]]];

        if($sort != ''){
            array_splice( $pipeline, 1, 0, [[ '$sort' => [ 'userid' => -1 ]]] );
        }

//        var_dump($pipeline); exit;

        $out = $collection->aggregate($pipeline);

//        var_dump($out->toArray());

        return $out->toArray();
    }

    public function getAggregateHvpGlobal($db, $inizio, $fine, $valoreCorso = null, $array_id = null) {
        $collection = $this->conn->$db->mdl_logstore_standard_log;

        if (isset($valoreCorso) && isset($array_id)) {
            if ($valoreCorso == 0) {
                $match = ['$match' => [
                    'userid' => [
                        '$in' => $array_id
                    ],
                    'action' => [
                        '$in' => ['progress', 'submitted', 'clicked', 'joined']
                    ],
                    'timecreated' => [
                        '$gte' => $inizio,
                        '$lte' => $fine
                    ]
                ]];
            } else if (count($array_id) == 0) {
                $match = ['$match' => [
                    'courseid' => (int) $valoreCorso,
                    'action' => [
                        '$in' => ['progress', 'submitted', 'clicked', 'joined']
                    ],
                    'timecreated' => [
                        '$gte' => $inizio,
                        '$lte' => $fine
                    ]
                ]];
            } else {
                $match = ['$match' => [
                    'userid' => [
                        '$in' => $array_id
                    ],
                    'courseid' => (int) $valoreCorso,
                    'action' => [
                        '$in' => ['progress', 'submitted', 'clicked', 'joined']
                    ],
                    'timecreated' => [
                        '$gte' => $inizio,
                        '$lte' => $fine
                    ]
                ]];
            }
        } else if (isset($array_id) && !isset($valoreCorso)) {
            $match = ['$match' => [
                'userid' => [
                    '$in' => $array_id
                ],
                'action' => [
                    '$in' => ['progress', 'submitted', 'clicked', 'joined']
                ],
                'timecreated' => [
                    '$gte' => $inizio,
                    '$lte' => $fine
                ]
            ]];
        } else if (isset($valoreCorso) && !isset($array_id)) {
            $match = ['$match' => [
                'courseid' => (int) $valoreCorso,
                'action' => [
                    '$in' => ['progress', 'submitted', 'clicked', 'joined']
                ],
                'timecreated' => [
                    '$gte' => $inizio,
                    '$lte' => $fine
                ]
            ]];
        }

        $pipeline = [
            $match
            , ['$lookup' => [
                'from' => 'mdl_utenti_complete',
                'localField' => 'userid',
                'foreignField' => 'userid',
                'as' => 'info'
            ]], ['$set' => [
                'username' => [
                    '$arrayElemAt' => [
                        '$info.username',
                        0
                    ]
                ],
                'email' => [
                    '$arrayElemAt' => [
                        '$info.email',
                        0
                    ]
                ],
                'cf' => [
                    '$arrayElemAt' => [
                        '$info.idnumber',
                        0
                    ]
                ],
                'nome' => [
                    '$arrayElemAt' => [
                        '$info.nome',
                        0
                    ]
                ],
                'citta' => [
                    '$arrayElemAt' => [
                        '$info.citta',
                        0
                    ]
                ],
                'data' => [
                    '$arrayElemAt' => [
                        '$info.data',
                        0
                    ]
                ],
                'azienda' => [
                    '$arrayElemAt' => [
                        '$info.azienda',
                        0
                    ]
                ]
                ,
                'phone2' => [
                    '$arrayElemAt' => [
                        '$info.phone2',
                        0
                    ]
                ],
                'phone1' => [
                    '$arrayElemAt' => [
                        '$info.phone1',
                        0
                    ]
                ],
            ]], ['$project' => [
                'info' => 0
            ]]];

        $out = $collection->aggregate($pipeline);

        return $out->toArray();
    }

    public function getAggregateHvpAll($db, $valoreCorso = null, $array_id = null) {
        $collection = $this->conn->$db->mdl_logstore_standard_log;

        if (isset($valoreCorso) && isset($array_id)) {
            if ($valoreCorso == 0) {
                $match = ['$match' => [
                    'userid' => [
                        '$in' => $array_id
                    ],
                    'component' => "mod_hvp"
                ]];
            } else if (count($array_id) == 0) {
                $match = ['$match' => [
                    'courseid' => (int) $valoreCorso,
                    'component' => "mod_hvp"
                ]];
            } else {
                $match = ['$match' => [
                    'userid' => [
                        '$in' => $array_id
                    ],
                    'courseid' => (int) $valoreCorso,
                    'component' => "mod_hvp"
                ]];
            }
        } else if (isset($array_id) && !isset($valoreCorso)) {
            $match = ['$match' => [
                'userid' => [
                    '$in' => $array_id
                ],
                'component' => "mod_hvp"
            ]];
        } else if (isset($valoreCorso) && !isset($array_id)) {
            $match = ['$match' => [
                'courseid' => (int) $valoreCorso,
                'component' => "mod_hvp"
            ]];
        }

        $pipeline = [
            $match
            , ['$lookup' => [
                'from' => 'mdl_utenti_complete',
                'localField' => 'userid',
                'foreignField' => 'userid',
                'as' => 'info'
            ]], ['$set' => [
                'username' => [
                    '$arrayElemAt' => [
                        '$info.username',
                        0
                    ]
                ],
                'email' => [
                    '$arrayElemAt' => [
                        '$info.email',
                        0
                    ]
                ],
                'cf' => [
                    '$arrayElemAt' => [
                        '$info.idnumber',
                        0
                    ]
                ],
                'nome' => [
                    '$arrayElemAt' => [
                        '$info.nome',
                        0
                    ]
                ],
                'citta' => [
                    '$arrayElemAt' => [
                        '$info.citta',
                        0
                    ]
                ],
                'data' => [
                    '$arrayElemAt' => [
                        '$info.data',
                        0
                    ]
                ],
                'azienda' => [
                    '$arrayElemAt' => [
                        '$info.azienda',
                        0
                    ]
                ]
                ,
                'phone2' => [
                    '$arrayElemAt' => [
                        '$info.phone2',
                        0
                    ]
                ],
                'phone1' => [
                    '$arrayElemAt' => [
                        '$info.phone1',
                        0
                    ]
                ],
            ]], ['$project' => [
                'info' => 0
            ]]];

//        var_dump($pipeline); exit;

        $out = $collection->aggregate($pipeline);

        return $out->toArray();
    }

//    public function constructReportZoomDetail($user_id, $corso_id, $inizio, $fine, $db){
//        $data = array();
//        $collectionZoom = $this->conn->$db->mdl_zoom_meeting_participants_aggregate;
//
//        //zoom
//        $duration_zoom = 0;
//
//        $pipeline_zoom = [
//            [
//                '$match'=> [
//                    'userid' => $user_id,
//                    'idCorso' => intval($corso_id),
//                    'join_time' => [
//                        '$gte' => $inizio
//                    ],
//                    'leave_time' => [
//                        '$lte' => $fine
//                    ]
//                ]
//            ]
//        ];
//
//        $zoom = $collectionZoom->aggregate($pipeline_zoom);
//        $zoom_out = $zoom->toArray();
//
//        if(count($zoom_out) > 0){
//            $data = ['']
//            $duration_zoom = $zoom_out[0]->durata;
//        }
//    }

    public function constructHvpArrayNew($array, $hvpName = false, $nameCorsi = false, $db = false) {
        $result = array();

        foreach ($array as $singolo) {
            if (!isset($singolo['nome']))
                continue;
            if (!isset($singolo['azienda']))
                continue;
            $tmp = array();
            $tmp['id'] = $singolo['id'];
            $tmp['userid'] = $singolo['userid'];
            $name = $singolo['nome'];
            $tmp['name'] = $name;
            $tmp['username'] = $singolo['username'];
            $tmp['email'] = $singolo['email'];
            if (isset($singolo['cf'])){
                $tmp['cf'] = $singolo['cf'];
            } else {
                $tmp['cf'] = "n/d";
            }
//            if (isset($singolo['cf'])){
//                if($singolo['cf'] != ''){
//                    $tmp['cf'] = $singolo['cf'];
//                }else{
//                    if($db){
//                        $tmp['cf'] = $this->getCfUtenti($db, $singolo['userid']);
//                    }else{
//                        $tmp['cf'] = $singolo['cf'];
//                    }
//                }
//
//            } else {
//                $tmp['cf'] = "n/d";
//            }

            if (!isset($singolo['data']))
                $tmp['data_nascita'] = "n/d";
            else
                $tmp['data_nascita'] = date("d/m/Y", (int) $singolo['data']);

            if (!isset($singolo['citta']))
                $tmp['citta'] = "n/d";
            else
                $tmp['citta'] = $singolo['citta'];
            $tmp['phone'] = "n/d";
            if (isset($singolo['phone1']) && $singolo['phone1'] != "")
                $tmp['phone'] = $singolo['phone1'];
            if (isset($singolo['phone1']) && $singolo['phone2'] != "")
                $tmp['phone'] = $singolo['phone2'];
            $time = date("d/m/Y", $singolo['timecreated']);
            $tmp['timecreated'] = $time;
            $orario = date("H:i:s", $singolo['timecreated']);
            $tmp['timestamp'] = $singolo['timecreated'];
            $tmp['orario'] = $orario;
            $tmp['ip'] = $singolo['ip'];
            $tmp['action'] = $singolo['action'];

            if($singolo['action'] == 'submitted'){
                if(isset($singolo['new_object_id'])){
                    $tmp['objectid'] = $singolo['new_object_id'];
                }else{
                    $tmp['objectid'] = $singolo['objectid'];
                }
            }else{
                $tmp['objectid'] = $singolo['objectid'];
            }

//            $tmp['objectid'] = $singolo['objectid'];

            $tmp['azienda'] = $singolo['azienda'];
            $tmp['other'] = $singolo['other'];
            $tmp['lezione_id'] = $singolo['objectid'];
            $tmp['contextid'] = $singolo['contextid'];
            $tmp['contextinstanceid'] = $singolo['contextinstanceid'];
            if ($hvpName && isset($hvpName[$singolo['objectid']]))
                $tmp['lezione'] = $hvpName[$singolo['objectid']];
//            if ($hvpName && isset($hvpName[$singolo['name']]))
//                $tmp['lezione'] = $hvpName[$singolo['name']];
            else
                $tmp['lezione'] = "";
            if ($nameCorsi && isset($nameCorsi[$singolo['courseid']]))
                $tmp['corso'] = $nameCorsi[$singolo['courseid']];
            else
                $tmp['corso'] = "";
            $tmp['courseid'] = $singolo['courseid'];

            $tmp['submitted'] = false;

            $tmp['last_progress'] = 0;
            $tmp['last_viewed'] = 0;


            if(isset($singolo['last_progress'])){

                if (@unserialize($singolo['last_progress']) === false) {
                    $last_progress = json_decode($singolo['last_progress']);
                } else {
                    $last_progress = json_decode(unserialize($singolo['last_progress']));
                }

                if (!is_null($last_progress->progress)) {
                    @$last_progress = (int)$last_progress->progress;
                } else {
                    $last_progress = 0;
                }

                $tmp['last_progress'] = $last_progress;
            }

            if(isset($singolo['last_viewed'])){

                if (@unserialize($singolo['last_viewed']) === false) {
                    $last_viewed = json_decode($singolo['last_viewed']);
                } else {
                    $last_viewed = json_decode(unserialize($singolo['last_viewed']));
                }

                if (!is_null($last_viewed->progress)) {
                    @$last_progress_viewed = (int)$last_viewed->progress;
                } else {
                    $last_progress_viewed = 0;
                }

                $tmp['last_viewed'] = $last_progress_viewed;
            }

            if($singolo['action'] == 'submitted'){
                $tmp['submitted'] = true;
            }

            array_push($result, $tmp);
        }
        return $result;
    }

    public function constructHvpArray($array, $hvpName = false, $nameCorsi = false, $db = false) {
        $result = array();

//        var_dump($array); exit;

        foreach ($array as $singolo) {
            if (!isset($singolo['nome']))
                continue;
            if (!isset($singolo['azienda']))
                continue;
            $tmp = array();
            $tmp['id'] = $singolo['id'];
            $tmp['userid'] = $singolo['userid'];
            $name = $singolo['nome'];
            $tmp['name'] = $name;
            $tmp['username'] = $singolo['username'];
            $tmp['email'] = $singolo['email'];
            if (isset($singolo['cf'])){
                $tmp['cf'] = $singolo['cf'];
            } else {
                $tmp['cf'] = "n/d";
            }
//            if (isset($singolo['cf'])){
//                if($singolo['cf'] != ''){
//                    $tmp['cf'] = $singolo['cf'];
//                }else{
//                    if($db){
//                        $tmp['cf'] = $this->getCfUtenti($db, $singolo['userid']);
//                    }else{
//                        $tmp['cf'] = $singolo['cf'];
//                    }
//                }
//
//            } else {
//                $tmp['cf'] = "n/d";
//            }

            if (!isset($singolo['data']))
                $tmp['data_nascita'] = "n/d";
            else
                $tmp['data_nascita'] = date("d/m/Y", (int) $singolo['data']);

            if (!isset($singolo['citta']))
                $tmp['citta'] = "n/d";
            else
                $tmp['citta'] = $singolo['citta'];
            $tmp['phone'] = "n/d";
            if (isset($singolo['phone1']) && $singolo['phone1'] != "")
                $tmp['phone'] = $singolo['phone1'];
            if (isset($singolo['phone1']) && $singolo['phone2'] != "")
                $tmp['phone'] = $singolo['phone2'];
            $time = date("d/m/Y", $singolo['timecreated']);
            $tmp['timecreated'] = $time;
            $orario = date("H:i:s", $singolo['timecreated']);
            $tmp['timestamp'] = $singolo['timecreated'];
            $tmp['orario'] = $orario;
            $tmp['ip'] = $singolo['ip'];
            $tmp['action'] = $singolo['action'];
            $tmp['objectid'] = $singolo['objectid'];
            $tmp['azienda'] = $singolo['azienda'];
            $tmp['other'] = $singolo['other'];
            $tmp['lezione_id'] = $singolo['objectid'];
            $tmp['contextid'] = $singolo['contextid'];
            $tmp['contextinstanceid'] = $singolo['contextinstanceid'];
            if ($hvpName && isset($hvpName[$singolo['objectid']]))
                $tmp['lezione'] = $hvpName[$singolo['objectid']];
//            if ($hvpName && isset($hvpName[$singolo['name']]))
//                $tmp['lezione'] = $hvpName[$singolo['name']];
            else
                $tmp['lezione'] = "";
            if ($nameCorsi && isset($nameCorsi[$singolo['courseid']]))
                $tmp['corso'] = $nameCorsi[$singolo['courseid']];
            else
                $tmp['corso'] = "";
            $tmp['courseid'] = $singolo['courseid'];

            $tmp['submitted'] = false;

            if($singolo['action'] == 'submitted'){
                $tmp['submitted'] = true;
            }

            array_push($result, $tmp);
        }

//        var_dump($result);

        return $result;
    }

    public function constructHvpReportOriginal($array, $db) {
        $array = $this->sortArray($array);

//        var_dump($array);exit();

        if (isset($_GET['debug'])) {
            print_r($array);
            exit();
        }
        $finale = array();
        $prov = null;
        $prec = array();
        $tmp = array();
        $viewed = 0;
        $lezioni_terminate = array();
        $entrato = false;
        $last= end($array);

        foreach ($array as $singolo) {

            if (strpos($singolo['other'], "RESET") !== FALSE) {
                //continue;
                foreach ($finale as $key => $elem) {
                    if ($elem['userid'] == $singolo['userid'] && $elem['objectid'] == $singolo['objectid']) {

                        unset($finale[$key]);
                        unset($lezioni_terminate[$singolo['lezione']]);
                        $entrato = true;
                    }
                }
                if ($entrato == false)
                    continue;
            }

            if ($prov == null) {
                if (isset($prec['userid']) && $singolo['userid'] != $prec['userid'])
                    $lezioni_terminate = array();
                $prov = $singolo;
                $prec = $prov;
                continue;
            }

            if ($prov['userid'] == $singolo['userid'] && $prov['timecreated'] == $singolo['timecreated']) {//var_dump($prov);var_dump($singolo);exit('aaaa');
//                if($singolo['id']==7115397){
//                        print_r($singolo); //7115397
//                        print_r($prov); //7113935
//                        print_r($prec); //7113934
//                        print_r($lezioni_terminate);
//                        exit();
//                    }
                if ($prov['action'] == 'viewed' && $singolo['action'] == 'progress') {
                    if (count($array) == 2 || $singolo==$last) {
//                        if($singolo['id']==7115397){
//                            echo "qui"; exit;
//                        }
                        $prov['orario_logout'] = $singolo['orario'];
                        $prov['id_logout'] = $singolo['id'];

                        $prov['enzo'] = 1;

                        $progress = $singolo['other'];

                        if (@unserialize($progress) === false) {
                            $progress = json_decode($progress);
                        } else {
                            $progress = json_decode(unserialize($progress));
                        }

                        if (!is_null($progress)) {
                            @$tempo_cumulato = (int)$progress->progress;
                        } else {
                            $tempo_cumulato = 0;
                        }

//                        $progress = $singolo['other'];
//                        $toReplace = ['{', '}', '"'];
//                        $progress = str_replace($toReplace, "", $progress);
//
//                        if ($progress != "") {
//                            $progress = explode(".", $progress);
//                            $progress = explode(":", $progress[0]);
//                            @$tempo_cumulato = (int) @$progress[1];
//                        } else
//                            $tempo_cumulato = 0;
                        $tempo_cumulato = $this->convertSeconds($tempo_cumulato);

//                        if($prov['id']==7115398) {
//                            echo $prov['userid'].' Data:'.$prov['timecreated'].' Tempo cumulato:'.$tempo_cumulato.' other:'.$prov['other'].' objectid:'.$prov['objectid'].' Action: '.$prov['action']."<br>";
//                            exit;
//                        }

                        $prov['tempo_cumulato'] = $tempo_cumulato;
                        array_push($finale, $prov);
                        break;
                    }
                    $prec = $singolo;
                    $viewed = 1;
                } else if ($singolo['action'] == "submitted") {

                    /* MODIFICA ALDO
                    $tempo_cumulatoSumittedTotale = $this->getLastProgress($prov['userid'], $prov['timecreated'], $prov['objectid'], $db);

                    */

//                    $isYesterday = date('Ymd', $singolo['timestamp']) == date('Ymd', $last_action[1] - 86400);

//                    if($singolo['id'] == 7116311){
//                        $lastProgressViewed = $this->getDateLastProgressViewed($prov['userid'], $prov['timestamp'], $prov['objectid'], $db);
////                        var_dump($lastProgressViewed); exit;
//
//                        $isTime = $this->getRangeDateString($prov['timestamp'], $lastProgressViewed);
//
//                        var_dump($isTime); exit;
//
//                        if($isTime == 'Yesterday'){
//
//                        }
//                        var_dump($isTime); exit;
//
//                        $curr_date =strtotime(date("Y-m-d H:i:s"));
//                        $diffDate =floor(($curr_date-$lastProgressViewed)/(60*60*24));
//
//                        var_dump($diffDate); exit;
//
//                        //se yesterday
//                        if($diffDate == 1){
//
//                        }
//
//                        var_dump($lastProgressViewed);
//                        exit;
//                    }

                    //condizione valida in alcuni casi data 21/11/2022 (per userid 10786) corretto il 13/07/2022
//                    $tempo_cumulato = $this->getTimeLesson($prov['objectid'], $db);
//                    $tempo_cumulato = $this->convertSeconds(($tempo_cumulato));
//                    $tempo_cumulato = $this->convertSeconds($this->getLastProgressNew($singolo['userid'], $singolo['timestamp'], $prov['objectid'], $db));


                    //condizione valida in alcuni casi data 21/11/2022 (per userid 10786) corretto il 03/11/2022
                    $tempo_cumulato = $this->getTimeLesson($prov['objectid'], $db);
                    $lastProgress = $this->getLastProgressNew($singolo['userid'], $singolo['timestamp'], $prov['objectid'], $db);

//                    $lastProgress = $this->getLastProgressViewed($singolo['userid'], $singolo['timestamp'], $prov['objectid'], $db);

//                    if($prov['id'] == 7115398){
//                        var_dump($tempo_cumulato);
//                        var_dump($lastProgress);
//                        exit;
//                    }

                    $lastProgressViewed = $this->getDateLastProgressViewed($prov['userid'], $prov['timestamp'], $prov['objectid'], $db);
                    $isTime = $this->getRangeDateString($prov['timestamp'], $lastProgressViewed);

                    //se yesterday
                    if($isTime == 'Yesterday'){
                        $tempo_cumulato = $this->convertSeconds(($tempo_cumulato - $lastProgress));  //condizione che funziona nel caso
                    }else{
                        $tempo_cumulato = $this->convertSeconds(($tempo_cumulato));
                    }

                    /* MODIFICA ALDO $tempo_cumulato = $this->convertSeconds(($tempo_cumulato - $tempo_cumulatoSumittedTotale));*/

//                    $tempo_cumulato = $this->convertSeconds(($tempo_cumulato));

                    $prov['orario_logout'] = $singolo['orario'];
                    $prov['id_logout'] = $singolo['id'];
                    $prov['tempo_cumulato'] = $tempo_cumulato;
                    $prov['submitted'] = true;
                    $prov['enzo'] = 2;

//                    if($prov['userid']==2221) {
//                        //echo $prov['userid'].' Data:'.$prov['timecreated'].' Tempo cumulato:'.$tempo_cumulato.' other:'.$prov['other'].' objectid:'.$prov['objectid'].' Action: '.$prov['action']."<br>";
//                    }

                    array_push($finale, $prov);
                    $lezioni_terminate[$prov['lezione']] = $prov['objectid'];
                    $prov = null;
                    $viewed = 1;
                } else if ($singolo['action'] == "viewed" && $prov['action'] != "progress") {
                    if ($prec['action'] == "progress") {
                        $prov['orario_logout'] = $prec['orario'];
                        $prov['id_logout'] = $prec['id'];

                        $progress = $prec['other'];

                        if (@unserialize($progress) === false) {
                            $progress = json_decode($progress);
                        } else {
                            $progress = json_decode(unserialize($progress));
                        }

                        if (!is_null($progress)) {
                            @$tempo_cumulato = (int)$progress->progress;
                        } else {
                            $tempo_cumulato = 0;
                        }

//                        $progress = $prec['other'];
//                        $toReplace = ['{', '}', '"'];
//                        $progress = str_replace($toReplace, "", $progress);
//                        if ($progress != "") {
//                            $progress = explode(".", $progress);
//                            $progress = explode(":", $progress[0]);
//                            @$tempo_cumulato = (int) @$progress[1];
//                        } else
//                            $tempo_cumulato = 0;
                        $tempo_cumulato = $this->convertSeconds($tempo_cumulato);
                        $prov['tempo_cumulato'] = $tempo_cumulato;

                        $prov['enzo'] = 5;



//                        if($prov['id']==7115398) {
//                            echo $prov['userid'].' Data:'.$prov['timecreated'].' Tempo cumulato:'.$tempo_cumulato.' other:'.$prov['other'].' objectid:'.$prov['objectid'].' Action: '.$prov['action']."<br>";
//                            exit;
//                        }


                        if (!array_key_exists($prov['lezione'], $lezioni_terminate))
                            array_push($finale, $prov);
                        $viewed = 1;
                    } else {
                        if (array_key_exists($singolo['lezione'], $lezioni_terminate)) {
                            continue;
                        }
                    }
                    $prov = $singolo;
                    $prec = $prov;
                } else if ($prov['action'] == "progress") {
                    $prov = $singolo;
                    $prec = $prov;
                }
            } else {

//                if($prec['id']==7115397){
//                    print_r($singolo); //7115397
//                    print_r($prov); //7113935
//                    print_r($prec); //7113934
//                    print_r($lezioni_terminate);
//                    exit();
//                }

                if ($prec['action'] == "progress") {
                    $prov['orario_logout'] = $prec['orario'];
                    $prov['id_logout'] = $prec['id'];

                    $progress = $prec['other'];

                    if (@unserialize($progress) === false) {
                        $progress = json_decode($progress);
                    } else {
                        $progress = json_decode(unserialize($progress));
                    }

                    if (!is_null($progress)) {
                        @$tempo_cumulato = (int)$progress->progress;
                    } else {
                        $tempo_cumulato = 0;
                    }

//                    $progress = $prec['other'];
//                    $toReplace = ['{', '}', '"'];
//                    $progress = str_replace($toReplace, "", $progress);
//                    if ($progress != "") {
//                        $progress = explode(".", $progress);
//                        $progress = explode(":", $progress[0]);
//                        @$tempo_cumulato = (int) @$progress[1];
//                    } else
//                        $tempo_cumulato = 0;
                    $tempo_cumulato = $this->convertSeconds($tempo_cumulato);
                    $prov['tempo_cumulato'] = $tempo_cumulato;

                    $prov['enzo'] = 3;

//                    if($prov['id']==7115398) {
//                        echo $prov['userid'].' Data:'.$prov['timecreated'].' Tempo cumulato:'.$tempo_cumulato.' other:'.$prov['other'].' objectid:'.$prov['objectid'].' Action: '.$prov['action']."<br>";
//                        exit;
//                    }

                    if (!array_key_exists($prov['lezione'], $lezioni_terminate))
                        array_push($finale, $prov);
                }
                $viewed = 1;
                $lezioni_terminate = array();
                $prov = $singolo;
                $prec = $prov;
            }
        }

//        foreach($finale as $f){
//            if($f['userid'] == 10786 && $f['objectid'] == 376){
//                var_dump($f);
//            }
//        }
//
//        exit;

//        echo "qui\n";
//        var_dump($finale);exit();
        /*if ( $singolo['userid'] == 2122 ) {
            var_
        }*/
//exit();

        $prov = null;
        $new = array();
        $temp = array();
        foreach ($finale as $singolo) {
            if ($prov == null) {

//                echo $singolo['id'];
//                echo "---------------\n\n";

//                if($singolo['userid'] == 10786 && $singolo['timecreated'] == '09/06/2022'){
//                    echo "qui";
//                }

                $time_sing = explode(":", $singolo['tempo_cumulato']);
                $seconds_sing = $time_sing[0] * 3600 + $time_sing[1] * 60 + $time_sing[2];
                $temp[$singolo['lezione']] = $seconds_sing;
                $lastProgress = $this->getLastProgressNew($singolo['userid'], $singolo['timestamp'], $singolo['objectid'], $db);
                $tc = $seconds_sing - (int) $lastProgress;
                $singolo['diff'] = $this->convertSeconds($tc);
                array_push($new, $singolo);
                $prov = $singolo;
                continue;
            }
            $time_prov = explode(":", $prov['tempo_cumulato']);
            $time_sing = explode(":", $singolo['tempo_cumulato']);
            $seconds_prov = $time_prov[0] * 3600 + $time_prov[1] * 60 + $time_prov[2];
            $seconds_sing = $time_sing[0] * 3600 + $time_sing[1] * 60 + $time_sing[2];

            if ($prov['userid'] == $singolo['userid']) {

//                if($singolo['id'] == 9322066){
//                    var_dump($temp);
//                    var_dump(array_key_exists($singolo['lezione'], $temp));
//                    var_dump($singolo['lezione']);
//                    var_dump($seconds_sing);
//                    var_dump($temp[$singolo['lezione']]);
//                    var_dump(abs($seconds_sing - $temp[$singolo['lezione']]));
//                    exit;
//                }

//                if($singolo['userid'] == 10786 && $singolo['timecreated'] == '13/06/2022'){
//                    var_dump($temp);
//                    var_dump($singolo['lezione']);
//                    var_dump($seconds_sing >= $temp[$singolo['lezione']]);
//                    exit;
//                }

//                if($singolo['id'] == 7115398){
//                    var_dump($singolo['lezione']);
//                    var_dump(array_key_exists($singolo['lezione'], $temp));
//                    exit;
//                }

                /**
                 * nuove modifiche
                 */
                if (array_key_exists($singolo['lezione'], $temp)) {

//                    if($singolo['id'] == 7115398){
//                        var_dump($seconds_sing); //3621
//                        var_dump($temp[$singolo['lezione']]); //2115
//                        var_dump($singolo['submitted']); //true
//                        exit;
//                    }

//                    if($singolo['id'] == 7115398){
//                        $temp[$singolo['lezione']] = 3621;
//                    }

                    if ($seconds_sing >= $temp[$singolo['lezione']])  //TODO Da controllare
                        $diff = $seconds_sing - $temp[$singolo['lezione']];
                    else
                        $diff = 0; //$seconds_sing (come era prima)


//                    if($singolo['submitted'] == true){
//                        $diff = $seconds_sing;
//                    }

//                    $diff = abs($seconds_sing - $temp[$singolo['lezione']]);  //decommentare questo se non va in data 17/11/2022
                } else {

//                    if($singolo['userid'] == 6924 && $singolo['timecreated'] == '29/09/2022'){
//                        if (strpos($singolo['other'], "RESET") === FALSE){
//                            $lastProgress = $this->getLastProgress($singolo['userid'], $singolo['timestamp'], $singolo['objectid'], $db);
//                            $diff = $seconds_sing - $lastProgress;
//                            echo "non ho trovato reset \n";
//                            echo $seconds_sing . "\n";
//                            echo $lastProgress . "\n";
//                            echo abs($diff) . "\n";
//                        }else{
//                            echo "ho trovato reset \n";
//                        }
//
//                        echo "-------------\n";
//                    }

                    if (strpos($singolo['other'], "RESET") === FALSE) {
                        $lastProgress = $this->getLastProgressNew($singolo['userid'], $singolo['timestamp'], $singolo['objectid'], $db);
                    }else {
//                        //TODO da verificare aggiunta
//                        if (array_key_exists($singolo['lezione'], $temp)) {
//                            $lastProgress = $seconds_sing - $temp[$singolo['lezione']];
//                        }else{
//                            $lastProgress = 0;
//                        }
                        $lastProgress = 0;
                    }

//                    if($singolo['id'] == 7113935){
//                        var_dump($singolo);
//                        var_dump($prov);
//                        var_dump($temp);
//                        var_dump(array_key_exists($singolo['lezione'], $temp));
//                        var_dump($singolo['lezione']);
//                        var_dump($seconds_sing);
//                        var_dump($lastProgress);
//                        var_dump(abs($seconds_sing - $lastProgress));
//                        exit;
//                    }

//                    if($singolo['id'] == 7115398){
//                        var_dump($singolo['submitted']);
//                        exit;
//                    }
//
//                    if($singolo['submitted'] == false) {
//                        $diff = abs($seconds_sing - $lastProgress);
//                    }else{
//                        $diff = $seconds_sing;
//                    }

                    $diff = abs($seconds_sing - $lastProgress);
                }

                /**
                 * vecchio codice
                 */
//                if (array_key_exists($singolo['lezione'], $temp)) {
//                    if ($seconds_sing >= $temp[$singolo['lezione']])
//                        $diff = $seconds_sing - $temp[$singolo['lezione']];
//                    else
//                        $diff = 0;
//                } else {
//                    if (strpos($singolo['other'], "RESET") === FALSE)
//                        $lastProgress = $this->getLastProgress($singolo['userid'], $singolo['timestamp'], $singolo['objectid'], $db);
//                    else
//                        $lastProgress = 0;
//                    $diff = $seconds_sing - $lastProgress;
//                }

                $temp[$singolo['lezione']] = $seconds_sing;

                if($diff < 0){
                    $diff = $this->getLastProgressNew($singolo['userid'], $singolo['timestamp'], $singolo['objectid'], $db);
                }

//                if($singolo['submitted'] == false){
//                    $singolo['diff'] = $this->convertSeconds($diff);
//                }else{
//                    $singolo['diff'] = 0;
//                }

                $singolo['diff'] = $this->convertSeconds($diff);


                $singolo['syria'] = 1;

//                if($singolo['id'] == 7115398){
//                    var_dump($singolo['submitted']);
//                    var_dump($singolo['diff']);
//                    exit;
//                }

//                if ($singolo['userid'] == 6527 && $singolo['objectid'] == 57) {
//                    echo $lastProgress . "<br>";
//                    print_r($singolo);
//                    exit();
//                }
                array_push($new, $singolo);
            } else {
                $temp = array();
                $lastProgress = $this->getLastProgressNew($singolo['userid'], $singolo['timestamp'], $singolo['objectid'], $db);
                $tc = abs($seconds_sing - (int) $lastProgress);
                $singolo['diff'] = $this->convertSeconds($tc);
                $singolo['syria'] = 2;

//                if($singolo['userid'] == 10786 && $singolo['timecreated'] == '12/07/2022'){
//                    var_dump($temp);
//                    var_dump(array_key_exists($singolo['lezione'], $temp));
//                    var_dump($singolo['lezione']);
//                    var_dump($seconds_sing);
//                    var_dump($lastProgress);
//                    var_dump(abs($seconds_sing - $lastProgress));
//                    exit;
//                }

                array_push($new, $singolo);
                $prov = $singolo;
            }
        }

//        foreach($new as $f){
//            if($f['userid'] == 10786 && $f['objectid'] == 376){
//                var_dump($f);
//            }
//        }
//
//        exit;



        $new2 = array();
        foreach ($new as $singolo) {
            $singolo = json_decode(json_encode($singolo, true), true);
            if (isset($singolo))
                array_push($new2, $singolo);
        }


//        var_dump($new2);
//        exit;


        return $new2;
    }

    public function constructHvpFinal($array, $db)
    {
        $array = $this->sortArray($array);

//        var_dump($array);exit();

        if (isset($_GET['debug'])) {
            print_r($array);
            exit();
        }
        $finale = array();
        $prov = null;
        $prec = array();
        $tmp = array();
        $viewed = 0;
        $lezioni_terminate = array();

        $last = end($array);

//        var_dump($array); exit;

        //nuovo codice enzo
        $newArray = array();
        $arrControl = array();
        $arrControlFinal = array();
        $finale2 = array();

        $i = 0;
        foreach ($array as $key => $singolo) {

//            var_dump($singolo); exit;

//            if($singolo['timecreated'] == '16/12/2022' && $singolo['userid'] == 12677){
//                var_dump($singolo);
//            }

            //decommentare in caso di nuova funzione aggregate
            if($singolo['action'] == 'submitted'){
                $singolo['objectid'] = $this->getObjectId($singolo['contextinstanceid'], $db);
            }


            $checkTime = 0;

            $newArray = $singolo;

            $key_lezione = $singolo['userid'] . '-' . $singolo['objectid'];

//            if($key_lezione == '11312-299'){
//                var_dump($key_lezione); exit;
//            }

            list($giorno, $mese, $anno) = explode("/",$singolo['timecreated']);
            $timestamp_singolo = strtotime($anno.'-'.$mese.'-'.$giorno);

            if(isset($prec_time)) {
                $checkTime = $timestamp_singolo - $prec_time;
            }


            if (strpos($singolo['other'], 'RESET') !== FALSE) {
                $kk = $singolo['objectid'] . '-' . $singolo['userid'];
                //TODO controllo reset enzo
                $arrControlFinal[$singolo['objectid'] . '-' . $singolo['userid']][$singolo['objectid'] . $singolo['userid']]['last_progress_tmp'] = 0;
                unset($arrControlFinal[$kk]);
                unset($lezioni_terminate[$key_lezione]);
            }

            if ($singolo['action'] == "submitted") {

                if (!array_key_exists($key_lezione, $lezioni_terminate)) {

//                    if($singolo['userid'] == 11312){
//                        echo $singolo['id'] . "\n";
//                    }

//                    if($singolo['userid'] == 11312 && $singolo['id'] == 8055386){
//                        echo $array[$key-1]['id'] . "\n";
//                        var_dump($array[$key-1]['other']);
//                    }

                    $tempo_cumulato_first = $this->getTimeLesson($singolo['objectid'], $db);

//                    if($singolo['userid'] == 11312) {
//                        var_dump($array[$key - 1]['id']);
//                    }

                    //TODO controllo reset precedente
                    if (strpos($array[$key-1]['other'], 'RESET') !== FALSE) {

//                        if($singolo['userid'] == 11312){
//                            echo $singolo['id'] . "\n";
//                        }

                        $tempo_cumulato = $this->convertSeconds($tempo_cumulato_first);
                    }else {

//                        if($singolo['userid'] == 11312){
//                            echo $singolo['id'] . "\n";
//                        }

                        $lastProgress = $this->getLastProgress($singolo['userid'], $singolo['timestamp'], $singolo['objectid'], $db);
                        //decommentare in caso di nuova funzione aggregate
//                    $lastProgress = $singolo['last_progress'];

                        if (!$arrControlFinal[$singolo['objectid'] . '-' . $singolo['userid']][$singolo['objectid'] . $singolo['userid']]['last_progress_tmp']) {
                            $arrControlFinal[$singolo['objectid'] . '-' . $singolo['userid']][$singolo['objectid'] . $singolo['userid']]['last_progress_tmp'] = $lastProgress;
                        }

                        $tempo_cumulato_regresso = $arrControlFinal[$singolo['objectid'] . '-' . $singolo['userid']][$singolo['objectid'] . $singolo['userid'] . str_replace('/', '', $singolo['timecreated'])]['tempo_cumulato'];
                        if(strlen(trim($tempo_cumulato_regresso))){
                            $time_regresso = explode(":", $tempo_cumulato_regresso);
                            $seconds_regresso = $time_regresso[0] * 3600 + $time_regresso[1] * 60 + $time_regresso[2];
                        }else{
                            $seconds_regresso = 0;
                        }

                        if($arrControlFinal[$singolo['objectid'] . '-' . $singolo['userid']][$singolo['objectid'] . $singolo['userid']]['last_progress_tmp'] > $tempo_cumulato_first){
                            $tempo_cumulato_first = $arrControlFinal[$singolo['objectid'] . '-' . $singolo['userid']][$singolo['objectid'] . $singolo['userid']]['last_progress_tmp'];
                        }

                        $tempo_cumulato = $this->convertSeconds(($tempo_cumulato_first - $arrControlFinal[$singolo['objectid'] . '-' . $singolo['userid']][$singolo['objectid'] . $singolo['userid']]['last_progress_tmp']) + $seconds_regresso);
                    }

                    $newArray['orario_logout'] = $singolo['orario'];
                    $newArray['id_logout'] = $singolo['id'];
                    $newArray['tempo_cumulato'] = $tempo_cumulato;

                    $time_sing = explode(":", $tempo_cumulato);
                    $seconds_sing = $time_sing[0] * 3600 + $time_sing[1] * 60 + $time_sing[2];
                    $newArray['diff'] = 0;


                    $arrControlFinal[$singolo['objectid'] . '-' . $singolo['userid']][$singolo['objectid'] . $singolo['userid'] . str_replace('/', '', $singolo['timecreated'])] = $newArray;
                    $lezioni_terminate[$key_lezione] = $singolo['objectid'];
                }

            } elseif ($singolo['action'] == "progress") {

                $newArray['orario_logout'] = $singolo['orario'];
                $newArray['id_logout'] = $singolo['id'];
                $newArray['lezione_id'] = $singolo['objectid'];
                $progress = $singolo['other'];

                if (@unserialize($progress) === false) {
                    $progress = json_decode($progress);
                } else {
                    $progress = json_decode(unserialize($progress));
                }

                if (!is_null($progress)) {
                    @$tempo_cumulato = (int)$progress->progress;
                } else {
                    $tempo_cumulato = 0;
                }

                $tempo_cumulato = $this->convertSeconds($tempo_cumulato);

                $time_sing = explode(":", $tempo_cumulato);
                $seconds_sing = $time_sing[0] * 3600 + $time_sing[1] * 60 + $time_sing[2];
                $newArray['diff'] = 0;

                if (!array_key_exists($key_lezione, $lezioni_terminate)) {

                    $last_progres = $this->getLastProgressViewed($singolo['userid'], $singolo['timestamp'], $singolo['objectid'], $db);
                    //decommentare in caso di nuova funzione aggregate
//                    $last_progres = $singolo['last_viewed'];

                    if(!$arrControlFinal[$singolo['objectid'] . '-' . $singolo['userid']][$singolo['objectid'] . $singolo['userid']]['last_progress_tmp']){
                        $arrControlFinal[$singolo['objectid'] . '-' . $singolo['userid']][$singolo['objectid'] . $singolo['userid']]['last_progress_tmp'] = $last_progres;
                    }

                    if($seconds_sing < $arrControlFinal[$singolo['objectid'] . '-' . $singolo['userid']][$singolo['objectid'] . $singolo['userid']]['last_progress_tmp']){
                        $seconds_sing = $arrControlFinal[$singolo['objectid'] . '-' . $singolo['userid']][$singolo['objectid'] . $singolo['userid']]['last_progress_tmp'];
                        $diff = 0;
                    }else{
                        $diff = $seconds_sing - $arrControlFinal[$singolo['objectid'] . '-' . $singolo['userid']][$singolo['objectid'] . $singolo['userid']]['last_progress_tmp'];
                        $arrControlFinal[$singolo['objectid'] . '-' . $singolo['userid']][$singolo['objectid'] . $singolo['userid']]['last_progress_tmp'] = $seconds_sing;
//                        $diff = abs($seconds_sing - $new_value);
                    }

                    $tempo_cumulato_regresso = $arrControlFinal[$singolo['objectid'] . '-' . $singolo['userid']][$singolo['objectid'] . $singolo['userid'] . str_replace('/', '', $singolo['timecreated'])]['tempo_cumulato'];
                    if(strlen(trim($tempo_cumulato_regresso))){
                        $time_regresso = explode(":", $tempo_cumulato_regresso);
                        $seconds_regresso = $time_regresso[0] * 3600 + $time_regresso[1] * 60 + $time_regresso[2];
                    }else{
                        $seconds_regresso = 0;
                    }


                    $tot_diff = $diff + $seconds_regresso;

                    if($tot_diff < 0){
                        $tot_diff = 0;
                    }

                    $newArray['tempo_cumulato'] = $this->convertSeconds($tot_diff);

                    $arrControlFinal[$singolo['objectid'] . '-' . $singolo['userid']][$singolo['objectid'] . $singolo['userid'] . str_replace('/', '', $singolo['timecreated'])] = $newArray;

//                    if($singolo['id'] == 5220027){
//                        var_dump($arrControlFinal[$singolo['objectid'] . '-' . $singolo['userid']]);
//                        exit;
//                    }

                }

            }

            $newArray = array();
//            $lezioni_terminate = array();
            $prec_time = $timestamp_singolo;

            $i++;
        }

        $new = array();
        $new2 = array();

        foreach($arrControlFinal as $controlFinal){

            foreach($controlFinal as $control) {
                if(strlen(trim($control['orario_logout']))){
                    $time_orario = explode(":", $control['orario_logout']);
                    $tempo_orario = $time_orario[0] * 3600 + $time_orario[1] * 60 + $time_orario[2];
                }else{
                    $tempo_orario = 0;
                }

                $tempo_diff = 0;
                if ($control['diff'] != 0) {
                    $time_diff = explode(":", $control['diff']);
                    $tempo_diff = $time_diff[0] * 3600 + $time_diff[1] * 60 + $time_diff[2];
                }

                $orario_logout = ($tempo_orario + $tempo_diff);
                $control['orario_logout'] = $this->convertSeconds($orario_logout);

                array_push($finale2, $control);

            }

        }

        foreach ($finale2 as $singolo) {
            //TODO Controllo se esiste userid
            if (isset($singolo['userid'])) {
                $singolo = json_decode(json_encode($singolo, true), true);
                if (isset($singolo))
                    array_push($new2, $singolo);
            }
        }

//        var_dump($new2); exit;

        return $new2;
    }

    public function constructHvpReportTest($array, $db)
    {
        $array = $this->sortArray($array);

//        var_dump($array);exit();

        if (isset($_GET['debug'])) {
            print_r($array);
            exit();
        }
        $finale = array();
        $prov = null;
        $prec = array();
        $tmp = array();
        $viewed = 0;
        $lezioni_terminate = array();

        $last = end($array);

//        var_dump($array); exit;

        //nuovo codice enzo
        $newArray = array();
        $arrControl = array();
        $arrControlFinal = array();
        $finale2 = array();

//        var_dump($array); exit;

        $i = 0;
        foreach ($array as $singolo) {

            $checkTime = 0;

            $newArray = $singolo;

            list($giorno, $mese, $anno) = explode("/",$singolo['timecreated']);
            $timestamp_singolo = strtotime($anno.'-'.$mese.'-'.$giorno);

            if(isset($prec_time)) {
                $checkTime = $timestamp_singolo - $prec_time;
            }

//            var_dump($checkTime);

//            echo $prec_time . ' - ' . $timestamp_singolo . ($prec_time - $timestamp_singolo) . "\n";

//            var_dump($singolo);

//            if (strpos($singolo['other'], 'RESET') === FALSE) {

            if (strpos($singolo['other'], 'RESET') !== FALSE) {
                $kk = $singolo['objectid'] . '-' . $singolo['userid'];
//                    echo $kk . "\n";
//                    echo $singolo['timecreated'] . "\n";
//                    echo $singolo['id'] . "\n";
//                    echo $singolo['other'] . "\n\n";
                unset($arrControlFinal[$kk]);
            }

            if ($singolo['action'] == "submitted") {
//                var_dump($singolo); exit;
                $tempo_cumulato_first = $this->getTimeLesson($singolo['objectid'], $db);

//                    $lastProgress = $this->getLastProgress($singolo['userid'], $singolo['timestamp'], $singolo['objectid'], $db);
//                    $tempo_cumulato = $tempo_cumulato - (int)$lastProgress;

                $tempo_cumulato = $this->convertSeconds(($tempo_cumulato_first));

//                    $time_sing_orario = explode(":", $singolo['orario']);
//                    $seconds_sing_orario = $time_sing_orario[0] * 3600 + $time_sing_orario[1] * 60 + $time_sing_orario[2];
//                    $newArray['orario_logout'] = $this->convertSeconds($seconds_sing_orario + $tempo_cumulato_first);

                $newArray['orario_logout'] = $singolo['orario'];
                $newArray['id_logout'] = $singolo['id'];
                $newArray['tempo_cumulato'] = $tempo_cumulato;
//                array_push($finale2, $newArray);

                $time_sing = explode(":", $tempo_cumulato);
                $seconds_sing = $time_sing[0] * 3600 + $time_sing[1] * 60 + $time_sing[2];
                $newArray['diff'] = 0;

                $arrControlFinal[$singolo['objectid'] . '-' . $singolo['userid']][$singolo['objectid'] . $singolo['userid'] . str_replace('/', '', $singolo['timecreated'])] = $newArray;
//                    $arrControl[$singolo['objectid'] . $singolo['userid'] . str_replace('/', '', $singolo['timecreated'])] = $newArray;

//                    array_push($arrControlFinal, $arrControl);
//                    $arrControlFinal[$singolo['objectid'] . '-' . $singolo['userid']][] = $arrControl;

//                echo "terminate: " . $singolo['objectid'] . "\n";


                array_push($lezioni_terminate, $singolo['contextid']);

//                    $lezioni_terminate[$singolo['lezione']] = $singolo['objectid'];
//                $lezioni_terminate[$singolo['contextid']] = $singolo['contextid'];
//                var_dump($lezioni_terminate); exit;

//                    var_dump($checkTime);

            } elseif ($singolo['action'] == "progress") {

//                var_dump($singolo['action']);

//                $checkExists = array_intersect($singolo, $finale2);
//                $check = !empty($checkExists);
//
//                if($check){
//
//                }

                $newArray['orario_logout'] = $singolo['orario'];
                $newArray['id_logout'] = $singolo['id'];
                $newArray['lezione_id'] = $singolo['objectid'];

//                var_dump($singolo['action']);

                $progress = $singolo['other'];

                if (@unserialize($progress) === false) {
                    $progress = json_decode($progress);
                } else {
                    $progress = json_decode(unserialize($progress));
                }

                if (!is_null($progress)) {
                    @$tempo_cumulato = (int)$progress->progress;
                } else {
                    $tempo_cumulato = 0;
                }

                $tempo_cumulato = $this->convertSeconds($tempo_cumulato);

                $newArray['tempo_cumulato'] = $tempo_cumulato;

//                    if($singolo['id'] == 5017467){
//                        echo $tempo_cumulato;
//                        exit;
//                    }

//                    if($singolo['id'] == 6722832){
//                        var_dump($newArray['tempo_cumulato']);
//                        exit;
//                    }

                $time_sing = explode(":", $tempo_cumulato);
                $seconds_sing = $time_sing[0] * 3600 + $time_sing[1] * 60 + $time_sing[2];

//                    var_dump($checkTime);
//                    if($checkTime > 0){
//                        echo "enzo";
//                    }

                $newArray['diff'] = $this->convertSeconds($seconds_sing);

//                    if($singolo['objectid'] == 249){
//                        var_dump($lastProgress);
//                    }

//                var_dump($lezioni_terminate);

                if (!in_array($singolo['contextid'], $lezioni_terminate)) {

//                if (!array_key_exists($singolo['contextid'], $lezioni_terminate)) {

//                    array_push($finale2, $newArray);
//
//                    if (array_key_exists($singolo['objectid'], $arrControl)){
//                        $arrControl[$singolo['objectid']] = $finale2;
//                    }

//                        $arrControl[$singolo['objectid'] . $singolo['userid'] . str_replace('/', '', $singolo['timecreated'])] = $newArray;
//                        $arrControlFinal[$singolo['objectid'] . '-' . $singolo['userid']] = $arrControl;
                    $arrControlFinal[$singolo['objectid'] . '-' . $singolo['userid']][$singolo['objectid'] . $singolo['userid'] . str_replace('/', '', $singolo['timecreated'])] = $newArray;

//                        if($i == 2){
//                            var_dump($arrControlFinal);
//                            exit;
//                        }
                }

            } elseif ($singolo['action'] == "progress") {

                $tempo_cumulato = $arrControlFinal[$singolo['objectid'] . '-' . $singolo['userid']][$singolo['objectid'] . $singolo['userid'] . str_replace('/', '', $singolo['timecreated'])]['tempo_cumulato'];
//                echo "tempo_cumulato: " . $tempo_cumulato . "\n";
                $time_sing = explode(":", $tempo_cumulato);
                $seconds_sing = $time_sing[0] * 3600 + $time_sing[1] * 60 + $time_sing[2];

//                echo "tempo_cumulato_secondi: " . $seconds_sing . "\n";

                $progress_viewed = $singolo['other'];

                if (@unserialize($progress_viewed) === false) {
                    $progress_viewed = json_decode($progress_viewed);
                } else {
                    $progress_viewed = json_decode(unserialize($progress_viewed));
                }

                if (!is_null($progress_viewed)) {
                    @$tempo_cumulato_viewed = (int)$progress_viewed->progress;
//                    echo "tempo_cumulato_viewed: " . $tempo_cumulato_viewed . "\n";
                } else {
                    $tempo_cumulato_viewed = 0;
//                    echo "tempo_cumulato_viewed: " . $tempo_cumulato_viewed . "\n";
                }

//                echo "tempo_cumulato_viewed_secondi: " . $tempo_cumulato_viewed . "\n";
//                echo "tempo_cumulato_differenza: " . $tempo_cumulato . "\n";
//                echo "------------------\n\n";

//                var_dump($lezioni_terminate);

                if (!in_array($singolo['contextid'], $lezioni_terminate)) {
//                if (!array_key_exists($singolo['contextid'], $lezioni_terminate)) {
                    $tempo_cumulato = $tempo_cumulato_viewed - $seconds_sing;

//                    if($singolo['userid'] == 6925 && $singolo['timecreated'] == '13/10/2022'){
//                        var_dump($lezioni_terminate);
//                        echo $singolo['id'] . "\n";
//                        echo $singolo['lezione_id'] . "\n";
//                        echo $seconds_sing . "\n";
//                        echo $tempo_cumulato_viewed . "\n";
//                        echo $tempo_cumulato . "\n";
//                        echo "------------------\n\n";
//                    }

                    $arrControlFinal[$singolo['objectid'] . '-' . $singolo['userid']][$singolo['objectid'] . $singolo['userid'] . str_replace('/', '', $singolo['timecreated'])]['tempo_cumulato'] = $this->convertSeconds($tempo_cumulato);
                }

            }

//                var_dump($checkTime);

//            }else{
//
////                if($singolo['userid'] == 6924 && $singolo['objectid'] == 437) {
////                    echo "id: " . $singolo['id'] . "\n";
////                    echo "userid: " . $singolo['userid'] . "\n";
////                    echo "Nome: " . $singolo['name'] . "\n";
////                    echo "objectid: " . $singolo['objectid'] . "\n";
////                    echo "timecreated: " . $singolo['timecreated'] . "\n";
////                    echo "corso: " . $singolo['corso'] . "\n";
////                    echo "lezione: " . $singolo['lezione'] . "\n";
////                    echo "azione: " . $singolo['other'] . "\n";
////                    echo " -------------------- \n";
//////                    exit;
////                }
//
//                $kk = $singolo['objectid'] . '-' . $singolo['userid'];
////                echo "Key: " . $kk . "\n";
//                $cc = array_key_exists($kk, $arrControlFinal);
////                var_dump($cc);
//
////                var_dump($arrControlFinal);
//
////                unset($arrControl[$singolo['objectid'] . $singolo['userid'] . str_replace('/', '', $singolo['timecreated'])] );
//                unset($arrControlFinal[$kk]);
////                var_dump($arrControlFinal);
////                exit;
//            }

//            else{
//                foreach($arrControl as $reset){
//                    unset($arrControl[$key]);
//                }
//            }

//            else{
//                //elimino tutti i reset
//
//                $arrControl[$singolo['objectid'] . $singolo['userid']
//
//                foreach ($arrControl as $key => $elem) {
//
//                    if ($elem['userid'] == $singolo['userid'] && $elem['objectid'] == $singolo['objectid']) {
//
//                        unset($finale[$key]);
//                        unset($lezioni_terminate[$singolo['lezione']]);
//                    }
//                }
//            }


            $newArray = array();
            $prec_time = $timestamp_singolo;

            $i++;
        }



//        exit;
//
//        print_r(array_keys($arrControlFinal)); exit;
//
//        var_dump($arrControlFinal); exit;

//        end nuovo codice

        $new = array();
        $new2 = array();

        foreach($arrControlFinal as $controlFinal){

            foreach($controlFinal as $control) {

//                $time_sing = explode(":", $control['tempo_cumulato']);
//                $tempo_cumulato = $time_sing[0] * 3600 + $time_sing[1] * 60 + $time_sing[2];
//
//                $lastProgress = $this->getLastProgressNew($control['userid'], $control['timestamp'], $control['objectid'], $db);
//
//                $tempo_cumulato = $tempo_cumulato - $lastProgress;
//
//                if ($tempo_cumulato < 0) {
//                    $control['tempo_cumulato'] = $control['diff'];
//                } else {
//                    $control['tempo_cumulato'] = $this->convertSeconds($tempo_cumulato);
//                }

//                if($control['id'] == 5017467){
//                    echo $control['tempo_cumulato'];
//                    exit;
//                }

                $time_orario = explode(":", $control['orario_logout']);
                $tempo_orario = $time_orario[0] * 3600 + $time_orario[1] * 60 + $time_orario[2];

                $tempo_diff = 0;
                if ($control['diff'] != 0) {
                    $time_diff = explode(":", $control['diff']);
                    $tempo_diff = $time_diff[0] * 3600 + $time_diff[1] * 60 + $time_diff[2];
                }

                $orario_logout = ($tempo_orario + $tempo_diff);
                $control['orario_logout'] = $this->convertSeconds($orario_logout);

                array_push($finale2, $control);

            }

        }

//        var_dump($finale2);
//        exit;

        foreach ($finale2 as $singolo) {
            $singolo = json_decode(json_encode($singolo, true), true);
//            echo $i . "<br />";
//            print_r($singolo);
            if (isset($singolo))
                array_push($new2, $singolo);

//            $i++;
        }

//        var_dump($finale2);
//
//        exit;

//        var_dump($finale2);
//        exit;

//        var_dump($new2); exit;

        return $new2;
    }

    public function constructHvpReport($array, $db){
        $array = $this->sortArray($array);

//        var_dump($array);exit();

        if (isset($_GET['debug'])) {
            print_r($array);
            exit();
        }
        $finale = array();
        $prov = null;
        $prec = array();
        $tmp = array();
        $viewed = 0;
        $lezioni_terminate = array();

        $last = end($array);

//        var_dump($array); exit;

        //nuovo codice enzo
        $newArray = array();
        $arrControl = array();
        $arrControlFinal = array();
        $finale2 = array();

//        var_dump($array); exit;

        $i = 0;
        foreach ($array as $singolo) {

            $checkTime = 0;

            $newArray = $singolo;

            list($giorno, $mese, $anno) = explode("/",$singolo['timecreated']);
            $timestamp_singolo = strtotime($anno.'-'.$mese.'-'.$giorno);

            if(isset($prec_time)) {
                $checkTime = $timestamp_singolo - $prec_time;
            }

//            var_dump($checkTime);

//            echo $prec_time . ' - ' . $timestamp_singolo . ($prec_time - $timestamp_singolo) . "\n";

//            var_dump($singolo);

//            if (strpos($singolo['other'], 'RESET') === FALSE) {

            if (strpos($singolo['other'], 'RESET') !== FALSE) {
                $kk = $singolo['objectid'] . '-' . $singolo['userid'];
//                    echo $kk . "\n";
//                    echo $singolo['timecreated'] . "\n";
//                    echo $singolo['id'] . "\n";
//                    echo $singolo['other'] . "\n\n";
                unset($arrControlFinal[$kk]);
            }

            if ($singolo['action'] == "submitted") {
                $tempo_cumulato_first = $this->getTimeLesson($singolo['objectid'], $db);

//                    $lastProgress = $this->getLastProgress($singolo['userid'], $singolo['timestamp'], $singolo['objectid'], $db);
//                    $tempo_cumulato = $tempo_cumulato - (int)$lastProgress;

                $tempo_cumulato = $this->convertSeconds(($tempo_cumulato_first));

//                    $time_sing_orario = explode(":", $singolo['orario']);
//                    $seconds_sing_orario = $time_sing_orario[0] * 3600 + $time_sing_orario[1] * 60 + $time_sing_orario[2];
//                    $newArray['orario_logout'] = $this->convertSeconds($seconds_sing_orario + $tempo_cumulato_first);

                $newArray['orario_logout'] = $singolo['orario'];
                $newArray['id_logout'] = $singolo['id'];
                $newArray['tempo_cumulato'] = $tempo_cumulato;
//                array_push($finale2, $newArray);

                $time_sing = explode(":", $tempo_cumulato);
                $seconds_sing = $time_sing[0] * 3600 + $time_sing[1] * 60 + $time_sing[2];
                $newArray['diff'] = 0;

                $arrControlFinal[$singolo['objectid'] . '-' . $singolo['userid']][$singolo['objectid'] . $singolo['userid'] . str_replace('/', '', $singolo['timecreated'])] = $newArray;
//                    $arrControl[$singolo['objectid'] . $singolo['userid'] . str_replace('/', '', $singolo['timecreated'])] = $newArray;

//                    array_push($arrControlFinal, $arrControl);
//                    $arrControlFinal[$singolo['objectid'] . '-' . $singolo['userid']][] = $arrControl;

//                    $lezioni_terminate[$singolo['lezione']] = $singolo['objectid'];
                $lezioni_terminate[$singolo['lezione_id']] = $singolo['objectid'];

//                    var_dump($checkTime);

            } elseif ($singolo['action'] == "progress") {

//                $checkExists = array_intersect($singolo, $finale2);
//                $check = !empty($checkExists);
//
//                if($check){
//
//                }

                $newArray['orario_logout'] = $singolo['orario'];
                $newArray['id_logout'] = $singolo['id'];
                $newArray['lezione_id'] = $singolo['objectid'];

//                var_dump($singolo['lezione']);

                $progress = $singolo['other'];

                if (@unserialize($progress) === false) {
                    $progress = json_decode($progress);
                } else {
                    $progress = json_decode(unserialize($progress));
                }

                if (!is_null($progress)) {
                    @$tempo_cumulato = (int)$progress->progress;
                } else {
                    $tempo_cumulato = 0;
                }

                $tempo_cumulato = $this->convertSeconds($tempo_cumulato);

                $newArray['tempo_cumulato'] = $tempo_cumulato;

//                    if($singolo['id'] == 5017467){
//                        echo $tempo_cumulato;
//                        exit;
//                    }

//                    if($singolo['id'] == 6722832){
//                        var_dump($newArray['tempo_cumulato']);
//                        exit;
//                    }

                $time_sing = explode(":", $tempo_cumulato);
                $seconds_sing = $time_sing[0] * 3600 + $time_sing[1] * 60 + $time_sing[2];

//                    var_dump($checkTime);
//                    if($checkTime > 0){
//                        echo "enzo";
//                    }

                $newArray['diff'] = $this->convertSeconds($seconds_sing);

//                    if($singolo['objectid'] == 249){
//                        var_dump($lastProgress);
//                    }


                if (!array_key_exists($singolo['lezione_id'], $lezioni_terminate)) {

//                    array_push($finale2, $newArray);
//
//                    if (array_key_exists($singolo['objectid'], $arrControl)){
//                        $arrControl[$singolo['objectid']] = $finale2;
//                    }

//                        $arrControl[$singolo['objectid'] . $singolo['userid'] . str_replace('/', '', $singolo['timecreated'])] = $newArray;
//                        $arrControlFinal[$singolo['objectid'] . '-' . $singolo['userid']] = $arrControl;
                    $arrControlFinal[$singolo['objectid'] . '-' . $singolo['userid']][$singolo['objectid'] . $singolo['userid'] . str_replace('/', '', $singolo['timecreated'])] = $newArray;

//                        if($i == 2){
//                            var_dump($arrControlFinal);
//                            exit;
//                        }
                }

            }

//                var_dump($checkTime);

//            }else{
//
////                if($singolo['userid'] == 6924 && $singolo['objectid'] == 437) {
////                    echo "id: " . $singolo['id'] . "\n";
////                    echo "userid: " . $singolo['userid'] . "\n";
////                    echo "Nome: " . $singolo['name'] . "\n";
////                    echo "objectid: " . $singolo['objectid'] . "\n";
////                    echo "timecreated: " . $singolo['timecreated'] . "\n";
////                    echo "corso: " . $singolo['corso'] . "\n";
////                    echo "lezione: " . $singolo['lezione'] . "\n";
////                    echo "azione: " . $singolo['other'] . "\n";
////                    echo " -------------------- \n";
//////                    exit;
////                }
//
//                $kk = $singolo['objectid'] . '-' . $singolo['userid'];
////                echo "Key: " . $kk . "\n";
//                $cc = array_key_exists($kk, $arrControlFinal);
////                var_dump($cc);
//
////                var_dump($arrControlFinal);
//
////                unset($arrControl[$singolo['objectid'] . $singolo['userid'] . str_replace('/', '', $singolo['timecreated'])] );
//                unset($arrControlFinal[$kk]);
////                var_dump($arrControlFinal);
////                exit;
//            }

//            else{
//                foreach($arrControl as $reset){
//                    unset($arrControl[$key]);
//                }
//            }

//            else{
//                //elimino tutti i reset
//
//                $arrControl[$singolo['objectid'] . $singolo['userid']
//
//                foreach ($arrControl as $key => $elem) {
//
//                    if ($elem['userid'] == $singolo['userid'] && $elem['objectid'] == $singolo['objectid']) {
//
//                        unset($finale[$key]);
//                        unset($lezioni_terminate[$singolo['lezione']]);
//                    }
//                }
//            }


            $newArray = array();
            $lezioni_terminate = array();
            $prec_time = $timestamp_singolo;

            $i++;
        }

//        exit;
//
//        print_r(array_keys($arrControlFinal)); exit;
//
//        var_dump($arrControlFinal); exit;

//        end nuovo codice

        $new = array();
        $new2 = array();

        foreach($arrControlFinal as $controlFinal){

            foreach($controlFinal as $control) {

//                $time_sing = explode(":", $control['tempo_cumulato']);
//                $tempo_cumulato = $time_sing[0] * 3600 + $time_sing[1] * 60 + $time_sing[2];
//
//                $lastProgress = $this->getLastProgressNew($control['userid'], $control['timestamp'], $control['objectid'], $db);
//
//                $tempo_cumulato = $tempo_cumulato - $lastProgress;
//
//                if ($tempo_cumulato < 0) {
//                    $control['tempo_cumulato'] = $control['diff'];
//                } else {
//                    $control['tempo_cumulato'] = $this->convertSeconds($tempo_cumulato);
//                }

//                if($control['id'] == 5017467){
//                    echo $control['tempo_cumulato'];
//                    exit;
//                }

                $time_orario = explode(":", $control['orario_logout']);
                $tempo_orario = $time_orario[0] * 3600 + $time_orario[1] * 60 + $time_orario[2];

                $tempo_diff = 0;
                if ($control['diff'] != 0) {
                    $time_diff = explode(":", $control['diff']);
                    $tempo_diff = $time_diff[0] * 3600 + $time_diff[1] * 60 + $time_diff[2];
                }

                $orario_logout = ($tempo_orario + $tempo_diff);
                $control['orario_logout'] = $this->convertSeconds($orario_logout);

                array_push($finale2, $control);

            }

        }

//        var_dump($finale2);
//        exit;

        foreach ($finale2 as $singolo) {
            $singolo = json_decode(json_encode($singolo, true), true);
//            echo $i . "<br />";
//            print_r($singolo);
            if (isset($singolo))
                array_push($new2, $singolo);

//            $i++;
        }

//        var_dump($finale2);
//
//        exit;

//        var_dump($finale2);
//        exit;

//        var_dump($new2); exit;

        return $new2;
    }

    public function constructHvpReportPietro($array, $db){
        //Ordinamento dell'array
        $array = $this->sortArray($array);

        //Inizializzazione delle variabili
        $record      = array();
        $return_data = array();

        //Ciclo i dati ricevuti dal DB per consolidare i record
        foreach ($array as $item) {
            $item['start_lesson_time']   = false;
            $item['end_lesson_time']     = false;
            $item['current_lesson_time'] = false;
            $item['completed_lesson']    = false;

            if($item['action'] == 'viewed'){
                $index = isset($record[$item['contextid']]) ? (count($record[$item['contextid']])+1) : 1;
                if(!isset($record[$item['contextid']][$index])) $record[$item['contextid']][$index] = $item;

                //Inizio visualizzazione lezione corso
                $record[$item['contextid']][$index]['start_lesson_time'] = $item['orario'];
                $record[$item['contextid']][$index]['start_lesson_date'] = $item['timecreated'];
            }
            if($item['action'] == 'progress'){
                $progress = $item['other'];
                if (@unserialize($progress) === false) {
                    $progress = json_decode($progress);
                } else {
                    $progress = json_decode(unserialize($progress));
                }
                if (!is_null($progress)) {
                    @$work_time = (int)$progress->progress;
                } else {
                    $work_time = 0;
                }

                //Fine visualizzazione lezione corso
                $record[$item['contextid']][$index]['end_lesson_time']     = $item['orario'];
                $record[$item['contextid']][$index]['end_lesson_date']     = $item['timecreated'];
                $record[$item['contextid']][$index]['current_lesson_time'] = $work_time;
            }
            if($item['action'] == 'submitted'){
                //Completamento lezione corso
                $record[$item['contextid']][$index]['end_lesson_time']     = $item['orario'];
                $record[$item['contextid']][$index]['end_lesson_date']     = $item['timecreated'];
                $record[$item['contextid']][$index]['current_lesson_time'] = $work_time;
                $record[$item['contextid']][$index]['completed_lesson']    = true;
            }
        }

        //Calcolo entrata, uscita e durata
        if(!empty($record)) {
            foreach ($record as $key => $items) {
                foreach ($items as $item) {
                    if(isset($item['userid'])){
                        if(!$item['end_lesson_time'] || $item['start_lesson_date'] == $item['end_lesson_date']){
                            if(!$item['end_lesson_time']){
                                $item['end_lesson_time'] = $item['start_lesson_time'];
                                $item['diff'] = '00:00:00';
                            }else{
                                $start_lesson = new DateTimeImmutable($item['start_lesson_time']);
                                $end_lesson   = new DateTimeImmutable($item['end_lesson_time']);
                                $interval     = $start_lesson->diff($end_lesson);
                                $item['diff'] = $interval->format("%H:%I:%S");
                            }
                            $item['orario']         = $item['start_lesson_time'];
                            $item['orario_logout']  = $item['end_lesson_time'];
                            $item['tempo_cumulato'] = $this->convertSeconds($item['current_lesson_time']);
                            array_push($return_data, $item);
                        }else{
                            if(!$item['end_lesson_time']) $item['end_lesson_time'] = $item['start_lesson_time'];
                            //ITEM TO 23:59:00
                            $item['tmp_end_lesson_time'] = date('23:59:59', strtotime($item['start_lesson_time']));
                            $start_lesson = new DateTimeImmutable($item['start_lesson_time']);
                            $end_lesson   = new DateTimeImmutable($item['tmp_end_lesson_time']);
                            $interval     = $start_lesson->diff($end_lesson);
                            $item['diff'] = $interval->format("%H:%I:%S");

                            $item['orario']         = $item['start_lesson_time'];
                            $item['orario_logout']  = $item['tmp_end_lesson_time'];
                            $item['tempo_cumulato'] = '00:00:00';
                            array_push($return_data, $item);

                            //ITEM FROM 00:00:00
                            $item['timecreated'] = $item['end_lesson_date'];
                            $item['tmp_start_lesson_time'] = date('00:00:00', strtotime($item['end_lesson_time']));
                            $start_lesson = new DateTimeImmutable($item['tmp_start_lesson_time']);
                            $end_lesson   = new DateTimeImmutable($item['end_lesson_time']);
                            $interval     = $start_lesson->diff($end_lesson);
                            $item['diff'] = $interval->format("%H:%I:%S");

                            $item['orario']         = $item['tmp_start_lesson_time'];
                            $item['orario_logout']  = $item['end_lesson_time'];
                            $item['tempo_cumulato'] = $this->convertSeconds($item['current_lesson_time']);
                            array_push($return_data, $item);
                        }
                    }
                }
            }
        }
        return $return_data;
    }

    public function constructSummaryHvpReportPietro($array, $db){
        //Ordinamento dell'array
        $array = $this->sortArray($array);

        //Inizializzazione delle variabili
        $record      = array();
        $return_data = array();

        //Ciclo i dati ricevuti dal DB per consolidare i record
        $progressive_lesson = array();
        foreach ($array as $item){
            $k1 = $item['userid'];
            $k2 = $item['courseid'];
            $k3 = $item['timecreated'];
            $k4 = $item['contextid'];

            if(!isset($progressive_lesson[$k1]))                                               $progressive_lesson[$k1]                                               = array('data' => $item, 'courses' => array());
            if(!isset($progressive_lesson[$k1]['courses'][$k2]))                               $progressive_lesson[$k1]['courses'][$k2]                               = array('data' => $item, 'dates'   => array());
            if(!isset($progressive_lesson[$k1]['courses'][$k2]['dates'][$k3]))                 $progressive_lesson[$k1]['courses'][$k2]['dates'][$k3]                 = array('data' => $item, 'lessons' => array());
            if(!isset($progressive_lesson[$k1]['courses'][$k2]['dates'][$k3]['lessons'][$k4])) $progressive_lesson[$k1]['courses'][$k2]['dates'][$k3]['lessons'][$k4] = 0;

            $work_time = 0;
            $progress  = $item['other'];
            if (@unserialize($progress) === false) {
                $progress = json_decode($progress);
            } else {
                $progress = json_decode(unserialize($progress));
            }
            if(!is_null($progress)) @$work_time = (int)$progress->progress;
            if($work_time > $progressive_lesson[$k1]['courses'][$k2]['dates'][$k3]['lessons'][$k4]) $progressive_lesson[$k1]['courses'][$k2]['dates'][$k3]['lessons'][$k4] = $work_time;
        }

        $progressive_user_lesson = array();
        $return_users            = array();
        $return_courses          = array();
        foreach($progressive_lesson as $user_id => $user){
            if(!isset($progressive_user_lesson[$user_id])) $progressive_user_lesson[$user_id] = array();
            if(!isset($return_users[$user_id]))            $return_users[$user_id]            = $user['data'];
            foreach($user['courses'] as $course_id => $course){
                if(!isset($progressive_user_lesson[$user_id][$course_id])) $progressive_user_lesson[$user_id][$course_id] = array();
                if(!isset($return_courses[$course_id]))                    $return_courses[$course_id]                    = $course['data'];
                foreach($course['dates'] as $date){
                    if(!isset($progressive_user_lesson[$user_id][$course_id]['total_time'])) $progressive_user_lesson[$user_id][$course_id]['total'] = 0;
                    foreach($date['lessons'] as $lesson_id => $lesson_work_time){
                        if(!isset($progressive_user_lesson[$user_id][$course_id][$lesson_id])) $progressive_user_lesson[$user_id][$course_id][$lesson_id] = 0;
                        $progressive_user_lesson[$user_id][$course_id]['total_time'] += ($lesson_work_time - $progressive_user_lesson[$user_id][$course_id][$lesson_id]);
                        if($lesson_work_time > $progressive_user_lesson[$user_id][$course_id][$lesson_id]) $progressive_user_lesson[$user_id][$course_id][$lesson_id] = $lesson_work_time;
                    }
                }
            }
        }

        $return_data             = array();
        foreach($progressive_user_lesson as $user_id => $users){
            $item                 = array();
            $item['userid']       = $return_users[$user_id]['userid'];
            $item['username']     = $return_users[$user_id]['username'];
            $item['name']         = $return_users[$user_id]['name'];
            $item['cf']           = $return_users[$user_id]['cf'];
            $item['email']        = $return_users[$user_id]['email'];
            $item['data_nascita'] = $return_users[$user_id]['data_nascita'];
            $item['citta']        = $return_users[$user_id]['citta'];
            $item['phone']        = $return_users[$user_id]['phone'];
            foreach($users as $course_id => $courses){
                $item['corso']     = $return_courses[$course_id]['corso'];
                $item['total_time'] = $this->convertSeconds($courses['total_time']);
                $return_data[] = $item;
            }
        }

        return $return_data;
    }

    public function constructHvpReportConEnrico($array, $db) {
        $array = $this->sortArray($array);

        //var_dump($array);exit();

        if (isset($_GET['debug'])) {
            print_r($array);
            exit();
        }
        $finale = array();
        $prov = null;
        $prec = array();
        $tmp = array();
        $viewed = 0;
        $lezioni_terminate = array();
        $entrato = false;
        $last= end($array);

        foreach ($array as $singolo) {

            if (strpos($singolo['other'], "RESET") !== FALSE) {
                //continue;
                foreach ($finale as $key => $elem) {
                    if ($elem['userid'] == $singolo['userid'] && $elem['objectid'] == $singolo['objectid']) {

                        unset($finale[$key]);
                        unset($lezioni_terminate[$singolo['lezione']]);
                        $entrato = true;
                    }
                }
                if ($entrato == false)
                    continue;
            }

            if ($prov == null) {
                if (isset($prec['userid']) && $singolo['userid'] != $prec['userid'])
                    $lezioni_terminate = array();
                $prov = $singolo;
                $prec = $prov;
                continue;
            }

            if ($prov['userid'] == $singolo['userid'] && $prov['timecreated'] == $singolo['timecreated']) {//var_dump($prov);var_dump($singolo);exit('aaaa');
                /*if($singolo['userid']==2122 && $prov['objectid']==969){
                        print_r($singolo);
                        print_r($prov);
                        print_r($lezioni_terminate);
                        exit();
                    }*/
                if ($prov['action'] == 'viewed' && $singolo['action'] == 'progress') {
                    if (count($array) == 2 || $singolo==$last) {
                        $prov['orario_logout'] = $singolo['orario'];
                        $prov['id_logout'] = $singolo['id'];

                        $progress = $singolo['other'];

                        if(@unserialize($progress) === false){
                            $progress = json_decode($progress);
                        }else{
                            $progress = json_decode(unserialize($progress));
                        }

                        if(!is_null($progress->progress)){
                            @$tempo_cumulato = (int)$progress->progress;
                        }else{
                            $tempo_cumulato = 0;
                        }

//                        $progress = $singolo['other'];
//                        $toReplace = ['{', '}', '"'];
//                        $progress = str_replace($toReplace, "", $progress);
//
//                        if ($progress != "") {
//                            $progress = explode(".", $progress);
//                            $progress = explode(":", $progress[0]);
//                            @$tempo_cumulato = (int) @$progress[1];
//                        } else
//                            $tempo_cumulato = 0;
                        $tempo_cumulato = $this->convertSeconds($tempo_cumulato);

                        if($prov['userid']==2221) {
                            //echo $prov['userid'].' Data:'.$prov['timecreated'].' Tempo cumulato:'.$tempo_cumulato.' other:'.$prov['other'].' objectid:'.$prov['objectid'].' Action: '.$prov['action']."<br>";
                        }

                        $prov['tempo_cumulato'] = $tempo_cumulato;
                        array_push($finale, $prov);
                        break;
                    }
                    $prec = $singolo;
                    $viewed = 1;
                } else if ($singolo['action'] == "submitted") {

                    /* MODIFICA ALDO
                    $tempo_cumulatoSumittedTotale = $this->getLastProgress($prov['userid'], $prov['timecreated'], $prov['objectid'], $db);

                    */
                    $tempo_cumulato = $this->getTimeLesson($prov['objectid'], $db);

                    /* MODIFICA ALDO $tempo_cumulato = $this->convertSeconds(($tempo_cumulato - $tempo_cumulatoSumittedTotale));*/

                    $tempo_cumulato = $this->convertSeconds(($tempo_cumulato));

                    $prov['orario_logout'] = $singolo['orario'];
                    $prov['id_logout'] = $singolo['id'];
                    $prov['tempo_cumulato'] = $tempo_cumulato;

//                    if($prov['userid']==6924 && $prov['objectid'] == 3291) {
//                        echo $prov['userid'].' Data:'.$prov['timecreated'].' Tempo cumulato:'.$tempo_cumulato.' other:'.$prov['other'].' objectid:'.$prov['objectid'].' Action: '.$prov['action']."<br>";
//                        exit;
//                    }

//                    if($prov['userid']==2221) {
//                        //echo $prov['userid'].' Data:'.$prov['timecreated'].' Tempo cumulato:'.$tempo_cumulato.' other:'.$prov['other'].' objectid:'.$prov['objectid'].' Action: '.$prov['action']."<br>";
//                    }

//                    array_push($finale, $prov);
                    $lezioni_terminate[$prov['lezione']] = $prov['objectid'];
                    if (!array_key_exists($prov['lezione'], $lezioni_terminate))
                        array_push($finale, $prov);
                    $prov = null;
                    $viewed = 1;
                } else if ($singolo['action'] == "viewed" && $prov['action'] != "progress") {
                    if ($prec['action'] == "progress") {
                        $prov['orario_logout'] = $prec['orario'];
                        $prov['id_logout'] = $prec['id'];

                        $progress = $prec['other'];

                        if(@unserialize($progress) === false){
                            $progress = json_decode($progress);
                        }else{
                            $progress = json_decode(unserialize($progress));
                        }

                        if(!is_null($progress->progress)){
                            @$tempo_cumulato = (int)$progress->progress;
                        }else{
                            $tempo_cumulato = 0;
                        }

//                        $progress = $prec['other'];
//                        $toReplace = ['{', '}', '"'];
//                        $progress = str_replace($toReplace, "", $progress);
//                        if ($progress != "") {
//                            $progress = explode(".", $progress);
//                            $progress = explode(":", $progress[0]);
//                            @$tempo_cumulato = (int) @$progress[1];
//                        } else
//                            $tempo_cumulato = 0;
                        $tempo_cumulato = $this->convertSeconds($tempo_cumulato);
                        $prov['tempo_cumulato'] = $tempo_cumulato;

//                        if($prov['userid']==6924 && $prov['objectid'] == 3291) {
//                            echo $prov['userid'].' Data:'.$prov['timecreated'].' Tempo cumulato:'.$tempo_cumulato.' other:'.$prov['other'].' objectid:'.$prov['objectid'].' Action: '.$prov['action']."<br>";
//                            exit;
//                        }


                        if (!array_key_exists($prov['lezione'], $lezioni_terminate))
                            array_push($finale, $prov);
                        $viewed = 1;
                    } else {
                        if (array_key_exists($singolo['lezione'], $lezioni_terminate)) {
                            continue;
                        }
                    }
                    $prov = $singolo;
                    $prec = $prov;
                } else if ($prov['action'] == "progress") {
                    $prov = $singolo;
                    $prec = $prov;
                }
            } else {

                if ($prec['action'] == "progress") {
                    $prov['orario_logout'] = $prec['orario'];
                    $prov['id_logout'] = $prec['id'];

                    $progress = $prec['other'];

                    if(@unserialize($progress) === false){
                        $progress = json_decode($progress);
                    }else{
                        $progress = json_decode(unserialize($progress));
                    }

                    if(!is_null($progress->progress)){
                        @$tempo_cumulato = (int)$progress->progress;
                    }else{
                        $tempo_cumulato = 0;
                    }

//                    $progress = $prec['other'];
//                    $toReplace = ['{', '}', '"'];
//                    $progress = str_replace($toReplace, "", $progress);
//                    if ($progress != "") {
//                        $progress = explode(".", $progress);
//                        $progress = explode(":", $progress[0]);
//                        @$tempo_cumulato = (int) @$progress[1];
//                    } else
//                        $tempo_cumulato = 0;
                    $tempo_cumulato = $this->convertSeconds($tempo_cumulato);
                    $prov['tempo_cumulato'] = $tempo_cumulato;

                    if($prov['userid']==2221) {
                        //echo $prov['userid'].' Data:'.$prov['timecreated'].' Tempo cumulato:'.$tempo_cumulato.' other:'.$prov['other'].' objectid:'.$prov['objectid'].' Action: '.$prov['action']."<br>";
                    }

                    if (!array_key_exists($prov['lezione'], $lezioni_terminate))
                        array_push($finale, $prov);
                }
                $viewed = 1;
                $lezioni_terminate = array();
                $prov = $singolo;
                $prec = $prov;
            }
        }
        //var_dump($finale);exit();
        /*if ( $singolo['userid'] == 2122 ) {
            var_
        }*/
//exit();

        $prov = null;
        $new = array();
        $temp = array();
        foreach ($finale as $singolo) {
            if ($prov == null) {
                $time_sing = explode(":", $singolo['tempo_cumulato']);
                $seconds_sing = $time_sing[0] * 3600 + $time_sing[1] * 60 + $time_sing[2];
                $temp[$singolo['lezione']] = $seconds_sing;
                $lastProgress = $this->getLastProgress($singolo['userid'], $singolo['timestamp'], $singolo['objectid'], $db);
                $tc = $seconds_sing - (int) $lastProgress;
                $singolo['diff'] = $this->convertSeconds($tc);
                array_push($new, $singolo);
                $prov = $singolo;
                continue;
            }
            $time_prov = explode(":", $prov['tempo_cumulato']);
            $time_sing = explode(":", $singolo['tempo_cumulato']);
            $seconds_prov = $time_prov[0] * 3600 + $time_prov[1] * 60 + $time_prov[2];
            $seconds_sing = $time_sing[0] * 3600 + $time_sing[1] * 60 + $time_sing[2];

            if ($prov['userid'] == $singolo['userid']) {
                if (array_key_exists($singolo['lezione'], $temp)) {
                    if ($seconds_sing >= $temp[$singolo['lezione']])
                        $diff = $seconds_sing - $temp[$singolo['lezione']];
                    else
                        $diff = 0;
                } else {
                    if (strpos($singolo['other'], "RESET") === FALSE)
                        $lastProgress = $this->getLastProgress($singolo['userid'], $singolo['timestamp'], $singolo['objectid'], $db);
                    else
                        $lastProgress = 0;
                    $diff = $seconds_sing - $lastProgress;
                }
                $temp[$singolo['lezione']] = $seconds_sing;

                $singolo['diff'] = $this->convertSeconds($diff);
//                if ($singolo['userid'] == 6527 && $singolo['objectid'] == 57) {
//                    echo $lastProgress . "<br>";
//                    print_r($singolo);
//                    exit();
//                }
                array_push($new, $singolo);
            } else {
                $temp = array();
                $lastProgress = $this->getLastProgress($singolo['userid'], $singolo['timestamp'], $singolo['objectid'], $db);
                $tc = $seconds_sing - (int) $lastProgress;
                $singolo['diff'] = $this->convertSeconds($tc);
                array_push($new, $singolo);
                $prov = $singolo;
            }
        }



        $new2 = array();
        foreach ($new as $singolo) {
            $singolo = json_decode(json_encode($singolo, true), true);
            if (isset($singolo))
                array_push($new2, $singolo);
        }



        return $new2;
    }

    public function constructHvpReportGlobal($array, $db)
    {
        $array = $this->sortArray($array);

//        var_dump($array);exit();

        if (isset($_GET['debug'])) {
            print_r($array);
            exit();
        }
        $finale = array();
        $prov = null;
        $prec = array();
        $tmp = array();
        $viewed = 0;
        $lezioni_terminate = array();

        $last = end($array);

//        var_dump($array); exit;

        //nuovo codice enzo
        $newArray = array();
        $arrControl = array();
        $finale2 = array();

        $i = 0;
        foreach ($array as $singolo) {

            $checkTime = 0;

            $newArray = $singolo;

            list($giorno, $mese, $anno) = explode("/",$singolo['timecreated']);
            $timestamp_singolo = strtotime($anno.'-'.$mese.'-'.$giorno);

            if(isset($prec_time)) {
                $checkTime = $timestamp_singolo - $prec_time;
            }

//            var_dump($checkTime);

//            echo $prec_time . ' - ' . $timestamp_singolo . ($prec_time - $timestamp_singolo) . "\n";

            if (strpos($singolo['other'], 'RESET') === FALSE) {

                if ($singolo['action'] == "submitted") {
                    $tempo_cumulato_first = $this->getTimeLesson($singolo['objectid'], $db);

//                    $lastProgress = $this->getLastProgress($singolo['userid'], $singolo['timestamp'], $singolo['objectid'], $db);
//                    $tempo_cumulato = $tempo_cumulato - (int)$lastProgress;

                    $tempo_cumulato = $this->convertSeconds(($tempo_cumulato_first));

//                    $time_sing_orario = explode(":", $singolo['orario']);
//                    $seconds_sing_orario = $time_sing_orario[0] * 3600 + $time_sing_orario[1] * 60 + $time_sing_orario[2];
//                    $newArray['orario_logout'] = $this->convertSeconds($seconds_sing_orario + $tempo_cumulato_first);

                    $newArray['orario_logout'] = $singolo['orario'];
                    $newArray['id_logout'] = $singolo['id'];
                    $newArray['tempo_cumulato'] = $tempo_cumulato;
//                array_push($finale2, $newArray);

                    $time_sing = explode(":", $tempo_cumulato);
                    $seconds_sing = $time_sing[0] * 3600 + $time_sing[1] * 60 + $time_sing[2];
                    $newArray['diff'] = 0;

                    $arrControl[$singolo['objectid'] . $singolo['userid'] . str_replace('/', '', $singolo['timecreated'])] = $newArray;

//                    $lezioni_terminate[$singolo['lezione']] = $singolo['objectid'];
                    $lezioni_terminate[$singolo['lezione_id']] = $singolo['objectid'];

//                    var_dump($checkTime);

                } elseif ($singolo['action'] == "progress" || $singolo['action'] == "clicked" || $singolo['action'] == "joined") {

//                $checkExists = array_intersect($singolo, $finale2);
//                $check = !empty($checkExists);
//
//                if($check){
//
//                }

                    $newArray['orario_logout'] = $singolo['orario'];
                    $newArray['id_logout'] = $singolo['id'];
                    $newArray['lezione_id'] = $singolo['objectid'];

//                var_dump($singolo['lezione']);

                    $progress = $singolo['other'];

                    if (@unserialize($progress) === false) {
                        $progress = json_decode($progress);
                    } else {
                        $progress = json_decode(unserialize($progress));
                    }

                    if (!is_null($progress)) {
                        @$tempo_cumulato = (int)$progress->progress;
                    } else {
                        $tempo_cumulato = 0;
                    }

                    $tempo_cumulato = $this->convertSeconds($tempo_cumulato);

                    $newArray['tempo_cumulato'] = $tempo_cumulato;

//                    if($singolo['id'] == 6722832){
//                        var_dump($newArray['tempo_cumulato']);
//                        exit;
//                    }

                    $time_sing = explode(":", $tempo_cumulato);
                    $seconds_sing = $time_sing[0] * 3600 + $time_sing[1] * 60 + $time_sing[2];

//                    var_dump($checkTime);
//                    if($checkTime > 0){
//                        echo "enzo";
//                    }

                    $newArray['diff'] = $this->convertSeconds($seconds_sing);

//                    if($singolo['objectid'] == 249){
//                        var_dump($lastProgress);
//                    }


                    if (!array_key_exists($singolo['lezione_id'], $lezioni_terminate)) {

//                    array_push($finale2, $newArray);
//
//                    if (array_key_exists($singolo['objectid'], $arrControl)){
//                        $arrControl[$singolo['objectid']] = $finale2;
//                    }

                        $arrControl[$singolo['objectid'] . $singolo['userid'] . str_replace('/', '', $singolo['timecreated'])] = $newArray;
                    }

                }

//                var_dump($checkTime);

            }


            $newArray = array();
            $lezioni_terminate = array();
            $prec_time = $timestamp_singolo;

            $i++;
        }

//        var_dump($arrControl); exit;

//        end nuovo codice

        $new = array();
        $new2 = array();

        foreach($arrControl as $control){

            $time_sing = explode(":", $control['tempo_cumulato']);
            $tempo_cumulato = $time_sing[0] * 3600 + $time_sing[1] * 60 + $time_sing[2];
//            $newArray['diff'] = $this->convertSeconds($seconds_sing);

            $lastProgress = $this->getLastProgressNew($control['userid'], $control['timestamp'], $control['objectid'], $db);
//            $lastProgress = 1000;

//            if($control['userid'] == "9887" && $control['objectid'] == 249){
//                var_dump($control['timestamp']);
//                var_dump($tempo_cumulato);
//                echo "\n";
//                var_dump($lastProgress);
//                echo "----------------\n\n";
//            }

            $tempo_cumulato = $tempo_cumulato - $lastProgress;

//            if($tempo_cumulato > $lastProgress){
//                $tempo_cumulato = $tempo_cumulato - $lastProgress;
//            }

            if($tempo_cumulato < 0){
                $control['tempo_cumulato'] = $control['diff'];
            }else{
                $control['tempo_cumulato'] = $this->convertSeconds($tempo_cumulato);
            }

            $time_orario = explode(":", $control['orario_logout']);
            $tempo_orario = $time_orario[0] * 3600 + $time_orario[1] * 60 + $time_orario[2];

            $tempo_diff = 0;
            if ($control['diff'] != 0) {
                $time_diff = explode(":", $control['diff']);
                $tempo_diff = $time_diff[0] * 3600 + $time_diff[1] * 60 + $time_diff[2];
            }

            $orario_logout = ($tempo_orario + $tempo_diff);
//            $control['orario_logout'] = $lastProgress;
            $control['orario_logout'] = $this->convertSeconds($orario_logout);

//            if($control['id'] == 6721710){
//                var_dump($control['tempo_cumulato']);
//                exit;
//            }



//            if($control['userid'] == "9887" && $control['objectid'] == 249){
//                var_dump($tempo_cumulato);
//                var_dump($this->convertSeconds($tempo_cumulato));
//                echo "---++++++++---\n\n";
//            }


//            $control['diff'] = $this->convertSeconds($tempo_cumulato);

//            if($control['userid'] == "9887" && $control['objectid'] == 249){
//                var_dump($control['tempo_cumulato']);
//            }

            array_push($finale2, $control);

//            foreach($finale2 as $singolo) {
//                if (strpos($singolo['other'], 'RESET') !== FALSE) {
//
//                    $entrato = false;
//
//                    //continue;
//                    foreach ($finale2 as $key => $elem) {
//                        if ($elem['userid'] == $singolo['userid'] && $elem['objectid'] == $singolo['objectid']) {
//
//                            unset($finale2[$key]);
//                            unset($lezioni_terminate[$singolo['objectid']]);
//                            $entrato = true;
//                        }
//                    }
//                    if ($entrato == false)
//                        continue;
//                }
//
//                $singolo = json_decode(json_encode($singolo, true), true);
//
//            }


        }

//        var_dump($finale2);

        foreach ($finale2 as $singolo) {
            $singolo = json_decode(json_encode($singolo, true), true);
//            echo $i . "<br />";
//            print_r($singolo);
            if (isset($singolo))
                array_push($new2, $singolo);

//            $i++;
        }

//        var_dump($finale2);
//
//        exit;

//        var_dump($finale2);
//        exit;


        //vecchio codice
//        foreach ($array as $singolo) {
//
//            if (strpos($singolo['other'], 'RESET') !== FALSE) {
//
//                $entrato = false;
//
//                //continue;
//                foreach ($finale as $key => $elem) {
//                    if ($elem['userid'] == $singolo['userid'] && $elem['objectid'] == $singolo['objectid']) {
//
//                        unset($finale[$key]);
//                        unset($lezioni_terminate[$singolo['lezione']]);
//                        $entrato = true;
//                    }
//                }
//                if ($entrato == false)
//                    continue;
//            }
//
//            if ($prov == null) {
//                if (isset($prec['userid']) && $singolo['userid'] != $prec['userid'])
//                    $lezioni_terminate = array();
//                    $prov = $singolo;
//                    $prec = $prov;
//                    continue;
//            }
//
//            /*$isResettato = $this->isResettato($prov['userid'], $prov['objectid'], $prov['timecreated'], $db);
//
//            if ($isResettato) {
//                $prov['other'] = 'RESET';
//            }*/
//
////            var_dump($prov['timecreated']);
////            var_dump($singolo['timecreated']);
////            echo "<br><br>";
//
////            if($singolo['id'] == '6570974'){
////                $progress = $singolo['other'];
////
////                if(@unserialize($progress) === false){
////                    $progress = json_decode($progress);
////                }else{
////                    $progress = json_decode(unserialize($progress));
////                }
////
////                if(!is_null($progress->progress)){
////                    @$tempo_cumulato = (int)$progress->progress;
////                }else{
////                    $tempo_cumulato = 0;
////                }
////
////                var_dump($tempo_cumulato);
////                exit;
////
////            }
//
//            if ($prov['userid'] == $singolo['userid'] && $prov['timecreated'] == $singolo['timecreated']) {//var_dump($prov);var_dump($singolo);exit('aaaa');
//                /*if($singolo['userid']==2122 && $prov['objectid']==969){
//                        print_r($singolo);
//                        print_r($prov);
//                        print_r($lezioni_terminate);
//                        exit();
//                    }*/
//                if ($prov['action'] == 'viewed' && $singolo['action'] == 'progress') {
////                    var_dump($prov);var_dump($singolo); exit;
//                    if (count($array) == 2 || $singolo == $last) {
//                        $prov['orario_logout'] = $singolo['orario'];
//                        $prov['id_logout'] = $singolo['id'];
//
//                        //da verificare
////                        $progress = $singolo['other'];
////                        $toReplace = ['{', '}', '"'];
////                        $progress = str_replace($toReplace, "", $progress);
////
//////                        var_dump($prov['id']);
//////                        var_dump($progress);
//////                        print_r('------------------');
////
//////                        var_dump($prov['id']);
//////                        var_dump($singolo['id']);
//////                        var_dump($singolo);
////
////
////                        if ($progress != "") {
////                            $progress = explode(".", $progress);
////                            $progress = explode(":", $progress[0]);
////                            @$tempo_cumulato = (int)@$progress[1];
////                        } else
////                            $tempo_cumulato = 0;
//                        //end da verificare
//
//                        $progress = $singolo['other'];
//
//                        if(@unserialize($progress) === false){
//                            $progress = json_decode($progress);
//                        }else{
//                            $progress = json_decode(unserialize($progress));
//                        }
//
//                        if(!is_null($progress->progress)){
//                            @$tempo_cumulato = (int)$progress->progress;
//                        }else{
//                            $tempo_cumulato = 0;
//                        }
//
//                        $tempo_cumulato = $this->convertSeconds($tempo_cumulato);
//
////                        if ($prov['userid'] == 2221) {
////                            //echo $prov['userid'].' Data:'.$prov['timecreated'].' Tempo cumulato:'.$tempo_cumulato.' other:'.$prov['other'].' objectid:'.$prov['objectid'].' Action: '.$prov['action']."<br>";
////                        }
//
////                        echo "primo: " . $tempo_cumulato . "<br><br>";
//
//                        $prov['tempo_cumulato'] = $tempo_cumulato;
//
//                        array_push($finale, $prov);
//                        break;
//                    }
//                    $prec = $singolo;
//                    $viewed = 1;
//                } else if ($singolo['action'] == "submitted") {
//
//                    //enzo
//                    $tempo_cumulato = $this->getTimeLesson($prov['objectid'], $db);
//
//                    $tempo_cumulato = $this->convertSeconds(($tempo_cumulato));
////                    var_dump($tempo_cumulato); echo "<br>";
//
//                    $lastProgress = $this->getLastProgress($singolo['userid'], $singolo['timestamp'], $singolo['objectid'], $db);
////                    var_dump($tempo_cumulato);
//
//                    //enzo
////                    $time_sing = explode(":", $tempo_cumulato);
////                    $seconds_sing = $time_sing[0] * 3600 + $time_sing[1] * 60 + $time_sing[2];
////                    $tempo_cumulato = $seconds_sing - (int)$lastProgress;
////                    $tempo_cumulato = $this->convertSeconds(($tempo_cumulato));
//
//                    $prov['orario_logout'] = $singolo['orario'];
//                    $prov['id_logout'] = $singolo['id'];
//
////                    var_dump($singolo['submitted']);
//
////                    if ($singolo['submitted'] == false) {
//////                        echo "qui";
////                        $prov['submitted'] = true;
////                        $prov['tempo_cumulato'] = 0;
////                    }else{
//////                        echo "buo";
//////                        $prov['submitted'] = false;
////                        $prov['tempo_cumulato'] = $tempo_cumulato;
////                    }
//
//
////                    echo "secondo: " . $tempo_cumulato . "<br><br>";
//
//                    $prov['tempo_cumulato'] = $tempo_cumulato;
//
//
////                    if($prov['userid']==24) {
////                        echo $prov['userid'].' Data:'.$prov['timecreated'].' Tempo cumulato:'.$tempo_cumulato.' other:'.$prov['other'].' objectid:'.$prov['objectid'].' Action: '.$prov['action']."<br><br>";
////                    }
//
//
//                    array_push($finale, $prov);
//
////                    if($prov['objectid'] == 630 && $prov['userid'] == 9892){
////                        var_dump($prov);
////                    }
////                    var_dump($prov['objectid']);
////                    exit;
//                    $lezioni_terminate[$prov['lezione']] = $prov['objectid'];
//                    $prov = null;
//                    $viewed = 1;
//                } else if ($singolo['action'] == "viewed" && $prov['action'] != "progress") {
//                    if ($prec['action'] == "progress") {
//                        $prov['orario_logout'] = $prec['orario'];
//                        $prov['id_logout'] = $prec['id'];
//
//                        //da verifacre
////                        $progress = $prec['other'];
////                        $toReplace = ['{', '}', '"'];
////                        $progress = str_replace($toReplace, "", $progress);
////
////                        if ($progress != "") {
////                            $progress = explode(".", $progress);
////                            $progress = explode(":", $progress[0]);
////                            @$tempo_cumulato = (int)@$progress[1];
////                        } else
////                            $tempo_cumulato = 0;
//                        //end da verificare
//
//                        $progress = $prec['other'];
//
//                        if(@unserialize($progress) === false){
//                            $progress = json_decode($progress);
//                        }else{
//                            $progress = json_decode(unserialize($progress));
//                        }
//
//                        if(!is_null($progress->progress)){
//                            @$tempo_cumulato = (int)$progress->progress;
//                        }else{
//                            $tempo_cumulato = 0;
//                        }
//
//                        $tempo_cumulato = $this->convertSeconds($tempo_cumulato);
//
////                        echo "terzo: " . $tempo_cumulato . "<br><br>";
//                        $prov['tempo_cumulato'] = $tempo_cumulato;
//
////                        if($prov['userid']==9892) {
////                            echo $prov['userid'].' Data:'.$prov['timecreated'].' Tempo cumulato:'.$tempo_cumulato.' other:'.$prov['other'].' objectid:'.$prov['objectid'].' Action: '.$prov['action']."\n";
////                        }
//
//
//                        //enzo
//                        if (!array_key_exists($prov['lezione'], $lezioni_terminate))
//                            array_push($finale, $prov);
//                        $viewed = 1;
//                    } else {
//                        if (array_key_exists($singolo['lezione'], $lezioni_terminate)) {
//                            continue;
//                        }
//                    }
//
//                    $prov = $singolo;
//                    $prec = $prov;
//                } else if ($prov['action'] == "progress") {
////                    if($singolo['id'] == '6568116'){
////                        var_dump($singolo);
////                        exit;
////                    }
//
//                    $prov = $singolo;
//                    $prec = $prov;
//                }
//            } else {
//
//                if ($prec['action'] == "progress") {
//                    $prov['orario_logout'] = $prec['orario'];
//                    $prov['id_logout'] = $prec['id'];
//
//                    //da verificare
////                    $progress = $prec['other'];
////                    $toReplace = ['{', '}', '"'];
////                    $progress = str_replace($toReplace, "", $progress);
////                    if ($progress != "") {
////                        $progress = explode(".", $progress);
////                        $progress = explode(":", $progress[0]);
////                        @$tempo_cumulato = (int)@$progress[1];
////                    } else
////                        $tempo_cumulato = 0;
//                    //end da verificare
//
//                    $progress = $prec['other'];
//
//                    if(@unserialize($progress) === false){
//                        $progress = json_decode($progress);
//                    }else{
//                        $progress = json_decode(unserialize($progress));
//                    }
//
//                    if(!is_null($progress->progress)){
//                        @$tempo_cumulato = (int)$progress->progress;
//                    }else{
//                        $tempo_cumulato = 0;
//                    }
//
//                    $tempo_cumulato = $this->convertSeconds($tempo_cumulato);
////                    echo "quarto: " . $tempo_cumulato . "<br><br>";
//                    $prov['tempo_cumulato'] = $tempo_cumulato;
//
////                    if ($prov['userid'] == 9892) {
////                        echo $prov['userid'].' Data:'.$prov['timecreated'].' Tempo cumulato:'.$tempo_cumulato.' other:'.$prov['other'].' objectid:'.$prov['objectid'].' Action: '.$prov['action']."\n";
////                    }
//
//                    //enzo
//                    if (!array_key_exists($prov['lezione'], $lezioni_terminate)) {
//                        array_push($finale, $prov);
//
////                        if($prov['userid'] == '9892') {
////                            var_dump($singolo);
////                            var_dump($prov);
////                            var_dump($prec);
////                        }
//                    }
//
//
//
//                }
//                $viewed = 1;
//                $lezioni_terminate = array();
//                $prov = $singolo;
//                $prec = $prov;
//            }
//        }
//        var_dump($finale);exit();
        /*if ( $singolo['userid'] == 2122 ) {
            var_
        }*/
//exit();

//                echo "enzo <br />";
//        var_dump($finale);
////
//        foreach ($finale as $key => $elem) {
//            if (strpos($elem['other'], 'RESET') !== FALSE) {
//
//                var_dump($key);
//                unset($finale[$key]);
//                unset($lezioni_terminate[$singolo['lezione']]);
//            }
//        }

//        if (strpos($singolo['other'], "RESET") !== FALSE) {
//
//            $entrato = false;
//
//            //continue;
//            foreach ($finale as $key => $elem) {
//                if ($elem['userid'] == $singolo['userid'] && $elem['objectid'] == $singolo['objectid']) {
//
//                    unset($finale[$key]);
//                    unset($lezioni_terminate[$singolo['lezione']]);
//                    $entrato = true;
//                }
//            }
//            if ($entrato == false)
//                continue;
//        }

        //vecchio codice2
//        $prov = null;
//        $new = array();
//        $temp = array();
//
////        echo "enzo <br />";
////        var_dump($finale); exit;
//        foreach ($finale2 as $singolo) {
//
////            if($singolo['id'] == '6568116'){
////                var_dump($singolo);
////                exit;
////            }
//
////            var_dump($singolo);
//
////            if($singolo['other'] != 'RESET' && $singolo['other'] != '\"RESET\"'){
//
////            if (strpos($singolo['other'], "RESET") === FALSE){
//
//                if ($prov == null) {
//                    $time_sing = explode(":", $singolo['tempo_cumulato']);
//                    $seconds_sing = $time_sing[0] * 3600 + $time_sing[1] * 60 + $time_sing[2];
//                    $temp[$singolo['lezione']] = $seconds_sing;
//                    $lastProgress = $this->getLastProgress($singolo['userid'], $singolo['timestamp'], $singolo['objectid'], $db);
//                    $tc = $seconds_sing - (int)$lastProgress;
//
//                    //enzo
////                    if ($tc < 0) {
////                        $singolo['diff'] = 0;
////                    } else {
////                        $singolo['diff'] = $this->convertSeconds($tc);
////                    }
//
//                    $singolo['diff'] = $this->convertSeconds($tc);
//
//                    array_push($new, $singolo);
//                    $prov = $singolo;
//                    continue;
//                }
//
////            var_dump($temp[$singolo['lezione']]);
////            echo "<br><br>";
//
//            $time_prov = explode(":", $prov['tempo_cumulato']);
//            $time_sing = explode(":", $singolo['tempo_cumulato']);
//            $seconds_prov = $time_prov[0] * 3600 + $time_prov[1] * 60 + $time_prov[2];
//            $seconds_sing = $time_sing[0] * 3600 + $time_sing[1] * 60 + $time_sing[2];
//
//            if ($prov['userid'] == $singolo['userid']) {
//                if (array_key_exists($singolo['lezione'], $temp)) {
//
////                    if($singolo['userid'] == '9892') {
////                        var_dump($seconds_sing);
////                        var_dump($singolo['lezione']);
////                        var_dump($temp[$singolo['lezione']]);
////                        echo "<br><br>";
////                    }
//                    if ($seconds_sing >= $temp[$singolo['lezione']]) {
//                        $diff = $seconds_sing - $temp[$singolo['lezione']];
//
////                        if (strpos($singolo['other'], "RESET") !== FALSE) {
////                            var_dump($temp[$singolo['lezione']]);
////                            $diff = $diff - $temp[$singolo['lezione']];
////                        }
//
//                    }else {
//                        $diff = 0;
//                    }
//                } else {
//                    if (strpos($singolo['other'], "RESET") === FALSE) {
//                        $lastProgress = $this->getLastProgress($singolo['userid'], $singolo['timestamp'], $singolo['objectid'], $db);
//                    } else {
//                        $lastProgress = 0;
//                    }
//                    $diff = $seconds_sing - $lastProgress;
//                }
//                $temp[$singolo['lezione']] = $diff;
//
////                var_dump($this->convertSeconds($diff));
//
//                $singolo['diff'] = $this->convertSeconds($diff);
////                if ($singolo['userid'] == 24 && $singolo['timecreated'] == '01/06/2022' && $singolo['action'] == 'viewed') {
//////                    echo $lastProgress . "<br>";
////                    print_r($singolo);
//////                    exit();
////                }else{
////                    continue;
////                }
////                $prov = $singolo;
//                array_push($new, $singolo);
//            } else {
//                $temp = array();
//                $lastProgress = $this->getLastProgress($singolo['userid'], $singolo['timestamp'], $singolo['objectid'], $db);
//                $tc = $seconds_sing - (int)$lastProgress;
//                $singolo['diff'] = $this->convertSeconds($tc);
//                array_push($new, $singolo);
//                $prov = $singolo;
//            }
////        }
//    }
//
//
//
////        var_dump($new);
////        exit;
//
////        foreach ($new as $singolo) {
////            var_dump($singolo);
////        }
////        exit;
//
//
//        $new2 = array();
////        $i = 0;
//        foreach ($new as $singolo) {
//            $singolo = json_decode(json_encode($singolo, true), true);
////            echo $i . "<br />";
////            print_r($singolo);
//            if (isset($singolo))
//                array_push($new2, $singolo);
//
////            $i++;
//        }



        return $new2;
    }

    public function setHvpSheet($index, $spreadsheet, $letter, $filtri, $j, $new) {
        //INIZIO RE1-T2 Inserimento titolo del corso in intestazione (setHvpSheet)
        $course_filtered = false;
        if(isset($new[0]['corso']) && $course_filtered){
            //Imposto il titolo
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Corso seguito : " . $new[0]['corso']);

            //Unisco le celle
            $all_letters = range('A', 'Z');
            $merge_to_index = 16+$letter-count($filtri);
            if($merge_to_index >= count($all_letters)) $merge_to_index = count($all_letters)-1;
            $sheet = $spreadsheet->getActiveSheet();
            $sheet->MergeCells($letter . $index . ':' . $all_letters[$merge_to_index] . $index);
            $index += 2;
        }
        //FINE RE1-T2

        if (!in_array("data", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Data");
            $letter++;
        }
        if (!in_array("userid", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Userid");
            $letter++;
        }
        if (!in_array("username", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Username");
            $letter++;
        }
        if (!in_array("name", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Nome Cognome");
            $letter++;
        }
        if (!in_array("cf", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Codice Fiscale");
            $letter++;
        }
        if (!in_array("email", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Email");
            $letter++;
        }
        if (!in_array("dn", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Data di nascita");
            $letter++;
        }
        if (!in_array("ln", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Luogo di nascita");
            $letter++;
        }
        if (!in_array("azienda", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Azienda");
            $letter++;
        }
        if (!in_array("phone", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "N.di telefono");
            $letter++;
        }
        if (!in_array("corso", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Corso");
            $letter++;
        }
        if (!in_array("lezione", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Lezione");
            $letter++;
        }
        if (!in_array("orarioA", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Ora accesso al video");
            $letter++;
        }
        if (!in_array("orarioL", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Ora uscita dal video");
            $letter++;
        }
        if (!in_array("tc", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Minuti cumulati su video");
            $letter++;
        }
        if (!in_array("ts", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Durata sessione");
        }


        $spreadsheet->getActiveSheet()->setTitle("Report");


        $from = "A1";
        $to = $letter . $index;
        $spreadsheet->getActiveSheet()->getStyle("$from:$to")->getFont()->setBold(true);

        $i = $index + 1;
        foreach ($new as $singolo) {
            $letter = 'A';
            if (!in_array("data", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['timecreated']);
                $letter++;
            }
            if (!in_array("userid", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['userid']);
                $letter++;
            }
            if (!in_array("username", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['username']);
                $letter++;
            }
            if (!in_array("name", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['name']);
                $letter++;
            }

            if (!in_array("cf", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, @$singolo['cf']);
                $letter++;
            }

            if (!in_array("email", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["email"]);
                $letter++;
            }

            if (!in_array("dn", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["data_nascita"]);
                $letter++;
            }

            if (!in_array("ln", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["citta"]);
                $letter++;
            }

            if (!in_array("azienda", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["azienda"]);
                $letter++;
            }
            if (!in_array("phone", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["phone"]);
                $letter++;
            }
            if (!in_array("corso", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["corso"]);
                $letter++;
            }
            if (!in_array("lezione", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["lezione"]);
                $letter++;
            }
            if (!in_array("orarioA", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["orario"]);
                $letter++;
            }
            if (!in_array("orarioL", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["orario_logout"]);
                $letter++;
            }
            if (!in_array("tc", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["tempo_cumulato"]);
                $letter++;
            }
            if (!in_array("ts", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["diff"]);
            }
            $i++;
        }

        foreach (range('A', $letter) as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        $spreadsheet->getActiveSheet()->getStyle("A" . $index . ":" . $letter . $index)
            ->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('00FF7F');

        return $i;
    }

    public function setHvpNewSheet($index, $spreadsheet, $letter, $filtri, $j, $new) {
        //INIZIO RE1-T2 Inserimento titolo del corso in intestazione (setHvpSheet)
        $course_filtered = false;
        if(isset($new[0]['corso']) && $course_filtered){
            //Imposto il titolo
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Corso seguito : " . $new[0]['corso']);

            //Unisco le celle
            $all_letters = range('A', 'Z');
            $merge_to_index = 16+$letter-count($filtri);
            if($merge_to_index >= count($all_letters)) $merge_to_index = count($all_letters)-1;
            $sheet = $spreadsheet->getActiveSheet();
            $sheet->MergeCells($letter . $index . ':' . $all_letters[$merge_to_index] . $index);
            $index += 2;
        }
        //FINE RE1-T2

        if (!in_array("date", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Data");
            $letter++;
        }
        if (!in_array("user_id", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Userid");
            $letter++;
        }
        if (!in_array("username", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Username");
            $letter++;
        }
        if (!in_array("user_fullname", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Nome Cognome");
            $letter++;
        }
        if (!in_array("user_fiscalcode", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Codice Fiscale");
            $letter++;
        }
        if (!in_array("user_email", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Email");
            $letter++;
        }
        if (!in_array("user_date_of_birth", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Data di nascita");
            $letter++;
        }
        if (!in_array("user_city_of_birth", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Luogo di nascita");
            $letter++;
        }
        if (!in_array("user_company_name", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Azienda");
            $letter++;
        }
        if (!in_array("user_telephone", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "N.di telefono");
            $letter++;
        }
        if (!in_array("course_name", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Corso");
            $letter++;
        }
        if (!in_array("module_name", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Lezione");
            $letter++;
        }
        if (!in_array("start_session_time", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Ora accesso al video");
            $letter++;
        }
        if (!in_array("end_session_time", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Ora uscita dal video");
            $letter++;
        }
        if (!in_array("progress_time", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Minuti cumulati su video");
            $letter++;
        }
        if (!in_array("session_time", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Durata sessione");
        }

        $spreadsheet->getActiveSheet()->setTitle("Report");

        $from = "A1";
        $to = $letter . $index;
        $spreadsheet->getActiveSheet()->getStyle("$from:$to")->getFont()->setBold(true);

        $i = $index + 1;
        foreach ($new as $singolo) {
            $letter = 'A';
            if (!in_array("date", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['date']);
                $letter++;
            }
            if (!in_array("user_id", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['user_id']);
                $letter++;
            }
            if (!in_array("username", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['username']);
                $letter++;
            }
            if (!in_array("user_fullname", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['user_fullname']);
                $letter++;
            }
            if (!in_array("user_fiscalcode", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, @$singolo['user_fiscalcode']);
                $letter++;
            }
            if (!in_array("user_email", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["user_email"]);
                $letter++;
            }
            if (!in_array("user_date_of_birth", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["user_date_of_birth"]);
                $letter++;
            }
            if (!in_array("user_city_of_birth", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["user_city_of_birth"]);
                $letter++;
            }
            if (!in_array("user_company_name", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["user_company_name"]);
                $letter++;
            }
            if (!in_array("user_telephone", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["user_telephone"]);
                $letter++;
            }
            if (!in_array("course_name", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["course_name"]);
                $letter++;
            }
            if (!in_array("module_name", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["module_name"]);
                $letter++;
            }
            if (!in_array("start_session_time", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["start_session_time"]);
                $letter++;
            }
            if (!in_array("end_session_time", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["end_session_time"]);
                $letter++;
            }
            if (!in_array("progress_time", $filtri)) {
                if($singolo["reset"]) $singolo["progress_time"] .= ' (R)';
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["progress_time"]);
                $letter++;
            }
            if (!in_array("session_time", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["session_time"]);
            }
            $i++;
        }

        foreach (range('A', $letter) as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        $spreadsheet->getActiveSheet()->getStyle("A" . $index . ":" . $letter . $index)
            ->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('00FF7F');

        return $i;
    }

    public function setSummaryHvpSheet($index, $spreadsheet, $letter, $filtri, $j, $new) {
        $date_from = $new['date_from'];
        $date_to   = $new['date_to'];
        $new       = $new['data'];

        //Data inizio corsi
        $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Inizio periodo osservato : " . date('d/m/Y', strtotime($date_from)));
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->MergeCells($letter . $index . ':D' . $index);
        $index ++;

        //Data fine corsi
        $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Fine periodo osservato : " . date('d/m/Y', strtotime($date_to)));
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->MergeCells($letter . $index . ':D' . $index);
        $index += 2;

        //INIZIO RE1-T2 Inserimento titolo del corso in intestazione (setHvpSheet)
        $course_filtered = false;
        if(isset($new[0]['corso']) && $course_filtered){
            //Imposto il titolo
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Corso seguito : " . $new[0]['corso']);

            //Unisco le celle
            $all_letters = range('A', 'Z');
            $merge_to_index = 16+$letter-count($filtri);
            if($merge_to_index >= count($all_letters)) $merge_to_index = count($all_letters)-1;
            $sheet = $spreadsheet->getActiveSheet();
            $sheet->MergeCells($letter . $index . ':' . $all_letters[$merge_to_index] . $index);
            $index += 2;
        }
        //FINE RE1-T2
        if (!in_array("userid", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Userid");
            $letter++;
        }
        if (!in_array("username", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Username");
            $letter++;
        }
        if (!in_array("name", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Nome Cognome");
            $letter++;
        }
        if (!in_array("cf", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Codice Fiscale");
            $letter++;
        }
        if (!in_array("email", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Email");
            $letter++;
        }
        if (!in_array("dn", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Data di nascita");
            $letter++;
        }
        if (!in_array("ln", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Luogo di nascita");
            $letter++;
        }
        if (!in_array("azienda", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Azienda");
            $letter++;
        }
        if (!in_array("phone", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "N.di telefono");
            $letter++;
        }
        if (!in_array("corso", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Corso");
            $letter++;
        }
        if (!in_array("total_time", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Totale ore corso");
            $letter++;
        }

        $spreadsheet->getActiveSheet()->setTitle("Report");


        $from = "A1";
        $to = $letter . $index;
        $spreadsheet->getActiveSheet()->getStyle("$from:$to")->getFont()->setBold(true);

        $i = $index + 1;
        foreach ($new as $singolo) {
            $letter = 'A';
            if (!in_array("userid", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['userid']);
                $letter++;
            }
            if (!in_array("username", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['username']);
                $letter++;
            }
            if (!in_array("name", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['name']);
                $letter++;
            }

            if (!in_array("cf", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, @$singolo['cf']);
                $letter++;
            }

            if (!in_array("email", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["email"]);
                $letter++;
            }

            if (!in_array("dn", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["data_nascita"]);
                $letter++;
            }

            if (!in_array("ln", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["citta"]);
                $letter++;
            }

            if (!in_array("azienda", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["azienda"]);
                $letter++;
            }
            if (!in_array("phone", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["phone"]);
                $letter++;
            }
            if (!in_array("corso", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["corso"]);
                $letter++;
            }
            if (!in_array("total_time", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["total_time"]);
                $letter++;
            }
            $i++;
        }

        foreach (range('A', $letter) as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        $spreadsheet->getActiveSheet()->getStyle("A" . $index . ":" . $letter . $index)
            ->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('00FF7F');

        return $i;
    }

    public function setVideoTimeProSheet($index, $spreadsheet, $letter, $filtri, $j, $new) {
        if (!in_array("user_id", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "User ID");
            $letter++;
        }
        if (!in_array("username", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Username");
            $letter++;
        }
        if (!in_array("fullname", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Nome Cognome");
            $letter++;
        }
        if (!in_array("fiscal_code", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Codice Fiscale");
            $letter++;
        }
        if (!in_array("email", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Email");
            $letter++;
        }
        if (!in_array("date_of_birth", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Data di nascita");
            $letter++;
        }
        if (!in_array("city_of_birth", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Luogo di nascita");
            $letter++;
        }
        if (!in_array("company_name", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Azienda");
            $letter++;
        }
        if (!in_array("telephone", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Telefono");
            $letter++;
        }
        if (!in_array("course_name", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Corso");
            $letter++;
        }
        if (!in_array("course_id", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "ID Corso");
            $letter++;
        }
        if (!in_array("total_time", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Totale ore");
            $letter++;
        }
        foreach($new['array_session_dates'] as $key => $value){
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, $value);
            $letter++;
        }
        $spreadsheet->getActiveSheet()->setTitle("Report");
        $from = "A1";
        $to = $letter . $index;
        $spreadsheet->getActiveSheet()->getStyle("$from:$to")->getFont()->setBold(true);

        $i = $index + 1;

        foreach($new['array_session_values'] as $item){
            $letter = 'A';
            if (!in_array("user_id", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $item['user_id']);
                $letter++;
            }
            if (!in_array("username", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $item['username']);
                $letter++;
            }
            if (!in_array("fullname", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $item['fullname']);
                $letter++;
            }
            if (!in_array("fiscal_code", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, @$item['fiscal_code']);
                $letter++;
            }
            if (!in_array("email", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $item["email"]);
                $letter++;
            }
            if (!in_array("date_of_birth", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, '-');
                $letter++;
            }
            if (!in_array("city_of_birth", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $item["city"]);
                $letter++;
            }
            if (!in_array("company_name", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $item["company_name"]);
                $letter++;
            }
            if (!in_array("telephone", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $item["telephone"]);
                $letter++;
            }
            if (!in_array("course_name", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $item["course_name"]);
                $letter++;
            }
            if (!in_array("course_id", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $item["course_id"]);
                $letter++;
            }
            if (!in_array("total_time", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $item["total_time"]);
                $letter++;
            }
            foreach($new['array_session_dates'] as $key => $value){
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $item[$key]);
                $letter++;
            }
            $i++;
        }
        foreach (range('A', $letter) as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        $spreadsheet->getActiveSheet()->getStyle("A" . $index . ":" . $letter . $index)
            ->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('00FF7F');

        return $i;
    }

    public function constructSHvpReport($array) {

        $prov = null;
        $mesi = array();
        $new = array();
        $cumulatoTot = 0;
        $tempo_tot = 0;
        $cont = 0;
        $totResult = count($array);
        $array_mesi = array();
        $temp = array();

//        var_dump($array); exit;

        foreach ($array as $singolo) {

//            var_dump($singolo); exit;

            if($singolo['diff'] == 0){
                $singolo['diff'] = '00:00:00';
            }

//            //enzo
//            if($singolo['userid'] == 24 && $singolo['timecreated'] == '20/07/2022'){
//                var_dump($singolo);
//                continue;
//            }

            if ($totResult == 1) {
                $data_singolo = str_replace("/", "", $singolo['timecreated']);
                $timesingolo_split = explode(":", $singolo['diff']);
                $seconds_singolo = $timesingolo_split[0] * 3600 + $timesingolo_split[1] * 60 + $timesingolo_split[2];
                $mesi[$singolo['timecreated']] = $singolo['tempo_cumulato'];
                array_push($array_mesi, $singolo['timecreated']);
                $tempo = explode(":", $singolo['tempo_cumulato']);
                $secondi = $tempo[0] * 3600 + $tempo[1] * 60 + $tempo[2];
                $tempo_tot = $secondi;
                $singolo['mesi'] = $mesi;
                $singolo['tempo_tot'] = $this->convertSeconds($tempo_tot);
                array_push($new, $singolo);

                break;
            }

            if ($prov == null) {
                $prov = $singolo;
                $cont++;
                continue;
            }
            $cont++;
            $data_prov = str_replace("/", "", $prov['timecreated']);
            $data_singolo = str_replace("/", "", $singolo['timecreated']);
            $timeprov_split = explode(":", $prov['diff']);
            $timesingolo_split = explode(":", $singolo['diff']);
            $seconds_prov = $timeprov_split[0] * 3600 + $timeprov_split[1] * 60 + $timeprov_split[2];
            $seconds_singolo = $timesingolo_split[0] * 3600 + $timesingolo_split[1] * 60 + $timesingolo_split[2];

            if ($prov['userid'] == $singolo['userid']) {

                if ($data_prov == $data_singolo) {

//                    var_dump($prov['diff']);

//                    print_r($prov['diff']); echo "<br><br>";
//                    var_dump($seconds_prov);
//                    var_dump($seconds_singolo);

                    if ($cumulatoTot == 0) {
                        $cumulatoTot += $seconds_prov + $seconds_singolo;
//                        $cumulatoTot += $seconds_singolo;
                    } else {
                        $cumulatoTot += $seconds_singolo;
                    }
                    if (!in_array($prov['timecreated'], $array_mesi))
                        array_push($array_mesi, $prov['timecreated']);

                    $mesi[$prov['timecreated']] = $this->convertSeconds($cumulatoTot);

                    $prov = $singolo;
                    $prov['mesi'] = $mesi;

                    if (strpos($prov['other'], "RESET") !== FALSE) {
//                        echo "qui";
                        $cumulatoTot -= $seconds_singolo;
                    }

                    $prov['tempo_cumulato'] = $this->convertSeconds($cumulatoTot);

//                    var_dump($prov['tempo_cumulato']);

//                    if($singolo['userid'] == 2165){
//                        var_dump($mesi);
////                        var_dump($data_singolo);
//                    }else{
//                        continue;
//                    }

//                    var_dump($cumulatoTot);

                } else {
                    if ($cumulatoTot == 0) {
                        $mesi[$prov['timecreated']] = $prov['diff'];
                    } else {
                        $mesi[$prov['timecreated']] = $prov['tempo_cumulato'];
                    }
                    $mesi[$singolo['timecreated']] = $singolo['tempo_cumulato'];
                    if (!in_array($prov['timecreated'], $array_mesi))
                        array_push($array_mesi, $prov['timecreated']);
                    if (!in_array($singolo['timecreated'], $array_mesi))
                        array_push($array_mesi, $singolo['timecreated']);

                    $prov = $singolo;
                    $prov['mesi'] = $mesi;
                    $cumulatoTot = 0;
                }
            } else {
                if (isset($prov['mesi'])) {
                    foreach ($prov['mesi'] as $tempo) {
                        $tempo = explode(":", $tempo);
                        $secondi = $tempo[0] * 3600 + $tempo[1] * 60 + $tempo[2];
                        $tempo_tot += $secondi;
                    }
                } else {
                    $mesi[$prov['timecreated']] = $prov['tempo_cumulato'];
                    $prov['mesi'] = $mesi;
                    if (!in_array($prov['timecreated'], $array_mesi))
                        array_push($array_mesi, $prov['timecreated']);
                    $tempo = explode(":", $prov['tempo_cumulato']);
                    $secondi = $tempo[0] * 3600 + $tempo[1] * 60 + $tempo[2];
                    $tempo_tot = $secondi;
                }

                $prov['tempo_tot'] = $this->convertSeconds($tempo_tot);

                array_push($new, $prov);
                $tempo_tot = 0;
                $cumulatoTot = 0;
                $prov = $singolo;
                $mesi = null;
            }
            if ($cont == $totResult) {
                if (isset($prov['mesi'])) {
                    foreach ($prov['mesi'] as $tempo) {
                        $tempo = explode(":", $tempo);
                        $secondi = $tempo[0] * 3600 + $tempo[1] * 60 + $tempo[2];
                        $tempo_tot += $secondi;
                    }
                } else {
                    $mesi[$prov['timecreated']] = $prov['tempo_cumulato'];
                    $prov['mesi'] = $mesi;
                    if (!in_array($prov['timecreated'], $array_mesi))
                        array_push($array_mesi, $prov['timecreated']);
                    $tempo = explode(":", $prov['tempo_cumulato']);
                    $secondi = $tempo[0] * 3600 + $tempo[1] * 60 + $tempo[2];
                    $tempo_tot = $secondi;
                }
                $prov['tempo_tot'] = $this->convertSeconds($tempo_tot);
                array_push($new, $prov);
            }
        }


        usort($array_mesi, "compareByTimeStampNew");

//        var_dump($new);
//        exit;


        foreach ($new as &$singolo) {
            foreach ($array_mesi as $mese) {
                if (!in_array($mese, array_keys($singolo['mesi']))) {
                    $singolo['mesi'][$mese] = "00:00:00";
                }
            }
            uksort($singolo['mesi'], "compareByTimeStampNew");
        }
        $return = json_encode($new) . "*" . json_encode($array_mesi);

//        var_dump($array_mesi); exit;

        return $return;
    }

    public function constructSHvpReportOriginal($array) {

        $prov = null;
        $mesi = array();
        $new = array();
        $cumulatoTot = 0;
        $tempo_tot = 0;
        $cont = 0;
        $totResult = count($array);
        $array_mesi = array();
        $temp = array();

        foreach ($array as $singolo) {

            if ($totResult == 1) {
                $data_singolo = str_replace("/", "", $singolo['timecreated']);
                $timesingolo_split = explode(":", $singolo['diff']);
                $seconds_singolo = $timesingolo_split[0] * 3600 + $timesingolo_split[1] * 60 + $timesingolo_split[2];
                $mesi[$singolo['timecreated']] = $singolo['tempo_cumulato'];
                array_push($array_mesi, $singolo['timecreated']);
                $tempo = explode(":", $singolo['tempo_cumulato']);
                $secondi = $tempo[0] * 3600 + $tempo[1] * 60 + $tempo[2];
                $tempo_tot = $secondi;
                $singolo['mesi'] = $mesi;
                $singolo['tempo_tot'] = $this->convertSeconds($tempo_tot);
                array_push($new, $singolo);

                break;
            }

            if ($prov == null) {
                $prov = $singolo;
                $cont++;
                continue;
            }

//            if($singolo['userid'] == 6924 && $singolo['timecreated'] == '29/09/2022'){
//                var_dump($singolo['id']);
//                var_dump($singolo['tempo_cumulato']);
//                echo "-------\n";
//            }

            $cont++;
            $data_prov = str_replace("/", "", $prov['timecreated']);
            $data_singolo = str_replace("/", "", $singolo['timecreated']);
            $timeprov_split = explode(":", $prov['diff']);
            $timesingolo_split = explode(":", $singolo['diff']);
//            $timeprov_split = explode(":", ($prov['diff'] > 0) ? $prov['diff'] : $prov['tempo_cumulato'] );
//            $timesingolo_split = explode(":", ($singolo['diff'] > 0) ? $singolo['diff'] : $singolo['tempo_cumulato'] );
            $seconds_prov = $timeprov_split[0] * 3600 + $timeprov_split[1] * 60 + $timeprov_split[2];
            $seconds_singolo = $timesingolo_split[0] * 3600 + $timesingolo_split[1] * 60 + $timesingolo_split[2];

//            if($singolo['userid'] == 10786 && $singolo['timecreated'] == '13/07/2022'){
////                        if($singolo['id'] == 7115398){
////                            var_dump($data_prov);
////                            var_dump($data_singolo);
//                var_dump($cumulatoTot);
////                            var_dump($seconds_prov);
////                            var_dump($seconds_singolo);
////                            exit;
////                        }
//            }

            if ($prov['userid'] == $singolo['userid']) {


                if ($data_prov == $data_singolo) {

//                    if($singolo['id'] == 7115398){
//                        var_dump($prov);
//                        exit;
//                    }

                    if ($cumulatoTot == 0) {
                        $cumulatoTot += $seconds_prov + $seconds_singolo;
                    } else {
                        $cumulatoTot += $seconds_singolo;
                    }
                    if (!in_array($prov['timecreated'], $array_mesi))
                        array_push($array_mesi, $prov['timecreated']);

                    $mesi[$prov['timecreated']] = $this->convertSeconds($cumulatoTot);

                    $prov = $singolo;
                    $prov['mesi'] = $mesi;
                    $prov['tempo_cumulato'] = $this->convertSeconds($cumulatoTot);
                } else {
                    if ($cumulatoTot == 0) {
                        $mesi[$prov['timecreated']] = $prov['diff'];
                    } else {
                        $mesi[$prov['timecreated']] = $prov['tempo_cumulato'];
                    }
                    $mesi[$singolo['timecreated']] = $singolo['tempo_cumulato'];
                    if (!in_array($prov['timecreated'], $array_mesi))
                        array_push($array_mesi, $prov['timecreated']);
                    if (!in_array($singolo['timecreated'], $array_mesi))
                        array_push($array_mesi, $singolo['timecreated']);

                    $prov = $singolo;
                    $prov['mesi'] = $mesi;
                    $cumulatoTot = 0;
                }
            } else {
                if (isset($prov['mesi'])) {
                    foreach ($prov['mesi'] as $tempo) {
                        $tempo = explode(":", $tempo);
                        $secondi = $tempo[0] * 3600 + $tempo[1] * 60 + $tempo[2];
                        $tempo_tot += $secondi;
                    }
                } else {
                    $mesi[$prov['timecreated']] = $prov['tempo_cumulato'];
                    $prov['mesi'] = $mesi;
                    if (!in_array($prov['timecreated'], $array_mesi))
                        array_push($array_mesi, $prov['timecreated']);
                    $tempo = explode(":", $prov['tempo_cumulato']);
                    $secondi = $tempo[0] * 3600 + $tempo[1] * 60 + $tempo[2];
                    $tempo_tot = $secondi;
                }

                $prov['tempo_tot'] = $this->convertSeconds($tempo_tot);

                array_push($new, $prov);
                $tempo_tot = 0;
                $cumulatoTot = 0;
                $prov = $singolo;
                $mesi = null;
            }
            if ($cont == $totResult) {
                if (isset($prov['mesi'])) {
                    foreach ($prov['mesi'] as $tempo) {
                        $tempo = explode(":", $tempo);
                        $secondi = $tempo[0] * 3600 + $tempo[1] * 60 + $tempo[2];
                        $tempo_tot += $secondi;
                    }
                } else {
                    $mesi[$prov['timecreated']] = $prov['tempo_cumulato'];
                    $prov['mesi'] = $mesi;
                    if (!in_array($prov['timecreated'], $array_mesi))
                        array_push($array_mesi, $prov['timecreated']);
                    $tempo = explode(":", $prov['tempo_cumulato']);
                    $secondi = $tempo[0] * 3600 + $tempo[1] * 60 + $tempo[2];
                    $tempo_tot = $secondi;
                }
                $prov['tempo_tot'] = $this->convertSeconds($tempo_tot);
                array_push($new, $prov);
            }
        }

//        var_dump($new);
//        exit;


        usort($array_mesi, "compareByTimeStamp");


        foreach ($new as &$singolo) {
            foreach ($array_mesi as $mese) {
                if (!in_array($mese, array_keys($singolo['mesi']))) {
                    $singolo['mesi'][$mese] = "00:00:00";
                }
            }
            uksort($singolo['mesi'], "compareByTimeStamp");
        }
        $return = json_encode($new) . "*" . json_encode($array_mesi);
        return $return;
    }

    public function groupByArrayEnzo($arr, $criteria): array
    {
        return array_reduce($arr, function($accumulator, $item) use ($criteria) {
            $key = (is_callable($criteria)) ? $criteria($item) : $item[$criteria];
            if (!array_key_exists($key, $accumulator)) {
                $accumulator[$key] = [];
            }

            array_push($accumulator[$key], $item);
            return $accumulator;
        }, []);
    }

//    public function constructSHvpReportNewEnzo($array) {
//
//        $newArray = array();
//        $mesi = array();
//        $totResult = count($array);
//        $array_mesi = array();
//        $array_mesi_return = array();
//        $cont = 0;
//        $tempo_tot = 0;
//        $new = array();
//
////        var_dump($array); exit; //1566 array
//
////        foreach ($array as $singolo) {
////            if(!array_key_exists($singolo['userid'], $newArray)){
////                array_push($newArray, $singolo);
////            }
////
//////            $newArray[$singolo['userid']][] = $singolo;
////        }
//
//        $newArray = $this->groupByArrayEnzo($array, 'userid');
//
////        var_dump($newArray); exit; //57 array
//
//        foreach($newArray as $arr){
//
//            foreach($arr as $r){
//
////                echo $r['userid'] . "\n";
//
//                $tempo_generale = 0;
//
//                if (!array_key_exists($r['timecreated'], $mesi)) {
//                    $tempo_tot = 0;
//                }
//
//                $tempo = explode(":", $r['tempo_cumulato']);
//                $secondi = $tempo[0] * 3600 + $tempo[1] * 60 + $tempo[2];
//                $tempo_tot += $secondi;
//                $mesi[$r['timecreated']] = $this->convertSeconds($tempo_tot);
//
//                if (!in_array($r['timecreated'], $array_mesi_return))
//                    array_push($array_mesi_return, $r['timecreated']);
//
////                $r['mesi'] = $mesi;
////
////                array_push($array_mesi, $r);
//            }
//
////            var_dump($mesi); exit;
//
//            foreach ($mesi as $tempo) {
////                if($r['userid'] == 2413){
////                    var_dump($tempo);
////                }
//
//                $tempo = explode(":", $tempo);
//                $secondi = $tempo[0] * 3600 + $tempo[1] * 60 + $tempo[2];
//
////                if($r['userid'] == 2413){
////                    var_dump($tempo);
////                }
//
//                $tempo_generale += $secondi;
//
////                if($r['userid'] == 2413){
////                    var_dump($secondi);
////                    var_dump($tempo_generale);
////                    exit;
////                }
//            }
//
////            foreach ($array_mesi_return as $mese) {
////                if (!in_array($mese, array_keys($arr['mesi']))) {
////                    $arr['mesi'][$mese] = "00:00:00";
////                }
////            }
//
////            var_dump($arr); exit; //4 array
//
//            foreach($arr as $r){
//
////                $newArray[$r['userid']]['mesi'] = $mesi;
////                $newArray[$r['userid']]['tempo_tot'] = $this->convertSeconds($tempo_generale);
//
////                echo "primo\n";
////                echo $r['userid'] . "\n";
//                if (!in_array($r['userid'], $array_mesi)) {
////                    echo "secondo\n";
////                    echo $r['userid'] . "\n";
////                    echo "Nome: " . $r['name'] . "\n";
//                    $r['mesi'] = $mesi;
//                    $r['tempo_tot'] = $this->convertSeconds($tempo_generale);
//                    $array_mesi = $r;
//                    array_push($new, $array_mesi);
//                }else{
//                    $r['mesi'] = $mesi;
//                    $r['tempo_tot'] = $this->convertSeconds($tempo_generale);
//                    $array_mesi['userid'] = $r;
//                    array_push($new, $array_mesi);
//                }
//
////                echo "------\n\n";
//            }
//
//            var_dump($array_mesi); exit;
//
//            $mesi = [];
////            unset($array_mesi_return);
//
//        }
//
////        var_dump($array_mesi);exit;
//
//        usort($array_mesi_return, "compareByTimeStampNew");
//
//        foreach ($new as &$singolo) {
//            foreach ($array_mesi_return as $mese) {
//                if (!in_array($mese, array_keys($singolo['mesi']))) {
//                    $singolo['mesi'][$mese] = "00:00:00";
//                }
//            }
//            uksort($singolo['mesi'], "compareByTimeStampNew");
//        }
//
////        var_dump($array_mesi_return); exit;
//
//        $return = json_encode($new) . "*" . json_encode($array_mesi_return);
//        return $return;
//    }


    public function constructSHvpReportFinal($array, $db)
    {
        $mesi = array();
        $arrControlFinal = array();
        $cumulatoTot = 0;
        $totResult = count($array);
        $array_mesi = array();
        $new = array();
        $tmp = array();
        $mesi_temp = array();
        $submitted = false;
        $temp_parz = 0;

        foreach ($array as $singolo) {

            if ($totResult == 1) {
                $data_singolo = str_replace("/", "", $singolo['timecreated']);
                $mesi[$singolo['timecreated']] = $singolo['tempo_cumulato'];
                array_push($array_mesi, $singolo['timecreated']);
                $tempo = explode(":", $singolo['tempo_cumulato']);
                $secondi = $tempo[0] * 3600 + $tempo[1] * 60 + $tempo[2];
                $tempo_tot = $secondi;
                $singolo['mesi'] = $mesi;
                $singolo['tempo_tot'] = $this->convertSeconds($tempo_tot);
                array_push($new, $singolo);

                break;
            }

            $data_singolo = str_replace("/", "", $singolo['timecreated']);
            $timesingolo_split = explode(":", $singolo['tempo_cumulato']);
            $seconds_singolo = $timesingolo_split[0] * 3600 + $timesingolo_split[1] * 60 + $timesingolo_split[2];

            //check se lezione finita
            if ($singolo['action'] == "submitted") {
                $submitted = true;
                $singolo['lezione_finita'] = $submitted;
            }

            $key = $singolo['userid'].'-'.$data_singolo;

            if (!array_key_exists($key, $arrControlFinal)) {
                $cumulatoTot += $seconds_singolo;
                $singolo['comulato'] = $cumulatoTot;
            }else{
                $newCumulato = $arrControlFinal[$key]['comulato'] + $seconds_singolo;
                $singolo['comulato'] = $newCumulato;
            }

            $singolo['tempo_cumulato'] = $this->convertSeconds($singolo['comulato']);


            if (!in_array($singolo['timecreated'], $array_mesi))
                array_push($array_mesi, $singolo['timecreated']);



//            $tempo_tot += $tmp['comulato'];
            $singolo['tempo_tot'] = $singolo['tempo_cumulato'];
            $arrControlFinal[$key] = $singolo;

            $cumulatoTot = 0;

        }

//        var_dump($arrControlFinal); exit;

        $array2Control = array();
        $tempo_tot = 0;
        $mesi = array();
        foreach($arrControlFinal as $control){

            $tempo_tot += $control['comulato'];

            $key = $control['userid'];

            $mesi[$key][$control['timecreated']] = $this->convertSeconds($control['comulato']);
            $array2Control[$key] = $control;
            $array2Control[$key]['mesi'] = $mesi[$key];
            $array2Control[$key]['tempo_tot'] = $tempo_tot;
        }

        foreach($array2Control as $control){
            $cumulatoTot = 0;
            foreach($control['mesi'] as $mesi){
                $timesingolo_split = explode(":", $mesi);
                $seconds_singolo = $timesingolo_split[0] * 3600 + $timesingolo_split[1] * 60 + $timesingolo_split[2];
                $cumulatoTot += $seconds_singolo;
            }

            $control['tempo_tot'] = $this->convertSeconds($cumulatoTot);
            array_push($new, $control);
        }

        usort($array_mesi, "compareByTimeStampNew");

        foreach ($new as &$singolo) {
            //TODO aggiunto controllo array vuoto
            foreach ($array_mesi as $mese) {
                if ($mese != '' && !in_array($mese, array_keys($singolo['mesi']))) {
                    $singolo['mesi'][$mese] = "00:00:00";
                }
            }
            uksort($singolo['mesi'], "compareByTimeStampNew");
        }

//        var_dump($new); exit;

        $return = json_encode($new) . "*" . json_encode($array_mesi);

        return $return;
    }

    public function constructSHvpReportNewEnzo($array) {

        $prov = null;
        $mesi = array();
        $new = array();
        $cumulatoTot = 0;
        $tempo_tot = 0;
        $cont = 0;
        $totResult = count($array);
        $array_mesi = array();
        $temp = array();

//        var_dump($array); exit;

        foreach ($array as $singolo) {

//            var_dump($singolo); exit;

            if($singolo['diff'] == 0){
                $singolo['diff'] = '00:00:00';
            }

//            //enzo
//            if($singolo['userid'] == 24 && $singolo['timecreated'] == '20/07/2022'){
//                var_dump($singolo);
//                continue;
//            }

            if ($totResult == 1) {
                $data_singolo = str_replace("/", "", $singolo['timecreated']);
//                $timesingolo_split = explode(":", $singolo['diff']);
//                $seconds_singolo = $timesingolo_split[0] * 3600 + $timesingolo_split[1] * 60 + $timesingolo_split[2];
                $mesi[$singolo['timecreated']] = $singolo['tempo_cumulato'];
                array_push($array_mesi, $singolo['timecreated']);
                $tempo = explode(":", $singolo['tempo_cumulato']);
                $secondi = $tempo[0] * 3600 + $tempo[1] * 60 + $tempo[2];
                $tempo_tot = $secondi;
                $singolo['mesi'] = $mesi;
                $singolo['tempo_tot'] = $this->convertSeconds($tempo_tot);
                array_push($new, $singolo);

                break;
            }

            if ($prov == null) {
                $prov = $singolo;
                $cont++;
                continue;
            }
            $cont++;
            $data_prov = str_replace("/", "", $prov['timecreated']);
            $data_singolo = str_replace("/", "", $singolo['timecreated']);
            $timeprov_split = explode(":", $prov['tempo_cumulato']);
            $timesingolo_split = explode(":", $singolo['tempo_cumulato']);
            $seconds_prov = $timeprov_split[0] * 3600 + $timeprov_split[1] * 60 + $timeprov_split[2];
            $seconds_singolo = $timesingolo_split[0] * 3600 + $timesingolo_split[1] * 60 + $timesingolo_split[2];

            if ($prov['userid'] == $singolo['userid']) {

                if ($data_prov == $data_singolo) {

//                    echo $prov['id'] . "\n";
//                    echo $singolo['id'] . "\n";
//                    exit;

//                    var_dump($prov['diff']);

//                    print_r($prov['diff']); echo "<br><br>";
//                    var_dump($seconds_prov);
//                    var_dump($seconds_singolo);

                    if ($cumulatoTot == 0) {
                        $cumulatoTot += $seconds_prov + $seconds_singolo;
//                        $cumulatoTot += $seconds_singolo;
                    } else {
                        $cumulatoTot += $seconds_singolo;
                    }

//                    var_dump($cumulatoTot); exit;

                    if (!in_array($prov['timecreated'], $array_mesi))
                        array_push($array_mesi, $prov['timecreated']);

                    $mesi[$prov['timecreated']] = $this->convertSeconds($cumulatoTot);

                    $prov = $singolo;
                    $prov['mesi'] = $mesi;

//                    if (strpos($prov['other'], "RESET") !== FALSE) {
//                        echo "qui";
//                        $cumulatoTot -= $seconds_singolo;
//                    }

                    $prov['tempo_cumulato'] = $this->convertSeconds($cumulatoTot);

//                    var_dump($prov['tempo_cumulato']); exit;

//                    if($singolo['userid'] == 2165){
//                        var_dump($mesi);
////                        var_dump($data_singolo);
//                    }else{
//                        continue;
//                    }

//                    var_dump($cumulatoTot);

                } else {
//                    echo $prov['id'] . "\n";
//                    echo $singolo['id'] . "\n";
//                    exit;
//                    if ($cumulatoTot == 0) {
//                        $mesi[$prov['timecreated']] = $prov['tempo_cumulato'];
//                    } else {
                    $mesi[$prov['timecreated']] = $prov['tempo_cumulato'];
//                    }
                    $mesi[$singolo['timecreated']] = $singolo['tempo_cumulato'];
                    if (!in_array($prov['timecreated'], $array_mesi))
                        array_push($array_mesi, $prov['timecreated']);
//                    if (!in_array($singolo['timecreated'], $array_mesi))
//                        array_push($array_mesi, $singolo['timecreated']);

                    $prov = $singolo;
                    $prov['mesi'] = $mesi;
                    $cumulatoTot = 0;
                }
            } else {
                if (isset($prov['mesi'])) {
                    foreach ($prov['mesi'] as $tempo) {
                        $tempo = explode(":", $tempo);
                        $secondi = $tempo[0] * 3600 + $tempo[1] * 60 + $tempo[2];
                        $tempo_tot += $secondi;
                    }
                } else {
                    $mesi[$prov['timecreated']] = $prov['tempo_cumulato'];
                    $prov['mesi'] = $mesi;
                    if (!in_array($prov['timecreated'], $array_mesi))
                        array_push($array_mesi, $prov['timecreated']);
                    $tempo = explode(":", $prov['tempo_cumulato']);
                    $secondi = $tempo[0] * 3600 + $tempo[1] * 60 + $tempo[2];
                    $tempo_tot = $secondi;
                }

                $prov['tempo_tot'] = $this->convertSeconds($tempo_tot);

                array_push($new, $prov);
                $tempo_tot = 0;
                $cumulatoTot = 0;
                $prov = $singolo;
                $mesi = null;
            }
            if ($cont == $totResult) {
                if (isset($prov['mesi'])) {
                    foreach ($prov['mesi'] as $tempo) {
                        $tempo = explode(":", $tempo);
                        $secondi = $tempo[0] * 3600 + $tempo[1] * 60 + $tempo[2];
                        $tempo_tot += $secondi;
                    }
                } else {
                    $mesi[$prov['timecreated']] = $prov['tempo_cumulato'];
                    $prov['mesi'] = $mesi;
                    if (!in_array($prov['timecreated'], $array_mesi))
                        array_push($array_mesi, $prov['timecreated']);
                    $tempo = explode(":", $prov['tempo_cumulato']);
                    $secondi = $tempo[0] * 3600 + $tempo[1] * 60 + $tempo[2];
                    $tempo_tot = $secondi;
                }
                $prov['tempo_tot'] = $this->convertSeconds($tempo_tot);
                array_push($new, $prov);
            }
        }


        usort($array_mesi, "compareByTimeStampNew");

//        var_dump($new);
//        exit;


        foreach ($new as &$singolo) {
            foreach ($array_mesi as $mese) {
                if (!in_array($mese, array_keys($singolo['mesi']))) {
                    $singolo['mesi'][$mese] = "00:00:00";
                }
            }
            uksort($singolo['mesi'], "compareByTimeStampNew");
        }
        $return = json_encode($new) . "*" . json_encode($array_mesi);

//        var_dump($array_mesi); exit;

        return $return;
    }

    public function constructSHvpReportNew($array) {

        $newArray = array();
        $mesi = array();
        $totResult = count($array);
        $array_mesi = array();
        $array_mesi_return = array();
        $cont = 0;
        $tempo_tot = 0;
        $new = array();

//        var_dump($array); exit; //1566 array

        foreach ($array as $singolo) {
            $newArray[$singolo['userid']][] = $singolo;
        }

//        var_dump($newArray); exit; //57 array

        foreach($newArray as $arr){

            foreach($arr as $r){

                $tempo_generale = 0;

                if (!array_key_exists($r['timecreated'], $mesi)) {
                    $tempo_tot = 0;
                }

                $tempo = explode(":", $r['tempo_cumulato']);
                $secondi = $tempo[0] * 3600 + $tempo[1] * 60 + $tempo[2];
                $tempo_tot += $secondi;
                $mesi[$r['timecreated']] = $this->convertSeconds($tempo_tot);

                if (!in_array($r['timecreated'], $array_mesi_return))
                    array_push($array_mesi_return, $r['timecreated']);

//                $r['mesi'] = $mesi;
//
//                array_push($array_mesi, $r);
            }

//            var_dump($mesi); exit;

            foreach ($mesi as $tempo) {
//                if($r['userid'] == 2413){
//                    var_dump($tempo);
//                }

                $tempo = explode(":", $tempo);
                $secondi = $tempo[0] * 3600 + $tempo[1] * 60 + $tempo[2];

//                if($r['userid'] == 2413){
//                    var_dump($tempo);
//                }

                $tempo_generale += $secondi;

//                if($r['userid'] == 2413){
//                    var_dump($secondi);
//                    var_dump($tempo_generale);
//                    exit;
//                }
            }

//            foreach ($array_mesi_return as $mese) {
//                if (!in_array($mese, array_keys($arr['mesi']))) {
//                    $arr['mesi'][$mese] = "00:00:00";
//                }
//            }

//            var_dump($arr); exit; //4 array

            foreach($arr as $r){
                if (!in_array($r['userid'], $array_mesi)) {
                    $r['mesi'] = $mesi;
                    $r['tempo_tot'] = $this->convertSeconds($tempo_generale);
                    $array_mesi = $r;
                    array_push($new, $array_mesi);
                }
            }

            $mesi = [];
//            unset($array_mesi_return);

        }

        usort($array_mesi_return, "compareByTimeStampNew");

        foreach ($new as &$singolo) {
            foreach ($array_mesi_return as $mese) {
                if (!in_array($mese, array_keys($singolo['mesi']))) {
                    $singolo['mesi'][$mese] = "00:00:00";
                }
            }
            uksort($singolo['mesi'], "compareByTimeStampNew");
        }

//        var_dump($array_mesi_return); exit;

        $return = json_encode($new) . "*" . json_encode($array_mesi_return);
        return $return;
    }

    public function convertSeconds($seconds) {
        $hours = floor($seconds / 3600);
        $mins = floor($seconds / 60 % 60);
        $secs = floor($seconds % 60);
        $timeFormat = sprintf('%02d:%02d:%02d', $hours, $mins, $secs);
        return $timeFormat;
    }

    public function convertTimeToSeconds($str_time){
        sscanf($str_time, "%d:%d:%d", $hours, $minutes, $seconds);
        $time_seconds = 0;
        if(isset($hours))   $time_seconds += $hours*3600;
        if(isset($minutes)) $time_seconds += $minutes*60;
        if(isset($seconds)) $time_seconds += $seconds;
        return $time_seconds;
    }


    public function getRuoliCorsi($db) {
        $collection = $this->conn->$db->corsi_ruoli;
        $cursore = $collection->find(
            [],
            ['projection' => ['userid' => 1, 'ruolo' => 1]]
        );
        $ruoli = array();

        foreach ($cursore as $singolo) {
            $ruolo = "";
            switch ($singolo['ruolo']) {
                case 'student':
                    $ruolo = "studente";
                    break;
                case 'manager':
                    $ruolo = "manager";
                    break;
                case 'editingteacher':
                    $ruolo = "docente";
                    break;
                case 'teacher':
                    $ruolo = "controller";
                    break;
                default:break;
            }
            $ruoli[$singolo['userid']] = $ruolo;
        }

        return $ruoli;
    }

    public function getAggregateBBB($db, $inizio, $fine, $idCorso = null, $array_id = null) {
        $collection = $this->conn->$db->mdl_logstore_standard_log;

        if (isset($idCorso) && isset($array_id)) {
            if ($idCorso == 0) {
                $match = ['$match' => [
                    'userid' => [
                        '$in' => $array_id
                    ],
                    'component' => "mod_bigbluebuttonbn",
                    'timecreated' => [
                        '$gte' => $inizio,
                        '$lte' => $fine
                    ],
                    'action' => [
                        '$ne' => 'viewed'
                    ]
                ]];
            } else if (count($array_id) == 0) {
                $match = ['$match' => [
                    'courseid' => (int) $idCorso,
                    'component' => "mod_bigbluebuttonbn",
                    'timecreated' => [
                        '$gte' => $inizio,
                        '$lte' => $fine
                    ],
                    'action' => [
                        '$ne' => 'viewed'
                    ]
                ]];
            } else {
                $match = ['$match' => [
                    'userid' => [
                        '$in' => $array_id
                    ],
                    'courseid' => (int) $idCorso,
                    'component' => "mod_bigbluebuttonbn",
                    'timecreated' => [
                        '$gte' => $inizio,
                        '$lte' => $fine
                    ],
                    'action' => [
                        '$ne' => 'viewed'
                    ]
                ]];
            }
        } else if (isset($idCorso) && !isset($array_id)) {
            $match = ['$match' => [
                'courseid' => (int) $idCorso,
                'component' => "mod_bigbluebuttonbn",
                'timecreated' => [
                    '$gte' => $inizio,
                    '$lte' => $fine
                ],
                'action' => [
                    '$ne' => 'viewed'
                ]
            ]];
        } else if (!isset($idCorso) && isset($array_id)) {
            $match = ['$match' => [
                'userid' => [
                    '$in' => $array_id
                ],
                'component' => "mod_bigbluebuttonbn",
                'timecreated' => [
                    '$gte' => $inizio,
                    '$lte' => $fine
                ],
                'action' => [
                    '$ne' => 'viewed'
                ]
            ]];
        }
        $pipeline = [$match,
            ['$lookup' => [
                'from' => 'mdl_utenti_complete',
                'localField' => 'userid',
                'foreignField' => 'userid',
                'as' => 'info'
            ]], ['$set' => [
                'username' => [
                    '$arrayElemAt' => [
                        '$info.username',
                        0
                    ]
                ],
                'email' => [
                    '$arrayElemAt' => [
                        '$info.email',
                        0
                    ]
                ],
                'cf' => [
                    '$arrayElemAt' => [
                        '$info.idnumber',
                        0
                    ]
                ],
                'nome' => [
                    '$arrayElemAt' => [
                        '$info.nome',
                        0
                    ]
                ],
                'citta' => [
                    '$arrayElemAt' => [
                        '$info.citta',
                        0
                    ]
                ],
                'data' => [
                    '$arrayElemAt' => [
                        '$info.data',
                        0
                    ]
                ],
                'azienda' => [
                    '$arrayElemAt' => [
                        '$info.azienda',
                        0
                    ]
                ],
                'ruolo' => [
                    '$arrayElemAt' => [
                        '$info.ruolo',
                        0
                    ]
                ],
                'phone2' => [
                    '$arrayElemAt' => [
                        '$info.phone2',
                        0
                    ]
                ],
                'phone1' => [
                    '$arrayElemAt' => [
                        '$info.phone1',
                        0
                    ]
                ],
            ]], ['$project' => [
                'info' => 0
            ]]];

        $out = $collection->aggregate($pipeline);
        $res = $out->toArray();
        return $res;
    }

    public function constructBbbArray($array, $bbbName, $nameCorsi) {
        $result = array();

        foreach ($array as $singolo) {
            $tmp = array();
            if (!isset($singolo['nome'])) continue;

            if(isset($bbbName[$singolo['objectid']])){
                $tmp['aula'] = $bbbName[$singolo['objectid']];
            }else{
                //continue;
                $tmp['aula'] = '';
            }

            if (isset($nameCorsi[$singolo['courseid']])){
                $tmp['corso'] = $nameCorsi[$singolo['courseid']];
            }else{
                //continue;
                $tmp['corso'] = '';
            }


            $tmp['id'] = $singolo['id'];
            $tmp['userid'] = $singolo['userid'];

            $name = $singolo['nome'];
            $tmp['name'] = $name;
            $tmp['username'] = $singolo['username'];
            $tmp['email'] = $singolo['email'];

            $tmp['phone'] = "n/d";
            if (isset($singolo['phone1']) && $singolo['phone1'] != "")
                $tmp['phone'] = $singolo['phone1'];
            if (isset($singolo['phone1']) && $singolo['phone2'] != "")
                $tmp['phone'] = $singolo['phone2'];
            if (isset($singolo['cf']))
                $tmp['cf'] = $singolo['cf'];
            else
                $tmp['cf'] = "n/d";

            if (!isset($singolo['data']))
                $tmp['data_nascita'] = "n/d";
            else
                $tmp['data_nascita'] = date("d/m/Y", $singolo['data']);
            if (!isset($singolo['citta']))
                $tmp['citta'] = "n/d";
            else
                $tmp['citta'] = $singolo['citta'];
            $time = date("d/m/Y", $singolo['timecreated']);
            $tmp['timecreated'] = $time;
            $orario = date("H:i:s", $singolo['timecreated']);
            $tmp['timestamp'] = $singolo['timecreated'];
            $tmp['orario'] = $orario;
            $tmp['ip'] = $singolo['ip'];
            $tmp['action'] = $singolo['action'];
            $tmp['objectid'] = $singolo['objectid'];
            $tmp['courseid'] = $singolo['courseid'];
            if (isset($singolo['azienda']))
                $tmp['azienda'] = $singolo['azienda'];
            else
                $tmp['azienda'] = "n/d";
            $ruolo = "n/d";
            switch ($singolo['ruolo']) {
                case 'student':
                    $ruolo = "studente";
                    break;
                case 'manager':
                    $ruolo = "manager";
                    break;
                case 'editingteacher':
                    $ruolo = "docente";
                    break;
                case 'teacher':
                    $ruolo = "controller";
                    break;
                default:break;
            }
            $tmp['ruolo'] = $ruolo;
            array_push($result, $tmp);
        }
        return $result;
    }

    public function getEnrole($userid, $array) {

        if (isset($array[$userid])) {
            return $array[$userid];
        }
    }

    public function setBbbSheet($index, $spreadsheet, $letter, $filtri, $j, $finale) {
        //INIZIO RE1-T2 Inserimento titolo del corso in intestazione (setBbbSheet)
        $course_filtered = false;
        if(isset($finale[0]['corso']) && $course_filtered){
            //Imposto il titolo
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Corso seguito : " . $finale[0]['corso']);

            //Unisco le celle
            $all_letters = range('A', 'Z');
            $merge_to_index = 16+$letter-count($filtri);
            if($merge_to_index >= count($all_letters)) $merge_to_index = count($all_letters)-1;
            $sheet = $spreadsheet->getActiveSheet();
            $sheet->MergeCells($letter . $index . ':' . $all_letters[$merge_to_index] . $index);
            $index += 2;
        }
        //FINE RE1-T2

        if (!in_array("data", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Data");
            $letter++;
        }
        if (!in_array("userid", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Userid");
            $letter++;
        }
        if (!in_array("username", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Username");
            $letter++;
        }
        if (!in_array("name", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Nome Cognome");
            $letter++;
        }
        if (!in_array("cf", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Codice Fiscale");
            $letter++;
        }
        if (!in_array("email", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Email");
            $letter++;
        }
        if (!in_array("dn", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Data di nascita");
            $letter++;
        }
        if (!in_array("ln", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Luogo di nascita");
            $letter++;
        }
        if (!in_array("azienda", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Azienda");
            $letter++;
        }
        if (!in_array("phone", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "N.di telefono");
            $letter++;
        }
        if (!in_array("ruolo", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Ruolo");
            $letter++;
        }
        if (!in_array("corso", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Corso");
            $letter++;
        }
        if (!in_array("aula", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Nome Aula");
            $letter++;
        }
        if (!in_array("orarioA", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Ora ingresso");
            $letter++;
        }
        if (!in_array("orarioL", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Ora uscita");
            $letter++;
        }
        if (!in_array("tc", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Tempo cumulato");
        }




        $spreadsheet->getActiveSheet()->setTitle("Report");


        $from = "A1";
        $to = $letter . $index;
        $spreadsheet->getActiveSheet()->getStyle("$from:$to")->getFont()->setBold(true);

        $i = $index + 1;
        foreach ($finale as $singolo) {
            $letter = 'A';
            if (!in_array("data", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['timecreated']);
                $letter++;
            }
            if (!in_array("userid", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['userid']);
                $letter++;
            }
            if (!in_array("username", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['username']);
                $letter++;
            }
            if (!in_array("name", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['name']);
                $letter++;
            }

            if (!in_array("cf", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, @$singolo['cf']);
                $letter++;
            }

            if (!in_array("email", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["email"]);
                $letter++;
            }

            if (!in_array("dn", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, @$singolo["data_nascita"]);
                $letter++;
            }

            if (!in_array("ln", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, @$singolo["citta"]);
                $letter++;
            }

            if (!in_array("azienda", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, @$singolo["azienda"]);
                $letter++;
            }
            if (!in_array("phone", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, @$singolo["phone"]);
                $letter++;
            }

            if (!in_array("ruolo", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["ruolo"]);
                $letter++;
            }
            if (!in_array("corso", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["corso"]);
                $letter++;
            }
            if (!in_array("aula", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["aula"]);
                $letter++;
            }
            if (!in_array("orarioA", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["orario"]);
                $letter++;
            }
            if (!in_array("orarioL", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["orario_logout"]);
                $letter++;
            }
            if (!in_array("tc", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo["tempo_cumulato"]);
            }


            $i++;
        }
        foreach (range('A', $letter) as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        $spreadsheet->getActiveSheet()->getStyle("A" . $index . ":" . $letter . $index)
            ->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('00FF7F');

        return $i;
    }

    public function setSintesiBbbSheet($index, $spreadsheet, $letter, $filtri, $j, $new, $array_mesi, $fromReport = null) {
        //INIZIO RE1-T2 Inserimento titolo del corso in intestazione (setSintesiBbbSheet)
        $course_filtered = false;
        if(isset($fromReport)) $course_filtered = true;
        if(isset($new[0]['corso']) && $course_filtered){
            //Imposto il titolo
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Corso seguito : " . $new[0]['corso']);

            //Unisco le celle
            $all_letters = range('A', 'Z');
            $merge_to_index = 13+$letter+count($array_mesi)-count($filtri);
            if($merge_to_index >= count($all_letters)) $merge_to_index = count($all_letters)-1;
            $sheet = $spreadsheet->getActiveSheet();
            $sheet->MergeCells($letter . $index . ':' . $all_letters[$merge_to_index] . $index);
            $index += 2;
        }
        //FINE RE1-T2

        if (!in_array("userid", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Userid");
            $letter++;
        }
        if (!in_array("username", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Username");
            $letter++;
        }
        if (!in_array("name", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Nome Cognome");
            $letter++;
        }
        if (!in_array("cf", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Codice Fiscale");
            $letter++;
        }
        if (!in_array("email", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Email");
            $letter++;
        }
        if (!in_array("dn", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Data di Nascita");
            $letter++;
        }
        if (!in_array("ln", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Luogo di Nascita");
            $letter++;
        }
        if (!in_array("azienda", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Azienda");
            $letter++;
        }
        if (!in_array("phone", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "N.di telefono");
            $letter++;
        }
        if (!in_array("ruolo", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Ruolo");
            $letter++;
        }
        if (isset($fromReport) && !in_array("corso", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Corso");
            $letter++;
        }
        if (isset($fromReport) && !in_array("courseid", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Id Corso");
            $letter++;
        }
        if (!in_array("tc", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue("$letter" . $index, "Tempo cumulato");
            $letter++;
        }

        $column = $letter;
        $giro = 0;
        foreach ($array_mesi as $mese) {
            if (!in_array("data_" . $giro, $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($column . $index, $mese);
                $column++;
            }
            $giro++;
        }


        $spreadsheet->getActiveSheet()->setTitle("Report");


        $from = "A1";
        $to = "$column" . $index;
        $spreadsheet->getActiveSheet()->getStyle("$from:$to")->getFont()->setBold(true);

        $i = $index + 1;
        foreach ($new as $singolo) {
            $letter = 'A';

            if (!in_array("userid", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['userid']);
                $letter++;
            }
            if (!in_array("username", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['username']);
                $letter++;
            }
            if (!in_array("name", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['name']);
                $letter++;
            }
            if (!in_array("cf", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['cf']);
                $letter++;
            }
            if (!in_array("email", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['email']);
                $letter++;
            }

            if (!in_array("dn", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, @$singolo['data_nascita']);
                $letter++;
            }

            if (!in_array("ln", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, @$singolo['citta']);
                $letter++;
            }

            if (!in_array("azienda", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['azienda']);
                $letter++;
            }
            if (!in_array("phone", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['phone']);
                $letter++;
            }
            if (!in_array("ruolo", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['ruolo']);
                $letter++;
            }
            if (isset($fromReport) && !in_array("corso", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['corso']);
                $letter++;
            }
            if (isset($fromReport) && !in_array("courseid", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['courseid']);
                $letter++;
            }
            if (!in_array("tc", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue("$letter$i", $singolo["tempo_tot"]);
                $letter++;
            }


            $column = $letter;
            $giro = 0;
            foreach ($singolo['mesi'] as $value) {
                if (!in_array("data_" . $giro, $filtri)) {
                    $spreadsheet->setActiveSheetIndex($j)->setCellValue("$column$i", $value);
                    $column++;
                }
                $giro++;
            }


            $i++;
        }
        foreach (range('A', "$column") as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }
        $spreadsheet->getActiveSheet()->getStyle("A" . $index . ":" . $column . $index)
            ->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('00FF7F');

        return $i;
    }

    public function getMidnight($data) {
        $date = date_create_from_format("d/m/Y", $data);
        $date = date_format($date, 'Y-m-d');
        $midnight = strtotime($date);
        return $midnight;
    }

    public function getTime($data, $beforeMidnight = null, $afterMidnight = null) {
        $date = date_create_from_format("d/m/Y", $data);
        $date = date_format($date, 'Y-m-d');
        if (isset($beforeMidnight)) {
            $timestamp = strtotime($date . " 23:59:59");
        } else {
            $timestamp = strtotime($date . " 00:00:00");
        }
        return $timestamp;
    }

    public function getTimeLesson($objectid, $db) {
        $tempoLezione = 0;
        $collection = $this->conn->$db->mdl_hvp;
        $cursor = $collection->find(
            ['id' => (int) $objectid],
            ['projection' => ['json_content' => 1]]
        );
        foreach ($cursor as $singolo) {
            $split = explode('"time":', $singolo['json_content']);
            if (isset($split[1])) {
                $split = explode(",", $split[1]);
                $split = explode(".", $split[0]);
                $tempoLezione = (int) $split[0];
            }
        }
        return $tempoLezione;
    }

    public function getObjectId($contextinstanceid, $db) {
        $objectid = 0;
        $collection = $this->conn->$db->mdl_logstore_standard_log;

//        $cursor = $collection->distinct("objectid",
//            ['contextid' => (int) $contextid,
//                ['projection' => ['objectid' => 1]]
//            ]);

//        $contextinstanceid

//        $cursor = $collection->find(
//            ["contextinstanceid" => $contextinstanceid, "objectid" => ['$ne' => 0]],
//            ['projection' => ['objectid' => 1],
//                'sort' => ['_id' => -1],
//                'limit' => 1]
//        );

        //nuovo con indice
        $cursor = $collection->find(
            ["contextinstanceid" => $contextinstanceid, "component" => "mod_hvp", "objectid" => ['$gt' => 0]],
            ['projection' => ['objectid' => 1],
                'sort' => ['timecreated' => -1],
                'limit' => 1]
        );

        foreach ($cursor as $singolo) {
//            if($contextinstanceid == 3192){
//                var_dump($singolo);
//                exit;
//            }
            $objectid = $singolo['objectid'];
        }
        return $objectid;
    }

    public function getLastProgressNewApi($userid, $timecreated, $objectid, $db) {

        $date = date('Y-m-d', $timecreated);
        $date_new  = date( "Y-m-d", strtotime( $date . "-2 day"));
        $timecreated2 = strtotime( $date . '23:59:59' . "-1 day");

        $collection = $this->conn->$db->mdl_logstore_standard_log;
        $cursor = $collection->find(
            ['objectid' => (int) $objectid, 'userid' => (int) $userid, 'component' => "mod_hvp",
                "action" => "progress", "timecreated" => ['$gte' => (int) $timecreated2]],
            ['projection' => ['other' => 1],
                'sort' => ['_id' => -1],
                'limit' => 1]
        );

        $tempo_cumulato = 0;
        foreach ($cursor as $singolo) {

//            var_dump($singolo);
//            exit;

            //da  verificare
//            $progress = $singolo['other'];
//            $toReplace = ['{', '}', '"'];
//            $progress = str_replace($toReplace, "", $progress);
//            if ($progress != "") {
//                $progress = explode(".", $progress);
//                $progress = explode(":", $progress[0]);
//                @$tempo_cumulato = (int) @$progress[1];
//            } else
//                $tempo_cumulato = 0;
            //end da verificare

            $progress = $singolo['other'];

            if(@unserialize($progress) === false){
                $progress = json_decode($progress);
            }else{
                $progress = json_decode(unserialize($progress));
            }

            if(!is_null($progress)){
                @$tempo_cumulato += (int)$progress->progress;
            }else{
                $tempo_cumulato += 0;
            }


        }

        return $tempo_cumulato;
    }

    public function getLastProgressGlobalNewApi($userid, $timecreated, $objectid, $db) {

        $date = date('Y-m-d', $timecreated);
        $date_new  = date( "Y-m-d", strtotime( $date . "-2 day"));
        $timecreated2 = strtotime( $date . '23:59:59' . "-1 day");

        $lezioni_terminate = array();

        $collection = $this->conn->$db->mdl_logstore_standard_log;
        $cursor = $collection->find(
            ['contextinstanceid' => (int) $objectid, 'userid' => (int) $userid,
                'action' => [
                    '$in' => ['progress', 'submitted', 'clicked', 'joined']
                ],
                "timecreated" => ['$gte' => (int) $timecreated2]],
            ['projection' => ['other' => 1, 'action' => 1, 'contextinstanceid' => 1, 'objectid' => 1, 'userid' => 1],
                'sort' => ['userid' => -1],
                'limit' => 1]
        );

        $tempo_cumulato = 0;
        foreach ($cursor as $singolo) {

//            var_dump($singolo);
//            exit;

            //da  verificare
//            $progress = $singolo['other'];
//            $toReplace = ['{', '}', '"'];
//            $progress = str_replace($toReplace, "", $progress);
//            if ($progress != "") {
//                $progress = explode(".", $progress);
//                $progress = explode(":", $progress[0]);
//                @$tempo_cumulato = (int) @$progress[1];
//            } else
//                $tempo_cumulato = 0;
            //end da verificare

            if($singolo['action'] == 'submitted'){
                $singolo['objectid'] = $this->getObjectId($singolo['contextinstanceid'], $db);
            }

            $key_lezione = $singolo['userid'] . '-' . $singolo['objectid'];

            if ($singolo['action'] == "submitted") {
                if (!array_key_exists($key_lezione, $lezioni_terminate)) {
//                    echo "prova: " . $singolo['objectid'];
                    $tempo_cumulato = $this->getTimeLesson($singolo['objectid'], $db);
                    $lezioni_terminate[$key_lezione] = $singolo['objectid'];
                }
            }else{
                $progress = $singolo['other'];

                if(@unserialize($progress) === false){
                    $progress = json_decode($progress);
                }else{
                    $progress = json_decode(unserialize($progress));
                }

                if(!is_null($progress)){
                    @$tempo_cumulato += (int)$progress->progress;
                }else{
                    $tempo_cumulato += 0;
                }
            }


        }

        return $tempo_cumulato;
    }

    public function getLastActionAfterNextLogin($userid, $id, $db) {
        $collection = $this->conn->$db->mdl_logstore_standard_log;
        $cursor = $collection->find(
            [
                'userid' => (int) $userid, "id" => ['$lt' => (int) $id], "action" => ['$ne' => ['failed', 'login']]
            ],
            ['projection' => ['id' => 1, 'timecreated' => 1],
                'sort' => ['timecreated' => -1],
                'limit' => 1]
        );

        foreach ($cursor as $singolo) {
            $id = $singolo['id'];
            $timecreated = $singolo['timecreated'];
        }

        $result = array($id, $timecreated);

        return $result;
    }

    public function getLastActionToday($userid, $timestamp, $db) {
        $inizio = strtotime("today", $timestamp);
        $fine   = strtotime("tomorrow", $inizio) - 1;

        $collection = $this->conn->$db->mdl_logstore_standard_log;
        $cursor = $collection->find(
            [
                'userid' => (int) $userid,
                'timecreated' => [
                    '$gte' => $inizio,
                    '$lte' => $fine
                ],
                "action" => ['$ne' => ['failed', 'login']]
            ],
            ['projection' => ['id' => 1, 'timecreated' => 1],
                'sort' => ['timecreated' => -1],
                'limit' => 1]
        );

        foreach ($cursor as $singolo) {
            $id = $singolo['id'];
            $timecreated = $singolo['timecreated'];
        }

        $result = array($id, $timecreated);

        return $result;
    }

    function getRangeDateString($currentTime, $timestamp) {
        if ($timestamp) {
            // Reset time to 00:00:00
            $timestamp=strtotime(date('Y-m-d 00:00:00',$timestamp));
            $days=round(($timestamp-$currentTime)/86400);
            switch($days) {
                case '0';
                    return 'Today';
                    break;
                case '-1';
                    return 'Yesterday';
                    break;
                case '-2';
                    return 'Day before yesterday';
                    break;
                case '1';
                    return 'Tomorrow';
                    break;
                case '2';
                    return 'Day after tomorrow';
                    break;
                default:
                    if ($days > 0) {
                        return 'In '.$days.' days';
                    } else {
                        return ($days*-1).' days ago';
                    }
                    break;
            }
        }
    }

    public function getLastProgressViewed($userid, $timecreated, $objectid, $db) {

//        $date = date('Y-m-d', $timecreated);
//        $date_new  = date( "Y-m-d", strtotime( $date . "-1 day"));
//        $timecreated2 = strtotime( $date . '23:59:59' . "-1 day");
//
//        var_dump($timecreated);

        $collection = $this->conn->$db->mdl_logstore_standard_log;
        $cursor = $collection->find(
            ['objectid' => (int) $objectid, 'userid' => (int) $userid, 'component' => "mod_hvp",
                "action" => "viewed", "timecreated" => ['$lt' => (int) $timecreated]],
            ['projection' => ['other' => 1, 'id' => 1],
                'sort' => ['timecreated' => -1],
                'limit' => 1]
        );

        $tempo_cumulato = 0;
        foreach ($cursor as $singolo) {

//            var_dump($singolo);
//            exit;

            //da  verificare
//            $progress = $singolo['other'];
//            $toReplace = ['{', '}', '"'];
//            $progress = str_replace($toReplace, "", $progress);
//            if ($progress != "") {
//                $progress = explode(".", $progress);
//                $progress = explode(":", $progress[0]);
//                @$tempo_cumulato = (int) @$progress[1];
//            } else
//                $tempo_cumulato = 0;
            //end da verificare

//            var_dump($singolo['id']); exit;

            $progress = $singolo['other'];

//            var_dump($progress);

            if(@unserialize($progress) === false){
                $progress = json_decode($progress);
            }else{
                $progress = json_decode(unserialize($progress));
            }

            if(!is_null($progress)){
                @$tempo_cumulato += (int)$progress->progress;
            }else{
                $tempo_cumulato += 0;
            }


        }

        return $tempo_cumulato;
    }

    public function getDateLastProgressViewed($userid, $timecreated, $objectid, $db) {

        $collection = $this->conn->$db->mdl_logstore_standard_log;
        $cursor = $collection->find(
            ['objectid' => (int) $objectid, 'userid' => (int) $userid, 'component' => "mod_hvp",
                "action" => "viewed", "timecreated" => ['$lt' => (int) $timecreated]],
            ['projection' => ['id' => 1, 'timecreated' => 1],
                'sort' => ['timecreated' => -1],
                'limit' => 1]
        );

        $date = '';
        foreach ($cursor as $singolo) {
            $date = $singolo['timecreated'];
        }

        return $date;
    }

    public function getFirstViewedEnrico($userid, $timecreated, $objectid, $db) {

        $date = date('Y-m-d', $timecreated);
        $date_new  = date( "Y-m-d", strtotime( $date . "-1 day"));
        $inizio = strtotime( $date . '23:59:59' . "-1 day");
        $fine = strtotime( $date . '23:59:59');

        $collection = $this->conn->$db->mdl_logstore_standard_log;
        $cursor = $collection->find(
            ['objectid' => (int) $objectid, 'userid' => (int) $userid, 'component' => "mod_hvp",
                "action" => "viewed", 'timecreated' => [
                '$gte' => $inizio,
                '$lte' => $fine
            ]],
            ['projection' => ['other' => 1, 'id' => 1],
                'sort' => ['timecreated' => 1],
                'limit' => 1]
        );

        $tempo_cumulato = 0;
        foreach ($cursor as $singolo) {

            $progress = $singolo['other'];

            if(@unserialize($progress) === false){
                $progress = json_decode($progress);
            }else{
                $progress = json_decode(unserialize($progress));
            }

            if(!is_null($progress)){
                @$tempo_cumulato += (int)$progress->progress;
            }else{
                $tempo_cumulato += 0;
            }
        }

        return $tempo_cumulato;
    }

    public function getLastProgressEnrico($userid, $timecreated, $objectid, $db) {

        $collection = $this->conn->$db->mdl_logstore_standard_log;
        $cursor = $collection->find(
            ['objectid' => (int) $objectid, 'userid' => (int) $userid, 'component' => "mod_hvp",
                "action" => "progress", "timecreated" => ['$lt' => (int) $timecreated]],
            ['projection' => ['other' => 1],
                'sort' => ['timecreated' => -1],
                'limit' => 1]
        );

        $tempo_cumulato = 0;
        foreach ($cursor as $singolo) {

            $progress = $singolo['other'];

            if(@unserialize($progress) === false){
                $progress = json_decode($progress);
            }else{
                $progress = json_decode(unserialize($progress));
            }

            if(!is_null($progress)){
                @$tempo_cumulato += (int)$progress->progress;
            }else{
                $tempo_cumulato += 0;
            }
        }

        return $tempo_cumulato;
    }

    public function getLastProgressNew($userid, $timecreated, $objectid, $db, $ret = null) {

        $date = date('Y-m-d', $timecreated);
        $date_new  = date( "Y-m-d", strtotime( $date . "-1 day"));
        $timecreated2 = strtotime( $date . '23:59:59' . "-1 day");

        $collection = $this->conn->$db->mdl_logstore_standard_log;
        $cursor = $collection->find(
            ['objectid' => (int) $objectid, 'userid' => (int) $userid, 'component' => "mod_hvp",
                "action" => "progress", "timecreated" => ['$lt' => (int) $timecreated2]],
            ['projection' => ['other' => 1, 'id' => 1, 'objectid' => 1],
                'sort' => ['timecreated' => -1],
                'limit' => 1]
        );

        $tempo_cumulato = 0;
        foreach ($cursor as $singolo) {

//            if($ret == 'ret'){
//                var_dump($singolo);
//            }

//            var_dump($singolo);
//            exit;

            //da  verificare
//            $progress = $singolo['other'];
//            $toReplace = ['{', '}', '"'];
//            $progress = str_replace($toReplace, "", $progress);
//            if ($progress != "") {
//                $progress = explode(".", $progress);
//                $progress = explode(":", $progress[0]);
//                @$tempo_cumulato = (int) @$progress[1];
//            } else
//                $tempo_cumulato = 0;
            //end da verificare

            $progress = $singolo['other'];

//            var_dump($progress);

            if(@unserialize($progress) === false){
                $progress = json_decode($progress);
            }else{
                $progress = json_decode(unserialize($progress));
            }

            if(!is_null($progress)){
                @$tempo_cumulato += (int)$progress->progress;
            }else{
                $tempo_cumulato += 0;
            }


        }

        return $tempo_cumulato;
    }

    public function getLastProgress($userid, $timecreated, $objectid, $db) {
        $collection = $this->conn->$db->mdl_logstore_standard_log;
        $cursor = $collection->find(
            ['objectid' => (int) $objectid, 'userid' => (int) $userid, 'component' => "mod_hvp",
                "action" => "progress", "timecreated" => ['$lt' => (int) $timecreated]],
            ['projection' => ['id' => 1, 'other' => 1],
                'sort' => ['_id' => -1],
                'limit' => 1]
        );
        $tempo_cumulato = 0;
        foreach ($cursor as $singolo) {

//            var_dump($singolo);
//            exit;

            //da  verificare
//            $progress = $singolo['other'];
//            $toReplace = ['{', '}', '"'];
//            $progress = str_replace($toReplace, "", $progress);
//            if ($progress != "") {
//                $progress = explode(".", $progress);
//                $progress = explode(":", $progress[0]);
//                @$tempo_cumulato = (int) @$progress[1];
//            } else
//                $tempo_cumulato = 0;
            //end da verificare

            $progress = $singolo['other'];

            if(@unserialize($progress) === false){
                $progress = json_decode($progress);
            }else{
                $progress = json_decode(unserialize($progress));
            }

            if(!is_null($progress->progress)){
                @$tempo_cumulato = (int)$progress->progress;
            }else{
                $tempo_cumulato = 0;
            }


        }

        return $tempo_cumulato;
    }

    public function isResettatoNow($userid, $objectid, $inizio, $db) {

        $date = date('Y-m-d', $inizio);
        $date_new  = date( "Y-m-d", strtotime( $date . "-1 day"));
        $inizio = strtotime( $date . '23:59:59' . "-1 day");
        $fine = strtotime( 'now');

        $collection = $this->conn->$db->mdl_logstore_standard_log;
        $cursor = $collection->find(
            ['objectid' => (int) $objectid, 'userid' => (int) $userid, 'component' => "mod_hvp", 'timecreated' => [
                '$gte' => $inizio,
                '$lte' => $fine
            ]],
            ['projection' => ['other' => 1],
                'sort' => ['timecreated' => -1],
                'limit' => 1]
        );

        foreach ($cursor as $singolo) {

            if (strpos($singolo['other'], "RESET") !== FALSE) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function isResettato($userid, $objectid, $timecreated, $db) {
        $collection = $this->conn->$db->mdl_logstore_standard_log;
        $cursor = $collection->find(
            ['objectid' => (int) $objectid, 'userid' => (int) $userid, 'component' => "mod_hvp", "timecreated" => ['$lt' => (int) $timecreated]],
            ['projection' => ['other' => 1],
                'sort' => ['timecreated' => -1],
                'limit' => 1]
        );

        foreach ($cursor as $singolo) {

            if (strpos($singolo['other'], "RESET") !== FALSE) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function getLoginDate($db, $inizio, $fine, $idUtente = null, $array_id = null, $idCorso = null, $sort = 1) {
        $collection = $this->conn->$db->mdl_logstore_standard_log;

        if (!isset($idUtente)) {
            if (isset($idCorso) && isset($array_id)) {
                if ($idCorso == 0) {
                    $match = ['$match' => [
                        'userid' => [
                            '$in' => $array_id
                        ],
                        'timecreated' => [
                            '$gt' => $inizio,
                            '$lt' => $fine
                        ]
                    ]];
                } else if (count($array_id) == 0) {
                    $match = ['$match' => [
                        'courseid' => (int) $idCorso,
                        'timecreated' => [
                            '$gte' => $inizio,
                            '$lte' => $fine
                        ]
                    ]];
                } else {
                    $match = ['$match' => [
                        'userid' => [
                            '$in' => $array_id
                        ],
                        'courseid' => (int) $idCorso,
                        'timecreated' => [
                            '$gte' => $inizio,
                            '$lte' => $fine
                        ]
                    ]];
                }
            } else if (!isset($idCorso)) {
                $match = ['$match' => [
                    'userid' => [
                        '$in' => $array_id
                    ],
                    'timecreated' => [
                        '$gt' => $inizio,
                        '$lt' => $fine
                    ],
                    'action' => 'loggedin'
                ]];
            }
        } else {
            $match = ['$match' => [
                'userid' => (int) $idUtente,
                'timecreated' => [
                    '$gt' => $inizio,
                    '$lt' => $fine
                ],
                'action' => 'loggedin'
            ]];
        }
        $pipeline = [$match,
            ['$lookup' => [
                'from' => 'mdl_utenti_complete',
                'localField' => 'userid',
                'foreignField' => 'userid',
                'as' => 'info'
            ]], ['$set' => [
                'username' => [
                    '$arrayElemAt' => [
                        '$info.username',
                        0
                    ]
                ],
                'email' => [
                    '$arrayElemAt' => [
                        '$info.email',
                        0
                    ]
                ],
                'cf' => [
                    '$arrayElemAt' => [
                        '$info.idnumber',
                        0
                    ]
                ],
                'nome' => [
                    '$arrayElemAt' => [
                        '$info.nome',
                        0
                    ]
                ],
                'citta' => [
                    '$arrayElemAt' => [
                        '$info.citta',
                        0
                    ]
                ],
                'data' => [
                    '$arrayElemAt' => [
                        '$info.data',
                        0
                    ]
                ],
                'azienda' => [
                    '$arrayElemAt' => [
                        '$info.azienda',
                        0
                    ]
                ]
                ,
                'phone2' => [
                    '$arrayElemAt' => [
                        '$info.phone2',
                        0
                    ]
                ],
                'phone1' => [
                    '$arrayElemAt' => [
                        '$info.phone1',
                        0
                    ]
                ],
            ]], ['$project' => [
                'info' => 0
            ]],
            [ '$sort' => [ 'timecreated' => $sort ] ]
        ];
        //var_dump($pipeline) ;exit();
        $out = $collection->aggregate($pipeline);
        return $out->toArray();
    }

    public function reportMongo($db, $inizio, $fine){
        $collection = $this->conn->$db->mdl_logstore_standard_log;

        $match = [
            '$match'=> [
                'component' => 'mod_hvp',
                'action' => [
                    '$in' => [
                        'progress', 'submitted'
                    ]
                ],
                'timecreated' => [
                    '$gte' => $inizio,
                    '$lte' => $fine
                ]
            ]
        ];

        $group = [
            '$group' => [
                '_id' => [
                    'objectid' => '$objectid',
                    'timecreated' => [
                        'format' => '%Y-%m-%d',
                        'date' => [
                            '$toDate' => [
                                '$multiply' => [
                                    1000, '$timecreated'
                                ]
                            ]
                        ]
                    ]
                ],
                'id' => [
                    '$first' => '$id'
                ],
                'action' => [
                    '$first' => '$action'
                ],
                'userid' => [
                    '$first' => '$userid'
                ],
                'objectid' => [
                    '$first' => '$objectid'
                ],
                'other' => [
                    '$first' => '$other'
                ],
                'timecreated' => [
                    '$first' => '$timecreated'
                ],
                'count' => [
                    '$sum' => 1
                ]
            ]
        ];

        $sort = [
            '$sort' => [
                'timecreated' => -1,
                'objectid' => 1
            ]
        ];

        $pipeline = [$match, $group];

        $options = array("allowDiskUse" => true);

        $out = $collection->aggregate($pipeline, $options);
        return $out->toArray();
    }

    public function getLogoutDate($db, $dataLogin1, $dataLogin2, $idUtente) {
        $collection = $this->conn->$db->mdl_logstore_standard_log;

        //echo $dataLogin1.' '.$dataLogin2.' '.$idUtente."<br>";
        $match = ['$match' => [
            'userid' => (int) $idUtente,
            'timecreated' => [
                '$gt' => $dataLogin1,
                '$lt' => $dataLogin2
            ],
            'action' => 'loggedout'
        ]];

        $pipeline = [$match,
            ['$lookup' => [
                'from' => 'mdl_utenti_complete',
                'localField' => 'userid',
                'foreignField' => 'userid',
                'as' => 'info'
            ]], ['$set' => [
                'username' => [
                    '$arrayElemAt' => [
                        '$info.username',
                        0
                    ]
                ],
                'email' => [
                    '$arrayElemAt' => [
                        '$info.email',
                        0
                    ]
                ],
                'cf' => [
                    '$arrayElemAt' => [
                        '$info.idnumber',
                        0
                    ]
                ],
                'nome' => [
                    '$arrayElemAt' => [
                        '$info.nome',
                        0
                    ]
                ],
                'citta' => [
                    '$arrayElemAt' => [
                        '$info.citta',
                        0
                    ]
                ],
                'data' => [
                    '$arrayElemAt' => [
                        '$info.data',
                        0
                    ]
                ],
                'azienda' => [
                    '$arrayElemAt' => [
                        '$info.azienda',
                        0
                    ]
                ]
                ,
                'phone2' => [
                    '$arrayElemAt' => [
                        '$info.phone2',
                        0
                    ]
                ],
                'phone1' => [
                    '$arrayElemAt' => [
                        '$info.phone1',
                        0
                    ]
                ],
            ]], ['$project' => [
                'info' => 0
            ]],
            [ '$sort' => [ 'timecreated' => 1 ] ]
        ];
        //var_dump($pipeline) ;exit();
        $out = $collection->aggregate($pipeline);
        $cursore =  $out->toArray();
        //var_dump($cursore);exit();
        $dataLogout = false;

        if (isset($cursore[0]['timecreated']) && $cursore[0]['timecreated']) {
            $dataLogout = $cursore[0]['timecreated'];
        }
        //var_dump($dataLogout);exit('ertrte');
        /* Se non c'� l'evento logout prendo l'evento appena prima al prossimo login */
        if (!$dataLogout) {
            $arrLogin = $this->getLoginDate($db, $dataLogin1, $dataLogin2, $idUtente);
            //var_dump($arrLogin);exit('aaaaa333');
            if (isset($arrLogin[0]['timecreated']) && $arrLogin[0]['timecreated']) {
                $loginSuccessivo = $arrLogin[0]['timecreated'];
                //var_dump($loginSuccessivo);exit('aaaaa');
                $arrLastEvent = $this->getLastEvent($db, $dataLogin1, $loginSuccessivo, $idUtente);

                if (isset($arrLastEvent[0]['timecreated']) && $arrLastEvent[0]['timecreated']) {
                    if ($arrLastEvent[0]['action'] == 'failed') {
                        $arrLastEvent2 = $this->getLastEvent($db, $dataLogin1, $arrLastEvent[0]['timecreated'], $idUtente);
                        if (isset($arrLastEvent2[0]['timecreated']) && $arrLastEvent2[0]['timecreated']) {
                            $dataLogout = $arrLastEvent2[0]['timecreated'];
                        } else {
                            /* Questo � il caso in cui un utente ha in sequenza: login, failed, login
                             * in questo caso non � possibile definire il tempo di logout e quindi lo valorizzo con il timestamp del login */
                            $dataLogout = $dataLogin1;
                        }
                    } else {
                        $dataLogout = $arrLastEvent[0]['timecreated'];
                        //var_dump($dataLogout);exit('aaaaa333');
                    }
                } else {
                    /* Questo � il caso in cui un utente ha in sequenza: login, login
                     * in questo caso non � possibile definire il tempo di logout e quindi lo valorizzo con il timestamp del primo login */
                    $dataLogout = $dataLogin1;
                }
            } else {
                /* Prendo l'ultimo evento della giornata */
                $arrLastEvent3 = $this->getLastEvent($db, $dataLogin1, $dataLogin2, $idUtente);

                if (isset($arrLastEvent3[0]['timecreated']) && $arrLastEvent3[0]['timecreated']) {
                    $dataLogout = $arrLastEvent3[0]['timecreated'];
                } else {
                    /* Questo � il caso in cui non c'� il logout e l'ultimo evento della giornata � un login */
                    $dataLogout = $dataLogin1;
                }
                //var_dump($arrLastEvent3);exit();

            }
        } else {
            /* Se c'� l'evento logout controllo prima se tra il login e questo logout c'� un altro login */
            $arrLogin = $this->getLoginDate($db, $dataLogin1, $dataLogout, $idUtente);
            //var_dump($arrLogin);exit('aaaaa333');
            if (isset($arrLogin[0]['timecreated']) && $arrLogin[0]['timecreated']) {
                $loginSuccessivo = $arrLogin[0]['timecreated'];
                //var_dump($loginSuccessivo);exit('aaaaa');
                $arrLastEvent = $this->getLastEvent($db, $dataLogin1, $loginSuccessivo, $idUtente);

                if (isset($arrLastEvent[0]['timecreated']) && $arrLastEvent[0]['timecreated']) {
                    if ($arrLastEvent[0]['action'] == 'failed') {
                        $arrLastEvent2 = $this->getLastEvent($db, $dataLogin1, $arrLastEvent[0]['timecreated'], $idUtente);
                        if (isset($arrLastEvent2[0]['timecreated']) && $arrLastEvent2[0]['timecreated']) {
                            $dataLogout = $arrLastEvent2[0]['timecreated'];
                        } else {
                            /* Questo � il caso in cui un utente ha in sequenza: login, failed, login
                             * in questo caso non � possibile definire il tempo di logout e quindi lo valorizzo con il timestamp del login */
                            $dataLogout = $dataLogin1;
                        }
                    } else {
                        $dataLogout = $arrLastEvent[0]['timecreated'];
                        //var_dump($dataLogout);exit('aaaaa333');
                    }
                }
            }
        }
        //var_dump($dataLogout);exit();
        return $dataLogout;
    }

    public function getLastEvent($db, $dataLogin1, $dataLogin2, $idUtente) {
        $collection = $this->conn->$db->mdl_logstore_standard_log;


        $match = ['$match' => [
            'userid' => (int) $idUtente,
            'timecreated' => [
                '$gt' => $dataLogin1,
                '$lt' => $dataLogin2
            ],
        ]];

        $pipeline = [$match,
            ['$lookup' => [
                'from' => 'mdl_utenti_complete',
                'localField' => 'userid',
                'foreignField' => 'userid',
                'as' => 'info'
            ]], ['$set' => [
                'username' => [
                    '$arrayElemAt' => [
                        '$info.username',
                        0
                    ]
                ],
                'email' => [
                    '$arrayElemAt' => [
                        '$info.email',
                        0
                    ]
                ],
                'cf' => [
                    '$arrayElemAt' => [
                        '$info.idnumber',
                        0
                    ]
                ],
                'nome' => [
                    '$arrayElemAt' => [
                        '$info.nome',
                        0
                    ]
                ],
                'citta' => [
                    '$arrayElemAt' => [
                        '$info.citta',
                        0
                    ]
                ],
                'data' => [
                    '$arrayElemAt' => [
                        '$info.data',
                        0
                    ]
                ],
                'azienda' => [
                    '$arrayElemAt' => [
                        '$info.azienda',
                        0
                    ]
                ]
                ,
                'phone2' => [
                    '$arrayElemAt' => [
                        '$info.phone2',
                        0
                    ]
                ],
                'phone1' => [
                    '$arrayElemAt' => [
                        '$info.phone1',
                        0
                    ]
                ],
            ]], ['$project' => [
                'info' => 0
            ]],
            [ '$sort' => [ 'timecreated' => -1 ] ]
        ];
        //var_dump($pipeline) ;exit();
        $out = $collection->aggregate($pipeline);

        return  $out->toArray();

    }

    public function setArray($array)
    {
        $arrReturn = array();

        foreach ($array as $rowLog) {

            $arrLezioni = array();

            $user_id = $rowLog['userid'];
            $action = $rowLog['action'];
            $objectid = $rowLog['objectid'];
            $courseid = $rowLog['courseid'];
            $other = $rowLog['other'];

            if (strpos($other, 'RESET') !== FALSE) {
                $progress = 0;
                $other = 'RESET';
            }

            $toReplace = ['{', '}', '"'];
            $progress = str_replace($toReplace, "", $other);

            if ($progress != "") {
                $progress = explode(".", $progress);
                $progress = explode(":", $progress[0]);
                @$progress = (int) @$progress[1];
            } else {
                $progress = 0;
            }



            $timecreated = $rowLog['timecreated'];
            $giorno = date("d-m-Y", $timecreated);

            //echo "<br>$timecreated";

            $arrReturn[$user_id][$giorno][$objectid][] = $progress;

        }

        var_dump($arrReturn);exit();
    }

    public function getTimestampGiorno($data)
    {
        $inizio = strtotime($data . " 00:00:00");
        $fine = strtotime($data . " 23:59:59");

        return array('inizio' => $inizio, 'fine' => $fine);
    }

}

function compareByTimeStampNew($time1, $time2) {
    $time1 = str_replace("/", "-", $time1);
    $time2 = str_replace("/", "-", $time2);

    return strtotime($time1) - strtotime($time2);
}
