<?php

$abs_path = '/home/admin/public_html';
if(isset($_SERVER['DOCUMENT_ROOT']) && strlen(trim($_SERVER['DOCUMENT_ROOT']))) $abs_path = $_SERVER['DOCUMENT_ROOT'];

require_once($abs_path . '/vendor/autoload.php');
require_once($abs_path . '/skilledin/report/config/connection/MongoDB/skilledin.php');
require_once($abs_path . '/skilledin/report/obj/Utility.php');
//function compareByTimeStamp($time1, $time2) {
//    $time1 = str_replace("/", "-", $time1);
//    $time2 = str_replace("/", "-", $time2);
//
//    return strtotime($time1) - strtotime($time2);
//}

class Skilledin{
    private $connection = null;
    private $s_tool;

    public function __construct(){
        $connection_string  = "mongodb+srv://";
        $connection_string .= conn_username . ":" . urlencode(conn_password) . "@" . conn_server;
        $connection_string .= "?retryWrites=true&w=majority";
        $this->connection = new MongoDB\Client($connection_string);
        $this->s_tool = new Utility();
    }

    //Utenti
    public function getUsers($user_filter = array(), $user_options = array()) {
        //Definisco la tablla
        $collection = $this->connection->admin->utenti;

        //Definisco filtri
        $filter  = array();
        if(isset($user_filter['id'])) $filter['id'] = (int) $user_filter['id'];

        //Definisco opizioni
        $options = array();

        //Eseguo la query
        $cursor = $collection->find($filter, $options);

        //Acquisisco i risultati
        $objects = array();
        foreach ($cursor as $item) {
            $objects[] = (array) $item;
        }

        //Restituisco il valore
        if(!empty($objects)) return $objects;
        return false;
    }
    public function getUser($user_id){
        $user = $this->getUsers(array('id' => (int) $user_id));
        if($user) return array_pop($user);
        return false;
    }
    public function editUser($user_id, $data = array()){
        if(!empty($data)){
            //Definisco la tablla
            $collection = $this->connection->admin->utenti;

            //Definisco i filtri
            $filter = array('id' => (int) $user_id);

            //Eseguo la modifica
            $update = array();
            if(isset($data['email']))         $update['email']         = $data['email'];
            if(isset($data['password']))      $update['password']      = MD5($data['password']);
            if(isset($data['idRuolo']))       $update['idRuolo']       = $data['idRuolo'];
            if(isset($data['ruolo']))         $update['ruolo']         = $data['ruolo'];
            if(isset($data['idPiattaforma'])) $update['idPiattaforma'] = $data['idPiattaforma'];
            $result = $collection->updateOne($filter, array('$set' => $update));
            return $result;
        }
        return false;
    }
    public function deleteUser($user_id){
        //Definisco la tablla
        $collection = $this->connection->admin->utenti;

        //Eseguo l'eliminazione
        $collection->deleteOne(array('id' => $user_id));
    }

    //Piattaforme
    public function getPlatforms($user_filter = array(), $user_options = array()) {
        //Definisco la tablla
        $collection = $this->connection->admin->piattaforme;

        //Definisco filtri
        $filter  = array();
        if(isset($user_filter['id'])) $filter['id'] = (int) $user_filter['id'];
        //Definisco opizioni

        $options = array();

        //Eseguo la query
        $cursor = $collection->find($filter, $options);

        //Acquisisco i risultati
        $objects = array();
        foreach ($cursor as $item) {
            $objects[] = (array) $item;
        }

        //Restituisco il valore
        if(!empty($objects)) return $objects;
        return false;
    }

    public function getPlatform($platform_id){
        $platform = $this->getPlatforms(array('id' => (int) $platform_id));
        if($platform) return array_pop($platform);
        return false;
    }

    //Aziende
    public function getCompaniesOLD($user_filter = array(), $user_options = array()){
        if(isset($user_filter['platform_id'])){
            $table = '';
            if($user_filter['platform_id'] == 0) $table = 'fnc';
            if($user_filter['platform_id'] == 1) $table = 'f40';
            if($user_filter['platform_id'] == 2) $table = 'live';
            if($user_filter['platform_id'] == 3) $table = 'new';
            if($user_filter['platform_id'] == 5) $table = 'formaz';
            $collection = $this->connection->$table->mdl_user_info_data;

            $cursor = $collection->find(
                ['data' => ['$ne' => ""], 'id' => ['$ne' => 0], 'fieldid' => ['$eq' => 1], 'data' => ['$exists' => true]],
                ['projection' => ['data' => 1], ['sort' => ['data' => 1]],
                ]
            );

            $array_aziende = array();
            if ($cursor) {
                $tmp = array();
                foreach ($cursor as $singolo) {
                    if (!in_array($singolo['data'], $array_aziende)) {
                        $singolo['data']= trim($singolo['data']);
                        $tmp = $singolo['data'];
                        array_push($array_aziende, $tmp);
                    }
                }
            }
            return $array_aziende;
        }else{
            return false;
        }
    }
    public function getCompanyUsersOLD($user_filter = array(), $user_optiopns = array()){
        if(isset($user_filter['platform_id'])){
            $table = '';
            if($user_filter['platform_id'] == 0) $table = 'fnc';
            if($user_filter['platform_id'] == 1) $table = 'f40';
            if($user_filter['platform_id'] == 2) $table = 'live';
            if($user_filter['platform_id'] == 3) $table = 'new';
            if($user_filter['platform_id'] == 5) $table = 'formaz';
            $collection = $this->connection->$table->mdl_user_info_data;

            $match  = ['$match'  => ['data' => $user_filter['company_name']]];
            $lookup = ['$lookup' => ['from'         => 'mdl_utenti_complete',
                                    'localField'   => 'userid',
                                    'foreignField' => 'userid',
                                    'as'           => 'info']];
            $set    = ['$set'    => ['fullname'      => ['$arrayElemAt' => ['$info.nome', 0]],
                                     'username'      => ['$arrayElemAt' => ['$info.username',0]],
                                     'fiscal_code'   => ['$arrayElemAt' => ['$info.idnumber',0]],
                                     'email'         => ['$arrayElemAt' => ['$info.email',0]],
                                     'company_name'  => ['$arrayElemAt' => ['$info.azienda', 0]],
                                     'role_name'     => ['$arrayElemAt' => ['$info.ruolo',0]],
                                     'city_of_birth' => ['$arrayElemAt' => ['$info.citta',0]],
                                     'date_of_birth' => ['$arrayElemAt' => ['$info.data',0]],
                                     'phone2'        => ['$arrayElemAt' => ['$info.phone2', 0]],
                                     'phone1'        => ['$arrayElemAt' => ['$info.phone1',0]]]];

            $pipeline   = array($match, $lookup, $set);
            $cursor     = $collection->aggregate($pipeline);

            $users = array();
            if($cursor){
                foreach($cursor as $item){
                    $users[] = array('id' => $item['userid'], 'fullname' => $item['fullname']);
                }
            }
            return $users;
        }
        return false;
    }

    //Courses
    public function getCoursesOLD($user_filter = array(), $user_options = array()){
        if(isset($user_filter['platform_id'])){
            $table = '';
            if($user_filter['platform_id'] == 0) $table = 'fnc';
            if($user_filter['platform_id'] == 1) $table = 'f40';
            if($user_filter['platform_id'] == 2) $table = 'live';
            if($user_filter['platform_id'] == 3) $table = 'new';
            if($user_filter['platform_id'] == 5) $table = 'formaz';

            $collection = $this->connection->$table->corsiAssociati;

            //Definisco filtri
            $filter  = array();
            if(isset($user_filter['id'])) $filter['_id'] = (int) $user_filter['id'];

            //Definisco opizioni
            $options = array();

            //Eseguo la query
            $cursor = $collection->find($filter, $options);

            $response = array();
            foreach ($cursor as $item){
                $response[$item['_id']] = array('id'       => $item['_id']     ,
                                                'fullname' => $item['fullname'] );
            }
            return $response;
        }else{
            return false;
        }
    }
    public function getCoursesModulesHVPOLD($user_filter = array(), $user_options = array()){
        if(isset($user_filter['platform_id'])){
            $table = '';
            if($user_filter['platform_id'] == 0) $table = 'fnc';
            if($user_filter['platform_id'] == 1) $table = 'f40';
            if($user_filter['platform_id'] == 2) $table = 'live';
            if($user_filter['platform_id'] == 3) $table = 'new';
            if($user_filter['platform_id'] == 5) $table = 'formaz';

            $collection = $this->connection->$table->mdl_hvp;

            //Definisco filtri
            $filter  = array();
            if(isset($user_filter['course_id'])) $filter['course'] = (int) $user_filter['course_id'];
            if(isset($user_filter['module_id'])) $filter['id'] = (int) $user_filter['module_id'];
            $match = ['$match' => $filter];

            //Definisco opizioni
            $options = array();
            if(isset($user_options['sort'])){
                $sorting = $user_options['sorting'] ?? 1;
                $sort = ['$sort' =>  [$user_options['sort'] => $sorting]];
            }else{
                $sort = array();
            }

            $pipeline   = array();
            $pipeline[] = $match;
            if(!empty($sort)) $pipeline[] = $sort;

            //Eseguo la query
            $cursor = $collection->aggregate($pipeline);
            $response = array();
            foreach ($cursor as $item){
                $response[$item['id']] = array('id'           => $item['id']          ,
                                               'name'         => $item['name']        ,
                                               'json_content' => $item['json_content'] );
            }
            return $response;
        }else{
            return false;
        }
    }

    //Company User
    public function getCompanyUserOLD($user_filter = array(), $user_options = array()){
        if(isset($user_filter['platform_id'])){
            $table = '';
            if($user_filter['platform_id'] == 0) $table = 'fnc';
            if($user_filter['platform_id'] == 1) $table = 'f40';
            if($user_filter['platform_id'] == 2) $table = 'live';
            if($user_filter['platform_id'] == 3) $table = 'new';
            if($user_filter['platform_id'] == 5) $table = 'formaz';

            $collection = $this->connection->$table->mdl_utenti_complete;

            //Definisco filtri
            $filter  = array();
            if(isset($user_filter['user_id'])) $filter['userid'] = (int) $user_filter['user_id'];

            //Definisco opizioni
            $options = array();

            //Eseguo la query
            $cursor = $collection->find($filter, $options);

            $response = array();
            foreach ($cursor as $item){
                $response[$item['userid']] = $item;
            }
            return $response;
        }else{
            return false;
        }
    }

    //Reset
    public function getDataForHVPReset($user_filter = array(), $user_options = array()){
        $table = '';
        if(isset($user_filter['platform_id'])){
            if($user_filter['platform_id'] == -1) $table = 'database_test';
            if($user_filter['platform_id'] == 0)  $table = 'fnc';
            if($user_filter['platform_id'] == 1)  $table = 'f40';
            if($user_filter['platform_id'] == 2)  $table = 'live';
            if($user_filter['platform_id'] == 3)  $table = 'new';
            if($user_filter['platform_id'] == 5)  $table = 'formaz';
            $collection = $this->connection->$table->mdl_logstore_standard_log;

            //Definisco filtri
            $filter  = array();
            if(isset($user_filter['company_users']))     $filter['userid']['$in']     = $user_filter['company_users'];
            if(isset($user_filter['user_id']))           $filter['userid']            = (int) $user_filter['user_id'];
            if(isset($user_filter['course_id']))         $filter['courseid']          = (int) $user_filter['course_id'];
            if(isset($user_filter['module_id']))         $filter['objectid']          = (int) $user_filter['module_id'];
            if(isset($user_filter['contextinstanceid'])) $filter['contextinstanceid'] = (int) $user_filter['contextinstanceid'];
            if(isset($user_filter['component']))         $filter['component']         = $user_filter['component'];
            $filter['reset'] = (isset($user_filter['reset'])) ? $user_filter['reset'] : false;

            //Definisco opizioni
            $options = array();

            //Estraggo tutti gli elementi filtrati dall'utente con valore reset opposto a quello da imputare (default: false)
            $cursor = $collection->find($filter, $options);

            $items = array();
            foreach ($cursor as $item){
                $items[$item['id']] = (array) $item;
            }

            //Estraggo tutti gli elementi per il quale il reset non è elaborato
            if(!isset($user_filter['only_exists'])){
                $filter['reset']['$exists'] = false;
                $cursor = $collection->find($filter, $options);
                foreach ($cursor as $item){
                    $items[$item['id']] = (array) $item;
                }
            }
            return $items;
        }else{
            return false;
        }
    }

    public function scheduleResetLogHVP($data, $platform_id = -1){
        if(!empty($data)){
            //Definisco la tablla
            if($platform_id == -1) $table = 'database_test';
            if($platform_id == 0)  $table = 'fnc';
            if($platform_id == 1)  $table = 'f40';
            if($platform_id == 2)  $table = 'live';
            if($platform_id == 3)  $table = 'new';
            if($platform_id == 5)  $table = 'formaz';
            $collection = $this->connection->$table->hvp_reset_schedule;

            //Eseguo l'inserimento
            $insert = $collection->insertOne($data);
            return $insert;
        }
        return false;
    }

    public function completeScheduledResetLogHPV($key, $reset_count = 0, $platform_id = -1){
        //Definisco la tablla
        if($platform_id == -1) $table = 'database_test';
        if($platform_id == 0)  $table = 'fnc';
        if($platform_id == 1)  $table = 'f40';
        if($platform_id == 2)  $table = 'live';
        if($platform_id == 3)  $table = 'new';
        if($platform_id == 5)  $table = 'formaz';
        $collection = $this->connection->$table->hvp_reset_schedule;

        //Definisco i filtri
        $filter = array('key' => $key);

        //Eseguo la modifica
        $update                    = array();
        $update['date_completed']  = date('Y-m-d H:i:s');
        $update['affected_record'] = $reset_count;
        $update['status']          = 1;
        return $collection->updateOne($filter, array('$set' => $update));
    }

    public function unsetScheduledResetLogHPV($key, $reset_count = 0, $platform_id = -1){
        //Definisco la tablla
        if($platform_id == -1) $table = 'database_test';
        if($platform_id == 0)  $table = 'fnc';
        if($platform_id == 1)  $table = 'f40';
        if($platform_id == 2)  $table = 'live';
        if($platform_id == 3)  $table = 'new';
        if($platform_id == 5)  $table = 'formaz';
        $collection = $this->connection->$table->hvp_reset_schedule;

        //Definisco i filtri
        $filter = array('key' => $key);

        //Eseguo la modifica
        $update                    = array();
        $update['date_completed']  = date('Y-m-d H:i:s');
        $update['affected_record'] = $reset_count;
        $update['status']          = 2;
        return $collection->updateOne($filter, array('$set' => $update));
    }

    public function getScheduledResetLogHPV($user_filter = array(), $user_options = array()){
        $table = '';
        if(isset($user_filter['platform_id'])){
            if($user_filter['platform_id'] == -1) $table = 'database_test';
            if($user_filter['platform_id'] == 0) $table = 'fnc';
            if($user_filter['platform_id'] == 1) $table = 'f40';
            if($user_filter['platform_id'] == 2) $table = 'live';
            if($user_filter['platform_id'] == 3) $table = 'new';
            if($user_filter['platform_id'] == 5) $table = 'formaz';
            $collection = $this->connection->$table->hvp_reset_schedule;

            //Definisco filtri
            $filter  = array();
            if(isset($user_filter['key']))            $filter['key']            = $user_filter['key'];
            if(isset($user_filter['date_completed'])) $filter['date_completed'] = (int) $user_filter['date_completed'];
            if(isset($user_filter['status']))         $filter['status']         = (int) $user_filter['status'];

            //Definisco opizioni
            $options = array();

            //Eseguo la query
            $cursor = $collection->find($filter, $options);
            $items = array();
            foreach ($cursor as $item){
                $items[] = (array) $item;
            }
            return $items;
        }else{
            return false;
        }
    }

    public function getContextIstanceIDFromObjectId($object_id, $platform_id = -1){
        //Definisco la tablla
        if($platform_id == -1) $table = 'database_test';
        if($platform_id == 0)  $table = 'fnc';
        if($platform_id == 1)  $table = 'f40';
        if($platform_id == 2)  $table = 'live';
        if($platform_id == 3)  $table = 'new';
        if($platform_id == 5)  $table = 'formaz';
        $collection = $this->connection->$table->mdl_logstore_standard_log;

        //Definisco i filtri
        $filter = array('objectid' => (int) $object_id, 'component' => 'mod_hvp');
        $option = array();
        //Estraggo i valori

        //Eseguo la query
        $cursor = $collection->find($filter, $option);

        //Acquisisco i risultati
        $objects = array();
        foreach ($cursor as $item) {
            $item = (array) $item;
            if($item['contextinstanceid'] > 0) $objects[$item['contextinstanceid']] = $item['contextinstanceid'];
        }
        if(count($objects)) return array_shift($objects);
        return false;
    }

    public function resetLogHVP($log_id, $data = array(), $platform_id = -1){
        if(!empty($data)){
            //Definisco la tablla
            if($platform_id == -1) $table = 'database_test';
            if($platform_id == 0)  $table = 'fnc';
            if($platform_id == 1)  $table = 'f40';
            if($platform_id == 2)  $table = 'live';
            if($platform_id == 3)  $table = 'new';
            if($platform_id == 5)  $table = 'formaz';
            $collection = $this->connection->$table->mdl_logstore_standard_log;

            //Definisco i filtri
            $filter = array('id' => (int) $log_id);

            //Eseguo la modifica
            $update = array();
            if(isset($data['reset'])) $update['reset'] = $data['reset'];
            $result = $collection->updateOne($filter, array('$set' => $update));
            return $result;
        }
        return false;
    }

    public function insertManualLog($data = array(), $platform_id = -1){
        //Definisco la tablla
        if($platform_id == -1) $table = 'database_test';
        if($platform_id == 0)  $table = 'fnc';
        if($platform_id == 1)  $table = 'f40';
        if($platform_id == 2)  $table = 'live';
        if($platform_id == 3)  $table = 'new';
        if($platform_id == 5)  $table = 'formaz';
        $collection = $this->connection->$table->mdl_logstore_standard_log;

        $new = array();
        //DATA FORMAT
        if(isset($data['id'])) 	              $new['id']  	            = (int) $data['id'];
        if(isset($data['eventname']))         $new['eventname']         = "'" . $data['eventname'] . "'";
        if(isset($data['component'])) 	      $new['component']  	    = "'" . $data['component'] . "'";
        if(isset($data['action'])) 	          $new['action']  	        = "'" . $data['action'] . "'";
        if(isset($data['target'])) 	          $new['target']  	        = "'" . $data['target'] . "'";
        if(isset($data['objecttable'])) 	  $new['objecttable']  	    = "'" . $data['objecttable'] . "'";
        if(isset($data['objectid'])) 	      $new['objectid']  	    = (int) $data['objectid'];
        if(isset($data['crud'])) 	          $new['crud']  	        = "'" . $data['crud'] . "'";
        if(isset($data['edulevel'])) 	      $new['edulevel']  	    = (int) $data['edulevel'];
        if(isset($data['contextid'])) 	      $new['contextid']  	    = (int) $data['contextid'];
        if(isset($data['contextlevel'])) 	  $new['contextlevel']  	= (int) $data['contextlevel'];
        if(isset($data['contextinstanceid'])) $new['contextinstanceid'] = (int) $data['contextinstanceid'];
        if(isset($data['userid'])) 	          $new['userid']  	        = (int) $data['userid'];
        if(isset($data['courseid'])) 	      $new['courseid']  	    = (int) $data['courseid'];
        if(isset($data['relateduserid'])) 	  $new['relateduserid']  	= (int) $data['relateduserid'];
        if(isset($data['anonymous'])) 	      $new['anonymous']  	    = (int) $data['anonymous'];
        if(isset($data['other'])) 	          $new['other']  	        = "'" . $data['other'] . "'";
        if(isset($data['timecreated'])) 	  $new['timecreated']  	    = (int) $data['timecreated'];
        if(isset($data['origin'])) 	          $new['origin']  	        = "'" . $data['origin'] . "'";
        if(isset($data['ip'])) 	              $new['ip']  	            = "'" . $data['ip'] . "'";
        if(isset($data['realuserid'])) 	      $new['realuserid']  	    = (int) $data['realuserid'];

        //DATA CHECK
        $new['eventname']         = (isset($new['eventname']))         ? $new['eventname']         : "''";
        $new['component']         = (isset($new['component']))         ? $new['component']         : "''";
        $new['action']            = (isset($new['action']))            ? $new['action']            : "''";
        $new['target']            = (isset($new['target']))            ? $new['target']            : "''";
        $new['objecttable']       = (isset($new['objecttable']))       ? $new['objecttable']       : "''";
        $new['objectid'] 	      = (isset($new['objectid']))          ? $new['objectid']          : 0;
        $new['crud']              = (isset($new['crud']))              ? $new['crud']              : "''";
        $new['edulevel'] 	      = (isset($new['edulevel']))          ? $new['edulevel']          : 0;
        $new['contextid'] 	      = (isset($new['contextid']))         ? $new['contextid']         : 0;
        $new['contextlevel'] 	  = (isset($new['contextlevel']))      ? $new['contextlevel']      : 0;
        $new['contextinstanceid'] = (isset($new['contextinstanceid'])) ? $new['contextinstanceid'] : 0;
        $new['userid'] 	          = (isset($new['userid']))            ? $new['userid']            : 0;
        $new['courseid'] 	      = (isset($new['courseid']))          ? $new['courseid']          : 0;
        $new['relateduserid'] 	  = (isset($new['relateduserid']))     ? $new['relateduserid']     : 0;
        $new['anonymous'] 	      = (isset($new['anonymous']))         ? $new['anonymous']         : 0;
        $new['other']             = (isset($new['other']))             ? $new['other']             : "''";
        $new['timecreated'] 	  = (isset($new['timecreated']))       ? $new['timecreated']       : 0;
        $new['origin']            = (isset($new['origin']))            ? $new['origin']            : "''";
        $new['ip']                = (isset($new['ip']))                ? $new['ip']                : "''";
        $new['realuserid'] 	      = (isset($new['realuserid']))        ? $new['realuserid']        : 0;
        //Estraggo i valori

        //Eseguo la query
        $collection->insertOne([
            'id' => (int)$new["id"],
            'eventname'         => utf8_encode($new["eventname"]),
            'component'         => utf8_encode($new["component"]),
            'action'            => utf8_encode($new["action"]),
            'target'            => utf8_encode($new["target"]),
            'objecttable'       => utf8_encode($new["objecttable"]),
            'objectid'          => (int)$new["objectid"],
            'crud'              => utf8_encode($new["crud"]),
            'edulevel'          => (int)$new["edulevel"],
            'contextid'         => (int)$new["contextid"],
            'contextlevel'      => utf8_encode($new["contextlevel"]),
            'contextinstanceid' => (int)$new["contextinstanceid"],
            'userid'            => (int)$new["userid"],
            'courseid'          => (int)$new["courseid"],
            'relateduserid'     => (int)$new["relateduserid"],
            'anonymous'         => (int)$new["anonymous"],
            'other'             => utf8_encode($new["other"]),
            'timecreated'       => (int)$new["timecreated"],
            'origin'            => utf8_encode($new["origin"]),
            'ip'                => utf8_encode($new["ip"]),
            'realuserid'        => (int)$new["realuserid"]
        ]);
    }

    //CRON HISTORY
    public function addCronHistory($data = array()){
        $collection = $this->connection->admin->cron_history;

        $new = array();
        //DATA FORMAT
        $new['platform_id']   = (isset($data['platform_id']))   ? (int) $data['platform_id'] : -1;
        $new['platform_name'] = (isset($data['platform_name'])) ? $data['platform_name']     : "''";
        $new['job_name']  	  = (isset($data['job_name'])) 	    ? $data['job_name']          : "''";
        $new['job_date']      = (isset($data['job_date']))      ? $data['job_date']          : date('Y-m-d');
        $new['job_started']   = (isset($data['job_started']))   ? $data['job_started']       : date('Y-m-d H:i:s');
        $new['job_completed'] = (isset($data['job_completed'])) ? $data['job_completed']     : "-";
        $new['other']  	      = (isset($data['other'])) 	    ? $data['other']             : "''";
        $new['status']        = (isset($data['status']))        ? (int) $data['status']      : 1;
        $new['status_text']   = (isset($data['status_text'])) 	? $data['status_text']       : "In esecuzione";

        //Eseguo la query
        $collection->insertOne([
            'platform_id'   => $new["platform_id"],
            'platform_name' => utf8_encode($new["platform_name"]),
            'job_name'      => utf8_encode($new["job_name"]),
            'job_date'      => utf8_encode($new["job_date"]),
            'job_started'   => utf8_encode($new["job_started"]),
            'job_completed' => utf8_encode($new["job_completed"]),
            'other'         => utf8_encode($new["other"]),
            'status'        => $new["status"],
            'status_text'   => utf8_encode($new["status_text"])
        ]);
    }
    public function setCompletdCronHistory($cron){
        $collection = $this->connection->admin->cron_history;

        //Definisco i filtri
        $filter = array('platform_id'   => $cron['platform_id']  ,
                        'platform_name' => $cron['platform_name'],
                        'job_name'      => $cron['job_name']     ,
                        'job_started'   => $cron['job_started']   );

        $job_completed     = date('Y-m-d H:i:s');
        $started_seconds   = $this->s_tool->convertTimeToSeconds(date('H:i:s', strtotime($cron['job_started'])));
        $completed_seconds = $this->s_tool->convertTimeToSeconds(date('H:i:s', strtotime($job_completed)));
        $job_time          = $completed_seconds - $started_seconds;

        $origin   = new DateTimeImmutable(date('Y-m-d', strtotime($cron['job_started'])));
        $target   = new DateTimeImmutable(date('Y-m-d', strtotime($job_completed)));
        $job_days = $origin->diff($target);
        if($job_days->days > 1) $job_time += $job_days->days*86400;

        //Eseguo la modifica
        $update                  = array();
        $update['job_completed'] = $job_completed;
        $update['job_time']      = $this->s_tool->convertSecondsToTime($job_time);
        $update['status']        = 2;
        $update['status_text']   = "Completato";
        return $collection->updateOne($filter, array('$set' => $update));
    }
    public function getCronHistories($user_filter = array()){
        //Definisco la tablla
        $collection = $this->connection->admin->cron_history;

        //Definisco filtri
        $filter  = array();
        if(isset($user_filter['id'])) $filter['id'] = (int) $user_filter['id'];
        //Definisco opizioni

        $options = array();

        //Eseguo la query
        $cursor = $collection->find($filter, $options);

        //Acquisisco i risultati
        $objects = array();
        foreach ($cursor as $item) {
            $objects[] = (array) $item;
        }

        //Restituisco il valore
        if(!empty($objects)) return $objects;
        return false;
    }

    public function syncCourse($user_filter = array(), $course){
        if(isset($user_filter['platform_id'])) {
            $mongo_id = 0;

            if ($user_filter['platform_id'] == 0) $table = 'fnc';
            if ($user_filter['platform_id'] == 1) $table = 'f40';
            if ($user_filter['platform_id'] == 2) $table = 'live';
            if ($user_filter['platform_id'] == 3) $table = 'new';
            if ($user_filter['platform_id'] == 5) $table = 'formaz';

            $collection = $this->connection->$table->mdl_course;

            if ($course){

                $row = $course;

                        $check = $collection->find([
                            'id' => (int)$row['id']
                        ]);

                        foreach ($check as $c) {
                            $mongo_id = $c['id'];
                        }

//                        echo $row['id'] . "\n";

                        if ($mongo_id > 0) {

                            $collection->updateOne(
                                ['id' => (int)$row["id"]],
                                [
                                    '$set' => [
                                        'id' => (int)$row["id"],
                                        'category' => (int)$row["category"],
                                        'fullname' => utf8_encode($row["fullname"]),
                                        'shortname' => utf8_encode($row["shortname"]),
                                        'enddate' => (int)$row["enddate"],
                                        'startdate' => (int)$row["startdate"],
                                        'timecreated' => (int)$row["timecreated"],
                                        'timemodified' => (int)$row["timemodified"],
                                    ]
                                ]
                            );

                            $response = 'Aggiornato';

                        } else {
                            $collection->insertOne([
                                'id' => (int)$row["id"],
                                'category' => (int)$row["category"],
                                'fullname' => utf8_encode($row["fullname"]),
                                'shortname' => utf8_encode($row["shortname"]),
                                'enddate' => (int)$row["enddate"],
                                'startdate' => (int)$row["startdate"],
                                'timecreated' => (int)$row["timecreated"],
                                'timemodified' => (int)$row["timemodified"],
                            ]);

                            $response = 'Inserito';
                        }

        }

            return $response;



//            return $response;
        }else{
            return false;
        }
    }

}
