<?php

$abs_path = '/home/admin/public_html';
if(isset($_SERVER['DOCUMENT_ROOT']) && strlen(trim($_SERVER['DOCUMENT_ROOT']))) $abs_path = $_SERVER['DOCUMENT_ROOT'];

require_once($abs_path . '/vendor/autoload.php');
require_once($abs_path . '/skilledin/report/config/connection/MongoDB/skilledin.php');
require_once($abs_path . '/skilledin/report/obj/Utility.php');
require_once($abs_path . '/skilledin/report/obj/Report.php');
class SkilledinReport {
    public $tool;
    public $old_report;
    public $connection;
    public $database = '';


    public function __construct($database){
        $this->mongoConnect();
        $this->setDatabase($database);
        $this->tool = new Utility();
        $this->old_report = new Report();
    }
    public function setDatabase($database){
        $this->database = $database;
    }
    public function mongoConnect(){
        $connection_string  = "mongodb+srv://";
        $connection_string .= conn_username . ":" . urlencode(conn_password) . "@" . conn_server;
        $connection_string .= "?retryWrites=true&w=majority";
        $this->connection   = new MongoDB\Client($connection_string);
    }

    //Piattaforme
    public function getPlatforms($user_filter = array(), $user_options = array()) {
        $connection = $this->connection;
        $database   = $this->database;

        //Definisco la collection
        $collection = $connection->$database->piattaforme;

        //Definisco filtri
        $filter  = array();
        if(isset($user_filter['id'])) $filter['id'] = (int) $user_filter['id'];
        //Definisco opizioni

        $options = array();

        //Eseguo la query
        $cursor = $collection->find($filter, $options);

        //Acquisisco i risultati
        $objects = array();
        foreach ($cursor as $item) {
            $objects[] = (array) $item;
        }

        //Restituisco il valore
        if(!empty($objects)) return $objects;
        return false;
    }
    public function getPlatform($platform_id){
        $platform = $this->getPlatforms(array('id' => (int) $platform_id));
        if($platform) return array_pop($platform);
        return false;
    }

    //Aziende piani formativi
    public function getAziendePianiFormativi($user_filter = array(), $user_options = array()){
        $connection = $this->connection;
        $database   = $this->database;

        //Definisco la collection
        $collection = $connection->$database->mdl_aziende_piani_formativi;

        //Definisco filtri
        $filter  = array();
        if(isset($user_filter['skilledin_plan_id'])) $filter['skilledin_plan_id'] = $user_filter['skilledin_plan_id'];
        if(isset($user_filter['nome_azienda']))      $filter['nome_azienda']      = $user_filter['nome_azienda'];
        if(isset($user_filter['piva_cf']))           $filter['piva_cf']           = $user_filter['piva_cf'];
        if(isset($user_filter['piano']))             $filter['piano']             = $user_filter['piano'];
        if(isset($user_filter['data_inizio_piano'])) $filter['data_inizio_piano'] = $user_filter['data_inizio_piano'];
        if(isset($user_filter['data_fine_piano']))   $filter['data_fine_piano']   = $user_filter['data_fine_piano'];


        //Definisco opzioni
        $options = array();

        //Eseguo la query
        $cursor = $collection->find($filter, $options);

        //Acquisisco i risultati
        $objects = array();
        foreach ($cursor as $item) {
            $objects[] = (array) $item;
        }

        //Restituisco il valore
        if(!empty($objects)) return $objects;
        return false;
    }
    public function getAziendaPianoFormativo($skilledin_plan_id){
        $items = $this->getAziendePianiFormativi(array('skilledin_plan_id' => $skilledin_plan_id));
        if($items) return array_shift($items);
        return false;
    }

    public function addAziendePianiFormativi($data = array()){
        $connection = $this->connection;
        $database   = $this->database;

        //Definisco la collection
        $collection = $connection->$database->mdl_aziende_piani_formativi;

        //Eseguo l'inserimento
        $filter = (isset($data['skilledin_plan_id'])) ? array('skilledin_plan_id' => $data['skilledin_plan_id']) : $data;
        if(isset($data['skilledin_plan_id'])) unset($data['skilledin_plan_id']);
        $result = $collection->updateOne($filter, array('$set' => $data), array('upsert' => true));
        return $result;
    }

    //Aziende piani formativi corsi
    public function getAziendePianiFormativiCorsi($user_filter = array(), $user_options = array()){
        $connection = $this->connection;
        $database   = $this->database;


        //Definisco la collection
        $collection = $connection->$database->mdl_aziende_piani_corsi;

        //Definisco filtri
        $filter  = array();
        if(isset($user_filter['id_aziende_piani'])) $filter['id_aziende_piani'] = (int) $user_filter['id_aziende_piani'];

        //Definisco opzioni
        $options = array();

        //Eseguo la query
        $cursor = $collection->find($filter, $options);

        //Acquisisco i risultati
        $objects = array();
        foreach ($cursor as $item) {
            $objects[] = (array) $item;
        }

        //Restituisco il valore
        if(!empty($objects)) return $objects;
        return false;
    }
    public function addAziendaPianoFormativoCorso($data = array()){
        $connection = $this->connection;
        $database   = $this->database;

        //Definisco la collection
        $collection = $connection->$database->mdl_aziende_piani_corsi;

        //Eseguo l'inserimento
        $filter = $data;
        $result = $collection->updateOne($filter, array('$set' => $data), array('upsert' => true));
        return $result;
    }
    public function deleteAziendaPianoFormativoCorso($user_filter = array(), $user_options = array()){
        $connection = $this->connection;
        $database   = $this->database;

        //Definisco la collection
        $collection = $connection->$database->mdl_aziende_piani_corsi;

        //Definisco filtri
        $filter  = array();
        if(isset($user_filter['id']))               $filter['id']               = (int) $user_filter['id'];
        if(isset($user_filter['id_aziende_piani'])) $filter['id_aziende_piani'] = (int) $user_filter['id_aziende_piani'];
        if(isset($user_filter['azienda']))          $filter['azienda']          = $user_filter['azienda'];
        if(isset($user_filter['id_corso']))         $filter['id_corso']         = (int) $user_filter['id_corso'];
        if(isset($user_filter['nome_corso']))       $filter['nome_corso']       = $user_filter['nome_corso'];
        if(isset($user_filter['durata_corso']))     $filter['durata_corso']     = $user_filter['durata_corso'];

        //Definisco opzioni
        $options = array();
        //Eseguo la query
        $collection->deleteOne($filter);
    }

    public function calculateReportAziendePianiFormativi($skilledin_plan_id){
        $company_plan_data = $this->getAziendaPianoFormativo($skilledin_plan_id);
        if($company_plan_data){
            $company_plan_data['total_course_duration'] = 0;
            $company_plan_data['total_course_time']     = 0;
            $company_plan_data['scotamento_time']       = 0;
            $company_plan_data['partial_course_time']   = 0;
            $company_plan_data['user_data_table']       = array();
            $datediff = $company_plan_data['stamp_fine_piano']- time();
            $company_plan_data['days_to_go'] = round($datediff / (60 * 60 * 24));
            $platforms            = $this->getPlatforms();
            foreach ($platforms as $platform) {
                $company_plan_data['courses'] = array();
                //Definizione di object
                $subobject = false;
                if($platform['id'] == 5) $subobject = new Formazione('formaz');
                if($platform['id'] == 1) $subobject = new Formazione40('f40');
                if($platform['id'] == 0) $subobject = new FondoNuoveCompetenze('fnc');
                if($platform['id'] == 2) $subobject = new Live('live');
                if($platform['id'] == 3) $subobject = new PlatformNew('new');

                //Se per la piattaforma non è possibile istanziare l'oggetto, la piattaforma non è gestita
                if(!$subobject)     continue;
                $company_users        = $subobject->getUserListFromCompanyName($company_plan_data['nome_azienda']);

                //Se per la piattaforma non vengono trovati utenti, la piattaforma non è usata dall'azienda
                if(!$company_users) continue;

                $company_subplan_data = $subobject->getAziendaPianoFormativo($skilledin_plan_id);
                if($company_subplan_data){
                    $company_plan_courses = $subobject->getAziendePianiFormativiCorsi(array('id_aziende_piani' => $company_subplan_data['id']));
                    if($company_plan_courses){
                        foreach ($company_plan_courses as $company_plan_course){
                            if(!isset($company_plan_course['id'])) continue;
                            $company_plan_data['courses'][] = $company_plan_course['id_corso'];
                        }
                    }
                    if(!empty($company_plan_data['courses'])){
                        foreach ($company_plan_data['courses'] as $course_id){
                            $filter    = array();
                            $filter['course_id']   = $course_id;
                            $course_modules = $subobject->getHVPCoursesModules($filter, array('sort' => 'id'));
                            if($course_modules){
                                foreach ($course_modules as $course_module){
                                    $course_module['object_time'] = 0;
                                    $split = isset($course_module['json_content']) ? explode('"time":', $course_module['json_content']) : array();
                                    if (isset($split[1])) {
                                        $split = explode(",", $split[1]);
                                        $split = explode(".", $split[0]);
                                        $course_module['object_time'] = $split[0];
                                    }
                                    $company_plan_data['total_course_duration'] += $course_module['object_time'];
                                }
                            }
                        }
                    }

                    $filter                      = array();
                    $filter['user_company_name'] = $company_plan_data['nome_azienda'];
                    $filter['courses_id']        = $company_plan_data['courses'];
                    $filter['start_from']        = $company_plan_data['stamp_inizio_piano'];
                    $filter['start_to']          = $company_plan_data['stamp_fine_piano'];

                    //REPORT HVP
                    $data                        = $subobject->getHVPStoredSessions($filter, array('sort' => 'timecreated'));
                    $aggregate                   = $subobject->getSHVPReportData($data);
                    if($aggregate){
                        foreach ($aggregate['array_session_values'] as $user_report) {
                            if(!isset($company_plan_data['user_data_table'][$user_report['user_fiscalcode']])){
                                $company_plan_data['user_data_table'][$user_report['user_fiscalcode']] = array('fiscal_code'       => $user_report['user_fiscalcode'],
                                                                                                               'name'              => $user_report['user_fullname']  ,
                                                                                                               'partial_user_time' => 0,
                                                                                                               'partial_user_hour' => 0  );
                            }
                            $partial_user_time = 0;
                            foreach ($aggregate['array_session_dates'] as $data) {
                                $partial_user_time  += $this->tool->convertTimeToSeconds($user_report[$data]);
                            }
                            $company_plan_data['user_data_table'][$user_report['user_fiscalcode']]['partial_user_time'] += $partial_user_time;
                            $company_plan_data['user_data_table'][$user_report['user_fiscalcode']]['partial_user_hour'] += $this->tool->convertSecondsToHour($partial_user_time);
                            $company_plan_data['total_course_time']                                                     += $partial_user_time;
                            $company_plan_data['partial_course_time']                                                   += $partial_user_time;
                        }
                    }
                    $filter                  = array();
                    $filter['company_users'] = $company_users;
                    $filter['start_from']    = $company_plan_data['stamp_inizio_piano'];
                    $filter['start_to']      = $company_plan_data['stamp_fine_piano'];
                    $data                    = $subobject->getVideoTimeSessions($filter, array('sort' => 'timestarted'));
                    if($data){
                        $data['company_name']   = $company_plan_data['nome_azienda'];
                        $aggregate              = $subobject->getVideoTimeReportData($data);
                        if($aggregate){
                            foreach($aggregate['array_session_values'] as $item){
                                var_dump($item);
                                exit;

                                $company_plan_data['total_course_time']   += $this->tool->convertTimeToSeconds($item['total_time']);
                                $company_plan_data['partial_course_time'] += $this->tool->convertTimeToSeconds($item['total_time']);
                            }
                        }
                    }

                    //REPORT BBB (Corsi live, senza durata)
                    if(!empty($company_plan_data['courses'])){
                        foreach($company_plan_data['courses'] as $course_id){
                            $bbbName = $this->old_report->getNameAttivitaBBB($subobject->database);
                            $nameCorsi = $this->old_report->getAllNameCourse($subobject->database);
                            $array_id = array_values($company_users);
                            $res    = $this->old_report->getAggregateBBB($subobject->database, $company_plan_data['stamp_inizio_piano'], $company_plan_data['stamp_fine_piano'], $course_id, $array_id);
                            if($res){
                                $result = $this->old_report->constructBbbArray($res, $bbbName, $nameCorsi);
                                $finale = $this->old_report->constructSintesiReport($result);
                                $totResult = count($finale);
                                $tmp = $this->old_report->constructMinCumulatiReport($finale, $totResult);
                                $tmp = explode("*", $tmp);
                                $new = json_decode($tmp[0], true);
                                foreach($new as $item){
                                    var_dump($item);
                                    exit;

                                    $company_plan_data['total_course_time'] += $this->tool->convertTimeToSeconds($item['tempo_tot']);
                                }
                            }
                        }
                    }
                }
            }

            $count_users = ($company_users) ? count($company_users) : 0;

            $company_plan_data['live_course_time']      = $company_plan_data['total_course_time'] - $company_plan_data['partial_course_time'];
            $company_plan_data['unit_course_duration_time']  = $company_plan_data['total_course_duration'];
            $company_plan_data['total_course_duration'] = ($company_plan_data['total_course_duration'] * $count_users);
            $company_plan_data['scotamento_time']       = $company_plan_data['total_course_duration'] - $company_plan_data['partial_course_time'];

            $company_plan_data['live_course_time']      = $this->tool->convertSecondsToTime($company_plan_data['live_course_time']);
            $company_plan_data['partial_course_time']   = $this->tool->convertSecondsToTime($company_plan_data['partial_course_time']);
            $company_plan_data['total_course_hour']     = $this->tool->convertSecondsToHour($company_plan_data['total_course_time']);
            $company_plan_data['total_course_time']     = $this->tool->convertSecondsToTime($company_plan_data['total_course_time']);
            $company_plan_data['total_course_duration'] = $this->tool->convertSecondsToTime($company_plan_data['total_course_duration']);
            $company_plan_data['unit_course_duration_hour']  = $this->tool->convertSecondsToHour($company_plan_data['unit_course_duration_time']);
            $company_plan_data['unit_course_duration']  = $this->tool->convertSecondsToTime($company_plan_data['unit_course_duration_time']);
            $company_plan_data['scotamento_time']       = $this->tool->convertSecondsToTime($company_plan_data['scotamento_time']);
            return $company_plan_data;
        }
        return false;
    }
    //Utenti
    public function getUsers($user_filter = array(), $user_options = array()) {
        $connection = $this->connection;
        $database   = $this->database;

        //Definisco la collection
        $collection = $connection->$database->mdl_utenti_complete;

        //Definisco filtri
        $filter  = array();
        if(isset($user_filter['user_id'])) $filter['userid'] = $user_filter['user_id'];

        //Definisco opzioni
        $options = array();

        //Eseguo la query
        $cursor = $collection->find($filter, $options);

        //Acquisisco i risultati
        $objects = array();
        foreach ($cursor as $item) {
            $objects[] = (array) $item;
        }

        //Restituisco il valore
        if(!empty($objects)) return $objects;
        return false;
    }
    public function getUser($user_id){
        $user = $this->getUsers(array('user_id' => $user_id));
        if($user) return array_shift($user);
        return false;
    }
    public function getUserListFromCompanyName($company_name = ''){
        $connection = $this->connection;
        $database   = $this->database;

        //Definisco la collection
        $collection = $connection->$database->mdl_user_info_data;

        //Definisco filtri
        $filter  = array();
        $filter['data'] = $company_name;

        //Definisco opzioni
        $options = array();

        //Eseguo la query
        $cursor = $collection->find($filter, $options);

        //Acquisisco i risultati
        $objects = array();
        foreach ($cursor as $item) {
            $item = (array) $item;
            $objects[] = $item['userid'];
        }

        //Restituisco il valore
        if(!empty($objects)) return $objects;
        return false;
    }

    //Aziende
    public function addAziendaPiattaforma($data = array()){
        $connection = $this->connection;
        $database   = $this->database;

        //Definisco la collection
        $collection = $connection->$database->aziende_piattaforme;

        //Eseguo l'inserimento
        $filter = $data;
        $result = $collection->updateOne($filter, array('$set' => $data), array('upsert' => true));
        return $result;
    }
    public function getCompaniesFromAggregation($user_filter = array(), $user_options = array()) {
        $connection = $this->connection;
        $database   = $this->database;

        //Definisco la collection
        $collection = $connection->$database->aziende_piattaforme;

        //Definisco filtri
        $filter  = array();
        if(isset($user_filter['plaftform_id'])) $filter['plaftform_id'] = $user_filter['plaftform_id'];

        //Definisco opzioni
        $options = array();
        if(isset($user_options['sort'])){
            $sorting = $user_options['sorting'] ?? 1;
            $options[] = array('sort' => array($user_options['sort'] => $sorting));
        }

        //Eseguo la query
        $cursor = $collection->find($filter, $options);

        //Acquisisco i risultati
        $objects = array();
        foreach ($cursor as $item) {
            $item = (array) $item;
            $objects[$item['company_name']] = $item['company_name'];
        }

        //Restituisco il valore
        if(!empty($objects)) return $objects;
        return false;
    }
    public function getCompaniesFromUsers($user_filter = array(), $user_options = array()){
        $connection = $this->connection;
        $database   = $this->database;

        //Definisco la collection
        $collection = $connection->$database->mdl_user_info_data;

        $cursor = $collection->find(
                ['data'    => ['$ne' => ""]      ,
                 'id'      => ['$ne' => 0]       ,
                 'fieldid' => ['$eq' => 1]       ,
                 'data'    => ['$exists' => true]
                ],
                ['projection' => ['data' => 1],
                                 ['sort' => ['data' => 1]],
                ]
        );

        $array_aziende = array();
        if ($cursor) {
            $tmp = array();
            foreach ($cursor as $singolo) {
                if (!in_array($singolo['data'], $array_aziende)) {
                    $singolo['data']= trim($singolo['data']);
                    $tmp = $singolo['data'];
                    array_push($array_aziende, $tmp);
                }
            }
        }
        if(!empty($array_aziende)) return $array_aziende;
        return false;
    }
    //Corsi
    public function getCourses($user_filter = array(), $user_options = array()) {
        $connection = $this->connection;
        $database   = $this->database;



        //Definisco la collection
        $collection = $connection->$database->mdl_course;

        //Definisco filtri
        $filter  = array();
        if(isset($user_filter['course_id'])) $filter['id'] = (int) $user_filter['course_id'];

        //Definisco opzioni
        $options = array();
        //Eseguo la query
        $cursor = $collection->find($filter, $options);
        //Acquisisco i risultati
        $objects = array();
        foreach ($cursor as $item) {

            $objects[] = (array) $item;
        }

        //Restituisco il valore
        if(!empty($objects)) return $objects;
        return false;
    }
    public function getCourse($course_id){
        $course = $this->getCourses(array('course_id' => $course_id));
        if($course) return array_shift($course);
        return false;
    }

    //Moduli (HVP)
    public function getHVPCoursesModules($user_filter = array(), $user_options = array()){
        $connection = $this->connection;
        $database   = $this->database;

        //Definisco la collection
        $collection = $connection->$database->mdl_hvp;

        //Definisco filtri
        $filter  = array();
        if(isset($user_filter['course_id'])) $filter['course'] = (int) $user_filter['course_id'];
        if(isset($user_filter['module_id'])) $filter['id']     = (int) $user_filter['module_id'];
        $match = ['$match' => $filter];

        //Definisco opizioni
        $options = array();
        if(isset($user_options['sort'])){
            $sorting = $user_options['sorting'] ?? 1;
            $sort = ['$sort' =>  [$user_options['sort'] => $sorting]];
        }else{
            $sort = array();
        }

        $pipeline   = array();
        $pipeline[] = $match;
        if(!empty($sort)) $pipeline[] = $sort;

        //Eseguo la query
        $cursor = $collection->aggregate($pipeline);
        $response = array();
        foreach ($cursor as $item){
            $response[$item['id']] = array('id'           => $item['id']          ,
                                           'name'         => $item['name']        ,
                                           'json_content' => $item['json_content'] );
        }
        if(!empty($response)) return $response;
        return false;
    }
    public function deleteHVPCourseModule($course_module_id){
        $connection = $this->connection;
        $database   = $this->database;

        //Definisco la collection
        $collection = $connection->$database->mdl_hvp;

        //Eseguo l'eliminazione
        $collection->deleteOne(array('id' => $course_module_id));
    }

    public function getCoursesCompletitions($user_list, $course_module_list) {
        $connection = $this->connection;
        $database   = $this->database;

        //Definisco la collection
        $collection = $connection->$database->mdl_course_completions;

        //Definisco filtri
        $filter  = array();
        if(isset($user_filter['user_id']))           $filter['userid']             = $user_filter['user_id'];
        if(isset($user_filter['course_id']))         $filter['courseid']           = $user_filter['course_id'];
        if(isset($user_filter['module_id']))         $filter['objectid']           = $user_filter['module_id'];
        if(isset($user_filter['contextinstanceid'])) $filter['contextinstanceid']  = $user_filter['contextinstanceid'];
        if(isset($user_filter['component']))         $filter['component']          = $user_filter['component'];
        if(isset($user_filter['start_from']))        $filter['timecreated']['$gt'] = $user_filter['start_from'];
        if(isset($user_filter['start_to']))          $filter['timecreated']['$lt'] = $user_filter['start_to'];
        if(isset($user_filter['company_users']))     $filter['userid']['$in']      = $user_filter['company_users'];

        $match = ['$match' => $filter];
        if(isset($user_options['sort'])){
            $sorting = $user_options['sorting'] ?? 1;
            $sort = ['$sort' =>  [$user_options['sort'] => $sorting]];
        }else{
            $sort = array();
        }

        $lookup_user          = ['$lookup'  => ['from'         => 'mdl_utenti_complete',
            'localField'   => 'userid'             ,
            'foreignField' => 'userid'             ,
            'as'           => 'user_info'           ]];
        $lookup_course        = ['$lookup'  => ['from'         => 'mdl_course' ,
            'localField'   => 'courseid'   ,
            'foreignField' => 'id'         ,
            'as'           => 'course_info' ]];
        $lookup_object        = ['$lookup'  => ['from'     => 'mdl_logstore_standard_log',
            'let'      => ['contextinstanceid' => '$contextinstanceid'],
            'pipeline' => [['$match'  => ['$expr' => ['$and' => [['$eq' => ['$contextinstanceid',
                '$$contextinstanceid']],
                ['$eq' => ['$component',
                    'mod_hvp']],
                ['$ne' => ['$objectid',
                    0]]]]]],
                ['$project' => ['objectid'     => 1 ]],
                ['$limit'   => 1]],
            'as'        => 'calculated_object_id',]];
        $lookup_last_progress = ['$lookup'  => ['from'     => 'mdl_logstore_standard_log',
            'let'      => ['contextinstanceid' => '$contextinstanceid',
                'userid'            => '$userid',
                'timecreated'       => '$timecreated'],
            'pipeline' => [0 => ['$match' => ['$and' => [0 => ['$expr' => ['$eq' => [0 => '$userid'            ,
                1 => '$$userid'            ]]],
                1 => ['$expr' => ['$eq' => [0 => '$contextinstanceid' ,
                    1 => '$$contextinstanceid' ]]],
                2 => ['$expr' => ['$eq' => [0 => '$component'         ,
                    1 => 'mod_hvp'             ]]],
                3 => ['$expr' => ['$eq' => [0 => '$action'            ,
                    1 => 'progress'            ]]],
                4 => ['$expr' => ['$lt' => [0 => '$timecreated'       ,
                    1 => '$$timecreated'       ]]]]]],
                1 =>['$sort'  => ['timecreated' => -1]],
                2 =>['$limit' => 1]],
            'as'       => 'last_progress']];
        $lookup_module        = ['$lookup'  => ['from'        => 'mdl_hvp'    ,
            'localField'   => 'objectid'   ,
            'foreignField' => 'id'         ,
            'as'          => 'module_info' ]];
        $set                  = ['$set'     => ['user_fullname'      => ['$arrayElemAt' => ['$user_info.nome',           0]],
            'username'           => ['$arrayElemAt' => ['$user_info.username',       0]],
            'user_fiscalcode'    => ['$arrayElemAt' => ['$user_info.idnumber',       0]],
            'user_email'         => ['$arrayElemAt' => ['$user_info.email',          0]],
            'user_company_name'  => ['$arrayElemAt' => ['$user_info.azienda',        0]],
            'user_city_of_birth' => ['$arrayElemAt' => ['$user_info.citta',          0]],
            'user_date_of_birth' => ['$arrayElemAt' => ['$user_info.data',           0]],
            'user_phone1'        => ['$arrayElemAt' => ['$user_info.phone1',         0]],
            'user_phone2'        => ['$arrayElemAt' => ['$user_info.phone2',         0]],
            'course_name'        => ['$arrayElemAt' => ['$course_info.fullname',     0]],
            'object_name'        => ['$arrayElemAt' => ['$module_info.name',         0]],
            'object_data'        => ['$arrayElemAt' => ['$module_info.json_content', 0]] ]];
        $project              = ['$project' => ['id'                   => 1,
            'userid'               => 1,
            'username'             => 1,
            'user_fullname'        => 1,
            'user_fiscalcode'      => 1,
            'user_email'           => 1,
            'user_city_of_birth'   => 1,
            'user_date_of_birth'   => 1,
            'user_company_name'    => 1,
            'phone1'               => 1,
            'phone2'               => 1,
            'courseid'             => 1,
            'course_name'          => 1,
            'objectid'             => 1,
            'calculated_object_id' => 1,
            'object_name'          => 1,
            'object_data'          => 1,
            'contextid'            => 1,
            'contextlevel'         => 1,
            'contextinstanceid'    => 1,
            'action'               => 1,
            'timecreated'          => 1,
            'date'                 => 1,
            'time'                 => 1,
            'other'                => 1,
            'last_progress'        => 1 ]];

        //Definisco pipeline
        $pipeline   = array();
        $pipeline[] = $match;
        $pipeline[] = $lookup_user;
        $pipeline[] = $lookup_course;
        $pipeline[] = $lookup_object;
        $pipeline[] = $lookup_module;
        $pipeline[] = $set;
        $pipeline[] = $project;
        $pipeline[] = $sort;

        //Eseguo la query
        $cursor = $collection->aggregate($pipeline, array('allowDiskUse' => true));

        //Acquisisco i risultati
        $objects = array();
        foreach ($cursor as $item) {
            $item['calculated_object_id'] = (array) $item['calculated_object_id'];
            $item['calculated_object_id'] = (array) array_shift($item['calculated_object_id']);
            $item['calculated_object_id'] = $item['calculated_object_id']['objectid'];
            $item['object_time']          = 0;
            $objects[]                    = (array) $item;
        }

        //Restituisco il valore
        if(!empty($objects)) return $objects;
        return false;
    }
    public function getHVPObjects($user_filter = array(), $user_options = array()) {
        $connection = $this->connection;
        $database   = $this->database;

        //Definisco la collection
        $collection = $connection->$database->mdl_hvp;

        //Definisco filtri
        $filter  = array();
        if(isset($user_filter['objectid'])) $filter['id'] = $user_filter['objectid'];

        //Definisco opzioni
        $options = array();

        //Eseguo la query
        $cursor = $collection->find($filter, $options);

        //Acquisisco i risultati
        $objects = array();
        foreach ($cursor as $item) {
            $objects[] = (array) $item;
        }

        //Restituisco il valore
        if(!empty($objects)) return $objects;
        return false;
    }
    public function getHVPObject($object_id){
        $hvp_object = $this->getHVPObjects(array('objectid' => $object_id));
        if($hvp_object) return array_shift($hvp_object);
        return false;
    }
    public function getLogs($user_filter = array(), $user_options = array()){
        $connection = $this->connection;
        $database   = $this->database;

        //Definisco la collection
        $collection = $connection->$database->mdl_logstore_standard_log;

        //Definisco filtri
        $filter  = array();
        if(isset($user_filter['user_id']))           $filter['userid']             = $user_filter['user_id'];
        if(isset($user_filter['course_id']))         $filter['courseid']           = $user_filter['course_id'];
        if(isset($user_filter['module_id']))         $filter['objectid']           = $user_filter['module_id'];
        if(isset($user_filter['contextinstanceid'])) $filter['contextinstanceid']  = $user_filter['contextinstanceid'];
        if(isset($user_filter['component']))         $filter['component']          = $user_filter['component'];
        if(isset($user_filter['start_from']))        $filter['timecreated']['$gt'] = $user_filter['start_from'];
        if(isset($user_filter['start_to']))          $filter['timecreated']['$lt'] = $user_filter['start_to'];
        if(isset($user_filter['company_users']))     $filter['userid']['$in']      = $user_filter['company_users'];

        $match = ['$match' => $filter];
        if(isset($user_options['sort'])){
            $sorting = $user_options['sorting'] ?? 1;
            $sort = ['$sort' =>  [$user_options['sort'] => $sorting]];
        }else{
            $sort = array();
        }

        $lookup_user          = ['$lookup'  => ['from'         => 'mdl_utenti_complete',
            'localField'   => 'userid'             ,
            'foreignField' => 'userid'             ,
            'as'           => 'user_info'           ]];
        $lookup_course        = ['$lookup'  => ['from'         => 'mdl_course' ,
            'localField'   => 'courseid'   ,
            'foreignField' => 'id'         ,
            'as'           => 'course_info' ]];
        $lookup_object        = ['$lookup'  => ['from'     => 'mdl_logstore_standard_log',
            'let'      => ['contextinstanceid' => '$contextinstanceid'],
            'pipeline' => [['$match'  => ['$expr' => ['$and' => [['$eq' => ['$contextinstanceid',
                '$$contextinstanceid']],
                ['$eq' => ['$component',
                    'mod_hvp']],
                ['$ne' => ['$objectid',
                    0]]]]]],
                ['$project' => ['objectid'     => 1 ]],
                ['$limit'   => 1]],
            'as'        => 'calculated_object_id',]];
        $lookup_last_progress = ['$lookup'  => ['from'     => 'mdl_logstore_standard_log',
            'let'      => ['contextinstanceid' => '$contextinstanceid',
                'userid'            => '$userid',
                'timecreated'       => '$timecreated'],
            'pipeline' => [0 => ['$match' => ['$and' => [0 => ['$expr' => ['$eq' => [0 => '$userid'            ,
                1 => '$$userid'            ]]],
                1 => ['$expr' => ['$eq' => [0 => '$contextinstanceid' ,
                    1 => '$$contextinstanceid' ]]],
                2 => ['$expr' => ['$eq' => [0 => '$component'         ,
                    1 => 'mod_hvp'             ]]],
                3 => ['$expr' => ['$eq' => [0 => '$action'            ,
                    1 => 'progress'            ]]],
                4 => ['$expr' => ['$lt' => [0 => '$timecreated'       ,
                    1 => '$$timecreated'       ]]]]]],
                1 =>['$sort'  => ['timecreated' => -1]],
                2 =>['$limit' => 1]],
            'as'       => 'last_progress']];
        $lookup_module        = ['$lookup'  => ['from'        => 'mdl_hvp'    ,
            'localField'   => 'objectid'   ,
            'foreignField' => 'id'         ,
            'as'          => 'module_info' ]];
        $set                  = ['$set'     => ['user_fullname'      => ['$arrayElemAt' => ['$user_info.nome',           0]],
            'username'           => ['$arrayElemAt' => ['$user_info.username',       0]],
            'user_fiscalcode'    => ['$arrayElemAt' => ['$user_info.idnumber',       0]],
            'user_email'         => ['$arrayElemAt' => ['$user_info.email',          0]],
            'user_company_name'  => ['$arrayElemAt' => ['$user_info.azienda',        0]],
            'user_city_of_birth' => ['$arrayElemAt' => ['$user_info.citta',          0]],
            'user_date_of_birth' => ['$arrayElemAt' => ['$user_info.data',           0]],
            'user_phone1'        => ['$arrayElemAt' => ['$user_info.phone1',         0]],
            'user_phone2'        => ['$arrayElemAt' => ['$user_info.phone2',         0]],
            'course_name'        => ['$arrayElemAt' => ['$course_info.fullname',     0]],
            'object_name'        => ['$arrayElemAt' => ['$module_info.name',         0]],
            'object_data'        => ['$arrayElemAt' => ['$module_info.json_content', 0]] ]];
        $project              = ['$project' => ['id'                   => 1,
            'userid'               => 1,
            'username'             => 1,
            'user_fullname'        => 1,
            'user_fiscalcode'      => 1,
            'user_email'           => 1,
            'user_city_of_birth'   => 1,
            'user_date_of_birth'   => 1,
            'user_company_name'    => 1,
            'phone1'               => 1,
            'phone2'               => 1,
            'courseid'             => 1,
            'course_name'          => 1,
            'objectid'             => 1,
            'calculated_object_id' => 1,
            'object_name'          => 1,
            'object_data'          => 1,
            'contextid'            => 1,
            'contextlevel'         => 1,
            'contextinstanceid'    => 1,
            'action'               => 1,
            'timecreated'          => 1,
            'date'                 => 1,
            'time'                 => 1,
            'other'                => 1,
            'last_progress'        => 1 ]];

        //Definisco pipeline
        $pipeline   = array();
        $pipeline[] = $match;
        $pipeline[] = $lookup_user;
        $pipeline[] = $lookup_course;
        $pipeline[] = $lookup_object;
        $pipeline[] = $lookup_module;
        $pipeline[] = $set;
        $pipeline[] = $project;
        $pipeline[] = $sort;

        //Eseguo la query
        $cursor = $collection->aggregate($pipeline, array('allowDiskUse' => true));

        //Acquisisco i risultati
        $objects = array();
        foreach ($cursor as $item) {
            $item['calculated_object_id'] = (array) $item['calculated_object_id'];
            $item['calculated_object_id'] = (array) array_shift($item['calculated_object_id']);
            $item['calculated_object_id'] = $item['calculated_object_id']['objectid'];
            $item['object_time']          = 0;
            $objects[]                    = (array) $item;
        }

        //Restituisco il valore
        if(!empty($objects)) return $objects;
        return false;
    }
    public function getLogObjectFromUserContext($user_id, $course_id, $contextinstanceid){
        $filter = array();
        $filter['userid']            = $user_id;
        $filter['course_id']         = $course_id;
        $filter['contextinstanceid'] = $contextinstanceid;
        $logs = $this->getLogs($filter);
        if($logs) return array_shift($logs);
        return false;
    }
    public function getBBBSessions($user_filter = array(), $user_options = array()){
       $connection = $this->connection;
       $database   = $this->database;

       //Definisco la collection
       $collection = $connection->$database->mdl_logstore_standard_log;

       if(isset($user_filter['courses_id']) && isset($user_filter['user_company_name'])){
           if (empty($user_filter['courses_id'])) {
               $company_users = $this->getUserListFromCompanyName($user_filter['user_company_name']);
               $match = ['$match' => [
                   'userid' => ['$in' => $company_users],
                   'component' => "mod_bigbluebuttonbn",
                   'timecreated' => [
                       '$gte' => $user_filter['start_from'],
                       '$lte' => $user_filter['start_to']
                   ],
                   'action' => [
                       '$ne' => 'viewed'
                   ]
               ]];
           }
           else if (!isset($user_filter['user_company_name'])) {
               $match = ['$match' => [
                   'courseid' => ['$in' => $user_filter['courses_id']],
                   'component' => "mod_bigbluebuttonbn",
                   'timecreated' => [
                       '$gte' => $user_filter['start_from'],
                       '$lte' => $user_filter['start_to']
                   ],
                   'action' => [
                       '$ne' => 'viewed'
                   ]
               ]];
           }
           else {
               $company_users = $this->getUserListFromCompanyName($user_filter['user_company_name']);
               $match = ['$match' => [
                   'userid' => ['$in' => $company_users ],
                   'courseid' => ['$in' => $user_filter['courses_id']],
                   'component' => "mod_bigbluebuttonbn",
                       'timecreated' => [
                           '$gte' => $user_filter['start_from'],
                           '$lte' => $user_filter['start_to']
                       ],
                       'action' => [
                           '$ne' => 'viewed'
                       ]
                   ]];
               }
       }
       else if (isset($user_filter['courses_id']) && !isset($user_filter['user_company_name'])) {
               $match = ['$match' => [
                   'courseid' => ['$in' => $user_filter['courses_id']],
                   'component' => "mod_bigbluebuttonbn",
                   'timecreated' => [
                       '$gte' =>  $user_filter['start_from'],
                       '$lte' =>  $user_filter['start_to']
                   ],
                   'action' => [
                       '$ne' => 'viewed'
                   ]
               ]];
           }
       else if (!isset($user_filter['courses_id']) && isset($user_filter['user_company_name'])) {
           $company_users = $this->getUserListFromCompanyName($user_filter['user_company_name']);
               $match = ['$match' => [
                   'userid' => ['$in' => $company_users
                   ],
                   'component' => "mod_bigbluebuttonbn",
                   'timecreated' => [
                       '$gte' =>  $user_filter['start_from'],
                       '$lte' =>  $user_filter['start_to']
                   ],
                   'action' => [
                       '$ne' => 'viewed'
                   ]
               ]];
           }

       $pipeline = [$match, ['$lookup' => ['from'         => 'mdl_utenti_complete',
                                           'localField'   => 'userid',
                                           'foreignField' => 'userid',
                                           'as'           => 'info' ]],
                            ['$set'    => ['username'     => ['$arrayElemAt'  => ['$info.username', 0]],
                                           'email'        => ['$arrayElemAt'  => ['$info.email'   , 0]],
                                           'cf'           => ['$arrayElemAt'  => ['$info.idnumber', 0]],
                                           'nome'         => ['$arrayElemAt'  => ['$info.nome'    , 0]],
                                           'citta'        => ['$arrayElemAt'  => ['$info.citta'   , 0]],
                                           'data'         => ['$arrayElemAt'  => ['$info.data'    , 0]],
                                           'azienda'      => ['$arrayElemAt'  => ['$info.azienda' , 0]],
                                           'ruolo'        => [ '$arrayElemAt' => ['$info.ruolo'   , 0]],
                                           'phone1'       => ['$arrayElemAt' => ['$info.phone1'   , 0]],
                                           'phone2'       => ['$arrayElemAt' => ['$info.phone2'   , 0]] ]],
                            ['$project' => ['info'        => 0]]];
       $out = $collection->aggregate($pipeline);
       $res = $out->toArray();
       return $res;
    }
    public function getHVPSessions($user_filter = array(), $user_options = array()){
        $report_data = false;
        $module_data = array();
        $object_data = array();

        if(!is_array($user_filter)) $user_filter = array();
        $user_filter['component'] = 'mod_hvp';
        $logs = $this->getLogs($user_filter, $user_options);
        if($logs){
            $report_data = array();
            $reset_logs  = array();
            foreach($logs as $log){
                if($log['objectid'] == $log['calculated_object_id'] && !isset($module_data[$log['calculated_object_id']])){
                    if(!isset($log['object_name'])) $log['object_name'] = 'N/D';
                    if(!isset($log['object_data'])) $log['object_data'] = serialize('');
                    $module_data[$log['calculated_object_id']] = array('object_name' => $log['object_name'],
                        'object_data' => $log['object_data']);
                }
                if($log['other'] == 'RESET') $log['other'] = serialize($log['other']);
                $key = $log['userid'] .'_' . $log['courseid'] .'_' . $log['contextinstanceid'];
                if(!isset($object_data[$key]) && $log['objectid'] > 0) $object_data[$key] = $log['objectid'];
                if(strlen(trim($log['other']))  && $log['other'] == 'RESET'){
                    if((isset($reset_logs[$key]) && $reset_logs[$key] < $log['timecreated']) || !isset($reset_logs[$key])){
                        $reset_logs[$key] = $log['timecreated'];
                    }
                }
            }
            foreach($logs as $log){
                if($log['objectid'] == 0){
                    $log['objectid']    = $log['calculated_object_id'];
                    $log['object_name'] = $module_data[$log['calculated_object_id']]['object_name'];
                    $log['object_data'] = $module_data[$log['calculated_object_id']]['object_data'];
                }
                $key = $log['userid'] .'_' . $log['courseid'] .'_' . $log['contextinstanceid'];
                if((int) $log['objectid'] == 0){
                    if(isset($object_data[$key])){
                        $log['objectid'] = $object_data[$key];
                    } else {
                        $log['objectid'] = $log['calculated_object_id'][0]['objectid'];
                    }
                }
                $split = isset($log['object_data']) ? explode('"time":', $log['object_data']) : array();
                if (isset($split[1])) {
                    $split = explode(",", $split[1]);
                    $split = explode(".", $split[0]);
                    $log['object_time'] = $split[0];
                }

                $work_time = 0;
                $progress  = $log['other'];
                if($log['action'] == 'viewed'){
                    $progress = unserialize($progress);
                    if(strlen(trim($progress))){
                        $progress = json_decode($progress);
                        if(isset($progress->progress)) $work_time = $progress->progress;
                    }
                }
                if($log['action'] == 'progress'){
                    $progress  = json_decode($progress);
                    if(isset($progress->progress)) $work_time = $progress->progress;
                }
                if($log['action'] == 'submitted') {
                    //$work_time = $module_data[$log['objectid']]['object_time'];
                    $work_time = $log['object_time'];
                }

                $data = array();
                $data['log_id']               = $log['id'];
                $data['user_id']              = $log['userid'];
                $data['username']             = $log['username']           ?? 'N/D';
                $data['user_fullname']        = $log['user_fullname']      ?? 'N/D';
                $data['user_fiscalcode']      = $log['user_fiscalcode']    ?? 'N/D';
                $data['user_email']           = $log['user_email']         ?? 'N/D';
                $data['user_date_of_birth']   = isset($log['user_date_of_birth']) ? date('d/m/Y', $log['user_date_of_birth']) : 'N/D';
                $data['user_city_of_birth']   = $log['user_city_of_birth'] ?? 'N/D';
                $data['user_company_name']    = $log['user_company_name']  ?? 'N/D';
                $data['user_telephone']       = $log['user_telephone']     ?? 'N/D';
                $data['course_id']            = $log['courseid'];
                $data['course_name']          = $log['course_name']        ?? 'N/D';
                $data['object_id']            = $log['objectid'];
                $data['calculated_object_id'] = $log['calculated_object_id'];
                $data['object_name']          = $log['object_name'] ?? 'N/D';
                $data['object_time']          = $log['object_time'];
                $data['object_data']          = $log['object_data'] ?? serialize('');
                $data['contextid']            = $log['contextid'];
                $data['contextlevel']         = $log['contextlevel'];
                $data['contextinstanceid']    = $log['contextinstanceid'];
                $data['action']               = $log['action'];
                $data['timecreated']          = $log['timecreated'];
                $data['date']                 = date('d/m/Y', $log['timecreated']);
                $data['time']                 = date('H:i:s', $log['timecreated']);
                $data['other']                = $log['other'];
                $data['work_time']            = $work_time;
                if($data['work_time'] > 0 && $data['object_time'] > 0 && $data['work_time'] > $data['object_time']){
                    $data['work_time'] = $data['object_time'];
                }
                if(isset($log['reset'])){
                    $data['reset'] = $log['reset'];
                }else{
                    $data['reset'] = false;
                    if(isset($reset_logs[$key])){
                        if($reset_logs[$key] > $log['timecreated']) $data['reset'] = true;
                    }
                }
                //return $data;
                $report_data[] = $data;
            }
        }
        return $report_data;
    }
    public function getHVPStoredSessions($user_filter = array(), $user_options = array()){
        $connection = $this->connection;
        $database   = $this->database;

        //Definisco la collection
        $collection = $connection->$database->skilledin_report;

        //Definisco filtri
        $filter  = array();
        if(isset($user_filter['user_id']))           $filter['user_id']            = $user_filter['user_id'];
        if(isset($user_filter['course_id']))         $filter['course_id']          = $user_filter['course_id'];
        if(isset($user_filter['courses_id']))        $filter['course_id']['$in']   = $user_filter['courses_id'];
        if(isset($user_filter['module_id']))         $filter['object_id']          = $user_filter['module_id'];
        if(isset($user_filter['contextinstanceid'])) $filter['contextinstanceid']  = $user_filter['contextinstanceid'];
        if(isset($user_filter['start_from']))        $filter['timecreated']['$gt'] = $user_filter['start_from'];
        if(isset($user_filter['start_to']))          $filter['timecreated']['$lt'] = $user_filter['start_to'];
        if(isset($user_filter['company_users']))     $filter['user_id']['$in']     = $user_filter['company_users'];
        if(isset($user_filter['user_company_name'])) $filter['user_company_name']  = $user_filter['user_company_name'];



        $match = ['$match' => $filter];
        if(isset($user_options['sort'])){
            $sorting = $user_options['sorting'] ?? 1;
            $sort = ['$sort' =>  [$user_options['sort'] => $sorting]];
        }else{
            $sort = ['$sort' =>  ['timecreated' => -1]];
        }

        //Definisco pipeline
        $pipeline   = array();
        $pipeline[] = $match;
        $pipeline[] = $sort;

        //Eseguo la query
        $cursor = $collection->aggregate($pipeline, array('allowDiskUse' => true));

        //Acquisisco i risultati
        $objects = array();
        foreach ($cursor as $item) {
            $objects[] = (array) $item;
        }

        //Restituisco il valore
        if(!empty($objects)) return $objects;
        return false;
    }
    public function getHVPReportData($data){
        $report       = array();
        $grouped_item = array();
        foreach($data as $item){
            $key = $item['date'] . '_' . $item['user_id'] . '_' . $item['course_id'] . '_' . $item['object_id'];
            $report_item = array();
            $report_item['date']               = $item['date'];
            $report_item['user_id']            = $item['user_id'];
            $report_item['username']           = $item['username'];
            $report_item['user_fullname']      = $item['user_fullname'];
            $report_item['user_fiscalcode']    = $item['user_fiscalcode'];
            $report_item['user_email']         = $item['user_email'];
            $report_item['user_date_of_birth'] = $item['user_date_of_birth'];
            $report_item['user_city_of_birth'] = $item['user_city_of_birth'];
            $report_item['user_company_name']  = $item['user_company_name'];
            $report_item['user_telephone']     = $item['user_telephone'];
            $report_item['course_id']          = $item['course_id'];
            $report_item['course_name']        = $item['course_name'];
            $report_item['object_id']          = $item['object_id'];
            $report_item['module_name']        = $item['object_name'];
            $report_item['module_data']        = $item['object_data'];
            if(!isset($grouped_item[$key])) $grouped_item[$key] = array('data' => $report_item, 'sessions' => array());
            $grouped_item[$key]['sessions'][] = $item;
        }
        foreach($grouped_item as $item){
            $activities    = array();
            $session_count = 0;
            foreach($item['sessions'] as $session){
                if($session['action'] == 'viewed'){
                    $session_count++;
                    $activities[$session_count] = array('start_session_time' => $session['time'] ,
                        'end_session_time'   => '23:59:59'       ,
                        'progress_time'      => '00:00:00'       ,
                        'session_time'       => '00:00:00'       ,
                        'reset'              => $session['reset'] );
                }
                if($session['action'] == 'progress' || $session['action'] == 'submitted'){
                    //Fine visualizzazione lezione corso
                    if($session_count > 0){
                        $activities[$session_count]['end_session_time'] = $session['time'];
                        if($session['action'] == 'progress'  && $session['work_time']   > $activities[$session_count]['progress_time']) $activities[$session_count]['progress_time'] = $session['work_time'];
                        if($session['action'] == 'submitted' && $session['object_time'] > $activities[$session_count]['progress_time']) $activities[$session_count]['progress_time'] = $session['object_time'];
                        $activities[$session_count]['reset'] = $session['reset'];
                    }else{
                        $session_count ++;
                        if($session['action'] == 'progress'){
                            $activities[$session_count] = array('start_session_time' => '00:00:00'           ,
                                'end_session_time'   => $session['time']     ,
                                'progress_time'      => $session['work_time'],
                                'session_time'       => '00:00:00'           ,
                                'reset'              => $session['reset']     );
                        }
                        if($session['action'] == 'submitted'){
                            $activities[$session_count] = array('start_session_time' => '00:00:00'           ,
                                'end_session_time'   => $session['time']       ,
                                'progress_time'      => $session['object_time'],
                                'session_time'       => '00:00:00'             ,
                                'reset'              => $session['reset']       );
                        }
                    }
                }
            }
            foreach($activities as $index => $activity){
                //Se la sessione non si chiude, ma esiste una sessione successiva, la sessione si chiude all'inizio della sessione successiva.
                if($activity['end_session_time'] == '23:59:59' && isset($activities[($index+1)])) $activity['end_session_time'] = $activities[($index+1)]['start_session_time'];

                $data                       = $item['data'];
                $data['start_session_time'] = $activity['start_session_time'];
                $data['end_session_time']   = $activity['end_session_time'];
                $data['progress_time']      = $this->tool->convertSecondsToTime($activity['progress_time']);
                $data['session_time']       = $this->tool->convertTimeToSeconds($activity['end_session_time']) - $this->tool->convertTimeToSeconds($activity['start_session_time']);
                $data['session_time']       = $this->tool->convertSecondsToTime($data['session_time']);
                $data['reset']              = $activity['reset'];
                $report[] = $data;
            }
        }
        return $report;
    }
    public function getSHVPReportData($data){
        $return               = false;
        $report               = array();
        $s_report             = array();
        $grouped_item         = array();
        $array_session_values = array();
        $array_session_dates  = array();
        $progressive_check    = array();

        if(is_array($data) && !empty($data)){
            foreach($data as $item){
                $key = $item['date'] . '_' . $item['user_id'] . '_' . $item['course_id'] . '_' . $item['object_id'];
                $report_item = array();
                $report_item['date']               = $item['date'];
                $report_item['user_id']            = $item['user_id'];
                $report_item['username']           = $item['username'];
                $report_item['user_fullname']      = $item['user_fullname'];
                $report_item['user_fiscalcode']    = $item['user_fiscalcode'];
                $report_item['user_email']         = $item['user_email'];
                $report_item['user_date_of_birth'] = $item['user_date_of_birth'];
                $report_item['user_city_of_birth'] = $item['user_city_of_birth'];
                $report_item['user_company_name']  = $item['user_company_name'];
                $report_item['user_telephone']     = $item['user_telephone'];
                $report_item['course_id']          = $item['course_id'];
                $report_item['course_name']        = $item['course_name'];
                $report_item['object_id']          = $item['object_id'];
                $report_item['module_name']        = $item['object_name'];
                $report_item['object_time']        = $item['object_time'];
                if(!isset($grouped_item[$key])) $grouped_item[$key] = array('data' => $report_item, 'sessions' => array());
                $grouped_item[$key]['sessions'][] = $item;
                $array_session_dates[$item['date']] = $item['date'];
            }
            foreach($grouped_item as $item){
                $activities    = array();
                $session_count = 0;
                foreach($item['sessions'] as $session){
                    if($session['reset'] == true) continue;
                    if(!isset($session['progress_time'])) $session['progress_time'] = 0;
                    if($session['action'] == 'viewed'){
                        $session_count ++;
                        $activities[$session_count] = array('start_session_time' => $session['time']         ,
                            'end_session_time'   => '23:59:59'               ,
                            'progress_time'      => $session['progress_time'],
                            'session_time'       => '00:00:00'               ,
                            'reset'              => $session['reset']        ,
                            'session_start_at'   => $session['progress_time'] );
                    }
                    if($session['action'] == 'progress' || $session['action'] == 'submitted'){
                        //Fine visualizzazione lezione corso
                        if($session_count > 0){
                            $activities[$session_count]['end_session_time'] = $session['time'];
                            if($session['action'] == 'progress'  && $session['work_time']   > $activities[$session_count]['progress_time']) $activities[$session_count]['progress_time'] = $session['work_time'];
                            if($session['action'] == 'submitted' && $session['object_time'] > $activities[$session_count]['progress_time']) $activities[$session_count]['progress_time'] = $session['object_time'];
                        }else{
                            $session_count ++;
                            if($session['action'] == 'progress'){
                                // $session['work_time'] -= $activities[$session_count]['progress_time'];
                                $activities[$session_count] = array('start_session_time' => '00:00:00'           ,
                                    'end_session_time'   => $session['time']     ,
                                    'progress_time'      => $session['work_time'],
                                    'session_time'       => '00:00:00'           ,
                                    'reset'              => $session['reset']    ,
                                    'session_start_at'   => $session['progress_time'] );
                            }
                            if($session['action'] == 'submitted'){
                                //$session['object_time'] -= $activities[$session_count]['progress_time'];
                                $activities[$session_count] = array('start_session_time' => '00:00:00'             ,
                                    'end_session_time'   => $session['time']       ,
                                    'progress_time'      => $session['object_time'],
                                    'session_time'       => '00:00:00'             ,
                                    'reset'              => $session['reset']      ,
                                    'session_start_at'   => $session['progress_time'] );
                            }
                        }
                    }
                }
                foreach($activities as $index => $activity){
                    //Se la sessione non si chiude, ma esiste una sessione successiva, la sessione si chiude all'inizio della sessione successiva.
                    if($activity['end_session_time'] == '23:59:59' && isset($activities[($index+1)])) $activity['end_session_time'] = $activities[($index+1)]['start_session_time'];

                    $data                       = $item['data'];
                    $data['start_session_time'] = $activity['start_session_time'];
                    $data['end_session_time']   = $activity['end_session_time'];
                    $data['progress_time']      = $activity['progress_time'];
                    $data['session_time']       = $this->tool->convertTimeToSeconds($activity['end_session_time']) - $this->tool->convertTimeToSeconds($activity['start_session_time']);
                    $data['session_time']       = $this->tool->convertSecondsToTime($data['session_time']);
                    $report[] = $data;
                }
            }
            foreach($report as $record){
                $key_1 = $record['user_id'];
                $key_2 = $record['date'];
                $key_3 = $record['course_id'];
                $key_4 = $record['object_id'];

                if(!isset($s_report[$key_1]))                                                        $s_report[$key_1]                                                         = array('user'         => $record, 'dates'   => array());
                if(!isset($s_report[$key_1]['dates'][$key_2]))                                       $s_report[$key_1]['dates'][$key_2]                                        = array('date_session' => $key_2 , 'courses' => array());
                if(!isset($s_report[$key_1]['dates'][$key_2]['courses'][$key_3]))                    $s_report[$key_1]['dates'][$key_2]['courses'][$key_3]                     = array('course'       => $record, 'objects' => array());
                if(!isset($s_report[$key_1]['dates'][$key_2]['courses'][$key_3]['objects'][$key_4])) $s_report[$key_1]['dates'][$key_2]['courses'][$key_3]['objects'][$key_4]  = array('object'       => $record, 'value'   => 0);

                if(!isset($progressive_check[$key_1]))                 $progressive_check[$key_1]                 = array();
                if(!isset($progressive_check[$key_1][$key_3]))         $progressive_check[$key_1][$key_3]         = array();
                if(!isset($progressive_check[$key_1][$key_3][$key_4])) $progressive_check[$key_1][$key_3][$key_4] = 0;

                if($record['progress_time'] > $progressive_check[$key_1][$key_3][$key_4]){
                    $progression = $record['progress_time'] - $progressive_check[$key_1][$key_3][$key_4];
                    $progressive_check[$key_1][$key_3][$key_4] = $record['progress_time'];
                    $s_report[$key_1]['dates'][$key_2]['courses'][$key_3]['objects'][$key_4]['value'] += $progression;
                }

            }
            foreach($s_report as $user_id => $user){
                $session_record = array();
                $session_record['user_id']            = $user['user']['user_id'];
                $session_record['username']           = $user['user']['username'];
                $session_record['user_fullname']      = $user['user']['user_fullname'];
                $session_record['user_fiscalcode']    = $user['user']['user_fiscalcode'];
                $session_record['user_email']         = $user['user']['user_email'];
                $session_record['user_date_of_birth'] = $user['user']['user_date_of_birth'];
                $session_record['user_city_of_birth'] = $user['user']['user_city_of_birth'];
                $session_record['user_company_name']  = $user['user']['user_company_name'];
                $session_record['user_telephone']     = $user['user']['user_telephone'];

                $total_time = 0;
                foreach($user['dates'] as $date){
                    foreach($date['courses'] as $course){
                        $session_record['course_id']   = $course['course']['course_id'];
                        $session_record['course_name'] = $course['course']['course_name'];
                        foreach ($course['objects'] as $object){
                            if(!isset($session_record[$date['date_session']])) $session_record[$date['date_session']] = 0;
                            $session_record[$date['date_session']] += $object['value'];
                        }
                    }
                    $total_time += $session_record[$date['date_session']];
                    $session_record[$date['date_session']] = $this->tool->convertSecondsToTime($session_record[$date['date_session']]);
                }
                $session_record['total_time'] = $this->tool->convertSecondsToTime($total_time);
                foreach($array_session_dates as $array_session_date){
                    if(!isset($session_record[$array_session_date])) $session_record[$array_session_date] = '00:00:00';
                }
                $array_session_values[] = $session_record;
            }
            $return = array('array_session_dates'  => $array_session_dates ,
                            'array_session_values' => $array_session_values );
        }
        return $return;
    }
    public function getSummaryHVPReportData($data){
        //Ciclo i dati ricevuti dal DB per consolidare i record
        $progressive_lesson = array();
        foreach ($data as $item){
            $k1 = $item['user_id'];
            $k2 = $item['course_id'];
            $k3 = $item['timecreated'];
            $k4 = $item['calculated_object_id'];

            if(!isset($progressive_lesson[$k1]))                                               $progressive_lesson[$k1]                                               = array('data' => $item, 'courses' => array());
            if(!isset($progressive_lesson[$k1]['courses'][$k2]))                               $progressive_lesson[$k1]['courses'][$k2]                               = array('data' => $item, 'dates'   => array());
            if(!isset($progressive_lesson[$k1]['courses'][$k2]['dates'][$k3]))                 $progressive_lesson[$k1]['courses'][$k2]['dates'][$k3]                 = array('data' => $item, 'lessons' => array());
            if(!isset($progressive_lesson[$k1]['courses'][$k2]['dates'][$k3]['lessons'][$k4])) $progressive_lesson[$k1]['courses'][$k2]['dates'][$k3]['lessons'][$k4] = 0;
            if($item['work_time'] > $progressive_lesson[$k1]['courses'][$k2]['dates'][$k3]['lessons'][$k4]) $progressive_lesson[$k1]['courses'][$k2]['dates'][$k3]['lessons'][$k4] = $item['work_time'];
        }

        $progressive_user_lesson = array();
        $return_users            = array();
        $return_courses          = array();
        foreach($progressive_lesson as $user_id => $user){
            if(!isset($progressive_user_lesson[$user_id])) $progressive_user_lesson[$user_id] = array();
            if(!isset($return_users[$user_id]))            $return_users[$user_id]            = $user['data'];
            foreach($user['courses'] as $course_id => $course){
                if(!isset($progressive_user_lesson[$user_id][$course_id])) $progressive_user_lesson[$user_id][$course_id] = array();
                if(!isset($return_courses[$course_id]))                    $return_courses[$course_id]                    = $course['data'];
                foreach($course['dates'] as $date){
                    if(!isset($progressive_user_lesson[$user_id][$course_id]['total_time'])) $progressive_user_lesson[$user_id][$course_id]['total_time'] = 0;
                    foreach($date['lessons'] as $lesson_id => $lesson_work_time){
                        if(!isset($progressive_user_lesson[$user_id][$course_id][$lesson_id])) $progressive_user_lesson[$user_id][$course_id][$lesson_id] = 0;
                        if($lesson_work_time > $progressive_user_lesson[$user_id][$course_id][$lesson_id]){
                            $progressive_user_lesson[$user_id][$course_id]['total_time'] += ($lesson_work_time - $progressive_user_lesson[$user_id][$course_id][$lesson_id]);
                            $progressive_user_lesson[$user_id][$course_id][$lesson_id] = $lesson_work_time;
                        }
                    }
                }
            }
        }

        $return_data             = array();
        foreach($progressive_user_lesson as $user_id => $users){
            $item                 = array();
            $item['userid']       = $return_users[$user_id]['user_id'];
            $item['username']     = $return_users[$user_id]['username'];
            $item['name']         = $return_users[$user_id]['user_fullname'];
            $item['cf']           = $return_users[$user_id]['user_fiscalcode'];
            $item['email']        = $return_users[$user_id]['user_email'];
            $item['data_nascita'] = $return_users[$user_id]['user_date_of_birth'];
            $item['citta']        = $return_users[$user_id]['user_city_of_birth'];
            $item['phone']        = $return_users[$user_id]['user_telephone'];
            foreach($users as $course_id => $courses){
                $item['corso']      = $return_courses[$course_id]['course_name'];
                $item['total_time'] = $this->tool->convertSecondsToTime($courses['total_time']);
                $return_data[] = $item;
            }
        }
        return $return_data;
    }
    public function getCoursesModules($user_filter = array(), $user_options = array()) {
        $connection = $this->connection;
        $database   = $this->database;
        //Definisco la collection
        $collection = $connection->$database->mdl_course_modules;

        //Definisco filtri
        $filter  = array();
        if(isset($user_filter['course_module_id'])) $filter['id'] = $user_filter['course_module_id'];

        //Definisco opzioni
        $options = array();

        //Eseguo la query
        $cursor = $collection->find($filter, $options);

        //Acquisisco i risultati
        $objects = array();
        foreach ($cursor as $item) {
            $objects[] = (array) $item;
        }

        //Restituisco il valore
        if(!empty($objects)) return $objects;
        return false;
    }
    public function getCourseModule($course_module_id){
        $course_module = $this->getCoursesModules(array('course_module_id' => $course_module_id));
        if($course_module) return array_shift($course_module);
        return false;
    }
    public function getUsersCourseModuleCompletitions($user_list, $course_module_list) {
        $connection = $this->connection;
        $database   = $this->database;
        //Definisco la collection
        $collection = $connection->$database->mdl_course_modules_completion;

        //Definisco filtri
        $filter = array('userid'         => array('$in' => $user_list)         ,
            'coursemoduleid' => array('$in' => $course_module_list) );

        //Eseguo la query
        $cursor = $collection->find($filter);

        //Acquisisco i risultati
        $objects = array();
        foreach ($cursor as $item) {
            $objects[] = (array) $item;
        }

        //Restituisco il valore
        if(!empty($objects)) return $objects;
        return false;
    }
    public function getVideoTimeLessons($user_filter = array(), $user_options = array()) {
        $connection = $this->connection;
        $database   = $this->database;
        //Definisco la collection
        $collection = $connection->$database->mdl_videotime;

        //Definisco filtri
        $filter  = array();
        if(isset($user_filter['videotime_lesson_id'])) $filter['id'] = $user_filter['videotime_lesson_id'];

        //Definisco opzioni
        $options = array();

        //Eseguo la query
        $cursor = $collection->find($filter, $options);

        //Acquisisco i risultati
        $objects = array();
        foreach ($cursor as $item) {
            $objects[] = (array) $item;
        }

        //Restituisco il valore
        if(!empty($objects)) return $objects;
        return false;
    }
    public function getVideoTimeLesson($videotime_lesson_id){
        $videotime_lesson = $this->getVideoTimeLessons(array('videotime_lesson_id' => $videotime_lesson_id));
        if($videotime_lesson) return array_shift($videotime_lesson);
        return false;
    }
    public function getVideoTimeSessions($user_filter = array(), $user_options = array()) {
        $connection = $this->connection;
        $database   = $this->database;
        //Definisco la collection
        $collection = $connection->$database->mdl_videotime_session;

        //Definisco filtri
        $filter  = array();
        if(isset($user_filter['start_from']))    $filter['timestarted']['$gt'] = $user_filter['start_from'];
        if(isset($user_filter['start_to']))      $filter['timestarted']['$lt'] = $user_filter['start_to'];
        if(isset($user_filter['company_users'])) $filter['user_id']['$in']     = $user_filter['company_users'];

        //Definisco opzioni
        $options = array();
        if(isset($user_options['sort'])){
            $sorting = $user_options['sorting'] ?? 1;
            $options[] = array('sort' => array($user_options['sort'] => $sorting));
        }
        //Eseguo la query
        $cursor = $collection->find($filter, $options);

        //Acquisisco i risultati
        $objects = array();
        foreach ($cursor as $item) {
            $objects[] = (array) $item;
        }

        //Restituisco il valore
        if(!empty($objects)) return $objects;
        return false;
    }
    public function _getVideoTimeReportData($data){
        $report                        = array();
        $user_data                     = array();
        $course_module_data            = array();
        $module_data                   = array();
        $course_data                   = array();
        $course_module_completion_data = array();
        foreach ($data as $record){
            if(!isset($user_data[$record['user_id']]))                                                              $user_data[$record['user_id']]                                                              = $this->getUser($record['user_id']);
            if(!isset($course_module_data[$record['module_id']]))                                                   $course_module_data[$record['module_id']]                                                   = $this->getCourseModule($record['module_id']);
            if(!isset($module_data[$course_module_data[$record['module_id']]['instance']]))                         $module_data[$course_module_data[$record['module_id']]['instance']]                         = $this->getVideoTimeLesson($course_module_data[$record['module_id']]['instance']);
            if(!isset($course_data[$module_data[$course_module_data[$record['module_id']]['instance']]['course']])) $course_data[$module_data[$course_module_data[$record['module_id']]['instance']]['course']] = $this->getCourse($module_data[$course_module_data[$record['module_id']]['instance']]['course']);
        }

        $user_list                = array_keys($user_data);
        $course_module_list       = array_keys($course_module_data);
        $course_module_completion = $this->getUsersCourseModuleCompletitions($user_list, $course_module_list);
        foreach ($course_module_completion as $item){
            if(!isset($course_module_completion_data[$item['userid']])) $course_module_completion_data[$item['userid']] = array();
            $course_module_completion_data[$item['userid']][$item['coursemoduleid']] = $item;
        }

        //PREPARTO I DATI
        foreach ($data as $record){
            $user          = $user_data[$record['user_id']];
            $course_module = $course_module_data[$record['module_id']];
            $module        = $module_data[$course_module['instance']];
            $course        = $course_data[$module['course']];

            $course_module_completion = $course_module_completion_data[$user['id']][$course_module['id']];
            $record['timefinished']   = $record['timestarted'] + $record['time'];

            $lesson_status = 'IN CORSO';
            if($course_module_completion['completionstate'] == 1 && $record['percent_watch'] == 1){
                $lesson_status = 'LEZIONE COMPLETATA';
            }
            if($record['timefinished'] > $course_module_completion['timemodified']){
                $record['percent_watch'] = 1;
                $lesson_status = 'COMPLETATA E VISUALIZZATA NUOVAMENTE';
            }

            $item = array();
            $item['fullname']      = $user['nome'];
            $item['username']      = $user['username'];
            $item['email']         = $user['email'];
            $item['fiscal_code']   = $user['idnumber'];
            $item['course_name']   = $course['fullname'];
            $item['lesson_name']   = $module['name'];
            $item['session_start'] = date('d/m/Y H:i:s', $record['timestarted']);
            $item['session_end']   = date('d/m/Y H:i:s', $record['timefinished']);
            $item['session_time']  = $this->tool->convertSecondsToTime($record['time']);
            $item['video_time']    = $this->tool->convertSecondsToTime($record['current_watch_time']);
            $item['percentage']    = floor($record['percent_watch']*100) ." %";
            $item['lesson_status'] = $lesson_status;
            $item['timefinished']  = $record['timefinished'];
            $item['timemodified']  = $course_module_completion['timemodified'];
            $item['user']          = $user;
            $report[] = $item;
        }
        return $report;
    }
    public function getVideoTimeReportData($data){
        $report_data                   = array();
        $user_data                     = array();
        $course_module_data            = array();
        $module_data                   = array();
        $course_data                   = array();
        $course_module_completion_data = array();
        foreach ($data as $record){
            if(!isset($user_data[$record['user_id']]))                                                              $user_data[$record['user_id']]                                                              = $this->getUser($record['user_id']);
            if(!isset($course_module_data[$record['module_id']]))                                                   $course_module_data[$record['module_id']]                                                   = $this->getCourseModule($record['module_id']);
            if(!isset($module_data[$course_module_data[$record['module_id']]['instance']]))                         $module_data[$course_module_data[$record['module_id']]['instance']]                         = $this->getVideoTimeLesson($course_module_data[$record['module_id']]['instance']);
            if(!isset($course_data[$module_data[$course_module_data[$record['module_id']]['instance']]['course']])) $course_data[$module_data[$course_module_data[$record['module_id']]['instance']]['course']] = $this->getCourse($module_data[$course_module_data[$record['module_id']]['instance']]['course']);
        }

        $user_list                = array_keys($user_data);
        $course_module_list       = array_keys($course_module_data);
        $course_module_completion = $this->getUsersCourseModuleCompletitions($user_list, $course_module_list);
        foreach ($course_module_completion as $item){
            if(!isset($course_module_completion_data[$item['userid']])) $course_module_completion_data[$item['userid']] = array();
            $course_module_completion_data[$item['userid']][$item['coursemoduleid']] = $item;
        }

        //PREPARTO I DATI
        $user_progressive_lesson = array();
        foreach ($data as $record){
            $user                     = $user_data[$record['user_id']];
            $course_module            = $course_module_data[$record['module_id']];
            $module                   = $module_data[$course_module['instance']];
            $course                   = $course_data[$module['course']];
            $course_module_completion = $course_module_completion_data[$user['id']][$course_module['id']];
            $record['timefinished']   = $record['timestarted'] + $record['time'];
            $lesson_status = 'IN CORSO';
            if($course_module_completion['completionstate'] == 1 && $record['percent_watch'] == 1){
                $lesson_status = 'LEZIONE COMPLETATA';
            }

            if($record['timestarted'] > $course_module_completion['timemodified']){
                $record['percent_watch']      = 1;
                $lesson_status                = 'COMPLETATA E VISUALIZZATA NUOVAMENTE';
                $record['current_watch_time'] = 0;
            }

            if(!isset($user_progressive_lesson[$record['user_id']]))                       $user_progressive_lesson[$record['user_id']]                       = array();
            if(!isset($user_progressive_lesson[$record['user_id']][$record['module_id']])) $user_progressive_lesson[$record['user_id']][$record['module_id']] = 0;
            if($record['current_watch_time'] > $user_progressive_lesson[$record['user_id']][$record['module_id']]){
                $increase_progression         = $record['current_watch_time'] - $user_progressive_lesson[$record['user_id']][$record['module_id']];
                $record['current_watch_time'] = $increase_progression;
                $user_progressive_lesson[$record['user_id']][$record['module_id']]+= $increase_progression;
            }else{
                $record['current_watch_time'] = 0;
            }

            $telephone = '';
            if(strlen(trim($user['phone2']))) $telephone = $user['phone2'];
            if(strlen(trim($user['phone1']))) $telephone = $user['phone1'];

            $item = array();
            $item['user_id']            = $user['userid'];
            $item['fullname']           = $user['nome'];
            $item['username']           = $user['username'];
            $item['email']              = $user['email'];
            $item['fiscal_code']        = $user['idnumber'];
            $item['city']               = $user['city'];
            $item['telephone']          = $telephone;
            $item['course_id']          = $course['id'];
            $item['course_name']        = $course['fullname'];
            $item['lesson_id']          = $module['id'];
            $item['lesson_name']        = $module['name'];
            $item['session_start']      = date('d/m/Y H:i:s', $record['timestarted']);
            $item['session_end']        = date('d/m/Y H:i:s', $record['timefinished']);
            $item['session_time']       = $this->tool->convertSecondsToTime($record['time']);
            $item['video_time']         = $this->tool->convertSecondsToTime($record['current_watch_time']);
            $item['percentage']         = floor($record['percent_watch']*100) ." %";
            $item['lesson_status']      = $lesson_status;
            $item['current_watch_time'] = $record['current_watch_time'];
            $item['timestarted']        = $record['timestarted'];
            $item['timefinished']       = $record['timefinished'];
            $item['timemodified']       = $course_module_completion['timemodified'];
            $report_data[] = $item;
        }

        $aggregation = array();
        foreach ($report_data as $item){
            $date_session = strtotime(date('m/d/Y 00:00:00', $item['timestarted']));
            if($date_session > 0){
                if(!isset($aggregation[$item['user_id']]))                                                                                          $aggregation[$item['user_id']]                                                                                          = array('data' => $item, 'courses'  => array());
                if(!isset($aggregation[$item['user_id']]['courses'][$item['course_id']]))                                                           $aggregation[$item['user_id']]['courses'][$item['course_id']]                                                           = array('data' => $item, 'lessons'  => array());
                if(!isset($aggregation[$item['user_id']]['courses'][$item['course_id']]['lessons'][$item['lesson_id']]))                            $aggregation[$item['user_id']]['courses'][$item['course_id']]['lessons'][$item['lesson_id']]                            = array('data' => $item, 'sessions' => array());
                if(!isset($aggregation[$item['user_id']]['courses'][$item['course_id']]['lessons'][$item['lesson_id']]['sessions'][$date_session])) $aggregation[$item['user_id']]['courses'][$item['course_id']]['lessons'][$item['lesson_id']]['sessions'][$date_session] = 0;
                $aggregation[$item['user_id']]['courses'][$item['course_id']]['lessons'][$item['lesson_id']]['sessions'][$date_session] += $item['current_watch_time'];
            }else{
                $item['date_session'] = $date_session;
                $item['time_session'] = date('m/d/Y 00:00:00', $item['timestarted']);
                if(!isset($report['errors'])) $aggregation['errors'] = array();
                $aggregation['errors'][] = $item;
            }
        }
        //GIORNI CON VALORI
        $array_session_dates = array();
        foreach($aggregation as $user_id => $user){
            foreach($user['courses'] as $course_id => $course){
                foreach($course['lessons'] as $lesson_id => $lesson) {
                    foreach ($lesson['sessions'] as $date => $session_time) {
                        if (!in_array($date, $array_session_dates)) $array_session_dates[$date] = date('d/m/Y', $date);
                    }
                }
            }
        }
        ksort($array_session_dates);

        //VALORIZZAZIONE RECORD
        $array_session_values = array();
        foreach($aggregation as $user_id => $user_courses){
            foreach($user_courses['courses'] as $user_course){
                $item = array();
                $item['user_id']            = $user_courses['data']['user_id'];
                $item['fullname']           = $user_courses['data']['fullname'];
                $item['username']           = $user_courses['data']['username'];
                $item['email']              = $user_courses['data']['email'];
                $item['fiscal_code']        = $user_courses['data']['fiscal_code'];
                $item['city']               = $user_courses['data']['city'];
                $item['telephone']          = $user_courses['data']['telephone'];
                $item['date_of_birth']      = '';
                $item['company_name']       = $data['company_name'];
                $item['course_id']          = $user_course['data']['course_id'];
                $item['course_name']        = $user_course['data']['course_name'];
                $item['total_time']         = 0;
                foreach($user_course['lessons'] as $user_lessons){
                    for($date_iterator = 0; $date_iterator < count($array_session_dates); $date_iterator++){
                        $keys = array_keys($array_session_dates);
                        if(!isset($user_lessons['sessions'][$keys[$date_iterator]])) $user_lessons['sessions'][$keys[$date_iterator]] = 0;
                        if(!isset($item[$keys[$date_iterator]]))                     $item[$keys[$date_iterator]]                     = 0;

                        $item[$keys[$date_iterator]] += $user_lessons['sessions'][$keys[$date_iterator]];
                        $item['total_time']          += $user_lessons['sessions'][$keys[$date_iterator]];
                    }
                }
                for($date_iterator = 0; $date_iterator < count($array_session_dates); $date_iterator++){
                    $keys = array_keys($array_session_dates);
                    $item[$keys[$date_iterator]] = $this->tool->convertSecondsToTime($item[$keys[$date_iterator]]);
                }
                $item['total_time']         = $this->tool->convertSecondsToTime($item['total_time']);
                $array_session_values[]     = $item;
            }
        }
        $report = array('array_session_dates'  => $array_session_dates ,
                        'array_session_values' => $array_session_values,
                        'aggregation'          => $aggregation         ,
                        'report_data'          => $report_data          );
        return $report;
    }
    public function insertReportLog($data){
        $connection = $this->connection;
        $database   = $this->database;

        if(!empty($data)){
            $collection = $connection->$database->skilledin_report;

            //Eseguo l'inserimento
            $filter = ['log_id' => $data['log_id']];
            $result = $collection->updateOne($filter, array('$set' => $data), array('upsert' => true));
            return $result;
        }
        return false;
    }
    public function getLastReportLog($user_filter = array(), $user_options = array()){
        $connection = $this->connection;
        $database   = $this->database;
        $collection = $connection->$database->skilledin_report;

        //Definisco filtri
        $filter  = array();
        if(isset($user_filter['log_id']))    $filter['log_id']['$lt'] = $user_filter['log_id'];
        if(isset($user_filter['user_id']))   $filter['user_id']       = $user_filter['user_id'];
        if(isset($user_filter['course_id'])) $filter['course_id']     = $user_filter['course_id'];
        if(isset($user_filter['object_id'])) $filter['object_id']     = $user_filter['object_id'];

        //Definisco opzioni
        $options = ['sort' => ['date' => -1], 'limit' => 1];

        //Eseguo la query
        $cursor = $collection->find($filter, $options);
        //Acquisisco i risultati
        $objects = array();
        foreach ($cursor as $item) {
            $objects[] = (array) $item;
        }

        //Restituisco il valore
        if(!empty($objects)) return array_shift($objects);
        return false;
    }
    public function getLastDateLog($user_filter = array(), $user_options = array()){
        $connection = $this->connection;
        $database   = $this->database;
        $collection = $connection->$database->skilledin_report;

        //Definisco filtri
        $filter  = array();
        if(isset($user_filter['platform'])) $filter['platform'] = $user_filter['platform'];

        //Definisco opzioni
        $options = ['sort' => ['timecreated' => -1], 'limit' => 1, 'projection' => ['date' => 1]];

        //Eseguo la query
        $cursor = $collection->find($filter, $options);

        //Acquisisco i risultati
        foreach ($cursor as $item) {
            return $item['date'];
        }
        return false;
    }
    public function getCourseDuration($user_filter = array(), $user_options = array()) {
        $connection = $this->connection;
        $database   = $this->database;

        //Definisco la collection
        $collection = $connection->$database->mdl_course_duration;

        //Definisco filtri
        $filter  = array();
        if(isset($user_filter['course_id'])) $filter['course_id']['$in'] = $user_filter['course_id'];

        //Definisco opzioni
        $options = array();

        //Eseguo la query
        $cursor = $collection->find($filter, $options);

        //Acquisisco i risultati
        $objects = array();
        foreach ($cursor as $item) {
            $objects[] = (array) $item;
        }

        //Restituisco il valore
        if(!empty($objects)) return $objects;
        return false;
    }


    public function getBBBObjects($user_filter = array(), $user_options = array()) {
        $connection = $this->connection;
        $database   = $this->database;

        //Definisco la collection
        $collection = $connection->$database->mdl_bigbluebuttonbn;
        //Definisco filtri
        $filter  = array();
        if(isset($user_filter['objectid'])) $filter['id'] = $user_filter['objectid'];

        //Definisco opzioni
        $options = array();

        //Eseguo la query
        $cursor = $collection->find($filter, $options);

        //Acquisisco i risultati
        $objects = array();
        foreach ($cursor as $item) {
            $objects[] = (array) $item;
        }

        //Restituisco il valore
        if(!empty($objects)) return $objects;
        return false;
    }
    public function constructBbbArray($array, $bbbName, $nameCorsi) {
        $result = array();

        foreach ($array as $singolo) {
            $tmp = array();
            if (!isset($singolo['nome'])) continue;

            if(isset($bbbName[$singolo['objectid']])){
                $tmp['aula'] = $bbbName[$singolo['objectid']];
            }else{
                //continue;
                $tmp['aula'] = '';
            }

            if (isset($nameCorsi[$singolo['courseid']])){
                $tmp['corso'] = $nameCorsi[$singolo['courseid']];
            }else{
                //continue;
                $tmp['corso'] = '';
            }


            $tmp['id'] = $singolo['id'];
            $tmp['userid'] = $singolo['userid'];

            $name = $singolo['nome'];
            $tmp['name'] = $name;
            $tmp['username'] = $singolo['username'];
            $tmp['email'] = $singolo['email'];

            $tmp['phone'] = "n/d";
            if (isset($singolo['phone1']) && $singolo['phone1'] != "")
                $tmp['phone'] = $singolo['phone1'];
            if (isset($singolo['phone1']) && $singolo['phone2'] != "")
                $tmp['phone'] = $singolo['phone2'];
            if (isset($singolo['cf']))
                $tmp['cf'] = $singolo['cf'];
            else
                $tmp['cf'] = "n/d";

            if (!isset($singolo['data']))
                $tmp['data_nascita'] = "n/d";
            else
                $tmp['data_nascita'] = date("d/m/Y", $singolo['data']);
            if (!isset($singolo['citta']))
                $tmp['citta'] = "n/d";
            else
                $tmp['citta'] = $singolo['citta'];
            $time = date("d/m/Y", $singolo['timecreated']);
            $tmp['timecreated'] = $time;
            $orario = date("H:i:s", $singolo['timecreated']);
            $tmp['timestamp'] = $singolo['timecreated'];
            $tmp['orario'] = $orario;
            $tmp['ip'] = $singolo['ip'];
            $tmp['action'] = $singolo['action'];
            $tmp['objectid'] = $singolo['objectid'];
            $tmp['courseid'] = $singolo['courseid'];
            if (isset($singolo['azienda']))
                $tmp['azienda'] = $singolo['azienda'];
            else
                $tmp['azienda'] = "n/d";
            $ruolo = "n/d";
            switch ($singolo['ruolo']) {
                case 'student':
                    $ruolo = "studente";
                    break;
                case 'manager':
                    $ruolo = "manager";
                    break;
                case 'editingteacher':
                    $ruolo = "docente";
                    break;
                case 'teacher':
                    $ruolo = "controller";
                    break;
                default:break;
            }
            $tmp['ruolo'] = $ruolo;
            array_push($result, $tmp);
        }
        return $result;
    }

    public function constructSintesiReport($array) {
        $array = $this->sortArray($array);


        $result = array();
        $prov = null;
        $primo = array();
        $prec = array(); //per risolvere viewed dashboard
        $contatore = 0; //per risolvere duplicati

        foreach ($array as $singolo) {
            if ($prov == null) {
                $prov = $singolo;
                $primo = $prov;
                $prec = $prov;
                $contatore++;
                continue;
            }
            $contatore++;
            $data_prov    = str_replace("/", "", $prov['timecreated']);
            $data_singolo = str_replace("/", "", $singolo['timecreated']);
            if ($prov['userid'] == $singolo['userid']) {
                if ($data_prov == $data_singolo) {
                    if ($singolo['action'] == 'loggedin' || $singolo['action'] == 'joined') {

                        if ($singolo['action'] == 'joined' && $prov['action'] == 'joined') {

                            $differenza = $singolo['timestamp'] - $prov['timestamp'];
                            if ($differenza < 3600) {
                                if ($prec['action'] == 'left') {
                                    $primo['orario_logout'] = $prec['orario'];
                                    $primo['id_logout'] = $prec['id'];



                                    $tempo_cumulato = $prec['timestamp'] - $primo['timestamp'];
                                    $tempo_cumulato = $this->tool->convertTimeToSeconds($tempo_cumulato);
                                    $primo['tempo_cumulato'] = $tempo_cumulato;
                                    array_push($result, $primo);
                                    $primo = $singolo;
                                    $prov = $singolo;
                                    $prec = $singolo;
                                }
                                continue;
                            }
                        }

                        if ($singolo['action'] == "joined") {
                            if ($prec['action'] == "left") {
                                $primo['orario_logout'] = $prec['orario'];
                                $primo['id_logout'] = $prec['id'];
                                $tempo_cumulato = $prec['timestamp'] - $primo['timestamp'];
                                $tempo_cumulato = $this->tool->convertTimeToSeconds($tempo_cumulato);
                                $primo['tempo_cumulato'] = $tempo_cumulato;
                                array_push($result, $primo);
                                $primo = $singolo;
                                $prov = $singolo;
                                $prec = $singolo;
                                continue;
                            }
                        }

                        if ($singolo['timestamp'] == $prov['timestamp']) {
                            if ($prec == $prov) {
                                $primo = $singolo;
                                $prov = $singolo;
                                $prec = $prov;
                                continue;
                            } else {
                                $primo['orario_logout'] = $prec['orario'];
                                $primo['id_logout'] = $prec['id'];
                                $tempo_cumulato = $prec['timestamp'] - $primo['timestamp'];
                                $tempo_cumulato = $this->tool->convertTimeToSeconds($tempo_cumulato);
                                $primo['tempo_cumulato'] = $tempo_cumulato;
                                array_push($result, $primo);
                            }
                        } else {
                            if ($primo == $prov) continue;
                            $primo['orario_logout'] = $prov['orario'];
                            $primo['id_logout'] = $prov['id'];
                            $tempo_cumulato = $prov['timestamp'] - $primo['timestamp'];
                            $tempo_cumulato = $this->tool->convertTimeToSeconds($tempo_cumulato);
                            $primo['tempo_cumulato'] = $tempo_cumulato;

                            array_push($result, $primo);
                        }


                        $prov = $singolo;
                        $primo = $singolo;
                        $prec = $prov;
                    } else if ($singolo['action'] == 'loggedout' || $singolo['action'] == 'left') {

                        if ($singolo['action'] == "left") {
                            $prec = $singolo;
                            $prov = $singolo;
                        } else {

                            //qui entra
                            $primo['orario_logout'] = $singolo['orario'];
                            $primo['id_logout'] = $singolo['id'];
                            $tempo_cumulato = $singolo['timestamp'] - $primo['timestamp'];
                            $tempo_cumulato = $this->tool->convertTimeToSeconds($tempo_cumulato);
                            $primo['tempo_cumulato'] = $tempo_cumulato;
                            array_push($result, $primo);
                            $prov = null;
                            continue;
                        }
                    } else if ($singolo['action'] != 'loggedout' || $singolo['action'] != 'left') {
                        $prec = $prov;
                        $prov = $singolo;
                    }
                } else {
                    $midnight = $this->getMidnight($singolo['timecreated']);
                    $fourHours = $midnight + 60 * 60 * 6;
                    if ($singolo['timestamp'] >= $midnight && $singolo['timestamp'] <= $fourHours && $singolo['action'] != 'loggedin') {
                        $primo['orario_logout'] = "23:59:59";
                        $primo['id_logout'] = $primo['id'];

                        $beforeMidnight = $this->getTime($primo['timecreated'], true);
                        $tempo_cumulato = $beforeMidnight - $primo['timestamp'];
                        $tempo_cumulato = $this->tool->convertTimeToSeconds($tempo_cumulato);
                        $primo['tempo_cumulato'] = $tempo_cumulato;
                        array_push($result, $primo);

                        $primo = $singolo;
                        $primo['orario'] = "00:00:00";
                        $afterMidnight = $this->getTime($singolo['timecreated'], null, true);
                        $primo['timestamp'] = $afterMidnight;
                        $prec = $prov;
                        $prov = $singolo;
                    } else {
                        if ($prec['action'] == "left") {
                            $primo['orario_logout'] = $prec['orario'];
                            $primo['id_logout'] = $prec['id'];
                            $tempo_cumulato = $prec['timestamp'] - $primo['timestamp'];
                            $tempo_cumulato = $this->tool->convertTimeToSeconds($tempo_cumulato);
                            $primo['tempo_cumulato'] = $tempo_cumulato;
                            array_push($result, $primo);
                        } else {
                            $primo['orario_logout'] = $prov['orario'];
                            $primo['id_logout'] = $prov['id'];
                            $tempo_cumulato_oo = $prov['timestamp'] - $primo['timestamp'];
                            $tempo_cumulato = $this->tool->convertTimeToSeconds($tempo_cumulato_oo);
                            $primo['tempo_cumulato'] = $tempo_cumulato;

                            array_push($result, $primo);
                        }
                        $prov = $singolo;
                        $primo = $singolo;
                        $prec = $prov;
                    }
                }
                if ($contatore == count($array)) {
                    if ($prec['action'] == "left") {
                        $primo['orario_logout'] = $prec['orario'];
                        $primo['id_logout'] = $prec['id'];
                        $tempo_cumulato = $prec['timestamp'] - $primo['timestamp'];
                        $tempo_cumulato = $this->tool->convertTimeToSeconds($tempo_cumulato);
                        $primo['tempo_cumulato'] = $tempo_cumulato;
                        array_push($result, $primo);
                    } else {
                        $primo['orario_logout'] = $prov['orario'];
                        $primo['id_logout'] = $prov['id'];
                        $tempo_cumulato = $prov['timestamp'] - $primo['timestamp'];
                        $tempo_cumulato = $this->tool->convertTimeToSeconds($tempo_cumulato);
                        $primo['tempo_cumulato'] = $tempo_cumulato;
                        array_push($result, $primo);
                    }
                }
            } else {
                if ($prec['action'] == "left") {
                    $primo['orario_logout'] = $prec['orario'];
                    $primo['id_logout'] = $prec['id'];
                    $tempo_cumulato = $prec['timestamp'] - $primo['timestamp'];
                    $tempo_cumulato = $this->tool->convertTimeToSeconds($tempo_cumulato);
                    $primo['tempo_cumulato'] = $tempo_cumulato;
                    array_push($result, $primo);
                } else {
                    $primo['orario_logout'] = $prov['orario'];
                    $primo['id_logout'] = $prov['id'];
                    $tempo_cumulato = $prov['timestamp'] - $primo['timestamp'];
                    $tempo_cumulato = $this->tool->convertTimeToSeconds($tempo_cumulato);
                    $primo['tempo_cumulato'] = $tempo_cumulato;
                    array_push($result, $primo);
                }
                $prov = $singolo;
                $primo = $singolo;
                $prec = $prov;
            }
        }
        return $result;
    }

    public function sortArray($array) {
        uasort($array, function ($first, $second) {
            if ($first['userid'] == $second['userid']) {
                return $first['timestamp'] - $second['timestamp'];
            } else {
                return $first['userid'] <= $second['userid'];
            }
        });
        return $array;
    }
    public function getMidnight($data) {
        $date = date_create_from_format("d/m/Y", $data);
        $date = date_format($date, 'Y-m-d');
        $midnight = strtotime($date);
        return $midnight;
    }

    public function constructMinCumulatiReport($array, $totResult) {
        $array      = $this->sortArray($array);
        $prov       = null;
        $mesi       = array();
        $new        =  array();
        $array_mesi = array();
        $cumulatoTot = 0;
        $tempo_tot = 0;
        $cont = 0;



        foreach ($array as $singolo) {
            if ($prov == null && $totResult > 1) {
                echo 'jump<br>';

                $prov = $singolo;
                $cont++;
                continue;
            }

            if ($totResult == 1) {
                $data_singolo = str_replace("/", "", $singolo['timecreated']);
                $timesingolo_split = explode(":", $singolo['tempo_cumulato']);
                $seconds_singolo = $timesingolo_split[0] * 3600 + $timesingolo_split[1] * 60 + $timesingolo_split[2];
                $mesi[$singolo['timecreated']] = $singolo['tempo_cumulato'];
                if (!in_array($singolo['timecreated'], $array_mesi))
                    array_push($array_mesi, $singolo['timecreated']);
                $tempo = explode(":", $singolo['tempo_cumulato']);
                $secondi = $tempo[0] * 3600 + $tempo[1] * 60 + $tempo[2];
                $tempo_tot = $secondi;
                $singolo['mesi'] = $mesi;
                $singolo['tempo_tot'] = $this->tool->convertTimeToSeconds($tempo_tot);
                array_push($new, $singolo);
                break;
            }

            $cont++;
            $data_prov = str_replace("/", "", $prov['timecreated']);
            $data_singolo = str_replace("/", "", $singolo['timecreated']);
            $timeprov_split = explode(":", $prov['tempo_cumulato']);
            $timesingolo_split = explode(":", $singolo['tempo_cumulato']);
            $seconds_prov = $timeprov_split[0] * 3600 + $timeprov_split[1] * 60 + $timeprov_split[2];
            $seconds_singolo = $timesingolo_split[0] * 3600 + $timesingolo_split[1] * 60 + $timesingolo_split[2];
            if ($prov['userid'] == $singolo['userid']) {
                if ($data_prov == $data_singolo) {

                    $cumulatoTot = $seconds_prov + $seconds_singolo;
                    if (!in_array($prov['timecreated'], $array_mesi))
                        array_push($array_mesi, $prov['timecreated']);

                    $mesi[$prov['timecreated']] = $this->tool->convertTimeToSeconds($cumulatoTot);

                    $prov = $singolo;
                    $prov['mesi'] = $mesi;
                    $prov['tempo_cumulato'] = $this->tool->convertTimeToSeconds($cumulatoTot);
                } else {

                    $mesi[$prov['timecreated']] = $prov['tempo_cumulato'];
                    $mesi[$singolo['timecreated']] = $singolo['tempo_cumulato'];
                    if (!in_array($prov['timecreated'], $array_mesi))    array_push($array_mesi, $prov['timecreated']);
                    if (!in_array($singolo['timecreated'], $array_mesi)) array_push($array_mesi, $singolo['timecreated']);

                    $prov = $singolo;
                    $prov['mesi'] = $mesi;
                }
            } else {
                if (isset($prov['mesi'])) {
                    foreach ($prov['mesi'] as $tempo) {
                        $tempo = explode(":", $tempo);
                        $secondi = $tempo[0] * 3600 + $tempo[1] * 60 + $tempo[2];
                        $tempo_tot += $secondi;
                    }
                } else {
                    $mesi[$prov['timecreated']] = $prov['tempo_cumulato'];
                    $prov['mesi'] = $mesi;
                    if (!in_array($prov['timecreated'], $array_mesi))
                        array_push($array_mesi, $prov['timecreated']);
                    $tempo = explode(":", $prov['tempo_cumulato']);
                    $secondi = $tempo[0] * 3600 + $tempo[1] * 60 + $tempo[2];
                    $tempo_tot = $secondi;
                }

                $prov['tempo_tot'] = $this->tool->convertTimeToSeconds($tempo_tot);
                array_push($new, $prov);
                $tempo_tot = 0;
                $prov = $singolo;
                $mesi = null;
            }

            if ($cont == $totResult) {
                if (isset($prov['mesi'])) {
                    foreach ($prov['mesi'] as $tempo) {
                        $tempo = explode(":", $tempo);
                        $secondi = $tempo[0] * 3600 + $tempo[1] * 60 + $tempo[2];
                        $tempo_tot += $secondi;
                    }
                } else {
                    $mesi[$prov['timecreated']] = $prov['tempo_cumulato'];
                    $prov['mesi'] = $mesi;
                    if (!in_array($prov['timecreated'], $array_mesi))
                        array_push($array_mesi, $prov['timecreated']);
                    $tempo = explode(":", $prov['tempo_cumulato']);
                    $secondi = $tempo[0] * 3600 + $tempo[1] * 60 + $tempo[2];
                    $tempo_tot = $secondi;
                }
                $prov['tempo_tot'] = $this->tool->convertTimeToSeconds($tempo_tot);
                array_push($new, $prov);
            }
        }
        usort($array_mesi, "compareByTimeStamp");

        foreach ($new as &$singolo) {
            foreach ($array_mesi as $mese) {
                if (!in_array($mese, array_keys($singolo['mesi']))) {
                    $singolo['mesi'][$mese] = "00:00:00";
                }
            }
            uksort($singolo['mesi'], "compareByTimeStamp");
        }
        return $new;
    }

    public function LogPrint($log, $color = "#000000"){
        if (PHP_SAPI === 'cli'){
            echo date('Y-m-d H:i:s') . " - " . $log . "\n";
        }else{
            echo '<p style="color:' . $color . '" class="log_print_data"><span style="font-weight:bold;">' . date('Y-m-d H:i:s').'</span> - ' . $log . '</p>';
        }


        flush();
    }
}
