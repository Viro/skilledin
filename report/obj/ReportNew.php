<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/skilledin/report/config/connection/MongoDB/skilledin.php');
//function compareByTimeStamp($time1, $time2) {
//    $time1 = str_replace("/", "-", $time1);
//    $time2 = str_replace("/", "-", $time2);
//
//    return strtotime($time1) - strtotime($time2);
//}

class ReportNew{
    private $connection = null;
    private $database   = null;
    private $table      = null;

    public function __construct(){
        $connection_string  = "mongodb+srv://";
        $connection_string .= conn_username . ":" . urlencode(conn_password) . "@" . conn_server;
        $connection_string .= "?retryWrites=true&w=majority";
        $this->connection = new MongoDB\Client($connection_string);
    }

    public function setDatabase($database){
        $this->database = $database;
    }
    public function setTable($table){
        $this->table = $table;
    }

    public function getUsers($filter = array(), $options = array()) {
        $connection = $this->connection;
        $database   = $this->database;
        $table      = $this->table;

        //Definisco la tablla
        $collection = $connection->$database->$table;

        //Definisco filtri
        $filter  = array();

        //Definisco opizioni
        $options = array();

        //Eseguo la query
        $cursor = $collection->find($filter, $options);

        //Acquisisco i risultati
        $objects = array();
        foreach ($cursor as $item) {
            $objects[] = (array) $item;
        }

        //Restituisco il valore
        if(!empty($objects)) return $objects;
        return false;
    }

}