<?php

//require_once($_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php');

//function compareByTimeStamp($time1, $time2) {
//    $time1 = str_replace("/", "-", $time1);
//    $time2 = str_replace("/", "-", $time2);
//
//    return strtotime($time1) - strtotime($time2);
//}

class SkilledinWeb{
    public $connection = null;

    public function __construct($platform = ''){


        if($platform == 'f40')    require_once($_SERVER['DOCUMENT_ROOT'] . '/skilledin/report/config/connection/MySQL/formazione40.php');
        if($platform == 'fnc')    require_once($_SERVER['DOCUMENT_ROOT'] . '/skilledin/report/config/connection/MySQL/fondonuovecompetenze.php');
        if($platform == 'live')   require_once($_SERVER['DOCUMENT_ROOT'] . '/skilledin/report/config/connection/MySQL/live.php');
        if($platform == 'formaz') require_once($_SERVER['DOCUMENT_ROOT'] . '/skilledin/report/config/connection/MySQL/formazione.php');
        if($platform == 'new')    require_once($_SERVER['DOCUMENT_ROOT'] . '/skilledin/report/config/connection/MySQL/new.php');
        $this->connection = new mysqli(mysql_server, mysql_username, mysql_password, mysql_db);
        if ($this->connection->connect_error) {
            die("Errore di connessione: " . $this->connection->connect_error);
        }
    }


    public function insertManualLog($data = array()){
        $new = array();
        //DATA FORMAT
        if(isset($data['eventname']))         $new['eventname']         = "'" . $this->connection->real_escape_string($data['eventname']) . "'";
        if(isset($data['component'])) 	      $new['component']  	    = "'" . $data['component'] . "'";
        if(isset($data['action'])) 	          $new['action']  	        = "'" . $data['action'] . "'";
        if(isset($data['target'])) 	          $new['target']  	        = "'" . $data['target'] . "'";
        if(isset($data['objecttable'])) 	  $new['objecttable']  	    = "'" . $data['objecttable'] . "'";
        if(isset($data['objectid'])) 	      $new['objectid']  	    = (int) $data['objectid'];
        if(isset($data['crud'])) 	          $new['crud']  	        = "'" . $data['crud'] . "'";
        if(isset($data['edulevel'])) 	      $new['edulevel']  	    = (int) $data['edulevel'];
        if(isset($data['contextid'])) 	      $new['contextid']  	    = (int) $data['contextid'];
        if(isset($data['contextlevel'])) 	  $new['contextlevel']  	= (int) $data['contextlevel'];
        if(isset($data['contextinstanceid'])) $new['contextinstanceid'] = (int) $data['contextinstanceid'];
        if(isset($data['userid'])) 	          $new['userid']  	        = (int) $data['userid'];
        if(isset($data['courseid'])) 	      $new['courseid']  	    = (int) $data['courseid'];
        if(isset($data['relateduserid'])) 	  $new['relateduserid']  	= (int) $data['relateduserid'];
        if(isset($data['anonymous'])) 	      $new['anonymous']  	    = (int) $data['anonymous'];
        if(isset($data['other'])) 	          $new['other']  	        = "'" . $data['other'] . "'";
        if(isset($data['timecreated'])) 	  $new['timecreated']  	    = (int) $data['timecreated'];
        if(isset($data['origin'])) 	          $new['origin']  	        = "'" . $data['origin'] . "'";
        if(isset($data['ip'])) 	              $new['ip']  	            = "'" . $data['ip'] . "'";
        if(isset($data['realuserid'])) 	      $new['realuserid']  	    = (int) $data['realuserid'];

        //DATA CHECK
        $new['eventname']         = (isset($new['eventname']))         ? $new['eventname']         : "''";
        $new['component']         = (isset($new['component']))         ? $new['component']         : "''";
        $new['action']            = (isset($new['action']))            ? $new['action']            : "''";
        $new['target']            = (isset($new['target']))            ? $new['target']            : "''";
        $new['objecttable']       = (isset($new['objecttable']))       ? $new['objecttable']       : "''";
        $new['objectid'] 	      = (isset($new['objectid']))          ? $new['objectid']          : 0;
        $new['crud']              = (isset($new['crud']))              ? $new['crud']              : "''";
        $new['edulevel'] 	      = (isset($new['edulevel']))          ? $new['edulevel']          : 0;
        $new['contextid'] 	      = (isset($new['contextid']))         ? $new['contextid']         : 0;
        $new['contextlevel'] 	  = (isset($new['contextlevel']))      ? $new['contextlevel']      : 0;
        $new['contextinstanceid'] = (isset($new['contextinstanceid'])) ? $new['contextinstanceid'] : 0;
        $new['userid'] 	          = (isset($new['userid']))            ? $new['userid']            : 0;
        $new['courseid'] 	      = (isset($new['courseid']))          ? $new['courseid']          : 0;
        $new['relateduserid'] 	  = (isset($new['relateduserid']))     ? $new['relateduserid']     : "NULL";
        $new['anonymous'] 	      = (isset($new['anonymous']))         ? $new['anonymous']         : 0;
        $new['other']             = (isset($new['other']))             ? $new['other']             : "''";
        $new['timecreated'] 	  = (isset($new['timecreated']))       ? $new['timecreated']       : 0;
        $new['origin']            = (isset($new['origin']))            ? $new['origin']            : "''";
        $new['ip']                = (isset($new['ip']))                ? $new['ip']                : "''";
        $new['realuserid'] 	      = (isset($new['realuserid']))        ? $new['realuserid']        : "NULL";

        //INSERT
        $sql = "INSERT INTO `mdl_logstore_standard_log` 
				    SET `eventname`         = " . $new['eventname']         . ",
					    `component`         = " . $new['component'] 	 	. ",
						`action`            = " . $new['action']            . ",
						`target`  		    = " . $new['target']    	 	. ",
						`objecttable` 	  	= " . $new['objecttable']   	. ",
						`objectid` 			= " . $new['objectid']	 		. ",
						`crud` 		        = " . $new['crud'] 			    . ",
						`edulevel` 		    = " . $new['edulevel'] 			. ",
						`contextid` 		= " . $new['contextid'] 		. ",
						`contextlevel` 		= " . $new['contextlevel'] 		. ",
						`contextinstanceid` = " . $new['contextinstanceid'] . ",
						`userid` 		    = " . $new['userid'] 			. ",
						`courseid` 		    = " . $new['courseid'] 			. ",
						`relateduserid` 	= " . $new['relateduserid'] 	. ",
						`anonymous` 		= " . $new['anonymous'] 		. ",
						`other` 		    = " . $new['other'] 			. ",
						`timecreated` 		= " . $new['timecreated'] 		. ",
						`origin` 		    = " . $new['origin'] 			. ",
						`ip` 		        = " . $new['ip'] 			    . ",
						`realuserid` 		= " . $new['realuserid']    	. " ";

        $this->connection->query($sql);
        return $this->connection->insert_id;
    }

    public function getHVPContentUserDataByObject($user_id, $object_id){
        $sql   = "SELECT * FROM `mdl_hvp_content_user_data` WHERE `user_id` = $user_id AND `hvp_id` = $object_id";
        $query = $this->connection->query($sql);
        if($query->num_rows){
            $items = array();
            while($item = $query->fetch_assoc()){
                $items[] = $item;
            }
            return $items;
        }
        return false;
    }

    public function editHVPContentUserData($hvp_content_user_data_id, $data = array()){
        $set = array();
        if(isset($data['data'])) $set['data'] = "`data` = '" .  $data['data'] . "'";
        if(!empty($set)){
            $sql = "UPDATE `mdl_hvp_content_user_data` SET " . implode(", ", $set) . " WHERE `id` = $hvp_content_user_data_id";
            $this->connection->query($sql);
        }else{
            echo 'Nessun dato da aggiornare';
        }
    }

    public function getCourseModuleByCourseIstance($course_id, $object_id){
        $sql   = "SELECT * FROM `mdl_course_modules` WHERE `course` = $course_id AND `instance` = $object_id";
        $query = $this->connection->query($sql);
        if($query->num_rows){
            $items = array();
            while($item = $query->fetch_assoc()){
                $items[] = $item;
            }
            return $items;
        }
        return false;
    }

    public function getCourseModuleCompletionByUserCourseModule($user_id, $course_module_id){
        $sql = "SELECT * FROM `mdl_course_modules_completion` WHERE `userid` = $user_id AND `coursemoduleid` = $course_module_id";
        $query = $this->connection->query($sql);
        if($query->num_rows){
            $items = array();
            while($item = $query->fetch_assoc()){
                $items[] = $item;
            }
            return $items;
        }
        return false;
    }
    public function editCourseModuleCompletion($course_module_completion_id, $data = array()){
        $set = array();
        if(isset($data['completionstate'])) $set['completionstate'] = "`completionstate` = " .  (int) $data['completionstate'] . " ";
        if(isset($data['viewed']))          $set['viewed']          = "`viewed`          = " .  (int) $data['viewed']          . " ";
        if(!empty($set)){
            $sql = "UPDATE `mdl_course_modules_completion` SET " . implode(", ", $set) . " WHERE `id` = $course_module_completion_id";
            $this->connection->query($sql);
        }else{
            echo 'Nessun dato da aggiornare';
        }
    }

    public function getCourseModule($course_module_id){
        $query = $this->connection->query("SELECT * FROM `mdl_hvp` WHERE `id` = $course_module_id");
        if($query->num_rows) return $query->fetch_assoc();
        return false;
    }

    public function getCourse($course_id){
        $query = $this->connection->query("SELECT * FROM `mdl_course` WHERE `id` = $course_id");
        if($query->num_rows) return $query->fetch_assoc();
        return false;
    }

    public function addAziendePianiFormativi($data = array()){
        $new = array();
        //DATA FORMAT
        if(isset($data['skilledin_plan_id'])) 	      $new['skilledin_plan_id']  	    = "'" . $data['skilledin_plan_id']                                   . "'";
        if(isset($data['nome_azienda']))              $new['nome_azienda']              = "'" . $this->connection->real_escape_string($data['nome_azienda']) . "'";
        if(isset($data['nome_piano']))                $new['nome_piano']                = "'" . $this->connection->real_escape_string($data['nome_piano'])   . "'";
        if(isset($data['importo_massimo'])) 	      $new['importo_massimo']  	        = "'" . $data['importo_massimo']                                     . "'";
        if(isset($data['importo_finanziato'])) 	      $new['importo_finanziato']  	    = "'" . $data['importo_finanziato']                                  . "'";
        if(isset($data['durata_piano_in_ore'])) 	  $new['durata_piano_in_ore']  	    = (int) $data['durata_piano_in_ore'];
        if(isset($data['durata_piano_in_giorni'])) 	  $new['durata_piano_in_giorni']    = (int) $data['durata_piano_in_giorni'];
        if(isset($data['durata_piano_in_settimane'])) $new['durata_piano_in_settimane'] = (int) $data['durata_piano_in_settimane'];
        if(isset($data['data_inizio_piano'])) 	      $new['data_inizio_piano']  	    = "'" . $data['data_inizio_piano']                                   . "'";
        if(isset($data['data_fine_piano'])) 	      $new['data_fine_piano']  	        = "'" . $data['data_fine_piano']                                     . "'";

        //DATA CHECK
        $new['skilledin_plan_id']         = (isset($new['skilledin_plan_id']))         ? $new['skilledin_plan_id']         : "NULL";
        $new['nome_azienda']              = (isset($new['nome_azienda']))              ? $new['nome_azienda']              : "''";
        $new['nome_piano']                = (isset($new['nome_piano']))                ? $new['nome_piano']                : "''";
        $new['importo_massimo'] 	      = (isset($new['importo_massimo']))           ? $new['importo_massimo']           : 0;
        $new['importo_finanziato'] 	      = (isset($new['importo_finanziato']))        ? $new['importo_finanziato']        : 0;
        $new['durata_piano_in_ore'] 	  = (isset($new['durata_piano_in_ore']))       ? $new['durata_piano_in_ore']       : 0;
        $new['durata_piano_in_giorni']    = (isset($new['durata_piano_in_giorni']))    ? $new['durata_piano_in_giorni']    : 0;
        $new['durata_piano_in_settimane'] = (isset($new['durata_piano_in_settimane'])) ? $new['durata_piano_in_settimane'] : 0;
        $new['data_inizio_piano'] 	      = (isset($new['data_inizio_piano']))         ? $new['data_inizio_piano']         : date('Y-01-01');
        $new['data_fine_piano'] 	      = (isset($new['data_fine_piano']))           ? $new['data_fine_piano']           : date('Y-12-31');

        //INSERT
        $sql = "INSERT INTO `mdl_aziende_piani_formativi` 
				    SET `skilledin_plan_id`         = " . $new['skilledin_plan_id']         . ",
				        `nome_azienda`              = " . $new['nome_azienda']              . ",
					    `nome_piano`                = " . $new['nome_piano'] 	 	        . ",
						`importo_massimo`           = " . $new['importo_massimo']           . ",
						`importo_finanziato`  		= " . $new['importo_finanziato']    	. ",
						`durata_piano_in_ore` 	  	= " . $new['durata_piano_in_ore']   	. ",
						`durata_piano_in_giorni` 	= " . $new['durata_piano_in_giorni']	. ",
						`durata_piano_in_settimane` = " . $new['durata_piano_in_settimane'] . ",
						`data_inizio_piano` 		= " . $new['data_inizio_piano'] 		. ",
						`data_fine_piano` 		    = " . $new['data_fine_piano'] 		    . " ";
        $this->connection->query($sql);
        return $this->connection->insert_id;
    }

    public function editAziendePianiFormativi($skilledin_plan_id, $data){
        $set = array();
        if(isset($data['nome_piano']))                $set['nome_piano']                = "`nome_piano`                = '" .  $data['nome_piano']                . "'";
        if(isset($data['importo_massimo']))           $set['importo_massimo']           = "`importo_massimo`           =  " .  $data['importo_massimo']           . " ";
        if(isset($data['importo_finanziato']))        $set['importo_finanziato']        = "`importo_finanziato`        =  " .  $data['importo_finanziato']        . " ";
        if(isset($data['durata_piano_in_ore']))       $set['durata_piano_in_ore']       = "`durata_piano_in_ore`       =  " .  $data['durata_piano_in_ore']       . " ";
        if(isset($data['durata_piano_in_giorni']))    $set['durata_piano_in_giorni']    = "`durata_piano_in_giorni`    =  " .  $data['durata_piano_in_giorni']    . " ";
        if(isset($data['durata_piano_in_settimane'])) $set['durata_piano_in_settimane'] = "`durata_piano_in_settimane` =  " .  $data['durata_piano_in_settimane'] . " ";
        if(isset($data['data_inizio_piano']))         $set['data_inizio_piano']         = "`data_inizio_piano`         = '" .  $data['data_inizio_piano']         . "'";
        if(isset($data['data_inizio_piano']))         $set['data_fine_piano']           = "`data_fine_piano`           = '" .  $data['data_fine_piano']           . "'";
        if(!empty($set)){
            $sql = "UPDATE `mdl_aziende_piani_formativi` SET " . implode(", ", $set) . " WHERE `id` = $skilledin_plan_id";
            $this->connection->query($sql);
        }else{
            echo 'Nessun dato da aggiornare';
        }
    }

    public function addAziendePianiCorsi($data = array()){
        $new = array();
        //DATA FORMAT
        if(isset($data['id_aziende_piani'])) $new['id_aziende_piani'] = (int) $data['id_aziende_piani'];
        if(isset($data['azienda']))          $new['azienda']          = "'" . $this->connection->real_escape_string($data['azienda']) . "'";
        if(isset($data['id_corso'])) 	     $new['id_corso']  	      = (int) $data['id_corso'];
        if(isset($data['nome_corso']))       $new['nome_corso']       = "'" . $this->connection->real_escape_string($data['nome_corso']) . "'";

        //DATA CHECK
        $new['id_aziende_piani'] = (isset($new['id_aziende_piani'])) ? $new['id_aziende_piani'] : 0;
        $new['azienda']          = (isset($new['azienda']))          ? $new['azienda']          : "''";
        $new['id_corso'] 	     = (isset($new['id_corso']))         ? $new['id_corso']         : 0;
        $new['nome_corso']       = (isset($new['nome_corso']))       ? $new['nome_corso']       : "''";

        //INSERT
        $sql = "INSERT INTO `mdl_aziende_piani_corsi` 
				    SET `id_aziende_piani` = " . $new['id_aziende_piani'] . ",
					    `azienda`          = " . $new['azienda'] 	 	  . ",
						`id_corso`         = " . $new['id_corso']         . ",
						`nome_corso` 	   = " . $new['nome_corso']    	  . " ";
        $this->connection->query($sql);
        return $this->connection->insert_id;
    }
    public function getAziendePianiCorsiBy($field, $value){
        $query = $this->connection->query("SELECT * FROM `mdl_aziende_piani_formativi` WHERE `$field` = '$value'");
        if($query->num_rows) return $query->fetch_assoc();
        return false;
    }

    public function getAziendePianoFormativoCorsiByPlanCourse($id_aziende_piani, $id_corso){
        $query = $this->connection->query("SELECT * FROM `mdl_aziende_piani_corsi` WHERE `id_aziende_piani` = $id_aziende_piani AND `id_corso` = $id_corso");
//        echo "SELECT * FROM `mdl_aziende_piani_corsi` WHERE `id_aziende_piani` = $id_aziende_piani AND `id_corso` = $id_corso";
//        exit;
        if($query->num_rows) return $query->fetch_assoc();
        return false;
    }
    public function deleteAziendePianiCorsi($aziende_piani_corsi_id = 0){
        $sql = "DELETE FROM `mdl_aziende_piani_corsi` WHERE `id` = $aziende_piani_corsi_id";
        $this->connection->query($sql);
        return $this->connection->affected_rows;
    }



}
