<?php

class Utility{
    public function __construct(){

    }
    public function convertSecondsToTime($seconds) {
        $hours        = floor($seconds / 3600);
        $minutes      = floor($seconds / 60 % 60);
        $seconds      = floor($seconds % 60);
        $seconds_time = sprintf('%02d:%02d:%02d', $hours, $minutes, $seconds);
        return $seconds_time;
    }
    public function convertSecondsToHour($seconds) {

            $hours        = floor($seconds / 3600);
            $minutes      = floor($seconds / 60 % 60)/60;
            $seconds      = floor($seconds % 60)/3600;
            $hour_time    = $hours+$minutes+$seconds;
            return number_format($hour_time, 2, '.', '');
    }

    public function convertTimeToSeconds($str_time){
        sscanf($str_time, "%d:%d:%d", $hours, $minutes, $seconds);
        $time_seconds = 0;
        if(isset($hours))   $time_seconds += $hours*3600;
        if(isset($minutes)) $time_seconds += $minutes*60;
        if(isset($seconds)) $time_seconds += $seconds;
        return $time_seconds;
    }
}


?>