<?php

session_start();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require '../../vendor/autoload.php';



if (isset($_SESSION['user']['piattaforma'])) {
    //echo $_SESSION['user']['piattaforma'];
    //exit();
    $db = new MongoDB\Client(
            'mongodb+srv://jobtek:TimeVision2021%40%21%3F@timevision.t9ed5.mongodb.net/TimeVision?retryWrites=true&w=majority');
    $table = $_SESSION['user']['piattaforma'];

    $collection = $db->$table->mdl_user_info_data;

    $cursor = $collection->find(
            ['data' => ['$ne' => ""], 'id' => ['$ne' => 0], 'fieldid' => ['$eq' => 1], 'data' => ['$exists' => true]],
            ['projection' => ['data' => 1],
            ]
    );

    $array_aziende = array();
    if ($cursor) {
        $tmp = array();
        foreach ($cursor as $singolo) {
            if (!in_array($singolo['data'], $array_aziende)) {
                $singolo['data']= trim($singolo['data']);
                $tmp = $singolo['data'];
                array_push($array_aziende, $tmp);
            }
        }
    }
    echo json_encode($array_aziende);
    
}
exit();