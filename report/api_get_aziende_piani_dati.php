#!/usr/bin/php
<?php

ini_set('memory_limit', '2048M');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
ini_set('max_execution_time', 0);
error_reporting(E_ALL);

$abs_path = '/home/admin/public_html';
if(isset($_SERVER['DOCUMENT_ROOT']) && strlen(trim($_SERVER['DOCUMENT_ROOT']))) $abs_path = $_SERVER['DOCUMENT_ROOT'];

require_once($abs_path . "/skilledin/report/obj/Skilledin.php");
require_once($abs_path . "/skilledin/report/obj/SkilledinWeb.php");
require_once($abs_path . "/skilledin/report/obj/Report.php");
require_once($abs_path . "/skilledin/report/obj/Admin.php");
require_once($abs_path . "/skilledin/report/obj/SkilledinReport.php");
require_once($abs_path . "/skilledin/report/obj/Formazione.php");
require_once($abs_path . "/skilledin/report/obj/Formazione40.php");
require_once($abs_path . "/skilledin/report/obj/FondoNuoveCompetenze.php");
require_once($abs_path . "/skilledin/report/obj/Live.php");
require_once($abs_path . "/skilledin/report/obj/PlatformNew.php");
require_once($abs_path . "/skilledin/report/obj/Utility.php");

$enterpise         = '';
$token             = '';
$skilledin_plan_id = '';
if(isset($_POST['enterprise']))        $enterpise         = $_POST['enterprise'];
if(isset($_POST['token']))             $token             = $_POST['token'];
if(isset($_POST['skilledin_plan_id'])) $skilledin_plan_id = $_POST['skilledin_plan_id'];

if(isset($_GET['enterprise']))        $enterpise         = $_GET['enterprise'];
if(isset($_GET['token']))             $token             = $_GET['token'];
if(isset($_GET['skilledin_plan_id'])) $skilledin_plan_id = $_GET['skilledin_plan_id'];


if ($token == 'skpl_2023') {
    $report_data = false;
    $object      = new Admin('admin');
    $plans       = $object->getAziendePianiFormativi(array('nome_azienda' => $enterpise));
    if($skilledin_plan_id != ''){
        $report_data = $object->calculateReportAziendePianiFormativi($skilledin_plan_id);
    }else{
        if($plans){
            $next_expired_plan = 0;
            foreach($plans as $plan){
                if($next_expired_plan == 0 || $plan['stamp_fine_piano'] < $next_expired_plan){
                    $report_data = $object->calculateReportAziendePianiFormativi($plan['skilledin_plan_id']);
                    $next_expired_plan = $plan['stamp_fine_piano'];
                }
            }
        }
    }
    if($report_data){
        $response = array();
        foreach ($report_data['user_data_table'] as $item){
            if(!isset($item['partial_user_time'])) $item['partial_user_time'] = 0;

            $expected_time   = (int) $report_data['unit_course_duration_time']/$report_data['durata_piano_in_giorni'];
            $difference_time = abs($expected_time-$item['partial_user_time']);
            $difference_sign = ($item['partial_user_time']>$expected_time) ? '+' : '-';

            $data = array();
            $data['cf']               = $item['fiscal_code'];
            $data['fullname']         = $item['name'];
            $data['plan_time']        = $report_data['unit_course_duration'];
            $data['effective_time']   = $object->tool->convertSecondsToTime((int) $item['partial_user_time']);
            $data['effective_hour']   = $object->tool->convertSecondsToHour((int) $item['partial_user_time']);
            $data['effective_value']  = 0.00;
            $data['expected_time']    = $object->tool->convertSecondsToTime($expected_time);
            $data['expected_hour']    = $object->tool->convertSecondsToHour($expected_time);
            $data['expected_value']   = 0.00;
            $data['difference_time']    = $difference_sign . ' ' . $object->tool->convertSecondsToTime($difference_time);
//            $data['difference_time']    = $object->tool->convertSecondsToTime($difference_time);
            $data['difference_hour']    = $object->tool->convertSecondsToHour($difference_time);
            $data['difference_value'] = 0.00;
            $response[] = $data;
        }
        header('Content-Type: application/json; charset=utf-8');
        http_response_code(201);
        echo json_encode($response);
    }
}
