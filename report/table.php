<?php

session_start();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
ini_set('memory_limit', '-1');

error_reporting(E_ALL);
require '../../vendor/autoload.php';

$password = urlencode("TimeVision2021@!?");
$collection = new MongoDB\Client('mongodb+srv://jobtek:' . $password . '@timevision.t9ed5.mongodb.net/TimeVision?retryWrites=true&w=majority');
if (isset($_SESSION['user']['piattaforma']))
    $table = $_SESSION['user']['piattaforma'];

if (isset($_POST['nome'])) {
    
} else if (isset($_POST['aule'])) {

    $collection = $collection->$table->mdl_bigbluebuttonbn;

    $cursore = $collection->find(
            ['id' => ['$ne' => 0]],
            ['projection' => ['name' => 1, 'id' => 1]]
    );

    $aule = array();
    foreach ($cursore as $singolo) {
        $aule[$singolo['id']] = $singolo['name'];
    }
    echo json_encode($aule);
    exit();
}
else if (isset($_POST['iscritti'])) {

    $collection = $collection->$table->mdl_user;

    $cursore = $collection->find(
            ['id' => ['$ne' => 0]],
            ['projection' => ['firstname' => 1, 'lastname' => 1, 'id' => 1]]
    );

    $iscritti = array();
    foreach ($cursore as $singolo) {
        $iscritti[$singolo['id']] = $singolo['firstname'] . " " . $singolo['lastname'];
    }
    echo json_encode($iscritti);
    exit();
}
else if (isset($_POST['idCorso']) && !isset($_POST['azienda'])) {
    $idCorso = $_POST['idCorso'];
    $piattaforma = $_POST['platf'];
    if ($piattaforma == "sessione")
        $piattaforma = $table;
    $forced = true;
    if($_POST['report'] == 'shvpName_new' || $_POST['report'] == 'hvpName' || $forced){
        $collection = $collection->$piattaforma->mdl_user_info_data;

        $cursor = $collection->find(
            ['data' => ['$ne' => ""], 'id' => ['$ne' => 0], 'fieldid' => ['$eq' => 1], 'data' => ['$exists' => true]],
            ['projection' => ['data' => 1],
            ]
        );

        $array_aziende = array();
        if ($cursor) {
            $tmp = array();
            foreach ($cursor as $singolo) {
                if (!in_array($singolo['data'], $array_aziende)) {
                    $singolo['data']= trim($singolo['data']);
                    $tmp = $singolo['data'];
                    array_push($array_aziende, $tmp);
                }
            }
        }
        echo json_encode($array_aziende);
    }else{
        $collect = $collection->$piattaforma->corsiAssociati;
        $cursore = $collect->find(
            ['_id' => (int) $idCorso],
            ['projection' => ['aziende' => 1]]
        );
        $aziende = array();
        $collection = $collection->$piattaforma->mdl_user_info_data;
        $giro = 0;
        foreach ($cursore as $singolo) {
            foreach ($singolo['aziende'] as $azienda) {
                $aziende[$azienda] = $azienda;
                $giro++;
            }
        }
        echo json_encode($aziende);
    }
    exit();
}
else if (isset($_POST['platf']) && isset($_POST['azienda'])) {
    $piattaforma = $_POST['platf'];
    $idCorso = $_POST['idCorso'];
    if ($piattaforma == "sessione")
        $piattaforma = $table;
    $collect = $collection->$piattaforma->mdl_utenti_complete;
    if ($_POST['azienda'] != "Tutti") {
        $match = ['$match' => [
                'azienda' => $_POST['azienda']
            ]
        ];
    } else {
        $match = ['$match' => [
                'azienda' => [
                    '$ne' => ''
                ]
            ]
        ];
    }
    $pipeline = [$match,
        ['$lookup' => [
                'from' => 'mdl_logstore_standard_log',
                'localField' => 'userid',
                'foreignField' => 'userid',
                'as' => 'log'
            ]
        ], [
            '$unwind' => [
                'path' => '$log',
                'preserveNullAndEmptyArrays' => false
            ]
        ], [
            '$group' => [
                '_id' => [
                    'userid' => '$userid',
                    'nome' => '$nome',
                    'courseid' => '$log.courseid'
                ]
            ]
        ], [
            '$match' => [
                '_id.courseid' => (int) $idCorso
            ]
        ], [
            '$project' => [
                'userid' => '$_id.userid',
                'nome' => '$_id.nome',
                '_id' => 0
            ]
    ]];
    $out = $collect->aggregate($pipeline);
    $utenti = array();
    foreach ($out as $singolo) {
        $utenti[$singolo['userid']] = $singolo['nome'];
    }

    echo json_encode($utenti);
    exit();
} else {

    $gruppo = $_POST['azienda'];

    if ($gruppo != "") {


        $collection = $collection->$table->corsiAssociati;

        $cursore = $collection->find(
                [],
                ['projection' => ['fullname' => 1, 'aziende' => 1, '_id' => 1]]
        );

        $corsi = array();
        foreach ($cursore as $singolo) {
            foreach ($singolo['aziende'] as $azienda) {
                if(!isset($singolo['fullname'])) continue;
                if ($gruppo == $azienda)
                    $corsi[$singolo['_id']] = $singolo['fullname'];
            }
        }

        echo json_encode($corsi);
        exit();
    } else {


        $collection = $collection->$table->mdl_course;

        $cursore = $collection->find(
                ['id' => ['$ne' => 0]],
                ['projection' => ['id' => 1, 'fullname' => 1]]
        );

        $corsi = array();
        foreach ($cursore as $singolo) {
            $corsi[$singolo['id']] = $singolo['fullname'];
        }
        echo json_encode($corsi);
        exit();
    }
}
exit();
