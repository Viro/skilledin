<?php
session_start();
require '../../vendor/autoload.php';

/*
$collection = new MongoDB\Client(
        "mongodb+srv://jobtek:TimeVision2021%40%21%3F@timevision.t9ed5.mongodb.net/TimeVision?retryWrites=true&w=majority");
*/

require_once('config/connection/MongoDB/skilledin.php');
$connection_string  = "mongodb+srv://";
$connection_string .= conn_username . ":" . urlencode(conn_password) . "@" . conn_server;
$connection_string .= "?retryWrites=true&w=majority";
$collection = new MongoDB\Client($connection_string);


$piattaforme = $collection->admin->piattaforme;

$cursor = $piattaforme->find(
        [],
        ['projection' => ['id' => 1, 'nome' => 1]]
);


foreach ($cursor as $singolo) {
    $array_piattaforma[$singolo['id']] = $singolo['nome'];
}

if (!isset($_SESSION['user']))
    header("location:login.php");
if (isset($_POST['logout'])) {
    session_destroy();
    $id = $_COOKIE['FIRST'];
    setcookie('FIRST', $id, 1, "/");
    header("location:index.php");
}
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">

        <!-- Bootstrap CSS -->
        <!-- CSS only -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->




        <!-- Bootstrap 5 JS-->
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js"
                integrity="sha384-SR1sx49pcuLnqZUnnPwx6FCym0wLsk5JZuNx2bPPENzswTNFaQU1RDvt3wT4gWFG"
        crossorigin="anonymous"></script>
        <!-- JavaScript Bundle with Popper -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
                integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4"
        crossorigin="anonymous"></script>



        <title>Report</title>






        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap4.min.css">
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap4.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/2.1.0/css/buttons.dataTables.min.css">
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/2.1.0/js/dataTables.buttons.min.js"></script>
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/2.1.0/js/buttons.colVis.min.js"></script>


        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/js-cookie@3.0.1/dist/js.cookie.min.js"></script>


        <script>
            var idRuolo = Number(<?php echo $_SESSION['user']['idRuolo']; ?>);
            var idPiattaforma = '<?php echo $_SESSION['user']['idPiattaforma']; ?>';
            var checked = ["vuoto"];
        </script>
        <script src="../js/colors.js"></script>
        <script src="../js/listener.js"></script>
        <script src="../js/DataTable.js"></script>



        <style>
            body {
                margin-top: 40px;
                font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
            }

            #calendar {
                max-width: 800px;
                margin: 0 auto;
            }

            .select2-container {
                width: 100% !important;
                padding: 0;
            }

            ul.no-bullets {
                list-style-type: none; /* Remove bullets */
                padding: 0; /* Remove padding */
                margin: 0; /* Remove margins */
            }

            .dt-button-collection button.buttons-columnVisibility:before,
            .dt-button-collection button.buttons-columnVisibility.active span:before {
                display:block;
                position:absolute;
                top:1.2em;
                left:0;
                width:12px;
                height:12px;
                box-sizing:border-box;
            }

            .dt-button-collection button.buttons-columnVisibility:before {
                content:' ';
                margin-top:-6px;
                margin-left:10px;
                border:1px solid black;
                border-radius:3px;
            }

            .dt-button-collection button.buttons-columnVisibility.active span:before {
                content:'\2714';
                margin-top:-11px;
                margin-left:12px;
                text-align:center;
                text-shadow:1px 1px #DDD, -1px -1px #DDD, 1px -1px #DDD, -1px 1px #DDD;
            }

            .dt-button-collection button.buttons-columnVisibility span {
                margin-left:20px;
            }




        </style>
    </head>

    <body>
        <div class="container" >
            <?php if ($_SESSION['user']['idRuolo'] == 0 || $_SESSION['user']['idRuolo'] == 1 || $_SESSION['user']['idRuolo'] == 4) { ?>
                <a href="confirmPage.php?admin" class="previous" style="text-decoration: none;">‹ Torna indietro</a>
            <?php } ?>
            <div class="row my-2" >
                <div class="col-12">
                    <div class="card">
                        <div class="card-header" style="display:none;" id="card-header">
                            <div  style="display:flex;" >
                                <select class="form-select"  name="course" id="header-report"
                                        aria-label="Floating label select example" style="width: 95%;" >
                                            <?php
                                            foreach ($array_piattaforma as $key => $value) {
                                                if ($key == 4) {
                                                    ?>
                                            <option value="<?php echo $key; ?>" disabled><?php echo $value; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $key; ?>" selected><?php echo $value; ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                                <?php if ($_SESSION['user']['idRuolo'] == 0 || $_SESSION['user']['idRuolo'] == 1) { ?>
                                    <form action="settings.php" method="POST">
                                        <button class="btn btn-default btn-sm "  id="settings" name="settings" type="submit" style='font-size:12px;'>Settings
                                            <i class="fa fa-gear"></i></button>
                                    </form>
                                <?php } ?>
                                <form action="index.php" method="POST">
                                    <button class="btn btn-default btn-sm "  id="logout" name="logout" type="submit" style='font-size:12px;'>Log out <i class='fas fa-sign-out-alt'></i></button>
                                </form>
                            </div>
                        </div>
                        <div class="card-body" id="card-body"  style="display:none;">


                            <div class="d-flex align-items-start">
                                <div class="nav d-flex flex-row nav-pills me-3" id="v-pills-tab" role="tablist" aria-orientation="vertical" style="
                                     overflow: auto;
                                     width: 15%;
                                     height: 210px;
                                     justify-content: center;
                                     ">
                                    <button class="nav-link active" id="v-pills-total-tab" data-bs-toggle="pill"
                                            data-bs-target="#v-pills-total" type="button" role="tab"
                                            aria-controls="v-pills-total" aria-selected="true">Log standard</button>
                                    <button class="nav-link " id="v-pills-sintesi-tab" data-bs-toggle="pill"
                                            data-bs-target="#v-pills-sintesi" type="button" role="tab"
                                            aria-controls="v-pills-sintesi" aria-selected="false">Sintesi Login-Logout</button>
                                    <button class="nav-link" id="v-pills-timein-tab" data-bs-toggle="pill"
                                            data-bs-target="#v-pills-timein" type="button" role="tab"
                                            aria-controls="v-pills-timein" aria-selected="false">Tempo in piattaforma</button>
                                    <button class="nav-link" id="v-pills-bbb-tab" data-bs-toggle="pill"
                                            data-bs-target="#v-pills-bbb" type="button" role="tab" aria-controls="v-pills-bbb"
                                            aria-selected="false">Sincrona BBB</button>
                                    <button class="nav-link" id="v-pills-sbbb-tab" data-bs-toggle="pill"
                                            data-bs-target="#v-pills-sbbb" type="button" role="tab"
                                            aria-controls="v-pills-sbbb" aria-selected="false">Minuti cumulati BBB</button>
                                    <button class="nav-link" id="v-pills-hvp-tab" data-bs-toggle="pill"
                                            data-bs-target="#v-pills-hvp" type="button" role="tab"
                                            aria-controls="v-pills-hvp" aria-selected="false">Asincrona HVP</button>
                                    <button class="nav-link" id="v-pills-summaryhvp-tab" data-bs-toggle="pill"
                                            data-bs-target="#v-pills-summaryhvp" type="button" role="tab"
                                            aria-controls="v-pills-summaryhvp" aria-selected="false">Riepilogo asincrona HVP</button>
                                    <button class="nav-link" id="v-pills-shvp-tab" data-bs-toggle="pill"
                                            data-bs-target="#v-pills-shvp" type="button" role="tab"
                                            aria-controls="v-pills-shvp" aria-selected="false">Minuti video cumulati HVP</button>
                                    <button class="nav-link" id="v-pills-rhvp-tab" data-bs-toggle="pill"
                                            data-bs-target="#v-pills-rhvp" type="button" role="tab"
                                            aria-controls="v-pills-rhvp" aria-selected="false">Riepilogo HVP</button>
                                    <button class="nav-link" id="v-pills-zoom-tab" data-bs-toggle="pill"
                                            data-bs-target="#v-pills-zoom" type="button" role="tab" aria-controls="v-pills-zoom"
                                            aria-selected="false">Sincrona Zoom</button>
                                    <button class="nav-link" id="v-pills-s_zoom-tab" data-bs-toggle="pill"
                                            data-bs-target="#v-pills-s_zoom" type="button" role="tab" aria-controls="v-pills-zoom"
                                            aria-selected="false">Minuti cumulati Zoom</button>
                                    <button class="nav-link" id="v-pills-videotimepro-tab" data-bs-toggle="pill"
                                            data-bs-target="#v-pills-videotimepro" type="button" role="tab" aria-controls="v-pills-videotimepro"
                                            aria-selected="false">Minuti cumulati Videotime</button>
                                    <?php if ($_SESSION['user']['id'] == 3 || $_SESSION['user']['id'] == 5 || $_SESSION['user']['id'] == 19) { ?>
                                        <!-- Visione riservata utente Admin per report test -->
                                        <button class="nav-link text-danger" id="v-pills-hvp-new-tab" data-bs-toggle="pill"
                                                data-bs-target="#v-pills-hvp-new" type="button" role="tab" aria-controls="v-pills-hvp-new"
                                                aria-selected="false">Asincrona HVP (NEW)</button>
                                        <button class="nav-link text-danger" id="v-pills-shvp-new-tab" data-bs-toggle="pill"
                                                data-bs-target="#v-pills-shvp-new" type="button" role="tab" aria-controls="v-pills-shvp-new"
                                                aria-selected="false">Minuti cumulati HVP (NEW)</button>
                                        <button class="nav-link text-danger" id="v-pills-summaryhvp-new-tab" data-bs-toggle="pill"
                                                data-bs-target="#v-pills-summaryhvp-new" type="button" role="tab" aria-controls="v-pills-summaryhvp-new"
                                                aria-selected="false">Riepilogo HVP (NEW)</button>
                                    <?php }?>

                                </div>

                                <div class="tab-content w-100" id="v-pills-tabContent">
                                    <div class="tab-pane fade show active" id="v-pills-total" role="tabpanel"
                                         aria-labelledby="v-pills-total-tab">
                                        <div class="row">
                                            <ul class="nav nav-tabs mb-3" id="myTab" role="tablist">
                                                <li class="nav-item" role="presentation">
                                                    <button class="nav-link active" id="home-tab" data-bs-toggle="tab"
                                                            data-bs-target="#home" type="button" role="tab" aria-controls="home"
                                                            aria-selected="true">Azienda</button>
                                                </li>
                                                <li class="nav-item" role="presentation">
                                                    <button class="nav-link" id="profile-tab" data-bs-toggle="tab"
                                                            data-bs-target="#profile" type="button" role="tab"
                                                            aria-controls="profile" aria-selected="false">Iscritti</button>
                                                </li>
                                            </ul>
                                            <div class="tab-content" id="myTabContent">
                                                <div class="tab-pane fade show active" id="home" role="tabpanel"
                                                     aria-labelledby="home-tab">
                                                    <div class="row row-cols-2 g-2 g-lg-3">
                                                        <div class="col-12">
                                                            <label for="id_enterprise_total">Azienda</label>
                                                            <select class="js-example-basic-single js-states form-control" id="id_enterprise_total"></select>
                                                        </div>
                                                        <!--  <div class="col-6">
                                                  <div class="form-floating">
                                                                  <select class="form-select" name="course" id="id_course_total"
                                                                          aria-label="Floating label select example">
                                                                  </select>
                                                                  <label for="id_course_bbb">Corso</label>
                                                              </div>
                                                          </div>-->


                                                        <div class="col-6">
                                                            <div class="form-floating">
                                                                <input type="date" class="form-control" id="id_start_total"
                                                                       min="2021-01-01" value='<?php echo date("Y-m-d", strtotime("-1 month")); ?>'>
                                                                <label for="id_start_total">Inizio:</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="form-floating">
                                                                <input type="date" class="form-control" id="id_end_total"
                                                                       min="2021-01-01" value='<?php echo date('Y-m-d'); ?>'>
                                                                <label for="id_end_total">Fine:</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-12">
                                                            <div class="form-floating">
                                                                <button id="id_submitbutton_total" type="button"
                                                                        class="btn btn-primary btn-sm">Genera</button>
                                                                <button id="id_excel_total" type="button"
                                                                        class="btn btn-secondary btn-sm" disabled="true">Excel</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="profile" role="tabpanel"
                                                     aria-labelledby="profile-tab">
                                                    <div class="row row-cols-2 g-2 g-lg-3">
                                                        <div class="col-12">
                                                            <label for="id_iscritto_total">Utente</label>
                                                            <select class="js-example-basic-single js-states form-control" id="id_iscritto_total"></select>
                                                        </div>


                                                        <div class="col-6">
                                                            <div class="form-floating">
                                                                <input type="date" class="form-control"
                                                                       id="id_start_total_iscritto" min="2021-01-01"
                                                                       value='<?php echo date("Y-m-d", strtotime("-1 month")); ?>'>
                                                                <label for="id_start_video">Inizio:</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="form-floating">
                                                                <input type="date" class="form-control"
                                                                       id="id_end_total_iscritto" min="2021-01-01" value='<?php echo date('Y-m-d'); ?>'>
                                                                <label for="id_end_total">Fine:</label>
                                                            </div>
                                                        </div>

                                                        <div class="col-12">
                                                            <div class="form-floating">
                                                                <button id="id_submitbutton_total_iscritto" type="button"
                                                                        class="btn btn-primary btn-sm">Genera</button>
                                                                <button id="id_excel_total_iscritto" type="button"
                                                                        class="btn btn-secondary btn-sm" disabled="true" >Excel</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="v-pills-sintesi" role="tabpanel"
                                         aria-labelledby="v-pills-sintesi-tab">
                                        <div class="row">
                                            <ul class="nav nav-tabs mb-3" id="sintesi-tab" role="tablist">
                                                <li class="nav-item" role="presentation">
                                                    <button class="nav-link active" id="home-tab-sintesi" data-bs-toggle="tab"
                                                            data-bs-target="#home-sintesi" type="button" role="tab" aria-controls="home"
                                                            aria-selected="true">Azienda</button>
                                                </li>
                                                <li class="nav-item" role="presentation">
                                                    <button class="nav-link" id="profile-tab-sintesi" data-bs-toggle="tab"
                                                            data-bs-target="#profile-sintesi" type="button" role="tab"
                                                            aria-controls="profile" aria-selected="false">Iscritti</button>
                                                </li>
                                            </ul>
                                            <div class="tab-content" id="myTabContent-sintesi">
                                                <div class="tab-pane fade show active" id="home-sintesi" role="tabpanel"
                                                     aria-labelledby="home-tab-sintesi">
                                                    <div class="row row-cols-2 g-2 g-lg-3">
                                                        <div class="col-12">
                                                            <label for="id_enterprise_sintesi">Azienda</label>
                                                            <select class="js-example-basic-single js-states form-control" id="id_enterprise_sintesi"></select>
                                                        </div>

                                                        <div class="col-6">
                                                            <div class="form-floating">
                                                                <input type="date" class="form-control" id="id_start_sintesi"
                                                                       min="2021-01-01" value='<?php echo date("Y-m-d", strtotime("-1 month")); ?>'>
                                                                <label for="id_start_sintesi">Inizio:</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="form-floating">
                                                                <input type="date" class="form-control" id="id_end_sintesi"
                                                                       min="2021-01-01" value='<?php echo date('Y-m-d'); ?>'>
                                                                <label for="id_end_sintesi">Fine:</label>
                                                            </div>
                                                        </div>

                                                        <div class="col-12">
                                                            <div class="form-floating">
                                                                <button id="id_submitbutton_sintesi" type="button"
                                                                        class="btn btn-primary btn-sm">Genera</button>
                                                                <button id="id_excel_sintesi" type="button"
                                                                        class="btn btn-secondary btn-sm" disabled="true">Excel</button>
                                                                <button id="addLog_sintesi" type="button" class="btn btn-secondary btn-sm"
                                                                        data-toggle="modal" data-target="#modal--sintesi" disabled="true">Aggiungi log</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="profile-sintesi" role="tabpanel"
                                                     aria-labelledby="profile-tab-sintesi">
                                                    <div class="row row-cols-2 g-2 g-lg-3">
                                                        <div class="col-12">
                                                            <label for="id_iscritto_sintesi">Utente</label>
                                                            <select class="js-example-basic-single js-states form-control" id="id_iscritto_sintesi"></select>
                                                        </div>


                                                        <div class="col-6">
                                                            <div class="form-floating">
                                                                <input type="date" class="form-control"
                                                                       id="id_start_sintesi_iscritto" min="2021-01-01"
                                                                       value='<?php echo date("Y-m-d", strtotime("-1 month")); ?>'>
                                                                <label for="id_start_sintesi_iscritto">Inizio:</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="form-floating">
                                                                <input type="date" class="form-control"
                                                                       id="id_end_sintesi_iscritto" min="2021-01-01" value='<?php echo date('Y-m-d'); ?>'>
                                                                <label for="id_end_sintesi_iscritto">Fine:</label>
                                                            </div>
                                                        </div>

                                                        <div class="col-12">
                                                            <div class="form-floating">
                                                                <button id="id_submitbutton_sintesi_iscritto" type="button"
                                                                        class="btn btn-primary btn-sm">Genera</button>
                                                                <button id="id_excel_sintesi_iscritto" type="button"
                                                                        class="btn btn-secondary btn-sm" disabled="true" >Excel</button>
                                                                <button id="addLog_sintesi_iscritto" type="button" class="btn btn-secondary btn-sm"
                                                                        data-toggle="modal" data-target="#modal--sintesi" disabled="true">Aggiungi log</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="v-pills-timein" role="tabpanel"
                                         aria-labelledby="v-pills-timein-tab">
                                        <div class="row">
                                            <ul class="nav nav-tabs mb-3" id="timein-tab" role="tablist">
                                                <li class="nav-item" role="presentation">
                                                    <button class="nav-link active" id="home-tab-timein" data-bs-toggle="tab"
                                                            data-bs-target="#home-timein" type="button" role="tab" aria-controls="home"
                                                            aria-selected="true">Azienda</button>
                                                </li>
                                                  <li class="nav-item" role="presentation">
                                                    <button class="nav-link" id="profile-tab-timein" data-bs-toggle="tab"
                                                            data-bs-target="#profile-timein" type="button" role="tab"
                                                            aria-controls="profile" aria-selected="false">Iscritti</button>
                                                </li>
                                            </ul>
                                            <div class="tab-content" id="myTabContent-timein">
                                                <div class="tab-pane fade show active" id="home-timein" role="tabpanel"
                                                     aria-labelledby="home-tab-timein">
                                                    <div class="row row-cols-2 g-2 g-lg-3">
                                                        <div class="col-12">
                                                            <label for="id_enterprise_timein">Azienda</label>
                                                            <select class="js-example-basic-single js-states form-control" id="id_enterprise_timein"></select>
                                                        </div>

                                                        <div class="col-6">
                                                            <div class="form-floating">
                                                                <input type="date" class="form-control" id="id_start_timein"
                                                                       min="2021-01-01" value='<?php echo date("Y-m-d", strtotime("-1 month")); ?>'>
                                                                <label for="id_start_timein">Inizio:</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="form-floating">
                                                                <input type="date" class="form-control" id="id_end_timein"
                                                                       min="2021-01-01" value='<?php echo date('Y-m-d'); ?>'>
                                                                <label for="id_end_timein">Fine:</label>
                                                            </div>
                                                        </div>

                                                        <div class="col-12">
                                                            <div class="form-floating">
                                                                <button id="id_submitbutton_timein" type="button"
                                                                        class="btn btn-primary btn-sm">Genera</button>
                                                                <button id="id_excel_timein" type="button"
                                                                        class="btn btn-secondary btn-sm" disabled="true">Excel</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                 <div class="tab-pane fade" id="profile-timein" role="tabpanel"
                                                     aria-labelledby="profile-tab-timein">
                                                    <div class="row row-cols-2 g-2 g-lg-3">
                                                        <div class="col-6">
                                                            <label for="id_iscritto_timein">Corso</label>
                                                            <select class="js-example-basic-single js-states form-control" id="id_corso_timeinN"></select>
                                                        </div>
                                                        <div class="col-6">
                                                            <label for="id_enterprise_timeinN">Azienda</label>
                                                            <select class="js-example-basic-single js-states form-control" id="id_enterprise_timeinN"></select>
                                                        </div>

                                                        <div class="col-6">
                                                            <div class="form-floating">
                                                                <input type="date" class="form-control"
                                                                       id="id_start_timein_corso" min="2021-01-01"
                                                                       value='<?php echo date("Y-m-d", strtotime("-1 month")); ?>'>
                                                                <label for="id_start_timein_iscritto">Inizio:</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="form-floating">
                                                                <input type="date" class="form-control"
                                                                       id="id_end_timein_corso" min="2021-01-01" value='<?php echo date('Y-m-d'); ?>'>
                                                                <label for="id_end_timein_iscritto">Fine:</label>
                                                            </div>
                                                        </div>

                                                        <div class="col-12">
                                                            <div class="form-floating">
                                                                <button id="id_submitbutton_timein_corso" type="button"
                                                                        class="btn btn-primary btn-sm">Genera</button>
                                                                <button id="id_excel_timein_corso" type="button"
                                                                        class="btn btn-secondary btn-sm" disabled="true" >Excel</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="v-pills-rhvp" role="tabpanel"
                                         aria-labelledby="v-pills-rhvp-tab">
                                        <div class="row">
                                            <ul class="nav nav-tabs mb-3" id="rhvpName-tab" role="tablist">
                                                <li class="nav-item" role="presentation">
                                                    <button class="nav-link active" id="home-tab-rhvpName" data-bs-toggle="tab"
                                                            data-bs-target="#home-rhvpName" type="button" role="tab" aria-controls="home"
                                                            aria-selected="true">Azienda</button>
                                                </li>
                                                <li class="nav-item" role="presentation">
                                                    <button class="nav-link" id="profile-tab-rhvpName" data-bs-toggle="tab"
                                                            data-bs-target="#profile-rhvpName" type="button" role="tab"
                                                            aria-controls="profile" aria-selected="false">Iscritti</button>
                                                </li>
                                            </ul>
                                            <div class="tab-content" id="myTabContent-rhvpName">
                                                <div class="tab-pane fade show active" id="home-rhvpName" role="tabpanel"
                                                     aria-labelledby="home-tab-rhvpName">
                                                    <div class="row row-cols-2 g-2 g-lg-3">
                                                        <div class="col-12">
                                                            <label for="id_enterprise_rhvp">Azienda</label>
                                                            <select class="js-example-basic-single js-states form-control" id="id_enterprise_rhvp"></select>
                                                        </div>

                                                        <div class="col-6">
                                                            <div class="form-floating">
                                                                <input type="date" class="form-control" id="id_start_rhvp"
                                                                       min="2021-01-01" value='<?php echo date("Y-m-d", strtotime("-1 month")); ?>'>
                                                                <label for="id_start_rhvp">Inizio:</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="form-floating">
                                                                <input type="date" class="form-control" id="id_end_rhvp"
                                                                       min="2021-01-01" value='<?php echo date('Y-m-d'); ?>'>
                                                                <label for="id_end_rhvp">Fine:</label>
                                                            </div>
                                                        </div>

                                                        <div class="col-12">
                                                            <div class="form-floating">
                                                                <button id="id_submitbutton_rhvp" type="button"
                                                                        class="btn btn-primary btn-sm">Genera</button>
                                                                <button id="id_excel_rhvp" type="button"
                                                                        class="btn btn-secondary btn-sm" disabled="true">Excel</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="profile-rhvpName" role="tabpanel"
                                                     aria-labelledby="profile-tab-rhvpName">
                                                    <div class="row row-cols-2 g-2 g-lg-3">
                                                        <div class="col-6">
                                                            <label for="id_corso_rhvp">Corso</label>
                                                            <select class="js-example-basic-single js-states form-control" id="id_corso_rhvp"></select>
                                                        </div>
                                                        <div class="col-6">
                                                            <label for="id_enterprise_rhvpN">Azienda</label>
                                                            <select class="js-example-basic-single js-states form-control" id="id_enterprise_rhvpN"></select>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="form-floating">
                                                                <input type="date" class="form-control"
                                                                       id="id_start_rhvp_corso" min="2021-01-01"
                                                                       value='<?php echo date("Y-m-d", strtotime("-1 month")); ?>'>
                                                                <label for="id_start_rhvp_corso">Inizio:</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="form-floating">
                                                                <input type="date" class="form-control"
                                                                       id="id_end_rhvp_corso" min="2021-01-01" value='<?php echo date('Y-m-d'); ?>'>
                                                                <label for="id_end_rhvp_corso">Fine:</label>
                                                            </div>
                                                        </div>

                                                        <div class="col-12">
                                                            <div class="form-floating">
                                                                <button id="id_submitbutton_rhvp_corso" type="button"
                                                                        class="btn btn-primary btn-sm">Genera</button>
                                                                <button id="id_excel_rhvp_corso" type="button"
                                                                        class="btn btn-secondary btn-sm" disabled="true" >Excel</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="v-pills-shvp" role="tabpanel"
                                         aria-labelledby="v-pills-shvp-tab">
                                        <div class="row">
                                            <ul class="nav nav-tabs mb-3" id="shvpName-tab" role="tablist">
                                                <li class="nav-item" role="presentation">
                                                    <button class="nav-link active" id="home-tab-shvpName" data-bs-toggle="tab"
                                                            data-bs-target="#home-shvpName" type="button" role="tab" aria-controls="home"
                                                            aria-selected="true">Azienda</button>
                                                </li>
                                                <li class="nav-item" role="presentation">
                                                    <button class="nav-link" id="profile-tab-shvpName" data-bs-toggle="tab"
                                                            data-bs-target="#profile-shvpName" type="button" role="tab"
                                                            aria-controls="profile" aria-selected="false">Iscritti</button>
                                                </li>
                                            </ul>
                                            <div class="tab-content" id="myTabContent-shvpName">
                                                <div class="tab-pane fade show active" id="home-shvpName" role="tabpanel"
                                                     aria-labelledby="home-tab-shvpName">
                                                    <div class="row row-cols-2 g-2 g-lg-3">
                                                        <div class="col-12">
                                                            <label for="id_enterprise_shvp">Azienda</label>
                                                            <select class="js-example-basic-single js-states form-control" id="id_enterprise_shvp"></select>
                                                        </div>

                                                        <div class="col-6">
                                                            <div class="form-floating">
                                                                <input type="date" class="form-control" id="id_start_shvp"
                                                                       min="2021-01-01" value='<?php echo date("Y-m-d", strtotime("-1 month")); ?>'>
                                                                <label for="id_start_shvp">Inizio:</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="form-floating">
                                                                <input type="date" class="form-control" id="id_end_shvp"
                                                                       min="2021-01-01" value='<?php echo date('Y-m-d'); ?>'>
                                                                <label for="id_end_shvp">Fine:</label>
                                                            </div>
                                                        </div>

                                                        <div class="col-12">
                                                            <div class="form-floating">
                                                                <button id="id_submitbutton_shvp" type="button"
                                                                        class="btn btn-primary btn-sm">Genera</button>
                                                                <button id="id_excel_shvp" type="button"
                                                                        class="btn btn-secondary btn-sm" disabled="true">Excel</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="profile-shvpName" role="tabpanel"
                                                     aria-labelledby="profile-tab-shvpName">
                                                    <div class="row row-cols-2 g-2 g-lg-3">
                                                        <div class="col-6">
                                                            <label for="id_corso_shvp">Corso</label>
                                                            <select class="js-example-basic-single js-states form-control" id="id_corso_shvp"></select>
                                                        </div>
                                                        <div class="col-6">
                                                            <label for="id_enterprise_shvpN">Azienda</label>
                                                            <select class="js-example-basic-single js-states form-control" id="id_enterprise_shvpN"></select>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="form-floating">
                                                                <input type="date" class="form-control"
                                                                       id="id_start_shvp_corso" min="2021-01-01"
                                                                       value='<?php echo date("Y-m-d", strtotime("-1 month")); ?>'>
                                                                <label for="id_start_shvp_corso">Inizio:</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="form-floating">
                                                                <input type="date" class="form-control"
                                                                       id="id_end_shvp_corso" min="2021-01-01" value='<?php echo date('Y-m-d'); ?>'>
                                                                <label for="id_end_shvp_corso">Fine:</label>
                                                            </div>
                                                        </div>

                                                        <div class="col-12">
                                                            <div class="form-floating">
                                                                <button id="id_submitbutton_shvp_corso" type="button"
                                                                        class="btn btn-primary btn-sm">Genera</button>
                                                                <button id="id_excel_shvp_corso" type="button"
                                                                        class="btn btn-secondary btn-sm" disabled="true" >Excel</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="v-pills-hvp" role="tabpanel"
                                         aria-labelledby="v-pills-hvp-tab">
                                        <div class="row">
                                            <ul class="nav nav-tabs mb-3" id="myTab-hvp" role="tablist">
                                                <li class="nav-item" role="presentation">
                                                    <button class="nav-link active" id="home-tab-hvp" data-bs-toggle="tab"
                                                            data-bs-target="#home-hvp" type="button" role="tab" aria-controls="home-hvp"
                                                            aria-selected="true">Azienda</button>
                                                </li>
                                                <li class="nav-item" role="presentation">
                                                    <button class="nav-link" id="profile-tab-hvp" data-bs-toggle="tab"
                                                            data-bs-target="#profile-hvp" type="button" role="tab"
                                                            aria-controls="profile-hvp" aria-selected="false">Iscritti</button>
                                                </li>
                                            </ul>
                                            <div class="tab-content" id="myTabContent-hvp">
                                                <div class="tab-pane fade show active" id="home-hvp" role="tabpanel"
                                                     aria-labelledby="home-hvp">
                                                    <div class="row row-cols-2 g-2 g-lg-3">
                                                        <div class="col-6">

                                                            <label for="id_enterprise_hvp">Azienda</label>
                                                            <select class="js-example-basic-single js-states form-control" id="id_enterprise_hvp">
                                                            </select>

                                                        </div>
                                                        <div class="col-6">
                                                            <label for="id_course_hvp">Corsi</label>
                                                            <select class="js-example-basic-single js-states form-control" id="id_course_hvp">
                                                                <option value="0"  selected>Tutti</option>
                                                            </select>
                                                        </div>


                                                        <div class="col-6">
                                                            <div class="form-floating">
                                                                <input type="date" class="form-control" id="id_start_hvp"
                                                                       min="2021-01-01" value='<?php echo date("Y-m-d", strtotime("-1 month")); ?>'>
                                                                <label for="id_start_video">Inizio:</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="form-floating">
                                                                <input type="date" class="form-control" id="id_end_hvp"
                                                                       min="2021-01-01" value='<?php echo date('Y-m-d'); ?>'>
                                                                <label for="id_end_video">Fine:</label>
                                                            </div>
                                                        </div>

                                                        <div class="col-12">
                                                            <div class="form-floating">
                                                                <button id="id_submitbutton_hvp" type="button"
                                                                        class="btn btn-primary btn-sm">Genera</button>
                                                                <button id="id_excel_hvp" type="button"
                                                                        class="btn btn-secondary btn-sm" disabled="true">Excel</button>
                                                                <button id="addLog_hvp" type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#modal--hvp--course" disabled="true">Aggiungi log
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="profile-hvp" role="tabpanel"
                                                     aria-labelledby="profile-tab-hvp">
                                                    <div class="row row-cols-2 g-2 g-lg-3">
                                                        <div class="col-6">
                                                            <label for="id_courses_hvp">Corsi</label>
                                                            <select class="js-example-basic-single js-states form-control" id="id_courses_hvp">
                                                            </select>
                                                        </div>
                                                        <div class="col-6">
                                                            <label for="id_courses_hvp">Azienda</label>
                                                            <select class="js-example-basic-single js-states form-control" id="id_enterprise_hvpName">
                                                            </select>
                                                        </div>


                                                        <div class="col-6">
                                                            <div class="form-floating">
                                                                <input type="date" class="form-control"
                                                                       id="id_start_hvp_course" min="2021-01-01"
                                                                       value='<?php echo date("Y-m-d", strtotime("-1 month")); ?>'>
                                                                <label for="id_start_hvp">Inizio:</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="form-floating">
                                                                <input type="date" class="form-control"
                                                                       id="id_end_hvp_course" min="2021-01-01"
                                                                       value='<?php echo date('Y-m-d'); ?>'>
                                                                <label for="id_end_hvp">Fine:</label>
                                                            </div>
                                                        </div>

                                                        <div class="col-12">
                                                            <div class="form-floating"style="width: 100%;">
                                                                <button id="id_submitbutton_hvp_course" type="button"
                                                                        class="btn btn-primary btn-sm">Genera</button>
                                                                <button id="id_excel_hvp_course" type="button"
                                                                        class="btn btn-secondary btn-sm" disabled="true" >Excel</button>

                                                                <button id="addLog_hvp_course" type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#modal--hvp--course" disabled="true">Aggiungi log
                                                                </button>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="v-pills-summaryhvp" role="tabpanel" aria-labelledby="v-pills-summaryhvp-tab">
                                        <div class="row">
                                            <ul class="nav nav-tabs mb-3" id="myTab-summaryhvp" role="tablist">
                                                <li class="nav-item" role="presentation">
                                                    <button class="nav-link active" id="home-tab-summaryhvp" data-bs-toggle="tab"
                                                            data-bs-target="#home-summaryhvp" type="button" role="tab" aria-controls="home-summaryhvp"
                                                            aria-selected="true">Azienda</button>
                                                </li>
                                                <li class="nav-item" role="presentation">
                                                    <button class="nav-link" id="profile-tab-summaryhvp" data-bs-toggle="tab"
                                                            data-bs-target="#profile-summaryhvp" type="button" role="tab"
                                                            aria-controls="profile-summaryhvp" aria-selected="false">Iscritti</button>
                                                </li>
                                            </ul>
                                            <div class="tab-content" id="myTabContent-summaryhvp">
                                                <div class="tab-pane fade show active" id="home-summaryhvp" role="tabpanel"
                                                     aria-labelledby="home-summaryhvp">
                                                    <div class="row row-cols-2 g-2 g-lg-3">
                                                        <div class="col-6">
                                                            <label for="id_enterprise_summaryhvp">Azienda</label>
                                                            <select class="js-example-basic-single js-states form-control" id="id_enterprise_summaryhvp">
                                                            </select>

                                                        </div>
                                                        <div class="col-6">
                                                            <label for="id_course_summaryhvp">Corsi</label>
                                                            <select class="js-example-basic-single js-states form-control" id="id_course_summaryhvp">
                                                                <option value="0"  selected>Tutti</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="form-floating">
                                                                <input type="date" class="form-control" id="id_start_summaryhvp"
                                                                       min="2021-01-01" value='<?php echo date("Y-m-d", strtotime("-1 month")); ?>'>
                                                                <label for="id_start_summaryhvp">Inizio:</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="form-floating">
                                                                <input type="date" class="form-control" id="id_end_summaryhvp"
                                                                       min="2021-01-01" value='<?php echo date('Y-m-d'); ?>'>
                                                                <label for="id_end_summaryhvp">Fine:</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-12">
                                                            <div class="form-floating">
                                                                <button id="id_submitbutton_summaryhvp" type="button"
                                                                        class="btn btn-primary btn-sm">Genera</button>
                                                                <button id="id_excel_summaryhvp" type="button"
                                                                        class="btn btn-secondary btn-sm" disabled="true">Excel</button>
                                                                <button id="addLog_summaryhvp" type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#modal--summaryhvp--course" disabled="true">Aggiungi log
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="profile-summaryhvp" role="tabpanel"
                                                     aria-labelledby="profile-tab-summaryhvp">
                                                    <div class="row row-cols-2 g-2 g-lg-3">
                                                        <div class="col-6">
                                                            <label for="id_courses_summaryhvp">Corsi</label>
                                                            <select class="js-example-basic-single js-states form-control" id="id_courses_summaryhvp">
                                                            </select>
                                                        </div>
                                                        <div class="col-6">
                                                            <label for="id_enterprise_summaryhvp">Azienda</label>
                                                            <select class="js-example-basic-single js-states form-control" id="id_enterprise_summaryhvp">
                                                            </select>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="form-floating">
                                                                <input type="date" class="form-control"
                                                                       id="id_start_summaryhvp_course" min="2021-01-01"
                                                                       value='<?php echo date("Y-m-d", strtotime("-1 month")); ?>'>
                                                                <label for="id_start_summaryhvp_course">Inizio:</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="form-floating">
                                                                <input type="date" class="form-control"
                                                                       id="id_end_summaryhvp_course" min="2021-01-01"
                                                                       value='<?php echo date('Y-m-d'); ?>'>
                                                                <label for="id_end_summaryhvp_course">Fine:</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-12">
                                                            <div class="form-floating"style="width: 100%;">
                                                                <button id="id_submitbutton_summaryhvp_course" type="button"
                                                                        class="btn btn-primary btn-sm">Genera</button>
                                                                <button id="id_excel_summaryhvp_course" type="button"
                                                                        class="btn btn-secondary btn-sm" disabled="true" >Excel</button>

                                                                <button id="addLog_summaryhvp_course" type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#modal--summaryhvp--course" disabled="true">Aggiungi log
                                                                </button>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="v-pills-s_zoom" role="tabpanel"
                                         aria-labelledby="v-pills-s_zoom-tab">
                                        <div class="row">
                                            <ul class="nav nav-tabs mb-3" id="myTab-s_zoom" role="tablist">
                                                <li class="nav-item" role="presentation">
                                                    <button class="nav-link active" id="home-tab-s_zoom" data-bs-toggle="tab"
                                                            data-bs-target="#home-s_zoom" type="button" role="tab" aria-controls="home-s_zoom"
                                                            aria-selected="true">Azienda</button>
                                                </li>
                                                <li class="nav-item" role="presentation">
                                                    <button class="nav-link" id="profile-tab-s_zoom" data-bs-toggle="tab"
                                                            data-bs-target="#profile-s_zoom" type="button" role="tab"
                                                            aria-controls="profile-s_zoom" aria-selected="false">Iscritti</button>
                                                </li>
                                            </ul>
                                            <div class="tab-content" id="myTabContent-s_zoom">
                                                <div class="tab-pane fade show active" id="home-s_zoom" role="tabpanel"
                                                     aria-labelledby="home-s_zoom">
                                                    <div class="row row-cols-2 g-2 g-lg-3">
                                                        <div class="col-12">
                                                            <label for="id_enterprise_s_zoom">Azienda</label>
                                                            <select class="js-example-basic-single js-states form-control" id="id_enterprise_s_zoom"></select>
                                                        </div>

                                                        <div class="col-6">
                                                            <div class="form-floating">
                                                                <input type="date" class="form-control" id="id_start_s_zoom"
                                                                       min="2021-01-01" value='<?php echo date("Y-m-d", strtotime("-1 month")); ?>'>
                                                                <label for="id_start_s_zoom">Inizio:</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="form-floating">
                                                                <input type="date" class="form-control" id="id_end_s_zoom"
                                                                       min="2021-01-01" value='<?php echo date('Y-m-d'); ?>'>
                                                                <label for="id_end_s_zoom">Fine:</label>
                                                            </div>
                                                        </div>

                                                        <div class="col-12">
                                                            <div class="form-floating">
                                                                <button id="id_submitbutton_s_zoom" type="button"
                                                                        class="btn btn-primary btn-sm">Genera</button>
                                                                <button id="id_excel_s_zoom" type="button"
                                                                        class="btn btn-secondary btn-sm" disabled="true">Excel</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="profile-s_zoom" role="tabpanel"
                                                     aria-labelledby="profile-tab-s_zoom">
                                                    <div class="row row-cols-2 g-2 g-lg-3">
                                                        <div class="col-6">
                                                            <label for="id_iscritto_s_zoom">Corso</label>
                                                            <select class="js-example-basic-single js-states form-control" id="id_iscritto_s_zoom">
                                                            </select>
                                                        </div>

                                                        <div class="col-6">
                                                            <label for="id_iscritto_s_zoom">Azienda</label>
                                                            <select class="js-example-basic-single js-states form-control" id="id_enterprise_s_zoomName">
                                                            </select>
                                                        </div>


                                                        <div class="col-6">
                                                            <div class="form-floating">
                                                                <input type="date" class="form-control"
                                                                       id="id_start_s_zoom_iscritto" min="2021-01-01"
                                                                       value='<?php echo date("Y-m-d", strtotime("-1 month")); ?>'>
                                                                <label for="id_start_s_zoom_iscritto">Inizio:</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="form-floating">
                                                                <input type="date" class="form-control"
                                                                       id="id_end_s_zoom_iscritto" min="2021-01-01"
                                                                       value='<?php echo date('Y-m-d'); ?>'>
                                                                <label for="id_end_s_zoom_iscritto">Fine:</label>
                                                            </div>
                                                        </div>

                                                        <div class="col-12">
                                                            <div class="form-floating"style="width: 100%;">
                                                                <button id="id_submitbutton_s_zoom_iscritto" type="button"
                                                                        class="btn btn-primary btn-sm">Genera</button>
                                                                <button id="id_excel_s_zoom_iscritto" type="button"
                                                                        class="btn btn-secondary btn-sm" disabled="true" >Excel</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="v-pills-sbbb" role="tabpanel"
                                         aria-labelledby="v-pills-sbbb-tab">
                                        <div class="row">
                                            <ul class="nav nav-tabs mb-3" id="myTab-sbbb" role="tablist">
                                                <li class="nav-item" role="presentation">
                                                    <button class="nav-link active" id="home-tab-sbbb" data-bs-toggle="tab"
                                                            data-bs-target="#home-sbbb" type="button" role="tab" aria-controls="home-sbbb"
                                                            aria-selected="true">Azienda</button>
                                                </li>
                                                <li class="nav-item" role="presentation">
                                                    <button class="nav-link" id="profile-tab-sbbb" data-bs-toggle="tab"
                                                            data-bs-target="#profile-sbbb" type="button" role="tab"
                                                            aria-controls="profile-sbbb" aria-selected="false">Iscritti</button>
                                                </li>
                                            </ul>
                                            <div class="tab-content" id="myTabContent-sbbb">
                                                <div class="tab-pane fade show active" id="home-sbbb" role="tabpanel"
                                                     aria-labelledby="home-sbbb">
                                                    <div class="row row-cols-2 g-2 g-lg-3">
                                                        <div class="col-12">
                                                            <label for="id_enterprise_sbbb">Azienda</label>
                                                            <select class="js-example-basic-single js-states form-control" id="id_enterprise_sbbb"></select>
                                                        </div>

                                                        <div class="col-6">
                                                            <div class="form-floating">
                                                                <input type="date" class="form-control" id="id_start_sbbb"
                                                                       min="2021-01-01" value='<?php echo date("Y-m-d", strtotime("-1 month")); ?>'>
                                                                <label for="id_start_sbbb">Inizio:</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="form-floating">
                                                                <input type="date" class="form-control" id="id_end_sbbb"
                                                                       min="2021-01-01" value='<?php echo date('Y-m-d'); ?>'>
                                                                <label for="id_end_sbbb">Fine:</label>
                                                            </div>
                                                        </div>

                                                        <div class="col-12">
                                                            <div class="form-floating">
                                                                <button id="id_submitbutton_sbbb" type="button"
                                                                        class="btn btn-primary btn-sm">Genera</button>
                                                                <button id="id_excel_sbbb" type="button"
                                                                        class="btn btn-secondary btn-sm" disabled="true">Excel</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="profile-sbbb" role="tabpanel"
                                                     aria-labelledby="profile-tab-sbbb">
                                                    <div class="row row-cols-2 g-2 g-lg-3">
                                                        <div class="col-6">
                                                            <label for="id_iscritto_sbbb">Corso</label>
                                                            <select class="js-example-basic-single js-states form-control" id="id_iscritto_sbbb">
                                                            </select>
                                                        </div>
                                                        <div class="col-6">
                                                            <label for="id_enterprise_sbbb">Azienda</label>
                                                            <select class="js-example-basic-single js-states form-control" id="id_enterprise_sbbbN"></select>
                                                        </div>


                                                        <div class="col-6">
                                                            <div class="form-floating">
                                                                <input type="date" class="form-control"
                                                                       id="id_start_sbbb_iscritto" min="2021-01-01"
                                                                       value='<?php echo date("Y-m-d", strtotime("-1 month")); ?>'>
                                                                <label for="id_start_sbbb_iscritto">Inizio:</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="form-floating">
                                                                <input type="date" class="form-control"
                                                                       id="id_end_sbbb_iscritto" min="2021-01-01"
                                                                       value='<?php echo date('Y-m-d'); ?>'>
                                                                <label for="id_end_sbbb_iscritto">Fine:</label>
                                                            </div>
                                                        </div>

                                                        <div class="col-12">
                                                            <div class="form-floating"style="width: 100%;">
                                                                <button id="id_submitbutton_sbbb_iscritto" type="button"
                                                                        class="btn btn-primary btn-sm">Genera</button>
                                                                <button id="id_excel_sbbb_iscritto" type="button"
                                                                        class="btn btn-secondary btn-sm" disabled="true" >Excel</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="v-pills-bbb" role="tabpanel"
                                         aria-labelledby="v-pills-bbb-tab">
                                        <div class="row">
                                            <ul class="nav nav-tabs mb-3" id="myTab-bbb" role="tablist">
                                                <li class="nav-item" role="presentation">
                                                    <button class="nav-link active" id="home-tab-bbb" data-bs-toggle="tab"
                                                            data-bs-target="#home-bbb" type="button" role="tab" aria-controls="home-bbb"
                                                            aria-selected="true">Azienda</button>
                                                </li>
                                                <li class="nav-item" role="presentation">
                                                    <button class="nav-link" id="profile-tab-bbb" data-bs-toggle="tab"
                                                            data-bs-target="#profile-bbb" type="button" role="tab"
                                                            aria-controls="profile-bbb" aria-selected="false">Iscritti</button>
                                                </li>
                                            </ul>
                                            <div class="tab-content" id="myTabContent-bbb">
                                                <div class="tab-pane fade show active" id="home-bbb" role="tabpanel"
                                                     aria-labelledby="home-bbb">
                                                    <div class="row row-cols-2 g-2 g-lg-3">
                                                        <div class="col-6">
                                                            <label for="id_enterprise_bbb">Azienda</label>
                                                            <select class="js-example-basic-single js-states form-control" id="id_enterprise_bbb">
                                                            </select>
                                                        </div>
                                                        <div class="col-6">
                                                            <label for="id_course_bbb">Corsi</label>
                                                            <select class="js-example-basic-single js-states form-control" id="id_course_bbb">
                                                                <option value="0"  selected>Tutti</option>
                                                            </select>
                                                        </div>

                                                        <div class="col-6">
                                                            <div class="form-floating">
                                                                <input type="date" class="form-control" id="id_start_bbb"
                                                                       min="2021-01-01" value='<?php echo date("Y-m-d", strtotime("-1 month")); ?>'>
                                                                <label for="id_start_bbb">Inizio:</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="form-floating">
                                                                <input type="date" class="form-control" id="id_end_bbb"
                                                                       min="2021-01-01" value='<?php echo date('Y-m-d'); ?>'>
                                                                <label for="id_end_bbb">Fine:</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-12">
                                                            <div class="form-floating">
                                                                <button id="id_submitbutton_bbb" type="button"
                                                                        class="btn btn-primary btn-sm">Genera</button>
                                                                <button id="id_excel_bbb" type="button"
                                                                        class="btn btn-secondary btn-sm" disabled="true">Excel</button>
                                                                <button id="addLog_bb" type="button" class="btn btn-secondary btn-sm"
                                                                        data-toggle="modal" data-target="#modal--bb--course" disabled="true">Aggiungi log</button>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="tab-pane fade" id="profile-bbb" role="tabpanel"
                                                     aria-labelledby="profile-tab-bbb">
                                                    <div class="row row-cols-2 g-2 g-lg-3">
                                                        <div class="col-6">
                                                            <label for="id_courses_bbb">Corsi</label>
                                                            <select class="js-example-basic-single js-states form-control" id="id_courses_bbb">
                                                            </select>
                                                        </div>
                                                        <div class="col-6">
                                                            <label for="id_courses_bbb">Azienda</label>
                                                            <select class="js-example-basic-single js-states form-control" id="id_enterprise_bbbName">
                                                            </select>
                                                        </div>


                                                        <div class="col-6">
                                                            <div class="form-floating">
                                                                <input type="date" class="form-control"
                                                                       id="id_start_bbb_course" min="2021-01-01"
                                                                       value='<?php echo date("Y-m-d", strtotime("-1 month")); ?>'>
                                                                <label for="id_start_bbb">Inizio:</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="form-floating">
                                                                <input type="date" class="form-control"
                                                                       id="id_end_bbb_course" min="2021-01-01"
                                                                       value='<?php echo date('Y-m-d'); ?>'>
                                                                <label for="id_end_bbb">Fine:</label>
                                                            </div>
                                                        </div>

                                                        <div class="col-12">
                                                            <div class="form-floating"style="width: 100%;">
                                                                <button id="id_submitbutton_bbb_course" type="button"
                                                                        class="btn btn-primary btn-sm">Genera</button>
                                                                <button id="id_excel_bbb_course" type="button"
                                                                        class="btn btn-secondary btn-sm" disabled="true" >Excel</button>

                                                                <button id="addLog_bb_course" type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#modal--bb--course" disabled="true">Aggiungi log
                                                                </button>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="v-pills-zoom" role="tabpanel"
                                         aria-labelledby="v-pills-zoom-tab">
                                        <div class="row">
                                            <ul class="nav nav-tabs mb-3" id="myTab-zoom" role="tablist">
                                                <li class="nav-item" role="presentation">
                                                    <button class="nav-link active" id="home-tab-zoom" data-bs-toggle="tab"
                                                            data-bs-target="#home-zoom" type="button" role="tab" aria-controls="home-zoom"
                                                            aria-selected="true">Azienda</button>
                                                </li>
                                                <li class="nav-item" role="presentation">
                                                    <button class="nav-link" id="profile-tab-zoom" data-bs-toggle="tab"
                                                            data-bs-target="#profile-zoom" type="button" role="tab"
                                                            aria-controls="profile-zoom" aria-selected="false">Iscritti</button>
                                                </li>
                                            </ul>
                                            <div class="tab-content" id="myTabContent-zoom">
                                                <div class="tab-pane fade show active" id="home-zoom" role="tabpanel"
                                                     aria-labelledby="home-zoom">
                                                    <div class="row row-cols-2 g-2 g-lg-3">
                                                        <div class="col-6">

                                                            <label for="id_enterprise_zoom">Azienda</label>
                                                            <select class="js-example-basic-single js-states form-control" id="id_enterprise_zoom">
                                                            </select>

                                                        </div>
                                                        <div class="col-6">
                                                            <label for="id_course_zoom">Corsi</label>
                                                            <select class="js-example-basic-single js-states form-control" id="id_course_zoom">
                                                                <option value="0"  selected>Tutti</option>
                                                            </select>
                                                        </div>


                                                        <div class="col-6">
                                                            <div class="form-floating">
                                                                <input type="date" class="form-control" id="id_start_zoom"
                                                                       min="2021-06-01" value='<?php echo date("Y-m-d", strtotime("-1 month")); ?>'>
                                                                <label for="id_start_zoom">Inizio:</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="form-floating">
                                                                <input type="date" class="form-control" id="id_end_zoom"
                                                                       min="2021-05-05" value='<?php echo date('Y-m-d'); ?>'>
                                                                <label for="id_end_zoom">Fine:</label>
                                                            </div>
                                                        </div>

                                                        <div class="col-12">
                                                            <div class="form-floating">
                                                                <button id="id_submitbutton_zoom" type="button"
                                                                        class="btn btn-primary btn-sm">Genera</button>
                                                                <button id="id_excel_zoom" type="button"
                                                                        class="btn btn-secondary btn-sm" disabled="true">Excel</button>
                                                                <button id="addLog_zoom" type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#modal--hvp--course" disabled="true">Aggiungi log
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="profile-zoom" role="tabpanel"
                                                     aria-labelledby="profile-tab-zoom">
                                                    <div class="row row-cols-2 g-2 g-lg-3">
                                                        <div class="col-6">
                                                            <label for="id_courses_zoom">Corsi</label>
                                                            <select class="js-example-basic-single js-states form-control" id="id_courses_zoom">
                                                            </select>
                                                        </div>
                                                        <div class="col-6">
                                                            <label for="id_courses_zoom">Azienda</label>
                                                            <select class="js-example-basic-single js-states form-control" id="id_enterprise_zoomName">
                                                            </select>
                                                        </div>


                                                        <div class="col-6">
                                                            <div class="form-floating">
                                                                <input type="date" class="form-control"
                                                                       id="id_start_zoom_course" min="2021-01-01"
                                                                       value='<?php echo date("Y-m-d", strtotime("-1 month")); ?>'>
                                                                <label for="id_start_zoom">Inizio:</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="form-floating">
                                                                <input type="date" class="form-control"
                                                                       id="id_end_zoom_course" min="2021-01-01"
                                                                       value='<?php echo date('Y-m-d'); ?>'>
                                                                <label for="id_end_zoom">Fine:</label>
                                                            </div>
                                                        </div>

                                                        <div class="col-12">
                                                            <div class="form-floating"style="width: 100%;">
                                                                <button id="id_submitbutton_zoom_course" type="button"
                                                                        class="btn btn-primary btn-sm">Genera</button>
                                                                <button id="id_excel_zoom_course" type="button"
                                                                        class="btn btn-secondary btn-sm" disabled="true" >Excel</button>

                                                                <button id="addLog_zoom_course" type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#modal--hvp--course" disabled="true">Aggiungi log
                                                                </button>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="v-pills-videotimepro" role="tabpanel" aria-labelledby="v-pills-videotimepro-tab">
                                        <div class="row">
                                            <ul class="nav nav-tabs mb-3" id="myTab-videotimepro" role="tablist">
                                                <li class="nav-item" role="presentation">
                                                    <button class="nav-link active" id="home-tab-videotimepro" data-bs-toggle="tab"
                                                            data-bs-target="#home-videotimepro" type="button" role="tab" aria-controls="home-videotimepro"
                                                            aria-selected="true">Azienda</button>
                                                </li>
                                                <li class="nav-item" role="presentation">
                                                    <button class="nav-link" id="profile-tab-videotimepro" data-bs-toggle="tab"
                                                            data-bs-target="#profile-videotimepro" type="button" role="tab"
                                                            aria-controls="profile-videotimepro" aria-selected="false">Iscritti</button>
                                                </li>
                                            </ul>
                                            <div class="tab-content" id="myTabContent-videotimepro">
                                                <div class="tab-pane fade show active" id="home-videotimepro" role="tabpanel"
                                                     aria-labelledby="home-videotimepro">
                                                    <div class="row row-cols-2 g-2 g-lg-3">
                                                        <div class="col-6">
                                                            <label for="id_enterprise_videotimepro">Azienda</label>
                                                            <select class="js-example-basic-single js-states form-control" id="id_enterprise_videotimepro">
                                                            </select>

                                                        </div>
                                                        <div class="col-6">
                                                            <label for="id_course_videotimepro">Corsi</label>
                                                            <select class="js-example-basic-single js-states form-control" id="id_course_videotimepro">
                                                                <option value="0"  selected>Tutti</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="form-floating">
                                                                <input type="date" class="form-control" id="id_start_videotimepro"
                                                                       min="2021-01-01" value='<?php echo date("Y-m-d", strtotime("-1 month")); ?>'>
                                                                <label for="id_start_videotimepro">Inizio:</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="form-floating">
                                                                <input type="date" class="form-control" id="id_end_videotimepro"
                                                                       min="2021-01-01" value='<?php echo date('Y-m-d'); ?>'>
                                                                <label for="id_end_videotimepro">Fine:</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-12">
                                                            <div class="form-floating">
                                                                <button id="id_submitbutton_videotimepro" type="button"
                                                                        class="btn btn-primary btn-sm">Genera</button>
                                                                <button id="id_excel_videotimepro" type="button"
                                                                        class="btn btn-secondary btn-sm" disabled="true">Excel</button>
                                                                <button id="addLog_videotimepro" type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#modal--videotimepro--course" disabled="true">Aggiungi log
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="profile-videotimepro" role="tabpanel"
                                                     aria-labelledby="profile-tab-videotimepro">
                                                    <div class="row row-cols-2 g-2 g-lg-3">
                                                        <div class="col-6">
                                                            <label for="id_courses_videotimepro">Corsi</label>
                                                            <select class="js-example-basic-single js-states form-control" id="id_courses_videotimepro">
                                                            </select>
                                                        </div>
                                                        <div class="col-6">
                                                            <label for="id_enterprise_videotimepro">Azienda</label>
                                                            <select class="js-example-basic-single js-states form-control" id="id_enterprise_videotimepro">
                                                            </select>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="form-floating">
                                                                <input type="date" class="form-control"
                                                                       id="id_start_videotimepro_course" min="2021-01-01"
                                                                       value='<?php echo date("Y-m-d", strtotime("-1 month")); ?>'>
                                                                <label for="id_start_videotimepro_course">Inizio:</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="form-floating">
                                                                <input type="date" class="form-control"
                                                                       id="id_end_videotimepro_course" min="2021-01-01"
                                                                       value='<?php echo date('Y-m-d'); ?>'>
                                                                <label for="id_end_videotimepro_course">Fine:</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-12">
                                                            <div class="form-floating"style="width: 100%;">
                                                                <button id="id_submitbutton_videotimepro_course" type="button"
                                                                        class="btn btn-primary btn-sm">Genera</button>
                                                                <button id="id_excel_videotimepro_course" type="button"
                                                                        class="btn btn-secondary btn-sm" disabled="true" >Excel</button>
                                                                <button id="addLog_videotimepro_course" type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#modal--videotimepro--course" disabled="true">Aggiungi log
                                                                </button>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php if ($_SESSION['user']['id'] == 3 || $_SESSION['user']['id'] == 5 || $_SESSION['user']['id'] == 19) { ?>
                                        <!-- Visione riservata utente Admin per report test -->
                                        <div class="tab-pane fade" id="v-pills-hvp-new" role="tabpanel"
                                             aria-labelledby="v-pills-hvp-new-tab">
                                            <div class="row">
                                                <ul class="nav nav-tabs mb-3" id="myTab-hvp-new" role="tablist">
                                                    <li class="nav-item" role="presentation">
                                                        <button class="nav-link active" id="home-tab-hvp-new" data-bs-toggle="tab"
                                                                data-bs-target="#home-hvp-new" type="button" role="tab" aria-controls="home-hvp-new"
                                                                aria-selected="true">Azienda</button>
                                                    </li>
                                                    <li class="nav-item" role="presentation">
                                                        <button class="nav-link" id="profile-tab-hvp-new" data-bs-toggle="tab"
                                                                data-bs-target="#profile-hvp-new" type="button" role="tab"
                                                                aria-controls="profile-hvp-new" aria-selected="false">Iscritti</button>
                                                    </li>
                                                </ul>
                                                <div class="tab-content" id="myTabContent-hvp-new">
                                                    <div class="tab-pane fade show active" id="home-hvp-new" role="tabpanel"
                                                         aria-labelledby="home-hvp-new">
                                                        <div class="row row-cols-2 g-2 g-lg-3">
                                                            <div class="col-6">

                                                                <label for="id_enterprise_hvp_new">Azienda</label>
                                                                <select class="js-example-basic-single js-states form-control" id="id_enterprise_hvp_new">
                                                                </select>

                                                            </div>
                                                            <div class="col-6">
                                                                <label for="id_course_hvp_new">Corsi</label>
                                                                <select class="js-example-basic-single js-states form-control" id="id_course_hvp_new">
                                                                    <option value="0"  selected>Tutti</option>
                                                                </select>
                                                            </div>


                                                            <div class="col-6">
                                                                <div class="form-floating">
                                                                    <input type="date" class="form-control" id="id_start_hvp_new"
                                                                           min="2021-01-01" value='<?php echo date("Y-m-d", strtotime("-1 month")); ?>'>
                                                                    <label for="id_start_hvp_new">Inizio:</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-6">
                                                                <div class="form-floating">
                                                                    <input type="date" class="form-control" id="id_end_hvp_new"
                                                                           min="2021-01-01" value='<?php echo date('Y-m-d'); ?>'>
                                                                    <label for="id_end_hvp_new">Fine:</label>
                                                                </div>
                                                            </div>

                                                            <div class="col-12">
                                                                <div class="form-floating">
                                                                    <button id="id_submitbutton_hvp_new" type="button"
                                                                            class="btn btn-primary btn-sm">Genera</button>
                                                                    <button id="id_excel_hvp_new" type="button"
                                                                            class="btn btn-secondary btn-sm" disabled="true">Excel</button>
                                                                    <button id="addLog_hvp_new" type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#modal--hvp--new--course" disabled="true">Aggiungi log
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane fade" id="profile-hvp-new" role="tabpanel"
                                                         aria-labelledby="profile-tab-hvp-new">
                                                        <div class="row row-cols-2 g-2 g-lg-3">
                                                            <div class="col-6">
                                                                <label for="id_courses_hvp_new">Corsi</label>
                                                                <select class="js-example-basic-single js-states form-control" id="id_courses_hvp_new">
                                                                </select>
                                                            </div>
                                                            <div class="col-6">
                                                                <label for="id_enterprise_hvpName_new">Azienda</label>
                                                                <select class="js-example-basic-single js-states form-control" id="id_enterprise_hvpName_new">
                                                                </select>
                                                            </div>
                                                            <div class="col-6">
                                                                <div class="form-floating">
                                                                    <input type="date" class="form-control"
                                                                           id="id_start_hvp_course_new" min="2021-01-01"
                                                                           value='<?php echo date("Y-m-d", strtotime("-1 month")); ?>'>
                                                                    <label for="id_start_hvp_course_new">Inizio:</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-6">
                                                                <div class="form-floating">
                                                                    <input type="date" class="form-control"
                                                                           id="id_end_hvp_course_new" min="2021-01-01"
                                                                           value='<?php echo date('Y-m-d'); ?>'>
                                                                    <label for="id_end_hvp_course_new">Fine:</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-12">
                                                                <div class="form-floating"style="width: 100%;">
                                                                    <button id="id_submitbutton_hvp_course_new" type="button"
                                                                            class="btn btn-primary btn-sm">Genera</button>
                                                                    <button id="id_excel_hvp_course_new" type="button"
                                                                            class="btn btn-secondary btn-sm" disabled="true" >Excel</button>

                                                                    <button id="addLog_hvp_course_new" type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#modal--hvp--course--new" disabled="true">Aggiungi log
                                                                    </button>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="v-pills-shvp-new" role="tabpanel"
                                             aria-labelledby="v-pills-shvp-new-tab">
                                            <div class="row">
                                                <ul class="nav nav-tabs mb-3" id="myTab-shvp-new" role="tablist">
                                                    <li class="nav-item" role="presentation">
                                                        <button class="nav-link active" id="home-tab-shvp-new" data-bs-toggle="tab"
                                                                data-bs-target="#home-shvp-new" type="button" role="tab" aria-controls="home-shvp-new"
                                                                aria-selected="true">Azienda</button>
                                                    </li>
                                                    <li class="nav-item" role="presentation">
                                                        <button class="nav-link" id="profile-tab-shvp-new" data-bs-toggle="tab"
                                                                data-bs-target="#profile-shvp-new" type="button" role="tab"
                                                                aria-controls="profile-shvp-new" aria-selected="false">Iscritti</button>
                                                    </li>
                                                </ul>
                                                <div class="tab-content" id="myTabContent-shvp-new">
                                                    <div class="tab-pane fade show active" id="home-shvp-new" role="tabpanel"
                                                         aria-labelledby="home-shvp-new">
                                                        <div class="row row-cols-2 g-2 g-lg-3">
                                                            <div class="col-12">
                                                                <label for="id_enterprise_shvp_new">Azienda</label>
                                                                <select class="js-example-basic-single js-states form-control" id="id_enterprise_shvp_new">
                                                                </select>
                                                            </div>
                                                            <div class="col-6">
                                                                <div class="form-floating">
                                                                    <input type="date" class="form-control" id="id_start_shvp_new"
                                                                           min="2021-01-01" value='<?php echo date("Y-m-d", strtotime("-1 month")); ?>'>
                                                                    <label for="id_start_shvp_new">Inizio:</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-6">
                                                                <div class="form-floating">
                                                                    <input type="date" class="form-control" id="id_end_shvp_new"
                                                                           min="2021-01-01" value='<?php echo date('Y-m-d'); ?>'>
                                                                    <label for="id_end_shvp_new">Fine:</label>
                                                                </div>
                                                            </div>

                                                            <div class="col-12">
                                                                <div class="form-floating">
                                                                    <button id="id_submitbutton_shvp_new" type="button"
                                                                            class="btn btn-primary btn-sm">Genera</button>
                                                                    <button id="id_excel_shvp_new" type="button"
                                                                            class="btn btn-secondary btn-sm" disabled="true">Excel</button>
                                                                    <button id="addLog_shvp_new" type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#modal--shvp--new--course" disabled="true">Aggiungi log
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane fade" id="profile-shvp-new" role="tabpanel"
                                                         aria-labelledby="profile-tab-hvp-new">
                                                        <div class="row row-cols-2 g-2 g-lg-3">
                                                            <div class="col-6">
                                                                <label for="id_courses_shvpName_new">Corsi</label>
                                                                <select class="js-example-basic-single js-states form-control" id="id_courses_shvpName_new">
                                                                </select>
                                                            </div>
                                                            <div class="col-6">
                                                                <label for="id_enterprise_shvpName_new">Azienda</label>
                                                                <select class="js-example-basic-single js-states form-control" id="id_enterprise_shvpName_new">
                                                                </select>
                                                            </div>
                                                            <div class="col-6">
                                                                <div class="form-floating">
                                                                    <input type="date" class="form-control"
                                                                           id="id_start_shvp_course_new" min="2021-01-01"
                                                                           value='<?php echo date("Y-m-d", strtotime("-1 month")); ?>'>
                                                                    <label for="id_start_shvp_course_new">Inizio:</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-6">
                                                                <div class="form-floating">
                                                                    <input type="date" class="form-control"
                                                                           id="id_end_shvp_course_new" min="2021-01-01"
                                                                           value='<?php echo date('Y-m-d'); ?>'>
                                                                    <label for="id_end_shvp_course_new">Fine:</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-12">
                                                                <div class="form-floating"style="width: 100%;">
                                                                    <button id="id_submitbutton_shvp_course_new" type="button"
                                                                            class="btn btn-primary btn-sm">Genera</button>
                                                                    <button id="id_excel_shvp_course_new" type="button"
                                                                            class="btn btn-secondary btn-sm" disabled="true" >Excel</button>
                                                                    <button id="addLog_shvp_course_new" type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#modal--shvp--course--new" disabled="true">Aggiungi log
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="v-pills-summaryhvp-new" role="tabpanel"
                                             aria-labelledby="v-pills-summaryhvp-new-tab">
                                            <div class="row">
                                                <ul class="nav nav-tabs mb-3" id="myTab-summaryhvp-new" role="tablist">
                                                    <li class="nav-item" role="presentation">
                                                        <button class="nav-link active" id="home-tab-summaryhvp-new" data-bs-toggle="tab"
                                                                data-bs-target="#home-summaryhvp-new" type="button" role="tab" aria-controls="home-summaryhvp-new"
                                                                aria-selected="true">Azienda</button>
                                                    </li>
                                                    <li class="nav-item" role="presentation">
                                                        <button class="nav-link" id="profile-tab-summaryhvp-new" data-bs-toggle="tab"
                                                                data-bs-target="#profile-summaryhvp-new" type="button" role="tab"
                                                                aria-controls="profile-summaryhvp-new" aria-selected="false">Iscritti</button>
                                                    </li>
                                                </ul>
                                                <div class="tab-content" id="myTabContent-summaryhvp-new">
                                                    <div class="tab-pane fade show active" id="home-summaryhvp-new" role="tabpanel"
                                                         aria-labelledby="home-summaryhvp-new">
                                                        <div class="row row-cols-2 g-2 g-lg-3">
                                                            <div class="col-6">

                                                                <label for="id_enterprise_summaryhvp_new">Azienda</label>
                                                                <select class="js-example-basic-single js-states form-control" id="id_enterprise_summaryhvp_new">
                                                                </select>

                                                            </div>
                                                            <div class="col-6">
                                                                <label for="id_course_summaryhvp_new">Corsi</label>
                                                                <select class="js-example-basic-single js-states form-control" id="id_course_summaryhvp_new">
                                                                    <option value="0"  selected>Tutti</option>
                                                                </select>
                                                            </div>


                                                            <div class="col-6">
                                                                <div class="form-floating">
                                                                    <input type="date" class="form-control" id="id_start_summaryhvp_new"
                                                                           min="2021-01-01" value='<?php echo date("Y-m-d", strtotime("-1 month")); ?>'>
                                                                    <label for="id_start_summaryhvp_new">Inizio:</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-6">
                                                                <div class="form-floating">
                                                                    <input type="date" class="form-control" id="id_end_summaryhvp_new"
                                                                           min="2021-01-01" value='<?php echo date('Y-m-d'); ?>'>
                                                                    <label for="id_end_summaryhvp_new">Fine:</label>
                                                                </div>
                                                            </div>

                                                            <div class="col-12">
                                                                <div class="form-floating">
                                                                    <button id="id_submitbutton_summaryhvp_new" type="button"
                                                                            class="btn btn-primary btn-sm">Genera</button>
                                                                    <button id="id_excel_summaryhvp_new" type="button"
                                                                            class="btn btn-secondary btn-sm" disabled="true">Excel</button>
                                                                    <button id="addLog_summaryhvp_new" type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#modal--summaryhvp--new--course" disabled="true">Aggiungi log
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane fade" id="profile-summaryhvp-new" role="tabpanel"
                                                         aria-labelledby="profile-tab-summaryhvp-new">
                                                        <div class="row row-cols-2 g-2 g-lg-3">
                                                            <div class="col-6">
                                                                <label for="id_courses_summaryhvp_new">Corsi</label>
                                                                <select class="js-example-basic-single js-states form-control" id="id_courses_summaryhvp_new">
                                                                </select>
                                                            </div>
                                                            <div class="col-6">
                                                                <label for="id_enterprise_summaryhvpName_new">Azienda</label>
                                                                <select class="js-example-basic-single js-states form-control" id="id_enterprise_summaryHvpName_new">
                                                                </select>
                                                            </div>
                                                            <div class="col-6">
                                                                <div class="form-floating">
                                                                    <input type="date" class="form-control"
                                                                           id="id_start_summaryhvp_course_new" min="2021-01-01"
                                                                           value='<?php echo date("Y-m-d", strtotime("-1 month")); ?>'>
                                                                    <label for="id_start_summaryhvp_course_new">Inizio:</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-6">
                                                                <div class="form-floating">
                                                                    <input type="date" class="form-control"
                                                                           id="id_end_summaryhvp_course_new" min="2021-01-01"
                                                                           value='<?php echo date('Y-m-d'); ?>'>
                                                                    <label for="id_end_summaryhvp_course_new">Fine:</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-12">
                                                                <div class="form-floating"style="width: 100%;">
                                                                    <button id="id_submitbutton_summaryhvp_course_new" type="button"
                                                                            class="btn btn-primary btn-sm">Genera</button>
                                                                    <button id="id_excel_summaryhvp_course_new" type="button"
                                                                            class="btn btn-secondary btn-sm" disabled="true" >Excel</button>

                                                                    <button id="addLog_summaryhvp_course_new" type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#modal--summaryhvp--course--new" disabled="true">Aggiungi log
                                                                    </button>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php }?>

                                    <!-- <div class="tab-pane fade" id="v-pills-zoom" role="tabpanel"
                                          aria-labelledby="v-pills-zoom-tab">
                                         <div class="row">

                                    <!-- <ul class="nav nav-tabs mb-3" id="myTab" role="tablist">
                                        <li class="nav-item" role="presentation">
                                            <button class="nav-link active" id="home-tab" data-bs-toggle="tab"
                                                data-bs-target="#report_zoom" type="button" role="tab"
                                                aria-controls="home" aria-selected="true">Report</button>
                                        </li>
                                        <li class="nav-item" role="presentation">
                                            <button class="nav-link" id="profile-tab" data-bs-toggle="tab"
                                                data-bs-target="#live_zoom" type="button" role="tab"
                                                aria-controls="profile" aria-selected="false">Live</button>
                                        </li>
                                    </ul> -->
                                    <!-- <div class="tab-content" id="myTabContent2"> -->
                                    <!-- <div class="tab-pane fade show active" id="report_zoom" role="tabpanel"
                                          aria-labelledby="home-tab">
                                         <div class="row row-cols-2 g-2 g-lg-3">
                                             <div class="col-12">
                                                 <div class="form-floating">
                                                     <input class="form-control" name="enterprise"
                                                            id="id_enterprise_zoom" list="datalistZoomEnterprises"
                                                            aria-label="Floating label select example">
                                                     <label for="id_enterprise_zoom">Azienda</label>
                                                 </div>
                                             </div>
                                    <!--  <div class="col-6">
                                          <div class="form-floating">
                                    <!-- <input class="form-control" name="course_zoom"
                                            id="id_course_zoom" list="datalistZoomCourses"
                                            aria-label="Floating label select example">
                                        <label for="id_course_zoom">Corso</label>    -->
                                    <!-- <div class="input-group">
                                         <div class="col-10">
                                             <div class="form-floating">
                                                 <input class="form-control" name="meetings_list"
                                                        id="id_course_zoom" list="datalistZoomCourses"
                                                        aria-label="Floating label select example"
                                                        style="border-top-right-radius: 0; border-bottom-right-radius: 0; border-right: 0;">
                                                 <label for="id_course_zoom">Meetings</label>
                                             </div>
                                         </div>-->
                                    <!-- <div class="col-2">
                                         <div class="input-group-text h-100"
                                              style="border-top-left-radius: 0; border-bottom-left-radius: 0; border-left: 0;">
                                             <input class="form-check-input mt-0" type="checkbox"
                                                    value=""
                                                    aria-label="Checkbox for following text input"
                                                    id="flexCheckAllZoom">
                                             <label class="form-check-label mx-1"
                                                    for="flexCheckAllZoom">
                                                 Tutti
                                             </label>
                                         </div>
                                     </div>-->
                                    <!-- </div> -->
                                    <!-- <div class="col-6">
                                         <div class="form-floating">
                                             <input type="date" class="form-control" id="id_start_zoom"
                                                    min="2021-01-01" value="2021-01-01">
                                             <label for="id_start_zoom">Inizio:</label>
                                         </div>
                                     </div>
                                     <div class="col-6">
                                         <div class="form-floating">
                                             <input type="date" class="form-control" id="id_end_zoom"
                                                    min="2021-01-01" value="2021-09-21">
                                             <label for="id_end_zoom">Fine:</label>
                                         </div>
                                     </div>
                                     <div class="col-12">
                                         <div class="form-floating">
                                             <button id="id_submitbutton_zoom" type="button"
                                                     class="btn btn-primary btn-sm">Genera</button>
                                             <button id="id_excel_zoom" type="button"
                                                     class="btn btn-secondary btn-sm">Excel</button>
                                         </div>
                                     </div>
                                 </div>

                             </div>
                                    <!-- <div class="tab-pane fade" id="live_zoom" role="tabpanel"
                                            aria-labelledby="profile-tab">
                                            <div class="row g-3">
                                                <div class="col-5">
                                                    <div class="form-floating">
                                                        <input type="date" class="form-control"
                                                            id="id_start_live_zoom" min="2021-05-05"
                                                            value="2021-05-05">
                                                        <label for="id_start_live_zoom">Inizio:</label>
                                                    </div>
                                                </div>
                                                <div class="col-5">
                                                    <div class="form-floating">
                                                        <input type="date" class="form-control"
                                                            id="id_end_live_zoom" min="2021-05-05">
                                                        <label for="id_end_live_zoom">Fine:</label>
                                                    </div>
                                                </div>
                                                <div class="col-2">
                                                    <div class="btn-group  h-100 w-100">
                                                        <button id="id_meetings_live_zoom" type="button"
                                                            class="btn btn-primary btn-sm">Meetings</button>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="input-group">
                                                        <div class="col-lg-11">
                                                            <div class="form-floating">
                                                                <input class="form-control" name="meetings_list"
                                                                    id="id_meetings_list" list="datalistMeetings"
                                                                    aria-label="Floating label select example"
                                                                    style="border-top-right-radius: 0; border-bottom-right-radius: 0; border-right: 0;">
                                                                <label for="id_meetings_list">Meetings</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-1">
                                                            <div class="input-group-text h-100"
                                                                style="border-top-left-radius: 0; border-bottom-left-radius: 0; border-left: 0;">
                                                                <input class="form-check-input mt-0" type="checkbox"
                                                                    value=""
                                                                    aria-label="Checkbox for following text input"
                                                                    id="flexCheckAllZoom">
                                                                <label class="form-check-label mx-1"
                                                                    for="flexCheckAllZoom">
                                                                    Tutti
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="col-12">
                                                    <div class="form-floating">
                                                        <button id="id_submitbutton_live_zoom" type="button"
                                                            class="btn btn-primary btn-sm">Genera</button>
                                                        <button id="id_excel_live_zoom" type="button"
                                                            class="btn btn-secondary btn-sm">Excel</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> -->
                                    <!-- </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <a id="nav"  style="display: flex;justify-content:center;"></a>
        </div>
        <div style="display:flex;">
            <!--  <div id="container--filtri" style="display:none;margin-left: 7.5%;">
                <div class="accordion" id="accordionExample" style="display: flex;">
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingOne">
                            <button class="accordion-button collapsed"id="bt--accordion" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                <span> Colonne da non visualizzare </span>
                            </button>
                        </h2>
                        <div id="collapseOne" class="accordion-collapse collapse " aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                            <div class="accordion-body" style="display:flex;">
                                <div>
                                    <ul id="myUL" class="no-bullets">
                                        <li id="data--container">
                                            <span class="a-list-item">
                                                <a>
                                                    <div>
                                                        <label><input class="checkbox" id="dataFilter" type="checkbox" name="" value="">
                                                            <i class="a-icon a-icon-checkbox"></i>
                                                            <span class="a-label a-checkbox-label">Data</span>
                                                        </label>
                                                    </div>
                                                </a>
                                            </span>
                                        </li>
                                        <li>
                                            <span class="a-list-item">
                                                <a>
                                                    <div>
                                                        <label><input class="checkbox" id="useridFilter" type="checkbox" name="" value="">
                                                            <i class="a-icon a-icon-checkbox"></i>
                                                            <span class="a-label a-checkbox-label">Userid</span>
                                                        </label>
                                                    </div>

                                                </a>
                                            </span>
                                        </li>
                                        <li id="username--container">
                                            <span class="a-list-item">
                                                <a>
                                                    <div>
                                                        <label><input class="checkbox" id="usernameFilter" type="checkbox" name="" value="">
                                                            <i class="a-icon a-icon-checkbox"></i>
                                                            <span class="a-label a-checkbox-label">Username</span>
                                                        </label>
                                                    </div>
                                                </a>
                                            </span>
                                        </li>
                                        <li>
                                            <span class="a-list-item">
                                                <a>
                                                    <div>
                                                        <label><input class="checkbox" id="nameFilter" type="checkbox" name="" value="">
                                                            <i class="a-icon a-icon-checkbox"></i>
                                                            <span class="a-label a-checkbox-label">Nome e Cognome</span>
                                                        </label>
                                                    </div>

                                                </a>
                                            </span>
                                        </li>


                                        <li>
                                            <span class="a-list-item">
                                                <a>
                                                    <div>
                                                        <label><input class="checkbox" id="cfFilter" type="checkbox" name="" value="">
                                                            <i class="a-icon a-icon-checkbox"></i>
                                                            <span class="a-label a-checkbox-label">Codice Fiscale</span>
                                                        </label>
                                                    </div>

                                                </a>
                                            </span>
                                        </li>

                                        <li>
                                            <span class="a-list-item">
                                                <a>
                                                    <div>
                                                        <label><input class="checkbox" id="emailFilter" type="checkbox" name="" value="">
                                                            <i class="a-icon a-icon-checkbox"></i>
                                                            <span class="a-label a-checkbox-label">Email</span>
                                                        </label>
                                                    </div>

                                                </a>
                                            </span>
                                        </li>
                                        <li>
                                            <span class="a-list-item">
                                                <a>
                                                    <div>
                                                        <label><input class="checkbox" id="dnFilter" type="checkbox" name="" value="">
                                                            <i class="a-icon a-icon-checkbox"></i>
                                                            <span class="a-label a-checkbox-label">Data di nascita</span>
                                                        </label>
                                                    </div>

                                                </a>
                                            </span>
                                        </li>
                                        <li>
                                            <span class="a-list-item">
                                                <a>
                                                    <div>
                                                        <label><input class="checkbox" id="lnFilter" type="checkbox" name="" value="">
                                                            <i class="a-icon a-icon-checkbox"></i>
                                                            <span class="a-label a-checkbox-label">Luogo di nascita</span>
                                                        </label>
                                                    </div>
                                                </a>
                                            </span>
                                        </li>
                                        <li id="azienda--container">
                                            <span class="a-list-item">
                                                <a>
                                                    <div>
                                                        <label><input class="checkbox" id="aziendaFilter" type="checkbox" name="" value="">
                                                            <i class="a-icon a-icon-checkbox"></i>
                                                            <span id="span--azienda" class="a-label a-checkbox-label">Azienda</span>
                                                        </label>
                                                    </div>
                                                </a>
                                            </span>
                                        </li>
                                        <li id="phone--container">
                                            <span class="a-list-item">
                                                <a>
                                                    <div>
                                                        <label><input class="checkbox" id="phoneFilter" type="checkbox" name="" value="">
                                                            <i class="a-icon a-icon-checkbox"></i>
                                                            <span class="a-label a-checkbox-label">N.di telefono</span>
                                                        </label>
                                                    </div>
                                                </a>
                                            </span>
                                        </li>
                                    </ul>
                                </div>
                                <div>
                                    <ul id="myUL1" class="no-bullets">

                                        <li id="attivita--container">
                                            <span class="a-list-item">
                                                <a>
                                                    <div>
                                                        <label><input class="checkbox" id="attivitaFilter" type="checkbox" name="" value="">
                                                            <i class="a-icon a-icon-checkbox"></i>
                                                            <span class="a-label a-checkbox-label">Attivita</span>
                                                        </label>
                                                    </div>
                                                </a>
                                            </span>
                                        </li>
                                        <li id="azione--container">
                                            <span class="a-list-item">
                                                <a>
                                                    <div>
                                                        <label><input class="checkbox" id="azioneFilter" type="checkbox" name="" value="">
                                                            <i class="a-icon a-icon-checkbox"></i>
                                                            <span id="span--azione" class="a-label a-checkbox-label">Azione</span>
                                                        </label>
                                                    </div>
                                                </a>
                                            </span>
                                        </li>
                                        <li id="ip--container">
                                            <span class="a-list-item">
                                                <a>
                                                    <div>
                                                        <label><input class="checkbox" id="ipFilter" type="checkbox" name="" value="">
                                                            <i class="a-icon a-icon-checkbox"></i>
                                                            <span class="a-label a-checkbox-label">IP</span>
                                                        </label>
                                                    </div>
                                                </a>
                                            </span>
                                        </li>
                                        <li id="orarioA--container">
                                            <span class="a-list-item">
                                                <a>
                                                    <div>
                                                        <label><input class="checkbox" id="orarioAFilter" type="checkbox" name="" value="">
                                                            <i class="a-icon a-icon-checkbox"></i>
                                                            <span id="orarioA--span" class="a-label a-checkbox-label">Ora accesso</span>
                                                        </label>
                                                    </div>
                                                </a>
                                            </span>
                                        </li>
                                        <li id="orarioL--container">
                                            <span class="a-list-item">
                                                <a>
                                                    <div>
                                                        <label><input class="checkbox" id="orarioLFilter" type="checkbox" name="" value="">
                                                            <i class="a-icon a-icon-checkbox"></i>
                                                            <span id="orarioL--span" class="a-label a-checkbox-label">Ora uscita</span>
                                                        </label>
                                                    </div>
                                                </a>
                                            </span>
                                        </li>
                                        <li id="tc--container">
                                            <span class="a-list-item">
                                                <a>
                                                    <div>
                                                        <label><input class="checkbox" id="tcFilter" type="checkbox" name="" value="">
                                                            <i class="a-icon a-icon-checkbox"></i>
                                                            <span class="a-label a-checkbox-label">Tempo cumulato</span>
                                                        </label>
                                                    </div>
                                                </a>
                                            </span>
                                        </li>
                                        <li id="ruolo--container">
                                            <span class="a-list-item">
                                                <a>
                                                    <div>
                                                        <label><input class="checkbox" id="ruoloFilter" type="checkbox" name="" value="">
                                                            <i class="a-icon a-icon-checkbox"></i>
                                                            <span class="a-label a-checkbox-label">Ruolo</span>
                                                        </label>
                                                    </div>
                                                </a>
                                            </span>
                                        </li>
                                        <li id="corso--container">
                                            <span class="a-list-item">
                                                <a>
                                                    <div>
                                                        <label><input class="checkbox" id="corsoFilter" type="checkbox" name="" value="">
                                                            <i class="a-icon a-icon-checkbox"></i>
                                                            <span class="a-label a-checkbox-label">Corso</span>
                                                        </label>
                                                    </div>
                                                </a>
                                            </span>
                                        </li>
                                        <li id="aula--container">
                                            <span class="a-list-item">
                                                <a>
                                                    <div>
                                                        <label><input class="checkbox" id="aulaFilter" type="checkbox" name="" value="">
                                                            <i class="a-icon a-icon-checkbox"></i>
                                                            <span class="a-label a-checkbox-label">Aula</span>
                                                        </label>
                                                    </div>
                                                </a>
                                            </span>
                                        </li>
                                        <li id="lezione--container">
                                            <span class="a-list-item">
                                                <a>
                                                    <div>
                                                        <label><input class="checkbox" id="lezioneFilter" type="checkbox" name="" value="">
                                                            <i class="a-icon a-icon-checkbox"></i>
                                                            <span class="a-label a-checkbox-label">Lezione</span>
                                                        </label>
                                                    </div>
                                                </a>
                                            </span>
                                        </li>
                                        <li id="ts--container">
                                            <span class="a-list-item">
                                                <a>
                                                    <div>
                                                        <label><input class="checkbox" id="tsFilter" type="checkbox" name="" value="">
                                                            <i class="a-icon a-icon-checkbox"></i>
                                                            <span id="ts--span"class="a-label a-checkbox-label">Tempo sessione</span>
                                                        </label>
                                                    </div>
                                                </a>
                                            </span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>-->
        </div>
        <div style="margin-right: 7.5%;margin-left: 7.5%;margin-bottom: 7.5%;margin-top: 3%">

            <table id="report" class="table table-striped table-bordered" cellspacing="0" style="width:100%;">

                <thead id="report_head">
                    <tr>
                        <th>Report</th>

                    </tr>
                </thead>

                <tbody id="report_body">

                </tbody>

            </table>
        </div>

        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Scegli una piattaforma</h5>
                    </div>
                    <div class="modal-body">
                        <select class="form-select" name="course" id="selectPlatform"
                                aria-label="Floating label select example" >
                            <option value="-1" disabled selected>Seleziona...</option>
                            <?php
                            foreach ($array_piattaforma as $key => $value) {
                                echo $key . ' - ' . $value;
                                if ($key == 4) {
                                    ?>
                                    <option value="<?php echo $key; ?>" disabled><?php echo $value; ?></option>
                                <?php } else { ?>
                                    <option value="<?php echo $key; ?>" ><?php echo $value; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </div>

                </div>
            </div>
        </div>

        <div class="modal fade" id="modal--bb--course"  aria-labelledby="modal--bb--course--title" aria-hidden="true">
            <div class="modal-dialog ">
                <div class="modal-content">

                    <div class="modal-header">
                        <h5 class="modal-title" id="modal--bb--course--title">Inserisci i campi</h5>
                    </div>
                    <div class="modal-body">

                        <div class="mb-2">
                            <label for="data--modal--bb--course" >Data</label>
                            <input type="date" class="form-control mb-3" id="data--modal--bb--course" min="2021-01-01" value="2021-09-01" name="data_modal">
                            <label for="name--modal--bb--course">Nome Cognome</label>
                            <select class="js-example-basic-single js-states form-control mb-3" id="name--modal--bb--course" ></select>
                        </div>
                        <div class="mb-2">
                            <label for="corso--modal--bb--course">Corso</label>
                            <select class="js-example-basic-single js-states form-control mb-3" id="corso--modal--bb--course" name="corso_modal"></select>
                            <label for="aula--modal--bb--course">Nome Aula</label>
                            <select class="js-example-basic-single js-states form-control mb-3" id="aula--modal--bb--course" name="aula_modal"></select>
                        </div>
                        <div class="mb-2">
                            <label for="action--modal--bb--course">Orario</label>
                            <select class="form-select mb-3" aria-label="Floating label select example" id="action--modal--bb--course" name="action_modal">
                                <option value="seleziona" selected>Seleziona...</option>
                                <option value="joined">Ingresso</option>
                                <option value="left">Uscita</option>
                            </select>
                            <input type="time" class="form-control mb-3" id="orario--modal--bb--course"  value="00:00" name="orario_modal">
                        </div>
                    </div>
                    <div class="modal-footer justify-content-center">
                        <a type="button" class="btn btn-outline-danger waves-effect" data-dismiss="modal" id="annulla--bb--course" onclick="closeModal('bbbName')">Annulla</a>
                        <input type="submit" class="btn btn-primary" name="aggiungi_modal" id="aggiungi--bb--course" value="Aggiungi" onclick="return addLog('bbbName')">
                    </div>

                </div>

            </div>
        </div>


        <div class="modal fade" id="modal--sintesi"  aria-labelledby="modal--sintesi--title" aria-hidden="true">
            <div class="modal-dialog ">
                <div class="modal-content">

                    <div class="modal-header">
                        <h5 class="modal-title" id="modal--sintesi--title">Inserisci i campi</h5>
                    </div>
                    <div class="modal-body">

                        <div class="mb-2">
                            <label for="data--modal--sintesi" >Data</label>
                            <input type="date" class="form-control mb-3" id="data--modal--sintesi" min="2021-01-01" value="2021-09-01" name="data_modal">
                            <label for="name--modal--sintesi">Nome Cognome</label>
                            <select class="js-example-basic-single js-states form-control mb-3" id="name--modal--sintesi" ></select>
                        </div>
                        <div class="mb-2">
                            <label for="action--modal--sintesi">Orario</label>
                            <select class="form-select mb-3" aria-label="Floating label select example" id="action--modal--sintesi" name="action_modal">
                                <option value="seleziona" selected>Seleziona...</option>
                                <option value="loggedin">Login</option>
                                <option value="loggedout">Loggedout</option>
                            </select>
                            <input type="time" class="form-control mb-3" id="orario--modal--sintesi"  value="00:00" name="orario_modal">
                        </div>
                    </div>
                    <div class="modal-footer justify-content-center">
                        <a type="button" class="btn btn-outline-danger waves-effect" data-dismiss="modal" id="annulla--sintesi" onclick="closeModal('sintesi')">Annulla</a>
                        <input type="submit" class="btn btn-primary" name="aggiungi_modal" id="aggiungi--sintesi" value="Aggiungi" onclick="return addLog('sintesi')">
                    </div>
                </div>
            </div>
        </div>
    </body>

</html>
