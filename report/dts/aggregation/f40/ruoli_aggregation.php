<?php

require '/home/admin/public_html/vendor/autoload.php';

// Primo ad essere lanciato

$db = "f40";
$conn = new MongoDB\Client(
        "mongodb+srv://jobtek:TimeVision2021%40%21%3F@timevision.t9ed5.mongodb.net/TimeVision?retryWrites=true&w=majority");

$collection = $conn->$db->mdl_role_assignments;

$pipeline = [
    [
        '$lookup' => [
            'from' => 'mdl_role',
            'localField' => 'roleid',
            'foreignField' => 'id',
            'as' => 'ruolo'
        ]
    ], [
        '$project' => [
            'userid' => 1,
            'ruolo' => [
                '$arrayElemAt' => [
                    '$ruolo.shortname', 0
                ]
            ]
        ]
    ], [
        '$group' => [
            '_id' => [
                'userid' => '$userid'
            ],
            'ruolo' => [
                '$addToSet' => '$ruolo'
            ]
        ]
    ], [
        '$project' => [
            'userid' => '$_id.userid',
            'ruolo' => [
                '$arrayElemAt' => [
                    '$ruolo', 0
                ]
            ],
            '_id' => 0
        ]
    ], [
        '$sort' => [
            'userid' => 1
        ]
    ], [
        '$out' => 'corsi_ruoli'
    ]
];

$out = $collection->aggregate($pipeline);



