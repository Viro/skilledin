<?php

require '/home/admin/public_html/vendor/autoload.php';

$db = "fnc";
$conn = new MongoDB\Client(
        "mongodb+srv://jobtek:TimeVision2021%40%21%3F@timevision.t9ed5.mongodb.net/TimeVision?retryWrites=true&w=majority");

$data = strtotime("-6 month");
$collection = $conn->$db->mdl_utenti_complete;
$pipeline = [[
'$match' => [
    'azienda' => ['$exists' => true, '$ne' => "", '$ne' => null]
]
    ],
    ['$lookup' => [
            'from' => 'mdl_logstore_standard_log',
            'localField' => 'userid',
            'foreignField' => 'userid',
            'as' => 'log'
        ]
    ], [
        '$unwind' => [
            'path' => '$log',
            'preserveNullAndEmptyArrays' => false
        ]
    ], [
        '$match' => [
            'log.timecreated' => ['$gte' => $data],
            'log.courseid' => ['$ne' => 0]
        ]
    ], [
        '$project' => [
            'idCorso' => '$log.courseid',
            'azienda' => 1
        ]
    ], [
        '$group' => [
            '_id' => '$idCorso',
            'aziende' => [
                '$addToSet' => '$azienda'
            ]
        ]
        ],
    [
        '$lookup'=> [
            'from' => 'mdl_course',
            'localField' => '_id',
            'foreignField' => 'id',
            'as' => 'fullname'
        ]
    ],
    [
        '$addFields' =>[
            'fullname' => [
                '$arrayElemAt' =>['$fullname.fullname',0]
            ]
        ]
    ], [
        '$out' => 'corsiAssociati'
    ]
    ];
$out = $collection->aggregate($pipeline);


