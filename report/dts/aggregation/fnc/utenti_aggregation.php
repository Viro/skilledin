<?php


require '/home/admin/public_html/vendor/autoload.php';

// Secondo ad essere lanciato


$db = "fnc";
$conn = new MongoDB\Client(
        "mongodb+srv://jobtek:TimeVision2021%40%21%3F@timevision.t9ed5.mongodb.net/TimeVision?retryWrites=true&w=majority");

$collection = $conn->$db->mdl_user;
$pipeline = [
    [
        '$project' => [
            'id' => 1,
            'username' => 1,
            'idnumber' => 1,
            'nome' => [
                '$concat' => [
                    '$firstname', ' ', '$lastname'
                ]
            ],
            'email' => 1,
            'phone1' => 1,
            'phone2' => 1
        ]
    ], [
        '$lookup' => [
            'from' => 'mdl_user_info_data',
            'localField' => 'id',
            'foreignField' => 'userid',
            'as' => 'info'
        ]
    ], [
        '$unwind' => [
            'path' => '$info',
            'preserveNullAndEmptyArrays' => False
        ]
    ], [
        '$match' => [
            '$or' => [
                [
                    'info.fieldid' => [
                        '$eq' => 1
                    ]
                ], [
                    'info.fieldid' => [
                        '$eq' => 15
                    ]
                ], [
                    'info.fieldid' => [
                        '$eq' => 18
                    ]
                ]
            ]
        ]
    ], [
        '$sort' => [
            'info.fieldid' => 1
        ]
    ], [
        '$group' => [
            '_id' => [
                'userid' => '$id',
                'username' => '$username',
                'email' => '$email',
                'nome' => '$nome',
                'phone1' => '$phone1',
                'phone2' => '$phone2',
                'idnumber' => '$idnumber'
            ],
            'dati' => [
                '$push' => '$info.data'
            ]
        ]
    ], [
        '$set' => [
            'azienda' => [
                '$arrayElemAt' => [
                    '$dati', 0
                ]
            ],
            'citta' => [
                '$arrayElemAt' => [
                    '$dati', 2
                ]
            ],
            'data' => [
                '$arrayElemAt' => [
                    '$dati', 1
                ]
            ]
        ]
    ], [
        '$project' => [
            'dati' => 0
        ]
    ], [
        '$project' => [
            'userid' => '$_id.userid',
            'username' => '$_id.username',
            'idnumber' => '$_id.idnumber',
            'email' => '$_id.email',
            'nome' => '$_id.nome',
            'phone1' => '$_id.phone1',
            'phone2' => '$_id.phone2',
            'citta' => 1,
            'data' => 1,
            'azienda' => [
                '$trim'=>[
                    'input'=>'$azienda'
                ]
            ],
            '_id' => 0
        ]
    ], [
        '$lookup' => [
            'from' => 'corsi_ruoli',
            'localField' => 'userid',
            'foreignField' => 'userid',
            'as' => 'ruoli'
        ]
    ], [
        '$unwind' => [
            'path' => '$ruoli'
        ]
    ], [
        '$set' => [
            'ruolo' => '$ruoli.ruolo'
        ]
    ], [
        '$project' => [
            'ruoli' => 0
        ]
    ], [
        '$out' => 'mdl_utenti_complete'
    ]
];
$out = $collection->aggregate($pipeline);


