<?php


require '/home/admin/public_html/vendor/autoload.php';

$db = "formaz";
$conn = new MongoDB\Client(
        "mongodb+srv://jobtek:TimeVision2021%40%21%3F@timevision.t9ed5.mongodb.net/TimeVision?retryWrites=true&w=majority");

$collection = $conn->$db->mdl_hvp;
$pipeline = [
    [
        '$project'=> [
            'course'=> 1, 
            'json_content'=> [
                '$split'=> [
                    '$json_content', '"time":'
                ]
            ]
        ]
    ], [
        '$project'=> [
            'course'=> 1, 
            'json_content'=> [
                '$split'=> [
                    [
                        '$arrayElemAt'=> [
                            '$json_content', 1
                        ]
                    ], ','
                ]
            ]
        ]
    ], [
        '$project'=> [
            'course'=> 1, 
            'json_content'=> [
                '$split'=> [
                    [
                        '$arrayElemAt'=> [
                            '$json_content', 0
                        ]
                    ], '.'
                ]
            ]
        ]
    ], [
        '$project'=> [
            'course'=> 1, 
            'json_content'=> [
                '$toInt'=> [
                    '$arrayElemAt'=> [
                        '$json_content', 0
                    ]
                ]
            ]
        ]
    ], [
        '$group'=> [
            '_id'=> '$course', 
            'totalSeconds'=> [
                '$sum'=> '$json_content'
            ]
        ]
    ], [
        '$out'=> 'secondsCourse'
    ]
];
$out = $collection->aggregate($pipeline);


