<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
ini_set('memory_limit', '-1');

error_reporting(E_ALL);

require '/home/admin/public_html/vendor/autoload.php';
//require '../../../../../vendor/autoload.php';

//solo live e new

$db = "live";
$conn = new MongoDB\Client(
        "mongodb+srv://jobtek:TimeVision2021%40%21%3F@timevision.t9ed5.mongodb.net/TimeVision?retryWrites=true&w=majority");

$collection = $conn->$db->mdl_zoom_meeting_participants_new;
$pipeline = [
    [
        '$lookup'=> [
            'from'=> 'mdl_zoom_meeting_details',
            'localField'=> 'detailsid',
            'foreignField'=> 'id',
            'as'=> 'meeting'
        ]
    ], [
        '$unwind'=> [
            'path'=> '$meeting',
            'preserveNullAndEmptyArrays'=> False
        ]
    ], [
        '$lookup'=> [
            'from'=> 'mdl_zoom',
            'localField'=> 'meeting.zoomid',
            'foreignField'=> 'id',
            'as'=> 'zoom'
        ]
    ], [
        '$set'=> [
            'name'=> [
                '$arrayElemAt'=> [
                    '$zoom.name', 0
                ]
            ],
            'idCorso'=> [
                '$arrayElemAt'=> [
                    '$zoom.course', 0
                ]
            ]
        ]
    ], [
        '$project'=> [
            'meeting'=> 0,
            'zoom'=> 0
        ]
    ], [
        '$lookup'=> [
            'from'=> 'mdl_course',
            'localField'=> 'idCorso',
            'foreignField'=> 'id',
            'as'=> 'corso'
        ]
    ], [
        '$set'=> [
            'nomeCorso'=> [
                '$arrayElemAt'=> [
                    '$corso.fullname', 0
                ]
            ]
        ]
    ], [
        '$project'=> [
            'detailsid'=> 0,
            'corso'=> 0
        ]
    ], [
        '$out'=> 'mdl_zoom_meeting_participants_aggregate_new'
    ]
];
$out = $collection->aggregate($pipeline);


