<?php

ini_set('memory_limit', '2048M');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
ini_set('max_execution_time', 0);
error_reporting(E_ALL);

require '../../../../../vendor/autoload.php';


$db = "fnc";
$connection = new MongoDB\Client(
        "mongodb+srv://jobtek:TimeVision2021%40%21%3F@timevision.t9ed5.mongodb.net/TimeVision?retryWrites=true&w=majority");

$collection = $connection->$db->mdl_logstore_standard_log;
$pipeline = [
    [
        '$match' => [
            'component' => 'mod_hvp',
            'action' => [
                '$ne' => 'submitted'
            ]
        ]
    ], [
        '$lookup' => [
            'from' => 'mdl_utenti_complete',
            'localField' => 'userid',
            'foreignField' => 'userid',
            'as' => 'info'
        ]
    ], [
        '$group' => [
            '_id' => [
                'userid' => '$userid',
                'timecreated' => '$timecreated'
            ],
            'doppione' => [
                '$sum' => 1
            ]
        ]
    ], [
        '$match' => [
            'doppione' => [
                '$gt' => 1
            ]
        ]
    ], [
        '$sort' => [
            'doppione' => 1
        ]
    ]
];
$out = $collection->aggregate($pipeline);
$res = $out->toArray();

foreach ($res[1] as $value) {

    $cursore = $collection->find(
            ['userid' => (int) $value->userid, 'timecreated' => (int) $value->timecreated, 'component' => 'mod_hvp'],
            ['projection' => ['_id' => 0]]
    );
    $prov = null;
    foreach ($cursore as $elem) {
        if ($prov == null) {
            $prov = $elem;
            continue;
        }
        if ($elem['objectid'] != $prov['objectid']) {
            if ($elem['objectid'] > $prov['objectid'])
                $id = $elem['id'];
            else
                $id = $prov['id'];

            $collection->updateOne(
                    ['id' => (int) $id],
                    ['$inc' => ['timecreated' => 1]]
            );
            $prov = null;
            continue;
        }
        $prov = $elem;
    }
}

exit("fine");

