<?php
ini_set('memory_limit', '2048M');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
ini_set('max_execution_time', 600);
error_reporting(E_ALL);

require '/home/admin/public_html/vendor/autoload.php';
//require '../../../../../vendor/autoload.php';

$servername = "77.39.212.58:3306";
$username = "fnccorsi_fncmoodle";
$password = "CWo{}=+CWqU&";
$dbname = "fnccorsi_fnc";
$db ="fnc";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) die("Connection failed: " . $conn->connect_error);

//Connessione a mongodb
$connection = new MongoDB\Client("mongodb+srv://jobtek:TimeVision2021%40%21%3F@timevision.t9ed5.mongodb.net/TimeVision?retryWrites=true&w=majority");
$collection = $connection->$db->mdl_aziende_piani_formativi;

$sql = "SELECT * FROM mdl_aziende_piani_formativi";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        try {
            $mongo_id = 0;
            $check    = $collection->find(['id' => (int) $row['id']]);
            foreach ($check as $item) {
                $mongo_id = $item['id'];
            }
            if($mongo_id > 0){
                $collection->updateOne(
                    ['id'   => (int) $row["id"]],
                    ['$set' => ['id'                => (int) $row["id"]                      ,
                                'id_corso'          => (int) $row["id_corso"]                ,
                                'nome_azienda'      => utf8_encode($row["nome_azienda"])     ,
                                'piva_cf'           => utf8_encode($row["piva_cf"])          ,
                                'piano'             => utf8_encode($row["piano"])            ,
                                'data_inizio_piano' => utf8_encode($row["data_inizio_piano"]),
                                'data_fine_piano'   => utf8_encode($row["data_fine_piano"])   ]]
                );
            }else{
                $collection->insertOne(['id'                => (int) $row["id"]                      ,
                                        'id_corso'          => (int) $row["id_corso"]                ,
                                        'nome_azienda'      => utf8_encode($row["nome_azienda"])     ,
                                        'piva_cf'           => utf8_encode($row["piva_cf"])          ,
                                        'piano'             => utf8_encode($row["piano"])            ,
                                        'data_inizio_piano' => utf8_encode($row["data_inizio_piano"]),
                                        'data_fine_piano'   => utf8_encode($row["data_fine_piano"])   ]);
            }
        }catch (Exception $e){
            echo $e;
        }
    }
}
$conn->close();

