<?php

ini_set('memory_limit', '2048M');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require '/home/admin/public_html/vendor/autoload.php';


$servername = "77.39.212.58:3306";
$username = "fnccorsi_fncmoodle";
$password = "CWo{}=+CWqU&";
$dbname = "fnccorsi_fnc";
$db ="fnc";

// Create connection a mysql
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}


//Connessione a mongodb
$connection = new MongoDB\Client(
    "mongodb+srv://jobtek:TimeVision2021%40%21%3F@timevision.t9ed5.mongodb.net/TimeVision?retryWrites=true&w=majority");

$collection = $connection->$db->mdl_hvp;
$pivot = $connection->$db->pivot;

$data = date("Y-m-d");
$orario = date("H:i:s");

$cursor = $pivot->find(
    ['collection' => "mdl_hvp"],
    ['projection' => ['id' => 1, 'data' => 1, 'orario' => 1],
        'sort' => ['data' => -1, 'orario' => -1], 'limit' => 1]
);
$id = 0;
$mongo_course = 0;
foreach ($cursor as $singolo) {
    $id = (int) $singolo['id'];

    $data2 = $singolo['data'];
    $ora = $singolo['orario'];

    $strTimestamp = strtotime("$data2 $ora");
}

//tool csv
//https://retool.com/utilities/comma-separating-tool

//$csv = "'302','303','305','401','403','405','406','407','408','409','410','413','415','416','418','420','422','423','426','427','565','566','421','424','425','428','429','430','431','432','433','434','435','436','440','441','509','510','511','512','513','514','515','442','446','443','516','517','447','450','449','535','536','451','452','611','612','613','453','595','596','597','454','455','457','458','459','488','489','491','492','493','495','497','498','500','460','461','463','465','592','593','594','466','467','661','664','665','668','670','473','474','475','476','477','478','479','480','481','482','483','484','485','487','521','527','490','496','499','501','529','530','531','532','533','534','537','539','540','541','542','543','544','545','546','547','548','549','550','551','552','553','555','556','557','558','559','560','561','564','567','568','569','570','571','572','573','576','577','578','579','580','581','582','583','584','585','586','587','588','589','590','591','599','600','601','602','603','604','605','606','607','608','609','610','614','615','616','617','618','619','620','621','622','623','639','640','641','642','643','644','645','646','647','648','649','650','651','652','653','654','655','656','657','658','659','660','662','663','666','667','669','671','672','673','674','675','306','307','311','313','316','340','393','394','417','419','395','412','414','574','575','631','632','633','634','635','636','637','638','683','684','685','438','439','503','504','505','506','507','508','444','518','519','520','468','469','470','471','624','625','626','627','628','629','630'";


$sql = "SELECT * FROM mdl_hvp WHERE (timecreated >= $strTimestamp OR timemodified >= $strTimestamp)";
//$sql = "SELECT * FROM mdl_hvp WHERE course = 533 ORDER BY timecreated";
//$sql = "SELECT * FROM mdl_hvp WHERE course IN ($csv) ORDER BY timecreated";
//
//echo $sql; exit;

$result = $conn->query($sql);
if ($result->num_rows > 0) {

    while ($row = $result->fetch_assoc()) {

        try {

            $check = $collection->find([
                'id' => (int) $row['id']
            ]);

            foreach ($check as $c) {
                $mongo_id = $c['id'];
            }

            echo $row['id'] . "\n";

            if($mongo_id > 0){

                if($row["id"] == 622){
                    echo utf8_encode($row["name"]);
                }

                $collection->updateOne(
                    ['id' => (int) $row["id"]],
                    [
                        '$set' => [
                            'id' => (int) $row["id"],
                            'name' => utf8_encode($row["name"]),
                            'course' => (int) $row["course"],
                            'json_content' => utf8_encode($row["json_content"]),
                            'timecreated' => (int) $row["timecreated"],
                            'timemodified' => (int) $row["timemodified"],
                            'filtered' => utf8_encode($row["filtered"]),
                            'slug' => $row["slug"]
                        ]
                    ],
                    ['upsert' => true]
                );

            }else{
                $collection->insertOne([
                    'id' => (int) $row["id"],
                    'name' => utf8_encode($row["name"]),
                    'course' => (int) $row["course"],
                    'json_content' => utf8_encode($row["json_content"]),
                    'timecreated' => (int) $row["timecreated"],
                    'timemodified' => (int) $row["timemodified"],
                    'filtered' => utf8_encode($row["filtered"]),
                    'slug' => $row["slug"]
                ]);
            }
        } catch (Exception $e) {
            echo $e;
        }


    }

    $pivot->insertOne([
        'id' => 0,
        'collection' => "mdl_hvp",
        'data' => $data,
        'orario' => $orario
    ]);
}



$conn->close();

