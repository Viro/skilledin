<?php

ini_set('memory_limit', '2048M');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require '/home/admin/public_html/vendor/autoload.php';
//require '../../../../../vendor/autoload.php';


$servername = "77.39.212.60:3306";
$username = "formazio_forma40";
//$password = "#1qgMl@#BbS#";
$password = "uh$^^?YFM2l6";
$dbname = "formazio_formazione40";
$db = "f40";

// Create connection a mysql
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}


//Connessione a mongodb
$connection = new MongoDB\Client(
        "mongodb+srv://jobtek:TimeVision2021%40%21%3F@timevision.t9ed5.mongodb.net/TimeVision?retryWrites=true&w=majority");

$collection = $connection->$db->mdl_hvp;
$pivot = $connection->$db->pivot;

$data = date("Y-m-d");
$orario = date("H:i:s");

$cursor = $pivot->find(
        ['collection' => "mdl_hvp"],
        ['projection' => ['id' => 1, 'data' => 1, 'orario' => 1],
            'sort' => ['data' => -1, 'orario' => -1], 'limit' => 1]
);
$id = 0;
$mongo_course = 0;
foreach ($cursor as $singolo) {
    $id = (int) $singolo['id'];

    $data2 = $singolo['data'];
    $ora = $singolo['orario'];

    $strTimestamp = strtotime("$data2 $ora");
}


$sql = "SELECT * FROM mdl_hvp WHERE (timecreated >= $strTimestamp OR timemodified >= $strTimestamp)";
//$sql = "SELECT * FROM mdl_hvp ORDER BY timecreated";
$result = $conn->query($sql);
if ($result->num_rows > 0) {

    while ($row = $result->fetch_assoc()) {

        try {

            $check = $collection->find([
                'id' => (int) $row['id']
            ]);

            foreach ($check as $c) {
                $mongo_id = $c['id'];
            }

            echo $row['id'] . "\n";

            if($mongo_id > 0){

                if($row["id"] == 622){
                    echo utf8_encode($row["name"]);
                }

                $collection->updateOne(
                    ['id' => (int) $row["id"]],
                    [
                        '$set' => [
                            'id' => (int) $row["id"],
                            'name' => utf8_encode($row["name"]),
                            'course' => (int) $row["course"],
                            'json_content' => utf8_encode($row["json_content"]),
                            'timecreated' => (int) $row["timecreated"],
                            'timemodified' => (int) $row["timemodified"],
                            'filtered' => utf8_encode($row["filtered"]),
                            'slug' => $row["slug"]
                        ]
                    ],
                    ['upsert' => true]
                );

            }else{
                $collection->insertOne([
                    'id' => (int) $row["id"],
                    'name' => utf8_encode($row["name"]),
                    'course' => (int) $row["course"],
                    'json_content' => utf8_encode($row["json_content"]),
                    'timecreated' => (int) $row["timecreated"],
                    'timemodified' => (int) $row["timemodified"],
                    'filtered' => utf8_encode($row["filtered"]),
                    'slug' => $row["slug"]
                ]);
            }
        } catch (Exception $e) {
            echo $e;
        }


    }

    $pivot->insertOne([
        'id' => 0,
        'collection' => "mdl_hvp",
        'data' => $data,
        'orario' => $orario
    ]);
}



$conn->close();

