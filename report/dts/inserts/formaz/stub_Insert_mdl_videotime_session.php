<?php

ini_set('memory_limit', '2048M');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require '/home/admin/public_html/vendor/autoload.php';
//require '../../../../../vendor/autoload.php';


$servername = "77.39.209.12:3306";
$username = "formazionecorsin_corsi";
$password = "W~Q_NPp20X&y";
$dbname = "formazionecorsin_corsi";
$db = "formaz";

// Create connection a mysql
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}


//Connessione a mongodb
$connection = new MongoDB\Client(
    "mongodb+srv://jobtek:TimeVision2021%40%21%3F@timevision.t9ed5.mongodb.net/TimeVision?retryWrites=true&w=majority");

$collection = $connection->$db->mdl_videotime_session;
//AGGIORNAMENTO DI TUTTI I LOG IN STATO ZERO (Nuove sessioni o avanzamento di sessione)
$timeupdate = time();
//$sql = "SELECT * FROM mdl_videotime_session WHERE `state` = 0 ORDER BY id";
//Solo prima esecuzione
$sql = "SELECT * FROM mdl_videotime_session ORDER BY id";
$result = $conn->query($sql);
    if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        try {
            $mongo_id = 0;
            $check = $collection->find([
                'id' => (int) $row['id']
            ]);

            foreach ($check as $c) {
                $mongo_id = $c['id'];
            }

            echo $row['id'] . "\n";

            if($mongo_id > 0){
                $collection->updateOne(
                    ['id' => (int) $row["id"]],
                    [
                        '$set' => [
                            'id'                 => (int) $row["id"],
                            'module_id'          => (int) $row["module_id"],
                            'user_id'            => (int) $row["user_id"],
                            'time'               => (int) $row["time"],
                            'timestarted'        => (int) $row["timestarted"],
                            'state'              => (int) $row["state"],
                            'percent_watch'      => (float) $row["percent_watch"],
                            'current_watch_time' => (float) $row["current_watch_time"],
                            'timeupdated'        => (int) $timeupdate
                        ]
                    ],
                    ['upsert' => true]
                );

            }else{
                $collection->insertOne([
                    'id'                 => (int) $row["id"],
                    'module_id'          => (int) $row["module_id"],
                    'user_id'            => (int) $row["user_id"],
                    'time'               => (int) $row["time"],
                    'timestarted'        => (int) $row["timestarted"],
                    'state'              => (int) $row["state"],
                    'percent_watch'      => (float) $row["percent_watch"],
                    'current_watch_time' => (float) $row["current_watch_time"],
                    'timeupdated'        => (int) $timeupdate
                ]);
            }
        } catch (Exception $e) {
            echo $e;
        }
    }
}

//Aggiorno tutti i record precedenti al timestamp
$cursor = $collection->find(
    ['collection' => "mdl_videotime_session"],
    ['projection' => ['id' => 1]],
    ['match' => ['state' => 0, 'timeupdate' => ['$lte' => $timeupdate]]]
);
$id = 0;
foreach ($cursor as $singolo) {
    $id = (int) $singolo['id'];
    $sql = "SELECT * FROM mdl_videotime_session WHERE `id` = $id";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            try {
                    $collection->updateOne(
                        ['id' => (int) $row["id"]],
                        [
                            '$set' => [
                                'id'                 => (int) $row["id"],
                                'module_id'          => (int) $row["module_id"],
                                'user_id'            => (int) $row["user_id"],
                                'time'               => (int) $row["time"],
                                'timestarted'        => (int) $row["timestarted"],
                                'state'              => (int) $row["state"],
                                'percent_watch'      => (float) $row["percent_watch"],
                                'current_watch_time' => (float) $row["current_watch_time"],
                                'timeupdated'        => (int) $timeupdate
                            ]
                        ],
                        ['upsert' => true]
                    );
            } catch (Exception $e) {
                echo $e;
            }
        }
    }


}


$conn->close();

