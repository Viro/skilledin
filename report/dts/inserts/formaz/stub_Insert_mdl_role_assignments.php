<?php

ini_set('memory_limit', '2048M');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
ini_set('max_execution_time', 600);
error_reporting(E_ALL);

require '/home/admin/public_html/vendor/autoload.php';
//require '../../../../../vendor/autoload.php';


$servername = "77.39.209.12:3306";
$username = "formazionecorsin_corsi";
$password = "W~Q_NPp20X&y";
$dbname = "formazionecorsin_corsi";
$db = "formaz";

// Create connection a mysql
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}


//Connessione a mongodb
$connection = new MongoDB\Client(
        "mongodb+srv://jobtek:TimeVision2021%40%21%3F@timevision.t9ed5.mongodb.net/TimeVision?retryWrites=true&w=majority");

$collection = $connection->$db->mdl_role_assignments;
$pivot = $connection->$db->pivot;

$data = date("Y-m-d");
$orario = date("H:i:s");

$cursor = $pivot->find(
    ['collection' => "mdl_role_assignments"],
    ['projection' => ['id' => 1, 'data' => 1, 'orario' => 1],
        'sort' => ['data' => -1, 'orario' => -1], 'limit' => 1]
);

$id = 0;
//foreach ($cursor as $singolo) {
//    $id = (int) $singolo['id'];
//}

if($cursor) {
    foreach ($cursor as $singolo) {
        $id = (int) $singolo['id'];

        $data2 = $singolo['data'];
        $ora = $singolo['orario'];

        $strTimestamp = strtotime("$data2 $ora");
    }
}

$sql = "SELECT * FROM mdl_role_assignments WHERE timemodified >= $strTimestamp";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {

        try {

            $check = $collection->find([
                'id' => (int) $row['id']
            ]);

            foreach ($check as $c) {
                $mongo_row = $c['id'];
            }

            if($mongo_row > 0){
                $collection->updateOne(
                    ['id' => (int) $row["id"]],
                    [
                        '$set' => [
                            'id' => (int) $row["id"],
                            'roleid' => (int) $row["roleid"],
                            'contextid' => (int) $row["contextid"],
                            'userid' => (int) $row["userid"],
                            'timemodified' => (int) $row["timemodified"],
                            'modifierid' => (int) $row["modifierid"],
                            'itemid' => (int) $row["itemid"],
                        ]
                    ],
                    ['upsert' => true]
                );
            }else{
                $collection->insertOne([
                    'id' => (int) $row["id"],
                    'roleid' => (int) $row["roleid"],
                    'contextid' => (int) $row["contextid"],
                    'userid' => (int) $row["userid"],
                    'timemodified' => (int) $row["timemodified"],
                    'modifierid' => (int) $row["modifierid"],
                    'itemid' => (int) $row["itemid"],
                ]);
            }

        } catch (Exception $e) {
            echo $e;
        }

    }

    $pivot->insertOne([
        'id' => (int) $row["id"],
        'collection' => "mdl_role_assignments",
        'data' => $data,
        'orario' => $orario
    ]);

}


$conn->close();

