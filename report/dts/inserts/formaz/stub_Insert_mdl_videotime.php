<?php

ini_set('memory_limit', '2048M');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
ini_set('max_execution_time', 0);
error_reporting(E_ALL);

require '/home/admin/public_html/vendor/autoload.php';
//require '../../../../../vendor/autoload.php';


$servername = "77.39.209.12:3306";
$username = "formazionecorsin_corsi";
$password = "W~Q_NPp20X&y";
$dbname = "formazionecorsin_corsi";
$db = "formaz";

$conn = new mysqli($servername, $username, $password, $dbname);


if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}


//Connessione a mongodb
$connection = new MongoDB\Client(
    "mongodb+srv://jobtek:TimeVision2021%40%21%3F@timevision.t9ed5.mongodb.net/TimeVision?retryWrites=true&w=majority");

$collection = $connection->$db->mdl_videotime;

//$strTimestamp = '1640995200';
$sql = "SELECT * FROM mdl_videotime";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {

        try {
            $mongo_row = 0;
            $check = $collection->find([
                'id' => (int) $row['id']
            ]);

            foreach ($check as $c) {
                $mongo_row = $c['id'];
            }

            if($mongo_row > 0){
                $collection->updateOne(
                    ['id' => (int) $row["id"]],
                    [
                        '$set' => [
                            'id'                             => (int) $row["id"],
                            'course'                         => (int) $row["course"],
                            'name'                           => utf8_encode($row["name"]),
                            'intro'                          => utf8_encode($row["intro"]),
                            'introformat'                    => (int) $row["introformat"],
                            'vimeo_url'                      => utf8_encode($row["vimeo_url"]),
                            'video_description'              => utf8_encode($row["video_description"]),
                            'video_description_format'       => (int) $row["video_description_format"],
                            'timemodified'                   => (int) $row["timemodified"],
                            'completion_on_view_time'        => (int) $row["completion_on_view_time"],
                            'completion_on_view_time_second' => (int) $row["completion_on_view_time_second"],
                            'completion_on_finish'           => (int) $row["completion_on_finish"],
                            'completion_on_percent'          => (int) $row["completion_on_percent"],
                            'completion_on_percent_value'    => (int) $row["completion_on_percent_value"],
                            'completion_hide_detail'         => (int) $row["completion_hide_detail"],
                            'autoplay'                       => (int) $row["autoplay"],
                            'byline'                         => (int) $row["byline"],
                            'color'                          => utf8_encode($row["color"]),
                            'height'                         => (int) $row["height"],
                            'maxheight'                      => (int) $row["maxheight"],
                            'maxwidth'                       => (int) $row["maxwidth"],
                            'muted'                          => (int) $row["muted"],
                            'playsinline'                    => (int) $row["playsinline"],
                            'portrait'                       => (int) $row["portrait"],
                            'speed'                          => (int) $row["speed"],
                            'title'                          => (int) $row["title"],
                            'transparent'                    => (int) $row["transparent"],
                            'autopause'                      => (int) $row["autopause"],
                            'background'                     => (int) $row["background"],
                            'controls'                       => (int) $row["controls"],
                            'pip'                            => (int) $row["pip"],
                            'dnt'                            => (int) $row["dnt"],
                            'width'                          => (int) $row["width"],
                            'responsive'                     => (int) $row["responsive"],
                            'label_mode'                     => (int) $row["label_mode"],
                            'viewpercentgrade'               => (int) $row["viewpercentgrade"],
                            'next_activity_button'           => (int) $row["next_activity_button"],
                            'next_activity_id'               => (int) $row["next_activity_id"],
                            'next_activity_auto'             => (int) $row["next_activity_auto"],
                            'resume_playback'                => (int) $row["resume_playback"],
                            'preview_picture'                => (int) $row["preview_picture"],
                            'show_description'               => (int) $row["show_description"],
                            'show_description_in_player'     => (int) $row["show_description_in_player"],
                            'show_title'                     => (int) $row["show_title"],
                            'show_tags'                      => (int) $row["show_tags"],
                            'show_duration'                  => (int) $row["show_duration"],
                            'show_viewed_duration'           => (int) $row["show_viewed_duration"],
                            'columns'                        => (int) $row["columns"],
                            'preventfastforwarding'          => (int) $row["preventfastforwarding"],
                            'enabletabs'                     => (int) $row["enabletabs"]
                        ]
                    ],
                    ['upsert' => true]
                );
            }else{
                $collection->insertOne([
                    'id'                             => (int) $row["id"],
                    'course'                         => (int) $row["course"],
                    'name'                           => utf8_encode($row["name"]),
                    'intro'                          => utf8_encode($row["intro"]),
                    'introformat'                    => (int) $row["introformat"],
                    'vimeo_url'                      => utf8_encode($row["vimeo_url"]),
                    'video_description'              => utf8_encode($row["video_description"]),
                    'video_description_format'       => (int) $row["video_description_format"],
                    'timemodified'                   => (int) $row["timemodified"],
                    'completion_on_view_time'        => (int) $row["completion_on_view_time"],
                    'completion_on_view_time_second' => (int) $row["completion_on_view_time_second"],
                    'completion_on_finish'           => (int) $row["completion_on_finish"],
                    'completion_on_percent'          => (int) $row["completion_on_percent"],
                    'completion_on_percent_value'    => (int) $row["completion_on_percent_value"],
                    'completion_hide_detail'         => (int) $row["completion_hide_detail"],
                    'autoplay'                       => (int) $row["autoplay"],
                    'byline'                         => (int) $row["byline"],
                    'color'                          => utf8_encode($row["color"]),
                    'height'                         => (int) $row["height"],
                    'maxheight'                      => (int) $row["maxheight"],
                    'maxwidth'                       => (int) $row["maxwidth"],
                    'muted'                          => (int) $row["muted"],
                    'playsinline'                    => (int) $row["playsinline"],
                    'portrait'                       => (int) $row["portrait"],
                    'speed'                          => (int) $row["speed"],
                    'title'                          => (int) $row["title"],
                    'transparent'                    => (int) $row["transparent"],
                    'autopause'                      => (int) $row["autopause"],
                    'background'                     => (int) $row["background"],
                    'controls'                       => (int) $row["controls"],
                    'pip'                            => (int) $row["pip"],
                    'dnt'                            => (int) $row["dnt"],
                    'width'                          => (int) $row["width"],
                    'responsive'                     => (int) $row["responsive"],
                    'label_mode'                     => (int) $row["label_mode"],
                    'viewpercentgrade'               => (int) $row["viewpercentgrade"],
                    'next_activity_button'           => (int) $row["next_activity_button"],
                    'next_activity_id'               => (int) $row["next_activity_id"],
                    'next_activity_auto'             => (int) $row["next_activity_auto"],
                    'resume_playback'                => (int) $row["resume_playback"],
                    'preview_picture'                => (int) $row["preview_picture"],
                    'show_description'               => (int) $row["show_description"],
                    'show_description_in_player'     => (int) $row["show_description_in_player"],
                    'show_title'                     => (int) $row["show_title"],
                    'show_tags'                      => (int) $row["show_tags"],
                    'show_duration'                  => (int) $row["show_duration"],
                    'show_viewed_duration'           => (int) $row["show_viewed_duration"],
                    'columns'                        => (int) $row["columns"],
                    'preventfastforwarding'          => (int) $row["preventfastforwarding"],
                    'enabletabs'                     => (int) $row["enabletabs"]
                ]);
            }

        } catch (Exception $e) {
            echo $e;
        }
    }
}



$conn->close();

