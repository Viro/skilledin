<?php

ini_set('memory_limit', '2048M');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
ini_set('max_execution_time', 0);
error_reporting(E_ALL);

require '/home/admin/public_html/vendor/autoload.php';
//require '../../../../../vendor/autoload.php';


$servername = "77.39.209.12:3306";
$username = "formazionecorsin_corsi";
$password = "W~Q_NPp20X&y";
$dbname = "formazionecorsin_corsi";
$db = "formaz";

$conn = new mysqli($servername, $username, $password, $dbname);


if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}


//Connessione a mongodb
$connection = new MongoDB\Client(
    "mongodb+srv://jobtek:TimeVision2021%40%21%3F@timevision.t9ed5.mongodb.net/TimeVision?retryWrites=true&w=majority");

$collection = $connection->$db->mdl_course_modules_completion;
$pivot = $connection->$db->pivot;

$data = date("Y-m-d");
$orario = date("H:i:s");
$strTimestamp = 0;

$cursor = $pivot->find(
    ['collection' => "mdl_course_modules_completion"],
    ['projection' => ['id' => 1, 'data' => 1, 'orario' => 1],
        'sort' => ['data' => -1, 'orario' => -1], 'limit' => 1]
);
$id = 0;

if($cursor) {
    foreach ($cursor as $singolo) {
        $id = (int) $singolo['id'];

        $data2 = $singolo['data'];
        $ora = $singolo['orario'];

        $strTimestamp = strtotime("$data2 $ora");
    }
}
//$strTimestamp = '1640995200';
$sql = "SELECT * FROM mdl_course_modules_completion WHERE timemodified >= $strTimestamp";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {

        try {
            $mongo_row = 0;
            $check = $collection->find([
                'id' => (int) $row['id']
            ]);

            foreach ($check as $c) {
                $mongo_row = $c['id'];
            }

            if($mongo_row > 0){
                $collection->updateOne(
                    ['id' => (int) $row["id"]],
                    [
                        '$set' => [
                            'id'              => (int) $row["id"]             ,
                            'coursemoduleid'  => (int) $row["coursemoduleid"] ,
                            'userid'          => (int) $row["userid"]         ,
                            'completionstate' => (int) $row["completionstate"],
                            'viewed'          => (int) $row["viewed"]         ,
                            'overrideby'      => (int) $row["overrideby"]     ,
                            'timemodified'    => (int) $row["timemodified"]
                        ]
                    ],
                    ['upsert' => true]
                );
            }else{
                $collection->insertOne([
                    'id'              => (int) $row["id"]             ,
                    'coursemoduleid'  => (int) $row["coursemoduleid"] ,
                    'userid'          => (int) $row["userid"]         ,
                    'completionstate' => (int) $row["completionstate"],
                    'viewed'          => (int) $row["viewed"]         ,
                    'overrideby'      => (int) $row["overrideby"]     ,
                    'timemodified'    => (int) $row["timemodified"]
                ]);
            }

        } catch (Exception $e) {
            echo $e;
        }
    }

    $pivot->insertOne([
        'id'         => (int) $id,
        'collection' => "mdl_course_modules_completion",
        'data'       => $data,
        'orario'     => $orario
    ]);

}



$conn->close();

