<?php

ini_set('memory_limit', '2048M');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
ini_set('max_execution_time', 0);
error_reporting(E_ALL);

require '/home/admin/public_html/vendor/autoload.php';
//require '../../../../../vendor/autoload.php';


$servername = "185.81.6.69:3306";
$username = "newcorsi_moodle";
$password = "9f.LOcY9ArUt";
$dbname = "newcorsi_moodle";
$db = "new";




$conn = new mysqli($servername, $username, $password, $dbname);


if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}


//Connessione a mongodb
$connection = new MongoDB\Client(
        "mongodb+srv://jobtek:TimeVision2021%40%21%3F@timevision.t9ed5.mongodb.net/TimeVision?retryWrites=true&w=majority");

$collection = $connection->$db->mdl_logstore_standard_log;
$pivot = $connection->$db->pivot;

$data = date("Y-m-d");
$orario = date("H:i:s");


$cursor = $pivot->find(
        ['collection' => "mdl_logstore"],
        ['projection' => ['id' => 1, 'data' => 1, 'orario' => 1],
            'sort' => ['id' => -1], 'limit' => 1]
);
$id = 0;

foreach ($cursor as $singolo) {
    $id = (int) $singolo['id'];
    $data2 = $singolo['data'];
    $ora = $singolo['orario'];

    $strTimestamp = strtotime("$data2 $ora");
}



$sql = "SELECT * FROM mdl_logstore_standard_log WHERE timecreated > $strTimestamp";

$result = $conn->query($sql);
//echo $result->num_rows;exit();
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {

        $logstore_standard_logId = $row["id"];

        $curId = $collection->find( ['id' => intval($logstore_standard_logId)] );

        $mongo_mdl_logstore_standard_log_id = 0;

        foreach ($curId as $row2) {
            $mongo_mdl_logstore_standard_log_id = $row2['id'];
        }

        if ($mongo_mdl_logstore_standard_log_id) {

            $updateResult = $collection->updateOne(
                ['id' => intval($mongo_mdl_logstore_standard_log_id)],
                    [
                        '$set' => [
                            'eventname' => utf8_encode($row["eventname"]),
                            'component' => utf8_encode($row["component"]),
                            'action' => utf8_encode($row["action"]),
                            'target' => utf8_encode($row["target"]),
                            'objecttable' => utf8_encode($row["objecttable"]),
                            'objectid' => (int) $row["objectid"],
                            'crud' => utf8_encode($row["crud"]),
                            'edulevel' => (int) $row["edulevel"],
                            'contextid' => (int) $row["contextid"],
                            'contextlevel' => utf8_encode($row["contextlevel"]),
                            'contextinstanceid' => (int) $row["contextinstanceid"],
                            'userid' => (int) $row["userid"],
                            'courseid' => (int) $row["courseid"],
                            'relateduserid' => (int) $row["relateduserid"],
                            'anonymous' => (int) $row["anonymous"],
                            'other' => utf8_encode($row["other"]),
                            'timecreated' => (int) $row["timecreated"],
                            'origin' => utf8_encode($row["origin"]),
                            'ip' => utf8_encode($row["ip"]),
                            'realuserid' => (int) $row["realuserid"]
                        ]
                    ]
                );

        } else {
            $collection->insertOne([
                'id' => (int) $row["id"],
                'eventname' => utf8_encode($row["eventname"]),
                'component' => utf8_encode($row["component"]),
                'action' => utf8_encode($row["action"]),
                'target' => utf8_encode($row["target"]),
                'objecttable' => utf8_encode($row["objecttable"]),
                'objectid' => (int) $row["objectid"],
                'crud' => utf8_encode($row["crud"]),
                'edulevel' => (int) $row["edulevel"],
                'contextid' => (int) $row["contextid"],
                'contextlevel' => utf8_encode($row["contextlevel"]),
                'contextinstanceid' => (int) $row["contextinstanceid"],
                'userid' => (int) $row["userid"],
                'courseid' => (int) $row["courseid"],
                'relateduserid' => (int) $row["relateduserid"],
                'anonymous' => (int) $row["anonymous"],
                'other' => utf8_encode($row["other"]),
                'timecreated' => (int) $row["timecreated"],
                'origin' => utf8_encode($row["origin"]),
                'ip' => utf8_encode($row["ip"]),
                'realuserid' => (int) $row["realuserid"]
            ]);
        }


        $pivot->insertOne([
            'id' => (int) $row["id"],
            'collection' => "mdl_logstore",
            'data' => $data,
            'orario' => $orario
        ]);



    }
}



$conn->close();

