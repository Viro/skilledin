<?php

ini_set('memory_limit', '2048M');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require '/home/admin/public_html/vendor/autoload.php';
//require '../../../../../vendor/autoload.php';

$servername = "185.81.6.69:3306";
$username = "newcorsi_moodle";
$password = "9f.LOcY9ArUt";
$dbname = "newcorsi_moodle";
$db = "new";




// Create connection a mysql
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}


//Connessione a mongodb
$connection = new MongoDB\Client(
        "mongodb+srv://jobtek:TimeVision2021%40%21%3F@timevision.t9ed5.mongodb.net/TimeVision?retryWrites=true&w=majority");

$collection = $connection->$db->mdl_bigbluebuttonbn;
$pivot = $connection->$db->pivot;

$data = date("Y-m-d");
$orario = date("H:i:s");

$cursor = $pivot->find(
        ['collection' => "mdl_bbb"],
    ['projection' => ['id' => 1, 'data' => 1, 'orario' => 1],
        'sort' => ['data' => -1, 'orario' => -1], 'limit' => 1]
);
$id = 0;
foreach ($cursor as $singolo) {
    $id = (int) $singolo['id'];

    $data2 = $singolo['data'];
    $ora = $singolo['orario'];

    $strTimestamp = strtotime("$data2 $ora");
}


$sql = "SELECT * FROM mdl_bigbluebuttonbn ORDER BY timecreated";
$result = $conn->query($sql);
if ($result->num_rows > 0) {

    while ($row = $result->fetch_assoc()) {

        try {

            $check = $collection->find([
                'id' => (int) $row['id']
            ]);

            foreach ($check as $c) {
                $mongo_id = $c['id'];
            }

            echo $row['id'] . "\n";

            if($mongo_id > 0){
                $collection->updateOne(
                    ['id' => (int) $row["id"]],
                    [
                        '$set' => [
                            'id' => (int) $row["id"],
                            'course' => (int) $row["course"],
                            'name' => utf8_encode($row["name"]),
                            'meetingid' => (int) $row["meetingid"],
                            'timecreated' => (int) $row["timecreated"],
                            'timemodified' => (int) $row["timemodified"],
                            'participants' => utf8_encode($row["participants"]),
                            'type' => (int) $row["type"]
                        ]
                    ]
                );
            }else{
                $collection->insertOne([
                    'id' => (int) $row["id"],
                    'course' => (int) $row["course"],
                    'name' => utf8_encode($row["name"]),
                    'meetingid' => (int) $row["meetingid"],
                    'timecreated' => (int) $row["timecreated"],
                    'timemodified' => (int) $row["timemodified"],
                    'participants' => utf8_encode($row["participants"]),
                    'type' => (int) $row["type"]
                ]);
            }
        } catch (Exception $e) {
            echo $e;
        }
    }

    $pivot->insertOne([
        'id' => 0,
        'collection' => "mdl_bbb",
        'data' => $data,
        'orario' => $orario
    ]);
}



$conn->close();

