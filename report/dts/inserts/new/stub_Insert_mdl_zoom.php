<?php

ini_set('memory_limit', '2048M');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require '/home/admin/public_html/vendor/autoload.php';
//require '../../../../../vendor/autoload.php';

$servername = "185.81.6.69:3306";
$username = "newcorsi_moodle";
$password = "9f.LOcY9ArUt";
$dbname = "newcorsi_moodle";
$db = "new";


// Create connection a mysql
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}


//Connessione a mongodb
$connection = new MongoDB\Client(
        "mongodb+srv://jobtek:TimeVision2021%40%21%3F@timevision.t9ed5.mongodb.net/TimeVision?retryWrites=true&w=majority");

$collection = $connection->$db->mdl_zoom;
$pivot = $connection->$db->pivot;

$data = date("Y-m-d");
$orario = date("H:i:s");

$cursor = $pivot->find(
        ['collection' => "mdl_zoom"],
        ['projection' => ['id' => 1],
            'sort' => ['id' => -1], 'limit' => 1]
);
$id = 0;
foreach ($cursor as $singolo) {
    $id = (int) $singolo['id'];
}

$sql = "SELECT * FROM mdl_zoom WHERE id > $id";
$result = $conn->query($sql);
if ($result->num_rows > 0) {

    while ($row = $result->fetch_assoc()) {


        $pivot->insertOne([
            'id' => (int) $row["id"],
            'collection' => "mdl_zoom",
            'data' => $data,
            'orario' => $orario
        ]);

        $collection->insertOne([
            'id' => (int) $row["id"],
            'course' => (int) $row["course"],
            'meeting_id' => utf8_encode($row["meeting_id"]),
            'start_url' => utf8_encode($row["start_url"]),
            'join_url' => utf8_encode($row["join_url"]),
            'start_time' => (int) $row["start_time"],
            'duration' => (int) $row["duration"],
            'name' => utf8_encode($row["name"]),
            'created_at' => utf8_encode($row["created_at"])
        ]);
    }
}



$conn->close();

