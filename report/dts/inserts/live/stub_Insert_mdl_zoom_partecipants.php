<?php

ini_set('memory_limit', '2048M');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require '/home/admin/public_html/vendor/autoload.php';
//require '../../../../../vendor/autoload.php';

$servername = "77.39.212.59:3306";
$username = "livecors_moodle";
$password = "bn_moodle_01!";
//$password = "wi^hc8mwPTuq";
$dbname = "livecors_moodle";
$db = "live";

// Create connection a mysql
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}


//Connessione a mongodb
$connection = new MongoDB\Client(
        "mongodb+srv://jobtek:TimeVision2021%40%21%3F@timevision.t9ed5.mongodb.net/TimeVision?retryWrites=true&w=majority");

$collection = $connection->$db->mdl_zoom_meeting_participants;
$pivot = $connection->$db->pivot;

$data = date("Y-m-d");
$orario = date("H:i:s");

$cursor = $pivot->find(
        ['collection' => "mdl_zoom_partecipants"],
        ['projection' => ['id' => 1],
            'sort' => ['id' => -1], 'limit' => 1]
);
$id = 0;
foreach ($cursor as $singolo) {
    $id = (int) $singolo['id'];
}

$sql = "SELECT * FROM mdl_zoom_meeting_participants WHERE id > $id";
$result = $conn->query($sql);
if ($result->num_rows > 0) {

    while ($row = $result->fetch_assoc()) {



        $pivot->insertOne([
            'id' => (int) $row["id"],
            'collection' => "mdl_zoom_partecipants",
            'data' => $data,
            'orario' => $orario
        ]);

        $collection->insertOne([
            'id' => (int) $row["id"],
            'userid' => (int) $row["userid"],
            'zoomuserid' => (int) $row["zoomuserid"],
            'uuid' => utf8_encode($row["uuid"]),
            'user_email' => utf8_encode($row["user_email"]),
            'join_time' => (int) $row["join_time"],
            'leave_time' => (int) $row["leave_time"],
            'duration' => (int) $row["duration"],
            'name' => utf8_encode($row["name"]),
            'detailsid' => (int) $row["detailsid"]
        ]);
    }
}



$conn->close();

