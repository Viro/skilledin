<?php

ini_set('memory_limit', '2048M');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require '/home/admin/public_html/vendor/autoload.php';
//require '../../../../../vendor/autoload.php';

$servername = "77.39.212.59:3306";
$username = "livecors_moodle";
$password = "bn_moodle_01!";
//$password = "wi^hc8mwPTuq";
$dbname = "livecors_moodle";
$db = "live";

// Create connection a mysql
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}


//Connessione a mongodb
$connection = new MongoDB\Client(
    "mongodb+srv://jobtek:TimeVision2021%40%21%3F@timevision.t9ed5.mongodb.net/TimeVision?retryWrites=true&w=majority");

$collection = $connection->$db->mdl_zoom_meeting_participants_new;
$pivot = $connection->$db->pivot;

$data = date("Y-m-d");
$orario = date("H:i:s");

$strTimestamp = 0;

$cursor = $pivot->find(
    ['collection' => "mdl_zoom_partecipants_new"],
    ['projection' => ['id' => 1, 'data' => 1, 'orario' => 1],
        'sort' => ['data' => -1, 'orario' => -1], 'limit' => 1]
);

$id = 0;
$mongo_row = 0;

if($cursor) {
    foreach ($cursor as $singolo) {
        $id = (int) $singolo['id'];

        $data2 = $singolo['data'];
        $ora = $singolo['orario'];

        $strTimestamp = strtotime("$data2 $ora");
    }
}

//var_dump($strTimestamp); exit;

$sql = "SELECT * FROM mdl_zoom_meeting_participants WHERE join_time > $strTimestamp ORDER BY join_time ASC";
$result = $conn->query($sql);
if ($result->num_rows > 0) {

    while ($row = $result->fetch_assoc()) {

        try {

            $check = $collection->find([
                'id' => (int) $row['id']
            ]);

            foreach ($check as $c) {
                $mongo_row = $c['id'];
            }

            if($mongo_row > 0){
                $collection->updateOne(
                    ['id' => (int) $row["id"]],
                    [
                        '$set' => [
                            'id' => (int) $row["id"],
                            'userid' => (int) $row["userid"],
                            'zoomuserid' => (int) $row["zoomuserid"],
                            'uuid' => utf8_encode($row["uuid"]),
                            'user_email' => utf8_encode($row["user_email"]),
                            'join_time' => (int) $row["join_time"],
                            'leave_time' => (int) $row["leave_time"],
                            'duration' => (int) $row["duration"],
                            'name' => utf8_encode($row["name"]),
                            'detailsid' => (int) $row["detailsid"]
                        ]
                    ],
                    ['upsert' => true]
                );
            }else{
                $collection->insertOne([
                    'id' => (int) $row["id"],
                    'userid' => (int) $row["userid"],
                    'zoomuserid' => (int) $row["zoomuserid"],
                    'uuid' => utf8_encode($row["uuid"]),
                    'user_email' => utf8_encode($row["user_email"]),
                    'join_time' => (int) $row["join_time"],
                    'leave_time' => (int) $row["leave_time"],
                    'duration' => (int) $row["duration"],
                    'name' => utf8_encode($row["name"]),
                    'detailsid' => (int) $row["detailsid"]
                ]);
            }

        } catch (Exception $e) {
            echo $e;
        }
    }

    $pivot->insertOne([
        'id' => 0,
        'collection' => "mdl_zoom_partecipants_new",
        'data' => $data,
        'orario' => $orario
    ]);

}



$conn->close();

