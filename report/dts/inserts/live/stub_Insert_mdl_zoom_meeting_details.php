<?php

ini_set('memory_limit', '2048M');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require '/home/admin/public_html/vendor/autoload.php';
//require '../../../../../vendor/autoload.php';

$servername = "77.39.212.59:3306";
$username = "livecors_moodle";
$password = "bn_moodle_01!";
//$password = "wi^hc8mwPTuq";
$dbname = "livecors_moodle";
$db = "live";

// Create connection a mysql
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}


//Connessione a mongodb
$connection = new MongoDB\Client(
        "mongodb+srv://jobtek:TimeVision2021%40%21%3F@timevision.t9ed5.mongodb.net/TimeVision?retryWrites=true&w=majority");

$collection = $connection->$db->mdl_zoom_meeting_details;
$pivot = $connection->$db->pivot;

$data = date("Y-m-d");
$orario = date("H:i:s");

$cursor = $pivot->find(
        ['collection' => "mdl_zoom_meeting"],
        ['projection' => ['id' => 1],
            'sort' => ['id' => -1], 'limit' => 1]
);
$id = 0;
foreach ($cursor as $singolo) {
    $id = (int) $singolo['id'];
}

$sql = "SELECT * FROM mdl_zoom_meeting_details WHERE id > $id";
$result = $conn->query($sql);
if ($result->num_rows > 0) {

    while ($row = $result->fetch_assoc()) {



        $pivot->insertOne([
            'id' => (int) $row["id"],
            'collection' => "mdl_zoom_meeting",
            'data' => $data,
            'orario' => $orario
        ]);


        $collection->insertOne([
            'id' => (int) $row["id"],
            'meeting_id' => utf8_encode($row["meeting_id"]),
            'start_time' => (int) $row["start_time"],
            'total_minutes' => (int) $row["total_minutes"],
            'participants_count' => (int) $row["participants_count"],
            'end_time' => (int) $row["end_time"],
            'uuid' => utf8_encode($row["uuid"]),
            'duration' => (int) $row["duration"],
            'topic' => utf8_encode($row["topic"]),
            'zoomid' => (int) $row["zoomid"]
        ]);
    }
}



$conn->close();

