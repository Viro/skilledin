#!/usr/bin/php
<?php

ini_set('memory_limit', '2048M');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
ini_set('max_execution_time', 0);
error_reporting(E_ALL);

$abs_path = '/home/admin/public_html';
if(isset($_SERVER['DOCUMENT_ROOT']) && strlen(trim($_SERVER['DOCUMENT_ROOT']))) $abs_path = $_SERVER['DOCUMENT_ROOT'];

require_once($abs_path . "/skilledin/report/obj/Skilledin.php");
require_once($abs_path . "/skilledin/report/obj/SkilledinWeb.php");
require_once($abs_path . "/skilledin/report/obj/Report.php");
require_once($abs_path . "/skilledin/report/obj/Admin.php");
require_once($abs_path . "/skilledin/report/obj/SkilledinReport.php");
require_once($abs_path . "/skilledin/report/obj/Formazione.php");
require_once($abs_path . "/skilledin/report/obj/Formazione40.php");
require_once($abs_path . "/skilledin/report/obj/FondoNuoveCompetenze.php");
require_once($abs_path . "/skilledin/report/obj/Live.php");
require_once($abs_path . "/skilledin/report/obj/PlatformNew.php");
require_once($abs_path . "/skilledin/report/obj/Utility.php");

if (isset($_POST['enterprise']) && isset($_POST['token']) && $_POST['token'] == 'skpl_2023') {
    $report_data = false;
    $object      = new Admin('admin');
    $plans       = $object->getAziendePianiFormativi(array('nome_azienda' => $_POST['enterprise']));
    if(isset($_POST['skilledin_plan_id']) && $_POST['skilledin_plan_id'] != ''){
        $report_data = $object->calculateReportAziendePianiFormativi($_POST['skilledin_plan_id']);
    }else{
        if($plans){
            $next_expired_plan = 0;
            foreach($plans as $plan){
                if($next_expired_plan == 0 || $plan['stamp_fine_piano'] < $next_expired_plan){
                    $report_data = $object->calculateReportAziendePianiFormativi($plan['skilledin_plan_id']);
                    $next_expired_plan = $plan['stamp_fine_piano'];
                }
            }
        }
    }
    if($report_data){
        $report_data['data_fine_piano'] = date('d/m/Y', $report_data['stamp_fine_piano']);

        header('Content-Type: application/json; charset=utf-8');
        http_response_code(201);
        echo json_encode(array('data'      => $report_data,
                               'plan_list' => $plans   ));
    }
}
