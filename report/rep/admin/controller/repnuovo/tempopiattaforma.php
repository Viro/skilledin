<?php
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ControllerRepnuovoTempopiattaforma extends Controller {
    
    public function index() {
        
        if (!isset($this->session->data['user_token'])) {
            $this->session->data['user_token'] = 1;
        }
        $this->load->language('catalog/category');
        
        $this->document->setTitle('Corsi');
        
        $this->load->model('repnuovo/tempopiattaforma');
        
        $this->getList();
    }
    
    protected function getList() {
        
        if (isset($this->request->get['platf'])) {
            $db = $this->request->get['platf'];
        } else {
            exit('manca la piattaforma');
        }
        
        if (isset($this->request->get['enterprise'])) {
            $gruppo = $this->request->get['enterprise'];
        } else {
            $gruppo = 'Tutti';
        }
        
        if (isset($this->request->get['start'])) {
            $data_inizio = $this->request->get['start'];
        } else {
            $data_inizio = '01-04-2021';
        }
        
        if (isset($this->request->get['end'])) {
            $data_fine = $this->request->get['end'];
        } else {
            $data_fine = '31-04-2021';
        }
        
        if (isset($this->request->get['course'])) {
            $course = $this->request->get['course'];
        } else {
            $course = 0;
        }
        
        if (isset($this->request->post['scarica'])) {
            $scarica = $this->request->post['scarica'];
        } else {
            $scarica = 0;
        }
        
        $inizio = ($data_inizio == '01-01-1970') ? 0 : strtotime($data_inizio);
        $fine = ($data_fine == '01-01-2999') ? strtotime('now') + 86400 : strtotime($data_fine) + 86400;
        
        if (isset($this->request->get['utente'])) {
            $utente = $this->request->get['utente'];
        } else {
            $utente = 0;
        }
        
        $array_id = $this->model_repnuovo_tempopiattaforma->getIdUtentiFromAzienda($db, $gruppo);
        
        $arrTempoInPiattaforma = $this->model_repnuovo_tempopiattaforma->getArrTempoInPiattaforma($db, $inizio, $fine, $utente, $array_id);
        
        if (!$scarica) {
        
            $url = "&platf=$db&enterprise=$gruppo&start=$data_inizio&end=$data_fine";
            $data['arrTempoInPiattaforma'] = $arrTempoInPiattaforma['arrayResult'];
            $data['giorni'] = $arrTempoInPiattaforma['giorni'];
            
            $data['platf'] = $db;
            $data['enterprise'] = $gruppo;
            $data['start'] = $data_inizio;
            $data['end'] = $data_fine;
            $data['scarica'] = 1;
            $data['stampa'] = $this->url->link('repnuovo/tempopiattaforma', 'user_token=' . $this->session->data['user_token'] . $url, true);
            
            $pagination = new Pagination();
            $page = 1;
            $product_total = count($arrTempoInPiattaforma);
            
            
            $pagination->total = $product_total;
            $pagination->page = $page;
            $pagination->limit = 5000;
            $pagination->url = $this->url->link('repnuovo/tempopiattaforma', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);
            
            $data['pagination'] = $pagination->render();
            
            $data['user_token'] = $this->session->data['user_token'];
            $data['header'] = $this->load->controller('common/header');
            $data['column_left'] = $this->load->controller('common/column_left');
            $data['footer'] = $this->load->controller('common/footer');
            $data['error_warning'] = '';
            
            $this->response->setOutput($this->load->view('repnuovo/tempopiattaforma_list', $data));
            
        } else {
            $this->scarica($arrTempoInPiattaforma, $gruppo, $data_inizio, $data_fine);
        }
     }
     
     public function scarica($arrTempoInPiattaforma, $gruppo, $data_inizio, $data_fine)
     {
         $spreadsheet = new Spreadsheet();
         
         $userid = 0;
         $j = 0;
         $letter = "A";
         $filtri = array();
         $finale = $arrTempoInPiattaforma['arrayResult'];
         $array_mesi = $arrTempoInPiattaforma['giorni'];
         
         $this->model_repnuovo_tempopiattaforma->createCopertina($userid, $spreadsheet, $j);
         
         $j++;
         $spreadsheet->setActiveSheetIndex($j);
         $index = $this->model_repnuovo_tempopiattaforma->createSheet($userid, $spreadsheet, $j);
         $i = $this->model_repnuovo_tempopiattaforma->setTimeinSheet($index, $spreadsheet, $letter, $filtri, $j, $finale, $array_mesi);
         
         /*$writer = new Xlsx($spreadsheet);
         $writer->save(DIR_DOWNLOAD.'hello-world.xlsx');
         exit('ok');*/
         $nome = "report_tip_$gruppo" . "_" . $data_inizio . "_" . $data_fine . ".xlsx";
         $now = date("D, d M Y H:i:s");
         
         header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
         header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
         header("Last-Modified: {$now} GMT");
         header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
         header("Content-Disposition: attachment;filename=$nome");
         header("Content-Transfer-Encoding: binary");
         
         $writer = new Xlsx($spreadsheet);
         ob_start();
         $writer->save("php://output");
         $xlsData = ob_get_contents();
         ob_end_clean();
         echo $xlsData;exit();
         //var_dump($spreadsheet);exit();
     }
}

?>
