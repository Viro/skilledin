<?php 
class ControllerMoodleCorso extends Controller {
	private $error = array();
	
	public function index() {
		$this->load->language('catalog/category');
		
		$this->document->setTitle('Corsi');

		$this->load->model('moodle/corso');

		$this->getList();
	}
	
	protected function getList() {
	    $collection = new MongoDB\Client("mongodb+srv://jobtek:TimeVision2021%40%21%3F@timevision.t9ed5.mongodb.net/TimeVision?retryWrites=true&w=majority");
	    var_dump($collection);EXIT();
			if (isset($this->request->get['sort'])) {
				$sort = $this->request->get['sort'];
			} else {
				$sort = 'name';
			}

			if (isset($this->request->get['order'])) {
				$order = $this->request->get['order'];
			} else {
				$order = 'ASC';
			}

			if (isset($this->request->get['page'])) {
				$page = (int)$this->request->get['page'];
			} else {
				$page = 1;
			}
		
			$data['breadcrumbs'] = array();
			$data['corsi'] = array();
			
			$results = $this->model_moodle_corso->getCorsi();
			
			foreach ($results as $result) {
				
				$timecreated = date('d/m/Y', $result['timecreated']);
				
				$data['corsi'][] = array(
					'corso_id' => $result['id'],
					'fullname'        => $result['fullname'],
					'timecreated'        => $timecreated,
					'edit'        => $this->url->link('moodle/corso/reportCorso', 'user_token=' . $this->session->data['user_token'] . '&corso_id=' . $result['id'], true)
				);
			}
		
			if (isset($this->error['warning'])) {
					$data['error_warning'] = $this->error['warning'];
				} else {
					$data['error_warning'] = '';
				}

			if (isset($this->session->data['success'])) {
				$data['success'] = $this->session->data['success'];
				unset($this->session->data['success']);
			} else {
				$data['success'] = '';
			}
			
			
			if (isset($this->request->post['selected'])) {
				$data['selected'] = (array)$this->request->post['selected'];
			} else {
				$data['selected'] = array();
			}
			
			$url = '';

			if ($order == 'ASC') {
				$url .= '&order=DESC';
			} else {
				$url .= '&order=ASC';
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$data['sort_name'] = $this->url->link('moodle/corso', 'user_token=' . $this->session->data['user_token'] . '&sort=name' . $url, true);
			$data['sort_sort_order'] = $this->url->link('moodle/corso', 'user_token=' . $this->session->data['user_token'] . '&sort=sort_order' . $url, true);

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			
			$category_total = 10;

			$pagination = new Pagination();
			$pagination->total = $category_total;
			$pagination->page = 1;
			$pagination->limit = $this->config->get('config_limit_admin');
			$pagination->url = $this->url->link('moodle/corso', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);

			$data['pagination'] = $pagination->render();

			$data['results'] = sprintf($this->language->get('text_pagination'), ($category_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($category_total - $this->config->get('config_limit_admin'))) ? $category_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $category_total, ceil($category_total / $this->config->get('config_limit_admin')));

			$data['sort'] = $sort;
			$data['order'] = $order;

			$data['header'] = $this->load->controller('common/header');
			$data['column_left'] = $this->load->controller('common/column_left');
			$data['footer'] = $this->load->controller('common/footer');

			$this->response->setOutput($this->load->view('moodle/corso_list', $data));
	
	
	}
	
	public function reportCorso()
	{
		$this->load->model('moodle/report');
		$this->load->model('moodle/aula');
		
		$corso_id = $this->request->get['corso_id'];
		
		$data['corso_id'] = $corso_id;
		
		$url = '';

		if (isset($this->request->get['filter_aula'])) {
			$url .= '&filter_aula=' . urlencode(html_entity_decode($this->request->get['filter_aula'], ENT_QUOTES, 'UTF-8'));
			$aula_id = (int)$this->request->get['filter_aula'];
		} else {
			$aula_id = 0;
		}
		
		$utentiCorso = $this->model_moodle_report->getUtentiCorso($corso_id, $aula_id);
		
		$data['utentiCorso'] = $utentiCorso;
		
		$data['arrAula'] = $this->model_moodle_aula->getAule();
		
		$data['action'] = $this->url->link('moodle/corso/reportCorso', 'user_token=' . $this->session->data['user_token'] . $url, true);
		
		$data['user_token'] = $this->session->data['user_token'];
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('moodle/corso_report', $data));
		
	}
}
	










?>