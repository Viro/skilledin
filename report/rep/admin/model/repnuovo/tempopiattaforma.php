<?php
class ModelRepnuovoTempopiattaforma extends Model {
    
    private $conn;
    
    public function __construct() {
        $this->conn = new MongoDB\Client("mongodb+srv://jobtek:TimeVision2021%40%21%3F@timevision.t9ed5.mongodb.net/TimeVision?retryWrites=true&w=majority");
    }
    
    public function getArrTempoInPiattaforma($db, $inizio, $fine, $userid, $array_id)
    {
        /* Prendo il timespamp del primo login poi cercco se c'� un logout, se c'� controllo se tra questo logut e il primo login c'� un altro login, se non c'� prendo il timestamp
         * del logout altrimenti prendo il timestamp dell'evento prima del secondo login. Quindi poi nella stessa giornata cerco il prossimo login ( con il tiemspamp maggiore dell'ultimo 
         * lougt trovato e ripeto gli stessi passaggi. Se non c'� un ultimo logout nella giornata prendo l'ultimo evento della giornata. */
        $temporaneo = $this->getLoginDate($db, $inizio, $fine, null, $array_id);
        
        $arrDateLogin = array();
        
        foreach ($temporaneo as $key => $arrCursore) {
            $data = date("d-m-Y", $arrCursore['timecreated']);
            $userId = $arrCursore['userid'];
            $arrDateLogin[$userId][$data]['timecreatedLogin'][] = $arrCursore['timecreated'];        
            /* Memorizzo i dati dell'utente */
            $arrDateLogin[$userId]['userid'] = $arrCursore['userid'];
            $arrDateLogin[$userId]['name'] = $arrCursore['nome'];
            $arrDateLogin[$userId]['username'] = $arrCursore['username'];
            $arrDateLogin[$userId]['email'] = $arrCursore['email'];
            $arrDateLogin[$userId]['cf'] = 'n/d';
            $arrDateLogin[$userId]['data_nascita'] = date("d-m-Y", $arrCursore['data']);
            $arrDateLogin[$userId]['citta'] = $arrCursore['citta'];
            $arrDateLogin[$userId]['azienda'] = $arrCursore['azienda'];
            $arrDateLogin[$userId]['telefono'] = 'n/d';
        }
        
        foreach ($arrDateLogin as $keyUserid => $dataLogin) {
            $tempoGiorni = 0;
            foreach ($dataLogin as $keyDataGiorno => $arrTimecreatedLogin) {
                
                if ($keyDataGiorno != 'userid' && $keyDataGiorno != 'name' && $keyDataGiorno != 'username' && $keyDataGiorno != 'email' 
                    && $keyDataGiorno != 'cf' &&  $keyDataGiorno != 'data_nascita' && $keyDataGiorno != 'citta' && $keyDataGiorno != 'azienda' && $keyDataGiorno != 'telefono') {
                        
                    $arrGiorno = $this->getTimestampGiorno($keyDataGiorno);
                    $inizioGiorno = $arrGiorno['inizio'];
                    $fineGiorno = $arrGiorno['fine'];
                    
                    $differenza = 0;
                    
                    foreach ($arrTimecreatedLogin['timecreatedLogin'] as $keyLogout => $timestampLogin) {
                        
                        $timestampLogout = $this->getLogoutDate($db, $timestampLogin, $fineGiorno, $keyUserid);
                        
                        $arrDateLogin[$keyUserid][$keyDataGiorno]['timecreatedLogout'][$keyLogout] = $timestampLogout;
                        $differenza += ($timestampLogout - $timestampLogin);
                        $arrDateLogin[$keyUserid][$keyDataGiorno]['tempo'] = $this->convertSeconds($differenza);
                        $arrDateLogin[$keyUserid][$keyDataGiorno]['tempoSecondi'] = $differenza;     
                        
                        if ($keyUserid == 10166 ) {
                            echo "<br>Login: $timestampLogin, Logout: $timestampLogout, differenza: $differenza";
                        }
                        
                    }
                    
                    $tempoGiorni = ( $tempoGiorni + $differenza);
                
                }
            }
            
            $arrDateLogin[$keyUserid]['tempoGiorni'] = $tempoGiorni;
            $arrDateLogin[$keyUserid]['tempoGiorniFormattato'] = $this->convertSeconds($tempoGiorni);
        }
        
        //var_dump($arrDateLogin);exit();
        
        /* Recupero i giorni da considerare */
        foreach ($arrDateLogin as $keyUserid => $dataLogin) {
            foreach ($dataLogin as $keyDataGiorno => $arrTimecreatedLogin) {
                if ($keyDataGiorno != 'tempoGiorni' && $keyDataGiorno != 'tempoGiorniFormattato' && $keyDataGiorno != 'userid' && $keyDataGiorno != 'name' && $keyDataGiorno != 'username' && $keyDataGiorno != 'email'
                    && $keyDataGiorno != 'cf' &&  $keyDataGiorno != 'data_nascita' && $keyDataGiorno != 'citta' && $keyDataGiorno != 'azienda' && $keyDataGiorno != 'telefono') {
                        
                    $arrGiorni[$keyDataGiorno] = $keyDataGiorno;
                }
                
            }
        }
        
        asort($arrGiorni);
        
        /* Formatto l'array per la visualizzazione in tabella */
        $arrayResult = array();
        foreach ($arrDateLogin as $keyUserid => $dataLogin) {
            
            $arrayResult[$keyUserid]['userid'] = $dataLogin['userid'];
            $arrayResult[$keyUserid]['username'] = $dataLogin['username'];
            $arrayResult[$keyUserid]['name'] = $dataLogin['name'];
            $arrayResult[$keyUserid]['cf'] = $dataLogin['cf'];
            $arrayResult[$keyUserid]['email'] = $dataLogin['email'];
            $arrayResult[$keyUserid]['data_nascita'] = $dataLogin['data_nascita'];
            $arrayResult[$keyUserid]['citta'] = $dataLogin['citta'];
            $arrayResult[$keyUserid]['telefono'] = $dataLogin['telefono'];
            $arrayResult[$keyUserid]['azienda'] = $dataLogin['azienda'];
            $arrayResult[$keyUserid]['tempoCumulato'] = $dataLogin['tempoGiorniFormattato'];
            
            foreach ($arrGiorni as $keyGiorno => $giorno) {
                
                if (isset($dataLogin[$giorno])) {
                    $arrayResult[$keyUserid][$giorno] = $dataLogin[$giorno]['tempo'];
                } else {
                    $arrayResult[$keyUserid][$giorno] = '00:00:00';
                }
            }
            
            
            
        }
        
        return array('arrayResult' => $arrayResult, 'giorni' => $arrGiorni);
        
    }
    
    public function getLoginDate($db, $inizio, $fine, $idUtente = null, $array_id = null, $idCorso = null, $sort = 1) {
        $collection = $this->conn->$db->mdl_logstore_standard_log;
        
        if (!isset($idUtente)) {
            if (isset($idCorso) && isset($array_id)) {
                if ($idCorso == 0) {
                    $match = ['$match' => [
                        'userid' => [
                            '$in' => $array_id
                        ],
                        'timecreated' => [
                            '$gt' => $inizio,
                            '$lt' => $fine
                        ]
                    ]];
                } else if (count($array_id) == 0) {
                    $match = ['$match' => [
                        'courseid' => (int) $idCorso,
                        'timecreated' => [
                            '$gte' => $inizio,
                            '$lte' => $fine
                        ]
                    ]];
                } else {
                    $match = ['$match' => [
                        'userid' => [
                            '$in' => $array_id
                        ],
                        'courseid' => (int) $idCorso,
                        'timecreated' => [
                            '$gte' => $inizio,
                            '$lte' => $fine
                        ]
                    ]];
                }
            } else if (!isset($idCorso)) {
                $match = ['$match' => [
                    'userid' => [
                        '$in' => $array_id
                    ],
                    'timecreated' => [
                        '$gt' => $inizio,
                        '$lt' => $fine
                    ],
                    'action' => 'loggedin'
                ]];
            }
        } else {
            $match = ['$match' => [
                'userid' => (int) $idUtente,
                'timecreated' => [
                    '$gt' => $inizio,
                    '$lt' => $fine
                ],
                'action' => 'loggedin'
            ]];
        }
        $pipeline = [$match,
            ['$lookup' => [
                'from' => 'mdl_utenti_complete',
                'localField' => 'userid',
                'foreignField' => 'userid',
                'as' => 'info'
            ]], ['$set' => [
                'username' => [
                    '$arrayElemAt' => [
                        '$info.username',
                        0
                    ]
                ],
                'email' => [
                    '$arrayElemAt' => [
                        '$info.email',
                        0
                    ]
                ],
                'cf' => [
                    '$arrayElemAt' => [
                        '$info.idnumber',
                        0
                    ]
                ],
                'nome' => [
                    '$arrayElemAt' => [
                        '$info.nome',
                        0
                    ]
                ],
                'citta' => [
                    '$arrayElemAt' => [
                        '$info.citta',
                        0
                    ]
                ],
                'data' => [
                    '$arrayElemAt' => [
                        '$info.data',
                        0
                    ]
                ],
                'azienda' => [
                    '$arrayElemAt' => [
                        '$info.azienda',
                        0
                    ]
                ]
                ,
                'phone2' => [
                    '$arrayElemAt' => [
                        '$info.phone2',
                        0
                    ]
                ],
                'phone1' => [
                    '$arrayElemAt' => [
                        '$info.phone1',
                        0
                    ]
                ],
            ]], ['$project' => [
                'info' => 0
            ]],
            [ '$sort' => [ 'timecreated' => $sort ] ]
        ];
        
        $out = $collection->aggregate($pipeline);
        return $out->toArray();
    }
    
    public function getLogoutDate($db, $dataLogin1, $dataLogin2, $idUtente) {
        $collection = $this->conn->$db->mdl_logstore_standard_log;
        
        //echo $dataLogin1.' '.$dataLogin2.' '.$idUtente."<br>";
        $match = ['$match' => [
            'userid' => (int) $idUtente,
            'timecreated' => [
                '$gt' => $dataLogin1,
                '$lt' => $dataLogin2
            ],
            'action' => 'loggedout'
        ]];
        
        $pipeline = [$match,
            ['$lookup' => [
                'from' => 'mdl_utenti_complete',
                'localField' => 'userid',
                'foreignField' => 'userid',
                'as' => 'info'
            ]], ['$set' => [
                'username' => [
                    '$arrayElemAt' => [
                        '$info.username',
                        0
                    ]
                ],
                'email' => [
                    '$arrayElemAt' => [
                        '$info.email',
                        0
                    ]
                ],
                'cf' => [
                    '$arrayElemAt' => [
                        '$info.idnumber',
                        0
                    ]
                ],
                'nome' => [
                    '$arrayElemAt' => [
                        '$info.nome',
                        0
                    ]
                ],
                'citta' => [
                    '$arrayElemAt' => [
                        '$info.citta',
                        0
                    ]
                ],
                'data' => [
                    '$arrayElemAt' => [
                        '$info.data',
                        0
                    ]
                ],
                'azienda' => [
                    '$arrayElemAt' => [
                        '$info.azienda',
                        0
                    ]
                ]
                ,
                'phone2' => [
                    '$arrayElemAt' => [
                        '$info.phone2',
                        0
                    ]
                ],
                'phone1' => [
                    '$arrayElemAt' => [
                        '$info.phone1',
                        0
                    ]
                ],
            ]], ['$project' => [
                'info' => 0
            ]],
            [ '$sort' => [ 'timecreated' => 1 ] ]
        ];
        //var_dump($pipeline) ;exit();
        $out = $collection->aggregate($pipeline);
        $cursore =  $out->toArray();
        //var_dump($cursore);exit();
        $dataLogout = false;
        
        if (isset($cursore[0]['timecreated']) && $cursore[0]['timecreated']) {
            $dataLogout = $cursore[0]['timecreated'];
        }
        //var_dump($dataLogout);exit('ertrte');
        /* Se non c'� l'evento logout prendo l'evento appena prima al prossimo login */
        if (!$dataLogout) {
            $arrLogin = $this->getLoginDate($db, $dataLogin1, $dataLogin2, $idUtente);
            //var_dump($arrLogin);exit('aaaaa333');
            if (isset($arrLogin[0]['timecreated']) && $arrLogin[0]['timecreated']) {
                $loginSuccessivo = $arrLogin[0]['timecreated'];
                //var_dump($loginSuccessivo);exit('aaaaa');
                $arrLastEvent = $this->getLastEvent($db, $dataLogin1, $loginSuccessivo, $idUtente);
                
                if (isset($arrLastEvent[0]['timecreated']) && $arrLastEvent[0]['timecreated']) {
                    if ($arrLastEvent[0]['action'] == 'failed') {
                        $arrLastEvent2 = $this->getLastEvent($db, $dataLogin1, $arrLastEvent[0]['timecreated'], $idUtente);
                        if (isset($arrLastEvent2[0]['timecreated']) && $arrLastEvent2[0]['timecreated']) {
                            $dataLogout = $arrLastEvent2[0]['timecreated'];
                        } else {
                            /* Questo � il caso in cui un utente ha in sequenza: login, failed, login 
                             * in questo caso non � possibile definire il tempo di logout e quindi lo valorizzo con il timestamp del login */
                            $dataLogout = $dataLogin1;
                        }
                    } else {
                        $dataLogout = $arrLastEvent[0]['timecreated'];
                        //var_dump($dataLogout);exit('aaaaa333');
                    }
                } else {
                    /* Questo � il caso in cui un utente ha in sequenza: login, login
                     * in questo caso non � possibile definire il tempo di logout e quindi lo valorizzo con il timestamp del primo login */
                    $dataLogout = $dataLogin1;
                }
            } else {
                /* Prendo l'ultimo evento della giornata */
                $arrLastEvent3 = $this->getLastEvent($db, $dataLogin1, $dataLogin2, $idUtente);
                
                if (isset($arrLastEvent3[0]['timecreated']) && $arrLastEvent3[0]['timecreated']) {
                    $dataLogout = $arrLastEvent3[0]['timecreated'];
                } else {
                    /* Questo � il caso in cui non c'� il logout e l'ultimo evento della giornata � un login */
                    $dataLogout = $dataLogin1;
                }
                //var_dump($arrLastEvent3);exit();
                
            }
        } else {
            /* Se c'� l'evento logout controllo prima se tra il login e questo logout c'� un altro login */
            $arrLogin = $this->getLoginDate($db, $dataLogin1, $dataLogout, $idUtente);
            //var_dump($arrLogin);exit('aaaaa333');
            if (isset($arrLogin[0]['timecreated']) && $arrLogin[0]['timecreated']) {
                $loginSuccessivo = $arrLogin[0]['timecreated'];
                //var_dump($loginSuccessivo);exit('aaaaa');
                $arrLastEvent = $this->getLastEvent($db, $dataLogin1, $loginSuccessivo, $idUtente);
                
                if (isset($arrLastEvent[0]['timecreated']) && $arrLastEvent[0]['timecreated']) {
                    if ($arrLastEvent[0]['action'] == 'failed') {
                        $arrLastEvent2 = $this->getLastEvent($db, $dataLogin1, $arrLastEvent[0]['timecreated'], $idUtente);
                        if (isset($arrLastEvent2[0]['timecreated']) && $arrLastEvent2[0]['timecreated']) {
                            $dataLogout = $arrLastEvent2[0]['timecreated'];
                        } else {
                            /* Questo � il caso in cui un utente ha in sequenza: login, failed, login
                             * in questo caso non � possibile definire il tempo di logout e quindi lo valorizzo con il timestamp del login */
                            $dataLogout = $dataLogin1;
                        }
                    } else {
                        $dataLogout = $arrLastEvent[0]['timecreated'];
                        //var_dump($dataLogout);exit('aaaaa333');
                    }
                }
            }
        }
        //var_dump($dataLogout);exit();
        return $dataLogout;
    }
    
    public function getLastEvent($db, $dataLogin1, $dataLogin2, $idUtente) {
        $collection = $this->conn->$db->mdl_logstore_standard_log;
        
        
        $match = ['$match' => [
            'userid' => (int) $idUtente,
            'timecreated' => [
                '$gt' => $dataLogin1,
                '$lt' => $dataLogin2
            ],
        ]];
        
        $pipeline = [$match,
            ['$lookup' => [
                'from' => 'mdl_utenti_complete',
                'localField' => 'userid',
                'foreignField' => 'userid',
                'as' => 'info'
            ]], ['$set' => [
                'username' => [
                    '$arrayElemAt' => [
                        '$info.username',
                        0
                    ]
                ],
                'email' => [
                    '$arrayElemAt' => [
                        '$info.email',
                        0
                    ]
                ],
                'cf' => [
                    '$arrayElemAt' => [
                        '$info.idnumber',
                        0
                    ]
                ],
                'nome' => [
                    '$arrayElemAt' => [
                        '$info.nome',
                        0
                    ]
                ],
                'citta' => [
                    '$arrayElemAt' => [
                        '$info.citta',
                        0
                    ]
                ],
                'data' => [
                    '$arrayElemAt' => [
                        '$info.data',
                        0
                    ]
                ],
                'azienda' => [
                    '$arrayElemAt' => [
                        '$info.azienda',
                        0
                    ]
                ]
                ,
                'phone2' => [
                    '$arrayElemAt' => [
                        '$info.phone2',
                        0
                    ]
                ],
                'phone1' => [
                    '$arrayElemAt' => [
                        '$info.phone1',
                        0
                    ]
                ],
            ]], ['$project' => [
                'info' => 0
            ]],
            [ '$sort' => [ 'timecreated' => -1 ] ]
        ];
        //var_dump($pipeline) ;exit();
        $out = $collection->aggregate($pipeline);
        
        return  $out->toArray();
        
    }
    
    public function getTimestampGiorno($data)
    {
        $inizio = strtotime($data . " 00:00:00");
        $fine = strtotime($data . " 23:59:59");
        
        return array('inizio' => $inizio, 'fine' => $fine);
    }
    
    public function getIdUtentiFromAzienda($db, $gruppo) {
        $collection = $this->conn->$db->mdl_user_info_data;
        $gruppo = trim($gruppo);
        
        $cursor = $collection->find(
            ['data' => $gruppo],
            ['projection' => ['userid' => 1]]
            );
        $tmp = array();
        $array_id = array();
        foreach ($cursor as $singolo) {
            $tmp = $singolo['userid'];
            array_push($array_id, $tmp);
        }
        
        return $array_id;
    }
    
    public function convertSeconds($seconds) {
        $hours = floor($seconds / 3600);
        $mins = floor($seconds / 60 % 60);
        $secs = floor($seconds % 60);
        $timeFormat = sprintf('%02d:%02d:%02d', $hours, $mins, $secs);
        return $timeFormat;
    }
    
    public function createCopertina($userid, $spreadsheet, $j) {
        $txt1 = "";
        $txt2 = "";
        $src = "";
        $collection = $this->conn->admin->settings_excel_copertina;
        $cursor = $collection->find(
            ['userid' => (int) $userid],
            ['projection' => ['txt1' => 1, 'txt2' => 1, 'img' => 1],
                'limit' => 1,
                'sort' => ['_id' => -1],
            ]
            );
        
        foreach ($cursor as $singolo) {
            $txt1 = $singolo['txt1'];
            $txt2 = $singolo['txt2'];
            $src = $singolo['img'];
        }
        if ($src != "") {
            $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing->setName('S1');
            $drawing->setDescription('Intestazione');
            $drawing->setPath('img/' . $src);
            $drawing->setCoordinates('A1');
            $drawing->setHeight(100);
            $drawing->setWidth(800);
            $drawing->setWorksheet($spreadsheet->getActiveSheet());
        }
        if ($txt1 != "") {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue("A13", $txt1);
        }
        if ($txt2 != "") {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue("A19", $txt2);
        }
        $spreadsheet->setActiveSheetIndex($j)->setCellValue("H17", "REGISTRO ATTIVITA' FORMATIVE");
        $spreadsheet->getActiveSheet()->getStyle("H17")->getFont()->setBold(true);
        $spreadsheet->getActiveSheet()->getStyle("H17")->getFont()->setSize(18);
        
        $spreadsheet->getActiveSheet()->setTitle("Copertina");
        $spreadsheet->createSheet();
    }
    
    public function createSheet($userid, $spreadsheet, $j) {
        $s1 = "";
        $t1 = "";
        $collection = $this->conn->admin->settings_excel_sheet;
        $cursor = $collection->find(
            ['userid' => (int) $userid],
            ['projection' => ['s1' => 1, 't1' => 1],
                'limit' => 1,
                'sort' => ['_id' => -1],
            ]
            );
        foreach ($cursor as $singolo) {
            $s1 = $singolo['s1'];
            $t1 = $singolo['t1'];
        }
        $index = 1;
        if ($s1 != "") {
            $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing->setName('S1');
            $drawing->setDescription('Intestazione Sheet');
            $drawing->setPath('img/' . $s1);
            $drawing->setCoordinates('A1');
            $drawing->setHeight(100);
            $drawing->setWidth(800);
            $drawing->setWorksheet($spreadsheet->getActiveSheet());
            $index = 16;
        }
        if ($t1 != "") {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue("A13", $t1);
            $index = 16;
        }
        return $index;
    }
    
    public function setTimeinSheet($index, $spreadsheet, $letter, $filtri, $j, $finale, $array_mesi, $fromReport = null) {
        //var_dump($array_mesi);exit();
        if (!in_array("userid", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Userid");
            $letter++;
        }
        if (!in_array("username", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Username");
            $letter++;
        }
        
        if (!in_array("name", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Nome Cognome");
            $letter++;
        }
        if (!in_array("cf", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Codice Fiscale");
            $letter++;
        }
        if (!in_array("email", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Email");
            $letter++;
        }
        if (!in_array("dn", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Data di Nascita");
            $letter++;
        }
        if (!in_array("ln", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Luogo di Nascita");
            $letter++;
        }
        if (!in_array("azienda", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Azienda");
            $letter++;
        }
        if (!in_array("phone", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "N.di telefono");
            $letter++;
        }
        if (isset($fromReport) && !in_array("corso", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Corso");
            $letter++;
        }
        if (isset($fromReport) && !in_array("courseid", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $index, "Id Corso");
            $letter++;
        }
        if (!in_array("tc", $filtri)) {
            $spreadsheet->setActiveSheetIndex($j)->setCellValue("$letter" . $index, "Tempo cumulato");
            $letter++;
        }
        
        
        $column = $letter;
        $giro = 0;
        
        foreach ($array_mesi as $mese) {
            if (!in_array("data_" . $giro, $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($column . $index, $mese);
                $column++;
            }
            $giro++;
        }
        
        
        $spreadsheet->getActiveSheet()->setTitle("Report");
        
        
        $from = "A1";
        $to = "$column" . $index;
        $spreadsheet->getActiveSheet()->getStyle("$from:$to")->getFont()->setBold(true);
        
        $i = $index + 1;
        
        foreach ($finale as $singolo) {
            $letter = 'A';
            
            if (!in_array("userid", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['userid']);
                $letter++;
            }
            if (!in_array("username", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['username']);
                $letter++;
            }
            if (!in_array("name", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['name']);
                $letter++;
            }
            if (!in_array("cf", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['cf']);
                $letter++;
            }
            if (!in_array("email", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['email']);
                $letter++;
            }
            
            if (!in_array("dn", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, @$singolo['data_nascita']);
                $letter++;
            }
            
            if (!in_array("ln", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, @$singolo['citta']);
                $letter++;
            }
            
            if (!in_array("azienda", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['azienda']);
                $letter++;
            }
            if (!in_array("phone", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['telefono']);
                $letter++;
            }
            
            if (isset($fromReport) && !in_array("corso", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['corso']);
                $letter++;
            }
            if (isset($fromReport) && !in_array("courseid", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue($letter . $i, $singolo['courseid']);
                $letter++;
            }
            if (!in_array("tc", $filtri)) {
                $spreadsheet->setActiveSheetIndex($j)->setCellValue("$letter$i", $singolo["tempoCumulato"]);
                $letter++;
            }
            
            $column = $letter;
            $giro = 0;
            foreach ($array_mesi as $value) {
                if (!in_array("data_" . $giro, $filtri)) {
                    $spreadsheet->setActiveSheetIndex($j)->setCellValue("$column$i", $singolo[$value]);
                    $column++;
                }
                $giro++;
            }
            
            
            $i++;
        }
        foreach (range('A', "$column") as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
            ->setAutoSize(true);
        }
        $spreadsheet->getActiveSheet()->getStyle("A" . $index . ":" . $column . $index)
        ->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('00FF7F');
        
        return $i;
    }
    
    public function addSettingsExcel($userid, $spreadsheet, $i) {
        $s2 = "";
        $s3 = "";
        $collection = $this->conn->admin->settings_excel_sheet;
        $cursor = $collection->find(
            ['userid' => (int) $userid],
            ['projection' => ['s2' => 1, 'timbro' => 1],
                'limit' => 1,
                'sort' => ['_id' => -1],
            ]
            );
        foreach ($cursor as $singolo) {
            $s2 = $singolo['s2'];
            $s3 = $singolo['timbro'];
        }
        if ($s2 != "") {
            $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing->setName('S2');
            $drawing->setDescription('S2 bordo sinistro');
            $drawing->setPath('img/' . $s2);
            $coordinate = $i + 3;
            $drawing->setCoordinates('A' . $coordinate);
            $drawing->setHeight(100);
            $drawing->setWidth(100);
            $drawing->setWorksheet($spreadsheet->getActiveSheet());
        }
        if ($s3 != "") {
            $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing->setName('S3');
            $drawing->setDescription('S3 timbro');
            $drawing->setPath('img/' . $s3);
            $coordinate = $i + 3;
            $drawing->setCoordinates('L' . $coordinate);
            $drawing->setHeight(100);
            $drawing->setWidth(100);
            $drawing->setWorksheet($spreadsheet->getActiveSheet());
        }
    }
}

?>