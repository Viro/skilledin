<?php
class ModelMoodleReport extends Model {
	
	public function getUtentiCorso($corso_id, $aula_id) {
		
		$arrayUtenti = array();
		$strAnd  = '';
		
		if ($aula_id) {
			$strAnd .= " and objectid = $aula_id ";
		}
		
		$query = $this->db2->query("SELECT * FROM `mdl_logstore_standard_log` sl WHERE `courseid` = $corso_id and eventname like '%meeting_left' $strAnd order by sl.timecreated DESC");
		
		foreach ($query->rows as $key => $row) {
			
			$id = $row['id'];
			$user_id = $row['userid'];
			$objectid = $row['objectid'];
			$module = $row['component'];
			
			$query1 = $this->db2->query("SELECT * FROM mdl_user WHERE id = $user_id ");
			
			$nome = $query1->row['firstname'];
			$cognome = $query1->row['lastname'];
			
			$oraUscita = date('d/m/Y H:i:s', $row['timecreated']);
			
			$query3 = $this->db2->query("SELECT * FROM `mdl_logstore_standard_log` 
			WHERE `courseid` = $corso_id and userid = $user_id and eventname like '%meeting_joined' and id < $id order by timecreated DESC limit 1");
			
			$oraIngresso = date('d/m/Y H:i:s', $query3->row['timecreated']);
			
			$intervallo = ( $row['timecreated'] - $query3->row['timecreated'] );
			
			$arrayUtenti[$key]['user_id'] = $user_id;
			$arrayUtenti[$key]['nome'] = $nome;
			$arrayUtenti[$key]['cognome'] = $cognome;
			$arrayUtenti[$key]['oraIngresso'] = $oraIngresso;
			$arrayUtenti[$key]['oraUscita'] = $oraUscita;
			$arrayUtenti[$key]['intervallo'] = sprintf('%02d:%02d:%02d', ($intervallo /3600),($intervallo /60%60), $intervallo %60);
			$arrayUtenti[$key]['nomeAula'] = $this->getNomeAula($module, $objectid, $corso_id);
			
		}
		
		return $arrayUtenti;
	}
	
	private function getNomeAula($module, $aula_id, $corso_id)
	{
		if ($module == 'mod_bigbluebuttonbn') {
			$query = $this->db2->query("SELECT * FROM `mdl_bigbluebuttonbn` where id = $aula_id and course = $corso_id");
			if ($query->num_rows) {
				return $query->row['name'];
			}
			
		}	
		
	}
	
}