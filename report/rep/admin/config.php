<?php
// HTTP
define('HTTP_SERVER', 'https://update.corsinrete.com/skilledin/report/rep/admin/');
define('HTTP_CATALOG', 'https://update.corsinrete.com/skilledin/report/rep/');

// HTTPS
define('HTTPS_SERVER', 'https://update.corsinrete.com/skilledin/report/rep/admin/');
define('HTTPS_CATALOG', 'https://update.corsinrete.com/skilledin/report/rep/');

// DIR
define('DIR_APPLICATION', '/home/admin/public_html/skilledin/report/rep/admin/');
define('DIR_SYSTEM', '/home/admin/public_html/skilledin/report/rep/system/');
define('DIR_IMAGE', '/home/admin/public_html/skilledin/report/rep/image/');
define('DIR_STORAGE', '/home/admin/public_html/skilledin/report/rep/system/storage/');
define('DIR_CATALOG', '/home/admin/public_html/skilledin/report/rep/catalog/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/template/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD', DIR_STORAGE . 'download/');
define('DIR_LOGS', DIR_STORAGE . 'logs/');
define('DIR_MODIFICATION', DIR_STORAGE . 'modification/');
define('DIR_SESSION', DIR_STORAGE . 'session/');
define('DIR_UPLOAD', DIR_STORAGE . 'upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', '77.39.212.59');
define('DB_USERNAME', 'admin_rep');
define('DB_PASSWORD', 'LZ$=?1SM8Dkm');
define('DB_DATABASE', 'admin_rep');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
/*
define('DB_DRIVER_MOODLE', 'mysqli');
define('DB_HOSTNAME_MOODLE', 'localhost');
define('DB_USERNAME_MOODLE', 'root');
define('DB_PASSWORD_MOODLE', '');
define('DB_DATABASE_MOODLE', 'moodle');
define('DB_PORT_MOODLE', '3306');
*/
// OpenCart API
define('OPENCART_SERVER', 'https://www.opencart.com/');
