<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* repnuovo/tempopiattaforma_list.twig */
class __TwigTemplate_35a0457ebcb40a64885542d346925a1037b96eab3bf27cce7ed87e063a90f060 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo ($context["column_left"] ?? null);
        echo "
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"pull-right\"><a href=\"";
        // line 5
        echo ($context["add"] ?? null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo ($context["button_add"] ?? null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-plus\"></i></a> <a href=\"";
        echo ($context["repair"] ?? null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo ($context["button_rebuild"] ?? null);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-refresh\"></i></a>
        <button type=\"button\" data-toggle=\"tooltip\" title=\"";
        // line 6
        echo ($context["button_delete"] ?? null);
        echo "\" class=\"btn btn-danger\" onclick=\"confirm('";
        echo ($context["text_confirm"] ?? null);
        echo "') ? \$('#form-category').submit() : false;\"><i class=\"fa fa-trash-o\"></i></button>
      </div>
      <h1>Corsi</h1>
      <ul class=\"breadcrumb\">
        ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 11
            echo "        <li><a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 11);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 11);
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "      </ul>
    </div>
  </div>
  <div class=\"container-fluid\">
    ";
        // line 17
        if (($context["error_warning"] ?? null)) {
            // line 18
            echo "    <div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo ($context["error_warning"] ?? null);
            echo "
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
    ";
        }
        // line 22
        echo "    ";
        if (($context["success"] ?? null)) {
            // line 23
            echo "    <div class=\"alert alert-success alert-dismissible\"><i class=\"fa fa-check-circle\"></i> ";
            echo ($context["success"] ?? null);
            echo "
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
    ";
        }
        // line 27
        echo "    
    <div class=\"panel panel-default\">
      <div class=\"panel-heading\">
        <h3 class=\"panel-title\"><i class=\"fa fa-list\"></i> Lista corsi</h3>
      </div>
      <div class=\"panel-body\">
        <form action=\"";
        // line 33
        echo ($context["stampa"] ?? null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-category\">
        
        <input type=\"hidden\" name=\"enterprise\" value=\"";
        // line 35
        echo ($context["enterprise"] ?? null);
        echo "\" />
        <input type=\"hidden\" name=\"platf\" value=\"";
        // line 36
        echo ($context["platf"] ?? null);
        echo "\" />
        <input type=\"hidden\" name=\"start\" value=\"";
        // line 37
        echo ($context["start"] ?? null);
        echo "\" />
        <input type=\"hidden\" name=\"end\" value=\"";
        // line 38
        echo ($context["end"] ?? null);
        echo "\" />
        <input type=\"hidden\" name=\"end\" value=\"";
        // line 39
        echo ($context["scarica"] ?? null);
        echo "\" />
        
          <div class=\"table-responsive\">
            <table class=\"table table-bordered table-hover\">
              <thead>
                <tr>
                  <td style=\"width: 1px;\" class=\"text-center\"><input type=\"checkbox\" onclick=\"\$('input[name*=\\'selected\\']').prop('checked', this.checked);\" /></td>
                  <td class=\"text-left\">";
        // line 46
        if ((($context["sort"] ?? null) == "name")) {
            // line 47
            echo "                    <a href=\"";
            echo ($context["sort_name"] ?? null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, ($context["order"] ?? null));
            echo "\">Id Utente</a>
                    ";
        } else {
            // line 49
            echo "                    <a href=\"";
            echo ($context["sort_name"] ?? null);
            echo "\">Id Utente</a>
                    ";
        }
        // line 50
        echo "</td>
                  <td class=\"text-right\">";
        // line 51
        if ((($context["sort"] ?? null) == "sort_username")) {
            // line 52
            echo "                    <a href=\"";
            echo ($context["sort_username"] ?? null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, ($context["order"] ?? null));
            echo "\">Username</a>
                    ";
        } else {
            // line 54
            echo "                    <a href=\"";
            echo ($context["sort_username"] ?? null);
            echo "\">Username</a>
                    ";
        }
        // line 55
        echo "</td>
                  <td class=\"text-right\">Nome cognome</td>
                  <td class=\"text-right\">Codice fiscale</td>
                  <td class=\"text-right\">Email</td>
                  <td class=\"text-right\">Data di nascita</td>
                  <td class=\"text-right\">Luogo di nascita</td>
                  <td class=\"text-right\">Telefono</td>
                  <td class=\"text-right\">Azienda</td>
                  <td class=\"text-right\">Tempo cumulato</td>
                  ";
        // line 64
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["giorni"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["giorno"]) {
            // line 65
            echo "                  \t<td class=\"text-right\">";
            echo $context["giorno"];
            echo "</td>
                  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['giorno'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 67
        echo "                </tr>
              </thead>
              <tbody>
                ";
        // line 70
        if (($context["arrTempoInPiattaforma"] ?? null)) {
            // line 71
            echo "                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["arrTempoInPiattaforma"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["row"]) {
                // line 72
                echo "                <tr>
                  <td class=\"text-center\">";
                // line 73
                if (twig_in_filter(twig_get_attribute($this->env, $this->source, $context["row"], "userid", [], "any", false, false, false, 73), ($context["selected"] ?? null))) {
                    // line 74
                    echo "                    <input type=\"checkbox\" name=\"selected[]\" value=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["row"], "userid", [], "any", false, false, false, 74);
                    echo "\" checked=\"checked\" />
                    ";
                } else {
                    // line 76
                    echo "                    <input type=\"checkbox\" name=\"selected[]\" value=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["row"], "userid", [], "any", false, false, false, 76);
                    echo "\" />
                    ";
                }
                // line 77
                echo "</td>
                    
                    ";
                // line 79
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($context["row"]);
                foreach ($context['_seq'] as $context["_key"] => $context["valori"]) {
                    // line 80
                    echo "                    \t<td class=\"text-left\">";
                    echo $context["valori"];
                    echo "</td>
                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['valori'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 82
                echo "                  <!-- <td class=\"text-left\">";
                echo twig_get_attribute($this->env, $this->source, $context["row"], "userid", [], "any", false, false, false, 82);
                echo "</td>
                  <td class=\"text-left\">";
                // line 83
                echo twig_get_attribute($this->env, $this->source, $context["row"], "username", [], "any", false, false, false, 83);
                echo "</td>
                  <td class=\"text-right\">";
                // line 84
                echo twig_get_attribute($this->env, $this->source, $context["row"], "name", [], "any", false, false, false, 84);
                echo "</td>
                  <td class=\"text-right\">";
                // line 85
                echo twig_get_attribute($this->env, $this->source, $context["row"], "cf", [], "any", false, false, false, 85);
                echo "</td>
                  <td class=\"text-right\">";
                // line 86
                echo twig_get_attribute($this->env, $this->source, $context["row"], "email", [], "any", false, false, false, 86);
                echo "</td>
                  <td class=\"text-right\">";
                // line 87
                echo twig_get_attribute($this->env, $this->source, $context["row"], "data_nascita", [], "any", false, false, false, 87);
                echo "</td>
                  <td class=\"text-right\">";
                // line 88
                echo twig_get_attribute($this->env, $this->source, $context["row"], "citta", [], "any", false, false, false, 88);
                echo "</td>
                  <td class=\"text-right\">";
                // line 89
                echo twig_get_attribute($this->env, $this->source, $context["row"], "telefono", [], "any", false, false, false, 89);
                echo "</td>
                  <td class=\"text-right\">";
                // line 90
                echo twig_get_attribute($this->env, $this->source, $context["row"], "azienda", [], "any", false, false, false, 90);
                echo "</td>
                  <td class=\"text-right\">";
                // line 91
                echo twig_get_attribute($this->env, $this->source, $context["row"], "tempoCumulato", [], "any", false, false, false, 91);
                echo "</td> -->
                  
                  
                </tr>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['row'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 96
            echo "                ";
        } else {
            // line 97
            echo "                <tr>
                  <td class=\"text-center\" colspan=\"4\">";
            // line 98
            echo ($context["text_no_results"] ?? null);
            echo "</td>
                </tr>
                ";
        }
        // line 101
        echo "              </tbody>
            </table>
          </div>
        </form>
        <div class=\"row\">
          <div class=\"col-sm-6 text-left\">";
        // line 106
        echo ($context["pagination"] ?? null);
        echo "</div>
          <div class=\"col-sm-6 text-right\">";
        // line 107
        echo ($context["results"] ?? null);
        echo "</div>
        </div>
      </div>
    </div>
  </div>
</div>
";
        // line 113
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "repnuovo/tempopiattaforma_list.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  330 => 113,  321 => 107,  317 => 106,  310 => 101,  304 => 98,  301 => 97,  298 => 96,  287 => 91,  283 => 90,  279 => 89,  275 => 88,  271 => 87,  267 => 86,  263 => 85,  259 => 84,  255 => 83,  250 => 82,  241 => 80,  237 => 79,  233 => 77,  227 => 76,  221 => 74,  219 => 73,  216 => 72,  211 => 71,  209 => 70,  204 => 67,  195 => 65,  191 => 64,  180 => 55,  174 => 54,  166 => 52,  164 => 51,  161 => 50,  155 => 49,  147 => 47,  145 => 46,  135 => 39,  131 => 38,  127 => 37,  123 => 36,  119 => 35,  114 => 33,  106 => 27,  98 => 23,  95 => 22,  87 => 18,  85 => 17,  79 => 13,  68 => 11,  64 => 10,  55 => 6,  45 => 5,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "repnuovo/tempopiattaforma_list.twig", "");
    }
}
