<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* repnuovo/tempopiattaforma_list.twig */
class __TwigTemplate_ddf97c38059d9451a63371e12ea8518f509b6ecb9f0c0310b3fa15bfd8d06fbf extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo ($context["column_left"] ?? null);
        echo "
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"pull-right\"><a href=\"";
        // line 5
        echo ($context["add"] ?? null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo ($context["button_add"] ?? null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-plus\"></i></a> <a href=\"";
        echo ($context["repair"] ?? null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo ($context["button_rebuild"] ?? null);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-refresh\"></i></a>
        <button type=\"button\" data-toggle=\"tooltip\" title=\"";
        // line 6
        echo ($context["button_delete"] ?? null);
        echo "\" class=\"btn btn-danger\" onclick=\"confirm('";
        echo ($context["text_confirm"] ?? null);
        echo "') ? \$('#form-category').submit() : false;\"><i class=\"fa fa-trash-o\"></i></button>
      </div>
      <h1>Corsi</h1>
      <ul class=\"breadcrumb\">
        ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 11
            echo "        <li><a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 11);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 11);
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "      </ul>
    </div>
  </div>
  <div class=\"container-fluid\">
    ";
        // line 17
        if (($context["error_warning"] ?? null)) {
            // line 18
            echo "    <div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo ($context["error_warning"] ?? null);
            echo "
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
    ";
        }
        // line 22
        echo "    ";
        if (($context["success"] ?? null)) {
            // line 23
            echo "    <div class=\"alert alert-success alert-dismissible\"><i class=\"fa fa-check-circle\"></i> ";
            echo ($context["success"] ?? null);
            echo "
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
    ";
        }
        // line 27
        echo "    
    <div class=\"panel panel-default\">
      <div class=\"panel-heading\">
        <h3 class=\"panel-title\"><i class=\"fa fa-list\"></i> Lista corsi</h3>
      </div>
      <div class=\"panel-body\">
        <form action=\"";
        // line 33
        echo ($context["delete"] ?? null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-category\">
          <div class=\"table-responsive\">
            <table class=\"table table-bordered table-hover\">
              <thead>
                <tr>
                  <td style=\"width: 1px;\" class=\"text-center\"><input type=\"checkbox\" onclick=\"\$('input[name*=\\'selected\\']').prop('checked', this.checked);\" /></td>
                  <td class=\"text-left\">";
        // line 39
        if ((($context["sort"] ?? null) == "name")) {
            // line 40
            echo "                    <a href=\"";
            echo ($context["sort_name"] ?? null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, ($context["order"] ?? null));
            echo "\">Id Utente</a>
                    ";
        } else {
            // line 42
            echo "                    <a href=\"";
            echo ($context["sort_name"] ?? null);
            echo "\">Id Utente</a>
                    ";
        }
        // line 43
        echo "</td>
                  <td class=\"text-right\">";
        // line 44
        if ((($context["sort"] ?? null) == "sort_username")) {
            // line 45
            echo "                    <a href=\"";
            echo ($context["sort_username"] ?? null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, ($context["order"] ?? null));
            echo "\">Username</a>
                    ";
        } else {
            // line 47
            echo "                    <a href=\"";
            echo ($context["sort_username"] ?? null);
            echo "\">Username</a>
                    ";
        }
        // line 48
        echo "</td>
                  <td class=\"text-right\">Nome cognome</td>
                  <td class=\"text-right\">Codice fiscale</td>
                  <td class=\"text-right\">Email</td>
                  <td class=\"text-right\">Data di nascita</td>
                  <td class=\"text-right\">Luogo di nascita</td>
                  <td class=\"text-right\">Telefono</td>
                  <td class=\"text-right\">Azienda</td>
                  <td class=\"text-right\">Tempo cumulato</td>
                  <td class=\"text-right\">";
        // line 57
        echo ($context["column_action"] ?? null);
        echo "</td>
                </tr>
              </thead>
              <tbody>
                ";
        // line 61
        if (($context["arrTempoInPiattaforma"] ?? null)) {
            // line 62
            echo "                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["arrTempoInPiattaforma"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["row"]) {
                // line 63
                echo "                <tr>
                  <td class=\"text-center\">";
                // line 64
                if (twig_in_filter(twig_get_attribute($this->env, $this->source, $context["row"], "userid", [], "any", false, false, false, 64), ($context["selected"] ?? null))) {
                    // line 65
                    echo "                    <input type=\"checkbox\" name=\"selected[]\" value=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["row"], "userid", [], "any", false, false, false, 65);
                    echo "\" checked=\"checked\" />
                    ";
                } else {
                    // line 67
                    echo "                    <input type=\"checkbox\" name=\"selected[]\" value=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["row"], "userid", [], "any", false, false, false, 67);
                    echo "\" />
                    ";
                }
                // line 68
                echo "</td>
                  <td class=\"text-left\">";
                // line 69
                echo twig_get_attribute($this->env, $this->source, $context["row"], "username", [], "any", false, false, false, 69);
                echo "</td>
                  <td class=\"text-right\">";
                // line 70
                echo twig_get_attribute($this->env, $this->source, $context["row"], "name", [], "any", false, false, false, 70);
                echo "</td>
                  <td class=\"text-right\">";
                // line 71
                echo twig_get_attribute($this->env, $this->source, $context["row"], "cf", [], "any", false, false, false, 71);
                echo "</td>
                  <td class=\"text-right\">";
                // line 72
                echo twig_get_attribute($this->env, $this->source, $context["row"], "email", [], "any", false, false, false, 72);
                echo "</td>
                  <td class=\"text-right\">";
                // line 73
                echo twig_get_attribute($this->env, $this->source, $context["row"], "data_nascita", [], "any", false, false, false, 73);
                echo "</td>
                  <td class=\"text-right\">";
                // line 74
                echo twig_get_attribute($this->env, $this->source, $context["row"], "citta", [], "any", false, false, false, 74);
                echo "</td>
                  <td class=\"text-right\">";
                // line 75
                echo twig_get_attribute($this->env, $this->source, $context["row"], "azienda", [], "any", false, false, false, 75);
                echo "</td>
                  <td class=\"text-right\">";
                // line 76
                echo twig_get_attribute($this->env, $this->source, $context["row"], "telefono", [], "any", false, false, false, 76);
                echo "</td>
                  <td class=\"text-right\">";
                // line 77
                echo twig_get_attribute($this->env, $this->source, $context["row"], "tempoCumulato", [], "any", false, false, false, 77);
                echo "</td>
                  <td class=\"text-right\"><a href=\"";
                // line 78
                echo twig_get_attribute($this->env, $this->source, $context["row"], "edit", [], "any", false, false, false, 78);
                echo "\" data-toggle=\"tooltip\" title=\"";
                echo ($context["button_edit"] ?? null);
                echo "\" class=\"btn btn-primary\"><i class=\"fa fa-pencil\"></i></a></td>
                </tr>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['row'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 81
            echo "                ";
        } else {
            // line 82
            echo "                <tr>
                  <td class=\"text-center\" colspan=\"4\">";
            // line 83
            echo ($context["text_no_results"] ?? null);
            echo "</td>
                </tr>
                ";
        }
        // line 86
        echo "              </tbody>
            </table>
          </div>
        </form>
        <div class=\"row\">
          <div class=\"col-sm-6 text-left\">";
        // line 91
        echo ($context["pagination"] ?? null);
        echo "</div>
          <div class=\"col-sm-6 text-right\">";
        // line 92
        echo ($context["results"] ?? null);
        echo "</div>
        </div>
      </div>
    </div>
  </div>
</div>
";
        // line 98
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "repnuovo/tempopiattaforma_list.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  282 => 98,  273 => 92,  269 => 91,  262 => 86,  256 => 83,  253 => 82,  250 => 81,  239 => 78,  235 => 77,  231 => 76,  227 => 75,  223 => 74,  219 => 73,  215 => 72,  211 => 71,  207 => 70,  203 => 69,  200 => 68,  194 => 67,  188 => 65,  186 => 64,  183 => 63,  178 => 62,  176 => 61,  169 => 57,  158 => 48,  152 => 47,  144 => 45,  142 => 44,  139 => 43,  133 => 42,  125 => 40,  123 => 39,  114 => 33,  106 => 27,  98 => 23,  95 => 22,  87 => 18,  85 => 17,  79 => 13,  68 => 11,  64 => 10,  55 => 6,  45 => 5,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "repnuovo/tempopiattaforma_list.twig", "");
    }
}
