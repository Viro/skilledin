function getColorFromID(id, steps) {
  let center = 128;
  let width = 120;
  let frequency = 2 * Math.PI / steps;

  var c = makeColorGradient(frequency, frequency, frequency, 0, 2, 4, id, center, width);
  return c;
}


function ColorLuminance(hex, lum) {

  // validate hex string
  hex = String(hex).replace(/[^0-9a-f]/gi, '');
  if (hex.length < 6) {
    hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
  }
  lum = lum || 0;

  // convert to decimal and change luminosity
  var rgb = "#", c, i;
  for (i = 0; i < 3; i++) {
    c = parseInt(hex.substr(i * 2, 2), 16);
    c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
    rgb += ("00" + c).substr(c.length);
  }

  return rgb;
}

//The explanation of this feature is here: http://krazydad.com/tutorials/makecolors.php

function makeColorGradient(frequency1, frequency2, frequency3,
  phase1, phase2, phase3, i,
  center, width) {
  if (center == undefined) center = 128;
  if (width == undefined) width = 127;

  var red = Math.sin(frequency1 * i + phase1) * width + center;
  var grn = Math.sin(frequency2 * i + phase2) * width + center;
  var blu = Math.sin(frequency3 * i + phase3) * width + center;
  return rgbToHex(red, grn, blu);

}

function hexToRgb(hex) {
  var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return result ? {
    r: parseInt(result[1], 16),
    g: parseInt(result[2], 16),
    b: parseInt(result[3], 16)
  } : null;
}


function rgbToHex(red, green, blue) {
  var rgb = blue | (green << 8) | (red << 16);
  return '#' + (0x1000000 + rgb).toString(16).slice(1)
}
