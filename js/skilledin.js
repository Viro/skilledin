function getTableUsers(){
    var table = $('#table_user_list').DataTable({
        "dom"      : "Bfrtip",
        "ajax"      : {
            "url"     : "../report/response/user/user.php",
            "method"  : "POST",
            "dataSrc" : "",
            "data"    : {
                "action" : "getUsers"
            }
        },
        "stateSave": true,
        "language" : {
            "lengthMenu"  : "Mostra _MENU_ righe per pagina",
            "zeroRecords" : "Nessun elemento trovato",
            "info"        : "Mostrando pagina _PAGE_ di _PAGES_ su _TOTAL_ righe",
            "infoEmpty"   : "Nessuna riga disponibile",
            "search"      : "Cerca",
            "paginate"    : {
                "previous" : "Precedente",
                "next"     : "Successivo"
            }
        },
        "lengthMenu" : [[5, 10, 25, 50, 150, 300], [5, 10, 25, 50, 150, 300]],
        "autoWidth"  : false,
        "scrollY"    : 350,
        "scrollX"    : 300,
        "columns"    : [
            {"title" : "ID"    , "data" : "id"},
            {"title" : "Email" , "data" : "email"},
            {"title" : "Ruolo" , "data" : "role"},
            {"title" : "Data"  , "data" : "date"},
            {"title" : "Orario", "data" : "time"},
            {"title" : ""      , "data" : "action", "orderable": false}
        ],
        "columnDefs": [
            {
                "targets"        : -1,
                "data"           : null,
                "defaultContent" : "<button class=\"btn btn-primary\"><i class=\"fa fa-pencil\"></i></button>",
            },
        ],
        "order" : [[1, "asc"]]
    });
    $('#table_user_list tbody').on('click', 'button', function () {
        var data = table.row($(this).parents('tr')).data();

        //Azione di apertura form di modfica
        var form           = document.createElement("form");
        form.method = "POST";
        form.action = "https://update.corsinrete.com/skilledin/report/editUser.php";

        var action         = document.createElement("input");
        action.name          = "action";
        action.value         = "editUser";
        form.appendChild(action);

        var update_user_id = document.createElement("input");
        update_user_id.name  = "update_user_id";
        update_user_id.value = data['id'];
        form.appendChild(update_user_id);

        document.body.appendChild(form);

        form.submit();
    });
}
function updateCompaniesFromPlatform(platform_id){
    var data = "action=getHtmlSelectCompanies&platform_id=" + platform_id;
    $.ajax({
        url: 'https://update.corsinrete.com/skilledin/report/response/company/company.php',
        data: data,
        type: 'POST',
        dataType: 'json',
        success: function(companies) {
            $("#select_company_name").select2({
                placeholder: "Seleziona azienda...",
                data:companies,
            });
            $("#select_company_name_2").select2({
                placeholder: "Seleziona azienda...",
                data:companies,
            });
        }
    });
}

function updateUserFromCompany(company_name, platform_id){
    console.log("Company name : " + company_name);
    console.log("platform id : " + platform_id);
    var data = "action=getHtmlSelectUserCompany&company_name=" + encodeURIComponent(company_name) + '&platform_id=' + platform_id;
    console.log(data);
    $.ajax({
        url: 'https://update.corsinrete.com/skilledin/report/response/company/company.php',
        data: data,
        type: 'POST',
        dataType: 'json',
        success: function(users) {
            console.log(users);
            $("#select_user_id").select2({
                placeholder: "Seleziona utente...",
                data:users,
            });
        }
    });
}

function updatePlansFromCompany(company_name){
    var data = "action=getHtmlSelectPlanCompany&company_name=" + encodeURIComponent(company_name);
    console.log(data);
    $.ajax({
        url: 'https://update.corsinrete.com/skilledin/report/response/company/company.php',
        data: data,
        type: 'POST',
        dataType: 'json',
        success: function(users) {
            console.log(users);
            $("#select_plan_id").select2({
                placeholder: "Seleziona piano...",
                data:users,
            });
        }
    });
}

function updateCoursesFromPlatform(platform_id){
    var data = "action=getHtmlSelectCourses&platform_id=" + platform_id;
    $.ajax({
        url: 'https://update.corsinrete.com/skilledin/report/response/course/course.php',
        data: data,
        type: 'POST',
        dataType: 'json',
        success: function(courses) {
            $("#select_course_id").select2({
                placeholder: "Seleziona corso...",
                data:courses,
            });
        }
    });
}

function updateCoursesFromPlatformForPlan(platform_id, piano_formativo_id){
    var data = 'action=getHtmlSelectCourses&platform_id=' + platform_id + '&piano_formativo_id=' + piano_formativo_id;

    $.ajax({
        url: 'https://update.corsinrete.com/skilledin/report/response/course/course.php',
        data: data,
        type: 'POST',
        dataType: 'json',
        success: function(course_modules) {
            console.log(course_modules);
            $("#select_course_id").select2({
                placeholder: "Seleziona corso...",
                data:course_modules,
            });
        }
    });
}

function updateCoursesMoudleFromCourse(course_id, platform_id){
    var data = "action=getHtmlSelectCourseModules&course_id=" + course_id + '&platform_id=' + platform_id;

    $.ajax({
        url: 'https://update.corsinrete.com/skilledin/report/response/course/course.php',
        data: data,
        type: 'POST',
        dataType: 'json',
        success: function(course_modules) {
            console.log(course_modules);
            $("#select_course_module_id").select2({
                placeholder: "Seleziona corso...",
                data:course_modules,
            });
        }
    });
}



function showCourseInfo(course_id, platform_id){
    var data = "action=showCourseInfo&course_id=" + course_id + '&platform_id=' + platform_id;

    $.ajax({
        url: 'https://update.corsinrete.com/skilledin/report/response/course/course.php',
        data: data,
        type: 'POST',
        success: function(html) {
            $("#table_course_data").html(html);
        }
    });
}

function syncCourseInfo(course_id, platform_id){
    var data = "action=syncCourseInfo&course_id=" + course_id + '&platform_id=' + platform_id;

    $.ajax({
        url: 'https://update.corsinrete.com/skilledin/report/response/course/course.php',
        data: data,
        type: 'POST',
        success: function(response) {
            $("#result_course_data").html(response);
            $("#show-result").show();
        }
    });
}


function reloadPlatforms(){
    var data = "action=getHtmlSelectPlatforms";

    $.ajax({
        url: 'https://update.corsinrete.com/skilledin/report/response/common/system.php',
        data: data,
        type: 'POST',
        dataType: 'json',
        success: function(platforms) {
            console.log(platforms);
            $("#select_platform_id").select2({
                placeholder: "Seleziona piattaforma...",
                data:platforms,
            });
        }
    });
}
