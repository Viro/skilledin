var idRuolo = 2;
var checked = ["vuoto"];
function showPleaseWait(message) {
    if (document.querySelector("#pleaseWaitDialog") == null) {
        var modalLoading = '<div class="modal" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false" role="dialog">\
            <div class="modal-dialog">\
                <div class="modal-content">\
                    <div class="modal-header">\
                        <h4 id="modalMessaggioWait" class="modal-title">' + message + '</h4>\
                    </div>\
                    <div class="modal-body">\
                        <div class="progress">\
                          <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar"\
                          aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%; height: 40px">\
                          </div>\
                        </div>\
                    </div>\
                </div>\
            </div>\
        </div>';
        $(document.body).append(modalLoading);
    } else {
        document.getElementById('modalMessaggioWait').innerHTML = message;
    }

    $('#pleaseWaitDialog').modal({backdrop: 'static', keyboard: false});
    $("#pleaseWaitDialog").modal("show");
}


function hidePleaseWait() {
    $("#pleaseWaitDialog").modal("hide");
}



function timeinReport(array, mesi) {


    cleanDataTable();
    var thead = document.getElementById("report_head");
    thead.innerHTML = "";
    var trh = document.createElement('tr');
    trh.innerHTML =
            '<th class="userid"> Userid </th>' +
            '<th class="username"> Username </th>' +
            '<th class="name"> Nome Cognome </th>' +
            '<th class="cf"> Codice Fiscale </th>' +
            '<th class="email"> Email </th>' +
            '<th class="dn"> Data di Nascita </th>' +
            '<th class="ln"> Luogo di Nascita </th>' +
            '<th class="phone">N.di telefono </th>' +
            '<th class="azienda"> Azienda </th>' +
            '<th class="tc"> Tempo cumulato </th>';
    for (var i = 0; i < mesi.length; i++) {
        trh.innerHTML += '<th class="data_' + i + '">' + mesi[i] + '</th>';
    }
    thead.appendChild(trh);
    var tbody = document.getElementById("report_body");
    var nav = document.getElementById("nav");
    nav.innerHTML = "";
    tbody.innerHTML = "";
    for (var i = 0; i < array.length; i++) {
        var tr = document.createElement('tr');
        tr.innerHTML = '<td class="userid">' + array[i]['userid'] + '</td>' +
                '<td class="username">' + array[i]['username'] + '</td>' +
                '<td class="name">' + array[i]['name'] + '</td>' +
                '<td class="cf">' + array[i]['cf'] + '</td>' +
                '<td class="email">' + array[i]['email'] + '</td>' +
                '<td class="dn">' + array[i]['data_nascita'] + '</td>' +
                '<td class="ln">' + array[i]['citta'] + '</td>' +
                '<td class="phone">' + array[i]['phone'] + '</td>' +
                '<td class="azienda">' + array[i]['azienda'] + '</td>' +
                '<td class="tc">' + array[i]['tempo_tot'] + '</td>';
        var j = 0;
        for (const [key, value] of Object.entries(array[i]['mesi'])) {
            tr.innerHTML += '<td class="data_' + j + '">' + value + '</td>';
            j++;
        }
        tbody.appendChild(tr);
    }
    createDataTable();
}


function rhvpReport(array) {

    cleanDataTable();
    $('#report').DataTable({
        "data": array,
        dom: "Bfrtip",
        stateSave: true,
        buttons: [
            {
                extend: 'colvis',
                text: 'Colonne da non visualizzare',
                collectionLayout: 'fixed two-column'
            }
        ],
        "language": {
            "lengthMenu": "Mostra _MENU_ righe per pagina",
            "zeroRecords": "Nessun elemento trovato",
            "info": "Mostrando pagina _PAGE_ di _PAGES_ su _TOTAL_ righe",
            "infoEmpty": "Nessuna riga disponibile",
            "search": "Cerca",
            "paginate": {
                "previous": "Precedente",
                "next": "Successivo"
            }
        },
        "lengthMenu": [[50, 150, 300], [50, 150, 300]],
        autoWidth: false,
        scrollY: 300,
        scrollX: 300,
        "columns": [
            {"title": "Userid", "class": "userid", "data": "userid"},
            {"title": "Username", "class": "username", "data": "username"},
            {"title": "Nome Cognome", "class": "name", "data": "name"},
            {"title": "Codice Fiscale", "class": "cf", "data": "cf"},
            {"title": "Email", "class": "email", "data": "email"},
            {"title": "Data di Nascita", "class": "dn", "data": "data_nascita"},
            {"title": "Luogo di Nascita", "class": "ln", "data": "citta"},
            {"title": "N.di telefono", "class": "phone", "data": "phone"},
            {"title": "Totale effettivo di fruizione", "class": "orarioA", "data": "totalVisto"},
            {"title": "Durata complessiva", "class": "orarioL", "data": "totalCorso"},
            {"title": "Differenza", "class": "ts", "data": "diff"}
        ],
        "order": [[3, "asc"]]
    });
}


function sintesiReport(array) {


    cleanDataTable();
    var table;
    const createdCell = function (cell) {
        let original;
        let className;
        cell.setAttribute('contenteditable', true);
        cell.setAttribute('spellcheck', false);
        cell.addEventListener('focus', function (e) {
            className = cell.className;
            original = e.target.textContent;
        });
        cell.addEventListener('blur', function (e) {
            if (original !== e.target.textContent) {
                const row = table.row(e.target.parentElement);
                var data = row.data();
                if (className === "orarioA") {
                    updateField(data.id, data.id_logout);
                } else {
                    updateField(data.id_logout, data.id);
                }
            }
        });
    };
    if (idRuolo == 0) {
        table = $('#report').DataTable({
            "data": array,
            dom: "Bfrtip",
            stateSave: true,
            buttons: [
                {
                    extend: 'colvis',
                    text: 'Colonne da non visualizzare',
                    collectionLayout: 'fixed two-column'
                }
            ],
            "language": {
                "lengthMenu": "Mostra _MENU_ righe per pagina",
                "zeroRecords": "Nessun elemento trovato",
                "info": "Mostrando pagina _PAGE_ di _PAGES_ su _TOTAL_ righe",
                "infoEmpty": "Nessuna riga disponibile",
                "search": "Cerca",
                "paginate": {
                    "previous": "Precedente",
                    "next": "Successivo"
                }
            },
            "lengthMenu": [[50, 150, 300], [50, 150, 300]],
            autoWidth: false,
            scrollY: 300,
            scrollX: 300,
            "columns": [
                {"title": "Data", "class": "data", "data": "timecreated"},
                {"title": "Userid", "class": "userid", "data": "userid"},
                {"title": "Username", "class": "username", "data": "username"},
                {"title": "Nome Cognome", "class": "name", "data": "name"},
                {"title": "Codice Fiscale", "class": "cf", "data": "cf"},
                {"title": "Email", "class": "email", "data": "email"},
                {"title": "Data di Nascita", "class": "dn", "data": "data_nascita"},
                {"title": "Luogo di Nascita", "class": "ln", "data": "citta"},
                {"title": "N.di telefono", "class": "phone", "data": "phone"},
                {"title": "Azienda", "class": "azienda", "data": "azienda"},
                {"title": "Ora ingresso", "class": "orarioA", "data": "orario"},
                {"title": "Ora uscita", "class": "orarioL", "data": "orario_logout"},
                {"title": "Tempo cumulato", "class": "tc", "data": "tempo_cumulato"}
            ],
            columnDefs: [{
                    targets: [10, 11],
                    createdCell: createdCell
                }],
            "order": [[3, "asc"]]
        });
    } else {
        $('#report').DataTable({
            "data": array,
            dom: "Bfrtip",
            stateSave: true,
            buttons: [
                {
                    extend: 'colvis',
                    text: 'Colonne da non visualizzare',
                    collectionLayout: 'fixed two-column'
                }
            ],
            "language": {
                "lengthMenu": "Mostra _MENU_ righe per pagina",
                "zeroRecords": "Nessun elemento trovato",
                "info": "Mostrando pagina _PAGE_ di _PAGES_ su _TOTAL_ righe",
                "infoEmpty": "Nessuna riga disponibile",
                "search": "Cerca",
                "paginate": {
                    "previous": "Precedente",
                    "next": "Successivo"
                }
            },
            "lengthMenu": [[50, 150, 300], [50, 150, 300]],
            autoWidth: false,
            scrollY: 300,
            scrollX: 300,
            "columns": [
                {"title": "Data", "class": "data", "data": "timecreated"},
                {"title": "Userid", "class": "userid", "data": "userid"},
                {"title": "Username", "class": "username", "data": "username"},
                {"title": "Nome Cognome", "class": "name", "data": "name"},
                {"title": "Codice Fiscale", "class": "cf", "data": "cf"},
                {"title": "Email", "class": "email", "data": "email"},
                {"title": "Data di Nascita", "class": "dn", "data": "data_nascita"},
                {"title": "Luogo di Nascita", "class": "ln", "data": "citta"},
                {"title": "N.di telefono", "class": "phone", "data": "phone"},
                {"title": "Azienda", "class": "azienda", "data": "azienda"},
                {"title": "Ora ingresso", "class": "orarioA", "data": "orario"},
                {"title": "Ora uscita", "class": "orarioL", "data": "orario_logout"},
                {"title": "Tempo cumulato", "class": "tc", "data": "tempo_cumulato"}
            ],
            "order": [[3, "asc"]]
        });
    }
}

function sintesiBbb(array, mesi) {

    cleanDataTable();
    var thead = document.getElementById("report_head");
    thead.innerHTML = "";
    var trh = document.createElement('tr');
    trh.innerHTML =
            '<th class="userid"> Userid </th>' +
            '<th class="username"> Username </th>' +
            '<th class="name"> Nome Cognome </th>' +
            '<th class="cf"> Codice Fiscale </th>' +
            '<th class="email"> Email </th>' +
            '<th class="dn"> Data di Nascita </th>' +
            '<th class="ln"> Luogo di Nascita </th>' +
            '<th class="azienda"> Azienda </th>' +
            '<th class="phone">N.di telefono </th>' +
            '<th class="ruolo"> Ruolo </th>' +
            '<th class="tc"> Tempo cumulato </th>';
    for (var i = 0; i < mesi.length; i++) {
        trh.innerHTML += '<th class="data_' + i + '">' + mesi[i] + '</th>';
    }

    thead.appendChild(trh);
    var tbody = document.getElementById("report_body");
    var nav = document.getElementById("nav");
    nav.innerHTML = "";
    tbody.innerHTML = "";
    for (var i = 0; i < array.length; i++) {
        var tr = document.createElement('tr');
        tr.innerHTML = '<td class="userid">' + array[i]['userid'] + '</td>' +
                '<td class="username">' + array[i]['username'] + '</td>' +
                '<td class="name">' + array[i]['name'] + '</td>' +
                '<td class="cf">' + array[i]['cf'] + '</td>' +
                '<td class="email">' + array[i]['email'] + '</td>' +
                '<td class="dn">' + array[i]['data_nascita'] + '</td>' +
                '<td class="ln">' + array[i]['citta'] + '</td>' +
                '<td class="azienda">' + array[i]['azienda'] + '</td>' +
                '<td class="phone">' + array[i]['phone'] + '</td>' +
                '<td class="ruolo">' + array[i]['ruolo'] + '</td>' +
                '<td class="tc">' + array[i]['tempo_tot'] + '</td>';
        var j = 0;
        for (const [key, value] of Object.entries(array[i]['mesi'])) {
            tr.innerHTML += '<td class="data_' + j + '">' + value + '</td>';
            j++;
        }
        tbody.appendChild(tr);
    }
    createDataTable();
}

function sintesiBbbName(array, mesi) {

    cleanDataTable();

    var thead = document.getElementById("report_head");
    thead.innerHTML = "";

    var trh = document.createElement('tr');
    trh.innerHTML =
            '<th class="userid"> Userid </th>' +
            '<th class="username"> Username </th>' +
            '<th class="name"> Nome Cognome </th>' +
            '<th class="cf"> Codice Fiscale </th>' +
            '<th class="email"> Email </th>' +
            '<th class="dn"> Data di Nascita </th>' +
            '<th class="ln"> Luogo di Nascita </th>' +
            '<th class="azienda"> Azienda </th>' +
            '<th class="phone">N.di telefono </th>' +
            '<th class="ruolo"> Ruolo </th>' +
            '<th class="corso"> Corso </th>' +
            '<th class="courseid"> Id Corso </th>' +
            '<th class="tc"> Tempo cumulato </th>';

    for (var i = 0; i < mesi.length; i++) {
        trh.innerHTML += '<th class="data_' + i + '">' + mesi[i] + '</th>';
    }

    thead.appendChild(trh);
    var tbody = document.getElementById("report_body");
    var nav = document.getElementById("nav");
    nav.innerHTML = "";
    tbody.innerHTML = "";
    for (var i = 0; i < array.length; i++) {
        var tr = document.createElement('tr');
        tr.innerHTML = '<td class="userid">' + array[i]['userid'] + '</td>' +
                '<td class="username">' + array[i]['username'] + '</td>' +
                '<td class="name">' + array[i]['name'] + '</td>' +
                '<td class="cf">' + array[i]['cf'] + '</td>' +
                '<td class="email">' + array[i]['email'] + '</td>' +
                '<td class="dn">' + array[i]['data_nascita'] + '</td>' +
                '<td class="ln">' + array[i]['citta'] + '</td>' +
                '<td class="azienda">' + array[i]['azienda'] + '</td>' +
                '<td class="phone">' + array[i]['phone'] + '</td>' +
                '<td class="ruolo">' + array[i]['ruolo'] + '</td>' +
                '<td class="corso">' + array[i]['corso'] + '</td>' +
                '<td class="courseid">' + array[i]['courseid'] + '</td>' +
                '<td class="tc">' + array[i]['tempo_tot'] + '</td>';
        var j = 0;
        for (const [key, value] of Object.entries(array[i]['mesi'])) {
            tr.innerHTML += '<td class="data_' + j + '">' + value + '</td>';
            j++;
        }
        tbody.appendChild(tr);
    }
    createDataTable();
}



function bbbReport(array) {

    cleanDataTable();
    var table;
    const createdCell = function (cell) {
        let original;
        let className;
        cell.setAttribute('contenteditable', true);
        cell.setAttribute('spellcheck', false);
        cell.addEventListener('focus', function (e) {
            className = cell.className;
            original = e.target.textContent;
        });
        cell.addEventListener('blur', function (e) {
            if (original !== e.target.textContent) {
                const row = table.row(e.target.parentElement);
                var data = row.data();
                if (className === "orarioA") {
                    updateField(data.id, data.id_logout);
                } else {
                    updateField(data.id_logout, data.id);
                }


            }
        });
    };
    if (idRuolo == 0) {
        table = $('#report').DataTable({
            "data": array,
            dom: "Bfrtip",
            stateSave: true,
            buttons: [
                {
                    extend: 'colvis',
                    text: 'Colonne da non visualizzare',
                    collectionLayout: 'fixed two-column'
                }
            ],
            "language": {
                "lengthMenu": "Mostra _MENU_ righe per pagina",
                "zeroRecords": "Nessun elemento trovato",
                "info": "Mostrando pagina _PAGE_ di _PAGES_ su _TOTAL_ righe",
                "infoEmpty": "Nessuna riga disponibile",
                "search": "Cerca",
                "paginate": {
                    "previous": "Precedente",
                    "next": "Successivo"
                }
            },
            "lengthMenu": [[50, 150, 300], [50, 150, 300]],
            autoWidth: false,
            scrollY: 300,
            scrollX: 300,
            "columns": [
                {"title": "Data", "class": "data", "data": "timecreated"},
                {"title": "Userid", "class": "userid", "data": "userid"},
                {"title": "Username", "class": "username", "data": "username"},
                {"title": "Nome Cognome", "class": "name", "data": "name"},
                {"title": "Codice Fiscale", "class": "cf", "data": "cf"},
                {"title": "Email", "class": "email", "data": "email"},
                {"title": "Data di Nascita", "class": "dn", "data": "data_nascita"},
                {"title": "Luogo di Nascita", "class": "ln", "data": "citta"},
                {"title": "Azienda", "class": "azienda", "data": "azienda"},
                {"title": "N.di telefono", "class": "phone", "data": "phone"},
                {"title": "Ruolo", "class": "ruolo", "data": "ruolo"},
                {"title": "Corso", "class": "corso", "data": "corso"},
                {"title": "Nome aula", "class": "aula", "data": "aula"},
                {"title": "Ora ingresso", "class": "orarioA", "data": "orario"},
                {"title": "Ora uscita", "class": "orarioL", "data": "orario_logout"},
                {"title": "Tempo cumulato", "class": "tc", "data": "tempo_cumulato"}
            ],
            columnDefs: [{
                    targets: [13, 14],
                    createdCell: createdCell
                }],
            "order": [[3, "asc"]]
        });
    } else {
        $('#report').DataTable({
            "data": array,
            dom: "Bfrtip",
            stateSave: true,
            buttons: [
                {
                    extend: 'colvis',
                    text: 'Colonne da non visualizzare',
                    collectionLayout: 'fixed two-column'
                }
            ],
            "language": {
                "lengthMenu": "Mostra _MENU_ righe per pagina",
                "zeroRecords": "Nessun elemento trovato",
                "info": "Mostrando pagina _PAGE_ di _PAGES_ su _TOTAL_ righe",
                "infoEmpty": "Nessuna riga disponibile",
                "search": "Cerca",
                "paginate": {
                    "previous": "Precedente",
                    "next": "Successivo"
                }
            },
            "lengthMenu": [[50, 150, 300], [50, 150, 300]],
            autoWidth: false,
            scrollY: 300,
            scrollX: 300,
            "columns": [
                {"title": "Data", "class": "data", "data": "timecreated"},
                {"title": "Userid", "class": "userid", "data": "userid"},
                {"title": "Username", "class": "username", "data": "username"},
                {"title": "Nome Cognome", "class": "name", "data": "name"},
                {"title": "Codice Fiscale", "class": "cf", "data": "cf"},
                {"title": "Email", "class": "email", "data": "email"},
                {"title": "Data di Nascita", "class": "dn", "data": "data_nascita"},
                {"title": "Luogo di Nascita", "class": "ln", "data": "citta"},
                {"title": "Azienda", "class": "azienda", "data": "azienda"},
                {"title": "N.di telefono", "class": "phone", "data": "phone"},
                {"title": "Ruolo", "class": "ruolo", "data": "ruolo"},
                {"title": "Corso", "class": "corso", "data": "corso"},
                {"title": "Nome aula", "class": "aula", "data": "aula"},
                {"title": "Ora ingresso", "class": "orarioA", "data": "orario"},
                {"title": "Ora uscita", "class": "orarioL", "data": "orario_logout"},
                {"title": "Tempo cumulato", "class": "tc", "data": "tempo_cumulato"}
            ],
            "order": [[3, "asc"]]
        });
    }
}



function sintesiHvp(array, mesi) {


    cleanDataTable();
    var thead = document.getElementById("report_head");
    thead.innerHTML = "";
    var trh = document.createElement('tr');
    trh.innerHTML =
            '<th class="userid"> Userid </th>' +
            '<th class="username"> Username </th>' +
            '<th class="name"> Nome Cognome </th>' +
            '<th class="cf"> Codice Fiscale </th>' +
            '<th class="email"> Email </th>' +
            '<th class="dn"> Data di Nascita </th>' +
            '<th class="ln"> Luogo di Nascita </th>' +
            '<th class="azienda"> Azienda </th>' +
            '<th class="phone">N.di telefono </th>' +
            '<th class="tc"> Tempo cumulato </th>';
    for (var i = 0; i < mesi.length; i++) {
        trh.innerHTML += '<th class="data_' + i + '">' + mesi[i] + '</th>';
    }
    thead.appendChild(trh);
    var tbody = document.getElementById("report_body");
    var nav = document.getElementById("nav");
    nav.innerHTML = "";
    tbody.innerHTML = "";
    for (var i = 0; i < array.length; i++) {
        var tr = document.createElement('tr');
        tr.innerHTML = '<td class="userid">' + array[i]['userid'] + '</td>' +
                '<td class="username">' + array[i]['username'] + '</td>' +
                '<td class="name">' + array[i]['name'] + '</td>' +
                '<td class="cf">' + array[i]['cf'] + '</td>' +
                '<td class="email">' + array[i]['email'] + '</td>' +
                '<td class="dn">' + array[i]['data_nascita'] + '</td>' +
                '<td class="ln">' + array[i]['citta'] + '</td>' +
                '<td class="azienda">' + array[i]['azienda'] + '</td>' +
                '<td class="phone">' + array[i]['phone'] + '</td>' +
                '<td class="tc">' + array[i]['tempo_tot'] + '</td>';
        var j = 0;
        for (const [key, value] of Object.entries(array[i]['mesi'])) {
            tr.innerHTML += '<td class="data_' + j + '">' + value + '</td>';
            j++;
        }
        tbody.appendChild(tr);
    }
    createDataTable();
}


function sintesiHvpName(array, mesi) {

    cleanDataTable();
    var thead = document.getElementById("report_head");
    thead.innerHTML = "";

    var trh = document.createElement('tr');
    trh.innerHTML =
            '<th class="userid"> Userid </th>' +
            '<th class="username"> Username </th>' +
            '<th class="name"> Nome Cognome </th>' +
            '<th class="cf"> Codice Fiscale </th>' +
            '<th class="email"> Email </th>' +
            '<th class="dn"> Data di Nascita </th>' +
            '<th class="ln"> Luogo di Nascita </th>' +
            '<th class="azienda"> Azienda </th>' +
            '<th class="phone">N.di telefono </th>' +
            '<th class="corso"> Corso </th>' +
            '<th class="courseid"> Id Corso </th>' +
            '<th class="tc"> Tempo cumulato </th>';
    for (var i = 0; i < mesi.length; i++) {
        trh.innerHTML += '<th class="data_' + i + '">' + mesi[i] + '</th>';
    }
    thead.appendChild(trh);
    var tbody = document.getElementById("report_body");
    var nav = document.getElementById("nav");
    nav.innerHTML = "";
    tbody.innerHTML = "";
    for (var i = 0; i < array.length; i++) {
        console.log(array);
        var tr = document.createElement('tr');
        tr.innerHTML = '<td class="userid">' + array[i]['userid'] + '</td>' +
                '<td class="username">' + array[i]['username'] + '</td>' +
                '<td class="name">' + array[i]['name'] + '</td>' +
                '<td class="cf">' + array[i]['cf'] + '</td>' +
                '<td class="email">' + array[i]['email'] + '</td>' +
                '<td class="dn">' + array[i]['data_nascita'] + '</td>' +
                '<td class="ln">' + array[i]['citta'] + '</td>' +
                '<td class="azienda">' + array[i]['azienda'] + '</td>' +
                '<td class="phone">' + array[i]['phone'] + '</td>' +
                '<td class="corso">' + array[i]['corso'] + '</td>' +
                '<td class="courseid">' + array[i]['courseid'] + '</td>' +
                '<td class="tc">' + array[i]['tempo_tot'] + '</td>';
        var j = 0;
        for (const [key, value] of Object.entries(array[i]['mesi'])) {
            tr.innerHTML += '<td class="data_' + j + '">' + value + '</td>';
            j++;
        }
        tbody.appendChild(tr);
    }
    createDataTable();

}


function hvpReport(array) {

    cleanDataTable();
    $('#report').DataTable({
        "data": array,
        dom: "Bfrtip",
        stateSave: true,
        buttons: [
            {
                extend: 'colvis',
                text: 'Colonne da non visualizzare',
                collectionLayout: 'fixed two-column'
            }
        ],
        "language": {
            "lengthMenu": "Mostra _MENU_ righe per pagina",
            "zeroRecords": "Nessun elemento trovato",
            "info": "Mostrando pagina _PAGE_ di _PAGES_ su _TOTAL_ righe",
            "infoEmpty": "Nessuna riga disponibile",
            "search": "Cerca",
            "paginate": {
                "previous": "Precedente",
                "next": "Successivo"
            }
        },
        "lengthMenu": [[50, 150, 300], [50, 150, 300]],
        autoWidth: false,
        scrollY: 300,
        scrollX: 300,
        "columns": [
            {"title": "Data", "class": "data", "data": "timecreated"},
            {"title": "Userid", "class": "userid", "data": "userid"},
            {"title": "Username", "class": "username", "data": "username"},
            {"title": "Nome Cognome", "class": "name", "data": "name"},
            {"title": "Codice Fiscale", "class": "cf", "data": "cf"},
            {"title": "Email", "class": "email", "data": "email"},
            {"title": "Data di Nascita", "class": "dn", "data": "data_nascita"},
            {"title": "Luogo di Nascita", "class": "ln", "data": "citta"},
            {"title": "N.di telefono", "class": "phone", "data": "phone"},
            {"title": "Corso", "class": "corso", "data": "corso"},
            {"title": "Lezione", "class": "lezione", "data": "lezione"},
            {"title": "Ora accesso al video", "class": "orarioA", "data": "orario"},
            {"title": "Ora uscita dal video", "class": "orarioL", "data": "orario_logout"},
            {"title": "Tempo cumulato", "class": "tc", "data": "tempo_cumulato"},
            {"title": "Tempo sessione video", "class": "ts", "data": "diff"}
        ],
        "order": [[3, "asc"]]
    });
}

function logstdReport(array) {

    cleanDataTable();
    $('#report').DataTable({
        "data": array,
        dom: "Bfrtip",
        stateSave: true,
        buttons: [
            {
                extend: 'colvis',
                text: 'Colonne da non visualizzare',
                collectionLayout: 'fixed two-column'
            }
        ],
        "language": {
            "lengthMenu": "Mostra _MENU_ righe per pagina",
            "zeroRecords": "Nessun elemento trovato",
            "info": "Mostrando pagina _PAGE_ di _PAGES_ su _TOTAL_ righe",
            "infoEmpty": "Nessuna riga disponibile",
            "search": "Cerca",
            "paginate": {
                "previous": "Precedente",
                "next": "Successivo"
            }
        },
        "lengthMenu": [[50, 150, 300], [50, 150, 300]],
        autoWidth: false,
        scrollY: 300,
        scrollX: 300,
        "columns": [
            {"title": "Data/Ora", "class": "data", "data": "timecreated"},
            {"title": "Userid", "class": "userid", "data": "userid"},
            {"title": "Username", "class": "username", "data": "username"},
            {"title": "Nome Cognome", "class": "name", "data": "name"},
            {"title": "Codice Fiscale", "class": "cf", "data": "cf"},
            {"title": "Email", "class": "email", "data": "email"},
            {"title": "Data di Nascita", "class": "dn", "data": "data_nascita"},
            {"title": "Luogo di Nascita", "class": "ln", "data": "citta"},
            {"title": "N.di telefono", "class": "phone", "data": "phone"},
            {"title": "Attività tracciata", "class": "attivita", "data": "attivita"},
            {"title": "Azione", "class": "azione", "data": "action"},
            {"title": "Ip", "class": "ip", "data": "ip"}
        ],
        "order": [[3, "asc"]]

    });
}


function s_zoomReport(array, mesi) {

    cleanDataTable();
    var thead = document.getElementById("report_head");
    thead.innerHTML = "";
    var trh = document.createElement('tr');
    trh.innerHTML =
            '<th class="userid"> Userid </th>' +
            '<th class="username"> Username </th>' +
            '<th class="name"> Nome Cognome </th>' +
            '<th class="cf"> Codice Fiscale </th>' +
            '<th class="email"> Email </th>' +
            '<th class="dn"> Data di Nascita </th>' +
            '<th class="ln"> Luogo di Nascita </th>' +
            '<th class="azienda"> Azienda </th>' +
            '<th class="phone">N.di telefono </th>' +
            '<th class="ruolo">Ruolo </th>' +
            '<th class="corso">Corso </th>' +
            '<th class="idCorso">Id Corso </th>' +
            '<th class="tc"> Tempo cumulato </th>';
    for (var i = 0; i < mesi.length; i++) {
        trh.innerHTML += '<th class="data_' + i + '">' + mesi[i] + '</th>';
    }
    thead.appendChild(trh);
    var tbody = document.getElementById("report_body");
    var nav = document.getElementById("nav");
    nav.innerHTML = "";
    tbody.innerHTML = "";
    for (var i = 0; i < array.length; i++) {
        var tr = document.createElement('tr');
        tr.innerHTML = '<td class="userid">' + array[i]['userid'] + '</td>' +
                '<td class="username">' + array[i]['username'] + '</td>' +
                '<td class="name">' + array[i]['nome'] + '</td>' +
                '<td class="cf">' + array[i]['cf'] + '</td>' +
                '<td class="email">' + array[i]['email'] + '</td>' +
                '<td class="dn">' + array[i]['data_nascita'] + '</td>' +
                '<td class="ln">' + array[i]['citta'] + '</td>' +
                '<td class="azienda">' + array[i]['azienda'] + '</td>' +
                '<td class="phone">' + array[i]['phone'] + '</td>' +
                '<td class="ruolo">' + array[i]['ruolo'] + '</td>' +
                '<td class="corso">' + array[i]['nomeCorso'] + '</td>' +
                '<td class="idCorso">' + array[i]['idCorso'] + '</td>' +
                '<td class="tc">' + array[i]['tempo_tot'] + '</td>';
        var j = 0;
        for (const [key, value] of Object.entries(array[i]['mesi'])) {
            tr.innerHTML += '<td class="data_' + j + '">' + value + '</td>';
            j++;
        }
        tbody.appendChild(tr);
    }
    createDataTable();
}

function zoomReport(array) {

    cleanDataTable();
    $('#report').DataTable({
        "data": array,
        dom: "Bfrtip",
        stateSave: true,
        buttons: [
            {
                extend: 'colvis',
                text: 'Colonne da non visualizzare',
                collectionLayout: 'fixed two-column'
            }
        ],
        "language": {
            "lengthMenu": "Mostra _MENU_ righe per pagina",
            "zeroRecords": "Nessun elemento trovato",
            "info": "Mostrando pagina _PAGE_ di _PAGES_ su _TOTAL_ righe",
            "infoEmpty": "Nessuna riga disponibile",
            "search": "Cerca",
            "paginate": {
                "previous": "Precedente",
                "next": "Successivo"
            }
        },
        "lengthMenu": [[50, 150, 300], [50, 150, 300]],
        autoWidth: false,
        scrollY: 300,
        scrollX: 300,
        "bDeferRender": true,
        "columns": [
            {"title": "Data", "class": "data", "data": "timecreated"},
            {"title": "Userid", "class": "userid", "data": "userid"},
            {"title": "Username", "class": "username", "data": "username"},
            {"title": "Nome Cognome", "class": "name", "data": "nome"},
            {"title": "Codice Fiscale", "class": "cf", "data": "cf"},
            {"title": "Email", "class": "email", "data": "email"},
            {"title": "Data di Nascita", "class": "dn", "data": "data_nascita"},
            {"title": "Luogo di Nascita", "class": "ln", "data": "citta"},
            {"title": "N.di telefono", "class": "phone", "data": "phone"},
            {"title": "Ruolo", "class": "ruolo", "data": "ruolo"},
            {"title": "Corso", "class": "corso", "data": "nomeCorso"},
            {"title": "Nome aula", "class": "aula", "data": "name"},
            {"title": "Ora ingresso", "class": "orarioA", "data": "orario"},
            {"title": "Ora uscita", "class": "orarioL", "data": "orario_logout"},
            {"title": "Tempo cumulato", "class": "tc", "data": "tempo_cumulato"}
        ]
    });
}










function cleanDataTable() {
    $('#report').DataTable().clear();
    $('#report').DataTable().destroy();
    $('#report tbody').empty();
    $('#report thead').empty();
}

function createDataTable() {
    $('#report').DataTable({
        dom: "Bfrtip",
        stateSave: true,
        buttons: [
            {
                extend: 'colvis',
                text: 'Colonne da non visualizzare',
                collectionLayout: 'fixed two-column'
                        //columns:':lt(9)'
            }
        ],
        "language": {
            "lengthMenu": "Mostra _MENU_ righe per pagina",
            "zeroRecords": "Nessun elemento trovato",
            "info": "Mostrando pagina _PAGE_ di _PAGES_ su _TOTAL_ righe",
            "infoEmpty": "Nessuna riga disponibile",
            "search": "Cerca",
            "paginate": {
                "previous": "Precedente",
                "next": "Successivo"
            }
        },
        "lengthMenu": [[50, 150, 300], [50, 150, 300]],
        autoWidth: false,
        scrollY: 300,
        scrollX: 300,
        "order": [[3, "asc"]]
    });
}

function getAziendeAssociate(idCorso, piattaforma, report) {
    showPleaseWait("Caricamento...");
    $("#id_enterprise" + report).empty().trigger('change');


    $('#id_enterprise' + report).append($('<option>', {
        value: 0,
        text: 'Tutti'
    }));


    $.ajax({
        method: 'POST',
        url: "table.php",
        data: {
            idCorso: idCorso,
            platf: piattaforma
        }
    })

            .done(function (resultParsed) {
                hidePleaseWait();
                var array = JSON.parse(resultParsed);
                var tmp = [];

                for (const [key, value] of Object.entries(array)) {

                    var input = {"id": key, "text": value};
                    tmp.push(input);

                }

                $("#id_enterprise" + report).select2({
                    data: tmp
                });
                if (report != "") {
                    let azienda = $("#id_enterprise" + report + " option:selected").text();
                    getUtentiAssociati(azienda, piattaforma, report, idCorso);
                }


            })
            .fail(function (jqXHR, exception) {
                var msg = '';
                if (jqXHR.status === 0) {
                    msg = 'Not connect.\n Verify Network.';
                } else if (jqXHR.status == 404) {
                    msg = 'Requested page not found. [404]';
                } else if (jqXHR.status == 500) {
                    msg = 'Internal Server Error [500].';
                } else if (exception === 'parsererror') {
                    msg = 'Requested JSON parse failed.';
                } else if (exception === 'timeout') {
                    msg = 'Time out error.';
                } else if (exception === 'abort') {
                    msg = 'Ajax request aborted.';
                } else {
                    msg = 'Uncaught Error.\n' + jqXHR.responseText;
                }
                console.log(msg);
            });
}

function getUtentiAssociati(azienda, piattaforma, report, idCorso) {

    showPleaseWait("Caricamento...");
    if (azienda == "")
        return;
    $("#id_iscritto" + report).empty().trigger('change');
    $('#id_iscritto' + report).append($('<option>', {
        value: 0,
        text: 'Tutti'
    }));

    $.ajax({
        method: 'POST',
        url: "table.php",
        data: {
            azienda: azienda,
            platf: piattaforma,
            idCorso: idCorso
        }
    })

            .done(function (resultParsed) {

                var array = JSON.parse(resultParsed);
                if (array.length != undefined && array.length == 0) {
                    hidePleaseWait();
                    alert("Non ci sono utenti associati a questo corso!");

                    return;
                }

                var tmp = [];
                for (const [key, value] of Object.entries(array)) {
                    var input = {"id": key, "text": value};
                    tmp.push(input);
                }

                $("#id_iscritto" + report).select2({
                    data: tmp
                });

                hidePleaseWait();


            })
            .fail(function (jqXHR, exception) {
                var msg = '';
                if (jqXHR.status === 0) {
                    msg = 'Not connect.\n Verify Network.';
                } else if (jqXHR.status == 404) {
                    msg = 'Requested page not found. [404]';
                } else if (jqXHR.status == 500) {
                    msg = 'Internal Server Error [500].';
                } else if (exception === 'parsererror') {
                    msg = 'Requested JSON parse failed.';
                } else if (exception === 'timeout') {
                    msg = 'Time out error.';
                } else if (exception === 'abort') {
                    msg = 'Ajax request aborted.';
                } else {
                    msg = 'Uncaught Error.\n' + jqXHR.responseText;
                }
                console.log(msg);
            });
}

function getData(tag, report) {
    showPleaseWait("Generazione Reportistica");
    let method = "GET";
    let enterprise = $("#id_enterprise" + tag + " option:selected").text();
    let course = 0;
    let iscritto = 0;
    if (tag !== "_sintesi" && tag !== "_timein") {
        course = idCorso;
    } else {
        if (enterprise == "Tutti") {
            iscritto = [];
            $("#id_iscritto" + tag + " option").each(function () {
                if ($(this).val() != 0)
                    iscritto.push($(this).val());
            });

        } else {
            iscritto = $("#id_iscritto" + tag).val();
        }
    }
    let start = $("#id_start" + tag).val();
    let end = $("#id_end" + tag).val();
    $.ajax({
        method: method,
        url: "stub_table.php",
        data: {
            enterprise: enterprise,
            course: course,
            start: start,
            end: end,
            report: report,
            uid: 0,
            platf: piattaforma,
            iscritto: iscritto
        }

    })
            .done(function (resultParsed) {
                var arraySplit = resultParsed.split('*?');
                var array = JSON.parse(arraySplit[0]);
                hidePleaseWait();
                $("#id_excel" + tag).prop("disabled", false);
                if (report == 'zoom' || report == 'zoomName') {
                    zoomReport(array);
                } else if (report == "total" || report == 'totalName') {
                    logstdReport(array);
                } else if (report == "hvp" || report == "hvpName") {
                    hvpReport(array);
                } else if (report == "bbb" || report == 'bbbName') {
                    bbbReport(array);
                } else if (report == "sintesi" || report == 'sintesiName') {
                    sintesiReport(array);
                } else if (report == "timein") {
                    var mesi = JSON.parse(arraySplit[2]);
                    timeinReport(array, mesi);
                } else if (report == "shvp") {
                    var mesi = JSON.parse(arraySplit[2]);
                    sintesiHvp(array, mesi);
                } else if (report == "shvpName") {
                    var mesi = JSON.parse(arraySplit[2]);
                    sintesiHvpName(array, mesi);
                } else if (report == "sbbb") {
                    var mesi = JSON.parse(arraySplit[2]);
                    sintesiBbb(array, mesi);
                } else if (report == "sbbbName") {
                    var mesi = JSON.parse(arraySplit[2]);
                    sintesiBbbName(array, mesi);
                } else if (report == "rhvp") {
                    rhvpReport(array);
                } else if (report == "s_zoom" || report == "s_zoomName") {
                    var mesi = JSON.parse(arraySplit[2]);
                    s_zoomReport(array, mesi);
                }


            })
            .fail(function (jqXHR, exception) {
                var msg = '';
                if (jqXHR.status === 0) {
                    msg = 'Not connect.\n Verify Network.';
                } else if (jqXHR.status == 404) {
                    msg = 'Requested page not found. [404]';
                } else if (jqXHR.status == 500) {
                    msg = 'Internal Server Error [500].';
                } else if (exception === 'parsererror') {
                    msg = 'Requested JSON parse failed.';
                } else if (exception === 'timeout') {
                    msg = 'Time out error.';
                } else if (exception === 'abort') {
                    msg = 'Ajax request aborted.';
                } else {
                    msg = 'Uncaught Error.\n' + jqXHR.responseText;
                }
                console.log(msg);
            });
}

function getExcel(tag, report) {
    showPleaseWait("Generazione Excel");
    let enterprise = $("#id_enterprise" + tag + " option:selected").text();
    let course = 0;
    let iscritto = 0;
    if (tag !== "_sintesi" && tag !== "_timein") {
        course = idCorso;
    } else {
        if (enterprise == "Tutti") {
            iscritto = [];
            $("#id_iscritto" + tag + " option").each(function () {
                if ($(this).val() != 0)
                    iscritto.push($(this).val());
            });

        } else {
            iscritto = $("#id_iscritto" + tag).val();
        }
    }
    let start = $("#id_start" + tag).val();
    let end = $("#id_end" + tag).val();
    $.ajax({
        type: 'GET',
        url: "stub_table.php",
        data: {
            start: start,
            end: end,
            report: report,
            excel: "excel",
            enterprise: enterprise,
            course: course,
            filtri: checked,
            uid: 0,
            platf: piattaforma,
            iscritto: iscritto
        },
        dataType: 'json'
    }).done(function (data) {
        hidePleaseWait();
        if (data != "Nessun log") {
            $("#noLog").remove();
            var $a = $("<a>");
            $a.attr("href", data.file);
            $("body").append($a);
            $a.attr("download", "report_" + report + "_" + enterprise + "_" + start + "_" + end + ".xlsx");
            $a[0].click();
            $a.remove();
        } else {
            $("#noLog").html("");
            $("#report_head").html("");
            $("#report_body").html("");
            $("#nav").html("");
            $("#nav").after('<p id="noLog" style="display: flex;justify-content:center;">Nessun log trovato!</p>');
        }
    });
}


