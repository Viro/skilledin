var table = {};
var noConflict = false;
var jsonReportData = {};
var coursesPerEnterprise = {};
var zoomMeetings = {};
var calendar = {};
var loadingSpinner = '<div class="spinner-border text-primary" role="status"><span class="visually-hidden">Caricamento...</span></div>';
// const waitingDialog = require('bootstrap-waitingfor');

$(document).ready(function () {
    $('#report').DataTable(
            {
                info: false,
                searching: false,
                bLengthChange: false,
                bPaginate: false,
                "language": {
                    "emptyTable": "Tabella vuota"
                }

            });
});
function setEndToday() {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0');
    var yyyy = today.getFullYear();
    today = yyyy + '-' + mm + '-' + dd;
    let start = "2021-01-01";
    $('#id_start_video').val(start);
    $('#id_start_video40').val("2021-05-05");
    $('#id_end_total').val(today);
    $('#id_end_video').val(today);
    $('#id_end_video40').val(today);
    $('#id_end_bbb').val(today);
    $('#id_end_bbb_course').val(today);
    $('#id_end_zoom').val(today);
}

function isEmpty(obj) {
    return Object.keys(obj).length === 0;
}

/**
 * Displays overlay with "Please wait" text. Based on bootstrap modal. Contains animated progress bar.
 */
function showPleaseWait(message) {
    if (document.querySelector("#pleaseWaitDialog") == null) {
        var modalLoading = '<div class="modal" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false" role="dialog">\
            <div class="modal-dialog">\
                <div class="modal-content">\
                    <div class="modal-header">\
                        <h4 id="modalMessaggioWait" class="modal-title">' + message + '</h4>\
                    </div>\
                    <div class="modal-body">\
                        <div class="progress">\
                          <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar"\
                          aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%; height: 40px">\
                          </div>\
                        </div>\
                    </div>\
                </div>\
            </div>\
        </div>';
        $(document.body).append(modalLoading);
    } else {
        document.getElementById('modalMessaggioWait').innerHTML = message;
    }

    $('#pleaseWaitDialog').modal({backdrop: 'static', keyboard: false});
    $("#pleaseWaitDialog").modal("show");
}

/**
 * Hides "Please wait" overlay. See function showPleaseWait().
 */
function hidePleaseWait() {
    $("#pleaseWaitDialog").modal("hide");
}

function getMeetings() {
    var items = [];
    showPleaseWait("Caricamento dei meetings da Zoom...");
    var start = $("#id_start_zoom").val();
    var end = $("#id_end_zoom").val();
    $.getJSON("table.php?start=" + start + "&end=" + end + "&zoom=meetings&type=past", function (data) {

        var items = [];
        zoomMeetings = data;
        items.push("<datalist id=\"datalistMeetings\">");
        $.each(zoomMeetings, function (key, val) {
            items.push("<option data-value=" + encodeURI(key) + ">" + decodeURI(key) + "</option>");
        });
        items.push("</datalist>");
        document.getElementById("id_meetings_list").innerHTML = items;
        hidePleaseWait();
    });
}

var currPage = 1;
function getData(report) {
    var method = "GET";
    if (report == 'total') {
        var enterprise = $("#id_enterprise_total").val();
        var start = $("#id_start_total").val();
        var end = $("#id_end_total").val();
    }
    else if (report == 'hvp') {
        var enterprise = $("#id_enterprise_hvp").val();
        var start = $("#id_start_hvp").val();
        var end = $("#id_end_hvp").val();
    }
    else if (report == 'summaryhvp') {
        var enterprise = $("#id_enterprise_summaryhvp").val();
        var start = $("#id_start_summaryhvp").val();
        var end = $("#id_end_summaryhvp").val();
    }
    else if (report == 'bbb') {
        var enterprise = $("#id_enterprise_bbb").val();
        var start = $("#id_start_bbb").val();
        var end = $("#id_end_bbb").val();
    }
    else if (report == 'sbbb') {
        var enterprise = $("#id_enterprise_sbbb").val();
        var start = $("#id_start_sbbb").val();
        var end = $("#id_end_sbbb").val();
    }
    else if (report == 'sbbbName') {
        var enterprise = $("#id_enterprise_sbbbN option:selected").text();
        var start = $("#id_start_sbbb_iscritto").val();
        var end = $("#id_end_sbbb_iscritto").val();
    }
    else if (report == 'zoom') {
        var enterprise = $("#id_enterprise_zoom").val();
        var start = $("#id_start_zoom").val();
        var end = $("#id_end_zoom").val();
        /*if ($("#flexCheckAllZoom").is(':checked')) {
         course = "all";
         } else {
         var course = $('#datalistZoomCourses option').filter(function () {
         return this.value == $("#id_course_zoom").val();
         }).data("value")
         }*/

    }
    else if (report == "zoomName") {
        var enterprise = $("#id_enterprise_zoomName option:selected").text();
        var start = $("#id_start_zoom_course").val();
        var end = $("#id_end_zoom_course").val();
    }
    else if (report == "s_zoom") {
        var enterprise = $("#id_enterprise_s_zoom").val();
        var start = $("#id_start_s_zoom").val();
        var end = $("#id_end_s_zoom").val();
    }
    else if (report == "s_zoomName") {
        var enterprise = $("#id_enterprise_s_zoomName option:selected").text();
        var start = $("#id_start_s_zoom_iscritto").val();
        var end = $("#id_end_s_zoom_iscritto").val();
    }
    else if (report == 'zoom_live') {
        method = "POST";
        var course = "";
        var meeting = {};
        if ($("#flexCheckAllZoom").is(':checked')) {
            course = "all";
            meeting = zoomMeetings;
        } else {
            course = $("#id_meetings_list").val();
            meeting[course] = zoomMeetings[course];
        }
    }
    else if (report == "sintesi") {
        var enterprise = $("#id_enterprise_sintesi").val();
        var start = $("#id_start_sintesi").val();
        var end = $("#id_end_sintesi").val();
    }
    else if (report == "timein") {
        var enterprise = $("#id_enterprise_timein").val();
        var start = $("#id_start_timein").val();
        var end = $("#id_end_timein").val();
    }
    else if (report == "totalName") {
        var enterprise = $("#id_iscritto_total").val();
        var start = $("#id_start_total_iscritto").val();
        var end = $("#id_end_total_iscritto").val();
    }
    else if (report == "sintesiName") {
        var enterprise = $("#id_iscritto_sintesi").val();
        var start = $("#id_start_sintesi_iscritto").val();
        var end = $("#id_end_sintesi_iscritto").val();
    }
    else if (report == "bbbName") {
        var enterprise = $("#id_enterprise_bbbName option:selected").text();
        var start = $("#id_start_bbb_course").val();
        var end = $("#id_end_bbb_course").val();
    }
    else if (report == "hvpName") {
        var enterprise = $("#id_enterprise_hvpName option:selected").text();
        var start = $("#id_start_hvp_course").val();
        var end = $("#id_end_hvp_course").val();
    }
    else if (report == "summaryhvp_course") {
        var enterprise = $("#id_enterprise_summaryhvp option:selected").text();
        var start = $("#id_start_summaryhvp_course").val();
        var end = $("#id_end_summaryhvp_course").val();
    }
    else if (report == "shvp") {
        var enterprise = $("#id_enterprise_shvp").val();
        var start = $("#id_start_shvp").val();
        var end = $("#id_end_shvp").val();
    }
    else if (report == "rhvp") {
        var enterprise = $("#id_enterprise_rhvp").val();
        var start = $("#id_start_rhvp").val();
        var end = $("#id_end_rhvp").val();
    }
    else if (report == "shvpName") {
        var enterprise = $("#id_enterprise_shvpN option:selected").text();
        var start = $("#id_start_shvp_corso").val();
        var end = $("#id_end_shvp_corso").val();
    }
    else if (report == "rhvpName") {
        var enterprise = $("#id_enterprise_rhvpN option:selected").text();
        var start = $("#id_start_rhvp_corso").val();
        var end = $("#id_end_rhvp_corso").val();
    }
    else if (report == "timeinName") {
        var enterprise = $("#id_enterprise_timeinN option:selected").text();
        var start = $("#id_start_timein_corso").val();
        var end = $("#id_end_timein_corso").val();
    }
    else if (report == "videotimepro") {
        var enterprise = $("#id_enterprise_videotimepro option:selected").text();
        var start = $("#id_start_videotimepro").val();
        var end = $("#id_end_videotimepro").val();
    }
    else if (report == 'hvp_new') {
        var enterprise = $("#id_enterprise_hvp_new").val();
        var start = $("#id_start_hvp_new").val();
        var end = $("#id_end_hvp_new").val();
    }
    else if (report == "shvp_new") {
        var enterprise = $("#id_enterprise_shvp_new").val();
        var start = $("#id_start_shvp_new").val();
        var end = $("#id_end_shvp_new").val();
    }
    else if (report == "shvp_course_new") {
        var enterprise = $("#id_enterprise_shvpName_new option:selected").text();
        var start = $("#id_start_shvp_course_new").val();
        var end = $("#id_end_shvp_course_new").val();
    }
    else if (report == 'summaryhvp_new') {
        var enterprise = $("#id_enterprise_summaryhvp_new").val();
        var start = $("#id_start_summaryhvp_new").val();
        var end = $("#id_end_summaryhvp_new").val();
    }


    var corsoSelezionato;
    if (report == "hvp") {
        corsoSelezionato = document.getElementById("id_course_hvp").value;
    }
    else if (report == "summaryhvp") {
        corsoSelezionato = $("#id_course_summaryhvp option:selected").text();
    }
    else if (report == "zoom") {
        corsoSelezionato = $("#id_course_zoom option:selected").text();
    }
    else if (report == "zoomName") {
        corsoSelezionato = $("#id_courses_zoom option:selected").text();
    }
    else if (report == "sbbbName") {
        corsoSelezionato = $("#id_iscritto_sbbb").val();
    }
    else if (report == "shvpName") {
        corsoSelezionato = $("#id_corso_shvp").val();
    }
    else if (report == "rhvpName") {
        corsoSelezionato = $("#id_corso_rhvp").val();
    }
    else if (report == "bbbName") {
        corsoSelezionato = $("#id_courses_bbb").val();
    }
    else if (report == "hvpName") {
        corsoSelezionato = $("#id_courses_hvp").val();
    }
    else if (report == "s_zoomName") {
        corsoSelezionato = $("#id_iscritto_s_zoom option:selected").text();
    }
    else if (report == "timeinName") {
        corsoSelezionato = $("#id_corso_timeinN").val();
    }
    else if (report == "videotimepro") {
        corsoSelezionato = $("#id_corso_videotimepro").val();
    }
    else if (report == "hvp_new") {
        corsoSelezionato = $("#id_course_hvp_new option:selected").text();
    }
    else if (report == "shvp_new") {
        corsoSelezionato = $("#id_course_shvp_new option:selected").text();
    }
    else if (report == "shvp_course_new") {
        corsoSelezionato = $("#id_courses_shvpName_new").val();
    }
    else if (report == "summaryhvp_new") {
        corsoSelezionato = $("#id_course_summaryhvp_new option:selected").text();
    }
    else {
        corsoSelezionato = document.getElementById("id_course_bbb").value;
    }
//    console.log(method);
    currPage = 1;
    $("#noLog").html("");
    enterprise = enterprise.trim();
    // $("#example_processing").css("display", "block");
    // $(".dataTables_processing").css("z-index", "1");
    // $(".fa").css("display", "inline-block !important");
    // $(".fa-spinner.fa-spin").show();
    // $(".fa-spinner.fa-spin").css("display", "inline-block");
    var jqxhr = $.ajax({
        // url: "./report/table.php",
        method: method,
        url: "stub_table.php",
        data: {
            course: corsoSelezionato,
            enterprise: enterprise,
            meeting: meeting,
            start: start,
            end: end,
            report: report

        }

    })
            .done(function (resultParsed) {
                // console.log(resultParsed);
                var arraySplit = resultParsed.split('*?');
                var array = JSON.parse(arraySplit[0]);
                // console.log(arraySplit[1]);
                // console.log(array);
                // console.log(arraySplit);
                if (typeof arraySplit[2] == 'undefined'){
                    arraySplit[2] = false;
                }

                hidePleaseWait();
                // console.log("Report: " + report);
                if (report == 'zoom' || report == 'zoomName') {
                    zoomReport(array);
                }
                else if (report == "total" || report == 'totalName') {
                    logstdReport(array);
                }
                else if (report == "hvp" || report == "hvpName") {
                    hvpReport(array);
                }
                else if (report == "summaryhvp" || report == "summaryhvpName") {
                    summaryHvpReport(array);
                }
                else if (report == "bbb" || report == 'bbbName') {
                    bbbReport(array);
                }
                else if (report == "sintesi" || report == 'sintesiName') {
                    sintesiReport(array);
                }
                else if (report == "timein" || report == "timeinName") {
                    var mesi = JSON.parse(arraySplit[2]);
                    timeinReport(array, mesi);
                }
                else if (report == "shvp") {
                    var mesi = JSON.parse(arraySplit[2]);
                    sintesiHvp(array, mesi);
                }

                else if (report == "shvpName") {
                    var mesi = JSON.parse(arraySplit[2]);
                    sintesiHvpName(array, mesi);
                }
                else if (report == "sbbb") {
                    var mesi = JSON.parse(arraySplit[2]);
                    sintesiBbb(array, mesi);
                }
                else if (report == "sbbbName") {
                    var mesi = JSON.parse(arraySplit[2]);
                    sintesiBbbName(array, mesi);
                }
                else if (report == "rhvp") {
                    rhvpReport(array);
                }
                else if (report == "rhvpName") {
                    rhvpNameReport(array);
                }
                else if (report == "s_zoom" || report == "s_zoomName") {
                    var mesi = JSON.parse(arraySplit[2]);
                    s_zoomReport(array, mesi);
                }
                else if (report == "videotimepro" || report == "videotimeproName") {
                    var period_dates = JSON.parse(arraySplit[2]);
                    //console.log(array);
                    videotimeproReport(array, period_dates);
                }
                else if(report == "hvp_new"){
                    console.log(array);
                    hvpReportNew(array);
                }
                else if (report == "shvp_new" || report == "shvp_course_new") {
                    var mesi = JSON.parse(arraySplit[2]);
                    console.log(array);
                    console.log(mesi);
                    sintesiHvpNew(array, mesi);
                }
                else if (report == "summaryhvp_new" || report == "summaryhvp_curse_new") {
                    summaryHvpReport(array);
                }
            })
            .fail(function (jqXHR, exception) {
                var msg = '';
                if (jqXHR.status === 0) {
                    msg = 'Not connect.\n Verify Network.';
                } else if (jqXHR.status == 404) {
                    msg = 'Requested page not found. [404]';
                } else if (jqXHR.status == 500) {
                    msg = 'Internal Server Error [500].';
                } else if (exception === 'parsererror') {
                    msg = 'Requested JSON parse failed.';
                } else if (exception === 'timeout') {
                    msg = 'Time out error.';
                } else if (exception === 'abort') {
                    msg = 'Ajax request aborted.';
                } else {
                    msg = 'Uncaught Error.\n' + jqXHR.responseText;
                }
                console.log(msg);
            });
}


function timeinReport(array, mesi) {


    cleanDataTable();

    var thead = document.getElementById("report_head");
    thead.innerHTML = "";

    var trh = document.createElement('tr');
    trh.innerHTML =
            '<th class="userid"> Userid </th>' +
            '<th class="username"> Username </th>' +
            '<th class="name"> Nome Cognome </th>' +
            '<th class="cf"> Codice Fiscale </th>' +
            '<th class="email"> Email </th>' +
            '<th class="dn"> Data di Nascita </th>' +
            '<th class="ln"> Luogo di Nascita </th>' +
            '<th class="phone">N.di telefono </th>' +
            '<th class="azienda"> Azienda </th>' +
            '<th class="tc"> Tempo cumulato </th>';
    for (var i = 0; i < mesi.length; i++) {
        trh.innerHTML += '<th class="data_' + i + '">' + mesi[i] + '</th>';
    }
    thead.appendChild(trh);
    var tbody = document.getElementById("report_body");
    var nav = document.getElementById("nav");
    nav.innerHTML = "";
    tbody.innerHTML = "";
    for (var i = 0; i < array.length; i++) {
        var tr = document.createElement('tr');
        tr.innerHTML = '<td class="userid">' + array[i]['userid'] + '</td>' +
                '<td class="username">' + array[i]['username'] + '</td>' +
                '<td class="name">' + array[i]['name'] + '</td>' +
                '<td class="cf">' + array[i]['cf'] + '</td>' +
                '<td class="email">' + array[i]['email'] + '</td>' +
                '<td class="dn">' + array[i]['data_nascita'] + '</td>' +
                '<td class="ln">' + array[i]['citta'] + '</td>' +
                '<td class="phone">' + array[i]['phone'] + '</td>' +
                '<td class="azienda">' + array[i]['azienda'] + '</td>' +
                '<td class="tc">' + array[i]['tempo_tot'] + '</td>';
        var j = 0;
        for (const [key, value] of Object.entries(array[i]['mesi'])) {
            tr.innerHTML += '<td class="data_' + j + '">' + value + '</td>';
            j++;
        }
        tbody.appendChild(tr);
    }
    createDataTable();
}

function rhvpNameReport(array) {

    cleanDataTable();
    $('#report').DataTable({
        "data": array,
        dom: "Bfrtip",
        stateSave: true,
        buttons: [
            {
                extend: 'colvis',
                text: 'Colonne da non visualizzare',
                collectionLayout: 'fixed two-column'
            }
        ],
        "language": {
            "lengthMenu": "Mostra _MENU_ righe per pagina",
            "zeroRecords": "Nessun elemento trovato",
            "info": "Mostrando pagina _PAGE_ di _PAGES_ su _TOTAL_ righe",
            "infoEmpty": "Nessuna riga disponibile",
            "search": "Cerca",
            "paginate": {
                "previous": "Precedente",
                "next": "Successivo"
            }
        },
        "lengthMenu": [[50, 150, 300], [50, 150, 300]],
        autoWidth: false,
        scrollY: 300,
        scrollX: 300,
        "columns": [
            {"title": "Userid", "class": "userid", "data": "userid"},
            {"title": "Username", "class": "username", "data": "username"},
            {"title": "Nome Cognome", "class": "name", "data": "name"},
            {"title": "Codice Fiscale", "class": "cf", "data": "cf"},
            {"title": "Email", "class": "email", "data": "email"},
            {"title": "Data di Nascita", "class": "dn", "data": "data_nascita"},
            {"title": "Luogo di Nascita", "class": "ln", "data": "citta"},
            {"title": "N.di telefono", "class": "phone", "data": "phone"},
            {"title": "Azienda", "class": "azienda", "data": "azienda"},
            {"title": "Corso", "class": "corso", "data": "corso"},
            {"title": "Lezione", "class": "lezione", "data": "lezione"},
            {"title": "Totale effettivo di fruizione", "class": "orarioA", "data": "totalVisto"},
            {"title": "Durata complessiva", "class": "orarioL", "data": "totalCorso"},
            {"title": "Differenza", "class": "ts", "data": "diff"},
            {"title": "Percentuale completamento", "class": "percentage", "data": "percentage"}
        ],
        "order": [[3, "asc"]]
    });
}

function rhvpReport(array) {

    cleanDataTable();
    $('#report').DataTable({
        "data": array,
        dom: "Bfrtip",
        stateSave: true,
        buttons: [
            {
                extend: 'colvis',
                text: 'Colonne da non visualizzare',
                collectionLayout: 'fixed two-column'
            }
        ],
        "language": {
            "lengthMenu": "Mostra _MENU_ righe per pagina",
            "zeroRecords": "Nessun elemento trovato",
            "info": "Mostrando pagina _PAGE_ di _PAGES_ su _TOTAL_ righe",
            "infoEmpty": "Nessuna riga disponibile",
            "search": "Cerca",
            "paginate": {
                "previous": "Precedente",
                "next": "Successivo"
            }
        },
        "lengthMenu": [[50, 150, 300], [50, 150, 300]],
        autoWidth: false,
        scrollY: 300,
        scrollX: 300,
        "columns": [
            {"title": "Userid", "class": "userid", "data": "userid"},
            {"title": "Username", "class": "username", "data": "username"},
            {"title": "Nome Cognome", "class": "name", "data": "name"},
            {"title": "Codice Fiscale", "class": "cf", "data": "cf"},
            {"title": "Email", "class": "email", "data": "email"},
            {"title": "Data di Nascita", "class": "dn", "data": "data_nascita"},
            {"title": "Luogo di Nascita", "class": "ln", "data": "citta"},
            {"title": "N.di telefono", "class": "phone", "data": "phone"},
            {"title": "Totale effettivo di fruizione", "class": "orarioA", "data": "totalVisto"},
            {"title": "Durata complessiva", "class": "orarioL", "data": "totalCorso"},
            {"title": "Differenza", "class": "ts", "data": "diff"}
        ],
        "order": [[3, "asc"]]
    });
}

function sintesiReport(array) {


    cleanDataTable();
    var table;
    const createdCell = function (cell) {
        let original;
        let className;

        cell.setAttribute('contenteditable', true);
        cell.setAttribute('spellcheck', false);

        cell.addEventListener('focus', function (e) {
            className = cell.className;
            original = e.target.textContent;
        });

        cell.addEventListener('blur', function (e) {
            if (original !== e.target.textContent) {
                const row = table.row(e.target.parentElement);
                var data = row.data();
                if (className === "orarioA") {
                    updateField(data.id, data.id_logout);
                } else {
                    updateField(data.id_logout, data.id);
                }
            }
        });
    };

    if (idRuolo == 0) {
        table = $('#report').DataTable({
            "data": array,
            dom: "Bfrtip",
            stateSave: true,
            buttons: [
                {
                    extend: 'colvis',
                    text: 'Colonne da non visualizzare',
                    collectionLayout: 'fixed two-column'
                }
            ],
            "language": {
                "lengthMenu": "Mostra _MENU_ righe per pagina",
                "zeroRecords": "Nessun elemento trovato",
                "info": "Mostrando pagina _PAGE_ di _PAGES_ su _TOTAL_ righe",
                "infoEmpty": "Nessuna riga disponibile",
                "search": "Cerca",
                "paginate": {
                    "previous": "Precedente",
                    "next": "Successivo"
                }
            },
            "lengthMenu": [[50, 150, 300], [50, 150, 300]],
            autoWidth: false,
            scrollY: 300,
            scrollX: 300,
            "columns": [
                {"title": "Data", "class": "data", "data": "timecreated"},
                {"title": "Userid", "class": "userid", "data": "userid"},
                {"title": "Username", "class": "username", "data": "username"},
                {"title": "Nome Cognome", "class": "name", "data": "name"},
                {"title": "Codice Fiscale", "class": "cf", "data": "cf"},
                {"title": "Email", "class": "email", "data": "email"},
                {"title": "Data di Nascita", "class": "dn", "data": "data_nascita"},
                {"title": "Luogo di Nascita", "class": "ln", "data": "citta"},
                {"title": "N.di telefono", "class": "phone", "data": "phone"},
                {"title": "Azienda", "class": "azienda", "data": "azienda"},
                {"title": "Ora ingresso", "class": "orarioA", "data": "orario"},
                {"title": "Ora uscita", "class": "orarioL", "data": "orario_logout"},
                {"title": "Tempo cumulato", "class": "tc", "data": "tempo_cumulato"}
            ],
            columnDefs: [{
                    targets: [10, 11],
                    createdCell: createdCell
                }],
            "order": [[3, "asc"]]
        });
    } else {
        $('#report').DataTable({
            "data": array,
            dom: "Bfrtip",
            stateSave: true,
            buttons: [
                {
                    extend: 'colvis',
                    text: 'Colonne da non visualizzare',
                    collectionLayout: 'fixed two-column'
                }
            ],
            "language": {
                "lengthMenu": "Mostra _MENU_ righe per pagina",
                "zeroRecords": "Nessun elemento trovato",
                "info": "Mostrando pagina _PAGE_ di _PAGES_ su _TOTAL_ righe",
                "infoEmpty": "Nessuna riga disponibile",
                "search": "Cerca",
                "paginate": {
                    "previous": "Precedente",
                    "next": "Successivo"
                }
            },
            "lengthMenu": [[50, 150, 300], [50, 150, 300]],
            autoWidth: false,
            scrollY: 300,
            scrollX: 300,
            "columns": [
                {"title": "Data", "class": "data", "data": "timecreated"},
                {"title": "Userid", "class": "userid", "data": "userid"},
                {"title": "Username", "class": "username", "data": "username"},
                {"title": "Nome Cognome", "class": "name", "data": "name"},
                {"title": "Codice Fiscale", "class": "cf", "data": "cf"},
                {"title": "Email", "class": "email", "data": "email"},
                {"title": "Data di Nascita", "class": "dn", "data": "data_nascita"},
                {"title": "Luogo di Nascita", "class": "ln", "data": "citta"},
                {"title": "N.di telefono", "class": "phone", "data": "phone"},
                {"title": "Azienda", "class": "azienda", "data": "azienda"},
                {"title": "Ora ingresso", "class": "orarioA", "data": "orario"},
                {"title": "Ora uscita", "class": "orarioL", "data": "orario_logout"},
                {"title": "Tempo cumulato", "class": "tc", "data": "tempo_cumulato"}
            ],
            "order": [[3, "asc"]]
        });
    }
}

function sintesiBbb(array, mesi) {

    cleanDataTable();

    var thead = document.getElementById("report_head");
    thead.innerHTML = "";

    var trh = document.createElement('tr');
    trh.innerHTML =
            '<th class="userid"> Userid </th>' +
            '<th class="username"> Username </th>' +
            '<th class="name"> Nome Cognome </th>' +
            '<th class="cf"> Codice Fiscale </th>' +
            '<th class="email"> Email </th>' +
            '<th class="dn"> Data di Nascita </th>' +
            '<th class="ln"> Luogo di Nascita </th>' +
            '<th class="azienda"> Azienda </th>' +
            '<th class="phone">N.di telefono </th>' +
            '<th class="ruolo"> Ruolo </th>' +
            '<th class="tc"> Tempo cumulato </th>';

    for (var i = 0; i < mesi.length; i++) {
        trh.innerHTML += '<th class="data_' + i + '">' + mesi[i] + '</th>';
    }

    thead.appendChild(trh);
    var tbody = document.getElementById("report_body");
    var nav = document.getElementById("nav");
    nav.innerHTML = "";
    tbody.innerHTML = "";
    for (var i = 0; i < array.length; i++) {
        var tr = document.createElement('tr');
        tr.innerHTML = '<td class="userid">' + array[i]['userid'] + '</td>' +
                '<td class="username">' + array[i]['username'] + '</td>' +
                '<td class="name">' + array[i]['name'] + '</td>' +
                '<td class="cf">' + array[i]['cf'] + '</td>' +
                '<td class="email">' + array[i]['email'] + '</td>' +
                '<td class="dn">' + array[i]['data_nascita'] + '</td>' +
                '<td class="ln">' + array[i]['citta'] + '</td>' +
                '<td class="azienda">' + array[i]['azienda'] + '</td>' +
                '<td class="phone">' + array[i]['phone'] + '</td>' +
                '<td class="ruolo">' + array[i]['ruolo'] + '</td>' +
                '<td class="tc">' + array[i]['tempo_tot'] + '</td>';
        var j = 0;
        for (const [key, value] of Object.entries(array[i]['mesi'])) {
            tr.innerHTML += '<td class="data_' + j + '">' + value + '</td>';
            j++;
        }
        tbody.appendChild(tr);
    }
    createDataTable();
}

function sintesiBbbName(array, mesi) {

    cleanDataTable();

    var thead = document.getElementById("report_head");
    thead.innerHTML = "";

    var trh = document.createElement('tr');
    trh.innerHTML =
            '<th class="userid"> Userid </th>' +
            '<th class="username"> Username </th>' +
            '<th class="name"> Nome Cognome </th>' +
            '<th class="cf"> Codice Fiscale </th>' +
            '<th class="email"> Email </th>' +
            '<th class="dn"> Data di Nascita </th>' +
            '<th class="ln"> Luogo di Nascita </th>' +
            '<th class="azienda"> Azienda </th>' +
            '<th class="phone">N.di telefono </th>' +
            '<th class="ruolo"> Ruolo </th>' +
            '<th class="corso"> Corso </th>' +
            '<th class="courseid"> Id Corso </th>' +
            '<th class="tc"> Tempo cumulato </th>';

    for (var i = 0; i < mesi.length; i++) {
        trh.innerHTML += '<th class="data_' + i + '">' + mesi[i] + '</th>';
    }

    thead.appendChild(trh);
    var tbody = document.getElementById("report_body");
    var nav = document.getElementById("nav");
    nav.innerHTML = "";
    tbody.innerHTML = "";
    for (var i = 0; i < array.length; i++) {
        var tr = document.createElement('tr');
        tr.innerHTML = '<td class="userid">' + array[i]['userid'] + '</td>' +
                '<td class="username">' + array[i]['username'] + '</td>' +
                '<td class="name">' + array[i]['name'] + '</td>' +
                '<td class="cf">' + array[i]['cf'] + '</td>' +
                '<td class="email">' + array[i]['email'] + '</td>' +
                '<td class="dn">' + array[i]['data_nascita'] + '</td>' +
                '<td class="ln">' + array[i]['citta'] + '</td>' +
                '<td class="azienda">' + array[i]['azienda'] + '</td>' +
                '<td class="phone">' + array[i]['phone'] + '</td>' +
                '<td class="ruolo">' + array[i]['ruolo'] + '</td>' +
                '<td class="corso">' + array[i]['corso'] + '</td>' +
                '<td class="courseid">' + array[i]['courseid'] + '</td>' +
                '<td class="tc">' + array[i]['tempo_tot'] + '</td>';
        var j = 0;
        for (const [key, value] of Object.entries(array[i]['mesi'])) {
            tr.innerHTML += '<td class="data_' + j + '">' + value + '</td>';
            j++;
        }
        tbody.appendChild(tr);
    }
    createDataTable();
}

function bbbReport(array) {

    cleanDataTable();
    var table;
    const createdCell = function (cell) {
        let original;
        let className;

        cell.setAttribute('contenteditable', true);
        cell.setAttribute('spellcheck', false);

        cell.addEventListener('focus', function (e) {
            className = cell.className;
            original = e.target.textContent;
        });

        cell.addEventListener('blur', function (e) {
            if (original !== e.target.textContent) {
                const row = table.row(e.target.parentElement);
                var data = row.data();
                if (className === "orarioA") {
                    updateField(data.id, data.id_logout);
                } else {
                    updateField(data.id_logout, data.id);
                }


            }
        });
    };

    if (idRuolo == 0) {
        table = $('#report').DataTable({
            "data": array,
            dom: "Bfrtip",
            stateSave: true,
            buttons: [
                {
                    extend: 'colvis',
                    text: 'Colonne da non visualizzare',
                    collectionLayout: 'fixed two-column'
                }
            ],
            "language": {
                "lengthMenu": "Mostra _MENU_ righe per pagina",
                "zeroRecords": "Nessun elemento trovato",
                "info": "Mostrando pagina _PAGE_ di _PAGES_ su _TOTAL_ righe",
                "infoEmpty": "Nessuna riga disponibile",
                "search": "Cerca",
                "paginate": {
                    "previous": "Precedente",
                    "next": "Successivo"
                }
            },
            "lengthMenu": [[50, 150, 300], [50, 150, 300]],
            autoWidth: false,
            scrollY: 300,
            scrollX: 300,
            "columns": [
                {"title": "Data", "class": "data", "data": "timecreated"},
                {"title": "Userid", "class": "userid", "data": "userid"},
                {"title": "Username", "class": "username", "data": "username"},
                {"title": "Nome Cognome", "class": "name", "data": "name"},
                {"title": "Codice Fiscale", "class": "cf", "data": "cf"},
                {"title": "Email", "class": "email", "data": "email"},
                {"title": "Data di Nascita", "class": "dn", "data": "data_nascita"},
                {"title": "Luogo di Nascita", "class": "ln", "data": "citta"},
                {"title": "Azienda", "class": "azienda", "data": "azienda"},
                {"title": "N.di telefono", "class": "phone", "data": "phone"},
                {"title": "Ruolo", "class": "ruolo", "data": "ruolo"},
                {"title": "Corso", "class": "corso", "data": "corso"},
                {"title": "Nome aula", "class": "aula", "data": "aula"},
                {"title": "Ora ingresso", "class": "orarioA", "data": "orario"},
                {"title": "Ora uscita", "class": "orarioL", "data": "orario_logout"},
                {"title": "Tempo cumulato", "class": "tc", "data": "tempo_cumulato"}
            ],
            columnDefs: [{
                    targets: [13, 14],
                    createdCell: createdCell
                }],
            "order": [[3, "asc"]]
        });
    } else {
        $('#report').DataTable({
            "data": array,
            dom: "Bfrtip",
            stateSave: true,
            buttons: [
                {
                    extend: 'colvis',
                    text: 'Colonne da non visualizzare',
                    collectionLayout: 'fixed two-column'
                }
            ],
            "language": {
                "lengthMenu": "Mostra _MENU_ righe per pagina",
                "zeroRecords": "Nessun elemento trovato",
                "info": "Mostrando pagina _PAGE_ di _PAGES_ su _TOTAL_ righe",
                "infoEmpty": "Nessuna riga disponibile",
                "search": "Cerca",
                "paginate": {
                    "previous": "Precedente",
                    "next": "Successivo"
                }
            },
            "lengthMenu": [[50, 150, 300], [50, 150, 300]],
            autoWidth: false,
            scrollY: 300,
            scrollX: 300,
            "columns": [
                {"title": "Data", "class": "data", "data": "timecreated"},
                {"title": "Userid", "class": "userid", "data": "userid"},
                {"title": "Username", "class": "username", "data": "username"},
                {"title": "Nome Cognome", "class": "name", "data": "name"},
                {"title": "Codice Fiscale", "class": "cf", "data": "cf"},
                {"title": "Email", "class": "email", "data": "email"},
                {"title": "Data di Nascita", "class": "dn", "data": "data_nascita"},
                {"title": "Luogo di Nascita", "class": "ln", "data": "citta"},
                {"title": "Azienda", "class": "azienda", "data": "azienda"},
                {"title": "N.di telefono", "class": "phone", "data": "phone"},
                {"title": "Ruolo", "class": "ruolo", "data": "ruolo"},
                {"title": "Corso", "class": "corso", "data": "corso"},
                {"title": "Nome aula", "class": "aula", "data": "aula"},
                {"title": "Ora ingresso", "class": "orarioA", "data": "orario"},
                {"title": "Ora uscita", "class": "orarioL", "data": "orario_logout"},
                {"title": "Tempo cumulato", "class": "tc", "data": "tempo_cumulato"}
            ],
            "order": [[3, "asc"]]
        });
    }
}

function sintesiHvp(array, mesi) {

    cleanDataTable();
    var thead = document.getElementById("report_head");
    thead.innerHTML = "";

    var trh = document.createElement('tr');
    trh.innerHTML =
        '<th class="userid"> Userid </th>' +
        '<th class="username"> Username </th>' +
        '<th class="name"> Nome Cognome </th>' +
        '<th class="cf"> Codice Fiscale </th>' +
        '<th class="email"> Email </th>' +
        '<th class="dn"> Data di Nascita </th>' +
        '<th class="ln"> Luogo di Nascita </th>' +
        '<th class="azienda"> Azienda </th>' +
        '<th class="phone">N.di telefono </th>' +
        //            '<th class="corso"> Corso </th>' +
        //            '<th class="courseid"> Id Corso </th>' +
        '<th class="tc"> Tempo cumulato </th>';
    for (var i = 0; i < mesi.length; i++) {
        trh.innerHTML += '<th class="data_' + i + '">' + mesi[i] + '</th>';
    }
    thead.appendChild(trh);
    var tbody = document.getElementById("report_body");
    var nav = document.getElementById("nav");
    nav.innerHTML = "";
    tbody.innerHTML = "";
    for (var i = 0; i < array.length; i++) {
        var tr = document.createElement('tr');
        tr.innerHTML = '<td class="userid">' + array[i]['userid'] + '</td>' +
            '<td class="username">' + array[i]['username'] + '</td>' +
            '<td class="name">' + array[i]['name'] + '</td>' +
            '<td class="cf">' + array[i]['cf'] + '</td>' +
            '<td class="email">' + array[i]['email'] + '</td>' +
            '<td class="dn">' + array[i]['data_nascita'] + '</td>' +
            '<td class="ln">' + array[i]['citta'] + '</td>' +
            '<td class="azienda">' + array[i]['azienda'] + '</td>' +
            '<td class="phone">' + array[i]['phone'] + '</td>' +
            //                '<td class="corso">' + array[i]['corso'] + '</td>' +
            //                '<td class="courseid">' + array[i]['courseid'] + '</td>' +
            '<td class="tc">' + array[i]['tempo_tot'] + '</td>';
        var j = 0;
        for (const [key, value] of Object.entries(array[i]['mesi'])) {
            tr.innerHTML += '<td class="data_' + j + '">' + value + '</td>';
            j++;
        }
        tbody.appendChild(tr);
    }
    createDataTable();

}

function sintesiHvpNew(array, period_dates) {
    var columns = [];
    columns.push({"title": "User ID"        , "data": "user_id"});
    columns.push({"title": "Username"       , "data": "username"});
    columns.push({"title": "Nome Cognome"   , "data": "user_fullname"});
    columns.push({"title": "Codice Fiscale" , "data": "user_fiscalcode"});
    columns.push({"title": "Email"          , "data": "user_email"});
    columns.push({"title": "Data di nascita", "data": "user_date_of_birth"});
    columns.push({"title": "Luogo Nascita"  , "data": "user_city_of_birth"});
    columns.push({"title": "Azienda"        , "data": "user_company_name"});
    columns.push({"title": "Num telefono"   , "data": "user_telephone"});
    //columns.push({"title": "Corso"          , "data": "course_name"});
    //columns.push({"title": "Corso ID"       , "data": "course_id"});
    columns.push({"title": "Totale ore"     , "data": "total_time"});
    for (const [key, value] of Object.entries(period_dates)) {
        columns.push({"title": value, "data": key});
    }

    cleanDataTable();
    $('#report').DataTable({
        "data": array,
        dom: "Bfrtip",
        stateSave: true,
        buttons: [
            {
                extend: 'colvis',
                text: 'Colonne da non visualizzare',
                collectionLayout: 'fixed two-column'
            }
        ],
        "language": {
            "lengthMenu": "Mostra _MENU_ righe per pagina",
            "zeroRecords": "Nessun elemento trovato",
            "info": "Mostrando pagina _PAGE_ di _PAGES_ su _TOTAL_ righe",
            "infoEmpty": "Nessuna riga disponibile",
            "search": "Cerca",
            "paginate": {
                "previous": "Precedente",
                "next": "Successivo"
            }
        },
        "lengthMenu": [[50, 150, 300], [50, 150, 300]],
        autoWidth: false,
        scrollY: 300,
        scrollX: 300,
        "columns": columns,
        "order": [[3, "asc"]]
    });

}

function sintesiHvpName(array, mesi) {

    cleanDataTable();
    var thead = document.getElementById("report_head");
    thead.innerHTML = "";

    var trh = document.createElement('tr');
    trh.innerHTML =
            '<th class="userid"> Userid </th>' +
            '<th class="username"> Username </th>' +
            '<th class="name"> Nome Cognome </th>' +
            '<th class="cf"> Codice Fiscale </th>' +
            '<th class="email"> Email </th>' +
            '<th class="dn"> Data di Nascita </th>' +
            '<th class="ln"> Luogo di Nascita </th>' +
            '<th class="azienda"> Azienda </th>' +
            '<th class="phone">N.di telefono </th>' +
            '<th class="corso"> Corso </th>' +
            '<th class="courseid"> Id Corso </th>' +
            '<th class="tc"> Tempo cumulato </th>';
    for (var i = 0; i < mesi.length; i++) {
        trh.innerHTML += '<th class="data_' + i + '">' + mesi[i] + '</th>';
    }
    thead.appendChild(trh);
    var tbody = document.getElementById("report_body");
    var nav = document.getElementById("nav");
    nav.innerHTML = "";
    tbody.innerHTML = "";
    for (var i = 0; i < array.length; i++) {
        console.log(array);
        var tr = document.createElement('tr');
        tr.innerHTML = '<td class="userid">' + array[i]['userid'] + '</td>' +
                '<td class="username">' + array[i]['username'] + '</td>' +
                '<td class="name">' + array[i]['name'] + '</td>' +
                '<td class="cf">' + array[i]['cf'] + '</td>' +
                '<td class="email">' + array[i]['email'] + '</td>' +
                '<td class="dn">' + array[i]['data_nascita'] + '</td>' +
                '<td class="ln">' + array[i]['citta'] + '</td>' +
                '<td class="azienda">' + array[i]['azienda'] + '</td>' +
                '<td class="phone">' + array[i]['phone'] + '</td>' +
                '<td class="corso">' + array[i]['corso'] + '</td>' +
                '<td class="courseid">' + array[i]['courseid'] + '</td>' +
                '<td class="tc">' + array[i]['tempo_tot'] + '</td>';
        var j = 0;
        for (const [key, value] of Object.entries(array[i]['mesi'])) {
            tr.innerHTML += '<td class="data_' + j + '">' + value + '</td>';
            j++;
        }
        tbody.appendChild(tr);
    }
    createDataTable();

}

function hvpReport(array) {

    cleanDataTable();
    $('#report').DataTable({
        "data": array,
        dom: "Bfrtip",
        stateSave: true,
        buttons: [
            {
                extend: 'colvis',
                text: 'Colonne da non visualizzare',
                collectionLayout: 'fixed two-column'
            }
        ],
        "language": {
            "lengthMenu": "Mostra _MENU_ righe per pagina",
            "zeroRecords": "Nessun elemento trovato",
            "info": "Mostrando pagina _PAGE_ di _PAGES_ su _TOTAL_ righe",
            "infoEmpty": "Nessuna riga disponibile",
            "search": "Cerca",
            "paginate": {
                "previous": "Precedente",
                "next": "Successivo"
            }
        },
        "lengthMenu": [[50, 150, 300], [50, 150, 300]],
        autoWidth: false,
        scrollY: 300,
        scrollX: 300,
        "columns": [
            {"title": "Data", "class": "data", "data": "timecreated"},
            {"title": "Userid", "class": "userid", "data": "userid"},
            {"title": "Username", "class": "username", "data": "username"},
            {"title": "Nome Cognome", "class": "name", "data": "name"},
            {"title": "Codice Fiscale", "class": "cf", "data": "cf"},
            {"title": "Email", "class": "email", "data": "email"},
            {"title": "Data di Nascita", "class": "dn", "data": "data_nascita"},
            {"title": "Luogo di Nascita", "class": "ln", "data": "citta"},
            {"title": "N.di telefono", "class": "phone", "data": "phone"},
            {"title": "Corso", "class": "corso", "data": "corso"},
            {"title": "Lezione", "class": "lezione", "data": "lezione"},
            {"title": "Ora accesso al video", "class": "orarioA", "data": "orario"},
            {"title": "Ora uscita dal video", "class": "orarioL", "data": "orario_logout"},
            {"title": "Tempo cumulato", "class": "tc", "data": "tempo_cumulato"},
            {"title": "Tempo sessione video", "class": "ts", "data": "diff"}
        ],
        "order": [[3, "asc"]]
    });
}

function hvpReportNew(array) {

    cleanDataTable();
    $('#report').DataTable({
        "data": array,
        dom: "Bfrtip",
        stateSave: true,
        buttons: [
            {
                extend: 'colvis',
                text: 'Colonne da non visualizzare',
                collectionLayout: 'fixed two-column'
            }
        ],
        "language": {
            "lengthMenu": "Mostra _MENU_ righe per pagina",
            "zeroRecords": "Nessun elemento trovato",
            "info": "Mostrando pagina _PAGE_ di _PAGES_ su _TOTAL_ righe",
            "infoEmpty": "Nessuna riga disponibile",
            "search": "Cerca",
            "paginate": {
                "previous": "Precedente",
                "next": "Successivo"
            }
        },
        "lengthMenu": [[50, 150, 300], [50, 150, 300]],
        autoWidth: false,
        scrollY: 300,
        scrollX: 300,
        "columns": [
            {"title": "Data"                    , "data": "date"},
            {"title": "Userid"                  , "data": "user_id"},
            {"title": "Username"                , "data": "username"},
            {"title": "Nome Cognome"            , "data": "user_fullname"},
            {"title": "Codice Fiscale"          , "data": "user_fiscalcode"},
            {"title": "Email"                   , "data": "user_email"},
            {"title": "Data di Nascita"         , "data": "user_date_of_birth"},
            {"title": "Luogo di Nascita"        , "data": "user_city_of_birth"},
            {"title": "Azienda"                 , "data": "user_company_name"},
            {"title": "N.di telefono"           , "data": "user_telephone"},
            {"title": "Corso"                   , "data": "course_name"},
            {"title": "Lezione"                 , "data": "module_name"},
            {"title": "Ora accesso al video"    , "data": "start_session_time"},
            {"title": "Ora uscita dal video"    , "data": "end_session_time"},
            {"title": "Minuti cumulati su video", "data": "progress_time"},
            {"title": "Tempo sessione video"    , "data": "session_time"}
        ],
        "order": [[3, "asc"]]
    });
}

function summaryHvpReport(array) {

    cleanDataTable();
    $('#report').DataTable({
        "data": array,
        dom: "Bfrtip",
        stateSave: true,
        buttons: [
            {
                extend: 'colvis',
                text: 'Colonne da non visualizzare',
                collectionLayout: 'fixed two-column'
            }
        ],
        "language": {
            "lengthMenu": "Mostra _MENU_ righe per pagina",
            "zeroRecords": "Nessun elemento trovato",
            "info": "Mostrando pagina _PAGE_ di _PAGES_ su _TOTAL_ righe",
            "infoEmpty": "Nessuna riga disponibile",
            "search": "Cerca",
            "paginate": {
                "previous": "Precedente",
                "next": "Successivo"
            }
        },
        "lengthMenu": [[50, 150, 300], [50, 150, 300]],
        autoWidth: false,
        scrollY: 300,
        scrollX: 300,
        "columns": [
            {"title": "Userid", "class": "userid", "data": "userid"},
            {"title": "Username", "class": "username", "data": "username"},
            {"title": "Nome Cognome", "class": "name", "data": "name"},
            {"title": "Codice Fiscale", "class": "cf", "data": "cf"},
            {"title": "Email", "class": "email", "data": "email"},
            {"title": "Data di Nascita", "class": "dn", "data": "data_nascita"},
            {"title": "Luogo di Nascita", "class": "ln", "data": "citta"},
            {"title": "N.di telefono", "class": "phone", "data": "phone"},
            {"title": "Corso", "class": "corso", "data": "corso"},
            {"title": "Totale ore corso", "class": "total_time", "data": "total_time"}
        ],
        "order": [[3, "asc"]]
    });
}

function logstdReport(array) {

    cleanDataTable();

    $('#report').DataTable({
        "data": array,
        dom: "Bfrtip",
        stateSave: true,
        buttons: [
            {
                extend: 'colvis',
                text: 'Colonne da non visualizzare',
                collectionLayout: 'fixed two-column'
            }
        ],
        "language": {
            "lengthMenu": "Mostra _MENU_ righe per pagina",
            "zeroRecords": "Nessun elemento trovato",
            "info": "Mostrando pagina _PAGE_ di _PAGES_ su _TOTAL_ righe",
            "infoEmpty": "Nessuna riga disponibile",
            "search": "Cerca",
            "paginate": {
                "previous": "Precedente",
                "next": "Successivo"
            }
        },
        "lengthMenu": [[50, 150, 300], [50, 150, 300]],
        autoWidth: false,
        scrollY: 300,
        scrollX: 300,
        "columns": [
            {"title": "Data/Ora", "class": "data", "data": "timecreated"},
            {"title": "Userid", "class": "userid", "data": "userid"},
            {"title": "Username", "class": "username", "data": "username"},
            {"title": "Nome Cognome", "class": "name", "data": "name"},
            {"title": "Codice Fiscale", "class": "cf", "data": "cf"},
            {"title": "Email", "class": "email", "data": "email"},
            {"title": "Data di Nascita", "class": "dn", "data": "data_nascita"},
            {"title": "Luogo di Nascita", "class": "ln", "data": "citta"},
            {"title": "N.di telefono", "class": "phone", "data": "phone"},
            {"title": "Attività tracciata", "class": "attivita", "data": "attivita"},
            {"title": "Azione", "class": "azione", "data": "action"},
            {"title": "Ip", "class": "ip", "data": "ip"}
        ],
        "order": [[3, "asc"]]

    });
}


function s_zoomReport(array, mesi) {

    cleanDataTable();

    var thead = document.getElementById("report_head");
    thead.innerHTML = "";

    var trh = document.createElement('tr');
    trh.innerHTML =
            '<th class="userid"> Userid </th>' +
            '<th class="username"> Username </th>' +
            '<th class="name"> Nome Cognome </th>' +
            '<th class="cf"> Codice Fiscale </th>' +
            '<th class="email"> Email </th>' +
            '<th class="dn"> Data di Nascita </th>' +
            '<th class="ln"> Luogo di Nascita </th>' +
            '<th class="azienda"> Azienda </th>' +
            '<th class="phone">N.di telefono </th>' +
            '<th class="ruolo">Ruolo </th>' +
            '<th class="corso">Corso </th>' +
            '<th class="idCorso">Id Corso </th>' +
            '<th class="tc"> Tempo cumulato </th>';
    for (var i = 0; i < mesi.length; i++) {
        trh.innerHTML += '<th class="data_' + i + '">' + mesi[i] + '</th>';
    }
    thead.appendChild(trh);
    var tbody = document.getElementById("report_body");
    var nav = document.getElementById("nav");
    nav.innerHTML = "";
    tbody.innerHTML = "";
    for (var i = 0; i < array.length; i++) {
        var tr = document.createElement('tr');
        tr.innerHTML = '<td class="userid">' + array[i]['userid'] + '</td>' +
                '<td class="username">' + array[i]['username'] + '</td>' +
                '<td class="name">' + array[i]['nome'] + '</td>' +
                '<td class="cf">' + array[i]['cf'] + '</td>' +
                '<td class="email">' + array[i]['email'] + '</td>' +
                '<td class="dn">' + array[i]['data_nascita'] + '</td>' +
                '<td class="ln">' + array[i]['citta'] + '</td>' +
                '<td class="azienda">' + array[i]['azienda'] + '</td>' +
                '<td class="phone">' + array[i]['phone'] + '</td>' +
                '<td class="ruolo">' + array[i]['ruolo'] + '</td>' +
                '<td class="corso">' + array[i]['nomeCorso'] + '</td>' +
                '<td class="idCorso">' + array[i]['idCorso'] + '</td>' +
                '<td class="tc">' + array[i]['tempo_tot'] + '</td>';
        var j = 0;
        for (const [key, value] of Object.entries(array[i]['mesi'])) {
            tr.innerHTML += '<td class="data_' + j + '">' + value + '</td>';
            j++;
        }
        tbody.appendChild(tr);
    }
    createDataTable();

}

function zoomReport(array) {

    cleanDataTable();
    $('#report').DataTable({
        "data": array,
        dom: "Bfrtip",
        stateSave: true,
        buttons: [
            {
                extend: 'colvis',
                text: 'Colonne da non visualizzare',
                collectionLayout: 'fixed two-column'
            }
        ],
        "language": {
            "lengthMenu": "Mostra _MENU_ righe per pagina",
            "zeroRecords": "Nessun elemento trovato",
            "info": "Mostrando pagina _PAGE_ di _PAGES_ su _TOTAL_ righe",
            "infoEmpty": "Nessuna riga disponibile",
            "search": "Cerca",
            "paginate": {
                "previous": "Precedente",
                "next": "Successivo"
            }
        },
        "lengthMenu": [[50, 150, 300], [50, 150, 300]],
        autoWidth: false,
        scrollY: 300,
        scrollX: 300,
        "bDeferRender": true,
        "columns": [
            {"title": "Data", "class": "data", "data": "timecreated"},
            {"title": "Userid", "class": "userid", "data": "userid"},
            {"title": "Username", "class": "username", "data": "username"},
            {"title": "Nome Cognome", "class": "name", "data": "nome"},
            {"title": "Codice Fiscale", "class": "cf", "data": "cf"},
            {"title": "Email", "class": "email", "data": "email"},
            {"title": "Data di Nascita", "class": "dn", "data": "data_nascita"},
            {"title": "Luogo di Nascita", "class": "ln", "data": "citta"},
            {"title": "N.di telefono", "class": "phone", "data": "phone"},
            {"title": "Ruolo", "class": "ruolo", "data": "ruolo"},
            {"title": "Corso", "class": "corso", "data": "nomeCorso"},
            {"title": "Nome aula", "class": "aula", "data": "name"},
            {"title": "Ora ingresso", "class": "orarioA", "data": "orario"},
            {"title": "Ora uscita", "class": "orarioL", "data": "orario_logout"},
            {"title": "Tempo cumulato", "class": "tc", "data": "tempo_cumulato"}
        ]
    });
}

function videotimeproReport(array, period_dates) {
    var columns = [];
    columns.push({"title": "User ID"        , "data": "user_id"});
    columns.push({"title": "Username"       , "data": "username"});
    columns.push({"title": "Nome Cognome"   , "data": "fullname"});
    columns.push({"title": "Codice Fiscale" , "data": "fiscal_code"});
    columns.push({"title": "Email"          , "data": "email"});
    columns.push({"title": "Data di nascita", "data": "date_of_birth"});
    columns.push({"title": "Luogo Nascita"  , "data": "city"});
    columns.push({"title": "Azienda"        , "data": "company_name"});
    columns.push({"title": "Num telefono"   , "data": "telephone"});
    columns.push({"title": "Corso"          , "data": "course_name"});
    columns.push({"title": "Corso ID"       , "data": "course_id"});
    columns.push({"title": "Totale ore"     , "data": "total_time"});
    for (const [key, value] of Object.entries(period_dates)) {
        columns.push({"title": value, "data": key});
    }

    cleanDataTable();
    $('#report').DataTable({
        "data": array,
        dom: "Bfrtip",
        stateSave: true,
        buttons: [
            {
                extend: 'colvis',
                text: 'Colonne da non visualizzare',
                collectionLayout: 'fixed two-column'
            }
        ],
        "language": {
            "lengthMenu": "Mostra _MENU_ righe per pagina",
            "zeroRecords": "Nessun elemento trovato",
            "info": "Mostrando pagina _PAGE_ di _PAGES_ su _TOTAL_ righe",
            "infoEmpty": "Nessuna riga disponibile",
            "search": "Cerca",
            "paginate": {
                "previous": "Precedente",
                "next": "Successivo"
            }
        },
        "lengthMenu": [[50, 150, 300], [50, 150, 300]],
        autoWidth: false,
        scrollY: 300,
        scrollX: 300,
        "columns": columns,
        "order": [[3, "asc"]]
    });
}


/*function doPaginazione(totale, report, array, mesi) {




 var rowsShown = 150;
 var rowsTotal = totale;
 var numPages = (rowsTotal / rowsShown);
 numPages = numPages.toFixed(0);
 console.log("Pagina: " + currPage);
 console.log("Numero di Pagine: " + numPages);
 console.log("Totale: " + rowsTotal);
 var inizio = -(currPage - 1);
 if (currPage != 1) {
 $('#nav').append('<a href="#report" style="margin-left: 6px;cursor: pointer;" rel=' + Number(inizio) + '>' + " << " + '</a>');
 $('#nav').append('<a href="#report" style="margin-left: 6px;cursor: pointer; margin-right: 6px;" rel="-1">' + " < " + '</a>');
 }
 $('#nav').append(" " + currPage + " ");
 if ((currPage + 1) <= numPages)
 $('#nav').append('<a href="#report" style="margin-left: 6px;cursor: pointer;" rel="1">' + Number(currPage + 1) + " " + '</a>');
 if ((currPage + 2) <= numPages)
 $('#nav').append('<a href="#report" style="margin-left: 6px;cursor: pointer;" rel="2">' + Number(currPage + 2) + " " + '</a>');
 if ((currPage + 3) <= numPages)
 $('#nav').append('<a href="#report" style="margin-left: 6px;cursor: pointer;" rel="3">' + Number(currPage + 3) + " " + '</a>');
 if ((currPage + 4) <= numPages)
 $('#nav').append('<a href="#report" style="margin-left: 6px;cursor: pointer;" rel="4">' + Number(currPage + 4) + " " + '</a>');
 if ((currPage + 5) <= numPages)
 $('#nav').append('<a href="#report" style="margin-left: 6px;cursor: pointer;" rel="5">' + Number(currPage + 5) + " " + '</a>');
 if ((currPage + 6) <= numPages)
 $('#nav').append('<a href="#report" style="margin-left: 6px;cursor: pointer;" rel="6">' + Number(currPage + 6) + " " + '</a>');
 if ((currPage + 7) <= numPages)
 $('#nav').append('<a href="#report" style="margin-left: 6px;cursor: pointer;" rel="7">' + Number(currPage + 7) + " " + '</a>');
 if ((currPage + 8) <= numPages)
 $('#nav').append('<a href="#report" style="margin-left: 6px;cursor: pointer;" rel="8">' + Number(currPage + 8) + " " + '</a>');
 if ((currPage + 9) <= numPages)
 $('#nav').append('<a href="#report" style="margin-left: 6px;cursor: pointer;" rel="9">' + Number(currPage + 9) + " " + '</a>');
 if ((currPage + 10) <= numPages)
 $('#nav').append('<a href="#report" style="margin-left: 6px;cursor: pointer;" rel="10">' + Number(currPage + 10) + " " + '</a>');
 if ((currPage + 1) <= numPages)
 $('#nav').append('<a href="#report" id="avanti"  style="margin-left: 6px;cursor: pointer;" rel="1">' + " > " + '</a>');
 if (currPage < numPages)
 $('#nav').append('<a href="#report" style="margin-left: 6px;cursor: pointer;" rel=' + Number(numPages - currPage) + '>' + " >> " + '</a>');
 $('#report tbody tr').hide();
 $('#report tbody tr').slice(0, rowsShown).show();
 $('#nav a').bind('click', function () {

 //showPleaseWait("Attendi");
 $(this).addClass('active');
 currPage += Number($(this).attr('rel'));
 var startItem = (currPage - 1) * rowsShown;
 var arraySlice = array.slice(startItem, Number(startItem + 150));
 if (totale != 0) {
 if (report == 'zoom' || report == "zoomName") {
 zoomReport(arraySlice);
 } else if (report == "total" || report == 'totalName') {
 logstdReport(arraySlice);
 } else if (report == "hvp" || report == "hvpName") {
 hvpReport(arraySlice);
 } else if (report == "bbb" || report == 'bbbName') {
 bbbReport(arraySlice);
 } else if (report == "sintesi" || report == 'sintesiName') {
 sintesiReport(arraySlice);
 } else if (report == "timein") {

 timeinReport(arraySlice, mesi);
 } else if (report == "shvp") {
 sintesiHvp(arraySlice, mesi);
 } else if (report == "sbbb" || report == "sbbbName") {
 sintesiBbb(arraySlice, mesi);
 } else if (report == "rhvp") {
 rhvpReport(arraySlice);
 } else if (report == "s_zoom" || report == "s_zoomName") {

 s_zoomReport(arraySlice, mesi);
 }

 doPaginazione(totale, report, array, mesi);
 }
 });
 /* $.ajax({
 // url: "./report/table.php",
 method: 'GET',
 url: "stub_table.php",
 data: {
 enterprise: enterprise,
 start: start,
 end: end,
 report: report,
 skip: startItem

 }
 })
 .done(function (resultParsed) {
 var arraySplit = resultParsed.split('*?');
 var array = JSON.parse(arraySplit[0]);
 console.log(array);
 if (arraySplit[1] != 0) {
 if (report == 'zoom') {
 zoomReport(array);
 } else if (report == 'total') {
 logstdReport(array);
 } else if (report == "hvp") {
 hvpReport(array);
 } else if (report == "bbb") {
 bbbReport(array);
 } else if (report == "sintesi") {
 sintesiReport(array);
 } else if (report == "timein") {
 var mesi = JSON.parse(arraySplit[2]);
 timeinReport(array, mesi);
 }

 doPaginazione(arraySplit[1], report, array);
 hidePleaseWait();


 } else {
 $("#report_head").html("");
 $("#report_body").html("");
 $("#nav").html("");
 $("#nav").after('<p id="noLog" style="display: flex;justify-content:center;">Nessun log trovato!</p>');
 }
 })
 .fail(function (jqXHR, exception) {
 var msg = '';
 if (jqXHR.status === 0) {
 msg = 'Not connect.\n Verify Network.';
 } else if (jqXHR.status == 404) {
 msg = 'Requested page not found. [404]';
 } else if (jqXHR.status == 500) {
 msg = 'Internal Server Error [500].';
 } else if (exception === 'parsererror') {
 msg = 'Requested JSON parse failed.';
 } else if (exception === 'timeout') {
 msg = 'Time out error.';
 } else if (exception === 'abort') {
 msg = 'Ajax request aborted.';
 } else {
 msg = 'Uncaught Error.\n' + jqXHR.responseText;
 }
 console.log(msg);
 });

 });*/
//}



function getExcel(report) {

    if (report == 'total') {
        var enterprise = $("#id_enterprise_total").val();
        var start = $("#id_start_total").val();
        var end = $("#id_end_total").val();
    }
    else if (report == 'hvp') {
        var enterprise = $("#id_enterprise_hvp").val();
        var start = $("#id_start_hvp").val();
        var end = $("#id_end_hvp").val();
    }
    else if (report == 'hvp_new') {
        var enterprise = $("#id_enterprise_hvp_new").val();
        var start = $("#id_start_hvp_new").val();
        var end = $("#id_end_hvp_new").val();
    }
    else if (report == 'summaryhvp') {
        var enterprise = $("#id_enterprise_summaryhvp").val();
        var start = $("#id_start_summaryhvp").val();
        var end = $("#id_end_summaryhvp").val();
    }
    else if (report == 'summaryhvp_new') {
        var enterprise = $("#id_enterprise_summaryhvp_new").val();
        var start = $("#id_start_summaryhvp_new").val();
        var end = $("#id_end_summaryhvp_new").val();
    }
    else if (report == 'bbb') {
        var enterprise = $("#id_enterprise_bbb").val();
        var start = $("#id_start_bbb").val();
        var end = $("#id_end_bbb").val();
    }
    else if (report == 'sbbb') {
        var enterprise = $("#id_enterprise_sbbb").val();
        var start = $("#id_start_sbbb").val();
        var end = $("#id_end_sbbb").val();
    }
    else if (report == 'sbbbName') {
        var enterprise = $("#id_enterprise_sbbbN option:selected").text();
        var start = $("#id_start_sbbb_iscritto").val();
        var end = $("#id_end_sbbb_iscritto").val();
    }
    else if (report == 'zoom') {
        var enterprise = $("#id_enterprise_zoom").val();
        var start = $("#id_start_zoom").val();
        var end = $("#id_end_zoom").val();
    }
    else if (report == 's_zoom') {
        var enterprise = $("#id_enterprise_s_zoom").val();
        var start = $("#id_start_s_zoom").val();
        var end = $("#id_end_s_zoom").val();
    }
    else if (report == 's_zoomName') {
        var enterprise = $("#id_enterprise_s_zoomName option:selected").text();
        var start = $("#id_start_s_zoom_iscritto").val();
        var end = $("#id_end_s_zoom_iscritto").val();
    }
    else if (report == 'zoom_live') {
        method = "POST";
        var course = "";
        var meeting = {};
        if ($("#flexCheckAllZoom").is(':checked')) {
            course = "all";
            meeting = zoomMeetings;
        } else {
            course = $("#id_meetings_list").val();
            meeting[course] = zoomMeetings[course];
        }
    }
    else if (report == "sintesi") {
        var enterprise = $("#id_enterprise_sintesi").val();
        var start = $("#id_start_sintesi").val();
        var end = $("#id_end_sintesi").val();
    }
    else if (report == "timein") {
        var enterprise = $("#id_enterprise_timein").val();
        var start = $("#id_start_timein").val();
        var end = $("#id_end_timein").val();
    }
    else if (report == "sintesiName") {
        var enterprise = $("#id_iscritto_sintesi").val();
        var start = $("#id_start_sintesi_iscritto").val();
        var end = $("#id_end_sintesi_iscritto").val();
    }
    else if (report == "totalName") {
        var enterprise = $("#id_iscritto_total").val();
        var start = $("#id_start_total_iscritto").val();
        var end = $("#id_end_total_iscritto").val();
    }
    else if (report == "bbbName") {
        var enterprise = $("#id_enterprise_bbbName option:selected").text();
        var start = $("#id_start_bbb_course").val();
        var end = $("#id_end_bbb_course").val();
    }
    else if (report == "hvpName") {
        var enterprise = $("#id_enterprise_hvpName option:selected").text();
        var start = $("#id_start_hvp_course").val();
        var end = $("#id_end_hvp_course").val();
    }
    else if (report == "zoomName") {
        var enterprise = $("#id_enterprise_zoomName option:selected").text();
        var start = $("#id_start_zoom_course").val();
        var end = $("#id_end_zoom_course").val();
    }
    else if (report == "shvp") {
        var enterprise = $("#id_enterprise_shvp").val();
        var start = $("#id_start_shvp").val();
        var end = $("#id_end_shvp").val();
    }
    else if (report == "shvp_new") {
        var enterprise = $("#id_enterprise_shvp_new").val();
        var start = $("#id_start_shvp_new").val();
        var end = $("#id_end_shvp_new").val();
    }
    else if (report == "rhvp") {
        var enterprise = $("#id_enterprise_rhvp").val();
        var start = $("#id_start_rhvp").val();
        var end = $("#id_end_rhvp").val();
    }
    else if (report == "shvpName") {
        var enterprise = $("#id_enterprise_shvpN option:selected").text();
        var start = $("#id_start_shvp_corso").val();
        var end = $("#id_end_shvp_corso").val();
    }
    else if (report == "rhvpName") {
        var enterprise = $("#id_enterprise_rhvpN option:selected").text();
        var start = $("#id_start_rhvp_corso").val();
        var end = $("#id_end_rhvp_corso").val();
    }
    else if (report == "timeinName") {
        var enterprise = $("#id_enterprise_timeinN option:selected").text();
        var start = $("#id_start_timein_corso").val();
        var end = $("#id_end_timein_corso").val();
    }
    else if (report == 'videotimepro') {
        var enterprise = $("#id_enterprise_videotimepro").val();
        var start = $("#id_start_videotimepro").val();
        var end = $("#id_end_videotimepro").val();
    }
    else if (report == "shvp_course_new") {
        var enterprise = $("#id_enterprise_shvpName_new option:selected").text();
        var start = $("#id_start_shvp_course_new").val();
        var end = $("#id_end_shvp_course_new").val();
    }

    if (report == "hvp") {
        var corsoSelezionato = document.getElementById("id_course_hvp").value;
    }
    else if (report == "hvp_new") {
        var corsoSelezionato = $("#id_course_hvp_new option:selected").text();
    }
    else if (report == "summaryhvp") {
        var corsoSelezionato = $("#id_course_summaryhvp option:selected").text();
    }
    else if (report == "summaryhvp_new") {
        var corsoSelezionato = $("#id_course_summaryhvp_new option:selected").text();
    }
    else if (report == "zoom") {
        var corsoSelezionato = $("#id_course_zoom option:selected").text();
    }
    else if (report == "bbb") {
        var corsoSelezionato = document.getElementById("id_course_bbb").value;
    }
    else if (report == "zoomName") {
        var corsoSelezionato = $("#id_courses_zoom option:selected").text();
    }
    else if (report == "sbbbName") {
        corsoSelezionato = $("#id_iscritto_sbbb").val();
    }
    else if (report == "shvpName") {
        corsoSelezionato = $("#id_corso_shvp").val();
    }
    else if (report == "rhvpName") {
        corsoSelezionato = $("#id_corso_rhvp").val();
    }
    else if (report == "bbbName") {
        corsoSelezionato = $("#id_courses_bbb").val();
    }
    else if (report == "hvpName") {
        corsoSelezionato = $("#id_courses_hvp").val();
    }
    else if (report == "s_zoomName") {
        corsoSelezionato = $("#id_iscritto_s_zoom option:selected").text();
    }
    else if (report == "timeinName") {
        corsoSelezionato = $("#id_corso_timeinN").val();
    }
    else if (report == "videotimepro") {
        corsoSelezionato = $("#id_corso_videotimepro").val();
    }
    else if (report == "shvp_course_new") {
        corsoSelezionato = $("#id_courses_shvpName_new").val();
    }

    $.ajax({
        type: 'GET',
        url: "stub_table.php",
        data: {
            start: start,
            end: end,
            report: report,
            excel: "excel",
            enterprise: enterprise,
            course: corsoSelezionato ? corsoSelezionato : "",
            filtri: checked
        },
        dataType: 'json'
    }).done(function (data) {
        hidePleaseWait();
        if (data != "Nessun log") {
            $("#noLog").remove();
            var $a = $("<a>");
            $a.attr("href", data.file);
            $("body").append($a);
            $a.attr("download", "report_" + report + "_" + enterprise + "_" + start + "_" + end + ".xlsx");
            $a[0].click();
            $a.remove();
        } else {
            $("#noLog").html("");
            $("#report_head").html("");
            $("#report_body").html("");
            $("#nav").html("");
            $("#nav").after('<p id="noLog" style="display: flex;justify-content:center;">Nessun log trovato!</p>');
        }
    });
    return;
}






function getAziende(report) {
    showPleaseWait("Caricamento...");
    $.ajax({
        method: 'POST',
        url: "getAziende.php",
        data: {}
    })
            .done(function (resultParsed) {
                var array = JSON.parse(resultParsed);
                if (report == "total") {
                    $('#id_enterprise_total').select2({
                        data: array
                    });
                }
                else if (report == "zoom") {
                    $('#id_enterprise_zoom').select2({
                        data: array
                    });
                    azienda = $('#id_enterprise_zoom').val();
                    getCorsi(report, azienda);
                }
                else if (report == "s_zoom") {
                    $('#id_enterprise_s_zoom').select2({
                        data: array
                    });
                }
                else if (report == "hvp"){
                    $('#id_enterprise_hvp').select2({
                        data: array
                    });
                    azienda = $('#id_enterprise_hvp').val();
                    getCorsi(report, azienda);
                }
                else if (report == "hvp_new"){
                    $('#id_enterprise_hvp_new').select2({
                        data: array
                    });
                    azienda = $('#id_enterprise_hvp_new').val();
                    getCorsi('hvp', azienda);
                }
                else if (report == "shvp_new"){
                    $('#id_enterprise_shvp_new').select2({
                        data: array
                    });
                    azienda = $('#id_enterprise_shvp_new').val();
                    getCorsi('shvp', azienda);
                }
                else if (report == "summaryhvp_new"){
                    $('#id_enterprise_summaryhvp_new').select2({
                        data: array
                    });
                    azienda = $('#id_enterprise_summaryhvp_new').val();
                    getCorsi('summaryhvp', azienda);
                }
                else if (report == "summaryhvp") {
                    $('#id_enterprise_summaryhvp').select2({
                        data: array
                    });
                    azienda = $('#id_enterprise_summaryhvp').val();
                    getCorsi(report, azienda);
                } else if (report == "bbb") {
                    $('#id_enterprise_bbb').select2({
                        data: array
                    });
                    azienda = $('#id_enterprise_bbb').val();
                    getCorsi(report, azienda);
                } else if (report == "sintesi") {
                    $('#id_enterprise_sintesi').select2({
                        data: array
                    });
                } else if (report == "timein") {
                    $('#id_enterprise_timein').select2({
                        data: array
                    });
                } else if (report == "shvp") {
                    $('#id_enterprise_shvp').select2({
                        data: array
                    });
                } else if (report == "rhvp") {
                    $('#id_enterprise_rhvp').select2({
                        data: array
                    });
                } else if (report == "sbbb") {
                    $('#id_enterprise_sbbb').select2({
                        data: array
                    });
                } else if (report == "videotimepro") {
                    $('#id_enterprise_videotimepro').select2({
                        data: array
                    });
                    azienda = $('#id_enterprise_videotimepro').val();
                    getCorsi(report, azienda);
                }
                hidePleaseWait();
            })
            .fail(function (jqXHR, exception) {
                var msg = '';
                if (jqXHR.status === 0) {
                    msg = 'Not connect.\n Verify Network.';
                } else if (jqXHR.status == 404) {
                    msg = 'Requested page not found. [404]';
                } else if (jqXHR.status == 500) {
                    msg = 'Internal Server Error [500].';
                } else if (exception === 'parsererror') {
                    msg = 'Requested JSON parse failed.';
                } else if (exception === 'timeout') {
                    msg = 'Time out error.';
                } else if (exception === 'abort') {
                    msg = 'Ajax request aborted.';
                } else {
                    msg = 'Uncaught Error.\n' + jqXHR.responseText;
                }
                console.log(msg);
            });
}

function getIscritti() {
    $.ajax({
        method: 'POST',
        url: "getNameUtenti.php",
        data: {}
    })
            .done(function (resultParsed) {
                var array = JSON.parse(resultParsed);
                $('#id_iscritto_total').select2({
                    data: array
                });
                $('#id_iscritto_sintesi').select2({
                    data: array
                });
            })
            .fail(function (jqXHR, exception) {
                var msg = '';
                if (jqXHR.status === 0) {
                    msg = 'Not connect.\n Verify Network.';
                } else if (jqXHR.status == 404) {
                    msg = 'Requested page not found. [404]';
                } else if (jqXHR.status == 500) {
                    msg = 'Internal Server Error [500].';
                } else if (exception === 'parsererror') {
                    msg = 'Requested JSON parse failed.';
                } else if (exception === 'timeout') {
                    msg = 'Time out error.';
                } else if (exception === 'abort') {
                    msg = 'Ajax request aborted.';
                } else {
                    msg = 'Uncaught Error.\n' + jqXHR.responseText;
                }
                console.log(msg);
            });
}

function getCorsiByAzienda(nomeAzienda) {

    $.ajax({
        method: 'POST',
        url: "table.php",
        data: {
            nome: nomeAzienda
        }
    })
            .done(function (resultParsed) {
                hidePleaseWait();
                var tmp = [];
                var array = JSON.parse(resultParsed);
                //console.log(array);
                for (const [key, value] of Object.entries(array)) {
                    var input = {"id": key, "text": value};
                    tmp.push(input);
                }

                $("#id_course_bbb").select2({
                    data: tmp
                });
                /*var select = document.getElementById("id_course_bbb");
                 var fragment = document.createDocumentFragment();
                 console.log(array);
                 for (const [key, value] of Object.entries(array)) {
                 var opt = document.createElement('option');
                 opt.innerHTML = value;
                 opt.value = key;
                 fragment.appendChild(opt);
                 }

                 select.appendChild(fragment);*/
            })
            .fail(function (jqXHR, exception) {
                var msg = '';
                if (jqXHR.status === 0) {
                    msg = 'Not connect.\n Verify Network.';
                } else if (jqXHR.status == 404) {
                    msg = 'Requested page not found. [404]';
                } else if (jqXHR.status == 500) {
                    msg = 'Internal Server Error [500].';
                } else if (exception === 'parsererror') {
                    msg = 'Requested JSON parse failed.';
                } else if (exception === 'timeout') {
                    msg = 'Time out error.';
                } else if (exception === 'abort') {
                    msg = 'Ajax request aborted.';
                } else {
                    msg = 'Uncaught Error.\n' + jqXHR.responseText;
                }
                console.log(msg);
            });
}



function getCorsi(report, azienda) {
    showPleaseWait("Caricamento");
    if (azienda == null)
        azienda = $('#id_enterprise_' + report).val();
    $("#id_course_" + report).empty().trigger('change');
    $('#id_course_' + report).append($('<option>', {
        value: 0,
        text: 'Tutti'
    }));
    $.ajax({
        method: 'POST',
        url: "table.php",
        data: {
            azienda: azienda
        }
    })

            .done(function (resultParsed) {
                hidePleaseWait();
                var array = JSON.parse(resultParsed);
                if (report == "bbb") {
                    var tmp = [];
                    for (const [key, value] of Object.entries(array)) {
                        var input = {"id": key, "text": value};
                        tmp.push(input);
                    }
                    $("#id_course_bbb").select2({
                        data: tmp
                    });

                }
                else if (report == "bbbName") {
                    var tmp = [];
                    for (const [key, value] of Object.entries(array)) {
                        var input = {"id": key, "text": value};
                        tmp.push(input);
                    }
                    $("#id_courses_bbb").select2({
                        data: tmp
                    });
                    let idCorso = $("#id_courses_bbb").val();
                    getAziendeAssociate(idCorso, "bbbName");
                }
                else if (report == "add_bbbName") {
                    var tmp = [];
                    for (const [key, value] of Object.entries(array)) {
                        var input = {"id": key, "text": value};
                        tmp.push(input);
                    }
                    $("#corso--modal--bb--course").select2({
                        dropdownParent: $("#modal--bb--course"),
                        data: tmp
                    });
                }
                else if (report == "hvp") {

                    var tmp = [];
                    for (const [key, value] of Object.entries(array)) {
                        var input = {"id": key, "text": value};
                        tmp.push(input);
                    }
                    $("#id_course_hvp").select2({
                        data: tmp
                    });
                }
                else if (report == "hvp_new") {

                    var tmp = [];
                    for (const [key, value] of Object.entries(array)) {
                        var input = {"id": key, "text": value};
                        tmp.push(input);
                    }
                    $("#id_course_hvp_new").select2({
                        data: tmp
                    });
                }
                else if (report == "summaryhvp") {

                    var tmp = [];
                    for (const [key, value] of Object.entries(array)) {
                        var input = {"id": key, "text": value};
                        tmp.push(input);
                    }
                    $("#id_course_summaryhvp").select2({
                        data: tmp
                    });
                }
                else if (report == "hvpName") {
                    var tmp = [];
                    for (const [key, value] of Object.entries(array)) {
                        var input = {"id": key, "text": value};
                        tmp.push(input);
                    }
                    $("#id_courses_hvp").select2({
                        data: tmp
                    });
                    let idCorso = $("#id_courses_hvp").val();
                    getAziendeAssociate(idCorso, "hvpName");
                }
                else if (report == "zoom") {
                    var tmp = [];
                    for (const [key, value] of Object.entries(array)) {
                        var input = {"id": key, "text": value};
                        tmp.push(input);
                    }
                    $("#id_course_zoom").select2({
                        data: tmp
                    });
                }
                else if (report == "zoomName") {
                    var tmp = [];
                    for (const [key, value] of Object.entries(array)) {
                        var input = {"id": key, "text": value};
                        tmp.push(input);
                    }
                    $("#id_courses_zoom").select2({
                        data: tmp
                    });
                    let idCorso = $("#id_courses_zoom").val();
                    getAziendeAssociate(idCorso, "zoomName");
                }
                else if (report == "sbbbName") {
                    var tmp = [];
                    for (const [key, value] of Object.entries(array)) {
                        var input = {"id": key, "text": value};
                        tmp.push(input);
                    }
                    $("#id_iscritto_sbbb").select2({
                        data: tmp
                    });
                    let idCorso = $("#id_iscritto_sbbb").val();
                    getAziendeAssociate(idCorso, "sbbbN");
                }
                else if (report == "s_zoomName") {
                    var tmp = [];
                    for (const [key, value] of Object.entries(array)) {
                        var input = {"id": key, "text": value};
                        tmp.push(input);
                    }
                    $("#id_iscritto_s_zoom").select2({
                        data: tmp
                    });
                    let idCorso = $("#id_iscritto_s_zoom").val();
                    getAziendeAssociate(idCorso, "s_zoomName");
                }
                else if (report == "shvpName") {
                    var tmp = [];
                    for (const [key, value] of Object.entries(array)) {
                        var input = {"id": key, "text": value};
                        tmp.push(input);
                    }
                    $("#id_corso_shvp").select2({
                        data: tmp
                    });
                    let idCorso = $("#id_corso_shvp").val();
                    getAziendeAssociate(idCorso, "shvpN");
                }
                else if (report == "shvpName_new") {
                    var tmp = [];
                    for (const [key, value] of Object.entries(array)) {
                        var input = {"id": key, "text": value};
                        tmp.push(input);
                    }
                    $("#id_courses_shvpName_new").select2({
                        data: tmp
                    });
                    let idCorso = $("#id_courses_shvpName_new").val();
                    getAziendeAssociate(idCorso, "shvpName_new");
                }
                else if (report == "rhvpName") {
                    var tmp = [];
                    for (const [key, value] of Object.entries(array)) {
                        var input = {"id": key, "text": value};
                        tmp.push(input);
                    }
                    $("#id_corso_rhvp").select2({
                        data: tmp
                    });
                    let idCorso = $("#id_corso_rhvp").val();
                    getAziendeAssociate(idCorso, "rhvpN");
                }else if (report == "timeinN") {
                    var tmp = [];

                    for (const [key, value] of Object.entries(array)) {
                        var input = {"id": key, "text": value.trim()};
                        tmp.push(input);
                    }
                    $("#id_corso_timeinN").select2({
                        data: tmp
                    });
                    let idCorso = $("#id_corso_timeinN").val();
                    getAziendeAssociate(idCorso, "timeinN");
                } else if (report == "videotimepro") {

                    var tmp = [];
                    for (const [key, value] of Object.entries(array)) {
                        var input = {"id": key, "text": value};
                        tmp.push(input);
                    }
                    $("#id_course_videotimepro").select2({
                        data: tmp
                    });
                }
            })
            .fail(function (jqXHR, exception) {
                var msg = '';
                if (jqXHR.status === 0) {
                    msg = 'Not connect.\n Verify Network.';
                } else if (jqXHR.status == 404) {
                    msg = 'Requested page not found. [404]';
                } else if (jqXHR.status == 500) {
                    msg = 'Internal Server Error [500].';
                } else if (exception === 'parsererror') {
                    msg = 'Requested JSON parse failed.';
                } else if (exception === 'timeout') {
                    msg = 'Time out error.';
                } else if (exception === 'abort') {
                    msg = 'Ajax request aborted.';
                } else {
                    msg = 'Uncaught Error.\n' + jqXHR.responseText;
                }
                console.log(msg);
            });
}

var oldValue = "";
function updateField(id, idTime) {
    var value = $('#' + id).html();
    var maxTime = $('#' + idTime).html();
    var compare = 0;
    var message = "";
    if (id > idTime) { //sto modificando prima logout
        if (oldValue == maxTime) {
            return;
        }
        compare = maxTime.localeCompare(value);
        message = "L'orario di logout deve essere maggiore di quello del login!";
    } else {
        if (oldValue == value) {
            return;
        }
        compare = value.localeCompare(maxTime);
        message = "L'orario di login deve essere minore di quello del logout!";
    }
    if (compare > 0) {
        alert(message);
        return;
    }
    showPleaseWait("Aggiornamento campo in corso...");
    $.ajax({
        method: 'POST',
        url: "updateLog.php",
        data: {
            param: value,
            idLog: id
        }
    })
            .done(function () {
                hidePleaseWait();
            })
            .fail(function (jqXHR, exception) {
                var msg = '';
                if (jqXHR.status === 0) {
                    msg = 'Not connect.\n Verify Network.';
                } else if (jqXHR.status == 404) {
                    msg = 'Requested page not found. [404]';
                } else if (jqXHR.status == 500) {
                    msg = 'Internal Server Error [500].';
                } else if (exception === 'parsererror') {
                    msg = 'Requested JSON parse failed.';
                } else if (exception === 'timeout') {
                    msg = 'Time out error.';
                } else if (exception === 'abort') {
                    msg = 'Ajax request aborted.';
                } else {
                    msg = 'Uncaught Error.\n' + jqXHR.responseText;
                }
                console.log(msg);
            });
}


function initialize() {
    idPiattaforma = idPiattaforma.split(",");
    $('#selectPlatform option').each(function () {
        if ($(this).val() != '-1' && !idPiattaforma.includes($(this).val())) {
            $(this).remove();
        }
    });
    $('#header-report option').each(function () {
        if ($(this).val() != '-1' && !idPiattaforma.includes($(this).val())) {
            $(this).remove();
        }
    });
    if (idRuolo != 0) {
        $('#addLog_sintesi').hide();
        $('#addLog_bb').hide();
        $('#addLog_bb_course').hide();
    }
    if (Cookies.get("FIRST") == 0 || Cookies.get("FIRST") == 1) {
        $('#v-pills-bbb-tab').hide();
        $('#v-pills-sbbb-tab').hide();
        $('#v-pills-zoom-tab').hide();
        $('#v-pills-s_zoom-tab').hide();
    } else {
        $('#v-pills-bbb-tab').show();
        $('#v-pills-sbbb-tab').show();
        $('#v-pills-zoom-tab').show();
        $('#v-pills-s_zoom-tab').show();
    }

}

function saveValue(id) {
    oldValue = $('#' + id).html();
}

/*function filter(className) {
 var check = document.getElementById(className + "Filter");
 var column = document.getElementsByClassName(className);
 if (check.checked) {
 for (var i = 0; i < column.length; i++) {
 column[i].style.display = "none";
 }
 } else {
 for (var i = 0; i < column.length; i++) {
 column[i].style.display = "";
 }
 }
 }*/

/*function uncheckFilter() {
 var column = document.getElementsByClassName("checkbox");
 for (var i = 0; i < column.length; i++) {
 column[i].checked = false;
 }

 }*/

function getAule(report) {
    $.ajax({
        method: 'POST',
        url: "table.php",
        data: {
            aule: "aule"
        }
    })
            .done(function (resultParsed) {

                var array = JSON.parse(resultParsed);
                if (report == "bbb") {
                    var tmp = [];
                    for (const [key, value] of Object.entries(array)) {
                        var input = {"id": key, "text": value};
                        tmp.push(input);
                    }
                    $("#id_course_bbb").select2({
                        data: tmp
                    });
                } else if (report == "bbbName") {
                    var tmp = [];
                    for (const [key, value] of Object.entries(array)) {
                        var input = {"id": key, "text": value};
                        tmp.push(input);
                    }
                    $("#aula--modal--bb--course").select2({
                        dropdownParent: $("#modal--bb--course"),
                        data: tmp
                    });
                }
            })
            .fail(function (jqXHR, exception) {
                var msg = '';
                if (jqXHR.status === 0) {
                    msg = 'Not connect.\n Verify Network.';
                } else if (jqXHR.status == 404) {
                    msg = 'Requested page not found. [404]';
                } else if (jqXHR.status == 500) {
                    msg = 'Internal Server Error [500].';
                } else if (exception === 'parsererror') {
                    msg = 'Requested JSON parse failed.';
                } else if (exception === 'timeout') {
                    msg = 'Time out error.';
                } else if (exception === 'abort') {
                    msg = 'Ajax request aborted.';
                } else {
                    msg = 'Uncaught Error.\n' + jqXHR.responseText;
                }
                console.log(msg);
            });
}



function getNomi(report) {
    showPleaseWait("Caricamento");
    $.ajax({
        method: 'POST',
        url: "table.php",
        data: {
            iscritti: "iscritti"
        }
    })

            .done(function (resultParsed) {
                hidePleaseWait();
                var array = JSON.parse(resultParsed);
                if (report == "bbbName") {
                    var tmp = [];
                    for (const [key, value] of Object.entries(array)) {
                        var input = {"id": key, "text": value};
                        tmp.push(input);
                    }
                    $("#name--modal--bb--course").select2({
                        dropdownParent: $("#modal--bb--course"),
                        data: tmp
                    });
                } else if (report == "sintesi") {
                    var tmp = [];
                    for (const [key, value] of Object.entries(array)) {
                        var input = {"id": key, "text": value};
                        tmp.push(input);
                    }
                    $("#name--modal--sintesi").select2({
                        dropdownParent: $("#modal--sintesi"),
                        data: tmp
                    });
                } else if (report == "totalName") {
                    var tmp = [];
                    for (const [key, value] of Object.entries(array)) {
                        var input = {"id": key, "text": value};
                        tmp.push(input);
                    }
                    $('#id_iscritto_total').select2({

                        language: {
                            inputTooShort: function (args) {

                                return "Digita almeno 2 caratteri...";
                            },
                            noResults: function () {
                                return "Non trovato.";
                            },
                            searching: function () {
                                return "Searching...";
                            }
                        },
                        minimumInputLength: 2,
                        data: tmp
                    });
                } else if (report == "sintesiName") {
                    var tmp = [];
                    for (const [key, value] of Object.entries(array)) {
                        var input = {"id": key, "text": value};
                        tmp.push(input);
                    }
                    $('#id_iscritto_sintesi').select2({
                        language: {
                            inputTooShort: function (args) {

                                return "Digita almeno 2 caratteri...";
                            },
                            noResults: function () {
                                return "Non trovato.";
                            },
                            searching: function () {
                                return "Searching...";
                            }
                        },
                        minimumInputLength: 2,
                        data: tmp
                    });
                } else if (report == "sbbbName") {
                    var tmp = [];
                    for (const [key, value] of Object.entries(array)) {
                        var input = {"id": key, "text": value};
                        tmp.push(input);
                    }
                    $('#id_iscritto_sbbb').select2({
                        language: {
                            inputTooShort: function (args) {

                                return "Digita almeno 2 caratteri...";
                            },
                            noResults: function () {
                                return "Non trovato.";
                            },
                            searching: function () {
                                return "Searching...";
                            }
                        },
                        minimumInputLength: 2,
                        data: tmp
                    });
                } else if (report == "s_zoomName") {
                    var tmp = [];
                    for (const [key, value] of Object.entries(array)) {
                        var input = {"id": key, "text": value};
                        tmp.push(input);
                    }
                    $('#id_iscritto_s_zoom').select2({
                        language: {
                            inputTooShort: function (args) {

                                return "Digita almeno 2 caratteri...";
                            },
                            noResults: function () {
                                return "Non trovato.";
                            },
                            searching: function () {
                                return "Searching...";
                            }
                        },
                        minimumInputLength: 2,
                        data: tmp
                    });
                }
            })
            .fail(function (jqXHR, exception) {
                var msg = '';
                if (jqXHR.status === 0) {
                    msg = 'Not connect.\n Verify Network.';
                } else if (jqXHR.status == 404) {
                    msg = 'Requested page not found. [404]';
                } else if (jqXHR.status == 500) {
                    msg = 'Internal Server Error [500].';
                } else if (exception === 'parsererror') {
                    msg = 'Requested JSON parse failed.';
                } else if (exception === 'timeout') {
                    msg = 'Time out error.';
                } else if (exception === 'abort') {
                    msg = 'Ajax request aborted.';
                } else {
                    msg = 'Uncaught Error.\n' + jqXHR.responseText;
                }
                console.log(msg);
            });
}

function addLog(report) {

    if (report == "bbbName") {
        var id = "bb--course";
    } else if (report == "sintesi") {
        var id = "sintesi";
    }

//controlli

    if ($('#action--modal--' + id).val() == "seleziona") {
        alert("Seleziona azione");
        return false;
    }

    showPleaseWait("Aggiunta log in corso...");
    var data = $('#data--modal--' + id).val();
    var userid = $('#name--modal--' + id).val();
    var idCorso = $('#corso--modal--' + id).val();
    var idAula = $('#aula--modal--' + id).val();
    var azione = $('#action--modal--' + id).val();
    var orario = $('#orario--modal--' + id).val();
    input = {};
    if (report == 'bbbName') {
        input = {
            data: data,
            userid: userid,
            idCorso: idCorso,
            idAula: idAula,
            azione: azione,
            orario: orario,
            report: report
        };
    } else {
        input = {
            data: data,
            userid: userid,
            azione: azione,
            orario: orario,
            report: report
        };
    }

//aggiunta
    $.ajax({
        method: 'POST',
        url: "addLog.php",
        data: input
    })
            .done(function (resultParsed) {
                hidePleaseWait();
            })
            .fail(function (jqXHR, exception) {
                var msg = '';
                if (jqXHR.status === 0) {
                    msg = 'Not connect.\n Verify Network.';
                } else if (jqXHR.status == 404) {
                    msg = 'Requested page not found. [404]';
                } else if (jqXHR.status == 500) {
                    msg = 'Internal Server Error [500].';
                } else if (exception === 'parsererror') {
                    msg = 'Requested JSON parse failed.';
                } else if (exception === 'timeout') {
                    msg = 'Time out error.';
                } else if (exception === 'abort') {
                    msg = 'Ajax request aborted.';
                } else {
                    msg = 'Uncaught Error.\n' + jqXHR.responseText;
                }
                console.log(msg);
            });
    return true;
}

function closeModal(report) {
    if (report == 'bbbName') {
        var id = "bb--course";
    } else if (report == "sintesi") {
        var id = "sintesi";
    }

    $('#modal--' + id).modal('hide');
}

function cleanDataTable() {
    $('#report').DataTable().clear();
    $('#report').DataTable().destroy();
    $('#report tbody').empty();
    $('#report thead').empty();
}

function createDataTable() {
    $('#report').DataTable({
        dom: "Bfrtip",
        stateSave: true,
        buttons: [
            {
                extend: 'colvis',
                text: 'Colonne da non visualizzare',
                collectionLayout: 'fixed two-column'
                        //columns:':lt(9)'
            }
        ],
        "language": {
            "lengthMenu": "Mostra _MENU_ righe per pagina",
            "zeroRecords": "Nessun elemento trovato",
            "info": "Mostrando pagina _PAGE_ di _PAGES_ su _TOTAL_ righe",
            "infoEmpty": "Nessuna riga disponibile",
            "search": "Cerca",
            "paginate": {
                "previous": "Precedente",
                "next": "Successivo"
            }
        },
        "lengthMenu": [[50, 150, 300], [50, 150, 300]],
        autoWidth: false,
        scrollY: 300,
        scrollX: 300,
        "order": [[3, "asc"]]
    });
}

function getAziendeAssociate(idCorso, report) {
    console.log("Aziende associate per corso " + idCorso + " (Report: " + report + ")");
    $("#id_enterprise_" + report).empty().trigger('change');
    $('#id_enterprise_' + report).append($('<option>', {
        value: 0,
        text: 'Tutti'
    }));
    $.ajax({
        method: 'POST',
        url: "table.php",
        data: {
            idCorso: idCorso,
            report : report,
            platf: "sessione"
        }
    })

            .done(function (resultParsed) {
                hidePleaseWait();
                var array = JSON.parse(resultParsed);
                var tmp = [];
                for (const [key, value] of Object.entries(array)) {
                    if (key != "" && value != null) {
                        var input = {"id": key, "text": value};
                        tmp.push(input);
                    }
                }

                $("#id_enterprise_" + report).select2({
                    data: tmp
                });


            })
            .fail(function (jqXHR, exception) {
                var msg = '';
                if (jqXHR.status === 0) {
                    msg = 'Not connect.\n Verify Network.';
                } else if (jqXHR.status == 404) {
                    msg = 'Requested page not found. [404]';
                } else if (jqXHR.status == 500) {
                    msg = 'Internal Server Error [500].';
                } else if (exception === 'parsererror') {
                    msg = 'Requested JSON parse failed.';
                } else if (exception === 'timeout') {
                    msg = 'Time out error.';
                } else if (exception === 'abort') {
                    msg = 'Ajax request aborted.';
                } else {
                    msg = 'Uncaught Error.\n' + jqXHR.responseText;
                }
                console.log(msg);
            });
}
