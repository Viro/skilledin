function addCourseToPianoAzienda(piano_azienda_id, platform_id, course_id){
    var data = "action=addCourseToPianoAzienda&piano_azienda_id=" + encodeURIComponent(piano_azienda_id) + '&course_id=' + course_id + '&platform_id=' + platform_id;
    console.log(data);
    $.ajax({
        url: 'https://update.corsinrete.com/skilledin/report/response/company/company.php',
        data: data,
        type: 'POST',
        success: function(html) {
            console.log(html);
            reloadPianoAziendaCorsiAssociati(piano_azienda_id);
            reloadPianoAziendaCorsi(piano_azienda_id);
        }
    });
}

function deleteCourseToPianoAzienda(platform_id, skilledin_plan_id, course_id){
    if(confirm("Sicuro?")){
        var data = "action=deleteCourseToPianoAzienda&platform_id=" + platform_id + "&skilledin_plan_id=" + skilledin_plan_id + "&course_id=" + course_id;
        console.log(data);
        $.ajax({
            url: 'https://update.corsinrete.com/skilledin/report/response/company/company.php',
            data: data,
            type: 'POST',
            success: function(html) {
                console.log(html);
                reloadPianoAziendaCorsiAssociati(skilledin_plan_id);
                reloadPianoAziendaCorsi(skilledin_plan_id);
            }
        });
    }

}
function reloadPianoAziendaCorsi(piano_azienda_id, piattaforma_id){
    var data = 'action=getHtmlSelectCourses&platform_id=' + piattaforma_id + '&piano_formativo_id=' + piano_azienda_id;
    $("#select_course_id").val(null);
    $.ajax({
        url: 'https://update.corsinrete.com/skilledin/report/response/course/course.php',
        data: data,
        type: 'POST',
        dataType: 'json',
        success: function(course_modules) {
            $("#select_course_id").select2({
                placeholder: "Seleziona corso...",
                data:course_modules,
            });
        }
    });
}

function reloadPianoAziendaCorsiAssociati(skilledin_plan_id){
    var data = "action=reloadPianoAziendaCorsiAssociati&skilledin_plan_id=" + skilledin_plan_id;
    $.ajax({
        url: 'https://update.corsinrete.com/skilledin/report/response/company/company.php',
        data: data,
        type: 'POST',
        success: function(html) {
            $("#company_plan_course_list").html(html);
        }
    });
}



function realoadCompaniesFromAggregation(){
    var data = "action=getHtmlSelectCompaniesAggregated";
    console.log(data);
    $.ajax({
        url: 'https://update.corsinrete.com/skilledin/report/response/company/company.php',
        data: data,
        type: 'POST',
        dataType: 'json',
        success: function(course_modules) {
            console.log(course_modules);
            $("#select_company_name_create").select2({
                placeholder: "Seleziona azienda...",
                data:course_modules,
            });
            $("#select_company_name_search").select2({
                placeholder: "Seleziona azienda...",
                data:course_modules,
            });
        }
    });
}
