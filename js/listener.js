

window.onload = function () {



    initialize();

    /**
     * Modali
     */
    $(document).ready(function () {
        if (Cookies.get("FIRST") == null) {
            $('#exampleModal').modal({backdrop: 'static', keyboard: false});
            $('#exampleModal').modal('show');

        } else {
            $('#card-header').show();
            $('#card-body').show();
            $('#header-report').val(Cookies.get("FIRST")).attr("selected", true);
            getAziende("total");
        }

    });

    document.getElementById("addLog_bb_course").onclick = function () {
        $('#modal--bb--course').modal('show');
        getCorsi("add_bbbName", "");
        getAule("bbbName");
        getNomi("bbbName");

    };

    document.getElementById("addLog_bb").onclick = function () {
        $('#modal--bb--course').modal('show');
        getCorsi("add_bbbName", "");
        getAule("bbbName");
        getNomi("bbbName");

    };

    document.getElementById("addLog_sintesi").onclick = function () {
        $('#modal--sintesi').modal('show');
        getNomi("sintesi");

    };

    document.getElementById("addLog_sintesi_iscritto").onclick = function () {
        $('#modal--sintesi').modal('show');
        getNomi("sintesi");

    };


    /**
     * Fine Modali
     */

    document.getElementById("selectPlatform").onchange = function () {
        $('#exampleModal').modal('hide');
        $('#card-header').show();
        $('#card-body').show();

        var selezionato = $('#selectPlatform option:selected').text();
        $.ajax({
            method: 'POST',
            url: "setPiattaforma.php",
            data: {
                platform: selezionato
            }
        })
                .done(function (resultParsed) {
                    var id = $('#selectPlatform option:selected').val();

                    $('#header-report').val(id).attr("selected", true);

                    switch (selezionato) {
                        case "Fondo Nuove Competenze":
                            $('#v-pills-bbb-tab').hide();
                            $('#v-pills-sbbb-tab').hide();
                            $('#v-pills-zoom-tab').hide();
                            $('#v-pills-s_zoom-tab').hide();
                            break;
                        case "Formazione 4.0":
                            $('#v-pills-bbb-tab').hide();
                            $('#v-pills-sbbb-tab').hide();
                            $('#v-pills-zoom-tab').hide();
                            $('#v-pills-s_zoom-tab').hide();
                            break;
                        default:
                            break;
                    }
                    getAziende("total");
                })
                .fail(function (jqXHR, exception) {
                    var msg = '';
                    if (jqXHR.status === 0) {
                        msg = 'Not connect.\n Verify Network.';
                    } else if (jqXHR.status == 404) {
                        msg = 'Requested page not found. [404]';
                    } else if (jqXHR.status == 500) {
                        msg = 'Internal Server Error [500].';
                    } else if (exception === 'parsererror') {
                        msg = 'Requested JSON parse failed.';
                    } else if (exception === 'timeout') {
                        msg = 'Time out error.';
                    } else if (exception === 'abort') {
                        msg = 'Ajax request aborted.';
                    } else {
                        msg = 'Uncaught Error.\n' + jqXHR.responseText;
                    }
                    console.log(msg);
                });

    };


    document.getElementById("header-report").onchange = function () {
        var selezionato = $('#header-report option:selected').text();


        $.ajax({
            method: 'POST',
            url: "setPiattaforma.php",
            data: {
                platform: selezionato
            }
        })
                .done(function (resultParsed) {
                    switch (selezionato) {
                        case "Fondo Nuove Competenze":
                            $('#v-pills-bbb-tab').hide();
                            $('#v-pills-sbbb-tab').hide();
                            $('#v-pills-zoom-tab').hide();
                            $('#v-pills-s_zoom-tab').hide();
                            $('#v-pills-videotimepro-tab').hide();
                            break;
                        case "Formazione 4.0":
                            $('#v-pills-bbb-tab').hide();
                            $('#v-pills-sbbb-tab').hide();
                            $('#v-pills-zoom-tab').hide();
                            $('#v-pills-s_zoom-tab').hide();
                            $('#v-pills-videotimepro-tab').hide();
                            break;
                        case "New":
                            $('#v-pills-bbb-tab').show();
                            $('#v-pills-sbbb-tab').show();
                            $('#v-pills-hvp-tab').show();
                            $('#v-pills-shvp-tab').show();
                            $('#v-pills-zoom-tab').show();
                            $('#v-pills-s_zoom-tab').show();
                            $('#v-pills-videotimepro-tab').hide();
                            break;
                        case "Live":
                            $('#v-pills-bbb-tab').show();
                            $('#v-pills-sbbb-tab').show();
                            $('#v-pills-hvp-tab').show();
                            $('#v-pills-shvp-tab').show();
                            $('#v-pills-zoom-tab').show();
                            $('#v-pills-s_zoom-tab').show();
                            $('#v-pills-videotimepro-tab').hide();

                            break;
                       case "Formazione":
                            $('#v-pills-bbb-tab').show();
                            $('#v-pills-sbbb-tab').show();
                            $('#v-pills-hvp-tab').show();
                            $('#v-pills-shvp-tab').show();
                            $('#v-pills-zoom-tab').show();
                            $('#v-pills-s_zoom-tab').show();
                            $('#v-pills-videotimepro-tab').show();
                            break;
                        default:
                            break;
                    }

                    var id = $('#header-report option:selected').val();
                    Cookies.set("FIRST", id, {
                        sameSite: "strict"
                    });

                    location.reload();
                })
                .fail(function (jqXHR, exception) {
                    var msg = '';
                    if (jqXHR.status === 0) {
                        msg = 'Not connect.\n Verify Network.';
                    } else if (jqXHR.status == 404) {
                        msg = 'Requested page not found. [404]';
                    } else if (jqXHR.status == 500) {
                        msg = 'Internal Server Error [500].';
                    } else if (exception === 'parsererror') {
                        msg = 'Requested JSON parse failed.';
                    } else if (exception === 'timeout') {
                        msg = 'Time out error.';
                    } else if (exception === 'abort') {
                        msg = 'Ajax request aborted.';
                    } else {
                        msg = 'Uncaught Error.\n' + jqXHR.responseText;
                    }
                    console.log(msg);
                });


    };

    document.getElementById("v-pills-total-tab").onclick = function () {
        $('#report').DataTable().clear();
        $('#report_wrapper').hide();
        $('#nav').html("");
        checked = ["vuoto"];
    };
    document.getElementById("v-pills-sbbb-tab").onclick = function () {
        $('#report').DataTable().clear();
        $('#report_wrapper').hide();
        $('#nav').html("");
        getAziende("sbbb");
        checked = ["vuoto"];
    };
    document.getElementById("home-tab").onclick = function () {
        $('#report').DataTable().clear();
        $('#report_wrapper').hide();
        $('#nav').html("");
        getAziende("total");
        checked = ["vuoto"];
    };
    document.getElementById("v-pills-bbb-tab").onclick = function () {
        $('#report').DataTable().clear();
        $('#report_wrapper').hide();
        $('#nav').html("");
        getAziende("bbb");
        checked = ["vuoto"];
    };
    document.getElementById("profile-tab-bbb").onclick = function () {
        $('#report').DataTable().clear();
        $('#report_wrapper').hide();
        $('#nav').html("");
        getCorsi("bbbName", "");
    };
    document.getElementById("profile-tab-sbbb").onclick = function () {
        $('#report').DataTable().clear();
        $('#report_wrapper').hide();
        $('#nav').html("");
        getCorsi("sbbbName", "");
    };
    document.getElementById("profile-tab-zoom").onclick = function () {
        $('#report').DataTable().clear();
        $('#report_wrapper').hide();
        $('#nav').html("");
        getCorsi("zoomName", "");
    };
    document.getElementById("profile-tab-s_zoom").onclick = function () {
        $('#report').DataTable().clear();
        $('#report_wrapper').hide();
        $('#nav').html("");
        getCorsi("s_zoomName", "");
    };
    document.getElementById("profile-tab-sintesi").onclick = function () {
        $('#report').DataTable().clear();
        $('#report_wrapper').hide();
        $('#nav').html("");
        getNomi("sintesiName");
    };
    document.getElementById("profile-tab").onclick = function () {
        $('#nav').html("");
        getNomi("totalName");
        $('#report').DataTable().clear();
        $('#report_wrapper').hide();
    };
    document.getElementById("profile-tab-hvp").onclick = function () {
        $('#report').DataTable().clear();
        $('#report_wrapper').hide();
        $('#nav').html("");
        getCorsi("hvpName", "");
    };
    document.getElementById("profile-tab-shvpName").onclick = function () {
        $('#report').DataTable().clear();
        $('#report_wrapper').hide();
        $('#nav').html("");
        getCorsi("shvpName", "");
    };
    document.getElementById("profile-tab-rhvpName").onclick = function () {
        $('#report').DataTable().clear();
        $('#report_wrapper').hide();
        $('#nav').html("");
        getCorsi("rhvpName", "");
    };
    document.getElementById("profile-tab-timein").onclick = function () {
        $('#report').DataTable().clear();
        $('#report_wrapper').hide();
        $('#nav').html("");
        getCorsi("timeinN", "");
    };
    document.getElementById("profile-tab-videotimepro").onclick = function () {
        $('#report').DataTable().clear();
        $('#report_wrapper').hide();
        $('#nav').html("");
        getCorsi("videotimepro", "");
    };

    if(document.body.contains(document.getElementById("profile-tab-hvp-new"))){
        document.getElementById("profile-tab-hvp-new").onclick = function () {
            $('#report').DataTable().clear();
            $('#report_wrapper').hide();
            $('#nav').html("");
            getCorsi("hvpName_new", "");
        };
    }
    if(document.body.contains(document.getElementById("profile-tab-shvp-new"))) {
        document.getElementById("profile-tab-shvp-new").onclick = function () {
            $('#report').DataTable().clear();
            $('#report_wrapper').hide();
            $('#nav').html("");
            getCorsi("shvpName_new", "");
        };
    }
    if(document.body.contains(document.getElementById("profile-tab-summaryhvp-new"))) {
        document.getElementById("profile-tab-summaryhvp-new").onclick = function () {
                $('#report').DataTable().clear();
                $('#report_wrapper').hide();
                $('#nav').html("");
                getCorsi("hvpName", "");
        };
    }

    document.getElementById("v-pills-timein-tab").onclick = function () {
        $('#report').DataTable().clear();
        $('#report_wrapper').hide();
        $('#nav').html("");
        getAziende("timein");
        checked = ["vuoto"];
    };
    document.getElementById("v-pills-sintesi-tab").onclick = function () {
        $('#report').DataTable().clear();
        $('#report_wrapper').hide();
        $('#nav').html("");
        getAziende("sintesi");
        checked = ["vuoto"];
    };
    document.getElementById("v-pills-hvp-tab").onclick = function () {
        $('#report').DataTable().clear();
        $('#report_wrapper').hide();
        $('#nav').html("");
        getAziende("hvp");
        checked = ["vuoto"];
    };
    document.getElementById("v-pills-summaryhvp-tab").onclick = function () {
        $('#report').DataTable().clear();
        $('#report_wrapper').hide();
        $('#nav').html("");
        getAziende("summaryhvp");
        checked = ["vuoto"];
    };
    document.getElementById("v-pills-rhvp-tab").onclick = function () {
        $('#report').DataTable().clear();
        $('#report_wrapper').hide();
        $('#nav').html("");
        getAziende("rhvp");
        checked = ["vuoto"];
    };
    document.getElementById("v-pills-shvp-tab").onclick = function () {
        $('#report').DataTable().clear();
        $('#report_wrapper').hide();
        $('#nav').html("");
        getAziende("shvp");
        checked = ["vuoto"];
    };
    document.getElementById("v-pills-zoom-tab").onclick = function () {
        $('#report').DataTable().clear();
        $('#report_wrapper').hide();
        $('#nav').html("");
        getAziende("zoom");
        checked = ["vuoto"];
    };
    document.getElementById("v-pills-s_zoom-tab").onclick = function () {
        $('#report').DataTable().clear();
        $('#report_wrapper').hide();
        $('#nav').html("");
        getAziende("s_zoom");
        checked = ["vuoto"];
    };
    document.getElementById("v-pills-videotimepro-tab").onclick = function () {
        $('#report').DataTable().clear();
        $('#report_wrapper').hide();
        $('#nav').html("");
        getAziende("videotimepro");
        checked = ["vuoto"];
    };
    if(document.body.contains(document.getElementById("v-pills-hvp-new-tab"))) {
        document.getElementById("v-pills-hvp-new-tab").onclick = function () {
            $('#report').DataTable().clear();
            $('#report_wrapper').hide();
            $('#nav').html("");
            getAziende("hvp_new");
            checked = ["vuoto"];
        };
    }
    if(document.body.contains(document.getElementById("v-pills-shvp-new-tab"))) {
        document.getElementById("v-pills-shvp-new-tab").onclick = function () {
            $('#report').DataTable().clear();
            $('#report_wrapper').hide();
            $('#nav').html("");
            getAziende("shvp_new");
            checked = ["vuoto"];
        };
    }
    if(document.body.contains(document.getElementById("v-pills-summaryhvp-new-tab"))) {
        document.getElementById("v-pills-summaryhvp-new-tab").onclick = function () {
            $('#report').DataTable().clear();
            $('#report_wrapper').hide();
            $('#nav').html("");
            getAziende("summaryhvp_new");
            checked = ["vuoto"];
        };
    }
    /*
     **filtri checkbox**
     */
    /*
     document.getElementById("dataFilter").onclick = function () {
     filter("data");
     if (!$("#dataFilter").is(':checked')) {
     checked = checked.filter(e => e !== 'data');
     } else {
     checked.push("data");
     }
     };
     document.getElementById("useridFilter").onclick = function () {
     console.log(checked);
     filter("userid");
     if (!$("#useridFilter").is(':checked')) {
     checked = checked.filter(e => e !== 'userid');
     } else {
     checked.push("userid");
     }
     };

     document.getElementById("usernameFilter").onclick = function () {
     console.log(checked);
     filter("username");
     if (!$("#usernameFilter").is(':checked')) {
     checked = checked.filter(e => e !== 'username');
     } else {
     checked.push("username");
     }
     };

     document.getElementById("nameFilter").onclick = function () {
     filter("name");
     if (!$("#nameFilter").is(':checked')) {
     checked = checked.filter(e => e !== 'name');
     } else {
     checked.push("name");
     }
     };
     document.getElementById("cfFilter").onclick = function () {
     filter("cf");
     if (!$("#cfFilter").is(':checked')) {
     checked = checked.filter(e => e !== 'cf');
     } else {
     checked.push("cf");
     }
     };
     document.getElementById("emailFilter").onclick = function () {
     filter("email");
     if (!$("#emailFilter").is(':checked')) {
     checked = checked.filter(e => e !== 'email');
     } else {
     checked.push("email");
     }
     };
     document.getElementById("dnFilter").onclick = function () {
     filter("dn");
     if (!$("#dnFilter").is(':checked')) {
     checked = checked.filter(e => e !== 'dn');
     } else {
     checked.push("dn");
     }
     };
     document.getElementById("lnFilter").onclick = function () {
     filter("ln");
     if (!$("#lnFilter").is(':checked')) {
     checked = checked.filter(e => e !== 'ln');
     } else {
     checked.push("ln");
     }
     };
     document.getElementById("aziendaFilter").onclick = function () {
     filter("azienda");
     if (!$("#aziendaFilter").is(':checked')) {
     checked = checked.filter(e => e !== 'azienda');
     } else {
     checked.push("azienda");
     }
     };

     document.getElementById("phoneFilter").onclick = function () {
     filter("phone");
     if (!$("#phoneFilter").is(':checked')) {
     checked = checked.filter(e => e !== 'phone');
     } else {
     checked.push("phone");
     }
     };

     document.getElementById("attivitaFilter").onclick = function () {
     filter("attivita");
     if (!$("#attivitaFilter").is(':checked')) {
     checked = checked.filter(e => e !== 'attivita');
     } else {
     checked.push("attivita");
     }
     };

     document.getElementById("azioneFilter").onclick = function () {
     filter("azione");
     if (!$("#azioneFilter").is(':checked')) {
     checked = checked.filter(e => e !== 'azione');
     } else {
     checked.push("azione");
     }
     };

     document.getElementById("ipFilter").onclick = function () {
     filter("ip");
     if (!$("#ipFilter").is(':checked')) {
     checked = checked.filter(e => e !== 'ip');
     } else {
     checked.push("ip");
     }
     };

     document.getElementById("orarioAFilter").onclick = function () {
     filter("orarioA");
     if (!$("#orarioAFilter").is(':checked')) {
     checked = checked.filter(e => e !== 'orarioA');
     } else {
     checked.push("orarioA");
     }
     };

     document.getElementById("orarioLFilter").onclick = function () {
     filter("orarioL");
     if (!$("#orarioLFilter").is(':checked')) {
     checked = checked.filter(e => e !== 'orarioL');
     } else {
     checked.push("orarioL");
     }
     };

     document.getElementById("tcFilter").onclick = function () {
     filter("tc");
     if (!$("#tcFilter").is(':checked')) {
     checked = checked.filter(e => e !== 'tc');
     } else {
     checked.push("tc");
     }
     };

     document.getElementById("tsFilter").onclick = function () {
     filter("ts");
     if (!$("#tsFilter").is(':checked')) {
     checked = checked.filter(e => e !== 'ts');
     } else {
     checked.push("ts");
     }
     };

     document.getElementById("corsoFilter").onclick = function () {
     filter("corso");
     if (!$("#corsoFilter").is(':checked')) {
     checked = checked.filter(e => e !== 'corso');
     } else {
     checked.push("corso");
     }
     };

     document.getElementById("lezioneFilter").onclick = function () {
     filter("lezione");
     if (!$("#lezioneFilter").is(':checked')) {
     checked = checked.filter(e => e !== 'lezione');
     } else {
     checked.push("lezione");
     }
     };

     document.getElementById("ruoloFilter").onclick = function () {
     filter("ruolo");
     if (!$("#ruoloFilter").is(':checked')) {
     checked = checked.filter(e => e !== 'ruolo');
     } else {
     checked.push("ruolo");
     }
     };

     document.getElementById("aulaFilter").onclick = function () {
     filter("aula");
     if (!$("#aulaFilter").is(':checked')) {
     checked = checked.filter(e => e !== 'aula');
     } else {
     checked.push("aula");
     }
     };
     */

    /*
     **click**
     */

    //REPORT
    document.getElementById("id_submitbutton_total").onclick = function () {

        $('#attivita--container').show();
        $('#azione--container').show();
        $('#ip--container').show();
        $('#azienda--container').hide();
        $('#orarioA--container').hide();
        $('#orarioL--container').hide();
        $('#corso--container').hide();
        $('#ruolo--container').hide();
        $('#tc--container').hide();
        $('#aula--container').hide();
        $('#lezione--container').hide();
        $('#data--container').show();
        $('#ts--container').hide();
        $("#id_excel_total").prop("disabled", false);
        showPleaseWait("Generazione Reportistica...");
        getData("total");
    };
    document.getElementById("id_submitbutton_total_iscritto").onclick = function () {

        $('#attivita--container').show();
        $('#azione--container').show();
        $('#ip--container').show();
        $('#azienda--container').hide();
        $('#orarioA--container').hide();
        $('#orarioL--container').hide();
        $('#corso--container').hide();
        $('#ruolo--container').hide();
        $('#tc--container').hide();
        $('#aula--container').hide();
        $('#lezione--container').hide();
        $('#data--container').show();
        $('#ts--container').hide();
        $("#id_excel_total_iscritto").prop("disabled", false);
        showPleaseWait("Generazione Reportistica...");
        getData("totalName");
    };
    document.getElementById("id_submitbutton_sintesi_iscritto").onclick = function () {

        $('#attivita--container').hide();
        $('#azione--container').hide();
        $('#ip--container').hide();
        $('#azienda--container').show();
        $('#orarioA--container').show();
        $('#orarioA--span').html("Ora accesso");
        $('#orarioL--container').show();
        $('#orarioL--span').html("Ora uscita");
        $('#corso--container').hide();
        $('#ruolo--container').hide();
        $('#tc--container').show();
        $('#aula--container').hide();
        $('#lezione--container').hide();
        $('#data--container').show();
        $('#ts--container').hide();
        $('#azienda--container').show();

        $("#id_excel_sintesi_iscritto").prop("disabled", false);
        showPleaseWait("Generazione Reportistica...");
        getData("sintesiName");
    };
    document.getElementById("id_submitbutton_bbb_course").onclick = function () {
        $('#attivita--container').hide();
        $('#azione--container').hide();
        $('#ip--container').hide();
        $('#azienda--container').show();
        $('#orarioA--container').show();
        $('#orarioA--span').html("Ora accesso");
        $('#orarioL--container').show();
        $('#orarioL--span').html("Ora uscita");
        $('#corso--container').show();
        $('#ruolo--container').show();
        $('#tc--container').show();
        $('#aula--container').show();
        $('#lezione--container').hide();
        $('#data--container').show();
        $('#ts--container').hide();
        $('#ts--span').html("Tempo sessione");
        $('#id_excel_bbb_course').prop("disabled", false);
        $('#addLog_bb_course').prop("disabled", false);
        showPleaseWait("Generazione Reportistica...");
        getData("bbbName");
    };
    document.getElementById("id_submitbutton_bbb").onclick = function () {
        $('#attivita--container').hide();
        $('#azione--container').hide();
        $('#ip--container').hide();
        $('#azienda--container').show();
        $('#orarioA--container').show();
        $('#orarioA--span').html("Ora accesso");
        $('#orarioL--container').show();
        $('#orarioL--span').html("Ora uscita");
        $('#corso--container').show();
        $('#ruolo--container').show();
        $('#tc--container').show();
        $('#aula--container').show();
        $('#lezione--container').hide();
        $('#data--container').show();
        $('#ts--container').hide();
        $('#ts--span').html("Tempo sessione");
        $('#id_excel_bbb').prop("disabled", false);
        $('#addLog_bb').prop("disabled", false);
        showPleaseWait("Generazione Reportistica...");
        getData("bbb");
    };
    document.getElementById("id_submitbutton_sbbb").onclick = function () {
        $('#attivita--container').hide();
        $('#azione--container').hide();
        $('#ip--container').hide();
        $('#azienda--container').show();
        $('#orarioA--container').hide();
        $('#orarioA--span').html("Ora accesso");
        $('#orarioL--container').hide();
        $('#orarioL--span').html("Ora uscita");
        $('#corso--container').hide();
        $('#ruolo--container').show();
        $('#tc--container').show();
        $('#aula--container').hide();
        $('#lezione--container').hide();
        $('#data--container').hide();
        $('#ts--container').hide();
        $('#ts--span').html("Tempo sessione");
        $('#id_excel_sbbb').prop("disabled", false);
        showPleaseWait("Generazione Reportistica...");
        getData("sbbb");
    };
    document.getElementById("id_submitbutton_sbbb_iscritto").onclick = function () {
        $('#attivita--container').hide();
        $('#azione--container').hide();
        $('#ip--container').hide();
        $('#azienda--container').show();
        $('#orarioA--container').hide();
        $('#orarioA--span').html("Ora accesso");
        $('#orarioL--container').hide();
        $('#orarioL--span').html("Ora uscita");
        $('#corso--container').hide();
        $('#ruolo--container').show();
        $('#tc--container').show();
        $('#aula--container').hide();
        $('#lezione--container').hide();
        $('#data--container').hide();
        $('#ts--container').hide();
        $('#ts--span').html("Tempo sessione");
        $('#id_excel_sbbb_iscritto').prop("disabled", false);
        showPleaseWait("Generazione Reportistica...");
        getData("sbbbName");
    };
    document.getElementById("id_submitbutton_zoom").onclick = function () {
        $('#attivita--container').hide();
        $('#azione--container').hide();
        $('#ip--container').hide();
        $('#azienda--container').show();
        $('#orarioA--container').show();
        $('#orarioA--span').html("Ora accesso");
        $('#orarioL--container').show();
        $('#orarioL--span').html("Ora uscita");
        $('#corso--container').show();
        $('#ruolo--container').show();
        $('#tc--container').show();
        $('#aula--container').show();
        $('#lezione--container').hide();
        $('#data--container').show();
        $('#ts--container').hide();
        $('#ts--span').html("Tempo sessione");
        $('#id_excel_zoom').prop("disabled", false);
        showPleaseWait("Generazione Reportistica...");
        getData("zoom");
    };
    document.getElementById("id_submitbutton_s_zoom").onclick = function () {
        $('#attivita--container').hide();
        $('#azione--container').hide();
        $('#ip--container').hide();
        $('#azienda--container').show();
        $('#orarioA--container').hide();
        $('#orarioA--span').html("Ora accesso");
        $('#orarioL--container').hide();
        $('#orarioL--span').html("Ora uscita");
        $('#corso--container').hide();
        $('#ruolo--container').show();
        $('#tc--container').show();
        $('#aula--container').hide();
        $('#lezione--container').hide();
        $('#data--container').hide();
        $('#ts--container').hide();
        $('#ts--span').html("Tempo sessione");
        $('#id_excel_s_zoom').prop("disabled", false);
        showPleaseWait("Generazione Reportistica...");
        getData("s_zoom");
    };
    document.getElementById("id_submitbutton_s_zoom_iscritto").onclick = function () {
        $('#attivita--container').hide();
        $('#azione--container').hide();
        $('#ip--container').hide();
        $('#azienda--container').show();
        $('#orarioA--container').hide();
        $('#orarioA--span').html("Ora accesso");
        $('#orarioL--container').hide();
        $('#orarioL--span').html("Ora uscita");
        $('#corso--container').hide();
        $('#ruolo--container').show();
        $('#tc--container').show();
        $('#aula--container').hide();
        $('#lezione--container').hide();
        $('#data--container').hide();
        $('#ts--container').hide();
        $('#ts--span').html("Tempo sessione");
        $('#id_excel_s_zoom_iscritto').prop("disabled", false);
        showPleaseWait("Generazione Reportistica...");
        getData("s_zoomName");
    };
    document.getElementById("id_submitbutton_sintesi").onclick = function () {
        $('#attivita--container').hide();
        $('#azione--container').hide();
        $('#ip--container').hide();
        $('#azienda--container').show();
        $('#orarioA--container').show();
        $('#orarioA--span').html("Ora accesso");
        $('#orarioL--container').show();
        $('#orarioL--span').html("Ora uscita");
        $('#corso--container').hide();
        $('#ruolo--container').hide();
        $('#tc--container').show();
        $('#aula--container').hide();
        $('#lezione--container').hide();
        $('#data--container').show();
        $('#ts--container').hide();
        $('#azienda--container').show();
        $("#id_excel_sintesi").prop("disabled", false);
        $('#addLog_sintesi').prop("disabled", false);

        showPleaseWait("Generazione Reportistica...");
        getData("sintesi");
    };
    document.getElementById("id_submitbutton_timein").onclick = function () {

        $('#attivita--container').hide();
        $('#azione--container').hide();
        $('#ip--container').hide();
        $('#azienda--container').show();
        $('#orarioA--container').hide();
        $('#orarioL--container').hide();
        $('#corso--container').hide();
        $('#ruolo--container').hide();
        $('#tc--container').show();
        $('#aula--container').hide();
        $('#lezione--container').hide();
        $('#data--container').hide();
        $('#ts--container').hide();
        $("#id_excel_timein").prop("disabled", false);
        showPleaseWait("Generazione Reportistica...");
        getData("timein");
    };
    document.getElementById("id_submitbutton_hvp").onclick = function () {

        $('#attivita--container').hide();
        $('#azione--container').hide();
        $('#ip--container').hide();
        $('#azienda--container').show();
        $('#orarioA--container').show();
        $('#orarioA--span').html("Ora accesso");
        $('#orarioL--container').show();
        $('#orarioL--span').html("Ora uscita");
        $('#corso--container').show();
        $('#ruolo--container').hide();
        $('#tc--container').show();
        $('#aula--container').hide();
        $('#lezione--container').show();
        $('#data--container').show();
        $('#ts--container').show();
        $('#ts--span').html("Tempo sessione");
        showPleaseWait("Generazione Reportistica...");
        getData("hvp");
        $("#id_excel_hvp").prop("disabled", false);
    };
    document.getElementById("id_submitbutton_summaryhvp").onclick = function () {

        $('#attivita--container').hide();
        $('#azione--container').hide();
        $('#ip--container').hide();
        $('#azienda--container').show();
        $('#orarioA--container').show();
        $('#orarioA--span').html("Ora accesso");
        $('#orarioL--container').show();
        $('#orarioL--span').html("Ora uscita");
        $('#corso--container').show();
        $('#ruolo--container').hide();
        $('#tc--container').show();
        $('#aula--container').hide();
        $('#lezione--container').show();
        $('#data--container').show();
        $('#ts--container').show();
        $('#ts--span').html("Tempo sessione");
        showPleaseWait("Generazione Reportistica...");
        getData("summaryhvp");
        $("#id_excel_summaryhvp").prop("disabled", false);
    };
    document.getElementById("id_submitbutton_videotimepro").onclick = function () {

        $('#attivita--container').hide();
        $('#azione--container').hide();
        $('#ip--container').hide();
        $('#azienda--container').show();
        $('#orarioA--container').show();
        $('#orarioA--span').html("Ora accesso");
        $('#orarioL--container').show();
        $('#orarioL--span').html("Ora uscita");
        $('#corso--container').show();
        $('#ruolo--container').hide();
        $('#tc--container').show();
        $('#aula--container').hide();
        $('#lezione--container').show();
        $('#data--container').show();
        $('#ts--container').show();
        $('#ts--span').html("Tempo sessione");
        showPleaseWait("Generazione Reportistica...");
        getData("videotimepro");
        $("#id_excel_videotimepro").prop("disabled", false);
    };
    document.getElementById("id_submitbutton_rhvp").onclick = function () {

        $('#attivita--container').hide();
        $('#azione--container').hide();
        $('#ip--container').hide();
        $('#azienda--container').show();
        $('#orarioA--container').show(); //modifica nome
        $('#orarioA--span').html("Totale minuti video");
        $('#orarioL--container').show();
        $('#orarioL--span').html("Totale minuti corso");
        $('#corso--container').hide();
        $('#ruolo--container').hide();
        $('#tc--container').hide();
        $('#aula--container').hide();
        $('#lezione--container').hide();
        $('#data--container').hide();
        $('#ts--container').show();
        $('#ts--span').html("Differenza");
        showPleaseWait("Generazione Reportistica...");
        getData("rhvp");
        $("#id_excel_rhvp").prop("disabled", false);
    };
    document.getElementById("id_submitbutton_hvp_course").onclick = function () {

        $('#attivita--container').hide();
        $('#azione--container').hide();
        $('#ip--container').hide();
        $('#azienda--container').show();
        $('#orarioA--container').show();
        $('#orarioA--span').html("Ora accesso");
        $('#orarioL--container').show();
        $('#orarioL--span').html("Ora uscita");
        $('#corso--container').show();
        $('#ruolo--container').hide();
        $('#tc--container').show();
        $('#aula--container').hide();
        $('#lezione--container').show();
        $('#data--container').show();
        $('#ts--container').show();
        $('#ts--span').html("Tempo sessione");
        showPleaseWait("Generazione Reportistica...");
        getData("hvpName");
        $("#id_excel_hvp_course").prop("disabled", false);
    };
    document.getElementById("id_submitbutton_zoom_course").onclick = function () {

        $('#attivita--container').hide();
        $('#azione--container').hide();
        $('#ip--container').hide();
        $('#azienda--container').show();
        $('#orarioA--container').show();
        $('#orarioA--span').html("Ora accesso");
        $('#orarioL--container').show();
        $('#orarioL--span').html("Ora uscita");
        $('#corso--container').show();
        $('#ruolo--container').show();
        $('#tc--container').show();
        $('#aula--container').show();
        $('#lezione--container').hide();
        $('#data--container').show();
        $('#ts--container').hide();
        $('#ts--span').html("Tempo sessione");
        showPleaseWait("Generazione Reportistica...");
        getData("zoomName");
        $("#id_excel_zoom_course").prop("disabled", false);
    };
    document.getElementById("id_submitbutton_shvp").onclick = function () {

        $('#attivita--container').hide();
        $('#azione--container').hide();
        $('#ip--container').hide();
        $('#azienda--container').show();
        $('#orarioA--container').hide();
        $('#orarioL--container').hide();
        $('#corso--container').hide();
        $('#ruolo--container').hide();
        $('#tc--container').show();
        $('#aula--container').hide();
        $('#lezione--container').hide();
        $('#data--container').hide();
        $('#ts--container').hide();
        showPleaseWait("Generazione Reportistica...");
        getData("shvp");
        $("#id_excel_shvp").prop("disabled", false);
    };
    document.getElementById("id_submitbutton_timein_corso").onclick = function () {
        showPleaseWait("Generazione Reportistica...");
        getData("timeinName");
        $("#id_excel_timein_corso").prop("disabled", false);
    };
    document.getElementById("id_submitbutton_shvp_corso").onclick = function () {
        showPleaseWait("Generazione Reportistica...");
        getData("shvpName");
        $("#id_excel_shvp_corso").prop("disabled", false);
    };
    if(document.body.contains(document.getElementById("id_submitbutton_shvp_course_new"))) {
        document.getElementById("id_submitbutton_shvp_course_new").onclick = function () {
            showPleaseWait("Generazione Reportistica...");
            getData("shvp_course_new");
            $("#id_excel_shvp_course_new").prop("disabled", false);
        };
    }

    document.getElementById("id_submitbutton_rhvp_corso").onclick = function () {
        showPleaseWait("Generazione Reportistica...");
        getData("rhvpName");
        $("#id_excel_rhvp_corso").prop("disabled", false);
    };
    if(document.body.contains(document.getElementById("id_submitbutton_hvp_new"))) {
        document.getElementById("id_submitbutton_hvp_new").onclick = function () {
            $('#attivita--container').hide();
            $('#azione--container').hide();
            $('#ip--container').hide();
            $('#azienda--container').show();
            $('#orarioA--container').show();
            $('#orarioA--span').html("Ora accesso");
            $('#orarioL--container').show();
            $('#orarioL--span').html("Ora uscita");
            $('#corso--container').show();
            $('#ruolo--container').hide();
            $('#tc--container').show();
            $('#aula--container').hide();
            $('#lezione--container').show();
            $('#data--container').show();
            $('#ts--container').show();
            $('#ts--span').html("Tempo sessione");
            showPleaseWait("Generazione Reportistica...");
            getData("hvp_new");
            $("#id_excel_hvp_new").prop("disabled", false);
        };
    }
    if(document.body.contains(document.getElementById("id_submitbutton_shvp_new"))) {
        document.getElementById("id_submitbutton_shvp_new").onclick = function () {

            $('#attivita--container').hide();
            $('#azione--container').hide();
            $('#ip--container').hide();
            $('#azienda--container').show();
            $('#orarioA--container').show();
            $('#orarioA--span').html("Ora accesso");
            $('#orarioL--container').show();
            $('#orarioL--span').html("Ora uscita");
            $('#corso--container').show();
            $('#ruolo--container').hide();
            $('#tc--container').show();
            $('#aula--container').hide();
            $('#lezione--container').show();
            $('#data--container').show();
            $('#ts--container').show();
            $('#ts--span').html("Tempo sessione");
            showPleaseWait("Generazione Reportistica...");
            getData("shvp_new");
            $("#id_excel_shvp_new").prop("disabled", false);
        };
    }
    if(document.body.contains(document.getElementById("id_submitbutton_summaryhvp_new"))) {
        document.getElementById("id_submitbutton_summaryhvp_new").onclick = function () {

            $('#attivita--container').hide();
            $('#azione--container').hide();
            $('#ip--container').hide();
            $('#azienda--container').show();
            $('#orarioA--container').show();
            $('#orarioA--span').html("Ora accesso");
            $('#orarioL--container').show();
            $('#orarioL--span').html("Ora uscita");
            $('#corso--container').show();
            $('#ruolo--container').hide();
            $('#tc--container').show();
            $('#aula--container').hide();
            $('#lezione--container').show();
            $('#data--container').show();
            $('#ts--container').show();
            $('#ts--span').html("Tempo sessione");
            showPleaseWait("Generazione Reportistica...");
            getData("summaryhvp_new");
            $("#id_excel_summaryhvp_new").prop("disabled", false);
        };
    }


    //EXCEL
    document.getElementById("id_excel_sintesi").onclick = function () {
        $('#attivita--container').hide();
        $('#azione--container').hide();
        $('#ip--container').hide();
        $('#azienda--container').show();
        $('#orarioA--container').show();
        $('#orarioL--container').show();
        $('#corso--container').hide();
        $('#ruolo--container').hide();
        $('#tc--container').show();
        $('#aula--container').hide();
        $('#lezione--container').hide();
        $('#data--container').show();
        $('#ts--container').hide();
        $('#azienda--container').show();

        showPleaseWait("Generazione Excel");
        getExcel("sintesi");
    };
    document.getElementById("id_excel_shvp_corso").onclick = function () {
        showPleaseWait("Generazione Excel");
        getExcel("shvpName");
    };
    if(document.body.contains(document.getElementById("id_excel_shvp_course_new"))) {
        document.getElementById("id_excel_shvp_course_new").onclick = function () {
            showPleaseWait("Generazione Excel");
            getExcel("shvp_course_new");
        };
    }
    document.getElementById("id_excel_rhvp_corso").onclick = function () {
        showPleaseWait("Generazione Excel");
        getExcel("rhvpName");
    };
    document.getElementById("id_excel_timein_corso").onclick = function () {
        showPleaseWait("Generazione Excel");
        getExcel("timeinName");
    };
    document.getElementById("id_excel_total_iscritto").onclick = function () {
        $('#attivita--container').show();
        $('#azione--container').show();
        $('#ip--container').show();
        $('#azienda--container').hide();
        $('#orarioA--container').hide();
        $('#orarioL--container').hide();
        $('#corso--container').hide();
        $('#ruolo--container').hide();
        $('#tc--container').hide();
        $('#aula--container').hide();
        $('#lezione--container').hide();
        $('#data--container').show();
        $('#ts--container').hide();
        showPleaseWait("Generazione Excel");
        getExcel("totalName");
    };
    document.getElementById("id_excel_sintesi_iscritto").onclick = function () {

        $('#attivita--container').hide();
        $('#azione--container').hide();
        $('#ip--container').hide();
        $('#azienda--container').show();
        $('#orarioA--container').show();
        $('#orarioL--container').show();
        $('#corso--container').hide();
        $('#ruolo--container').hide();
        $('#tc--container').show();
        $('#aula--container').hide();
        $('#lezione--container').hide();
        $('#data--container').show();
        $('#ts--container').hide();
        $('#azienda--container').show();
        showPleaseWait("Generazione Excel");
        getExcel("sintesiName");
    };
    document.getElementById("id_excel_timein").onclick = function () {
        $('#attivita--container').hide();
        $('#azione--container').hide();
        $('#ip--container').hide();
        $('#azienda--container').show();
        $('#orarioA--container').hide();
        $('#orarioL--container').hide();
        $('#corso--container').hide();
        $('#ruolo--container').hide();
        $('#tc--container').show();
        $('#aula--container').hide();
        $('#lezione--container').hide();
        $('#data--container').hide();
        $('#ts--container').hide();
        showPleaseWait("Generazione Excel");
        getExcel("timein");
    };
    document.getElementById("id_excel_total").onclick = function () {
        $('#attivita--container').show();
        $('#azione--container').show();
        $('#ip--container').show();
        $('#azienda--container').hide();
        $('#orarioA--container').hide();
        $('#orarioL--container').hide();
        $('#corso--container').hide();
        $('#ruolo--container').hide();
        $('#tc--container').hide();
        $('#aula--container').hide();
        $('#lezione--container').hide();
        $('#data--container').show();
        $('#ts--container').hide();
        showPleaseWait("Generazione Excel");
        getExcel("total");
    };
    document.getElementById("id_excel_zoom").onclick = function () {

        $('#data--container').show();
        $('#azienda--container').show();
        showPleaseWait("Generazione Excel");
        getExcel("zoom");

    };
    document.getElementById("id_excel_bbb").onclick = function () {
        $('#data--container').show();
        $('#azienda--container').show();

        showPleaseWait("Generazione Excel");
        getExcel("bbb");

    };
    document.getElementById("id_excel_sbbb").onclick = function () {
        $('#data--container').hide();
        $('#azienda--container').show();

        showPleaseWait("Generazione Excel");
        getExcel("sbbb");

    };
    document.getElementById("id_excel_sbbb_iscritto").onclick = function () {

        showPleaseWait("Generazione Excel");
        getExcel("sbbbName");
    };
    document.getElementById("id_excel_bbb_course").onclick = function () {
        $('#data--container').show();
        $('#azienda--container').show();

        showPleaseWait("Generazione Excel");
        getExcel("bbbName");

    };
    document.getElementById("id_excel_hvp").onclick = function () {
        $('#attivita--container').hide();
        $('#azione--container').hide();
        $('#ip--container').hide();
        $('#azienda--container').show();
        $('#orarioA--container').show();
        $('#orarioL--container').show();
        $('#corso--container').show();
        $('#ruolo--container').hide();
        $('#tc--container').show();
        $('#aula--container').hide();
        $('#lezione--container').show();
        $('#data--container').show();
        $('#ts--container').show();

        showPleaseWait("Generazione Excel");
        getExcel("hvp");
    };
    if(document.body.contains(document.getElementById("id_excel_hvp_new"))) {
        document.getElementById("id_excel_hvp_new").onclick = function () {
            $('#attivita--container').hide();
            $('#azione--container').hide();
            $('#ip--container').hide();
            $('#azienda--container').show();
            $('#orarioA--container').show();
            $('#orarioL--container').show();
            $('#corso--container').show();
            $('#ruolo--container').hide();
            $('#tc--container').show();
            $('#aula--container').hide();
            $('#lezione--container').show();
            $('#data--container').show();
            $('#ts--container').show();

            showPleaseWait("Generazione Excel");
            getExcel("hvp_new");
        };
    }
    document.getElementById("id_excel_summaryhvp").onclick = function () {
        $('#attivita--container').hide();
        $('#azione--container').hide();
        $('#ip--container').hide();
        $('#azienda--container').show();
        $('#orarioA--container').show();
        $('#orarioL--container').show();
        $('#corso--container').show();
        $('#ruolo--container').hide();
        $('#tc--container').show();
        $('#aula--container').hide();
        $('#lezione--container').show();
        $('#data--container').show();
        $('#ts--container').show();

        showPleaseWait("Generazione Excel");
        getExcel("summaryhvp");
    };
    if(document.body.contains(document.getElementById("id_excel_summaryhvp_new"))) {
        document.getElementById("id_excel_summaryhvp_new").onclick = function () {
            $('#attivita--container').hide();
            $('#azione--container').hide();
            $('#ip--container').hide();
            $('#azienda--container').show();
            $('#orarioA--container').show();
            $('#orarioL--container').show();
            $('#corso--container').show();
            $('#ruolo--container').hide();
            $('#tc--container').show();
            $('#aula--container').hide();
            $('#lezione--container').show();
            $('#data--container').show();
            $('#ts--container').show();

            showPleaseWait("Generazione Excel");
            getExcel("summaryhvp_new");
        };
    }
    document.getElementById("id_excel_videotimepro").onclick = function () {
        $('#attivita--container').hide();
        $('#azione--container').hide();
        $('#ip--container').hide();
        $('#azienda--container').show();
        $('#orarioA--container').show();
        $('#orarioL--container').show();
        $('#corso--container').show();
        $('#ruolo--container').hide();
        $('#tc--container').show();
        $('#aula--container').hide();
        $('#lezione--container').show();
        $('#data--container').show();
        $('#ts--container').show();

        showPleaseWait("Generazione Excel");
        getExcel("videotimepro");
    };
    document.getElementById("id_excel_rhvp").onclick = function () {
        $('#data--container').hide();
        $('#azienda--container').show();

        showPleaseWait("Generazione Excel");
        getExcel("rhvp");
    };
    document.getElementById("id_excel_shvp").onclick = function () {
        $('#attivita--container').hide();
        $('#azione--container').hide();
        $('#ip--container').hide();
        $('#azienda--container').show();
        $('#orarioA--container').hide();
        $('#orarioL--container').hide();
        $('#corso--container').hide();
        $('#ruolo--container').hide();
        $('#tc--container').show();
        $('#aula--container').hide();
        $('#lezione--container').hide();
        $('#data--container').hide();
        $('#ts--container').hide();
        showPleaseWait("Generazione Excel");
        getExcel("shvp");
    };
    if(document.body.contains(document.getElementById("id_excel_shvp_new"))) {
        document.getElementById("id_excel_shvp_new").onclick = function () {
            $('#attivita--container').hide();
            $('#azione--container').hide();
            $('#ip--container').hide();
            $('#azienda--container').show();
            $('#orarioA--container').hide();
            $('#orarioL--container').hide();
            $('#corso--container').hide();
            $('#ruolo--container').hide();
            $('#tc--container').show();
            $('#aula--container').hide();
            $('#lezione--container').hide();
            $('#data--container').hide();
            $('#ts--container').hide();
            showPleaseWait("Generazione Excel");
            getExcel("shvp_new");
        };
    }
    document.getElementById("id_excel_hvp_course").onclick = function () {
        $('#attivita--container').hide();
        $('#azione--container').hide();
        $('#ip--container').hide();
        $('#azienda--container').show();
        $('#orarioA--container').show();
        $('#orarioL--container').show();
        $('#corso--container').show();
        $('#ruolo--container').hide();
        $('#tc--container').show();
        $('#aula--container').hide();
        $('#lezione--container').show();
        $('#data--container').show();
        $('#ts--container').show();

        showPleaseWait("Generazione Excel");
        getExcel("hvpName");
    };
    document.getElementById("id_excel_zoom_course").onclick = function () {
        $('#data--container').show();
        $('#azienda--container').show();

        showPleaseWait("Generazione Excel");
        getExcel("zoomName");
    };
    document.getElementById("id_excel_s_zoom").onclick = function () {
        $('#data--container').show();
        $('#azienda--container').show();

        showPleaseWait("Generazione Excel");
        getExcel("s_zoom");
    };
    document.getElementById("id_excel_s_zoom_iscritto").onclick = function () {

        showPleaseWait("Generazione Excel");
        getExcel("s_zoomName");
    };

    document.getElementById("id_enterprise_total").onchange = function () {
        $("#id_excel_total").prop("disabled", true);

    };
    document.getElementById("id_start_total").onchange = function () {
        $("#id_excel_total").prop("disabled", true);

    };
    document.getElementById("id_start_total_iscritto").onchange = function () {
        $("#id_excel_total_iscritto").prop("disabled", true);

    };
    document.getElementById("id_end_total_iscritto").onchange = function () {
        $("#id_excel_total_iscritto").prop("disabled", true);

    };
    document.getElementById("id_end_total").onchange = function () {
        $("#id_excel_total").prop("disabled", true);

    };
    document.getElementById("id_iscritto_total").onchange = function () {
        $("#id_excel_total_iscritto").prop("disabled", true);

    };
    document.getElementById("id_enterprise_sintesi").onchange = function () {
        $("#id_excel_sintesi").prop("disabled", true);

    };
    document.getElementById("id_start_sintesi").onchange = function () {
        $("#id_excel_sintesi").prop("disabled", true);

    };
    document.getElementById("id_end_sintesi").onchange = function () {
        $("#id_excel_sintesi").prop("disabled", true);

    };
    document.getElementById("id_enterprise_timein").onchange = function () {
        $("#id_excel_timein").prop("disabled", true);

    };
    document.getElementById("id_start_timein").onchange = function () {
        $("#id_excel_timein").prop("disabled", true);

    };
    document.getElementById("id_end_timein").onchange = function () {
        $("#id_excel_timein").prop("disabled", true);

    };
    document.getElementById("id_enterprise_bbb").onchange = function () {
        $("#id_excel_bbb").prop("disabled", true);
        azienda = $('#id_enterprise_bbb').val();
        getCorsi("bbb", azienda);

    };
    document.getElementById("id_enterprise_zoom").onchange = function () {
        $("#id_excel_zoom").prop("disabled", true);
        azienda = $('#id_enterprise_zoom').val();
        getCorsi("zoom", azienda);

    };

    $("#id_iscritto_sbbb").change(function () {
        $("#id_excel_sbbb_iscritto").prop("disabled", true);
        idCorso = $('#id_iscritto_sbbb').val();
        getAziendeAssociate(idCorso, "sbbbN");
    });
    $("#id_corso_shvp").change(function () {
        $("#id_excel_shvp_corso").prop("disabled", true);
        idCorso = $('#id_corso_shvp').val();
        getAziendeAssociate(idCorso, "shvpN");
    });
    $("#id_corso_rhvp").change(function () {
        $("#id_excel_rhvp_corso").prop("disabled", true);
        idCorso = $('#id_corso_rhvp').val();
        getAziendeAssociate(idCorso, "rhvpN");
    });
    $("#id_corso_timeinN").change(function () {
        $("#id_excel_timein_corso").prop("disabled", true);
        idCorso = $('#id_corso_timeinN').val();
        getAziendeAssociate(idCorso, "timeinN");
    });
    $("#id_courses_bbb").change(function () {
        $("#id_excel_bbb_course").prop("disabled", true);
        idCorso = $('#id_courses_bbb').val();
        getAziendeAssociate(idCorso, "bbbName");
    });
    $("#id_courses_hvp").change(function () {
        $("#id_excel_hvp_course").prop("disabled", true);
        idCorso = $('#id_courses_hvp').val();
        getAziendeAssociate(idCorso, "hvpName");
    });
    $("#id_courses_zoom").change(function () {
        $("#id_excel_zoom_course").prop("disabled", true);
        idCorso = $('#id_courses_zoom').val();
        getAziendeAssociate(idCorso, "zoomName");
    });
    $("#id_iscritto_s_zoom").change(function () {
        $("#id_excel_s_zoom_iscritto").prop("disabled", true);
        idCorso = $('#id_iscritto_s_zoom').val();
        getAziendeAssociate(idCorso, "s_zoomName");
    });
    $("#id_enterprise_bbbName").change(function () {
        $("#id_excel_bbb_course").prop("disabled", true);
    });
    $("#id_enterprise_timeinN").change(function () {
        $("#id_excel_timein_corso").prop("disabled", true);
    });
    $("#id_start_timein_corso").change(function () {
        $("#id_excel_timein_corso").prop("disabled", true);
    });
    $("#id_end_timein_corso").change(function () {
        $("#id_excel_timein_corso").prop("disabled", true);
    });
    $("#id_enterprise_hvpName").change(function () {
        $("#id_excel_hvp_course").prop("disabled", true);
    });
    $("#id_enterprise_zoomName").change(function () {
        $("#id_excel_zoom_course").prop("disabled", true);
    });
    $("#id_enterprise_s_zoomName").change(function () {
        $("#id_excel_s_zoom_iscritto").prop("disabled", true);
    });

    document.getElementById("id_start_bbb").onchange = function () {
        $("#id_excel_bbb").prop("disabled", true);

    };
    document.getElementById("id_end_bbb").onchange = function () {
        $("#id_excel_bbb").prop("disabled", true);

    };
    document.getElementById("id_course_bbb").onchange = function () {
        $("#id_excel_bbb").prop("disabled", true);

    };
    document.getElementById("id_enterprise_hvp").onchange = function () {
        $("#id_excel_hvp").prop("disabled", true);
        azienda = $('#id_enterprise_hvp').val();
        getCorsi("hvp", azienda);

    };
    if(document.body.contains(document.getElementById("id_enterprise_hvp_new"))) {
        document.getElementById("id_enterprise_hvp_new").onchange = function () {
            $("#id_excel_hvp_new").prop("disabled", true);
            azienda = $('#id_enterprise_hvp').val();
            getCorsi("hvp_new", azienda);

        };
    }
    document.getElementById("id_start_hvp").onchange = function () {
        $("#id_excel_hvp").prop("disabled", true);

    };
    document.getElementById("id_end_hvp").onchange = function () {
        $("#id_excel_hvp").prop("disabled", true);

    };
    document.getElementById("id_course_hvp").onchange = function () {
        $("#id_excel_hvp").prop("disabled", true);

    };
    document.getElementById("id_corso_shvp").onchange = function () {
        $("#id_excel_shvp_corso").prop("disabled", true);

    };
    document.getElementById("id_enterprise_shvpN").onchange = function () {
        $("#id_excel_shvp_corso").prop("disabled", true);

    };
    document.getElementById("id_start_shvp_corso").onchange = function () {
        $("#id_excel_shvp_corso").prop("disabled", true);

    };
    document.getElementById("id_end_shvp_corso").onchange = function () {
        $("#id_excel_shvp_corso").prop("disabled", true);

    };
    document.getElementById("id_corso_rhvp").onchange = function () {
        $("#id_excel_rhvp_corso").prop("disabled", true);

    };
    document.getElementById("id_enterprise_rhvpN").onchange = function () {
        $("#id_excel_rhvp_corso").prop("disabled", true);

    };
    document.getElementById("id_start_rhvp_corso").onchange = function () {
        $("#id_excel_rhvp_corso").prop("disabled", true);

    };
    document.getElementById("id_end_rhvp_corso").onchange = function () {
        $("#id_excel_rhvp_corso").prop("disabled", true);

    };
    document.getElementById("id_enterprise_shvp").onchange = function () {
        $("#id_excel_shvp").prop("disabled", true);

    };
    document.getElementById("id_start_shvp").onchange = function () {
        $("#id_excel_shvp").prop("disabled", true);

    };
    document.getElementById("id_end_shvp").onchange = function () {
        $("#id_excel_shvp").prop("disabled", true);

    };
    document.getElementById("id_enterprise_rhvp").onchange = function () {
        $("#id_excel_rhvp").prop("disabled", true);

    };
    document.getElementById("id_start_rhvp").onchange = function () {
        $("#id_excel_rhvp").prop("disabled", true);

    };
    document.getElementById("id_end_rhvp").onchange = function () {
        $("#id_excel_rhvp").prop("disabled", true);

    };
    document.getElementById("id_enterprise_s_zoom").onchange = function () {
        $("#id_excel_s_zoom").prop("disabled", true);

    };
    document.getElementById("id_start_s_zoom").onchange = function () {
        $("#id_excel_s_zoom").prop("disabled", true);

    };
    document.getElementById("id_end_s_zoom").onchange = function () {
        $("#id_excel_s_zoom").prop("disabled", true);

    };
    document.getElementById("id_start_zoom").onchange = function () {
        $("#id_excel_zoom").prop("disabled", true);

    };
    document.getElementById("id_end_zoom").onchange = function () {
        $("#id_excel_zoom").prop("disabled", true);

    };
    document.getElementById("id_course_zoom").onchange = function () {
        $("#id_excel_zoom").prop("disabled", true);

    };
    document.getElementById("id_courses_zoom").onchange = function () {
        $("#id_excel_zoom_course").prop("disabled", true);

    };
    document.getElementById("id_start_zoom_course").onchange = function () {
        $("#id_excel_zoom_course").prop("disabled", true);

    };
    document.getElementById("id_end_zoom_course").onchange = function () {
        $("#id_excel_zoom_course").prop("disabled", true);

    };
    document.getElementById("id_courses_hvp").onchange = function () {
        $("#id_excel_hvp_course").prop("disabled", true);

    };
    document.getElementById("id_start_hvp_course").onchange = function () {
        $("#id_excel_hvp_course").prop("disabled", true);

    };
    document.getElementById("id_end_hvp_course").onchange = function () {
        $("#id_excel_hvp_course").prop("disabled", true);

    };
    document.getElementById("id_courses_bbb").onchange = function () {
        $("#id_excel_bbb_course").prop("disabled", true);

    };
    document.getElementById("id_start_bbb_course").onchange = function () {
        $("#id_excel_bbb_course").prop("disabled", true);

    };
    document.getElementById("id_end_bbb_course").onchange = function () {
        $("#id_excel_bbb_course").prop("disabled", true);

    };
    document.getElementById("id_iscritto_sbbb").onchange = function () {
        $("#id_excel_sbbb_iscritto").prop("disabled", true);

    };
    document.getElementById("id_start_sbbb_iscritto").onchange = function () {
        $("#id_excel_sbbb_iscritto").prop("disabled", true);

    };
    document.getElementById("id_end_sbbb_iscritto").onchange = function () {
        $("#id_excel_sbbb_iscritto").prop("disabled", true);

    };
    document.getElementById("id_iscritto_s_zoom").onchange = function () {
        $("#id_excel_s_zoom_iscritto").prop("disabled", true);

    };
    document.getElementById("id_start_s_zoom_iscritto").onchange = function () {
        $("#id_excel_s_zoom_iscritto").prop("disabled", true);

    };
    document.getElementById("id_end_s_zoom_iscritto").onchange = function () {
        $("#id_excel_s_zoom_iscritto").prop("disabled", true);

    };

    $('#report').on('column-visibility.dt', function (e, settings, column, state) {

        if ($('#report').DataTable().columns(column).nodes()[0][0]) {
            let className = $('#report').DataTable().columns(column).nodes()[0][0].className;
            className = className.trim();
            className = className.split(" ");
            if (state) {
                checked = checked.filter(e => e !== className[0]);
            } else {
                checked.push(className[0]);
            }

        }
    });
    /*document.getElementById("flexCheckAllZoom").onclick = function () {
     if ($(this).is(':checked')) {
     $('#id_course_zoom').attr('disabled', 'disabled');
     } else {
     $('#id_course_zoom').removeAttr('disabled');
     }
     };*/

};
